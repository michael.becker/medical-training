$("#address\\.street").attr("list", "addresses");
		    $("#address\\.street").on("input", function(e) {
		        var val = $(this).val();
		        if(val === "") {
		            return;
                }

                // || val.length < 3 -> return

		        $.post("/admin/geolocations/findAddress", {address:val}, function(res) {
		            var dataList = $("#addresses");
		            dataList.empty();

		            if (Array.isArray(res)) {
		                res.forEach(function(element) {
		                    var opt = $("<option></option>");
		                    opt.attr("value", element['address']['fullAddress']);
		                    opt.attr("id", element['id']);
		                    dataList.append(opt);
		                });
		            }