package org.infai.ddls.medicaltrainingv2.model.admin;

import java.util.List;

import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;

public interface ImportList<T extends AbstractTrainingAppEntity> {
    public <U extends ImportListEntity<T>> List<U> getEntities();
}
