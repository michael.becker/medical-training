package org.infai.ddls.medicaltrainingv2.data.spec;

import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.springframework.data.jpa.domain.Specification;

public interface TrainingAppSpecification<T extends AbstractTrainingAppEntity> extends Specification<T> {
//    public static final String ATTRIBUTE_NAME_ID = "id";
}
