package org.infai.ddls.medicaltrainingv2.data.spec.training;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltrainingv2.data.filter.training.TrainingSchemeSegmentFilter;
import org.infai.ddls.medicaltrainingv2.data.spec.AbstractTrainingAppEntitySpecification;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingSchemeSegment;

@SuppressWarnings("serial")
public class TrainingSchemeSegmentSpecification extends AbstractTrainingAppEntitySpecification<TrainingSchemeSegment, TrainingSchemeSegmentFilter> {

    public TrainingSchemeSegmentSpecification(TrainingSchemeSegmentFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<TrainingSchemeSegment> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	// TODO Auto-generated method stub

    }

}
