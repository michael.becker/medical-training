package org.infai.ddls.medicaltrainingv2.data.spec.core;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltrainingv2.data.filter.core.MedicalFieldFilter;
import org.infai.ddls.medicaltrainingv2.data.spec.NamedTrainingAppEntitySpecification;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.infai.ddls.medicaltrainingv2.model.HasChildren;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField_;

@SuppressWarnings("serial")
public class MedicalFieldSpecification extends NamedTrainingAppEntitySpecification<MedicalField, MedicalFieldFilter> {
    /**
     * Erstellt ein Prädikat beginnend ab dem angegebenen Pfad. Der Pfad wird um die
     * Abfrage nach dem Attribute medicalField ergänzt.
     *
     * @param path
     * @param query
     * @param criteriaBuilder
     * @return
     */
    public static <T extends AbstractTrainingAppEntity> Predicate toPredicate(MedicalField field, Path<T> path, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	List<Predicate> fieldPredicates = new ArrayList<>();

	if (null != field) {
	    Predicate equalFieldPredicate = criteriaBuilder.equal(path, field);
	    fieldPredicates.add(equalFieldPredicate);

	    for (HasChildren<MedicalField> medicalField : field.getAllChildren()) {
		Predicate childFieldPredicate = criteriaBuilder.equal(path, medicalField);
		fieldPredicates.add(childFieldPredicate);
	    }

	    return criteriaBuilder.or(fieldPredicates.toArray(new Predicate[fieldPredicates.size()]));
	} else {
	    return criteriaBuilder.conjunction();
	}
    }

    public MedicalFieldSpecification(MedicalFieldFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<MedicalField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	super.initPredicates(root, query, criteriaBuilder);

	if (null != filter.getParent()) {
	    addPredicate(criteriaBuilder.equal(root.get(MedicalField_.parent), filter.getParent()));
	}
    }
}
