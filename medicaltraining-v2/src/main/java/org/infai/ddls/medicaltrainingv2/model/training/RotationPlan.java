package org.infai.ddls.medicaltrainingv2.model.training;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.infai.ddls.medicaltrainingv2.model.person.Registrar;

import lombok.Getter;
import lombok.Setter;

@Entity
@Audited
@Getter
@Setter
public class RotationPlan extends AbstractTrainingAppEntity {
    @ManyToOne(optional = false)
    private Registrar registrar;
    @ManyToOne(optional = false)
    private TrainingScheme trainingScheme;
    @OneToMany(mappedBy = RotationPlanSegment_.ROTATION_PLAN, cascade = CascadeType.ALL)
    @OrderBy(RotationPlanSegment_.SEGMENT_BEGIN)
    private List<RotationPlanSegment> rotationPlanSegments = new ArrayList<>();

    @Transient
    public List<RotationPlanSegment> getFinishedSegments(LocalDate date) {
	return getFinishedSegments(date, null);
    }

    /**
     * Gibt diejenigen Weiterbildungsabschnitte zurück, die für den gegebenen
     * Abschnitt der Weiterbildungsordnung bereits verplant sind.
     *
     * @param date
     * @param trainingSchemeSegment
     * @return
     */
    @Transient
    public List<RotationPlanSegment> getFinishedSegments(LocalDate date, TrainingSchemeSegment trainingSchemeSegment) {
	List<RotationPlanSegment> finishedSegments = new ArrayList<>(rotationPlanSegments.size());

	for (RotationPlanSegment segment : rotationPlanSegments) {
	    if (segment.getSegmentEnd().isBefore(date)) {
		if (null == trainingSchemeSegment || (null != segment.getAssignedSegment() && segment.getAssignedSegment().equals(trainingSchemeSegment))) {
		    finishedSegments.add(segment);
		}
	    }
	}

	return finishedSegments;
    }

    /*
     * Gibt diejenigen {@link #rotationPlanSegments} zurück, welche zum angegebenen
     * Datum noch in Ableistung bzw. für die Zukunft geplant sind. Dies entspricht
     * denjenigen Segmenten, deren Enddatum größer oder gleich ist als das
     * angegebene Datum.
     *
     * @param date
     *
     * @return
     */
    @Transient
    public List<RotationPlanSegment> getPlannedSegments(LocalDate date) {
	return getPlannedSegments(date, null);
    }

    /**
     * Gibt diejenigen Weiterbildungsabschnitte zurück, die für den gegebenen
     * Abschnitt der Weiterbildungsordnung noch geplant sind.
     *
     * @param date
     * @param trainingSchemeSegment
     * @return
     */
    @Transient
    public List<RotationPlanSegment> getPlannedSegments(LocalDate date, TrainingSchemeSegment trainingSchemeSegment) {
	List<RotationPlanSegment> plannedSegments = new ArrayList<>(rotationPlanSegments.size());

	for (RotationPlanSegment segment : rotationPlanSegments) {
	    if (segment.getSegmentEnd().isAfter(date) || segment.getSegmentEnd().equals(date)) {
		if (null == trainingSchemeSegment || (null != segment.getAssignedSegment() && segment.getAssignedSegment().equals(trainingSchemeSegment))) {
		    plannedSegments.add(segment);
		}
	    }
	}

	return plannedSegments;
    }

    @Transient
    public List<RotationPlanSegment> getUnassignedSegments() {
	List<RotationPlanSegment> unassignedSegments = new ArrayList<>();

	for (RotationPlanSegment rotationPlanSegment : rotationPlanSegments) {
	    if (null == rotationPlanSegment.getAssignedSegment()) {
		unassignedSegments.add(rotationPlanSegment);
	    }
	}

	return unassignedSegments;
    }
}
