package org.infai.ddls.medicaltrainingv2.data.spec.core;

import org.infai.ddls.medicaltrainingv2.data.filter.core.TitleFilter;
import org.infai.ddls.medicaltrainingv2.data.spec.NamedTrainingAppEntitySpecification;
import org.infai.ddls.medicaltrainingv2.model.core.Title;

@SuppressWarnings("serial")
public class TitleSpecification extends NamedTrainingAppEntitySpecification<Title, TitleFilter> {
    public TitleSpecification(TitleFilter filter) {
	super(filter);
    }
}
