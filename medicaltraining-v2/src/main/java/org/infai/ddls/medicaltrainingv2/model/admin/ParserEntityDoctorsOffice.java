package org.infai.ddls.medicaltrainingv2.model.admin;

import org.infai.ddls.medicaltrainingv2.model.location.DoctorsOffice;

import de.caterdev.utils.parser.ParserEntity;

public class ParserEntityDoctorsOffice extends ParserEntity<DoctorsOffice> {
    public ParserEntityDoctorsOffice() {
    }

    public ParserEntityDoctorsOffice(ParserEntity<DoctorsOffice> parserEntity) {
	setColumns(parserEntity.getColumns());
	setEntity(parserEntity.getEntity());
	setRowNumber(parserEntity.getRowNumber());
    }
}
