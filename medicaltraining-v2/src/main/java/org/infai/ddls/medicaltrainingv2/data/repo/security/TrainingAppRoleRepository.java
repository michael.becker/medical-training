package org.infai.ddls.medicaltrainingv2.data.repo.security;

import java.util.UUID;

import org.infai.ddls.medicaltrainingv2.model.security.TrainingAppRole;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainingAppRoleRepository extends PagingAndSortingRepository<TrainingAppRole, UUID> {

}
