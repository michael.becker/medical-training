package org.infai.ddls.medicaltrainingv2.model.admin;

import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;

import de.caterdev.utils.parser.ParserEntity;

public interface ImportListEntity<T extends AbstractTrainingAppEntity> {
    public ParserEntity<T> getOriginalEntity();
}
