package org.infai.ddls.medicaltrainingv2.model.admin;

import javax.validation.Valid;

import org.infai.ddls.medicaltrainingv2.model.core.Sex;

import de.caterdev.utils.parser.ParserEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImportSexListEntity implements ImportListEntity<Sex> {
    @Valid
    private ParserEntitySex originalEntity;
    private boolean doImport = true;

    public ImportSexListEntity() {
    }

    public ImportSexListEntity(ParserEntity<Sex> originalEntity) {
	setOriginalEntity(new ParserEntitySex(originalEntity));
    }
}
