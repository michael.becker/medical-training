package org.infai.ddls.medicaltrainingv2.data.spec.location;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltrainingv2.data.filter.location.RegionFilter;
import org.infai.ddls.medicaltrainingv2.data.spec.AbstractTrainingAppEntitySpecification;
import org.infai.ddls.medicaltrainingv2.model.location.Region;

@SuppressWarnings("serial")
public class RegionSpecification extends AbstractTrainingAppEntitySpecification<Region, RegionFilter> {

    public RegionSpecification(RegionFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<Region> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	// TODO Auto-generated method stub

    }

}
