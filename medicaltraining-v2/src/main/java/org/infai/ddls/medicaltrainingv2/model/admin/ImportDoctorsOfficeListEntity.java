package org.infai.ddls.medicaltrainingv2.model.admin;

import javax.validation.Valid;

import org.infai.ddls.medicaltrainingv2.model.location.DoctorsOffice;

import de.caterdev.utils.parser.ParserEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImportDoctorsOfficeListEntity implements ImportListEntity<DoctorsOffice> {
    @Valid
    private ParserEntityDoctorsOffice originalEntity;
    private boolean doImport = true;

    public ImportDoctorsOfficeListEntity() {
    }

    public ImportDoctorsOfficeListEntity(ParserEntity<DoctorsOffice> originalEntity) {
	setOriginalEntity(new ParserEntityDoctorsOffice(originalEntity));
    }
}
