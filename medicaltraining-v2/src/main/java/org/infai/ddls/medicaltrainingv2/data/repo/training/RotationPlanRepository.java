package org.infai.ddls.medicaltrainingv2.data.repo.training;

import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlan;
import org.springframework.stereotype.Repository;

@Repository
public interface RotationPlanRepository extends TrainingAppEntityRepository<RotationPlan> {

}
