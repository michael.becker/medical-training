package org.infai.ddls.medicaltrainingv2.model.person;

import javax.persistence.Entity;

import org.hibernate.envers.Audited;

@Entity
@Audited
public abstract class Doctor<T extends Doctor<?>> extends Person<T> {

}
