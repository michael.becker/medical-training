package org.infai.ddls.medicaltrainingv2.model.person;

import javax.persistence.Embeddable;
import javax.validation.constraints.Email;

import lombok.Getter;
import lombok.Setter;

@Embeddable
@Getter
@Setter
public class ContactInfo {
    @Email
    private String email;
    private String phoneNumber;
    private String phoneNumber2;
    private String faxNumber;
    private String web;
}
