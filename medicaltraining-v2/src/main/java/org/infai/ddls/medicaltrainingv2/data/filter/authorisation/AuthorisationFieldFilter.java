package org.infai.ddls.medicaltrainingv2.data.filter.authorisation;

import org.infai.ddls.medicaltrainingv2.data.filter.TrainingAppEntityFilter;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthorisationFieldFilter extends TrainingAppEntityFilter<AuthorisationField> {
    private AbstractTrainingSite trainingSite;
    private AuthorisedDoctor authorisedDoctor;
}
