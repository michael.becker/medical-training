package org.infai.ddls.medicaltrainingv2.controller;

public enum SaveAction {
    save, saveAndClose;

    public static final String REQUEST_PARAM = "saveAction";
}
