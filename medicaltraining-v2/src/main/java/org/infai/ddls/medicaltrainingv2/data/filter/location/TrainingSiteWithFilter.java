package org.infai.ddls.medicaltrainingv2.data.filter.location;

import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.infai.ddls.medicaltrainingv2.model.core.TreatmentType;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSiteWith;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrainingSiteWithFilter<T extends AbstractTrainingSiteWith> extends TrainingSiteFilter<T> {
    private MedicalField medicalField;
    private TreatmentType treatmentType;

    public <U extends AbstractTrainingSiteWith, V extends TrainingSiteWithFilter<U>> V convert(Class<V> type) throws Exception {
	V newFilter = type.getDeclaredConstructor().newInstance();
	newFilter.setAllowArchived(isAllowArchived());
	newFilter.setDistance(getDistance());
	newFilter.setLatitude(getLatitude());
	newFilter.setLongitude(getLongitude());
	newFilter.setMedicalField(getMedicalField());
	newFilter.setTreatmentType(getTreatmentType());
	newFilter.setZipCode(getZipCode());

	return newFilter;
    }
}
