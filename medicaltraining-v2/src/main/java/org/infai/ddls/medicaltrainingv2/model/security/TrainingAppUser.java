package org.infai.ddls.medicaltrainingv2.model.security;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class TrainingAppUser extends AbstractTrainingAppEntity {
    @NotBlank
    @Column(nullable = false)
    private String username;
    @NotEmpty
    @Column(nullable = false)
    private String password;
    private boolean active;
    @NotNull
    @ManyToMany
    private Set<TrainingAppRole> roles = new HashSet<>();

}