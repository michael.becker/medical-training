package org.infai.ddls.medicaltrainingv2.data.repo.core;

import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.model.core.Title;
import org.springframework.stereotype.Repository;

@Repository
public interface TitleRepository extends TrainingAppEntityRepository<Title> {

}
