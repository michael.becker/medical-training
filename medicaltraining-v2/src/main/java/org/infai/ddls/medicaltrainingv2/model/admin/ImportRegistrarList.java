package org.infai.ddls.medicaltrainingv2.model.admin;

import java.util.ArrayList;
import java.util.List;

import org.infai.ddls.medicaltrainingv2.model.person.Registrar;

import de.caterdev.utils.parser.ParserEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImportRegistrarList implements ImportList<Registrar> {
    private List<ImportRegistrarListEntity> entities = new ArrayList<>();

    public void setEntities(List<ParserEntity<Registrar>> entities) {
	for (ParserEntity<Registrar> entity : entities) {
	    this.entities.add(new ImportRegistrarListEntity(entity));
	}
    }

}
