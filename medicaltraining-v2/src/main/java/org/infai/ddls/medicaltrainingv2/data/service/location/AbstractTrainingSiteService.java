package org.infai.ddls.medicaltrainingv2.data.service.location;

import org.infai.ddls.medicaltrainingv2.data.filter.location.TrainingSiteFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;
import org.springframework.stereotype.Service;

@Service
public abstract class AbstractTrainingSiteService<T extends AbstractTrainingSite, U extends TrainingSiteFilter<T>> extends AbstractTrainingAppService<T, U> {
//    @Autowired
//    private TrainingSiteDAO dao;
//    @Autowired
//    private MedicalFieldService medicalFieldService;
//
//    @Override
//    public List<AbstractTrainingSite> get() {
//	return get(new TrainingSiteFilter(), Pageable.unpaged(), false, false);
//    }
//
//    @Override
//    public List<AbstractTrainingSite> get(TrainingSiteFilter filter, Pageable pageable) {
//	return get(filter, pageable, false, false);
//    }
//
//    @Transactional
//    public List<AbstractTrainingSite> get(TrainingSiteFilter filter, Pageable pageable, boolean loadChildSites, boolean loadEquipments) {
//	if (null != filter.getMedicalField()) {
//	    // notwendig für transitive Kindbeziehungen
//	    filter.setMedicalField(medicalFieldService.get(filter.getMedicalField().getId(), true, true));
//	}
//
//	List<AbstractTrainingSite> trainingSites = dao.get(new TrainingSiteSpecification(filter), pageable);
//
//	if (loadChildSites) {
//	    for (AbstractTrainingSite trainingSite : trainingSites) {
//		trainingSite.getChildren().size();
//
//		if (loadEquipments) {
//		    for (AbstractTrainingSite childSite : trainingSite.getChildren()) {
//			childSite.getEquipments().size();
//		    }
//		}
//	    }
//	}
//
//	if (loadEquipments) {
//	    for (AbstractTrainingSite trainingSite : trainingSites) {
//		trainingSite.getEquipments().size();
//	    }
//	}
//
//	return trainingSites;
//    }
//
//    @Override
//    public TrainingSite get(UUID id) {
//	return get(id, false, false);
//    }
//
//    @Transactional
//    public TrainingSite get(UUID id, boolean loadChildSites, boolean loadEquipments) {
//	TrainingSite trainingSite = dao.get(id);
//
//	if (loadChildSites) {
//	    trainingSite.getChildren().size();
//
//	    if (loadEquipments) {
//		for (TrainingSite child : trainingSite.getChildren()) {
//		    child.getEquipments().size();
//		}
//	    }
//	}
//
//	if (loadEquipments) {
//	    trainingSite.getEquipments().size();
//	}
//
//	return trainingSite;
//    }
//
//    @Override
//    @Transactional
//    public AbstractTrainingSite save(AbstractTrainingSite entity) {
//	return dao.save(entity);
//    }
}
