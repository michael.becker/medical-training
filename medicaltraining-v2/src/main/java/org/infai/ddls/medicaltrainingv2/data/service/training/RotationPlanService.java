package org.infai.ddls.medicaltrainingv2.data.service.training;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.infai.ddls.medicaltrainingv2.data.dao.training.RotationPlanDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.training.RotationPlanFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.spec.training.RotationPlanSpecification;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class RotationPlanService extends AbstractTrainingAppService<RotationPlan, RotationPlanFilter> {
    @Autowired
    private RotationPlanDAO dao;

    @Override
    @Transactional
    public List<RotationPlan> get() {
	return get(new RotationPlanFilter(), null, false, false);
    }

    @Override
    @Transactional
    public List<RotationPlan> get(RotationPlanFilter filter, Pageable pageable) {
	return get(filter, pageable, false, false);
    }

    @Transactional
    public List<RotationPlan> get(RotationPlanFilter filter, Pageable pageable, boolean loadRotationPlanSegments, boolean loadTrainingSchemeSegments) {
	List<RotationPlan> rotationPlans = dao.get(new RotationPlanSpecification(filter), pageable);

	for (RotationPlan rotationPlan : rotationPlans) {
	    eagerInitialisation(rotationPlan, loadRotationPlanSegments, loadTrainingSchemeSegments);
	}

	return rotationPlans;
    }

    @Override
    @Transactional
    public RotationPlan get(UUID id) {
	return get(id, false, false);
    }

    @Transactional
    public RotationPlan get(UUID id, boolean loadPreferredRegions) {
	RotationPlan rotationPlan = dao.get(id);

	if (loadPreferredRegions) {
	    rotationPlan.getRegistrar().getPreferredRegions().size();
	}

	return rotationPlan;
    }

    @Transactional
    public RotationPlan get(UUID id, boolean loadRotationPlanSegments, boolean loadTrainingSchemeSegments) {
	RotationPlan rotationPlan = dao.get(id);
	eagerInitialisation(rotationPlan, loadRotationPlanSegments, loadTrainingSchemeSegments);

	return rotationPlan;
    }

    @Override
    @Transactional
    public RotationPlan save(RotationPlan entity) {
	return dao.save(entity);
    }

    @Transactional
    private void eagerInitialisation(RotationPlan rotationPlan, boolean loadRotationPlanSegments, boolean loadTrainingSchemeSegments) {
	if (loadRotationPlanSegments) {
	    rotationPlan.getRotationPlanSegments().size();
	}
	if (loadTrainingSchemeSegments) {
	    rotationPlan.getTrainingScheme().getTrainingSchemeSegments().size();
	}
    }
}
