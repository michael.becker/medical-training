package org.infai.ddls.medicaltrainingv2.data.filter.person;

import org.infai.ddls.medicaltrainingv2.data.filter.TrainingAppEntityFilter;
import org.infai.ddls.medicaltrainingv2.model.core.Sex;
import org.infai.ddls.medicaltrainingv2.model.core.Title;
import org.infai.ddls.medicaltrainingv2.model.location.Address;
import org.infai.ddls.medicaltrainingv2.model.person.ContactInfo;
import org.infai.ddls.medicaltrainingv2.model.person.Person;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode(callSuper = true)
public abstract @Getter @Setter class PersonFilter<T extends Person<?>> extends TrainingAppEntityFilter<T> {
    private String firstName;
    private String lastName;
    private String fullName;
    private String addressText;
    private Sex sex;
    private Title title;
    private Address address = new Address();
    private ContactInfo contactInfo = new ContactInfo();
}