package org.infai.ddls.medicaltrainingv2.data.filter.training;

import java.time.LocalDate;

import org.infai.ddls.medicaltrainingv2.data.filter.TrainingAppEntityFilter;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.infai.ddls.medicaltrainingv2.model.person.Registrar;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlan;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public @Data class RotationPlanSegmentFilter extends TrainingAppEntityFilter<RotationPlanSegment> {
    private AuthorisationField authorisationField;
    private TrainingPositionField trainingPositionField;
    private RotationPlan rotationPlan;
    private LocalDate referenceDate;
    private Registrar registrar;
    private LocalDate endBefore;
    private LocalDate endAfter;
}
