package org.infai.ddls.medicaltrainingv2.data.service.location;

import java.util.List;
import java.util.UUID;

import org.infai.ddls.medicaltrainingv2.data.dao.location.DepartmentDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.location.DepartmentFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.spec.location.DepartmentSpecification;
import org.infai.ddls.medicaltrainingv2.model.location.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class DepartmentService extends AbstractTrainingAppService<Department, DepartmentFilter> {
    @Autowired
    private DepartmentDAO dao;

    @Override
    public List<Department> get() {
	return get(new DepartmentFilter(), DEFAULT_PAGING);
    }

    @Override
    public List<Department> get(DepartmentFilter filter, Pageable pageable) {
	return dao.get(new DepartmentSpecification(filter), pageable);
    }

    @Override
    public Department get(UUID id) {
	return dao.get(id);
    }

    @Override
    public Department save(Department entity) {
	return dao.save(entity);
    }

}
