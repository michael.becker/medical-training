package org.infai.ddls.medicaltrainingv2.data.dao.person;

import org.infai.ddls.medicaltrainingv2.data.filter.person.AuthorisedDoctorFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.person.AuthorisedDoctorRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.person.AuthorisedDoctorSpecification;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.springframework.stereotype.Component;

@Component
public class AuthorisedDoctorDAO extends PersonDAO<AuthorisedDoctor, AuthorisedDoctorRepository, AuthorisedDoctorFilter, AuthorisedDoctorSpecification> {
    public boolean isLANRUsedByOther(Integer lanr, AuthorisedDoctor authorisedDoctor) {
	if (null == authorisedDoctor.getId()) {
	    return repository.findByLanr(authorisedDoctor.getLanr()).isPresent();
	} else {
	    return repository.findByLanrAndIdNot(authorisedDoctor.getLanr(), authorisedDoctor.getId()).isPresent();
	}
    }
}
