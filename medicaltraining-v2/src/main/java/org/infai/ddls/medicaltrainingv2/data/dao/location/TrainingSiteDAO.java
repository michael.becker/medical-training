package org.infai.ddls.medicaltrainingv2.data.dao.location;

import org.infai.ddls.medicaltrainingv2.data.filter.location.TrainingSiteFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.location.TrainingSiteRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.location.TrainingSiteSpecification;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;

public abstract class TrainingSiteDAO<T extends AbstractTrainingSite, U extends TrainingSiteRepository<T>, V extends TrainingSiteFilter<T>, W extends TrainingSiteSpecification<T, V>> extends LocationDAO<T, U, V, W> {

}
