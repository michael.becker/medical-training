package org.infai.ddls.medicaltrainingv2.data.spec.training;

import java.time.LocalDate;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltrainingv2.data.filter.training.RotationPlanSegmentFilter;
import org.infai.ddls.medicaltrainingv2.data.spec.AbstractTrainingAppEntitySpecification;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.infai.ddls.medicaltrainingv2.model.person.Registrar;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlan;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment_;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlan_;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPosition_;

@SuppressWarnings("serial")
public class RotationPlanSegmentSpecification extends AbstractTrainingAppEntitySpecification<RotationPlanSegment, RotationPlanSegmentFilter> {
    public RotationPlanSegmentSpecification(RotationPlanSegmentFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<RotationPlanSegment> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (null != filter.getAuthorisationField()) {
	    addPredicate(isAtAuthorisation(filter.getAuthorisationField(), root, query, criteriaBuilder));
	}
	if (null != filter.getTrainingPositionField()) {
	    addPredicate(isAtTrainingPosition(filter.getTrainingPositionField(), root, query, criteriaBuilder));
	}
	if (null != filter.getRotationPlan()) {
	    addPredicate(isFromRotationPlan(filter.getRotationPlan(), root, query, criteriaBuilder));
	}
	if (null != filter.getReferenceDate()) {
	    Predicate referenceDateAfterBegin = criteriaBuilder.lessThanOrEqualTo(root.get(RotationPlanSegment_.segmentBegin), filter.getReferenceDate());
	    Predicate referenceDateBeforEnd = criteriaBuilder.greaterThanOrEqualTo(root.get(RotationPlanSegment_.segmentEnd), filter.getReferenceDate());

	    addPredicateConjunction(criteriaBuilder, referenceDateAfterBegin, referenceDateBeforEnd);
	}
	if (null != filter.getRegistrar()) {
	    addPredicate(isFromRegistrar(filter.getRegistrar(), root, query, criteriaBuilder));
	}

	if (null != filter.getEndBefore()) {
	    addPredicate(isEndBefore(filter.getEndBefore(), root, query, criteriaBuilder));
	}
	if (null != filter.getEndAfter()) {
	    addPredicate(isEndAfter(filter.getEndAfter(), root, query, criteriaBuilder));
	}
    }

    private Predicate isAtAuthorisation(AuthorisationField authorisationField, Root<RotationPlanSegment> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get(RotationPlanSegment_.trainingPosition).get(TrainingPosition_.authorisation), authorisationField);
    }

    private Predicate isAtTrainingPosition(TrainingPositionField trainingPosition, Root<RotationPlanSegment> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get(RotationPlanSegment_.trainingPosition), trainingPosition);
    }

    private Predicate isEndAfter(LocalDate endAfter, Root<RotationPlanSegment> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.greaterThanOrEqualTo(root.get(RotationPlanSegment_.SEGMENT_END), endAfter);
    }

    private Predicate isEndBefore(LocalDate endBefore, Root<RotationPlanSegment> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.lessThanOrEqualTo(root.get(RotationPlanSegment_.SEGMENT_END), endBefore);
    }

    private Predicate isFromRegistrar(Registrar registrar, Root<RotationPlanSegment> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get(RotationPlanSegment_.rotationPlan).get(RotationPlan_.registrar), registrar);
    }

    private Predicate isFromRotationPlan(RotationPlan rotationPlan, Root<RotationPlanSegment> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get(RotationPlanSegment_.rotationPlan), rotationPlan);
    }

}
