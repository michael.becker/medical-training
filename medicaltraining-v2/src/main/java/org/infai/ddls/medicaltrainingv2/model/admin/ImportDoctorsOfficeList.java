package org.infai.ddls.medicaltrainingv2.model.admin;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.infai.ddls.medicaltrainingv2.model.location.DoctorsOffice;

import de.caterdev.utils.parser.ParserEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImportDoctorsOfficeList implements ImportList<DoctorsOffice> {
    @Valid
    private List<ImportDoctorsOfficeListEntity> entities = new ArrayList<>();

    public void setEntities(List<ParserEntity<DoctorsOffice>> entities) {
	for (ParserEntity<DoctorsOffice> entity : entities) {
	    this.entities.add(new ImportDoctorsOfficeListEntity(entity));
	}
    }
}
