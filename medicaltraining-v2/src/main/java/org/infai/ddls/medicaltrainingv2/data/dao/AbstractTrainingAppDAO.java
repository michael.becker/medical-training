package org.infai.ddls.medicaltrainingv2.data.dao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.infai.ddls.medicaltrainingv2.data.filter.TrainingAppEntityFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.AbstractTrainingAppEntitySpecification;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;

import javax.transaction.Transactional;
import javax.validation.Validator;
import java.util.List;
import java.util.UUID;

public abstract class AbstractTrainingAppDAO<T extends AbstractTrainingAppEntity, U extends TrainingAppEntityRepository<T>, V extends TrainingAppEntityFilter<T>, W extends AbstractTrainingAppEntitySpecification<T, V>> implements TrainingAppDAO {
    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    protected U repository;

    @Autowired
    protected Validator validator;

    @Transactional
    public T archive(T entity) {
        entity.setArchived(true);

        return repository.save(entity);
    }

    @Transactional
    public void delete(T entity) {
        repository.delete(entity);
    }

    public boolean exists(UUID id) {
        return repository.existsById(id);
    }

    @Transactional
    public List<T> get() {
        return get(null, getPageable());
    }

    @Transactional
    public T get(UUID id) {
        return repository.findById(id).orElse(null);
    }

    @Transactional
    public List<T> get(W spec, Pageable pageable) {
        return repository.findAll(spec, evaluateSort(pageable));
    }

    @Transactional
    public T save(T entity) {
        return repository.save(entity);
    }

    @Transactional
    public Iterable<T> save(Iterable<T> entities) {
        return repository.saveAll(entities);
    }
}