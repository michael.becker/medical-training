package org.infai.ddls.medicaltrainingv2.model.location;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.core.Equipment;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.infai.ddls.medicaltrainingv2.model.core.TreatmentType;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Audited
@Getter
@Setter
@ToString(callSuper = true)
public abstract class AbstractTrainingSiteWith extends AbstractTrainingSite {
    @NotNull
    @ManyToOne(optional = false)
    private MedicalField medicalField;
    @ManyToMany(cascade = CascadeType.ALL)
    @ToString.Exclude
    private List<Equipment> equipments = new ArrayList<>();
    @NotNull
    @Column(nullable = false)
    private TreatmentType treatmentType;
    @ManyToMany
//    @OrderBy(AuthorisedDoctor_.LAST_NAME + " ASC")
    @ToString.Exclude
    private List<AuthorisedDoctor> authorisedDoctors = new ArrayList<>();
}
