package org.infai.ddls.medicaltrainingv2.model;

import javax.persistence.Transient;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CycleFreeConstraint
public interface HasChildren<T extends AbstractTrainingAppEntity> {
    /**
     * Retuns all children (direct and indirect via transitive associations) of this
     * element.
     *
     * @return all direct and indirect children of this element
     */
    @Transient
    public default <U extends HasChildren<T>> Set<U> getAllChildren() {
        Set<U> children = new HashSet<>();

        if (null == getChildren()) {
            return children;
        } else {
            addChildren((U) this, children);

            return children;
        }
    }

    /**
     * Returns all direct children of this object,
     *
     * @return all direct children of this object
     */
    public <U extends HasChildren<T>> List<U> getChildren();

    /**
     * Sets the direct children of this object.
     *
     * @param children the children of this object
     */
    public void setChildren(List<T> children);

    /**
     * Returns the parent of this object.
     *
     * @return the parent of this object
     */
    public <U extends HasChildren<T>> U getParent();

    /**
     * Sets the parent of this object.
     *
     * @param parent the parent of this object
     */
    public void setParent(T parent);

    @Transient
    private <U extends HasChildren<T>> void addChildren(U parent, Set<U> children) {
        for (U child : (List<U>) parent.getChildren()) {
            if (children.contains(child)) {
                continue;
            } else {
                children.add(child);
                addChildren(child, children);
            }
        }
    }
}
