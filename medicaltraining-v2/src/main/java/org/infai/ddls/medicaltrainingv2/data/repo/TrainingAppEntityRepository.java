package org.infai.ddls.medicaltrainingv2.data.repo;

import java.util.UUID;

import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

@NoRepositoryBean
public interface TrainingAppEntityRepository<T extends AbstractTrainingAppEntity> extends PagingAndSortingRepository<T, UUID>, JpaSpecificationExecutor<T> {

}
