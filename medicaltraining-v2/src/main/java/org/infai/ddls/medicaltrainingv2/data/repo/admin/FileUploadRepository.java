package org.infai.ddls.medicaltrainingv2.data.repo.admin;

import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.model.admin.FileUpload;
import org.springframework.stereotype.Repository;

@Repository
public interface FileUploadRepository extends TrainingAppEntityRepository<FileUpload> {

}
