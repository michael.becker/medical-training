package org.infai.ddls.medicaltrainingv2.data.filter.location;

import org.infai.ddls.medicaltrainingv2.data.filter.TrainingAppEntityFilter;
import org.infai.ddls.medicaltrainingv2.model.location.Location;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class LocationFilter<T extends Location> extends TrainingAppEntityFilter<T> {
    public static final String DEFAULT_ZIP_CODE = "99999";

    private String zipCode = DEFAULT_ZIP_CODE;
    private int distance;
    private double latitude;
    private double longitude;
}
