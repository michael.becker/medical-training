package org.infai.ddls.medicaltrainingv2.model.training.constraint;

import java.time.LocalDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment;

public class RotationPlanSegmentBeginEndValidator implements ConstraintValidator<RotationPlanSegmentBeginBeforeEndConstraint, RotationPlanSegment> {
    @Override
    public boolean isValid(RotationPlanSegment rotationPlanSegment, ConstraintValidatorContext context) {
	LocalDate begin = rotationPlanSegment.getSegmentBegin();
	LocalDate end = rotationPlanSegment.getSegmentEnd();

	if (null == begin || null == end) {
	    return true;
	}

	if (end.isBefore(begin)) {
	    return false;
	} else {
	    return true;
	}
    }
}