package org.infai.ddls.medicaltrainingv2.controller.core;

import org.infai.ddls.medicaltrainingv2.controller.AbstractTypedTrainingAppController;
import org.infai.ddls.medicaltrainingv2.data.filter.core.ConversationTypeFilter;
import org.infai.ddls.medicaltrainingv2.data.service.core.ConversationTypeService;
import org.infai.ddls.medicaltrainingv2.model.person.ConversationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/coredata/conversationtypes")
public class ConversationTypeController extends AbstractTypedTrainingAppController<ConversationType, ConversationTypeFilter, ConversationTypeService> {
    @Override
    protected ConversationType getEntity() {
	return new ConversationType();
    }

    @Override
    protected ConversationTypeFilter getFilter() {
	return new ConversationTypeFilter();
    }

    @Override
    protected String mappingDefault() {
	return "/coredata/conversationtypes";
    }

    @Override
    protected String templateFolder() {
	return "/coredata";
    }

}
