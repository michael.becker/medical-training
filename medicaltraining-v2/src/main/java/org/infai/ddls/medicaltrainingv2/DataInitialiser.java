package org.infai.ddls.medicaltrainingv2;

import java.time.LocalDate;

import org.infai.ddls.medicaltrainingv2.data.repo.security.TrainingAppRoleRepository;
import org.infai.ddls.medicaltrainingv2.data.repo.security.TrainingAppUserRepository;
import org.infai.ddls.medicaltrainingv2.data.service.authorisation.AuthorisationFieldService;
import org.infai.ddls.medicaltrainingv2.data.service.core.ConversationTypeService;
import org.infai.ddls.medicaltrainingv2.data.service.core.MedicalFieldService;
import org.infai.ddls.medicaltrainingv2.data.service.core.SexService;
import org.infai.ddls.medicaltrainingv2.data.service.core.TitleService;
import org.infai.ddls.medicaltrainingv2.data.service.location.TrainingSiteService;
import org.infai.ddls.medicaltrainingv2.data.service.person.AuthorisedDoctorService;
import org.infai.ddls.medicaltrainingv2.data.service.person.RegistrarService;
import org.infai.ddls.medicaltrainingv2.data.service.training.RotationPlanService;
import org.infai.ddls.medicaltrainingv2.data.service.training.TrainingPositionFieldService;
import org.infai.ddls.medicaltrainingv2.data.service.training.TrainingSchemeSegmentService;
import org.infai.ddls.medicaltrainingv2.data.service.training.TrainingSchemeService;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.infai.ddls.medicaltrainingv2.model.core.Sex;
import org.infai.ddls.medicaltrainingv2.model.core.Title;
import org.infai.ddls.medicaltrainingv2.model.core.TreatmentType;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSiteWith;
import org.infai.ddls.medicaltrainingv2.model.location.Address;
import org.infai.ddls.medicaltrainingv2.model.location.Clinic;
import org.infai.ddls.medicaltrainingv2.model.location.Department;
import org.infai.ddls.medicaltrainingv2.model.location.DoctorsOffice;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.model.person.ConversationType;
import org.infai.ddls.medicaltrainingv2.model.person.Registrar;
import org.infai.ddls.medicaltrainingv2.model.security.TrainingAppRole;
import org.infai.ddls.medicaltrainingv2.model.security.TrainingAppUser;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlan;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingScheme;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingSchemeSegment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import lombok.extern.apachecommons.CommonsLog;

@Component
@CommonsLog
public class DataInitialiser {
    @Autowired
    private TrainingAppUserRepository userRepository;
    @Autowired
    private TrainingAppRoleRepository roleRepository;
    @Autowired
    private MedicalFieldService medicalFieldService;
    @Autowired
    private SexService sexService;
    @Autowired
    private TitleService titleService;
    @Autowired
    private AuthorisedDoctorService authorisedDoctorService;
    @Autowired
    private TrainingSiteService trainingSiteService;
    @Autowired
    private TrainingSchemeService trainingSchemeService;
    @Autowired
    private RegistrarService registrarService;
    @Autowired
    private TrainingSchemeSegmentService trainingSchemeSegmentService;
    @Autowired
    private AuthorisationFieldService authorisationFieldService;
    @Autowired
    private TrainingPositionFieldService trainingPositionFieldService;
    @Autowired
    private ConversationTypeService conversationTypeService;
    @Autowired
    private RotationPlanService rotationPlanService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public void initialiseRolesAndUsers() {
	log.info("clearing roles and users");
	userRepository.deleteAll();
	roleRepository.deleteAll();

	log.info("initialising roles...");
	TrainingAppRole admin = new TrainingAppRole();
	admin.setName("ROLE_ADMIN");
	TrainingAppRole manager = new TrainingAppRole();
	manager.setName("ROLE_MANAGER");
	TrainingAppRole kvsa = new TrainingAppRole();
	kvsa.setName("ROLE_KVSA");
	TrainingAppRole user = new TrainingAppRole();
	user.setName("ROLE_USER");

	admin = roleRepository.save(admin);
	manager = roleRepository.save(manager);
	kvsa = roleRepository.save(kvsa);
	user = roleRepository.save(user);

	log.info("initialising users...");
	TrainingAppUser adminUser = new TrainingAppUser();
	adminUser.setUsername("admin");
	adminUser.setPassword(passwordEncoder.encode("admin"));
	adminUser.setActive(true);
	adminUser.getRoles().add(admin);
	adminUser.getRoles().add(manager);
	adminUser.getRoles().add(kvsa);
	adminUser.getRoles().add(user);

	TrainingAppUser managerUser = new TrainingAppUser();
	managerUser.setUsername("manager");
	managerUser.setPassword(passwordEncoder.encode("manager"));
	managerUser.setActive(true);
	managerUser.getRoles().add(manager);

	TrainingAppUser kvsaUser = new TrainingAppUser();
	kvsaUser.setUsername("kvsa");
	kvsaUser.setPassword(passwordEncoder.encode("kvsa"));
	kvsaUser.setActive(true);
	kvsaUser.getRoles().add(kvsa);

	TrainingAppUser userUser = new TrainingAppUser();
	userUser.setUsername("user");
	userUser.setPassword(passwordEncoder.encode("user"));
	userUser.setActive(true);
	userUser.getRoles().add(user);

	userRepository.save(adminUser);
	userRepository.save(managerUser);
	userRepository.save(kvsaUser);
	userRepository.save(userUser);

	MedicalField field = new MedicalField();
	field.setName("Unmittelbare Patientenversorgung");
	MedicalField unmittelbare = medicalFieldService.save(field);

	field = new MedicalField();
	field.setName("Chirurgie");
	field.setParent(unmittelbare);
	MedicalField chirurgie = medicalFieldService.save(field);

	field = new MedicalField();
	field.setName("Innere (Klinik)");
	field.setParent(unmittelbare);
	MedicalField innere = medicalFieldService.save(field);

	field = new MedicalField();
	field.setName("Hausärztliche Versorgung");
	field.setParent(unmittelbare);
	MedicalField hausarzt = medicalFieldService.save(field);

	field = new MedicalField();
	field.setName("Allgemeinmedizin");
	field.setParent(hausarzt);
	MedicalField allgemeinmedizin = medicalFieldService.save(field);

	Sex m = new Sex();
	m.setName("Herr");
	m.setDefaultPrefix("Herr");
	m.setDefaultSalutation("Sehr geehrter Herr");
	Sex f = new Sex();
	f.setName("Frau");
	f.setDefaultPrefix("Frau");
	f.setDefaultSalutation("Sehr geehrte Frau");
	sexService.save(m);
	sexService.save(f);

	Title dr = new Title();
	dr.setName("Dr.");
	titleService.save(dr);
	Title profDr = new Title();
	profDr.setName("Prof. Dr.");
	titleService.save(profDr);

	Address address = new Address();
	address.setStreet("Zschochersche Str. 94");
	address.setZipCode("04229");
	address.setCity("Leipzig");

	AuthorisedDoctor doctor = new AuthorisedDoctor();
	doctor.setFirstName("Vorname Weiterbilder1");
	doctor.setLastName("Nachname Weiterbilder1");
	doctor.setAddress(address);
	doctor = authorisedDoctorService.save(doctor);
	AuthorisedDoctor doctor2 = new AuthorisedDoctor();
	doctor2.setFirstName("Vorname Weiterbilder2");
	doctor2.setLastName("Nachname Weiterbilder2");
	doctor2.setAddress(address);
	doctor2 = authorisedDoctorService.save(doctor2);

	DoctorsOffice site = new DoctorsOffice();
	site.setName("Testpraxis");
	site.setMedicalField(allgemeinmedizin);
	site.setTreatmentType(TreatmentType.Outpatient);
	site.getAddress().setStreet("Zschochersche Str. 94");
	site.getAddress().setZipCode("04229");
	site.getAddress().setCity("Leipzig");
	site.getAuthorisedDoctors().add(doctor);
	site = trainingSiteService.save(site);

	Clinic clinic = new Clinic();
	clinic.setName("Waldklinik");
	clinic.setAddress(address);
	clinic = trainingSiteService.save(clinic);

	Department department = new Department();
	department.setName("Abteilung X");
	department.setAddress(address);
	department.setClinic(clinic);
	department.setMedicalField(chirurgie);
	department.setTreatmentType(TreatmentType.Inpatient);
	department = trainingSiteService.save(department);

	TrainingScheme trainingScheme = new TrainingScheme();
	trainingScheme.setName("Weiterbildung Allgemeinmedizin LSA");
	trainingScheme = trainingSchemeService.save(trainingScheme);

	trainingSchemeSegmentService.save(createTrainingSchemeSegment(18, innere, trainingScheme, TreatmentType.Inpatient));
	trainingSchemeSegmentService.save(createTrainingSchemeSegment(6, chirurgie, trainingScheme, TreatmentType.Inpatient, TreatmentType.Outpatient));
	trainingSchemeSegmentService.save(createTrainingSchemeSegment(12, unmittelbare, trainingScheme, TreatmentType.Inpatient, TreatmentType.Outpatient));
	trainingSchemeSegmentService.save(createTrainingSchemeSegment(18, allgemeinmedizin, trainingScheme, TreatmentType.Outpatient));
	trainingSchemeSegmentService.save(createTrainingSchemeSegment(6, hausarzt, trainingScheme, TreatmentType.Outpatient));

	Registrar registrar = new Registrar();
	registrar.setFirstName("Michael");
	registrar.setLastName("Becker");
	registrar.setAddress(address);
	registrarService.save(registrar);

	AuthorisationField authorisation = authorisationFieldService.save(createAuthorisationField(doctor, 6, trainingScheme, site, LocalDate.parse("2018-01-01"), null));
	TrainingPositionField trainingPosition = trainingPositionFieldService.save(createTrainingPosition(authorisation, LocalDate.parse("2018-01-01"), null, 2, true));

	rotationPlanService.save(createRotationPlan(registrar, trainingScheme));

	conversationTypeService.save(createConversationType("Telefon", "Kontakt per Telefonanruf"));
	conversationTypeService.save(createConversationType("E-Mail", "Kontakt über E-Mail"));
    }

    private AuthorisationField createAuthorisationField(AuthorisedDoctor authorisedDoctor, int maximalDuration, TrainingScheme trainingScheme, AbstractTrainingSiteWith trainingSite, LocalDate validFrom, LocalDate validUntil) {
	AuthorisationField authorisation = new AuthorisationField();
	authorisation.setAuthorisedDoctor(authorisedDoctor);
	authorisation.setMaximalDuration(maximalDuration);
	authorisation.setTrainingScheme(trainingScheme);
	authorisation.setTrainingSite(trainingSite);
	authorisation.setValidFrom(validFrom);
	authorisation.setValidUntil(validUntil);

	return authorisation;
    }

    private ConversationType createConversationType(String name, String description) {
	ConversationType conversationType = new ConversationType();
	conversationType.setName(name);
	conversationType.setDescription(description);

	return conversationType;
    }

    private RotationPlan createRotationPlan(Registrar registrar, TrainingScheme trainingScheme) {
	RotationPlan rotationPlan = new RotationPlan();
	rotationPlan.setRegistrar(registrar);
	rotationPlan.setTrainingScheme(trainingScheme);

	return rotationPlan;
    }

    private TrainingPositionField createTrainingPosition(AuthorisationField authorisation, LocalDate availableFrom, LocalDate availableUntil, int capacity, boolean partTimeAvailable) {
	TrainingPositionField trainingPosition = new TrainingPositionField();
	trainingPosition.setAuthorisation(authorisation);
	trainingPosition.setAvailableFrom(availableFrom);
	trainingPosition.setAvailableUntil(availableUntil);
	trainingPosition.setCapacity(capacity);
	trainingPosition.setPartTimeAvailable(partTimeAvailable);

	return trainingPosition;
    }

    private TrainingSchemeSegment createTrainingSchemeSegment(int duration, MedicalField medicalField, TrainingScheme trainingScheme, TreatmentType... permittedTreatmentTypes) {
	TrainingSchemeSegment segment = new TrainingSchemeSegment();
	segment.setDuration(duration);
	segment.setMedicalField(medicalField);
	segment.setTrainingScheme(trainingScheme);

	for (TreatmentType treatmentType : permittedTreatmentTypes) {
	    segment.getPermittedTreatmentTypes().add(treatmentType);
	}

	return segment;
    }
}
