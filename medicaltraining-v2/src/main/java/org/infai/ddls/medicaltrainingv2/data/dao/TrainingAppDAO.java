package org.infai.ddls.medicaltrainingv2.data.dao;

import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity_;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

public interface TrainingAppDAO {
    default Sort evaluateSort(Pageable pageable) {
        if (null == pageable) {
            return getSort();
        } else {
            return evaluateSort(pageable.getSort());
        }
    }

    default Sort evaluateSort(Sort sort) {
        if (null == sort || sort.isUnsorted()) {
            sort = getSort();
        }

        return sort;
    }

    default Pageable getPageable() {
        return PageRequest.of(1, 10, getSort());
    }

    default Sort getSort() {
        return Sort.by(Direction.ASC, AbstractTrainingAppEntity_.archived.getName());
    }
}
