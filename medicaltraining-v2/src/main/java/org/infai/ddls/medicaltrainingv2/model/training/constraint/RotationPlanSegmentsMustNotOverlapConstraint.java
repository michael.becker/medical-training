package org.infai.ddls.medicaltrainingv2.model.training.constraint;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = RotationPlanSegmentsDoNotOverlapValidator.class)
public @interface RotationPlanSegmentsMustNotOverlapConstraint {
    Class<?>[] groups() default {};

    String message() default "rotationplans.segmentsoverlap";

    Class<? extends Payload>[] payload() default {};
}
