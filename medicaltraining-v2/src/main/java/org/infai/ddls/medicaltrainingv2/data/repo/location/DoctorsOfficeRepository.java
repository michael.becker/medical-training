package org.infai.ddls.medicaltrainingv2.data.repo.location;

import org.infai.ddls.medicaltrainingv2.model.location.DoctorsOffice;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorsOfficeRepository extends TrainingSiteRepository<DoctorsOffice> {

}
