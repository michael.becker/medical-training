package org.infai.ddls.medicaltrainingv2.data.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltrainingv2.data.filter.NamedTrainingAppEntityFilter;
import org.infai.ddls.medicaltrainingv2.model.AbstractNamedTrainingAppEntity;
import org.infai.ddls.medicaltrainingv2.model.AbstractNamedTrainingAppEntity_;

@SuppressWarnings("serial")
public abstract class NamedTrainingAppEntitySpecification<T extends AbstractNamedTrainingAppEntity, U extends NamedTrainingAppEntityFilter<T>> extends AbstractTrainingAppEntitySpecification<T, U> {

    public NamedTrainingAppEntitySpecification(U filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (null != filter.getName()) {
	    addPredicate(likeIgnoreCase(filter.getName(), root.get(AbstractNamedTrainingAppEntity_.name), criteriaBuilder));
	}
    }
}
