package org.infai.ddls.medicaltrainingv2.controller;

import java.util.TreeSet;

import javax.servlet.ServletRequest;

public abstract class AbstractTrainingAppController {

    protected String evaluateSaveAction(String saveAction, String saveURL, String saveAndCloseURL) {
	switch (SaveAction.valueOf(saveAction)) {
	case save:
	    return redirect(saveURL);
	case saveAndClose:
	    return redirect(saveAndCloseURL);
	default:
	    throw new RuntimeException("SaveAction " + saveAction + " not supported!");
	}
    }

    protected String formatHTTPParameters(ServletRequest servletRequest) {
	StringBuilder logMessage = new StringBuilder();
	logMessage.append("\nHTTP Parameters\n");
	TreeSet<String> parameters = new TreeSet<>(servletRequest.getParameterMap().keySet());
	for (String parameter : parameters) {
	    for (String parValue : servletRequest.getParameterMap().get(parameter)) {
		logMessage.append("\t").append(parameter).append(": ").append(parValue).append("\n");
	    }
	}

	return logMessage.toString();
    }

    protected String redirect(String url) {
	return RequestMappings.__REDIRECT + url;
    }
}
