package org.infai.ddls.medicaltrainingv2.model;

import java.util.Map;
import java.util.UUID;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.envers.Audited;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Audited
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public abstract class AbstractTrainingAppEntity implements TrainingAppEntity {
    @Id
    @EqualsAndHashCode.Include
    private UUID id;
    @ColumnDefault("false")
    private boolean archived;
    @Lob
    private String description;
    @Lob
    private String notes;
    @ElementCollection
    @Lob
    private Map<String, String> additionalFields;

    public AbstractTrainingAppEntity() {
	this.id = UUID.randomUUID();
    }
}
