package org.infai.ddls.medicaltrainingv2.data.service.person;

import java.util.List;
import java.util.UUID;

import org.infai.ddls.medicaltrainingv2.data.dao.person.ConversationRegistrarDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.person.ConversationRegistrarFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.spec.person.ConversationRegistrarSpecification;
import org.infai.ddls.medicaltrainingv2.model.person.ConversationRegistrar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ConversationRegistrarService extends AbstractTrainingAppService<ConversationRegistrar, ConversationRegistrarFilter> {
    @Autowired
    private ConversationRegistrarDAO dao;

    @Override
    public List<ConversationRegistrar> get() {
	return get(new ConversationRegistrarFilter(), null);
    }

    @Override
    public List<ConversationRegistrar> get(ConversationRegistrarFilter filter, Pageable pageable) {
	return dao.get(new ConversationRegistrarSpecification(filter), pageable);
    }

    @Override
    public ConversationRegistrar get(UUID id) {
	return dao.get(id);
    }

    @Override
    public ConversationRegistrar save(ConversationRegistrar entity) {
	return dao.save(entity);
    }

}
