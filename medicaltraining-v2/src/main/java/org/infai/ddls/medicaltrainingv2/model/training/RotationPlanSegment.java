package org.infai.ddls.medicaltrainingv2.model.training;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.infai.ddls.medicaltrainingv2.model.ChildRelationUtil;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.infai.ddls.medicaltrainingv2.model.core.TreatmentType;
import org.infai.ddls.medicaltrainingv2.model.training.constraint.RotationPlanSegmentBeginBeforeEndConstraint;
import org.infai.ddls.medicaltrainingv2.model.training.constraint.RotationPlanSegmentMinimalDurationConstraint;
import org.infai.ddls.medicaltrainingv2.model.training.constraint.RotationPlanSegmentsMustNotOverlapConstraint;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Audited
@RotationPlanSegmentBeginBeforeEndConstraint
@RotationPlanSegmentMinimalDurationConstraint
@RotationPlanSegmentsMustNotOverlapConstraint
@Getter
@Setter
@ToString(callSuper = true)
public class RotationPlanSegment extends AbstractTrainingAppEntity {
    public static RotationPlanSegment create(RotationPlan rotationPlan, TrainingPositionField trainingPosition, LocalDate begin, LocalDate end, int fullTimeEquivalent, boolean funded, boolean approved) {
	RotationPlanSegment segment = new RotationPlanSegment();
	segment.setRotationPlan(rotationPlan);
	segment.setTrainingPosition(trainingPosition);
	segment.setSegmentBegin(begin);
	segment.setSegmentEnd(end);
	segment.setFullTimeEquivalent(fullTimeEquivalent);
	segment.setFunded(funded);
	segment.setApproved(approved);

	return segment;
    }

    @ManyToOne(optional = false)
    private TrainingPositionField trainingPosition;

    @NotNull
    @Column(nullable = false)
    private LocalDate segmentBegin;
    @NotNull
    @Column(nullable = false)
    private LocalDate segmentEnd;
    @NotNull
    @Min(1)
    @Max(100)
    @Column(nullable = false)
    private int fullTimeEquivalent;
    @NotNull
    @Column(nullable = false)
    private boolean funded;
    @NotNull
    @Column(nullable = false)
    private boolean approved;
    @ToString.Exclude
    @ManyToOne(optional = false)
    private RotationPlan rotationPlan;
    @ToString.Exclude
    @ManyToOne(optional = true)
    private TrainingSchemeSegment assignedSegment;

    @Transient
    public int getDuration() {
	if (null == segmentBegin || null == segmentEnd) {
	    return 0;
	} else {
	    Period period = Period.between(segmentBegin, segmentEnd.plusDays(1));

	    return period.getYears() * 12 + period.getMonths();
	}
    }

    /**
     * Gibt diejenigen Abschnitte einer Weiterbildungsordnung zurück für die dieser
     * Abschnitt förderbar ist.
     *
     * @param trainingScheme
     * @return
     */
    @Transient
    public List<TrainingSchemeSegment> getFundableTrainingSchemeSegments(TrainingScheme trainingScheme) {
	List<TrainingSchemeSegment> trainingSchemeSegments = new ArrayList<>();

	for (TrainingSchemeSegment trainingSchemeSegment : trainingScheme.getTrainingSchemeSegments()) {
	    TreatmentType treatmentType;
	    if (!trainingSchemeSegment.getPermittedTreatmentTypes().contains(TreatmentType.Inpatient)) {
		treatmentType = TreatmentType.Outpatient;
	    } else if (!trainingSchemeSegment.getPermittedTreatmentTypes().contains(TreatmentType.Outpatient)) {
		treatmentType = TreatmentType.Inpatient;
	    } else {
		treatmentType = null;
	    }

	    if (isFundableFor(trainingSchemeSegment.getMedicalField(), treatmentType)) {
		trainingSchemeSegments.add(trainingSchemeSegment);
	    }
	}

	return trainingSchemeSegments;
    }

    /**
     * Gibt zurück, inwiefern der Weiterbildungsabschnitt zum aktuellen Zeitpunkt
     * für das zugewiesene Weiterbildungssegment förderbar ist.
     *
     * @return
     */
    @Transient
    public FundableType getFundableType() {
	return getFundableType(LocalDate.now(), getAssignedSegment());
    }

    /**
     * Ermittelt, inwiefern der aktuelle Weiterbildungdabschnitt für den gegebenen
     * Abschnitt der Weiterbildungsordnung förderfähig ist.
     *
     * @param trainingSchemeSegment
     * @return
     */
    @Transient
    public FundableType getFundableType(LocalDate date, TrainingSchemeSegment trainingSchemeSegment) {
	if (null == trainingSchemeSegment) {
	    return FundableType.Unknown;
	}

	TreatmentType treatmentType;
	if (!trainingSchemeSegment.getPermittedTreatmentTypes().contains(TreatmentType.Inpatient)) {
	    treatmentType = TreatmentType.Outpatient;
	} else if (!trainingSchemeSegment.getPermittedTreatmentTypes().contains(TreatmentType.Outpatient)) {
	    treatmentType = TreatmentType.Inpatient;
	} else {
	    treatmentType = null;
	}

	if (!isFundableFor(trainingSchemeSegment.getMedicalField(), treatmentType)) {
	    return FundableType.NotFundable;
	} else {
	    // prüfen, ob bereits andere zum angegebenen Zeitpunkt abgeschlossene
	    // Weiterbildungsabschnitte für das
	    // entsprechende Ordnungssegment verplant wurden
	    int fundedDuration = trainingSchemeSegment.getDuration();
	    int plannedDuration = 0;
	    for (RotationPlanSegment rotationPlanSegment : rotationPlan.getFinishedSegments(date, trainingSchemeSegment)) {
		plannedDuration += rotationPlanSegment.getNormalisedDuration();
	    }

	    // falls bereits die maximale Anzahl an Monaten gefördert wurde, ist die
	    // aktuelle Station nicht mehr förderfähig
	    if (plannedDuration >= fundedDuration) {
		return FundableType.NotFundable;
	    }

	    // falls mit der geplanten Anzahl an Monaten die Förderhöchstdauer überschritten
	    // wird, kann die Station nur teilweise gefördert werden
	    plannedDuration += getNormalisedDuration();
	    if (plannedDuration > fundedDuration) {
		System.out.println(this + " ist teilweise förderbar: Zeitraum ist " + (plannedDuration - fundedDuration) + " zu lang");
		return FundableType.PartiallyFundable;
	    }

	    return FundableType.Fundable;
	}
    }

    @Transient
    public int getNormalisedDuration() {
	return getDuration() * fullTimeEquivalent / 100;
    }

    /**
     * Prüft ob der aktuele Weiterbildungsabschnitt für das gegebene medizinische
     * Feld und den gegebenen Behandlungstypen förderfähig ist.
     *
     * @param medicalField
     * @param treatmentType
     * @return
     */
    @Transient
    public boolean isFundableFor(MedicalField medicalField, TreatmentType treatmentType) {
	MedicalField thisField = trainingPosition.getAuthorisation().getTrainingSite().getMedicalField();
	TreatmentType thisTreatmentType = trainingPosition.getAuthorisation().getTrainingSite().getTreatmentType();

	if (null == treatmentType || thisTreatmentType.equals(treatmentType)) {
	    if (thisField.equals(medicalField) || ChildRelationUtil.isChildOf(thisField, medicalField)) {
		return true;
	    }
	}

	return false;
    }

    @Transient
    public boolean isNow() {
	LocalDate now = LocalDate.now();

	return segmentBegin.isBefore(now.plusDays(1)) && segmentEnd.isAfter(now.minusDays(1));
    }
}
