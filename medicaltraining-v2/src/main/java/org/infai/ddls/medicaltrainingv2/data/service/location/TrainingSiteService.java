package org.infai.ddls.medicaltrainingv2.data.service.location;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.infai.ddls.medicaltrainingv2.data.filter.location.ClinicFilter;
import org.infai.ddls.medicaltrainingv2.data.filter.location.DepartmentFilter;
import org.infai.ddls.medicaltrainingv2.data.filter.location.DoctorsOfficeFilter;
import org.infai.ddls.medicaltrainingv2.data.filter.location.TrainingSiteFilter;
import org.infai.ddls.medicaltrainingv2.data.filter.location.TrainingSiteWithFilter;
import org.infai.ddls.medicaltrainingv2.data.service.core.MedicalFieldService;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSiteWith;
import org.infai.ddls.medicaltrainingv2.model.location.Clinic;
import org.infai.ddls.medicaltrainingv2.model.location.Department;
import org.infai.ddls.medicaltrainingv2.model.location.DoctorsOffice;
import org.infai.ddls.medicaltrainingv2.model.location.TrainingSiteType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TrainingSiteService {
    @Autowired
    private ClinicService clinicService;
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private DoctorsOfficeService doctorsOfficeService;
    @Autowired
    private MedicalFieldService medicalFieldService;

    @Transactional
    public List<AbstractTrainingSite> get() {
	return get(new TrainingSiteFilter<>(), Pageable.unpaged());
    }

    @Transactional
    public List<Clinic> get(ClinicFilter filter, boolean loadDepartments, boolean loadEquipments, Pageable pageable) {
	List<Clinic> clinics = clinicService.get(filter, pageable);

	if (loadDepartments) {
	    for (Clinic clinic : clinics) {
		clinic.getDepartments().size();

		if (loadEquipments) {
		    for (Department department : clinic.getDepartments()) {
			department.getEquipments().size();
		    }
		}
	    }
	}

	return clinics;
    }

    @Transactional
    public List<DoctorsOffice> get(DoctorsOfficeFilter filter, boolean loadEquipments, Pageable pageable) {
	List<DoctorsOffice> doctorsOffices = doctorsOfficeService.get(filter, pageable);

	if (loadEquipments) {
	    for (DoctorsOffice doctorsOffice : doctorsOffices) {
		doctorsOffice.getEquipments().size();
	    }
	}

	return doctorsOffices;
    }

    @Transactional
    public List<AbstractTrainingSite> get(Pageable pageable) {
	return get(new TrainingSiteFilter<>(), pageable);
    }

    @Transactional
    public List<AbstractTrainingSite> get(TrainingSiteFilter<AbstractTrainingSite> filter, Pageable pageable) {
	List<AbstractTrainingSite> trainingSites = new ArrayList<>();

	trainingSites.addAll(doctorsOfficeService.get(DoctorsOfficeFilter.create(filter), pageable));
	trainingSites.addAll(clinicService.get(ClinicFilter.create(filter), pageable, true));

	return trainingSites;
    }

    @Transactional
    public List<AbstractTrainingSiteWith> get(TrainingSiteWithFilter<AbstractTrainingSiteWith> filter, boolean loadEquipments, Pageable pageable) throws Exception {
	List<AbstractTrainingSiteWith> trainingSites = new ArrayList<>();

	if (null != filter.getMedicalField()) {
	    // notwendig für transitive Kindbeziehungen
	    filter.setMedicalField(medicalFieldService.get(filter.getMedicalField().getId(), true, true));
	}

	trainingSites.addAll(departmentService.get(filter.convert(DepartmentFilter.class), pageable));
	trainingSites.addAll(doctorsOfficeService.get(filter.convert(DoctorsOfficeFilter.class), pageable));

	if (loadEquipments) {
	    for (AbstractTrainingSiteWith trainingSite : trainingSites) {
		trainingSite.getEquipments().size();
	    }
	}

	return trainingSites;
    }

    public AbstractTrainingSite get(UUID id) {
	AbstractTrainingSite trainingSite = clinicService.get(id);

	if (null == trainingSite) {
	    trainingSite = doctorsOfficeService.get(id);
	}
	if (null == trainingSite) {
	    trainingSite = departmentService.get(id);
	}

	return trainingSite;
    }

    @Transactional
    public AbstractTrainingSiteWith get(UUID id, TrainingSiteType type, boolean loadEquipments, boolean loadAuthorisedDoctors) {
	switch (type) {
	case DEPARTMENT:
	    return getDepartment(id, loadEquipments, loadAuthorisedDoctors);
	case DOCTORSOFFICE:
	    return getDoctorsOffice(id, loadEquipments, loadAuthorisedDoctors);
	default:
	    throw new RuntimeException("TrainingSiteType " + type + " not supported!");
	}
    }

    @Transactional
    public Clinic getClinic(UUID id, boolean loadDepartments, boolean loadEquipments) {
	Clinic clinic = clinicService.get(id);

	if (loadDepartments) {
	    clinic.getDepartments().size();

	    if (loadEquipments) {
		for (Department department : clinic.getDepartments()) {
		    department.getEquipments().size();
		}
	    }
	}

	return clinic;
    }

    @Transactional
    public Department getDepartment(UUID id, boolean loadEquipments, boolean loadAuthorisedDoctors) {
	Department department = departmentService.get(id);

	if (loadEquipments) {
	    department.getEquipments().size();
	}

	if (loadAuthorisedDoctors) {
	    department.getAuthorisedDoctors().size();
	}

	return department;
    }

    @Transactional
    public DoctorsOffice getDoctorsOffice(UUID id, boolean loadEquipments, boolean loadAuthorisedDoctors) {
	DoctorsOffice doctorsOffice = doctorsOfficeService.get(id);

	if (loadEquipments) {
	    doctorsOffice.getEquipments().size();
	}

	if (loadAuthorisedDoctors) {
	    doctorsOffice.getAuthorisedDoctors().size();
	}

	return doctorsOffice;
    }

    @Transactional
    public List<AbstractTrainingSite> getParentSites(boolean loadDepartments) {
	List<AbstractTrainingSite> trainingSites = new ArrayList<>();
	trainingSites.addAll(doctorsOfficeService.get());
	trainingSites.addAll(clinicService.get(true));

	return trainingSites;
    }

    @Transactional
    public Clinic save(Clinic clinic) {
	return clinicService.save(clinic);
    }

    @Transactional
    public Department save(Department department) {
	return departmentService.save(department);
    }

    @Transactional
    public DoctorsOffice save(DoctorsOffice doctorsOffice) {
	return doctorsOfficeService.save(doctorsOffice);
    }
}
