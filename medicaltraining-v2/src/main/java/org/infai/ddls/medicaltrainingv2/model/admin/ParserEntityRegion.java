package org.infai.ddls.medicaltrainingv2.model.admin;

import org.infai.ddls.medicaltrainingv2.model.location.Region;

import de.caterdev.utils.parser.ParserEntity;

public class ParserEntityRegion extends ParserEntity<Region> {
    public ParserEntityRegion() {
    }

    public ParserEntityRegion(ParserEntity<Region> parserEntity) {
	setColumns(parserEntity.getColumns());
	setEntity(parserEntity.getEntity());
	setRowNumber(parserEntity.getRowNumber());
    }
}
