package org.infai.ddls.medicaltrainingv2.data.service.core;

import java.util.List;
import java.util.UUID;

import org.infai.ddls.medicaltrainingv2.data.dao.core.SexDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.core.SexFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.spec.core.SexSpecification;
import org.infai.ddls.medicaltrainingv2.model.core.Sex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class SexService extends AbstractTrainingAppService<Sex, SexFilter> {
    @Autowired
    private SexDAO dao;

    @Override
    public List<Sex> get() {
	return get(new SexFilter(), DEFAULT_PAGING);
    }

    @Override
    public List<Sex> get(SexFilter filter, Pageable pageable) {
	return dao.get(new SexSpecification(filter), pageable);
    }

    @Override
    public Sex get(UUID id) {
	return dao.get(id);
    }

    @Override
    public Sex save(Sex entity) {
	return dao.save(entity);
    }

}
