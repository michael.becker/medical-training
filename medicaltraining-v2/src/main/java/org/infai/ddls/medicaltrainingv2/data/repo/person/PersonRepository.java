package org.infai.ddls.medicaltrainingv2.data.repo.person;

import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.model.person.Person;

public interface PersonRepository<T extends Person<T>> extends TrainingAppEntityRepository<T> {

}
