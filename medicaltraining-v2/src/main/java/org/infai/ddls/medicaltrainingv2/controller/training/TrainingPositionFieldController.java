package org.infai.ddls.medicaltrainingv2.controller.training;

import org.infai.ddls.medicaltrainingv2.controller.AbstractTypedTrainingAppController;
import org.infai.ddls.medicaltrainingv2.data.filter.training.RotationPlanSegmentFilter;
import org.infai.ddls.medicaltrainingv2.data.filter.training.TrainingPositionFieldFilter;
import org.infai.ddls.medicaltrainingv2.data.service.core.MedicalFieldService;
import org.infai.ddls.medicaltrainingv2.data.service.location.RegionService;
import org.infai.ddls.medicaltrainingv2.data.service.location.TrainingSiteService;
import org.infai.ddls.medicaltrainingv2.data.service.person.AuthorisedDoctorService;
import org.infai.ddls.medicaltrainingv2.data.service.training.RotationPlanSegmentService;
import org.infai.ddls.medicaltrainingv2.data.service.training.TrainingPositionFieldService;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.infai.ddls.medicaltrainingv2.model.location.Region;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionAvailability;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/training/trainingpositions")
public class TrainingPositionFieldController extends AbstractTypedTrainingAppController<TrainingPositionField, TrainingPositionFieldFilter, TrainingPositionFieldService> {
    @Autowired
    private MedicalFieldService medicalFieldService;
    @Autowired
    private AuthorisedDoctorService authorisedDoctorService;
    @Autowired
    private TrainingSiteService trainingSiteService;
    @Autowired
    private RegionService regionService;
    @Autowired
    private RotationPlanSegmentService rotationPlanSegmentService;

    @Override
    public String editEntity(@PathVariable UUID id, Model model) {
        return editEntity(getService().get(id, true), model);
    }

    @Override
    protected void additionalEditAttributes(TrainingPositionField entity, Model model) {
        super.additionalEditAttributes(entity, model);

        RotationPlanSegmentFilter filter = new RotationPlanSegmentFilter();
        filter.setTrainingPositionField(entity);
        filter.setEndAfter(LocalDate.now());

        List<RotationPlanSegment> segments = rotationPlanSegmentService.get(filter, null);
        model.addAttribute("rotationPlanSegments", segments);

        Map<LocalDate, TrainingPositionAvailability> availability = new LinkedHashMap<>();
        LocalDate now = LocalDate.now();
        for (int i = 0; i < 24; i++) {
            LocalDate testDate = now.plusMonths(i);
            TrainingPositionAvailability avail = entity.getAvailability(testDate.withDayOfMonth(1), testDate.withDayOfMonth(testDate.lengthOfMonth()), segments, 0);
            availability.put(testDate, avail);
        }
        model.addAttribute("availability", availability);

        filter = new RotationPlanSegmentFilter();
        filter.setTrainingPositionField(entity);
        filter.setEndBefore(LocalDate.now());
        model.addAttribute("pastSegments", rotationPlanSegmentService.get(filter, null));
    }

    @Override
    protected void additionalListAttributes(List<TrainingPositionField> entities, Model model) {
        super.additionalListAttributes(entities, model);

        model.addAttribute(new MedicalField().getViewNameMultipleEntities(), medicalFieldService.get());
        model.addAttribute(new AuthorisedDoctor().getViewNameMultipleEntities(), authorisedDoctorService.get());
        model.addAttribute("trainingSites", trainingSiteService.get(Pageable.unpaged()));
        model.addAttribute(new Region().getViewNameMultipleEntities(), regionService.get());
    }

    @Override
    protected TrainingPositionField getEntity() {
        return new TrainingPositionField();
    }

    @Override
    protected TrainingPositionFieldFilter getFilter() {
        return new TrainingPositionFieldFilter();
    }

    @Override
    protected String mappingDefault() {
        return "/training/trainingpositions";
    }

    @Override
    protected String templateFolder() {
        return "/training";
    }
}
