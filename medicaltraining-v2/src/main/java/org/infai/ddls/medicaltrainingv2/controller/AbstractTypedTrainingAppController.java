package org.infai.ddls.medicaltrainingv2.controller;

import lombok.Getter;
import lombok.extern.apachecommons.CommonsLog;
import org.infai.ddls.medicaltrainingv2.data.filter.TrainingAppEntityFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import java.util.List;
import java.util.UUID;

@CommonsLog
@Getter
public abstract class AbstractTypedTrainingAppController<T extends AbstractTrainingAppEntity, U extends TrainingAppEntityFilter<T>, V extends AbstractTrainingAppService<T, U>> extends AbstractTrainingAppController implements TrainingAppController<T, U, V> {
    @Autowired
    protected V service;

    /**
     * Archives an entity identified by its UUID.
     *
     * @param id
     * @param model
     * @return
     */
    @GetMapping("/{id}/archive")
    public String archiveEntity(@PathVariable UUID id, Model model) {
        T entity = getService().get(id);
        entity.setArchived(true);
        getService().save(entity);

        return redirect(mappingList());
    }

    @GetMapping("/create")
    public String createEntity(Model model) {
        return editEntity(getEntity(), model);
    }

    @GetMapping("/{id}/edit")
    public String editEntity(@PathVariable UUID id, Model model) {
        return editEntity(getService().get(id), model);
    }

    @PostMapping("/filter")
    public String filterEntities(@ModelAttribute @Validated U filter, BindingResult bindingResult, @RequestParam String filterAction, Model model, Pageable pageable, ServletRequest servletRequest) {
        switch (FilterAction.valueOf(filterAction)) {
            case filter:
                return listEntities(getService().get(filter, pageable), filter, model);
            case reset:
                return redirect(mappingList());
            default:
                throw new RuntimeException("FilterAction " + filterAction + " not supported!");
        }
    }

    @GetMapping("/list")
    public String listEntities(Model model) {
        return listEntities(getService().get(), getFilter(), model);
    }

    @PostMapping("update")
    public String updateEntity(@ModelAttribute @Validated T entity, BindingResult bindingResult, @RequestParam String saveAction, Model model, ServletRequest servletRequest) {
        System.out.println(formatHTTPParameters(servletRequest));

        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());

            return editEntity(entity, model);
        } else {
            entity = getService().save(entity);

            switch (SaveAction.valueOf(saveAction)) {
                case save:
                    return redirect(mappingEdit(entity.getId()));
                case saveAndClose:
                    return redirect(mappingList());
                default:
                    throw new RuntimeException("SaveAction " + saveAction + " not supported!");
            }

        }
    }

    protected void additionalEditAttributes(T entity, Model model) {
        return;
    }

    protected void additionalListAttributes(List<T> entities, Model model) {
        return;
    }

    protected String editEntity(T entity, Model model) {
        model.addAttribute(modelAttributeEntity(), entity);
        additionalEditAttributes(entity, model);

        return getEditTemplate();
    }

    protected String getEditTemplate() {
        return templateFolder() + "/edit" + getEntity().getTemplateNameSingleEntity();
    }

    protected abstract T getEntity();

    protected abstract U getFilter();

    protected String getListTemplate() {
        return templateFolder() + "/list" + getEntity().getTemplateNameMultipleEntities();
    }

    protected String listEntities(List<T> entities, U filter, Model model) {
        model.addAttribute(modelAttributeEntities(), entities);
        model.addAttribute(modelAttributeFilter(), filter);
        additionalListAttributes(entities, model);

        return getListTemplate();
    }

    protected abstract String mappingDefault();

    protected String mappingEdit(UUID id) {
        return mappingDefault() + "/" + id + "/edit";
    }

    protected String mappingList() {
        return mappingDefault() + "/list";
    }

    protected String modelAttributeEntities() {
        return getEntity().getViewNameMultipleEntities();
    }

    protected String modelAttributeEntity() {
        return getEntity().getViewNameSingleEntity();
    }

    protected String modelAttributeFilter() {
        return getFilter().getViewName();
    }

    protected abstract String templateFolder();

}
