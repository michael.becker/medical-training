package org.infai.ddls.medicaltrainingv2.data.dao.training;

import org.infai.ddls.medicaltrainingv2.data.dao.AbstractTrainingAppDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.training.TrainingPositionFieldFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.training.TrainingPositionFieldRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.training.TrainingPositionFieldSpecification;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;
import org.springframework.stereotype.Component;

@Component
public class TrainingPositionFieldDAO extends AbstractTrainingAppDAO<TrainingPositionField, TrainingPositionFieldRepository, TrainingPositionFieldFilter, TrainingPositionFieldSpecification> {

}
