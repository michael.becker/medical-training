package org.infai.ddls.medicaltrainingv2.data.service.person;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.infai.ddls.medicaltrainingv2.data.dao.person.AuthorisedDoctorDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.person.AuthorisedDoctorFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.spec.person.AuthorisedDoctorSpecification;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AuthorisedDoctorService extends AbstractTrainingAppService<AuthorisedDoctor, AuthorisedDoctorFilter> {
    @Autowired
    private AuthorisedDoctorDAO dao;

    @Override
    public List<AuthorisedDoctor> get() {
	return get(new AuthorisedDoctorFilter(), DEFAULT_PAGING, false, false, false);
    }

    @Override
    public List<AuthorisedDoctor> get(AuthorisedDoctorFilter filter, Pageable pageable) {
	return get(filter, pageable, false, false, false);
    }

    @Transactional
    public List<AuthorisedDoctor> get(AuthorisedDoctorFilter filter, Pageable pageable, boolean loadAuthorisations) {
	return get(filter, pageable, loadAuthorisations, false, false);
    }

    @Transactional
    public List<AuthorisedDoctor> get(AuthorisedDoctorFilter filter, Pageable pageable, boolean loadAuthorisations, boolean loadTrainingPositions, boolean loadExclusions) {
	List<AuthorisedDoctor> authorisedDoctors = dao.get(new AuthorisedDoctorSpecification(filter), pageable);

	if (loadAuthorisations || loadTrainingPositions || loadExclusions) {
	    for (AuthorisedDoctor authorisedDoctor : authorisedDoctors) {
		authorisedDoctor.getAuthorisations().size();

		if (loadTrainingPositions || loadExclusions) {
		    for (AuthorisationField authorisation : authorisedDoctor.getAuthorisations()) {
			authorisation.getTrainingPositions().size();

			if (loadExclusions) {
			    for (TrainingPositionField trainingPosition : authorisation.getTrainingPositions()) {
				trainingPosition.getExclusions().size();
			    }
			}
		    }
		}
	    }
	}

	return authorisedDoctors;
    }

    @Transactional
    public List<AuthorisedDoctor> get(boolean loadAuthorisations, boolean loadTrainingPositions, boolean loadExclusions) {
	return get(new AuthorisedDoctorFilter(), DEFAULT_PAGING, loadAuthorisations, loadTrainingPositions, loadExclusions);
    }

    @Override
    public AuthorisedDoctor get(UUID id) {
	return dao.get(id);
    }

    public boolean isLANRUsedByOther(AuthorisedDoctor authorisedDoctor) {
	if (null == authorisedDoctor.getLanr()) {
	    return false;
	} else {
	    return dao.isLANRUsedByOther(authorisedDoctor.getLanr(), authorisedDoctor);
	}
    }

    @Override
    @Transactional
    public AuthorisedDoctor save(AuthorisedDoctor entity) {
	return dao.save(entity);
    }
}
