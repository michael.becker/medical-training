package org.infai.ddls.medicaltrainingv2.data.service.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@PropertySource("classpath:/user.properties")
public class MedicalTrainingUserDetailsService implements UserDetailsService {
    @Autowired
    private Environment env;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        String propertyName = "user." + username + ".name";
        String propertyRole = "user." + username + ".role";
        String propertyPassword = "user." + username + ".password";

        if (!env.containsProperty(propertyName)) {
            throw new UsernameNotFoundException("Property " + propertyName + " not found");
        }
        if (!env.containsProperty(propertyRole)) {
            throw new UsernameNotFoundException("Property " + propertyRole + " not found");
        }
        if (!env.containsProperty(propertyPassword)) {
            throw new UsernameNotFoundException("Property " + propertyPassword + " not found");
        }

        String internalName = env.getProperty(propertyName);
        String userRole = env.getProperty(propertyRole);
        String userPassword = env.getProperty(propertyPassword);

        return User.withUsername(internalName).password(new BCryptPasswordEncoder().encode(userPassword)).roles(userRole).build();
    }
}
