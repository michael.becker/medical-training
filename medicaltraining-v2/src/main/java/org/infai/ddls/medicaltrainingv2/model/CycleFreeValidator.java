package org.infai.ddls.medicaltrainingv2.model;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CycleFreeValidator implements ConstraintValidator<CycleFreeConstraint, HasChildren<? extends AbstractTrainingAppEntity>> {
    @Override
    public boolean isValid(HasChildren<? extends AbstractTrainingAppEntity> value, ConstraintValidatorContext context) {
	if (null == value.getParent()) {
	    return true;
	} else if (ChildRelationUtil.isChildOf(value, value)) {
	    return false;
	} else {
	    return true;
	}
    }
}
