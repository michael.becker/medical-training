package org.infai.ddls.medicaltrainingv2.data.service.training;

import java.util.List;
import java.util.UUID;

import org.infai.ddls.medicaltrainingv2.data.dao.training.TrainingSchemeSegmentDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.training.TrainingSchemeSegmentFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.spec.training.TrainingSchemeSegmentSpecification;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingSchemeSegment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TrainingSchemeSegmentService extends AbstractTrainingAppService<TrainingSchemeSegment, TrainingSchemeSegmentFilter> {
    @Autowired
    private TrainingSchemeSegmentDAO dao;

    @Override
    public List<TrainingSchemeSegment> get() {
	return get(new TrainingSchemeSegmentFilter(), Pageable.unpaged());
    }

    @Override
    public List<TrainingSchemeSegment> get(TrainingSchemeSegmentFilter filter, Pageable pageable) {
	return dao.get(new TrainingSchemeSegmentSpecification(filter), pageable);
    }

    @Override
    public TrainingSchemeSegment get(UUID id) {
	return dao.get(id);
    }

    @Override
    public TrainingSchemeSegment save(TrainingSchemeSegment entity) {
	return dao.save(entity);
    }

}
