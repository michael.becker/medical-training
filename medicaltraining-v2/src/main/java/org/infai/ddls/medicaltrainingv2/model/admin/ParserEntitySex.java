package org.infai.ddls.medicaltrainingv2.model.admin;

import org.infai.ddls.medicaltrainingv2.model.core.Sex;

import de.caterdev.utils.parser.ParserEntity;

public class ParserEntitySex extends ParserEntity<Sex> {
    public ParserEntitySex() {
    }

    public ParserEntitySex(ParserEntity<Sex> parserEntity) {
	setColumns(parserEntity.getColumns());
	setEntity(parserEntity.getEntity());
	setRowNumber(parserEntity.getRowNumber());
    }
}
