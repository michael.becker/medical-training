package org.infai.ddls.medicaltrainingv2.model.admin;

import org.infai.ddls.medicaltrainingv2.model.person.Registrar;

import de.caterdev.utils.parser.ParserEntity;

public class ParserEntityRegistrar extends ParserEntity<Registrar> {
    public ParserEntityRegistrar() {
    }

    public ParserEntityRegistrar(ParserEntity<Registrar> parserEntity) {
	setColumns(parserEntity.getColumns());
	setEntity(parserEntity.getEntity());
	setRowNumber(parserEntity.getRowNumber());
    }
}
