package org.infai.ddls.medicaltrainingv2.controller;

public enum FilterAction {
    filter, reset;

    public static final String REQUEST_PARAM = "filterAction";
}
