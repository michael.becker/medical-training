package org.infai.ddls.medicaltrainingv2.data.dao.location;

import org.infai.ddls.medicaltrainingv2.data.dao.AbstractNamedTrainingAppDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.location.RegionFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.location.RegionRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.location.RegionSpecification;
import org.infai.ddls.medicaltrainingv2.model.location.Region;
import org.springframework.stereotype.Component;

@Component
public class RegionDAO extends AbstractNamedTrainingAppDAO<Region, RegionRepository, RegionFilter, RegionSpecification> {

}
