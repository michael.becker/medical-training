package org.infai.ddls.medicaltrainingv2.data.repo.location;

import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.model.location.Location;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface LocationRepository<T extends Location> extends TrainingAppEntityRepository<T> {

}
