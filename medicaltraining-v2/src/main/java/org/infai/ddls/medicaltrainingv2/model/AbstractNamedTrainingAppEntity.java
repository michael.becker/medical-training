package org.infai.ddls.medicaltrainingv2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

import org.hibernate.envers.Audited;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Audited
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true, callSuper = true)
public abstract class AbstractNamedTrainingAppEntity extends AbstractTrainingAppEntity {
    @NotBlank
    @Column(nullable = false)
    @ToString.Include
    private String name;
}
