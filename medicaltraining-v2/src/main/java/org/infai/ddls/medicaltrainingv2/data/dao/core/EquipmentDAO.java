package org.infai.ddls.medicaltrainingv2.data.dao.core;

import org.infai.ddls.medicaltrainingv2.data.dao.AbstractNamedTrainingAppDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.core.EquipmentFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.core.EquipmentRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.core.EquipmentSpecification;
import org.infai.ddls.medicaltrainingv2.model.core.Equipment;
import org.springframework.stereotype.Component;

@Component
public class EquipmentDAO extends AbstractNamedTrainingAppDAO<Equipment, EquipmentRepository, EquipmentFilter, EquipmentSpecification> {

}
