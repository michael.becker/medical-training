package org.infai.ddls.medicaltrainingv2.data.dao.location;

import org.infai.ddls.medicaltrainingv2.data.dao.AbstractNamedTrainingAppDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.location.LocationFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.location.LocationRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.location.LocationSpecification;
import org.infai.ddls.medicaltrainingv2.model.location.Location;

public class LocationDAO<T extends Location, U extends LocationRepository<T>, V extends LocationFilter<T>, W extends LocationSpecification<T, V>> extends AbstractNamedTrainingAppDAO<T, U, V, W> {
}
