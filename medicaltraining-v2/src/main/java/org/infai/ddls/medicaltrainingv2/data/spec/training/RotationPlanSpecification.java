package org.infai.ddls.medicaltrainingv2.data.spec.training;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltrainingv2.data.filter.training.RotationPlanFilter;
import org.infai.ddls.medicaltrainingv2.data.spec.AbstractTrainingAppEntitySpecification;
import org.infai.ddls.medicaltrainingv2.model.person.Registrar;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlan;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlan_;

@SuppressWarnings("serial")
public class RotationPlanSpecification extends AbstractTrainingAppEntitySpecification<RotationPlan, RotationPlanFilter> {
    public RotationPlanSpecification(RotationPlanFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<RotationPlan> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (null != filter.getRegistrar()) {
	    addPredicate(isFromRegistrar(filter.getRegistrar(), root, query, criteriaBuilder));
	}
    }

    private Predicate isFromRegistrar(Registrar registrar, Root<RotationPlan> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get(RotationPlan_.registrar), registrar);

    }
}
