package org.infai.ddls.medicaltrainingv2.model.admin;

import org.infai.ddls.medicaltrainingv2.model.location.Region;

import de.caterdev.utils.parser.ParserEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImportRegionListEntity implements ImportListEntity<Region> {
    private ParserEntityRegion originalEntity;
    private boolean doImport = true;

    public ImportRegionListEntity() {
    }

    public ImportRegionListEntity(ParserEntity<Region> originalEntity) {
	setOriginalEntity(new ParserEntityRegion(originalEntity));
    }
}
