package org.infai.ddls.medicaltrainingv2.data.repo.training;

import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainingPositionFieldRepository extends TrainingAppEntityRepository<TrainingPositionField> {

}
