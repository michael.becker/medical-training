package org.infai.ddls.medicaltrainingv2.model.core;

public enum TreatmentType {
    Inpatient, Outpatient;
}
