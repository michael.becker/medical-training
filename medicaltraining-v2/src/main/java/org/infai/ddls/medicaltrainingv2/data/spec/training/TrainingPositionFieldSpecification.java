package org.infai.ddls.medicaltrainingv2.data.spec.training;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;

import org.infai.ddls.medicaltrainingv2.data.filter.training.TrainingPositionFieldFilter;
import org.infai.ddls.medicaltrainingv2.data.spec.AbstractTrainingAppEntitySpecification;
import org.infai.ddls.medicaltrainingv2.data.spec.core.MedicalFieldSpecification;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.infai.ddls.medicaltrainingv2.model.authorisation.Authorisation_;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSiteWith;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSiteWith_;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite_;
import org.infai.ddls.medicaltrainingv2.model.location.Clinic;
import org.infai.ddls.medicaltrainingv2.model.location.Department;
import org.infai.ddls.medicaltrainingv2.model.location.Region;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField_;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPosition_;

@SuppressWarnings("serial")
public class TrainingPositionFieldSpecification extends AbstractTrainingAppEntitySpecification<TrainingPositionField, TrainingPositionFieldFilter> {
    public TrainingPositionFieldSpecification(TrainingPositionFieldFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	addPredicate(MedicalFieldSpecification.toPredicate(filter.getMedicalField(), root.get(TrainingPositionField_.authorisation).get(Authorisation_.trainingSite).get(AbstractTrainingSiteWith_.medicalField), query, criteriaBuilder));

	if (null != filter.getAvailableFrom()) {
	    addPredicate(isAvailableFrom(filter.getAvailableFrom(), root, query, criteriaBuilder));
	}
	if (null != filter.getAvailableUntil()) {
	    addPredicate(isAvailableUntil(filter.getAvailableUntil(), root, query, criteriaBuilder));
	}

	if (filter.isPartTimeAvailable()) {
	    addPredicate(criteriaBuilder.isTrue(root.get(TrainingPosition_.partTimeAvailable)));

	}

	addPredicate(isPartTimeAvailable(filter.getFullTimeEquivalent(), root, query, criteriaBuilder));
	addPredicate(isAvailable(filter.getMinimalCapacity(), root, query, criteriaBuilder));

//	addPredicate(LocationSpecification.toPredicate(filter.get, root.get("authorisationField").get("trainingSite").get("latitude"), root.get("authorisationField").get("trainingSite").get("longitude"), query, criteriaBuilder));

	if (null != filter.getAuthorisationField()) {
	    addPredicate(isFromAuthorisationField(filter.getAuthorisationField(), root, query, criteriaBuilder));
	}
	if (null != filter.getAuthorisedDoctor()) {
	    addPredicate(isFromAuthorisedDoctor(filter.getAuthorisedDoctor(), root, query, criteriaBuilder));
	}

	if (null != filter.getTrainingSite()) {
	    addPredicate(isAtTrainingSite(filter.getTrainingSite(), root, query, criteriaBuilder));
	}

	if (null != filter.getRegion()) {
	    addPredicate(isInRegion(filter.getRegion(), root, query, criteriaBuilder));
	}

	addPredicate(isInRegions(filter.getRegions(), root, query, criteriaBuilder));
    }

    private Predicate isAtTrainingSite(AbstractTrainingSite trainingSite, Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (trainingSite instanceof Clinic) {
	    List<Predicate> predicates = new ArrayList<>();
	    for (Department department : ((Clinic) trainingSite).getDepartments()) {
		predicates.add(isAtTrainingSite(department, root, query, criteriaBuilder));
	    }

	    return criteriaBuilder.or(predicates.toArray(new Predicate[predicates.size()]));
	} else {
	    return isAtTrainingSite((AbstractTrainingSiteWith) trainingSite, root, query, criteriaBuilder);
	}
    }

    private Predicate isAtTrainingSite(AbstractTrainingSiteWith trainingSite, Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get(TrainingPosition_.authorisation).get(Authorisation_.trainingSite), trainingSite);
    }

    private Predicate isAvailable(int minimalCapacity, Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.greaterThanOrEqualTo(root.get(TrainingPosition_.capacity), minimalCapacity);
    }

    private Predicate isAvailableFrom(@NotNull LocalDate begin, Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	List<Predicate> predicates = new ArrayList<>();
	predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(TrainingPosition_.availableFrom), begin));
	predicates.add(criteriaBuilder.or(criteriaBuilder.isNull(root.get(TrainingPosition_.availableUntil)), criteriaBuilder.greaterThanOrEqualTo(root.get(TrainingPosition_.availableUntil), begin)));

	return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }

    private Predicate isAvailableUntil(@NotNull LocalDate end, Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	List<Predicate> predicates = new ArrayList<>();
	predicates.add(criteriaBuilder.or(criteriaBuilder.isNull(root.get(TrainingPosition_.availableUntil)), criteriaBuilder.greaterThanOrEqualTo(root.get(TrainingPosition_.availableUntil), end)));
	predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(TrainingPosition_.availableFrom), end));

	return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }

    private Predicate isFromAuthorisationField(AuthorisationField authorisationField, Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get(TrainingPosition_.authorisation), authorisationField);
    }

    private Predicate isFromAuthorisedDoctor(AuthorisedDoctor authorisedDoctor, Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get(TrainingPosition_.authorisation).get(Authorisation_.authorisedDoctor), authorisedDoctor);
    }

    private Predicate isInRegion(Region region, Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get(TrainingPosition_.authorisation).get(Authorisation_.trainingSite).get(AbstractTrainingSite_.region), region);
    }

    private Predicate isInRegions(List<Region> regions, Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (regions.isEmpty()) {
	    return criteriaBuilder.conjunction();
	} else {
	    List<Predicate> predicates = new ArrayList<>(regions.size());

	    for (Region region : regions) {
		predicates.add(criteriaBuilder.equal(root.get(TrainingPosition_.authorisation).get(Authorisation_.trainingSite).get(AbstractTrainingSite_.region), region));
	    }

	    return criteriaBuilder.or(predicates.toArray(new Predicate[predicates.size()]));
	}
    }

    private Predicate isPartTimeAvailable(int fullTimeEquivalents, Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (fullTimeEquivalents < 100) {
	    return criteriaBuilder.isTrue(root.get(TrainingPosition_.partTimeAvailable));
	} else {
	    return criteriaBuilder.conjunction();
	}
    }

}