package org.infai.ddls.medicaltrainingv2.data.repo.person;

import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.model.person.ConversationRegistrar;
import org.springframework.stereotype.Repository;

@Repository
public interface ConversationRegistrarRepository extends TrainingAppEntityRepository<ConversationRegistrar> {

}
