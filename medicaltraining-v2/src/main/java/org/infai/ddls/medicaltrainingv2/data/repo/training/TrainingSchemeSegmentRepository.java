package org.infai.ddls.medicaltrainingv2.data.repo.training;

import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingSchemeSegment;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainingSchemeSegmentRepository extends TrainingAppEntityRepository<TrainingSchemeSegment> {

}
