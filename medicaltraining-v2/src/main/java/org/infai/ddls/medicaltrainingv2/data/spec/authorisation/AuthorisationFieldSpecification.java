package org.infai.ddls.medicaltrainingv2.data.spec.authorisation;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltrainingv2.data.filter.authorisation.AuthorisationFieldFilter;
import org.infai.ddls.medicaltrainingv2.data.spec.AbstractTrainingAppEntitySpecification;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField_;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;

@SuppressWarnings("serial")
public class AuthorisationFieldSpecification extends AbstractTrainingAppEntitySpecification<AuthorisationField, AuthorisationFieldFilter> {

    public AuthorisationFieldSpecification(AuthorisationFieldFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<AuthorisationField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (null != filter.getTrainingSite()) {
	    addPredicate(hasTrainingSite(filter.getTrainingSite(), root, query, criteriaBuilder));
	}
	if (null != filter.getAuthorisedDoctor()) {
	    addPredicate(hasAuthorisedDoctor(filter.getAuthorisedDoctor(), root, query, criteriaBuilder));
	}
    }

    private Predicate hasAuthorisedDoctor(AuthorisedDoctor authorisedDoctor, Root<AuthorisationField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get(AuthorisationField_.authorisedDoctor), authorisedDoctor);
    }

    private Predicate hasTrainingSite(AbstractTrainingSite trainingSite, Root<AuthorisationField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get(AuthorisationField_.trainingSite), trainingSite);
    }

}
