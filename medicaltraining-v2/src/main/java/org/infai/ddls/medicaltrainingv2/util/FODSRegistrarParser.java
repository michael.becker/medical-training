package org.infai.ddls.medicaltrainingv2.util;

import org.infai.ddls.medicaltrainingv2.model.admin.ParserEntityRegistrar;
import org.infai.ddls.medicaltrainingv2.model.core.Sex;
import org.infai.ddls.medicaltrainingv2.model.location.Region;
import org.infai.ddls.medicaltrainingv2.model.person.ConversationRegistrar;
import org.infai.ddls.medicaltrainingv2.model.person.Registrar;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FODSRegistrarParser extends AbstractFODSAuthorisedSheetParser<Registrar, ParserEntityRegistrar> {
    private static final int COLUMN_LASTNAME = 1;
    private static final int COLUMN_FIRSTNAME = 2;
    private static final int COLUMN_SEX = 3;
    private static final int COLUMN_TITLE = 4;
    private static final int COLUMN_STREET = 5;
    private static final int COLUMN_ZIPCODE = 6;
    private static final int COLUMN_CITY = 7;
    private static final int COLUMN_PHONE = 8;
    private static final int COLUMN_MAIL = 9;

    private static final int COLUMN_KOMPAS = 10;
    private static final int COLUMN_NOTES = 11;
    private static final int COLUMN_LATERAL_ENTRY = 12;
    private static final int COLUMN_EXISTING_STATIONS = 13;
    private static final int COLUMN_CURRENT_STATION = 14;
    private static final int COLUMN_APPROVAL_CURRENT_STATION = 15;
    private static final int COLUMN_PLANNED_STATIONS = 16;
    private static final int COLUMN_MISSING_STATIONS = 17;
    private static final int COLUMN_WISHES = 18;
    private static final int COLUMN_PREFERRED_REGIONS = 19;
    private static final int COLUMN_CAREER_PLAN = 20;
    private static final int COLUMN_APPROVAL_VAHS = 21;
    private static final int COLUMN_APPROVAL_KVSA = 22;
    private static final int COLUMN_LAST_CONTACT_CONTENT = 23;
    private static final int COLUMN_LAST_CONTACT_TYPE = 24;
    private static final int COLUMN_LAST_CONTACT_WHO = 25;

    private Map<String, Region> regions = new HashMap<>();
    private Map<String, Sex> sexes = new HashMap<>();

    public FODSRegistrarParser(Collection<Region> regions, Collection<Sex> sexes) {
        for (Region region : regions) {
            this.regions.put(region.getName(), region);
        }

        for (Sex sex : sexes) {
            this.sexes.put(sex.getName(), sex);
        }
    }

    @Override
    protected ParserEntityRegistrar parserEntity() {
        return new ParserEntityRegistrar();
    }

    @Override
    protected Registrar parseRowResult(List<String> row, int rowNumber) throws Exception {
        Registrar registrar = new Registrar();
        registrar.setFirstName(row.get(COLUMN_FIRSTNAME));
        registrar.setLastName(row.get(COLUMN_LASTNAME));

        String streetAndHousenumber = row.get(COLUMN_STREET).trim();
        String street = streetAndHousenumber;
        String housenumber = "n/a";
        int separatorStreetHousenumber = streetAndHousenumber.lastIndexOf(" ");
        if (separatorStreetHousenumber > 0) {
            street = streetAndHousenumber.substring(0, separatorStreetHousenumber).trim();
            housenumber = streetAndHousenumber.substring(separatorStreetHousenumber).trim();
        } else {
            System.out.println(separatorStreetHousenumber);
        }

        registrar.setAddress(createAddress(street, housenumber, row.get(COLUMN_ZIPCODE), row.get(COLUMN_CITY)));

        registrar.getContactInfo().setEmail(row.get(COLUMN_MAIL));
        registrar.getContactInfo().setPhoneNumber(row.get(COLUMN_PHONE));

        registrar.setSex(sexes.get(row.get(COLUMN_SEX)));

        registrar.setKompasSubscription(LocalDate.parse(row.get(COLUMN_KOMPAS), DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));
        if (null != row.get(COLUMN_APPROVAL_VAHS) && !row.get(COLUMN_APPROVAL_VAHS).equals("-")) {
            registrar.setApprovalVAHSTransmission(LocalDate.parse(row.get(COLUMN_APPROVAL_VAHS), DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));
        }
        if (null != row.get(COLUMN_APPROVAL_KVSA)) {
            registrar.setApprovalKVSATransmission(LocalDate.parse(row.get(COLUMN_APPROVAL_KVSA), DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));
        }

        registrar.setNotes(row.get(COLUMN_NOTES));

        if (row.get(COLUMN_LATERAL_ENTRY).equals("ja")) {
            registrar.setLateralEntry(true);
        }

        ConversationRegistrar conversation = new ConversationRegistrar();
        conversation.setContact(registrar);
        conversation.setContactPerson(row.get(COLUMN_LAST_CONTACT_WHO));
        conversation.setConversationType(null);
        conversation.setConversationDate(null);
        conversation.setDescription(row.get(COLUMN_LAST_CONTACT_CONTENT));

        if (null != row.get(COLUMN_PREFERRED_REGIONS)) {
            String[] preferredRegions = row.get(COLUMN_PREFERRED_REGIONS).split(",");
            for (String preferredRegion : preferredRegions) {
                registrar.getPreferredRegions().add(regions.get(preferredRegion.trim()));
            }
        }

        return registrar;
    }

}
