package org.infai.ddls.medicaltrainingv2.data.service.training;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.infai.ddls.medicaltrainingv2.data.dao.training.TrainingPositionFieldDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.training.RotationPlanSegmentFilter;
import org.infai.ddls.medicaltrainingv2.data.filter.training.TrainingPositionFieldFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.service.core.MedicalFieldService;
import org.infai.ddls.medicaltrainingv2.data.spec.training.TrainingPositionFieldSpecification;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TrainingPositionFieldService extends AbstractTrainingAppService<TrainingPositionField, TrainingPositionFieldFilter> {
    @Autowired
    private TrainingPositionFieldDAO dao;
    @Autowired
    private MedicalFieldService medicalFieldService;
    @Autowired
    private RotationPlanSegmentService rotationPlanSegmentService;

    @Override
    public List<TrainingPositionField> get() {
	return get(new TrainingPositionFieldFilter(), DEFAULT_PAGING);
    }

    @Override
    @Transactional
    public List<TrainingPositionField> get(TrainingPositionFieldFilter filter, Pageable pageable) {
	// notwendig, damit auch Kindbereiche gefunden werden
	if (null != filter.getMedicalField()) {
	    filter.setMedicalField(medicalFieldService.get(filter.getMedicalField().getId(), true, true));
	}

	List<TrainingPositionField> positions = dao.get(new TrainingPositionFieldSpecification(filter), pageable);

	// falls im Filter eine zeitliche Einschränkung gesetzt wurde: Die Verfügbarkeit
	// der jeweiligen WB-Stellen in diesem Zeitraum setzen
	if (null != filter.getAvailableFrom() || null != filter.getAvailableUntil()) {
	    for (TrainingPositionField position : positions) {
		position.setAvailability(position.getAvailability(filter.getAvailableFrom(), filter.getAvailableUntil(), rotationPlanSegmentService.get(new RotationPlanSegmentFilter(), null)));
	    }
	}

	return positions;
    }

    @Override
    public TrainingPositionField get(UUID id) {
	return get(id, false);
    }

    @Transactional
    public TrainingPositionField get(UUID id, boolean loadExclusions) {
	TrainingPositionField trainingPosition = dao.get(id);

	if (loadExclusions) {
	    trainingPosition.getExclusions().size();
	}

	return trainingPosition;
    }

    @Override
    public TrainingPositionField save(TrainingPositionField entity) {
	return dao.save(entity);
    }

}
