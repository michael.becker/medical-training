package org.infai.ddls.medicaltrainingv2.model.admin;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ColumnDefault;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class FileUpload extends AbstractTrainingAppEntity {
    @NotNull
    @Column(nullable = false)
    private LocalDate uploadDate;
    @NotNull
    @Column(nullable = false)
    private String fileName;
    @NotNull
    @Column(nullable = false)
    @ColumnDefault("false")
    private boolean titlesImported = false;
    @NotNull
    @Column(nullable = false)
    @ColumnDefault("false")
    private boolean sexesImported = false;
    @NotNull
    @Column(nullable = false)
    @ColumnDefault("false")
    private boolean regionsImported = false;
    @NotNull
    @Column(nullable = false)
    @ColumnDefault("false")
    private boolean trainingSitesImported = false;
    @NotNull
    @Column(nullable = false)
    @ColumnDefault("false")
    private boolean authorisedDoctorsImported = false;
    @NotNull
    @Column(nullable = false)
    @ColumnDefault("false")
    private boolean registrarsImported = false;
}
