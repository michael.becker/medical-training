package org.infai.ddls.medicaltrainingv2.data.dao.location;

import org.infai.ddls.medicaltrainingv2.data.filter.location.ClinicFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.location.ClinicRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.location.ClinicSpecification;
import org.infai.ddls.medicaltrainingv2.model.location.Clinic;
import org.springframework.stereotype.Component;

@Component
public class CliniCDAO extends TrainingSiteDAO<Clinic, ClinicRepository, ClinicFilter, ClinicSpecification> {

}
