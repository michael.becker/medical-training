package org.infai.ddls.medicaltrainingv2.data.spec.location;

import org.infai.ddls.medicaltrainingv2.data.filter.location.DoctorsOfficeFilter;
import org.infai.ddls.medicaltrainingv2.model.location.DoctorsOffice;

@SuppressWarnings("serial")
public class DoctorsOfficeSpecification extends TrainingSiteWithSpecification<DoctorsOffice, DoctorsOfficeFilter> {

    public DoctorsOfficeSpecification(DoctorsOfficeFilter filter) {
	super(filter);
    }

}
