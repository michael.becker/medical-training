package org.infai.ddls.medicaltrainingv2.data.dao.core;

import org.infai.ddls.medicaltrainingv2.data.dao.AbstractNamedTrainingAppDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.core.ConversationTypeFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.core.ConversationTypeRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.core.ConversationTypeSpecification;
import org.infai.ddls.medicaltrainingv2.model.person.ConversationType;
import org.springframework.stereotype.Component;

@Component
public class ConversationTypeDAO extends AbstractNamedTrainingAppDAO<ConversationType, ConversationTypeRepository, ConversationTypeFilter, ConversationTypeSpecification> {

}
