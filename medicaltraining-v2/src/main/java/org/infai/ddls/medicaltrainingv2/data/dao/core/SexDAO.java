package org.infai.ddls.medicaltrainingv2.data.dao.core;

import org.infai.ddls.medicaltrainingv2.data.dao.AbstractNamedTrainingAppDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.core.SexFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.core.SexRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.core.SexSpecification;
import org.infai.ddls.medicaltrainingv2.model.core.Sex;
import org.springframework.stereotype.Component;

@Component
public class SexDAO extends AbstractNamedTrainingAppDAO<Sex, SexRepository, SexFilter, SexSpecification> {

}
