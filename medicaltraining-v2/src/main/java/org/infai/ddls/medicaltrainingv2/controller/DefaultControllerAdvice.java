package org.infai.ddls.medicaltrainingv2.controller;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import org.infai.ddls.medicaltrainingv2.data.service.authorisation.AuthorisationFieldService;
import org.infai.ddls.medicaltrainingv2.data.service.location.TrainingSiteService;
import org.infai.ddls.medicaltrainingv2.data.service.training.TrainingSchemeSegmentService;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingSchemeSegment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

@ControllerAdvice
public class DefaultControllerAdvice {
    private class AuthorisationFieldEditorSupport extends PropertyEditorSupport {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
	    if (null == text || text.equals("")) {
		setValue(null);
	    } else {
		setValue(authorisationFieldService.get(UUID.fromString(text)));
	    }
	}
    }

    private class LocalDateEditorSupport extends PropertyEditorSupport {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
	    if (null == text || text.equals("")) {
		setValue(null);
	    } else {
		setValue(LocalDate.parse(text, DateTimeFormatter.ISO_DATE));
	    }
	}
    }

    private class TrainingSchemeSegmentEditorSupport extends PropertyEditorSupport {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
	    if (null == text || text.equals("")) {
		setValue(null);
	    } else {
		setValue(trainingSchemeSegmentService.get(UUID.fromString(text)));
	    }
	}
    }

    private class TrainingSiteEditorSupport extends PropertyEditorSupport {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
	    if (null == text || text.equals("")) {
		setValue(null);
	    } else {
		setValue(trainingSiteService.get(UUID.fromString(text)));
	    }
	}
    }

    @Autowired
    private TrainingSiteService trainingSiteService;
    @Autowired
    private AuthorisationFieldService authorisationFieldService;
    @Autowired
    private TrainingSchemeSegmentService trainingSchemeSegmentService;

    @Autowired
    private Validator validator;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
	binder.setAutoGrowCollectionLimit(500);
	binder.setValidator(validator);
	binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));

	binder.registerCustomEditor(LocalDate.class, new LocalDateEditorSupport());
	binder.registerCustomEditor(AbstractTrainingSite.class, new TrainingSiteEditorSupport());
	binder.registerCustomEditor(AuthorisationField.class, new AuthorisationFieldEditorSupport());
	binder.registerCustomEditor(TrainingSchemeSegment.class, new TrainingSchemeSegmentEditorSupport());
    }
}
