package org.infai.ddls.medicaltrainingv2.data.filter.person;

import org.infai.ddls.medicaltrainingv2.model.person.Registrar;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public @Data class RegistrarFilter extends PersonFilter<Registrar> {
    private String trainingPosition;
}
