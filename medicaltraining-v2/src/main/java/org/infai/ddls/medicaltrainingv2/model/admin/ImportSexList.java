package org.infai.ddls.medicaltrainingv2.model.admin;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.infai.ddls.medicaltrainingv2.model.core.Sex;

import de.caterdev.utils.parser.ParserEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImportSexList implements ImportList<Sex> {
    @Valid
    private List<ImportSexListEntity> entities = new ArrayList<>();

    public void setEntities(List<ParserEntity<Sex>> entities) {
	for (ParserEntity<Sex> entity : entities) {
	    this.entities.add(new ImportSexListEntity(entity));
	}
    }
}
