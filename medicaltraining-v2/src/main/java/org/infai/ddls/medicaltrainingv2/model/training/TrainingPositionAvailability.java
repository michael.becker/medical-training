package org.infai.ddls.medicaltrainingv2.model.training;

public enum TrainingPositionAvailability {
    BLOCKED, OCCUPIED, OVERLAPPING, AVAILABLE, UNKNOWN;
}
