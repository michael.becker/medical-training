package org.infai.ddls.medicaltrainingv2.model.person;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.location.Region;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlan;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlan_;

import lombok.Getter;
import lombok.Setter;

@Entity
@Audited
@Getter
@Setter
public class Registrar extends Doctor<Registrar> {
    private LocalDate kompasSubscription;
    @ColumnDefault("false")
    private boolean lateralEntry;
    @ManyToMany
    private List<Region> preferredRegions = new ArrayList<>();
    private LocalDate approvalVAHSTransmission;
    private LocalDate approvalKVSATransmission;
    @OneToMany(mappedBy = RotationPlan_.REGISTRAR)
    private List<RotationPlan> rotationPlans = new ArrayList<>();
}
