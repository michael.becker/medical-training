package org.infai.ddls.medicaltrainingv2.controller.person;

import lombok.extern.apachecommons.CommonsLog;
import org.infai.ddls.medicaltrainingv2.data.filter.person.ConversationRegistrarFilter;
import org.infai.ddls.medicaltrainingv2.data.filter.person.RegistrarFilter;
import org.infai.ddls.medicaltrainingv2.data.filter.training.RotationPlanSegmentFilter;
import org.infai.ddls.medicaltrainingv2.data.service.core.ConversationTypeService;
import org.infai.ddls.medicaltrainingv2.data.service.location.RegionService;
import org.infai.ddls.medicaltrainingv2.data.service.person.ConversationRegistrarService;
import org.infai.ddls.medicaltrainingv2.data.service.person.RegistrarService;
import org.infai.ddls.medicaltrainingv2.data.service.training.RotationPlanSegmentService;
import org.infai.ddls.medicaltrainingv2.model.person.ConversationRegistrar;
import org.infai.ddls.medicaltrainingv2.model.person.Registrar;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/persons/registrars")
@CommonsLog
public class RegistrarController extends AbstractPersonController<Registrar, RegistrarFilter, RegistrarService> {
    public static final String VIEW_REGISTRARS_EDIT = "registrars/editRegistrar";
    public static final String MAPPING = "/doctors/registrars";

    public static final String MAPPING_CREATE = MAPPING + "/create";
    public static final String MAPPING_EDIT = MAPPING + "/{id}/edit";
    public static final String MAPPING_UPDATE = MAPPING + "/update";
    public static final String MAPPING_DELETE = MAPPING + "/{id}";
    @Autowired
    private ConversationTypeService conversationTypeService;
    @Autowired
    private ConversationRegistrarService conversationService;
    @Autowired
    private RotationPlanSegmentService rotationPlanSegmentService;
    @Autowired
    private RegionService regionService;

    @PostMapping("/{registrarId}/conversations/create")
    public String createConversation(@PathVariable UUID registrarId, @ModelAttribute @Validated ConversationRegistrar conversation, BindingResult bindingResult, Model model, @RequestParam String saveAction) {
        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());

            ConversationRegistrarFilter filter = new ConversationRegistrarFilter();
            filter.setRegistrar(service.get(registrarId));

            model.addAttribute("conversationTypes", conversationTypeService.get());
            model.addAttribute("conversationRegistrar", conversation);
            model.addAttribute("conversations", conversationService.get(filter, null));

            return "/persons/editRegistrarConversation";
        } else {
            conversation = conversationService.save(conversation);
            return evaluateSaveAction(saveAction, "/persons/registrars/" + registrarId + "/conversations/" + conversation.getId() + "/edit", "/persons/registrars/list");
        }
    }

    @GetMapping("/{registrarId}/conversations/create")
    public String createConversation(@PathVariable UUID registrarId, Model model) {
        ConversationRegistrar conversation = new ConversationRegistrar();
        conversation.setConversationDate(LocalDate.now());
        conversation.setContact(service.get(registrarId));

        return editConversation(conversation, model);
    }

    @GetMapping("/{registrarId}/conversations/{conversationId}/edit")
    public String editConversation(@PathVariable UUID registrarId, @PathVariable UUID conversationId, Model model) {
        return editConversation(conversationService.get(conversationId), model);
    }

    @Override
    public String editEntity(@PathVariable UUID id, Model model) {
        return editEntity(getService().get(id, true), model);
    }

    @GetMapping("/{registrarId}/rotationplan")
    public String editRotationPlan(@PathVariable UUID registrarId, Model model) {
        Registrar registrar = getService().get(registrarId, false, true);

        if (registrar.getRotationPlans().isEmpty()) {
            return redirect("/training/rotationplans/createforregistrar/" + registrarId);
        } else {
            return redirect("/training/rotationplans/" + registrar.getRotationPlans().get(0).getId() + "/edit");
        }
    }

    @Override
    protected void additionalEditAttributes(Registrar entity, Model model) {
        super.additionalEditAttributes(entity, model);

        model.addAttribute("regions", regionService.get());
    }

    @Override
    protected void additionalListAttributes(List<Registrar> entities, Model model) {
        super.additionalListAttributes(entities, model);

        Map<UUID, RotationPlanSegment> currentSegments = new HashMap<>();
        LocalDate now = LocalDate.now();
        for (Registrar registrar : entities) {
            RotationPlanSegmentFilter filter = new RotationPlanSegmentFilter();
            filter.setRegistrar(registrar);
            filter.setReferenceDate(now);

            List<RotationPlanSegment> segments = rotationPlanSegmentService.get(filter, null);

            if (segments.size() >= 1) {
                currentSegments.put(registrar.getId(), segments.get(0));
            }
        }

        model.addAttribute("currentSegments", currentSegments);
    }

    @Override
    protected Registrar getEntity() {
        return new Registrar();
    }

    @Override
    protected RegistrarFilter getFilter() {
        return new RegistrarFilter();
    }

    @Override
    protected String mappingDefault() {
        return "/persons/registrars";
    }

    @Override
    protected String templateFolder() {
        return "/persons";
    }

    private String editConversation(ConversationRegistrar conversation, Model model) {
        ConversationRegistrarFilter filter = new ConversationRegistrarFilter();
        filter.setRegistrar(service.get(conversation.getContact().getId()));

        model.addAttribute("conversationRegistrar", conversation);
        model.addAttribute("conversationTypes", conversationTypeService.get());
        model.addAttribute("conversations", conversationService.get(filter, null));

        return "/persons/editRegistrarConversation";
    }
}
