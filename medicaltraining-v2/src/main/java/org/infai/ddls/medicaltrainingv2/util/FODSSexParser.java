package org.infai.ddls.medicaltrainingv2.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.infai.ddls.medicaltrainingv2.model.admin.ParserEntitySex;
import org.infai.ddls.medicaltrainingv2.model.core.Sex;

public class FODSSexParser extends AbstractFODSAuthorisedSheetParser<Sex, ParserEntitySex> {
    private Set<String> sexes = new HashSet<>();

    @Override
    protected ParserEntitySex parserEntity() {
	return new ParserEntitySex();
    }

    @Override
    protected Sex parseRowResult(List<String> row, int rowNumber) throws Exception {
	String sexName = row.get(COLUMN_DOCTOR1_SEX);

	if (null == sexName || sexes.contains(sexName)) {
	    skipRow();
	    return null;
	} else {
	    sexName = sexName.trim();
	    sexes.add(sexName);
	    Sex sex = new Sex();
	    sex.setName(sexName);

	    return sex;
	}
    }

}
