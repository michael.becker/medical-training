package org.infai.ddls.medicaltrainingv2.data.repo.location;

import org.infai.ddls.medicaltrainingv2.model.location.Clinic;
import org.springframework.stereotype.Repository;

@Repository
public interface ClinicRepository extends TrainingSiteRepository<Clinic> {

}
