package org.infai.ddls.medicaltrainingv2.model.core;

import javax.persistence.Entity;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.AbstractNamedTrainingAppEntity;

@Entity
@Audited
public class Title extends AbstractNamedTrainingAppEntity {
}
