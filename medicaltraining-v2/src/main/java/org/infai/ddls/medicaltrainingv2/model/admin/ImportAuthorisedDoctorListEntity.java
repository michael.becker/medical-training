package org.infai.ddls.medicaltrainingv2.model.admin;

import de.caterdev.utils.parser.ParserEntity;
import lombok.Getter;
import lombok.Setter;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;

import javax.validation.Valid;

@Getter
@Setter
public class ImportAuthorisedDoctorListEntity implements ImportListEntity<AuthorisedDoctor> {
    @Valid
    private ParserEntityAuthorisedDoctor originalEntity;
    private boolean doImport = true;
    private boolean doImportAuthorisation = true;
    private int iteration;

    public ImportAuthorisedDoctorListEntity() {
    }

    public ImportAuthorisedDoctorListEntity(ParserEntity<AuthorisedDoctor> originalEntity) {
        this(originalEntity, 1);
    }

    public ImportAuthorisedDoctorListEntity(ParserEntity<AuthorisedDoctor> originalEntity, int iteration) {
        setOriginalEntity(new ParserEntityAuthorisedDoctor(originalEntity));
        setIteration(iteration);
    }
}