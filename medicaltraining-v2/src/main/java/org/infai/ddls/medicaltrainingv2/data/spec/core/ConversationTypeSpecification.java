package org.infai.ddls.medicaltrainingv2.data.spec.core;

import org.infai.ddls.medicaltrainingv2.data.filter.core.ConversationTypeFilter;
import org.infai.ddls.medicaltrainingv2.data.spec.NamedTrainingAppEntitySpecification;
import org.infai.ddls.medicaltrainingv2.model.person.ConversationType;

@SuppressWarnings("serial")
public class ConversationTypeSpecification extends NamedTrainingAppEntitySpecification<ConversationType, ConversationTypeFilter> {

    public ConversationTypeSpecification(ConversationTypeFilter filter) {
	super(filter);
    }

}
