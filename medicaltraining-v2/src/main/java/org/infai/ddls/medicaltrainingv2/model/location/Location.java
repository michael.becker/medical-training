package org.infai.ddls.medicaltrainingv2.model.location;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.AbstractNamedTrainingAppEntity;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Audited
@Getter
@Setter
@ToString(callSuper = true)
public abstract class Location extends AbstractNamedTrainingAppEntity {
    @NotNull
    @Valid
    @Embedded
    @Column(nullable = false)
    @ToString.Exclude
    protected Address address = new Address();
    protected double latitude;
    protected double longitude;
}
