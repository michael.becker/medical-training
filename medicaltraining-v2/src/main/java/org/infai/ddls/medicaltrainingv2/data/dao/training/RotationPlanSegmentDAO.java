package org.infai.ddls.medicaltrainingv2.data.dao.training;

import org.infai.ddls.medicaltrainingv2.data.dao.AbstractTrainingAppDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.training.RotationPlanSegmentFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.training.RotationPlanSegmentRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.training.RotationPlanSegmentSpecification;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment_;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

@Component
public class RotationPlanSegmentDAO extends AbstractTrainingAppDAO<RotationPlanSegment, RotationPlanSegmentRepository, RotationPlanSegmentFilter, RotationPlanSegmentSpecification> {
    @Override
    public Sort getSort() {
	return super.getSort().and(Sort.by(Direction.ASC, RotationPlanSegment_.SEGMENT_BEGIN, RotationPlanSegment_.SEGMENT_END));
    }
}
