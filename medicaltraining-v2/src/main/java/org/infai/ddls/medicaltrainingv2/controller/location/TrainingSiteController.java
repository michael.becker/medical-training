package org.infai.ddls.medicaltrainingv2.controller.location;

import lombok.extern.apachecommons.CommonsLog;
import org.infai.ddls.medicaltrainingv2.controller.AbstractTrainingAppController;
import org.infai.ddls.medicaltrainingv2.controller.FilterAction;
import org.infai.ddls.medicaltrainingv2.controller.SaveAction;
import org.infai.ddls.medicaltrainingv2.data.filter.location.TrainingSiteFilter;
import org.infai.ddls.medicaltrainingv2.data.service.core.EquipmentService;
import org.infai.ddls.medicaltrainingv2.data.service.core.MedicalFieldService;
import org.infai.ddls.medicaltrainingv2.data.service.location.RegionService;
import org.infai.ddls.medicaltrainingv2.data.service.location.TrainingSiteService;
import org.infai.ddls.medicaltrainingv2.data.service.person.AuthorisedDoctorService;
import org.infai.ddls.medicaltrainingv2.model.core.TreatmentType;
import org.infai.ddls.medicaltrainingv2.model.location.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/locations/trainingsites")
@CommonsLog
public class TrainingSiteController extends AbstractTrainingAppController {
    @Autowired
    private TrainingSiteService service;
    @Autowired
    private EquipmentService equipmentService;
    @Autowired
    private MedicalFieldService medicalFieldService;
    @Autowired
    private AuthorisedDoctorService authorisedDoctorService;
    @Autowired
    private RegionService regionService;

    @GetMapping("/clinics/create")
    public String createClinic(Model model) {
        return editClinic(new Clinic(), model);
    }

    @GetMapping("/clinics/{clinicId}/departments/create")
    public String createDepartment(@PathVariable UUID clinicId, Model model) {
        Department department = new Department();
        Clinic clinic = service.getClinic(clinicId, false, false);
        department.setClinic(clinic);
        department.setAddress(clinic.getAddress());
        department.setContactInfo(clinic.getContactInfo());

        return editDepartment(department, model);
    }

    @GetMapping("/doctorsoffices/create")
    public String createDoctorsOffice(Model model) {
        return editDoctorsOffice(new DoctorsOffice(), model);
    }

    @GetMapping("/clinics/{clinicId}/edit")
    public String editClinic(@PathVariable UUID clinicId, Model model) {
        return editClinic(service.getClinic(clinicId, true, true), model);
    }

    @GetMapping("/clinics/{clinicId}/departments/{departmentId}/edit")
    public String editDepartment(@PathVariable UUID departmentId, Model model) {
        return editDepartment(service.getDepartment(departmentId, true, true), model);
    }

    @GetMapping("/doctorsoffices/{doctorsOfficeId}/edit")
    public String editDoctorsOffice(@PathVariable UUID doctorsOfficeId, Model model) {
        return editDoctorsOffice(service.getDoctorsOffice(doctorsOfficeId, true, true), model);
    }

    @PostMapping("/filter")
    public String filterTrainingSites(@ModelAttribute @Validated TrainingSiteFilter<AbstractTrainingSite> filter, BindingResult bindingResult, @RequestParam String filterAction, Model model, ServletRequest servletRequest) {
        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());

            return listTrainingSites(service.get(Pageable.unpaged()), filter, model);
        } else {
            switch (FilterAction.valueOf(filterAction)) {
                case filter:
                    return listTrainingSites(service.get(filter, null), filter, model);
                case reset:
                    return redirect("/locations/trainingsites/list");
                default:
                    throw new RuntimeException("FilterAction " + filterAction + " not supported!");
            }
        }
    }

    @GetMapping("/list")
    public String listTrainingSites(Model model, Pageable pageable) {
        return listTrainingSites(service.get(pageable), new TrainingSiteFilter<>(), model);
    }

    @PostMapping("/clinics/update")
    public String updateClinic(@ModelAttribute @Validated Clinic clinic, BindingResult bindingResult, @RequestParam String saveAction, Model model, ServletRequest servletRequest) {
        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());

            return editClinic(clinic, model);
        } else {
            clinic = service.save(clinic);

            switch (SaveAction.valueOf(saveAction)) {
                case save:
                    return redirect("/locations/trainingsites/clinics/" + clinic.getId() + "/edit");
                case saveAndClose:
                    return redirect("/locations/trainingsites/list");
                default:
                    throw new RuntimeException("SaveAction " + saveAction + " not supported!");
            }
        }
    }

    @PostMapping("/clinics/{clinicId}/departments/update")
    public String updateDepartment(@ModelAttribute @Validated Department department, BindingResult bindingResult, @PathVariable UUID clinicId, @RequestParam String saveAction, Model model) {
        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());

            department.setClinic(service.getClinic(clinicId, false, false));
            return editDepartment(department, model);
        } else {
            department.setClinic(service.getClinic(clinicId, false, false));
            department = service.save(department);

            switch (SaveAction.valueOf(saveAction)) {
                case save:
                    return redirect("/locations/trainingsites/clinics/" + department.getClinic().getId() + "/departments/" + department.getId() + "/edit");
                case saveAndClose:
                    return redirect("/locations/trainingsites/clinics/" + department.getClinic().getId() + "/edit");
                default:
                    throw new RuntimeException("SaveAction " + saveAction + " not supported!");

            }
        }
    }

    @PostMapping("/doctorsoffices/update")
    public String updateDoctorsOffice(@ModelAttribute("doctorsOffice") @Validated DoctorsOffice doctorsOffice, BindingResult bindingResult, @RequestParam String saveAction, Model model, ServletRequest servletRequest) {
        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());

            return editDoctorsOffice(doctorsOffice, model);
        } else {
            doctorsOffice = service.save(doctorsOffice);

            switch (SaveAction.valueOf(saveAction)) {
                case save:
                    return redirect("/locations/trainingsites/doctorsoffices/" + doctorsOffice.getId() + "/edit");
                case saveAndClose:
                    return redirect("/locations/trainingsites/list");
                default:
                    throw new RuntimeException("SaveAction " + saveAction + " not supported!");
            }

        }
    }

    private String editClinic(Clinic clinic, Model model) {
        model.addAttribute("clinic", clinic);
        model.addAttribute("departments", clinic.getDepartments());
        model.addAttribute("regions", regionService.get());

        return "/locations/editClinic";
    }

    private String editDepartment(Department department, Model model) {
        model.addAttribute("department", department);
        model.addAttribute("equipments", equipmentService.get());
        model.addAttribute("medicalFields", medicalFieldService.get());
        model.addAttribute("treatmentTypes", TreatmentType.values());
        model.addAttribute("regions", regionService.get());

        return "/locations/editDepartment";
    }

    private String editDoctorsOffice(DoctorsOffice doctorsOffice, Model model) {
        model.addAttribute("doctorsOffice", doctorsOffice);
        model.addAttribute("equipments", equipmentService.get());
        model.addAttribute("medicalFields", medicalFieldService.get());
        model.addAttribute("treatmentTypes", TreatmentType.values());
        model.addAttribute("regions", regionService.get());

        return "/locations/editDoctorsOffice";
    }

    private String listTrainingSites(List<AbstractTrainingSite> trainingSites, TrainingSiteFilter<AbstractTrainingSite> filter, Model model) {
        model.addAttribute("trainingSites", trainingSites);
        model.addAttribute("trainingSiteFilter", filter);
        model.addAttribute("regions", regionService.get());
        model.addAttribute("trainingSiteTypes", TrainingSiteType.parentTypes());

        return "/locations/listTrainingSites";
    }
}
