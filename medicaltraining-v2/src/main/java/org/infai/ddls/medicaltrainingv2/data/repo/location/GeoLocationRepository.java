package org.infai.ddls.medicaltrainingv2.data.repo.location;

import org.infai.ddls.medicaltrainingv2.model.location.GeoLocation;

public interface GeoLocationRepository extends LocationRepository<GeoLocation> {
}
