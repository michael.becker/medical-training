package org.infai.ddls.medicaltrainingv2.data.spec;

import org.infai.ddls.medicaltrainingv2.data.filter.TrainingAppEntityFilter;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity_;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public abstract class AbstractTrainingAppEntitySpecification<T extends AbstractTrainingAppEntity, U extends TrainingAppEntityFilter<T>> implements TrainingAppSpecification<T> {
    protected U filter;
    private List<Predicate> predicates;

    public AbstractTrainingAppEntitySpecification(U filter) {
        this.predicates = new ArrayList<>();
        this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        initPredicates(root, query, criteriaBuilder);

        if (null != filter && !filter.isAllowArchived()) {
            // wir müssen hier beide Prädikate prüfen, da je nach DB-Implementierung boolean
            // unterschiedlich abgebildet werden kann
            Predicate isFalse = criteriaBuilder.isFalse(root.get(AbstractTrainingAppEntity_.archived));
            Predicate isNull = criteriaBuilder.isNull(root.get(AbstractTrainingAppEntity_.archived));

            addPredicateDisjunction(criteriaBuilder, isFalse, isNull);
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }

    protected void addPredicate(Predicate predicate) {
        this.predicates.add(predicate);
    }

    protected void addPredicateConjunction(CriteriaBuilder criteriaBuilder, Predicate... predicates) {
        this.predicates.add(criteriaBuilder.and(predicates));
    }

    protected void addPredicateDisjunction(CriteriaBuilder criteriaBuilder, Predicate... predicates) {
        this.predicates.add(criteriaBuilder.or(predicates));
    }

    protected abstract void initPredicates(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder);

    protected Predicate likeIgnoreCase(String pattern, Path<String> path, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.like(criteriaBuilder.lower(path), "%" + pattern.toLowerCase() + "%");
    }
}