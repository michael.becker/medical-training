package org.infai.ddls.medicaltrainingv2.model.authorisation;

import javax.persistence.Entity;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;

import lombok.Getter;
import lombok.Setter;

@Entity
@Audited
@Getter
@Setter
public class AuthorisationField extends Authorisation<TrainingPositionField> {
}
