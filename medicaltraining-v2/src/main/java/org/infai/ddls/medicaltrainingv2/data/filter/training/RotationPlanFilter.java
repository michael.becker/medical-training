package org.infai.ddls.medicaltrainingv2.data.filter.training;

import org.infai.ddls.medicaltrainingv2.data.filter.TrainingAppEntityFilter;
import org.infai.ddls.medicaltrainingv2.model.person.Registrar;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlan;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingScheme;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public @Data class RotationPlanFilter extends TrainingAppEntityFilter<RotationPlan> {
    private Registrar registrar;
    private TrainingScheme trainingScheme;
}
