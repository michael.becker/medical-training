package org.infai.ddls.medicaltrainingv2.data.spec.location;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltrainingv2.data.filter.location.TrainingSiteFilter;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite_;

@SuppressWarnings("serial")
public abstract class TrainingSiteSpecification<T extends AbstractTrainingSite, U extends TrainingSiteFilter<T>> extends LocationSpecification<T, U> {
    public TrainingSiteSpecification(U filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (null != filter.getRegion()) {
	    addPredicate(criteriaBuilder.equal(root.get(AbstractTrainingSite_.region), filter.getRegion()));
	}
	if (null != filter.getTrainingSiteType()) {
	    addPredicate(criteriaBuilder.equal(root.get(AbstractTrainingSite_.trainingSiteType), filter.getTrainingSiteType()));
	}

    }
}
