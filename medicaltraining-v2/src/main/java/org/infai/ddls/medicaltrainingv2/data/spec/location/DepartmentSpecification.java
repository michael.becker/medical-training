package org.infai.ddls.medicaltrainingv2.data.spec.location;

import org.infai.ddls.medicaltrainingv2.data.filter.location.DepartmentFilter;
import org.infai.ddls.medicaltrainingv2.model.location.Department;

@SuppressWarnings("serial")
public class DepartmentSpecification extends TrainingSiteWithSpecification<Department, DepartmentFilter> {
    public DepartmentSpecification(DepartmentFilter filter) {
	super(filter);
    }

}
