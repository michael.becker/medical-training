package org.infai.ddls.medicaltrainingv2.data.spec.core;

import org.infai.ddls.medicaltrainingv2.data.filter.core.SexFilter;
import org.infai.ddls.medicaltrainingv2.data.spec.NamedTrainingAppEntitySpecification;
import org.infai.ddls.medicaltrainingv2.model.core.Sex;

@SuppressWarnings("serial")
public class SexSpecification extends NamedTrainingAppEntitySpecification<Sex, SexFilter> {
    public SexSpecification(SexFilter filter) {
	super(filter);
    }
}
