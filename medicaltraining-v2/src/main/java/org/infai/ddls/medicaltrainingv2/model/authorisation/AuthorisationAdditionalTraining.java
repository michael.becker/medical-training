package org.infai.ddls.medicaltrainingv2.model.authorisation;

import javax.persistence.Entity;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionAdditionalTraining;

import lombok.Getter;
import lombok.Setter;

@Entity
@Audited
@Getter
@Setter
public class AuthorisationAdditionalTraining extends Authorisation<TrainingPositionAdditionalTraining> {

}
