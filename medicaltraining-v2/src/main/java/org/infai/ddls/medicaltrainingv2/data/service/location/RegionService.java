package org.infai.ddls.medicaltrainingv2.data.service.location;

import java.util.List;
import java.util.UUID;

import org.infai.ddls.medicaltrainingv2.data.dao.location.RegionDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.location.RegionFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.spec.location.RegionSpecification;
import org.infai.ddls.medicaltrainingv2.model.location.Region;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class RegionService extends AbstractTrainingAppService<Region, RegionFilter> {
    @Autowired
    private RegionDAO dao;

    @Override
    public List<Region> get() {
	return get(new RegionFilter(), DEFAULT_PAGING);
    }

    @Override
    public List<Region> get(RegionFilter filter, Pageable pageable) {
	return dao.get(new RegionSpecification(filter), pageable);
    }

    @Override
    public Region get(UUID id) {
	return dao.get(id);
    }

    @Override
    public Region save(Region entity) {
	return dao.save(entity);
    }

}
