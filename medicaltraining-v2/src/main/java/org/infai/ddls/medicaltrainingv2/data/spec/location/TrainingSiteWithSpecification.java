package org.infai.ddls.medicaltrainingv2.data.spec.location;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltrainingv2.data.filter.location.TrainingSiteWithFilter;
import org.infai.ddls.medicaltrainingv2.data.spec.core.MedicalFieldSpecification;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSiteWith;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSiteWith_;

@SuppressWarnings("serial")
public abstract class TrainingSiteWithSpecification<T extends AbstractTrainingSiteWith, U extends TrainingSiteWithFilter<T>> extends TrainingSiteSpecification<T, U> {
    public TrainingSiteWithSpecification(U filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	super.initPredicates(root, query, criteriaBuilder);

	addPredicate(MedicalFieldSpecification.toPredicate(filter.getMedicalField(), root.get(AbstractTrainingSiteWith_.medicalField), query, criteriaBuilder));

//	addPredicate(LocationSpecification.toPredicate(filter, root.get(JPA_PARAMETER_NAME_LATITUDE), root.get(JPA_PARAMETER_NAME_LONGITUDE), query, criteriaBuilder));
//	addPredicate(allowDepartments(filter.isAllowDepartments(), root, query, criteriaBuilder));
//	addPredicate(allowParents(filter.isAllowParents(), root, query, criteriaBuilder));
    }
}
