package org.infai.ddls.medicaltrainingv2.model.location;

import javax.persistence.Entity;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.AbstractNamedTrainingAppEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Audited
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true, callSuper = true)
public class Region extends AbstractNamedTrainingAppEntity {
}