package org.infai.ddls.medicaltrainingv2.controller.training;

import java.util.UUID;

import javax.servlet.ServletRequest;

import org.infai.ddls.medicaltrainingv2.controller.AbstractTypedTrainingAppController;
import org.infai.ddls.medicaltrainingv2.controller.SaveAction;
import org.infai.ddls.medicaltrainingv2.data.filter.training.TrainingSchemeFilter;
import org.infai.ddls.medicaltrainingv2.data.service.core.MedicalFieldService;
import org.infai.ddls.medicaltrainingv2.data.service.training.TrainingSchemeSegmentService;
import org.infai.ddls.medicaltrainingv2.data.service.training.TrainingSchemeService;
import org.infai.ddls.medicaltrainingv2.model.core.TreatmentType;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingScheme;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingSchemeSegment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.extern.apachecommons.CommonsLog;

@Controller
@RequestMapping("/training/trainingschemes")
@CommonsLog
public class TrainingSchemeController extends AbstractTypedTrainingAppController<TrainingScheme, TrainingSchemeFilter, TrainingSchemeService> {
    @Autowired
    private TrainingSchemeSegmentService trainingSchemeSegmentService;
    @Autowired
    private MedicalFieldService medicalFieldService;

    @Override
    public String editEntity(@PathVariable UUID id, Model model) {
	return editEntity(getService().get(id, true), model);
    }

    @Override
    public String listEntities(Model model) {
	return listEntities(getService().get(new TrainingSchemeFilter(), Pageable.unpaged(), true), new TrainingSchemeFilter(), model);
    }

    @GetMapping("/{trainingSchemeId}/trainingschemesegments/create")
    public String trainingSchemeSegmentCreate(@PathVariable UUID trainingSchemeId, Model model) {
	if (!getService().exists(trainingSchemeId)) {
	    log.warn("TrainingScheme " + trainingSchemeId + " not persisted!");

	    TrainingScheme trainingScheme = new TrainingScheme();
	    trainingScheme.setId(trainingSchemeId);
	    return editEntity(trainingScheme, model);
	}
	TrainingSchemeSegment trainingSchemeSegment = new TrainingSchemeSegment();
	trainingSchemeSegment.setTrainingScheme(getService().get(trainingSchemeId));

	return editTrainingSchemeSegment(trainingSchemeSegment, model);
    }

    @GetMapping("/{trainingSchemeId}/trainingschemesegments/{trainingSchemeSegmentId}/edit")
    public String trainingSchemeSegmentEdit(@PathVariable UUID trainingSchemeId, @PathVariable UUID trainingSchemeSegmentId, Model model) {
	return editTrainingSchemeSegment(trainingSchemeSegmentService.get(trainingSchemeSegmentId), model);
    }

    @PostMapping("/{trainingSchemeId}/trainingschemesegments/update")
    public String trainingSchemeSegmentUpdat(@ModelAttribute @Validated TrainingSchemeSegment trainingSchemeSegment, BindingResult bindingResult, @RequestParam String saveAction, Model model, ServletRequest servletRequest) {
	if (bindingResult.hasErrors()) {
	    log.warn(bindingResult.getAllErrors());

	    return editTrainingSchemeSegment(trainingSchemeSegment, model);
	} else {

	    trainingSchemeSegment = trainingSchemeSegmentService.save(trainingSchemeSegment);

	    switch (SaveAction.valueOf(saveAction)) {
	    case save:
		return redirect("/training/trainingschemes/" + trainingSchemeSegment.getTrainingScheme().getId() + "/trainingschemesegments/" + trainingSchemeSegment.getId() + "/edit");
	    case saveAndClose:
		return redirect("/training/trainingschemes/" + trainingSchemeSegment.getTrainingScheme().getId() + "/edit");
	    default:
		throw new RuntimeException("SaveAction " + saveAction + " not supported!");
	    }

	}
    }

    @Override
    protected void additionalEditAttributes(TrainingScheme entity, Model model) {
	super.additionalEditAttributes(entity, model);

	model.addAttribute("treatmentTypes", TreatmentType.values());
    }

    @Override
    protected TrainingScheme getEntity() {
	return new TrainingScheme();
    }

    @Override
    protected TrainingSchemeFilter getFilter() {
	return new TrainingSchemeFilter();
    }

    @Override
    protected String mappingDefault() {
	return "/training/trainingschemes";
    }

    @Override
    protected String templateFolder() {
	return "/training";
    }

    private String editTrainingSchemeSegment(TrainingSchemeSegment trainingSchemeSegment, Model model) {
	model.addAttribute(trainingSchemeSegment.getViewNameSingleEntity(), trainingSchemeSegment);
	model.addAttribute("medicalFields", medicalFieldService.get());
	model.addAttribute("treatmentTypes", TreatmentType.values());

	return "/training/editTrainingSchemeSegment";
    }

}
