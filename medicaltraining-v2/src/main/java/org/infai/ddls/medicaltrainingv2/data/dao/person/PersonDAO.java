package org.infai.ddls.medicaltrainingv2.data.dao.person;

import org.infai.ddls.medicaltrainingv2.data.dao.AbstractTrainingAppDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.person.PersonFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.person.PersonRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.person.PersonSpecification;
import org.infai.ddls.medicaltrainingv2.model.person.Person;
import org.infai.ddls.medicaltrainingv2.model.person.Person_;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

public abstract class PersonDAO<T extends Person<T>, U extends PersonRepository<T>, V extends PersonFilter<T>, W extends PersonSpecification<T, V>> extends AbstractTrainingAppDAO<T, U, V, W> {
    @Override
    public Sort getSort() {
	return super.getSort().and(Sort.by(Direction.ASC, Person_.lastName.getName(), Person_.firstName.getName()));
    }
}
