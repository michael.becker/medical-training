package org.infai.ddls.medicaltrainingv2.data.service.core;

import java.util.List;
import java.util.UUID;

import org.infai.ddls.medicaltrainingv2.data.dao.core.EquipmentDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.core.EquipmentFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.spec.core.EquipmentSpecification;
import org.infai.ddls.medicaltrainingv2.model.core.Equipment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class EquipmentService extends AbstractTrainingAppService<Equipment, EquipmentFilter> {
    @Autowired
    private EquipmentDAO dao;

    @Override
    public List<Equipment> get() {
	return get(new EquipmentFilter(), DEFAULT_PAGING);
    }

    @Override
    public List<Equipment> get(EquipmentFilter filter, Pageable pageable) {
	return dao.get(new EquipmentSpecification(filter), pageable);
    }

    @Override
    public Equipment get(UUID id) {
	return dao.get(id);
    }

    @Override
    public Equipment save(Equipment entity) {
	return dao.save(entity);
    }

}
