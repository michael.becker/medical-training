package org.infai.ddls.medicaltrainingv2.model.training;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Audited
public @Getter @Setter class TrainingPositionExclusion extends AbstractTrainingAppEntity {
    @NotNull
    private LocalDate exclusionFrom;
    @NotNull
    private LocalDate exclusionUntil;
}
