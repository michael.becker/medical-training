package org.infai.ddls.medicaltrainingv2.data.service.training;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.infai.ddls.medicaltrainingv2.data.dao.training.TrainingSchemeDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.training.TrainingSchemeFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.spec.training.TrainingSchemeSpecification;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingScheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TrainingSchemeService extends AbstractTrainingAppService<TrainingScheme, TrainingSchemeFilter> {
    @Autowired
    private TrainingSchemeDAO dao;

    @Override
    @Transactional
    public List<TrainingScheme> get() {
	return get(new TrainingSchemeFilter(), Pageable.unpaged(), false);
    }

    @Override
    @Transactional
    public List<TrainingScheme> get(TrainingSchemeFilter filter, Pageable pageable) {
	return get(filter, pageable, false);
    }

    @Transactional
    public List<TrainingScheme> get(TrainingSchemeFilter filter, Pageable pageable, boolean loadTrainingSegments) {
	TrainingSchemeSpecification spec = new TrainingSchemeSpecification(filter);

	List<TrainingScheme> trainingSchemes = dao.get(spec, pageable);

	if (loadTrainingSegments) {
	    for (TrainingScheme trainingScheme : trainingSchemes) {
		trainingScheme.getTrainingSchemeSegments().size();
	    }
	}

	return trainingSchemes;
    }

    @Override
    public TrainingScheme get(UUID id) {
	return get(id, false);
    }

    @Transactional
    public TrainingScheme get(UUID id, boolean loadTrainingSegments) {
	TrainingScheme trainingScheme = dao.get(id);

	if (loadTrainingSegments) {
	    trainingScheme.getTrainingSchemeSegments().size();
	}

	return trainingScheme;
    }

    @Override
    public TrainingScheme save(TrainingScheme entity) {
	return dao.save(entity);
    }

}
