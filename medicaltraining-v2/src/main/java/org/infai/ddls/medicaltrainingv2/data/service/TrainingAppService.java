package org.infai.ddls.medicaltrainingv2.data.service;

import java.util.List;
import java.util.UUID;

import org.infai.ddls.medicaltrainingv2.data.filter.TrainingAppEntityFilter;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.springframework.data.domain.Pageable;

public interface TrainingAppService<T extends AbstractTrainingAppEntity, U extends TrainingAppEntityFilter<T>> {
    public Pageable DEFAULT_PAGING = Pageable.unpaged();

    public default boolean exists(UUID id) {
	return null != get(id);
    }

    public List<T> get();

    public List<T> get(U filter, Pageable pageable);

    public T get(UUID id);

    public T save(T entity);
}
