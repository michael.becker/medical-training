package org.infai.ddls.medicaltrainingv2.data.repo.location;

import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface TrainingSiteRepository<T extends AbstractTrainingSite> extends LocationRepository<T> {

}
