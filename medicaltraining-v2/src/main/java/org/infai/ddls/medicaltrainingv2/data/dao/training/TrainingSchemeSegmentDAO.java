package org.infai.ddls.medicaltrainingv2.data.dao.training;

import org.infai.ddls.medicaltrainingv2.data.dao.AbstractTrainingAppDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.training.TrainingSchemeSegmentFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.training.TrainingSchemeSegmentRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.training.TrainingSchemeSegmentSpecification;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingSchemeSegment;
import org.springframework.stereotype.Component;

@Component
public class TrainingSchemeSegmentDAO extends AbstractTrainingAppDAO<TrainingSchemeSegment, TrainingSchemeSegmentRepository, TrainingSchemeSegmentFilter, TrainingSchemeSegmentSpecification> {

}
