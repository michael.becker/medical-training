package org.infai.ddls.medicaltrainingv2.model.admin;

import org.infai.ddls.medicaltrainingv2.model.core.Title;

import de.caterdev.utils.parser.ParserEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImportTitleListEntity implements ImportListEntity<Title> {
    private ParserEntityTitle originalEntity;
    private boolean doImport = true;

    public ImportTitleListEntity() {
    }

    public ImportTitleListEntity(ParserEntity<Title> originalEntity) {
	setOriginalEntity(new ParserEntityTitle(originalEntity));
    }
}