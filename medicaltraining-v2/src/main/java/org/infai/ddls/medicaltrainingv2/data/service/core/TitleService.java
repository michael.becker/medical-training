package org.infai.ddls.medicaltrainingv2.data.service.core;

import java.util.List;
import java.util.UUID;

import org.infai.ddls.medicaltrainingv2.data.dao.core.TitleDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.core.TitleFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.spec.core.TitleSpecification;
import org.infai.ddls.medicaltrainingv2.model.core.Title;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TitleService extends AbstractTrainingAppService<Title, TitleFilter> {
    @Autowired
    private TitleDAO dao;

    @Override
    public List<Title> get() {
	return get(new TitleFilter(), DEFAULT_PAGING);
    }

    @Override
    public List<Title> get(TitleFilter filter, Pageable pageable) {
	return dao.get(new TitleSpecification(filter), pageable);
    }

    @Override
    public Title get(UUID id) {
	return dao.get(id);
    }

    @Override
    public Title save(Title entity) {
	return dao.save(entity);
    }

}
