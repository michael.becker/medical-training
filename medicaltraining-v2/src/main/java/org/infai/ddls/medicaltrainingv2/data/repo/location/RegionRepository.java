package org.infai.ddls.medicaltrainingv2.data.repo.location;

import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.model.location.Region;
import org.springframework.stereotype.Repository;

@Repository
public interface RegionRepository extends TrainingAppEntityRepository<Region> {

}
