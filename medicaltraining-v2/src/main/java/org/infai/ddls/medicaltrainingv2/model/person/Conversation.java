package org.infai.ddls.medicaltrainingv2.model.person;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Audited
@Getter
@Setter
public abstract class Conversation<T extends Person<?>> extends AbstractTrainingAppEntity {
    @NotNull
    @Column(nullable = false)
    private LocalDate conversationDate;
    @NotBlank
    @Column(nullable = false)
    private String contactPerson;
    @NotNull
    @ManyToOne(optional = false, targetEntity = Person.class)
    private T contact;
    @NotNull
    @ManyToOne(optional = false)
    private ConversationType conversationType;
}
