package org.infai.ddls.medicaltrainingv2.model.training.constraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment;

public class RotationPlanSegmentDurationValidator implements ConstraintValidator<RotationPlanSegmentMinimalDurationConstraint, RotationPlanSegment> {

    @Override
    public boolean isValid(RotationPlanSegment rotationPlanSegment, ConstraintValidatorContext context) {
	if (rotationPlanSegment.getNormalisedDuration() < 3) {
	    return false;
	} else {
	    return true;
	}
    }
}