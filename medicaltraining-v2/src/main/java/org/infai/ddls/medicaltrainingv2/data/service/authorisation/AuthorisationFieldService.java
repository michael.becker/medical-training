package org.infai.ddls.medicaltrainingv2.data.service.authorisation;

import org.infai.ddls.medicaltrainingv2.data.dao.authorisation.AuthorisationFieldDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.authorisation.AuthorisationFieldFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.service.location.TrainingSiteService;
import org.infai.ddls.medicaltrainingv2.data.spec.authorisation.AuthorisationFieldSpecification;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
public class AuthorisationFieldService extends AbstractTrainingAppService<AuthorisationField, AuthorisationFieldFilter> {
    @Autowired
    private AuthorisationFieldDAO dao;
    @Autowired
    private TrainingSiteService trainingSiteService;

    @Override
    public List<AuthorisationField> get() {
        return get(new AuthorisationFieldFilter(), Pageable.unpaged());
    }

    @Override
    @Transactional
    public List<AuthorisationField> get(AuthorisationFieldFilter filter, Pageable pageable) {
        List<AuthorisationField> authorisations = dao.get(new AuthorisationFieldSpecification(filter), pageable);

        return authorisations;
    }

    @Override
    public AuthorisationField get(UUID id) {
        return dao.get(id);
    }

    @Override
    @Transactional
    public AuthorisationField save(AuthorisationField entity) {
        entity.setTrainingSite(trainingSiteService.get(entity.getTrainingSite().getId(), entity.getTrainingSite().getTrainingSiteType(), false, true));

        if (!entity.getTrainingSite().getAuthorisedDoctors().contains(entity.getAuthorisedDoctor())) {
            entity.getTrainingSite().getAuthorisedDoctors().add(entity.getAuthorisedDoctor());
        }

        return dao.save(entity);
    }

}
