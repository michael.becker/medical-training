package org.infai.ddls.medicaltrainingv2.data.repo.training;

import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment;
import org.springframework.stereotype.Repository;

@Repository
public interface RotationPlanSegmentRepository extends TrainingAppEntityRepository<RotationPlanSegment> {

}
