package org.infai.ddls.medicaltrainingv2.data.filter.core;

import org.infai.ddls.medicaltrainingv2.data.filter.NamedTrainingAppEntityFilter;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MedicalFieldFilter extends NamedTrainingAppEntityFilter<MedicalField> {
    private MedicalField parent;
}
