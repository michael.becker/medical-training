package org.infai.ddls.medicaltrainingv2.data.spec.person;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltrainingv2.data.filter.person.PersonFilter;
import org.infai.ddls.medicaltrainingv2.data.spec.AbstractTrainingAppEntitySpecification;
import org.infai.ddls.medicaltrainingv2.model.location.Address_;
import org.infai.ddls.medicaltrainingv2.model.person.Person;
import org.infai.ddls.medicaltrainingv2.model.person.Person_;

@SuppressWarnings("serial")
public class PersonSpecification<T extends Person<?>, U extends PersonFilter<T>> extends AbstractTrainingAppEntitySpecification<T, U> {
    public PersonSpecification(U personFilter) {
	super(personFilter);
    }

    @Override
    protected void initPredicates(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (null != filter.getFirstName()) {
	    addPredicate(isLikeFirstName(filter.getFirstName(), root, criteriaBuilder));
	}
	if (null != filter.getLastName()) {
	    addPredicate(isLikeLastName(filter.getLastName(), root, criteriaBuilder));
	}
	if (null != filter.getFullName()) {
	    Predicate firstName = isLikeFirstName(filter.getFullName(), root, criteriaBuilder);
	    Predicate lastName = isLikeLastName(filter.getFullName(), root, criteriaBuilder);

	    addPredicateDisjunction(criteriaBuilder, firstName, lastName);
	}
	if (null != filter.getSex()) {
	    addPredicate(criteriaBuilder.equal(root.get(Person_.sex), filter.getSex()));
	}
	if (null != filter.getTitle()) {
	    addPredicate(criteriaBuilder.equal(root.get(Person_.title), filter.getTitle()));
	}
	if (null != filter.getAddressText()) {
	    Predicate street = isLikeStreet(filter.getAddressText(), root, criteriaBuilder);
	    Predicate zipCode = isLikeZipCode(filter.getAddressText(), root, criteriaBuilder);
	    Predicate city = isLikeCity(filter.getAddressText(), root, criteriaBuilder);

	    addPredicateDisjunction(criteriaBuilder, street, zipCode, city);
	}
    }

    private Predicate isLikeCity(String city, Root<T> root, CriteriaBuilder criteriaBuilder) {
	return likeIgnoreCase(city, root.get(Person_.address).get(Address_.city), criteriaBuilder);
    }

    private Predicate isLikeFirstName(String firstName, Root<T> root, CriteriaBuilder criteriaBuilder) {
	return likeIgnoreCase(firstName, root.get(Person_.firstName), criteriaBuilder);
    }

    private Predicate isLikeLastName(String lastName, Root<T> root, CriteriaBuilder criteriaBuilder) {
	return likeIgnoreCase(lastName, root.get(Person_.lastName), criteriaBuilder);
    }

    private Predicate isLikeStreet(String street, Root<T> root, CriteriaBuilder criteriaBuilder) {
	return likeIgnoreCase(street, root.get(Person_.address).get(Address_.street), criteriaBuilder);
    }

    private Predicate isLikeZipCode(String zipCode, Root<T> root, CriteriaBuilder criteriaBuilder) {
	return likeIgnoreCase(zipCode, root.get(Person_.address).get(Address_.zipCode), criteriaBuilder);
    }
}