package org.infai.ddls.medicaltrainingv2.controller.location;

import org.infai.ddls.medicaltrainingv2.controller.AbstractTypedTrainingAppController;
import org.infai.ddls.medicaltrainingv2.data.filter.location.LocationFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.model.location.Location;

public abstract class AbstractLocationController<T extends Location, U extends LocationFilter<T>, V extends AbstractTrainingAppService<T, U>> extends AbstractTypedTrainingAppController<T, U, V> {
}
