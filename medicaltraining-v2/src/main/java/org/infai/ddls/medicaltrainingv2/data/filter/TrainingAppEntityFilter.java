package org.infai.ddls.medicaltrainingv2.data.filter;

import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.thymeleaf.util.StringUtils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class TrainingAppEntityFilter<T extends AbstractTrainingAppEntity> {
    private boolean allowArchived = false;

    public String getViewName() {
	return StringUtils.unCapitalize(getClass().getSimpleName());
    }
}
