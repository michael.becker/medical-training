package org.infai.ddls.medicaltrainingv2.data.spec.person;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltrainingv2.data.filter.person.AuthorisedDoctorFilter;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor_;

@SuppressWarnings("serial")
public class AuthorisedDoctorSpecification extends PersonSpecification<AuthorisedDoctor, AuthorisedDoctorFilter> {

    public AuthorisedDoctorSpecification(AuthorisedDoctorFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<AuthorisedDoctor> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	addPredicate(new PersonSpecification<AuthorisedDoctor, AuthorisedDoctorFilter>(filter).toPredicate(root, query, criteriaBuilder));

	if (null != filter.getLanr()) {
	    addPredicate(criteriaBuilder.like(root.get(AuthorisedDoctor_.lanr).as(String.class), "%" + filter.getLanr() + "%"));
	}
    }

}
