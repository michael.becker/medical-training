package org.infai.ddls.medicaltrainingv2.model.training;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.apachecommons.CommonsLog;
import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.infai.ddls.medicaltrainingv2.model.authorisation.Authorisation;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Audited
@Getter
@Setter
@ToString(callSuper = true)
@CommonsLog
public abstract class TrainingPosition<T extends Authorisation<?>> extends AbstractTrainingAppEntity {
    public static final int DEFAULT_MAXIMAL_OVERLAP_DURATION = 2;

    @NotNull
    @Column(nullable = false)
    protected LocalDate availableFrom;
    protected LocalDate availableUntil;
    @Min(1)
    protected int capacity;
    protected boolean partTimeAvailable;
    @NotNull
    @OneToMany
    @Valid
    @ToString.Exclude
    protected List<TrainingPositionExclusion> exclusions = new ArrayList<>();
    @Transient
    @ToString.Exclude
    private TrainingPositionAvailability availability = TrainingPositionAvailability.UNKNOWN;
    @NotNull
    @ManyToOne(optional = false, targetEntity = Authorisation.class)
    private T authorisation;

    public void addExclusion(LocalDate from, LocalDate until) {
        TrainingPositionExclusion exclusion = new TrainingPositionExclusion();
        exclusion.setExclusionFrom(from);
        exclusion.setExclusionUntil(until);

        this.exclusions.add(exclusion);
    }

    /**
     * Ruft {@link #getAvailability(LocalDate, LocalDate, Collection, int)} mit dem Wert
     * von {@link #DEFAULT_MAXIMAL_OVERLAP_DURATION} auf.
     *
     * @param from                 Startdatum der Verfügbarkeitsprüfung
     * @param until                Enddatum der Verfügbarkeitsprüfung
     * @param rotationPlanSegments Weiterbildungsabschnitte, die bereits verplant
     *                             sind
     */
    @Transient
    public TrainingPositionAvailability getAvailability(LocalDate from, LocalDate until, Collection<RotationPlanSegment> rotationPlanSegments) {
        return getAvailability(from, until, rotationPlanSegments, DEFAULT_MAXIMAL_OVERLAP_DURATION);
    }

    /**
     * Ermittelt, ob diese Weiterbildungsstelle in einem bestimmten Zeitraum
     * verfügbar ist abhängig von den geplanten Weiterbildungsabschnitten.
     *
     * @param from                 Startdatum der Verfügbarkeitsprüfung
     * @param until                Enddatum der Verfügbarkeitsprüfung
     * @param rotationPlanSegments Weiterbildungsabschnitte, die bereits verplant
     *                             sind
     * @param maximalOverlap       maximale Anzahl an Monaten, die mit einem
     *                             bestehenden Weiterbildungsabschnitt überlappt
     *                             werden darf
     * @return <ul>
     * <li>{@link TrainingPositionAvailability#AVAILABLE}, wenn die
     * Weiterbildungsstelle im geplanten Zeitraum frei ist</li>
     * <li>{@link TrainingPositionAvailability#BLOCKED}, wenn die
     * Weiterbildungsstelle im geplanten Zeitraum geblockt ist</li>
     * <li>{@link TrainingPositionAvailability#OVERLAPPING}, wenn die
     * Weiterbildungsstelle im geplanten Zeitraum belegt ist aber sich die
     * Belegung maximal um die gegebene Anzahl an Monaten überlappen</li>
     * <li>{@link TrainingPositionAvailability#OCCUPIED}, wenn die
     * Weiterbildungsstelle im geplanten Zeitraum belegt ist und sich die
     * Belegung mehr als die gegebene Anzahl an Monaten überlappen</li>
     * </ul>
     */
    @Transient
    public TrainingPositionAvailability getAvailability(LocalDate from, LocalDate until, Collection<RotationPlanSegment> rotationPlanSegments, int maximalOverlap) {
        log.info("prüfe Verfügbarkeit der Position " + this);
        log.info("\tfrom: " + from);
        log.info("\tuntil: " + until);
        log.info("\tverplante Segmente: " + rotationPlanSegments);

        if (isBlocked(from, until)) {
            return TrainingPositionAvailability.BLOCKED;
        }

        // die bereits existierenden Weiterbildungsabschnitte im angegebenen Zeitraum
        List<RotationPlanSegment> existingSegments = new ArrayList<>(rotationPlanSegments.size());
        for (RotationPlanSegment rotationPlanSegment : rotationPlanSegments) {
            // wenn eine andere Weiterbildungsstelle referenziert wird, können wir den
            // entsprechende n Weiterbildungsabschnitt überspringen
            if (!rotationPlanSegment.getTrainingPosition().equals(this)) {
                continue;
            }
            LocalDate segmentBegin = rotationPlanSegment.getSegmentBegin();
            LocalDate segmentEnd = rotationPlanSegment.getSegmentEnd();

            if ((from.isBefore(segmentBegin) || from.isEqual(segmentBegin)) && (until.isAfter(segmentBegin) || until.isEqual(segmentBegin))) {
                existingSegments.add(rotationPlanSegment);
            } else if (from.isAfter(segmentBegin) && (from.isBefore(segmentEnd) || from.isEqual(segmentEnd))) {
                existingSegments.add(rotationPlanSegment);
            }
        }

        // wenn die Anzahl existierender Weiterbildungsabschnitte kleiner der Kapazität
        // ist, ist alles in Ordnung und die Stelle ist noch verfügbar
        if (existingSegments.size() < capacity) {
            return TrainingPositionAvailability.AVAILABLE;
        }

        for (RotationPlanSegment rotationPlanSegment : rotationPlanSegments) {
            LocalDate segmentBegin = rotationPlanSegment.getSegmentBegin();
            LocalDate segmentEnd = rotationPlanSegment.getSegmentEnd();

            if ((from.isBefore(segmentBegin) || from.isEqual(segmentBegin)) && (until.isAfter(segmentBegin) || until.isEqual(segmentBegin))) {
                int overlappingMonths = Period.between(segmentBegin, until.plusDays(1)).getMonths();

                if (overlappingMonths <= maximalOverlap) {
                    return TrainingPositionAvailability.OVERLAPPING;
                } else {
                    return TrainingPositionAvailability.OCCUPIED;
                }
            } else if (from.isAfter(segmentBegin) && (from.isBefore(segmentEnd) || from.isEqual(segmentEnd))) {
                int overlappingMonths = Period.between(from, segmentEnd.plusDays(1)).getMonths();

                if (overlappingMonths <= maximalOverlap) {
                    return TrainingPositionAvailability.OVERLAPPING;
                } else {
                    return TrainingPositionAvailability.OCCUPIED;
                }
            }
        }

        return TrainingPositionAvailability.AVAILABLE;
    }

    /**
     * Prüft, ob die Weiterbildungsstelle im angegebenen Zeitraum blockiert ist,
     * d.h. ob die Weiterbildungsstelle angegeben hat, dass zu dieser Zeit keine
     * freien Stellen existieren.
     *
     * @param from
     * @param until
     * @return
     */
    @Transient
    public boolean isBlocked(LocalDate from, LocalDate until) {
        if (0 == exclusions.size()) {
            return false;
        } else {
            for (TrainingPositionExclusion exclusion : exclusions) {
                if ((from.isBefore(exclusion.getExclusionFrom()) || from.isEqual(exclusion.getExclusionFrom())) && (until.isAfter(exclusion.getExclusionFrom()) || until.isEqual(exclusion.getExclusionFrom()))) {
                    return true;
                } else if (from.isAfter(exclusion.getExclusionFrom()) && (from.isBefore(exclusion.getExclusionUntil()) || from.isEqual(exclusion.getExclusionUntil()))) {
                    return true;
                }
            }

        }
        return false;
    }
}