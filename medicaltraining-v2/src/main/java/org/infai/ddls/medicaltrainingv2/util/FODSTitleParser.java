package org.infai.ddls.medicaltrainingv2.util;

import org.infai.ddls.medicaltrainingv2.model.admin.ParserEntityTitle;
import org.infai.ddls.medicaltrainingv2.model.core.Title;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FODSTitleParser extends AbstractFODSAuthorisedSheetParser<Title, ParserEntityTitle> {
    private Set<String> titles = new HashSet<>();

    @Override
    protected ParserEntityTitle parserEntity() {
        return new ParserEntityTitle();
    }

    @Override
    protected Title parseRowResult(List<String> row, int rowNumber) throws Exception {
        String titlename = row.get(COLUMN_DOCTOR1_TITLE) != null ? row.get(COLUMN_DOCTOR1_TITLE).trim() : null;

        if (null == titlename || titles.contains(titlename)) {
            skipRow();
            return null;
        } else {
            titles.add(titlename);
            Title title = new Title();
            title.setName(titlename);

            return title;
        }
    }
}
