package org.infai.ddls.medicaltrainingv2.model.location.constraint;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = AddressExistsValidator.class)
public @interface AddressExistsConstraint {
    Class<?>[] groups() default {};

    String message() default "{address.notfound}";

    Class<? extends Payload>[] payload() default {};
}
