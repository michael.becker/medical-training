package org.infai.ddls.medicaltrainingv2.data.dao.location;

import org.infai.ddls.medicaltrainingv2.data.filter.location.DoctorsOfficeFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.location.DoctorsOfficeRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.location.DoctorsOfficeSpecification;
import org.infai.ddls.medicaltrainingv2.model.location.DoctorsOffice;
import org.springframework.stereotype.Component;

@Component
public class DoctorsOfficeDAO extends TrainingSiteDAO<DoctorsOffice, DoctorsOfficeRepository, DoctorsOfficeFilter, DoctorsOfficeSpecification> {

}
