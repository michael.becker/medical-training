package org.infai.ddls.medicaltrainingv2.controller.core;

import java.util.List;

import org.infai.ddls.medicaltrainingv2.controller.AbstractTypedTrainingAppController;
import org.infai.ddls.medicaltrainingv2.data.filter.core.MedicalFieldFilter;
import org.infai.ddls.medicaltrainingv2.data.service.core.MedicalFieldService;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/coredata/medicalfields")
public class MedicalFieldController extends AbstractTypedTrainingAppController<MedicalField, MedicalFieldFilter, MedicalFieldService> {
    @Override
    protected void additionalEditAttributes(MedicalField entity, Model model) {
	super.additionalEditAttributes(entity, model);

	model.addAttribute("medicalFields", getService().get());
    }

    @Override
    protected void additionalListAttributes(List<MedicalField> entities, Model model) {
	model.addAttribute("allMedicalFields", getService().get());

    }

    @Override
    protected MedicalField getEntity() {
	return new MedicalField();
    }

    @Override
    protected MedicalFieldFilter getFilter() {
	return new MedicalFieldFilter();
    }

    @Override
    protected String mappingDefault() {
	return "/coredata/medicalfields";
    }

    @Override
    protected String templateFolder() {
	return "/coredata";
    }

}
