package org.infai.ddls.medicaltrainingv2.data.dao.location;

import org.infai.ddls.medicaltrainingv2.data.filter.location.DepartmentFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.location.DepartmentRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.location.DepartmentSpecification;
import org.infai.ddls.medicaltrainingv2.model.location.Department;
import org.springframework.stereotype.Component;

@Component
public class DepartmentDAO extends TrainingSiteDAO<Department, DepartmentRepository, DepartmentFilter, DepartmentSpecification> {

}
