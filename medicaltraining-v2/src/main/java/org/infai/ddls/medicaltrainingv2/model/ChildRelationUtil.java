package org.infai.ddls.medicaltrainingv2.model;

public class ChildRelationUtil {
    /**
     * Prüft ob das gegebene value-Element in einer Kind-Relation mit dem gegebenen
     * parent-Element steht.
     *
     * @param value  das Element, für das geprüft werden soll, ob es (indirektes)
     *               Kind von parent ist
     * @param parent das Elternelement
     * @return true, wenn value ein (indirekts) Kind von parent ist, ansonsten false
     */
    public static boolean isChildOf(HasChildren<? extends AbstractTrainingAppEntity> value, HasChildren<? extends AbstractTrainingAppEntity> parent) {
	if (value.equals(parent)) {
	    return false;
	} else if (null == value.getParent()) {
	    return false;
	} else if (value.getParent().equals(parent)) {
	    return true;
	} else {
	    return isChild(value.getParent(), parent);
	}
    }

    private static boolean isChild(HasChildren<? extends AbstractTrainingAppEntity> value, HasChildren<? extends AbstractTrainingAppEntity> parent) {
	if (null == value.getParent()) {
	    return false;
	} else if (value.getParent().equals(parent)) {
	    return true;
	} else {
	    return isChild(value.getParent(), parent);
	}
    }
}
