package org.infai.ddls.medicaltrainingv2.data.spec.location;

import org.infai.ddls.medicaltrainingv2.data.filter.location.ClinicFilter;
import org.infai.ddls.medicaltrainingv2.model.location.Clinic;

@SuppressWarnings("serial")
public class ClinicSpecification extends TrainingSiteSpecification<Clinic, ClinicFilter> {
    public ClinicSpecification(ClinicFilter filter) {
        super(filter);
    }
}
