package org.infai.ddls.medicaltrainingv2.data.filter.training;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.infai.ddls.medicaltrainingv2.data.filter.TrainingAppEntityFilter;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;
import org.infai.ddls.medicaltrainingv2.model.location.Region;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrainingPositionFieldFilter extends TrainingAppEntityFilter<TrainingPositionField> {
    private MedicalField medicalField;
    private boolean allowSubfields = true;
    private LocalDate availableFrom;
    private LocalDate availableUntil;
    private int minimalCapacity = 1;
    private AuthorisationField authorisationField;
    private AuthorisedDoctor authorisedDoctor;
    private AbstractTrainingSite trainingSite;
    private Region region;
    @Min(1)
    @Max(100)
    private int fullTimeEquivalent = 100;
    private boolean partTimeAvailable = false;
    private List<Region> regions = new ArrayList<>();
}
