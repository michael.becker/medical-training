package org.infai.ddls.medicaltrainingv2.data.dao.core;

import org.infai.ddls.medicaltrainingv2.data.dao.AbstractNamedTrainingAppDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.core.TitleFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.core.TitleRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.core.TitleSpecification;
import org.infai.ddls.medicaltrainingv2.model.core.Title;
import org.springframework.stereotype.Component;

@Component
public class TitleDAO extends AbstractNamedTrainingAppDAO<Title, TitleRepository, TitleFilter, TitleSpecification> {
}
