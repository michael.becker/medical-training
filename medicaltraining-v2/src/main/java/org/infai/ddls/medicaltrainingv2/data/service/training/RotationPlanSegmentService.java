package org.infai.ddls.medicaltrainingv2.data.service.training;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.infai.ddls.medicaltrainingv2.data.dao.training.RotationPlanSegmentDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.training.RotationPlanSegmentFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.spec.training.RotationPlanSegmentSpecification;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class RotationPlanSegmentService extends AbstractTrainingAppService<RotationPlanSegment, RotationPlanSegmentFilter> {
    @Autowired
    private RotationPlanSegmentDAO dao;

    @Override
    public List<RotationPlanSegment> get() {
	return get(new RotationPlanSegmentFilter(), null);
    }

    @Override
    public List<RotationPlanSegment> get(RotationPlanSegmentFilter filter, Pageable pageable) {
	return dao.get(new RotationPlanSegmentSpecification(filter), pageable);
    }

    @Override
    public RotationPlanSegment get(UUID id) {
	return get(id, false);
    }

    @Transactional
    public RotationPlanSegment get(UUID id, boolean loadTrainingSchemeSegments) {
	RotationPlanSegment segment = dao.get(id);

	if (loadTrainingSchemeSegments) {
	    segment.getRotationPlan().getTrainingScheme().getTrainingSchemeSegments().size();
	}

	return segment;
    }

    @Override
    public RotationPlanSegment save(RotationPlanSegment entity) {
	return dao.save(entity);
    }

}
