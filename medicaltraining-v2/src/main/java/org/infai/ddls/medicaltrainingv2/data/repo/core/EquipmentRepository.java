package org.infai.ddls.medicaltrainingv2.data.repo.core;

import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.model.core.Equipment;
import org.springframework.stereotype.Repository;

@Repository
public interface EquipmentRepository extends TrainingAppEntityRepository<Equipment> {

}
