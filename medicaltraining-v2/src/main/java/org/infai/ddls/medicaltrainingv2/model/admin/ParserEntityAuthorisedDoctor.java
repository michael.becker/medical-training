package org.infai.ddls.medicaltrainingv2.model.admin;

import de.caterdev.utils.parser.ParserEntity;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;

public class ParserEntityAuthorisedDoctor extends ParserEntity<AuthorisedDoctor> {
    public ParserEntityAuthorisedDoctor() {
    }

    public ParserEntityAuthorisedDoctor(ParserEntity<AuthorisedDoctor> parserEntity) {
        setColumns(parserEntity.getColumns());
        setEntity(parserEntity.getEntity());
        setRowNumber(parserEntity.getRowNumber());
    }

}
