package org.infai.ddls.medicaltrainingv2.data.filter.location;

import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;
import org.infai.ddls.medicaltrainingv2.model.location.Clinic;

public class ClinicFilter extends TrainingSiteFilter<Clinic> {
    public static ClinicFilter create(TrainingSiteFilter<AbstractTrainingSite> originalFilter) {
	ClinicFilter filter = new ClinicFilter();
	filter.setAllowArchived(originalFilter.isAllowArchived());
	filter.setDistance(originalFilter.getDistance());
	filter.setLatitude(originalFilter.getLatitude());
	filter.setLongitude(originalFilter.getLongitude());
	filter.setRegion(originalFilter.getRegion());
	filter.setTrainingSiteType(originalFilter.getTrainingSiteType());
	filter.setZipCode(originalFilter.getZipCode());

	return filter;
    }
}
