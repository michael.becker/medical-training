package org.infai.ddls.medicaltrainingv2.model.person;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.infai.ddls.medicaltrainingv2.model.core.Sex;
import org.infai.ddls.medicaltrainingv2.model.core.Title;
import org.infai.ddls.medicaltrainingv2.model.location.Address;

import lombok.Getter;
import lombok.Setter;

@Entity
@Audited
@Getter
@Setter
public abstract class Person<T extends Person<?>> extends AbstractTrainingAppEntity {
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @Past
    private LocalDate birthDate;
    @NotNull
    @Valid
    @Embedded
    @Column(nullable = false)
    private Address address = new Address();
    @Valid
    @Embedded
    private ContactInfo contactInfo = new ContactInfo();
    @Valid
    @ManyToOne
    private Sex sex;
    @Valid
    @ManyToOne
    private Title title;
    @Valid
    @OneToMany(mappedBy = Conversation_.CONTACT, targetEntity = Conversation.class)
    private List<Conversation<T>> conversations = new ArrayList<>();

    @Transient
    public String getFullAppellation() {
	StringBuilder builder = new StringBuilder();

	if (null != sex) {
	    builder.append(sex.getDefaultPrefix()).append(" ");
	}
	if (null != title) {
	    builder.append(title.getName()).append(" ");
	}

	builder.append(getFullName());

	return builder.toString();
    }

    @Transient
    public String getFullName() {
	return firstName + " " + lastName;
    }
}
