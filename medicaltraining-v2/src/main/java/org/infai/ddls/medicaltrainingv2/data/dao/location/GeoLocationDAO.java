package org.infai.ddls.medicaltrainingv2.data.dao.location;

import org.infai.ddls.medicaltrainingv2.data.filter.location.GeoLocationFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.location.GeoLocationRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.location.GeoLocationSpecification;
import org.infai.ddls.medicaltrainingv2.model.location.GeoLocation;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@Component
public class GeoLocationDAO extends LocationDAO<GeoLocation, GeoLocationRepository, GeoLocationFilter, GeoLocationSpecification> {
    @Override
    public Sort getSort() {
        return Sort.by(Sort.Direction.ASC, "address.zipCode", "address.city", "address.street", "address.housenumber");
    }
}
