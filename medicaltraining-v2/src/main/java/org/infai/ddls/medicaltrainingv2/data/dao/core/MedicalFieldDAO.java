package org.infai.ddls.medicaltrainingv2.data.dao.core;

import org.infai.ddls.medicaltrainingv2.data.dao.AbstractNamedTrainingAppDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.core.MedicalFieldFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.core.MedicalFieldRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.core.MedicalFieldSpecification;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.springframework.stereotype.Component;

@Component
public class MedicalFieldDAO extends AbstractNamedTrainingAppDAO<MedicalField, MedicalFieldRepository, MedicalFieldFilter, MedicalFieldSpecification> {

}
