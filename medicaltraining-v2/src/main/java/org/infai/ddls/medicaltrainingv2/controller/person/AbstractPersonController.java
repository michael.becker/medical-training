package org.infai.ddls.medicaltrainingv2.controller.person;

import org.infai.ddls.medicaltrainingv2.controller.AbstractTypedTrainingAppController;
import org.infai.ddls.medicaltrainingv2.data.filter.person.PersonFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.service.core.SexService;
import org.infai.ddls.medicaltrainingv2.data.service.core.TitleService;
import org.infai.ddls.medicaltrainingv2.model.person.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

public abstract class AbstractPersonController<T extends Person<?>, U extends PersonFilter<T>, V extends AbstractTrainingAppService<T, U>> extends AbstractTypedTrainingAppController<T, U, V> {
    @Autowired
    protected SexService sexService;
    @Autowired
    protected TitleService titleService;

    @Override
    protected void additionalEditAttributes(T entity, Model model) {
        super.additionalEditAttributes(entity, model);

        model.addAttribute("sexes", sexService.get());
        model.addAttribute("titles", titleService.get());
    }
}
