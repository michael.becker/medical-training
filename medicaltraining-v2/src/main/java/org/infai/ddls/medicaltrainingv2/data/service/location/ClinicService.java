package org.infai.ddls.medicaltrainingv2.data.service.location;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.infai.ddls.medicaltrainingv2.data.dao.location.CliniCDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.location.ClinicFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.spec.location.ClinicSpecification;
import org.infai.ddls.medicaltrainingv2.model.location.Clinic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ClinicService extends AbstractTrainingAppService<Clinic, ClinicFilter> {
    @Autowired
    private CliniCDAO dao;

    @Override
    public List<Clinic> get() {
	return get(new ClinicFilter(), DEFAULT_PAGING, false);
    }

    public List<Clinic> get(boolean loadDepartments) {
	return get(new ClinicFilter(), DEFAULT_PAGING, loadDepartments);
    }

    @Override
    public List<Clinic> get(ClinicFilter filter, Pageable pageable) {
	return get(filter, pageable, false);
    }

    @Transactional
    public List<Clinic> get(ClinicFilter filter, Pageable pageable, boolean loadDepartments) {
	List<Clinic> clinics = dao.get(new ClinicSpecification(filter), pageable);

	if (loadDepartments) {
	    for (Clinic clinic : clinics) {
		clinic.getDepartments().size();
	    }
	}

	return clinics;
    }

    @Override
    public Clinic get(UUID id) {
	return dao.get(id);
    }

    @Override
    public Clinic save(Clinic entity) {
	return dao.save(entity);
    }

}
