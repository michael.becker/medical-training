package org.infai.ddls.medicaltrainingv2.data.spec.location;

import org.apache.commons.codec.language.Soundex;
import org.infai.ddls.medicaltrainingv2.data.filter.location.GeoLocationFilter;
import org.infai.ddls.medicaltrainingv2.model.location.Address_;
import org.infai.ddls.medicaltrainingv2.model.location.GeoLocation;
import org.infai.ddls.medicaltrainingv2.model.location.GeoLocation_;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class GeoLocationSpecification extends LocationSpecification<GeoLocation, GeoLocationFilter> {
    public GeoLocationSpecification(GeoLocationFilter filter) {
        super(filter);
    }

    @Override
    protected void initPredicates(Root<GeoLocation> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        if (filter.isSubstringSearch()) {
            addPredicate(criteriaBuilder.like(criteriaBuilder.lower(root.get(GeoLocation_.ADDRESS).get(Address_.STREET)), filter.getStreet().toLowerCase() + "%"));
        }

        if (filter.isFuzzySearch()) {
            addPredicate(criteriaBuilder.equal(criteriaBuilder.function("soundex", String.class, root.get(GeoLocation_.ADDRESS).get(Address_.STREET)), new Soundex().soundex(filter.getStreet())));
        }
    }
}
