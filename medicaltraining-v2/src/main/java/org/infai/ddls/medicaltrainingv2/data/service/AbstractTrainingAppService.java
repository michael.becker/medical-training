package org.infai.ddls.medicaltrainingv2.data.service;

import org.infai.ddls.medicaltrainingv2.data.filter.TrainingAppEntityFilter;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;

public abstract class AbstractTrainingAppService<T extends AbstractTrainingAppEntity, U extends TrainingAppEntityFilter<T>> implements TrainingAppService<T, U> {
}
