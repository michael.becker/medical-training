package org.infai.ddls.medicaltrainingv2.controller.core;

import org.infai.ddls.medicaltrainingv2.controller.AbstractTypedTrainingAppController;
import org.infai.ddls.medicaltrainingv2.data.filter.core.SexFilter;
import org.infai.ddls.medicaltrainingv2.data.service.core.SexService;
import org.infai.ddls.medicaltrainingv2.model.core.Sex;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/coredata/sexes")
public class SexController extends AbstractTypedTrainingAppController<Sex, SexFilter, SexService> {
    @Override
    protected Sex getEntity() {
	return new Sex();
    }

    @Override
    protected SexFilter getFilter() {
	return new SexFilter();
    }

    @Override
    protected String mappingDefault() {
	return "/coredata/sexes";
    }

    @Override
    protected String templateFolder() {
	return "/coredata";
    }

}
