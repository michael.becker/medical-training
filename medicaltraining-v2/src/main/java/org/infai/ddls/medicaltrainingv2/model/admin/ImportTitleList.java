package org.infai.ddls.medicaltrainingv2.model.admin;

import java.util.ArrayList;
import java.util.List;

import org.infai.ddls.medicaltrainingv2.model.core.Title;

import de.caterdev.utils.parser.ParserEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImportTitleList implements ImportList<Title> {
    private List<ImportTitleListEntity> entities = new ArrayList<>();

    public void setTitles(List<ParserEntity<Title>> entities) {
	for (ParserEntity<Title> entity : entities) {
	    this.entities.add(new ImportTitleListEntity(entity));
	}
    }
}
