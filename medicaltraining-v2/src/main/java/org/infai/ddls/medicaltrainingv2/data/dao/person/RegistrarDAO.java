package org.infai.ddls.medicaltrainingv2.data.dao.person;

import org.infai.ddls.medicaltrainingv2.data.filter.person.RegistrarFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.person.RegistrarRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.person.RegistrarSpecification;
import org.infai.ddls.medicaltrainingv2.model.person.Registrar;
import org.springframework.stereotype.Component;

@Component
public class RegistrarDAO extends PersonDAO<Registrar, RegistrarRepository, RegistrarFilter, RegistrarSpecification> {

}
