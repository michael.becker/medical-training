package org.infai.ddls.medicaltrainingv2.data.spec.person;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltrainingv2.data.filter.person.RegistrarFilter;
import org.infai.ddls.medicaltrainingv2.model.person.Registrar;

@SuppressWarnings("serial")
public class RegistrarSpecification extends PersonSpecification<Registrar, RegistrarFilter> {
    public RegistrarSpecification(RegistrarFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<Registrar> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	addPredicate(new PersonSpecification<Registrar, RegistrarFilter>(filter).toPredicate(root, query, criteriaBuilder));

	if (null != filter.getTrainingPosition()) {
	}

    }
}
