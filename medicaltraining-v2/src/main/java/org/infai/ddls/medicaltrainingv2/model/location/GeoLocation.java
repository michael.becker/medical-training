package org.infai.ddls.medicaltrainingv2.model.location;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

@Entity
@Getter
@Setter
public class GeoLocation extends Location {
}
