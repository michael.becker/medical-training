package org.infai.ddls.medicaltrainingv2.data.spec.person;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltrainingv2.data.filter.person.ConversationRegistrarFilter;
import org.infai.ddls.medicaltrainingv2.data.spec.AbstractTrainingAppEntitySpecification;
import org.infai.ddls.medicaltrainingv2.model.person.ConversationRegistrar;
import org.infai.ddls.medicaltrainingv2.model.person.ConversationRegistrar_;

@SuppressWarnings("serial")
public class ConversationRegistrarSpecification extends AbstractTrainingAppEntitySpecification<ConversationRegistrar, ConversationRegistrarFilter> {

    public ConversationRegistrarSpecification(ConversationRegistrarFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<ConversationRegistrar> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (null != filter.getRegistrar()) {
	    addPredicate(criteriaBuilder.equal(root.get(ConversationRegistrar_.contact), filter.getRegistrar()));
	}
    }

}
