package org.infai.ddls.medicaltrainingv2.config;

import lombok.extern.apachecommons.CommonsLog;
import org.infai.ddls.medicaltrainingv2.data.service.security.MedicalTrainingUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@CommonsLog
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    public static final String EL_HAS_ROLE_ADMIN = "hasRole('ROLE_ADMIN')";
    public static final String EL_HAS_ROLE_MANAGER = "hasRole('ROLE_MANAGER')";
    public static final String EL_HAS_ROLE_KVSA = "hasRole('ROLE_KVSA')";
    public static final String EL_HAS_ROLE_USER = "hasRole('ROLE_USER')";

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private DataSource dataSource;
    @Value("${spring.queries.users-query}")
    private String usersQuery;
    @Value("${spring.queries.roles-query}")
    private String rolesQuery;
    @Autowired
    private MedicalTrainingUserDetailsService userService;

    @Value("${security.type}")
    private String securityType;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        if (securityType.equals("db")) {
            log.info("using db security");
            auth.jdbcAuthentication().dataSource(dataSource).usersByUsernameQuery(usersQuery).authoritiesByUsernameQuery(rolesQuery).passwordEncoder(passwordEncoder);
        } else if (securityType.equals("file")) {
            log.info("using file security");
            auth.userDetailsService(userService);
        } else {
            throw new RuntimeException("security.type must be one of file or db");
        }
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().anyRequest().authenticated().and().formLogin().permitAll().and().csrf().disable();
    }
}
