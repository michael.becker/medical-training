package org.infai.ddls.medicaltrainingv2.data.filter.person;

import org.infai.ddls.medicaltrainingv2.data.filter.TrainingAppEntityFilter;
import org.infai.ddls.medicaltrainingv2.model.person.ConversationRegistrar;
import org.infai.ddls.medicaltrainingv2.model.person.Registrar;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConversationRegistrarFilter extends TrainingAppEntityFilter<ConversationRegistrar> {
    private Registrar registrar;
}
