package org.infai.ddls.medicaltrainingv2.data.dao.training;

import org.infai.ddls.medicaltrainingv2.data.dao.AbstractTrainingAppDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.training.RotationPlanFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.training.RotationPlanRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.training.RotationPlanSpecification;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlan;
import org.springframework.stereotype.Component;

@Component
public class RotationPlanDAO extends AbstractTrainingAppDAO<RotationPlan, RotationPlanRepository, RotationPlanFilter, RotationPlanSpecification> {

}
