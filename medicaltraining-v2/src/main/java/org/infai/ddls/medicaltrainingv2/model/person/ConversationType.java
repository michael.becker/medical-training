package org.infai.ddls.medicaltrainingv2.model.person;

import javax.persistence.Entity;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.AbstractNamedTrainingAppEntity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Audited
@Getter
@Setter
public class ConversationType extends AbstractNamedTrainingAppEntity {

}
