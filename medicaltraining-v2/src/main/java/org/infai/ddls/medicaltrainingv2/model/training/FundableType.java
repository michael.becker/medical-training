package org.infai.ddls.medicaltrainingv2.model.training;

public enum FundableType {
    Fundable, NotFundable, PartiallyFundable, Unknown;
}
