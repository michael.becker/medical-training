package org.infai.ddls.medicaltrainingv2;

import lombok.extern.apachecommons.CommonsLog;
import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.data.repo.admin.FileUploadRepository;
import org.infai.ddls.medicaltrainingv2.data.repo.security.TrainingAppRoleRepository;
import org.infai.ddls.medicaltrainingv2.data.repo.security.TrainingAppUserRepository;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.infai.ddls.medicaltrainingv2.model.security.TrainingAppRole;
import org.infai.ddls.medicaltrainingv2.model.security.TrainingAppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.context.request.RequestContextListener;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = TrainingAppEntityRepository.class)
@EntityScan(basePackageClasses = AbstractTrainingAppEntity.class)
@CommonsLog
public class MedicaltrainingV2Application extends SpringBootServletInitializer {
    @Autowired
    private TrainingAppUserRepository userRepository;
    @Autowired
    private TrainingAppRoleRepository roleRepository;
    @Autowired
    private FileUploadRepository fileUploadRepository;

    public static void main(String[] args) {
        SpringApplication.run(MedicaltrainingV2Application.class, args);
    }

    @EventListener(ContextRefreshedEvent.class)
    public void contextRefreshedEvent() {
        log.info("Starting MedicalTrainingApp...");
        fileUploadRepository.deleteAll();

//	initialiseRolesAndUsers();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(MedicaltrainingV2Application.class);
    }

    private void initialiseRolesAndUsers() {
        log.info("clearing roles and users");
        userRepository.deleteAll();
        roleRepository.deleteAll();

        log.info("initialising roles...");
        TrainingAppRole admin = new TrainingAppRole();
        admin.setName("ROLE_ADMIN");
        TrainingAppRole manager = new TrainingAppRole();
        manager.setName("ROLE_MANAGER");
        TrainingAppRole kvsa = new TrainingAppRole();
        kvsa.setName("ROLE_KVSA");
        TrainingAppRole user = new TrainingAppRole();
        user.setName("ROLE_USER");

        admin = roleRepository.save(admin);
        manager = roleRepository.save(manager);
        kvsa = roleRepository.save(kvsa);
        user = roleRepository.save(user);

        log.info("initialising users...");
        TrainingAppUser adminUser = new TrainingAppUser();
        adminUser.setUsername("admin");
        adminUser.setPassword(passwordEncoder().encode("admin"));
        adminUser.setActive(true);
        adminUser.getRoles().add(admin);

        TrainingAppUser managerUser = new TrainingAppUser();
        managerUser.setUsername("manager");
        managerUser.setPassword(passwordEncoder().encode("manager"));
        managerUser.setActive(true);
        managerUser.getRoles().add(manager);

        TrainingAppUser kvsaUser = new TrainingAppUser();
        kvsaUser.setUsername("kvsa");
        kvsaUser.setPassword(passwordEncoder().encode("kvsa"));
        kvsaUser.setActive(true);
        kvsaUser.getRoles().add(kvsa);

        TrainingAppUser userUser = new TrainingAppUser();
        userUser.setUsername("user");
        userUser.setPassword(passwordEncoder().encode("user"));
        userUser.setActive(true);
        userUser.getRoles().add(user);

        userRepository.save(adminUser);
        userRepository.save(managerUser);
        userRepository.save(kvsaUser);
        userRepository.save(userUser);
    }
}
