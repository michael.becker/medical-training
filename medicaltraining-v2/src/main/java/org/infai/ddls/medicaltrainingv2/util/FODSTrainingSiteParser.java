package org.infai.ddls.medicaltrainingv2.util;

import de.caterdev.utils.parser.ParserEntity;
import org.infai.ddls.medicaltrainingv2.model.admin.ParserEntityDoctorsOffice;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.infai.ddls.medicaltrainingv2.model.core.TreatmentType;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;
import org.infai.ddls.medicaltrainingv2.model.location.Clinic;
import org.infai.ddls.medicaltrainingv2.model.location.DoctorsOffice;
import org.infai.ddls.medicaltrainingv2.model.location.Region;

import java.util.*;

public class FODSTrainingSiteParser extends AbstractFODSAuthorisedSheetParser<DoctorsOffice, ParserEntityDoctorsOffice> {
    private static final String TYPE_SINGLE_PRAXIS = "Einzelpraxis";

    private Set<String> trainingSiteNames = new HashSet<>();
    private Map<Integer, AbstractTrainingSite> duplicateTrainingSites = new HashMap<>();
    private Map<Integer, AbstractTrainingSite> skippedTrainingSites = new HashMap<>();
    private Map<String, Region> regions = new HashMap<>();
    private Map<String, MedicalField> medicalFields = new HashMap<>();
    private List<ParserEntity<DoctorsOffice>> doctorsOffices = new ArrayList<>();

    public FODSTrainingSiteParser(Collection<Region> regions, Collection<MedicalField> medicalFields) {
        for (Region region : regions) {
            this.regions.put(region.getName(), region);
        }
        for (MedicalField medicalField : medicalFields) {
            this.medicalFields.put(medicalField.getName(), medicalField);
        }
    }

    public List<ParserEntity<DoctorsOffice>> getDoctorsOffices() {
        return doctorsOffices;
    }

    public Map<Integer, AbstractTrainingSite> getDuplicateTrainingSites() {
        return duplicateTrainingSites;
    }

    public Map<Integer, AbstractTrainingSite> getSkippedTrainingSites() {
        return skippedTrainingSites;
    }

    @Override
    protected ParserEntityDoctorsOffice parserEntity() {
        return new ParserEntityDoctorsOffice();
    }

    @Override
    protected DoctorsOffice parseRowResult(List<String> rowResult, int rowNumber) throws Exception {
        String doctorFirstname = rowResult.get(COLUMN_DOCTOR1_FIRSTNAME);
        String doctorName = rowResult.get(COLUMN_DOCTOR1_LASTNAME);
        String trainingSiteCity = rowResult.get(COLUMN_TRAININGSITE_CITY);
        String trainingSitestreetAndHousenumber = rowResult.get(COLUMN_TRAININGSITE_STREET).trim();
        String trainingSiteZIP = rowResult.get(COLUMN_TRAININGSITE_ZIP);

        boolean skip = false;

        if (null == doctorFirstname) {
            System.out.println("skipped #" + rowNumber + " doctorFirstName");
            skip = true;
        }
        if (null == doctorName) {
            System.out.println("skipped #" + rowNumber + " doctorName");
            skip = true;
        }
        if (null == trainingSitestreetAndHousenumber) {
            System.out.println("skipped #" + rowNumber + ": trainingSiteStreet");
            skip = true;
        }
        if (null == trainingSiteZIP) {
            System.out.println("skipped #" + rowNumber + ": trainingSiteZIP");
            skip = true;
        }
        if (null == trainingSiteCity) {
            System.out.println("skipped #" + rowNumber + ": trainingSiteCity");
            skip = true;
        }

        if (skip) {
            skipRow();
            return null;
        }

        if (trainingSiteZIP.length() == 4) {
            trainingSiteZIP = "0" + trainingSiteZIP;
        } else if (trainingSiteZIP.startsWith("D-")) {
            trainingSiteZIP = trainingSiteZIP.substring(2);
        }

        // TODO: derzeit gibt es nicht die Möglichkeit, dass zwei Ärzte mit gleichem
        // Namen in einer Stadt eine Praxis haben
        String trainingSiteName = generateTrainingSiteName(rowResult);

        String trainingSiteStreet = trainingSitestreetAndHousenumber;
        String trainingSiteHousenumber = "n/a";
        int separatorStreetHousenumber = trainingSitestreetAndHousenumber.lastIndexOf(" ");
        if (separatorStreetHousenumber > 0) {
            trainingSiteStreet = trainingSitestreetAndHousenumber.substring(0, separatorStreetHousenumber).trim();
            trainingSiteHousenumber = trainingSitestreetAndHousenumber.substring(separatorStreetHousenumber).trim();
        } else {
            System.out.println(separatorStreetHousenumber);
        }

        AbstractTrainingSite trainingSite;

        if (null == rowResult.get(COLUMN_TRAININGSITE_TYPE) || rowResult.get(COLUMN_TRAININGSITE_TYPE).equals(TYPE_SINGLE_PRAXIS)) {
            trainingSite = new DoctorsOffice();
            // TODO: Behandlungsart und Fachgebiet noch anhand der Einträge bestimmen lassen
            ((DoctorsOffice) trainingSite).setTreatmentType(TreatmentType.Outpatient);
            ((DoctorsOffice) trainingSite).setMedicalField(createMedicalField(rowResult.get(COLUMN_TRAININGSITE_MEDICALFIELD)));
        } else {
            trainingSite = new Clinic();
            trainingSite.setName(trainingSiteName);
            trainingSite.setAddress(createAddress(trainingSiteStreet, trainingSiteHousenumber, trainingSiteZIP, trainingSiteCity));
            skippedTrainingSites.put(rowNumber + 1, trainingSite);
            skipRow();
            return null;
        }

        trainingSite.setName(trainingSiteName);
        trainingSite.setAddress(createAddress(trainingSiteStreet, trainingSiteHousenumber, trainingSiteZIP, trainingSiteCity));
        trainingSite.setContactInfo(createContactInfo(COLUMN_TRAININGSITE_EMAIL, COLUMN_TRAININGSITE_FAX, COLUMN_TRAININGSITE_PHONE, COLUMN_TRAININGSITE_MOBILE, COLUMN_TRAININGSITE_WEB, rowResult));
        trainingSite.setRegion(regions.get(rowResult.get(COLUMN_TRAININGSITE_REGION)));

        if (trainingSiteNames.contains(trainingSiteName)) {
            duplicateTrainingSites.put(rowNumber + 1, trainingSite);
            skipRow();
            return null;
        } else {
            ParserEntity<DoctorsOffice> entity = new ParserEntity<>();
            entity.setColumns(rowResult);
            entity.setEntity((DoctorsOffice) trainingSite);
            entity.setRowNumber(rowNumber);
            doctorsOffices.add(entity);
            trainingSiteNames.add(trainingSiteName);
        }
        return (DoctorsOffice) trainingSite;
    }

    private MedicalField createMedicalField(String rowContent) {
        for (String medicalFieldName : medicalFields.keySet()) {
            if (rowContent.contains(medicalFieldName)) {
                return medicalFields.get(medicalFieldName);
            }
        }

        return null;
    }

}
