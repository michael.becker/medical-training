package org.infai.ddls.medicaltrainingv2.data.spec.location;

import org.infai.ddls.medicaltrainingv2.data.filter.location.LocationFilter;
import org.infai.ddls.medicaltrainingv2.data.spec.AbstractTrainingAppEntitySpecification;
import org.infai.ddls.medicaltrainingv2.model.location.Location;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public abstract class LocationSpecification<T extends Location, U extends LocationFilter<T>> extends AbstractTrainingAppEntitySpecification<T, U> {
    public static final String JPA_PARAMETER_NAME_LATITUDE = "latitude";
    public static final String JPA_PARAMETER_NAME_LONGITUDE = "longitude";
    public static final String SQL_FUNCTION_NAME_DISTANCE_CALC = "lat_lng_distance";
    public static final String ATTRIBUTE_LATITUDE = "latitude";
    public static final String ATTRIBUTE_LONGITUDE = "longitude";

    public LocationSpecification(U filter) {
        super(filter);
    }

    public static <U extends Location> Predicate toPredicate(LocationFilter<U> filter, Path<U> latitudePath, Path<U> longitudePath, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        if (!filter.getZipCode().equals(LocationFilter.DEFAULT_ZIP_CODE)) {
            ParameterExpression<Double> latitude = criteriaBuilder.parameter(Double.class, JPA_PARAMETER_NAME_LATITUDE);
            ParameterExpression<Double> longitude = criteriaBuilder.parameter(Double.class, JPA_PARAMETER_NAME_LONGITUDE);

            predicates.add(criteriaBuilder.lessThanOrEqualTo(criteriaBuilder.function(SQL_FUNCTION_NAME_DISTANCE_CALC, Integer.class, latitudePath, longitudePath, latitude, longitude), filter.getDistance()));
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
