package org.infai.ddls.medicaltrainingv2.data.repo.location;

import org.infai.ddls.medicaltrainingv2.model.location.Department;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends TrainingSiteRepository<Department> {

}
