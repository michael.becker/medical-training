package org.infai.ddls.medicaltrainingv2.data.spec.training;

import org.infai.ddls.medicaltrainingv2.data.filter.training.TrainingSchemeFilter;
import org.infai.ddls.medicaltrainingv2.data.spec.NamedTrainingAppEntitySpecification;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingScheme;

@SuppressWarnings("serial")
public class TrainingSchemeSpecification extends NamedTrainingAppEntitySpecification<TrainingScheme, TrainingSchemeFilter> {

    public TrainingSchemeSpecification(TrainingSchemeFilter filter) {
	super(filter);
    }

}
