package org.infai.ddls.medicaltrainingv2.model.location;

import javax.persistence.Entity;

import org.hibernate.envers.Audited;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Audited
@Getter
@Setter
@ToString(callSuper = true)
public class DoctorsOffice extends AbstractTrainingSiteWith {
    public DoctorsOffice() {
	this.setTrainingSiteType(TrainingSiteType.DOCTORSOFFICE);
    }
}
