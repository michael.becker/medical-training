package org.infai.ddls.medicaltrainingv2.model.training.constraint;

import org.infai.ddls.medicaltrainingv2.model.training.RotationPlan;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RotationPlanSegmentsDoNotOverlapValidator implements ConstraintValidator<RotationPlanSegmentsMustNotOverlapConstraint, RotationPlanSegment> {
    /**
     * Prüft, dass ein Abschnitt eines Rotationsplans sich nicht mit anderen
     * Abschnitten überlappt. Überlappungen können folgende Form annehmen:
     * <ul>
     * <li>Beginn aktueller Abschnitt < Beginn existierender Abschnitt && Ende
     * aktueller Abschnitt > Beginn existierender Abschnitt</li>
     * <li>Beginn aktueller Abschnitt = Beginn existierender Abschnitt</li>
     * <li>Beginn aktueller Abschnitt > Beginn existierender Abschnitt && Beginn
     * aktueller Abschnitt < Ende existierender Abschnitt</li>
     * </ul>
     */
    @Override
    public boolean isValid(RotationPlanSegment rotationPlanSegment, ConstraintValidatorContext context) {
        RotationPlan rotationPlan = rotationPlanSegment.getRotationPlan();

        if (null != rotationPlan && null != rotationPlan.getRotationPlanSegments() && rotationPlan.getRotationPlanSegments().size() > 0) {
            for (RotationPlanSegment testSegment : rotationPlan.getRotationPlanSegments()) {
                System.out.println("vergleiche " + testSegment + " mit " + rotationPlanSegment);
                if (rotationPlanSegment.equals(testSegment)) {
                    continue;
                } else if (null == rotationPlanSegment.getSegmentBegin() || null == rotationPlanSegment.getSegmentEnd()) {
                    return false;
                } else if (rotationPlanSegment.getSegmentBegin().isBefore(testSegment.getSegmentBegin()) && rotationPlanSegment.getSegmentEnd().isAfter(testSegment.getSegmentBegin())) {
                    // Beginn akt < Beginn Test && Ende akt > Beginn Test return false;
                    return false;
                } else if (rotationPlanSegment.getSegmentBegin().equals(testSegment.getSegmentBegin())) {
                    // Beginn akt == Beginn test
                    return false;
                } else if (rotationPlanSegment.getSegmentBegin().isAfter(testSegment.getSegmentBegin()) && rotationPlanSegment.getSegmentBegin().isBefore(testSegment.getSegmentEnd())) {
                    // Beginn akt > Beginn Test && Beginn akt < Ende Test
                    return false;
                }
            }
        }

        return true;
    }
}