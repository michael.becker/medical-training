package org.infai.ddls.medicaltrainingv2.data.filter.location;

import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;
import org.infai.ddls.medicaltrainingv2.model.location.Region;
import org.infai.ddls.medicaltrainingv2.model.location.TrainingSiteType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrainingSiteFilter<T extends AbstractTrainingSite> extends LocationFilter<T> {
    private Region region;
    private TrainingSiteType trainingSiteType;
}
