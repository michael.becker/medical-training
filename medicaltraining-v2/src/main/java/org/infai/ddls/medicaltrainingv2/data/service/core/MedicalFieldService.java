package org.infai.ddls.medicaltrainingv2.data.service.core;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.infai.ddls.medicaltrainingv2.data.dao.core.MedicalFieldDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.core.MedicalFieldFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.spec.core.MedicalFieldSpecification;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MedicalFieldService extends AbstractTrainingAppService<MedicalField, MedicalFieldFilter> {
    @Autowired
    private MedicalFieldDAO dao;

    @Override
    public List<MedicalField> get() {
	return get(new MedicalFieldFilter(), DEFAULT_PAGING);
    }

    @Override
    @Transactional
    public List<MedicalField> get(MedicalFieldFilter filter, Pageable pageable) {
	return dao.get(new MedicalFieldSpecification(filter), pageable);
    }

    @Override
    public MedicalField get(UUID id) {
	return get(id, false, false);
    }

    @Transactional
    public MedicalField get(UUID id, boolean loadDirectChildren, boolean loadIndirectChildren) {
	MedicalField medicalField = dao.get(id);

	if (loadIndirectChildren) {
	    medicalField.getAllChildren().size();
	} else if (loadDirectChildren) {
	    medicalField.getChildren().size();
	}

	return medicalField;
    }

    @Override
    public MedicalField save(MedicalField entity) {
	return dao.save(entity);
    }
}
