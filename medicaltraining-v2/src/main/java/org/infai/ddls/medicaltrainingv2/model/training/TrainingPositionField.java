package org.infai.ddls.medicaltrainingv2.model.training;

import javax.persistence.Entity;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Audited
@Getter
@Setter
@ToString(callSuper = true)
public class TrainingPositionField extends TrainingPosition<AuthorisationField> {
}
