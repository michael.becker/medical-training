package org.infai.ddls.medicaltrainingv2.model.security;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class TrainingAppRole extends AbstractTrainingAppEntity {
    @NotBlank
    @Column(nullable = false)
    private String name;
}