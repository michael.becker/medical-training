package org.infai.ddls.medicaltrainingv2.controller.person;

import lombok.extern.apachecommons.CommonsLog;
import org.infai.ddls.medicaltrainingv2.data.filter.person.AuthorisedDoctorFilter;
import org.infai.ddls.medicaltrainingv2.data.service.authorisation.AuthorisationFieldService;
import org.infai.ddls.medicaltrainingv2.data.service.location.TrainingSiteService;
import org.infai.ddls.medicaltrainingv2.data.service.person.AuthorisedDoctorService;
import org.infai.ddls.medicaltrainingv2.data.service.training.TrainingPositionFieldService;
import org.infai.ddls.medicaltrainingv2.data.service.training.TrainingSchemeService;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import java.util.UUID;

@Controller
@RequestMapping("/persons/authoriseddoctors")
@CommonsLog
public class AuthorisedDoctorController extends AbstractPersonController<AuthorisedDoctor, AuthorisedDoctorFilter, AuthorisedDoctorService> {
    @Autowired
    private TrainingSchemeService trainingSchemeService;
    @Autowired
    private TrainingSiteService trainingSiteService;
    @Autowired
    private AuthorisationFieldService authorisationFieldService;
    @Autowired
    private TrainingPositionFieldService trainingPositionFieldService;
    @Autowired
    private Validator validator;

    @PostMapping("/{authorisedDoctorId}/authorisations/create")
    public String createAuthorisation(@PathVariable UUID authorisedDoctorId, @ModelAttribute AuthorisationField authorisation, BindingResult bindingResult, @RequestParam String saveAction, Model model, ServletRequest servletRequest) {
        // TODO: Wir validieren hier manuell, da Authorisation ein @Valid beim
        // AuthorisedDoctor hat - es wird aber nur die Id übertragen
        authorisation.setAuthorisedDoctor(getService().get(authorisedDoctorId));
        validator.validate(authorisation, bindingResult);

        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());

            model.addAttribute("authorisationField", authorisation);
            model.addAttribute("trainingSchemes", trainingSchemeService.get());
            model.addAttribute("trainingSites", trainingSiteService.getParentSites(true));

            return "/authorisations/createAuthorisationField";
        } else {
            authorisation = authorisationFieldService.save(authorisation);

            String saveURL = "/persons/authoriseddoctors/" + authorisedDoctorId + "/authorisations/" + authorisation.getId() + "/edit";
            String saveAndCloseURL = "/persons/authoriseddoctors/list";
            String saveAndCreateTrainingPositionURL = "/persons/authoriseddoctors/" + authorisedDoctorId + "/authorisations/" + authorisation.getId() + "/trainingpositions/create";

            if (saveAction.equals("saveAndCreateTrainingPosition")) {
                return redirect(saveAndCreateTrainingPositionURL);
            } else {
                return evaluateSaveAction(saveAction, saveURL, saveAndCloseURL);
            }
        }
    }

    @GetMapping("/{id}/authorisations/create")
    public String createAuthorisation(@PathVariable UUID id, Model model) {
        AuthorisationField authorisation = new AuthorisationField();
        authorisation.setAuthorisedDoctor(service.get(id));

        model.addAttribute("authorisationField", authorisation);
        model.addAttribute("trainingSchemes", trainingSchemeService.get());
        model.addAttribute("trainingSites", trainingSiteService.getParentSites(true));

        return "/authorisations/createAuthorisationField";
    }

    @GetMapping("/{authorisedDoctorId}/authorisations/{authorisationId}/trainingpositions/create")
    public String createTrainingPosition(@PathVariable UUID authorisedDoctorId, @PathVariable UUID authorisationId, Model model) {
        TrainingPositionField trainingPosition = new TrainingPositionField();
        trainingPosition.setAuthorisation(authorisationFieldService.get(authorisationId));

        model.addAttribute("trainingPositionField", trainingPosition);

        return "/training/createTrainingPosition";
    }

    @PostMapping("/{authorisedDoctorId}/authorisations/{authorisationId}/trainingpositions/create")
    public String createTrainingPosition(@PathVariable UUID authorisedDoctorId, @PathVariable UUID authorisationId, @ModelAttribute @Validated TrainingPositionField trainingPositionField, BindingResult bindingResult, @RequestParam String saveAction, Model model, ServletRequest servletRequest) {
        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());
            trainingPositionField.setAuthorisation(authorisationFieldService.get(authorisationId));
            return "/training/createTrainingPosition";

        } else {
            trainingPositionField = trainingPositionFieldService.save(trainingPositionField);

            String saveURL = "/persons/authoriseddoctors/" + authorisedDoctorId + "/authorisations/" + authorisationId + "/trainingpositions/edit/" + trainingPositionField.getId();
            String saveAndCloseURL = "/persons/authoriseddoctors/list";

            return evaluateSaveAction(saveAction, saveURL, saveAndCloseURL);
        }
    }

    @Override
    public String listEntities(Model model) {
        return listEntities(getService().get(getFilter(), null, true, true, true), getFilter(), model);
    }

    @Override
    protected AuthorisedDoctor getEntity() {
        return new AuthorisedDoctor();
    }

    @Override
    protected AuthorisedDoctorFilter getFilter() {
        return new AuthorisedDoctorFilter();
    }

    @Override
    protected String mappingDefault() {
        return "/persons/authoriseddoctors";
    }

    @Override
    protected String templateFolder() {
        return "/persons";
    }

}
