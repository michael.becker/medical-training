package org.infai.ddls.medicaltrainingv2.model.training;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.Valid;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.AbstractNamedTrainingAppEntity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Audited
@Getter
@Setter
public class TrainingScheme extends AbstractNamedTrainingAppEntity {
    @Valid
    @OneToMany(mappedBy = TrainingSchemeSegment_.TRAINING_SCHEME)
    private List<TrainingSchemeSegment> trainingSchemeSegments = new ArrayList<>();

    @Transient
    public int getDuration() {
	int duration = 0;
	for (TrainingSchemeSegment trainingSegment : trainingSchemeSegments) {
	    duration += trainingSegment.getDuration();
	}

	return duration;
    }
}
