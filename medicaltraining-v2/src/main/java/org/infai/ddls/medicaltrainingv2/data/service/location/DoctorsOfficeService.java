package org.infai.ddls.medicaltrainingv2.data.service.location;

import java.util.List;
import java.util.UUID;

import org.infai.ddls.medicaltrainingv2.data.dao.location.DoctorsOfficeDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.location.DoctorsOfficeFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.spec.location.DoctorsOfficeSpecification;
import org.infai.ddls.medicaltrainingv2.model.location.DoctorsOffice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class DoctorsOfficeService extends AbstractTrainingAppService<DoctorsOffice, DoctorsOfficeFilter> {
    @Autowired
    private DoctorsOfficeDAO dao;

    @Override
    public List<DoctorsOffice> get() {
	return get(new DoctorsOfficeFilter(), DEFAULT_PAGING);
    }

    @Override
    public List<DoctorsOffice> get(DoctorsOfficeFilter filter, Pageable pageable) {
	return dao.get(new DoctorsOfficeSpecification(filter), pageable);
    }

    @Override
    public DoctorsOffice get(UUID id) {
	return dao.get(id);
    }

    @Override
    public DoctorsOffice save(DoctorsOffice entity) {
	return dao.save(entity);
    }

}
