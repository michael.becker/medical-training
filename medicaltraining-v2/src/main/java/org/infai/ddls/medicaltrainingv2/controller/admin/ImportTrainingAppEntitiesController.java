package org.infai.ddls.medicaltrainingv2.controller.admin;

import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.io.FileUtils;
import org.infai.ddls.medicaltrainingv2.controller.AbstractTrainingAppController;
import org.infai.ddls.medicaltrainingv2.data.repo.admin.FileUploadRepository;
import org.infai.ddls.medicaltrainingv2.data.service.authorisation.AuthorisationFieldService;
import org.infai.ddls.medicaltrainingv2.data.service.core.ConversationTypeService;
import org.infai.ddls.medicaltrainingv2.data.service.core.MedicalFieldService;
import org.infai.ddls.medicaltrainingv2.data.service.core.SexService;
import org.infai.ddls.medicaltrainingv2.data.service.core.TitleService;
import org.infai.ddls.medicaltrainingv2.data.service.location.RegionService;
import org.infai.ddls.medicaltrainingv2.data.service.location.TrainingSiteService;
import org.infai.ddls.medicaltrainingv2.data.service.person.AuthorisedDoctorService;
import org.infai.ddls.medicaltrainingv2.data.service.person.RegistrarService;
import org.infai.ddls.medicaltrainingv2.data.service.training.TrainingSchemeService;
import org.infai.ddls.medicaltrainingv2.model.admin.*;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.infai.ddls.medicaltrainingv2.model.core.TreatmentType;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;

@Controller
@RequestMapping("/admin")
@CommonsLog
public class ImportTrainingAppEntitiesController extends AbstractTrainingAppController {
    @Autowired
    private TitleService titleService;
    @Autowired
    private SexService sexService;
    @Autowired
    private RegionService regionService;
    @Autowired
    private TrainingSiteService trainingSiteService;
    @Autowired
    private AuthorisedDoctorService authorisedDoctorService;
    @Autowired
    private FileUploadRepository fileUploadRepository;
    @Autowired
    private MedicalFieldService medicalFieldService;
    @Autowired
    private TrainingSchemeService trainingSchemeService;
    @Autowired
    private AuthorisationFieldService authorisationFieldService;
    @Autowired
    private RegistrarService registrarService;
    @Autowired
    private ConversationTypeService conversationTypeService;
    @Autowired
    private Validator validator;

    @PostMapping("/import/{id}/authoriseddoctors")
    public String importAuthorisedDoctors(@PathVariable UUID id, @ModelAttribute @Validated ImportAuthorisedDoctorList entities, BindingResult bindingResult, Model model, ServletRequest servletRequest) {
        for (int i = 0; i < entities.getEntities().size(); i++) {
            ImportAuthorisedDoctorListEntity entity = entities.getEntities().get(i);
            if (entity.isDoImport()) {
                AuthorisedDoctor authorisedDoctor = entity.getOriginalEntity().getEntity();

                if (entity.isDoImportAuthorisation()) {
                    for (AuthorisationField authorisation : authorisedDoctor.getAuthorisations()) {
                        authorisation.setAuthorisedDoctor(authorisedDoctor);
                    }
                } else {
                    authorisedDoctor.getAuthorisations().clear();
                }

                BindingResult authorisedDoctorBindingResult = new BeanPropertyBindingResult(authorisedDoctor, "authorisedDoctor");

                validator.validate(authorisedDoctor, authorisedDoctorBindingResult);

                for (FieldError error : authorisedDoctorBindingResult.getFieldErrors()) {
                    FieldError fieldError = new FieldError("target", "entities[" + i + "].originalEntity.entity." + error.getField(), error.getRejectedValue(), false, error.getCodes(), error.getArguments(), error.getDefaultMessage());
                    bindingResult.addError(fieldError);
                }
            }
        }

        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());
            importPreview(id, "importAuthorisedDoctorList", entities, authorisedDoctorService.get(), model);
            model.addAttribute("titles", titleService.get());
            model.addAttribute("sexes", sexService.get());
            model.addAttribute("trainingSites", trainingSiteService.get(Pageable.unpaged()));
            model.addAttribute("trainingSchemes", trainingSchemeService.get());

            return "/admin/importAuthorisedDoctors";
        } else {
            for (ImportAuthorisedDoctorListEntity entity : entities.getEntities()) {
                if (entity.isDoImport()) {
                    // wir speichern zunächst nur die Weiterbildungsbefugten ohne einer
                    // Referenzierung der WB-Befugnisse
                    Collection<AuthorisationField> authorisations = new ArrayList<>(entity.getOriginalEntity().getEntity().getAuthorisations());
                    entity.getOriginalEntity().getEntity().getAuthorisations().clear();
                    AuthorisedDoctor doctor = authorisedDoctorService.save(entity.getOriginalEntity().getEntity());

                    // Anschließend werden die Befugnisse mit einer Referenz auf die jeweiligen
                    // Befugten gespeichert
                    if (entity.isDoImportAuthorisation()) {
                        for (AuthorisationField authorisation : authorisations) {
                            authorisation.setAuthorisedDoctor(doctor);
                            authorisationFieldService.save(authorisation);
                        }
                    }
                }
            }

            FileUpload fileUpload = fileUploadRepository.findById(id).get();
            fileUpload.setAuthorisedDoctorsImported(true);
            fileUploadRepository.save(fileUpload);

            return redirect("/admin/import");
        }
    }

    @GetMapping("/import/{id}/authoriseddoctors")
    public String importAuthorisedDoctors(@PathVariable UUID id, Model model) throws Exception {
        ImportAuthorisedDoctorList entities = new ImportAuthorisedDoctorList();
        FODSAuthorisedDoctorParser parser = new FODSAuthorisedDoctorParser(trainingSiteService.get(Pageable.unpaged()), titleService.get(), sexService.get(), trainingSchemeService.get().get(0));

        Map<Integer, Collection<ParserEntityAuthorisedDoctor>> result = parser.parse(fileUploadRepository.findById(id).get().getFileName(), 3);
        for (Integer iteration : result.keySet()) {
            entities.addEntities(new ArrayList<>(result.get(iteration)), iteration);
        }

        importPreview(id, "importAuthorisedDoctorList", entities, authorisedDoctorService.get(), model);
        model.addAttribute("titles", titleService.get());
        model.addAttribute("sexes", sexService.get());
        model.addAttribute("trainingSites", trainingSiteService.get(Pageable.unpaged()));
        model.addAttribute("trainingSchemes", trainingSchemeService.get());

        return "/admin/importAuthorisedDoctors";
    }

    @GetMapping("/import")
    public String importFile(Model model) {
        model.addAttribute("fileUploads", fileUploadRepository.findAll());

        return "/admin/import";
    }

    @PostMapping("/import")
    public String importFile(@RequestParam("file") MultipartFile file, Model model) throws Exception {
        Path target = Files.createFile(Paths.get(FileUtils.getTempDirectoryPath() + "/authorisations" + UUID.randomUUID().toString() + ".fods"));

        FileUtils.copyInputStreamToFile(file.getInputStream(), target.toFile());

        FileUpload fileUpload = new FileUpload();
        fileUpload.setUploadDate(LocalDate.now());
        fileUpload.setFileName(target.toFile().getAbsolutePath());
        fileUpload.setDescription(FileUtils.readFileToString(target.toFile(), Charset.forName("UTF-8")));
        fileUploadRepository.save(fileUpload);

        return redirect("/admin/import");
    }

    @PostMapping("/import/{id}/regions")
    public String importRegions(@PathVariable UUID id, @ModelAttribute @Valid ImportRegionList entities, BindingResult bindingResult, Model model, ServletRequest servletRequest) {
        validate(entities, "importRegionList", bindingResult, model);

        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());
            return "/admin/importRegions";
        } else {
            for (ImportRegionListEntity entity : entities.getEntities()) {
                if (entity.isDoImport()) {
                    regionService.save(entity.getOriginalEntity().getEntity());
                }
            }

            FileUpload fileUpload = fileUploadRepository.findById(id).get();
            fileUpload.setRegionsImported(true);
            fileUploadRepository.save(fileUpload);

            return redirect("/admin/import");
        }
    }

    @GetMapping("/import/{id}/regions")
    public String importRegions(@PathVariable UUID id, Model model) throws Exception {
        ImportRegionList entities = new ImportRegionList();
        entities.setEntities(new ArrayList<>(new FODSRegionParser().parse(fileUploadRepository.findById(id).get().getFileName())));

        importPreview(id, "importRegionList", entities, regionService.get(), model);

        return "/admin/importRegions";
    }

    @PostMapping("/import/{id}/registrars")
    public String importRegistrars(@PathVariable UUID id, @ModelAttribute @Valid ImportRegistrarList entities, BindingResult bindingResult, Model model, ServletRequest servletRequest) {
        validate(entities, "importRegistrarList", bindingResult, model);

        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());

            importPreview(id, "importRegistrarList", entities, registrarService.get(), model);

            model.addAttribute("sexes", sexService.get());
            model.addAttribute("regions", regionService.get());
            model.addAttribute("conversationTypes", conversationTypeService.get());

            return "/admin/importRegistrars";
        } else {
            for (ImportRegistrarListEntity entity : entities.getEntities()) {
                if (entity.isDoImport()) {
                    registrarService.save(entity.getOriginalEntity().getEntity());
                }
            }

            FileUpload fileUpload = fileUploadRepository.findById(id).get();
            fileUpload.setRegistrarsImported(true);
            fileUploadRepository.save(fileUpload);

            return redirect("/admin/import");
        }
    }

    @GetMapping("/import/{id}/registrars")
    public String importRegistrars(@PathVariable UUID id, Model model) throws Exception {
        ImportRegistrarList entities = new ImportRegistrarList();
        entities.setEntities(new ArrayList<>(new FODSRegistrarParser(regionService.get(), sexService.get()).parse(fileUploadRepository.findById(id).get().getFileName())));

        importPreview(id, "importRegistrarList", entities, registrarService.get(), model);

        model.addAttribute("sexes", sexService.get());
        model.addAttribute("regions", regionService.get());
        model.addAttribute("conversationTypes", conversationTypeService.get());

        return "/admin/importRegistrars";
    }

    @PostMapping("/import/{id}/sexes")
    public String importSexes(@PathVariable UUID id, @ModelAttribute @Valid ImportSexList entities, BindingResult bindingResult, Model model, ServletRequest servletRequest) {
        validate(entities, "importSexList", bindingResult, model);

        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());
            importPreview(id, "importSexList", entities, sexService.get(), model);
            model.addAttribute(BindingResult.MODEL_KEY_PREFIX + "importSexList", bindingResult);
            System.out.println(model.asMap().keySet());
            return "/admin/importSexes";
        } else {
            for (ImportSexListEntity entity : entities.getEntities()) {
                if (entity.isDoImport()) {
                    sexService.save(entity.getOriginalEntity().getEntity());
                }
            }

            FileUpload fileUpload = fileUploadRepository.findById(id).get();
            fileUpload.setSexesImported(true);
            fileUploadRepository.save(fileUpload);

            return redirect("/admin/import");
        }
    }

    @GetMapping("/import/{id}/sexes")
    public String importSexes(@PathVariable UUID id, Model model) throws Exception {
        ImportSexList entities = new ImportSexList();
        entities.setEntities(new ArrayList<>(new FODSSexParser().parse(fileUploadRepository.findById(id).get().getFileName())));
        importPreview(id, "importSexList", entities, sexService.get(), model);

        return "/admin/importSexes";
    }

    @PostMapping("/import/{id}/titles")
    public String importTitles(@PathVariable UUID id, @ModelAttribute @Valid ImportTitleList titles, BindingResult bindingResult, Model model, ServletRequest servletRequest) {
        validate(titles, "importTitleList", bindingResult, model);

        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());
            return "/admin/importTitles";
        } else {

            for (ImportTitleListEntity entity : titles.getEntities()) {
                if (entity.isDoImport()) {
                    titleService.save(entity.getOriginalEntity().getEntity());
                }
            }

            FileUpload fileUpload = fileUploadRepository.findById(id).get();
            fileUpload.setTitlesImported(true);
            fileUploadRepository.save(fileUpload);

            return redirect("/admin/import");
        }
    }

    @GetMapping("/import/{id}/titles")
    public String importTitles(@PathVariable UUID id, Model model) throws Exception {
        ImportTitleList titles = new ImportTitleList();
        titles.setTitles(new ArrayList<>(new FODSTitleParser().parse(fileUploadRepository.findById(id).get().getFileName())));

        model.addAttribute("fileUpload", fileUploadRepository.findById(id).get());
        model.addAttribute("titles", titles);
        model.addAttribute("existingTitles", titleService.get());

        return "/admin/importTitles";
    }

    @PostMapping("/import/{id}/trainingsites")
    public String importTrainingSites(@PathVariable UUID id, @ModelAttribute @Valid ImportDoctorsOfficeList entities, BindingResult bindingResult, Model model, ServletRequest servletRequest) throws Exception {
        System.out.println(model.asMap().keySet());
        validate(entities, "importDoctorsOfficeList", bindingResult, model);
        System.out.println(model.asMap().keySet());

        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());

//		entities = new ImportDoctorsOfficeList();
//		FODSTrainingSiteParser parser = new FODSTrainingSiteParser(regionService.get(), medicalFieldService.get());
//		parser.parse(fileUploadRepository.findById(id).get().getFileName());
//		entities.setEntities(parser.getDoctorsOffices());

//		model.addAttribute("skippedTrainingSites", parser.getSkippedTrainingSites());
            model.addAttribute("regions", regionService.get());
            model.addAttribute("medicalFields", medicalFieldService.get());
            model.addAttribute("treatmentTypes", TreatmentType.values());

            importPreview(id, "importDoctorsOfficeList", entities, trainingSiteService.get(Pageable.unpaged()), model);

            return "/admin/importTrainingSites";
        } else {
            for (ImportDoctorsOfficeListEntity entity : entities.getEntities()) {
                if (entity.isDoImport()) {
                    trainingSiteService.save(entity.getOriginalEntity().getEntity());
                }
            }

            FileUpload fileUpload = fileUploadRepository.findById(id).get();
            fileUpload.setTrainingSitesImported(true);
            fileUploadRepository.save(fileUpload);

            return redirect("/admin/import");
        }
    }

    @GetMapping("/import/{id}/trainingsites")
    public String importTrainingSites(@PathVariable UUID id, Model model) throws Exception {
        ImportDoctorsOfficeList entities = new ImportDoctorsOfficeList();
        FODSTrainingSiteParser parser = new FODSTrainingSiteParser(regionService.get(), medicalFieldService.get());
        parser.parse(fileUploadRepository.findById(id).get().getFileName());
        entities.setEntities(parser.getDoctorsOffices());

        importPreview(id, "importDoctorsOfficeList", entities, trainingSiteService.get(Pageable.unpaged()), model);

        model.addAttribute("duplicateTrainingSites", parser.getDuplicateTrainingSites());
        model.addAttribute("skippedTrainingSites", parser.getSkippedTrainingSites());
        model.addAttribute("regions", regionService.get());
        model.addAttribute("medicalFields", medicalFieldService.get());
        model.addAttribute("treatmentTypes", TreatmentType.values());

        return "/admin/importTrainingSites";
    }

    private void importPreview(UUID fileUpload, String entitiesName, Object entities, @SuppressWarnings("rawtypes") List existingEntities, Model model) {
        model.addAttribute("fileUpload", fileUploadRepository.findById(fileUpload).get());
        model.addAttribute(entitiesName, entities);
        model.addAttribute("existingEntities", existingEntities);
    }

    /**
     * Validiert die übergebene Entitätliste und schreibt das Ergebnis der
     * Validierung als BindingResult in das übergebene Model
     *
     * @param entities
     * @param entityName
     * @param bindingResult das BindingResult, in welches geschrieben wird. Wenn
     *                      kein BindingResult angegeben wird (null), wird ein neues
     *                      BindingResult für das Objekt <code>entities</code> mit
     *                      dem Namen <code>entityName</code> erzeugt
     * @param model
     */
    private void validate(ImportList<?> entities, String entityName, BindingResult bindingResult, @NotNull Model model) {
        for (int i = 0; i < entities.getEntities().size(); i++) {
            DataBinder binder = new DataBinder(entities.getEntities().get(i).getOriginalEntity().getEntity());
            binder.setValidator(validator);
            binder.validate();
            for (FieldError error : binder.getBindingResult().getFieldErrors()) {
                FieldError myError = new FieldError(error.getObjectName(), "entities[" + i + "].originalEntity.entity." + error.getField(), error.getRejectedValue(), error.isBindingFailure(), error.getCodes(), error.getArguments(), error.getDefaultMessage());
                bindingResult.addError(myError);
            }
        }

        model.addAttribute(BindingResult.MODEL_KEY_PREFIX + entityName, bindingResult);
    }
}
