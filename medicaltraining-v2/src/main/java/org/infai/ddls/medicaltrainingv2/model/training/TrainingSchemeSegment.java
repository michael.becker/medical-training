package org.infai.ddls.medicaltrainingv2.model.training;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.infai.ddls.medicaltrainingv2.model.core.TreatmentType;

import lombok.Getter;
import lombok.Setter;

@Entity
@Audited
@Getter
@Setter
public class TrainingSchemeSegment extends AbstractTrainingAppEntity {
    @NotNull
    @Min(3)
    private int duration;
    @ManyToOne
    private MedicalField medicalField;
    @ElementCollection(fetch = FetchType.EAGER)
    private List<TreatmentType> permittedTreatmentTypes = new ArrayList<>(TreatmentType.values().length);
    @ManyToOne
    private TrainingScheme trainingScheme;
}
