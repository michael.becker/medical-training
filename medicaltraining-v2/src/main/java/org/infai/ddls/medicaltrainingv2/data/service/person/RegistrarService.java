package org.infai.ddls.medicaltrainingv2.data.service.person;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.infai.ddls.medicaltrainingv2.data.dao.person.RegistrarDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.person.RegistrarFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.spec.person.RegistrarSpecification;
import org.infai.ddls.medicaltrainingv2.model.person.Registrar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class RegistrarService extends AbstractTrainingAppService<Registrar, RegistrarFilter> {
    @Autowired
    private RegistrarDAO registrarDAO;

    @Override
    public List<Registrar> get() {
	return get(new RegistrarFilter(), false, null);
    }

    @Transactional
    public List<Registrar> get(boolean loadPreferredRegions) {
	return get(new RegistrarFilter(), loadPreferredRegions, null);
    }

    @Transactional
    public List<Registrar> get(RegistrarFilter filter, boolean loadPreferredRegions, Pageable pageable) {
	RegistrarSpecification spec = new RegistrarSpecification(filter);
	List<Registrar> registrars = registrarDAO.get(spec, pageable);

	if (loadPreferredRegions) {
	    for (Registrar registrar : registrars) {
		registrar.getPreferredRegions().size();
	    }
	}

	return registrars;
    }

    @Override
    @Transactional
    public List<Registrar> get(RegistrarFilter filter, Pageable pageable) {
	return get(filter, false, pageable);
    }

    @Override
    @Transactional
    public Registrar get(UUID id) {
	return get(id, false, false);
    }

    @Transactional
    public Registrar get(UUID id, boolean loadPreferredRegions) {
	return get(id, true, false);
    }

    @Transactional
    public Registrar get(UUID id, boolean loadPreferredRegions, boolean loadRotationPlans) {
	Registrar registrar = registrarDAO.get(id);

	if (loadPreferredRegions) {
	    registrar.getPreferredRegions().size();
	}
	if (loadRotationPlans) {
	    registrar.getRotationPlans().size();
	}

	return registrar;
    }

    @Override
    public Registrar save(Registrar entity) {
	return registrarDAO.save(entity);
    }
}
