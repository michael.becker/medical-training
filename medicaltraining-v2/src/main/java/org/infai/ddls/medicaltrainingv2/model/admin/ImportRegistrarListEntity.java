package org.infai.ddls.medicaltrainingv2.model.admin;

import org.infai.ddls.medicaltrainingv2.model.person.Registrar;

import de.caterdev.utils.parser.ParserEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImportRegistrarListEntity implements ImportListEntity<Registrar> {
    private ParserEntityRegistrar originalEntity;
    private boolean doImport = true;

    public ImportRegistrarListEntity() {
    }

    public ImportRegistrarListEntity(ParserEntity<Registrar> originalEntity) {
	setOriginalEntity(new ParserEntityRegistrar(originalEntity));
    }
}
