package org.infai.ddls.medicaltrainingv2.data.filter;

import org.infai.ddls.medicaltrainingv2.model.AbstractNamedTrainingAppEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class NamedTrainingAppEntityFilter<T extends AbstractNamedTrainingAppEntity> extends TrainingAppEntityFilter<T> {
    private String name;
}
