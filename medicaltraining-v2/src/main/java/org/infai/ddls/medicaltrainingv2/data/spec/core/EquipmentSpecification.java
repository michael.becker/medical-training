package org.infai.ddls.medicaltrainingv2.data.spec.core;

import org.infai.ddls.medicaltrainingv2.data.filter.core.EquipmentFilter;
import org.infai.ddls.medicaltrainingv2.data.spec.NamedTrainingAppEntitySpecification;
import org.infai.ddls.medicaltrainingv2.model.core.Equipment;

@SuppressWarnings("serial")
public class EquipmentSpecification extends NamedTrainingAppEntitySpecification<Equipment, EquipmentFilter> {
    public EquipmentSpecification(EquipmentFilter filter) {
	super(filter);
    }
}
