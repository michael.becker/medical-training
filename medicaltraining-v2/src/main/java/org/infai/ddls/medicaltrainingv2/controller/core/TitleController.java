package org.infai.ddls.medicaltrainingv2.controller.core;

import org.infai.ddls.medicaltrainingv2.controller.AbstractTypedTrainingAppController;
import org.infai.ddls.medicaltrainingv2.data.filter.core.TitleFilter;
import org.infai.ddls.medicaltrainingv2.data.service.core.TitleService;
import org.infai.ddls.medicaltrainingv2.model.core.Title;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/coredata/titles")
public class TitleController extends AbstractTypedTrainingAppController<Title, TitleFilter, TitleService> {
    @Override
    protected Title getEntity() {
	return new Title();
    }

    @Override
    protected TitleFilter getFilter() {
	return new TitleFilter();
    }

    @Override
    protected String mappingDefault() {
	return "/coredata/titles";
    }

    @Override
    protected String templateFolder() {
	return "/coredata";
    }
}
