package org.infai.ddls.medicaltrainingv2.model;

import org.apache.commons.lang3.StringUtils;

public interface TrainingAppEntity {
    public default String getTemplateNameMultipleEntities() {
	return getTemplateNameSingleEntity() + "s";
    }

    public default String getTemplateNameSingleEntity() {
	return getClass().getSimpleName();
    }

    public default String getViewNameMultipleEntities() {
	return getViewNameSingleEntity() + "s";
    }

    public default String getViewNameSingleEntity() {
	return StringUtils.uncapitalize(getClass().getSimpleName());
    }
}
