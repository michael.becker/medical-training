package org.infai.ddls.medicaltrainingv2.data.repo.training;

import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingScheme;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainingSchemeRepository extends TrainingAppEntityRepository<TrainingScheme> {

}
