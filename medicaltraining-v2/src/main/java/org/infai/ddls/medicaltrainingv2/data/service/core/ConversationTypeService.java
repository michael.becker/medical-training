package org.infai.ddls.medicaltrainingv2.data.service.core;

import java.util.List;
import java.util.UUID;

import org.infai.ddls.medicaltrainingv2.data.dao.core.ConversationTypeDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.core.ConversationTypeFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.spec.core.ConversationTypeSpecification;
import org.infai.ddls.medicaltrainingv2.model.person.ConversationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ConversationTypeService extends AbstractTrainingAppService<ConversationType, ConversationTypeFilter> {
    @Autowired
    private ConversationTypeDAO dao;

    @Override
    public List<ConversationType> get() {
	return get(new ConversationTypeFilter(), null);
    }

    @Override
    public List<ConversationType> get(ConversationTypeFilter filter, Pageable pageable) {
	return dao.get(new ConversationTypeSpecification(filter), pageable);
    }

    @Override
    public ConversationType get(UUID id) {
	return dao.get(id);
    }

    @Override
    public ConversationType save(ConversationType entity) {
	return dao.save(entity);
    }

}
