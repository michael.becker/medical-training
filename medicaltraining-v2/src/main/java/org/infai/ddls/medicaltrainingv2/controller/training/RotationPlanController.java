package org.infai.ddls.medicaltrainingv2.controller.training;

import lombok.extern.apachecommons.CommonsLog;
import org.infai.ddls.medicaltrainingv2.controller.AbstractTypedTrainingAppController;
import org.infai.ddls.medicaltrainingv2.controller.FilterAction;
import org.infai.ddls.medicaltrainingv2.data.filter.training.RotationPlanFilter;
import org.infai.ddls.medicaltrainingv2.data.filter.training.TrainingPositionFieldFilter;
import org.infai.ddls.medicaltrainingv2.data.service.core.MedicalFieldService;
import org.infai.ddls.medicaltrainingv2.data.service.location.RegionService;
import org.infai.ddls.medicaltrainingv2.data.service.location.TrainingSiteService;
import org.infai.ddls.medicaltrainingv2.data.service.person.AuthorisedDoctorService;
import org.infai.ddls.medicaltrainingv2.data.service.person.RegistrarService;
import org.infai.ddls.medicaltrainingv2.data.service.training.RotationPlanSegmentService;
import org.infai.ddls.medicaltrainingv2.data.service.training.RotationPlanService;
import org.infai.ddls.medicaltrainingv2.data.service.training.TrainingPositionFieldService;
import org.infai.ddls.medicaltrainingv2.data.service.training.TrainingSchemeService;
import org.infai.ddls.medicaltrainingv2.model.core.TreatmentType;
import org.infai.ddls.medicaltrainingv2.model.training.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/training/rotationplans")
@CommonsLog
public class RotationPlanController extends AbstractTypedTrainingAppController<RotationPlan, RotationPlanFilter, RotationPlanService> {
    @Autowired
    private RegistrarService registrarService;
    @Autowired
    private TrainingSchemeService trainingSchemeService;
    @Autowired
    private TrainingPositionFieldService trainingPositionFieldService;
    @Autowired
    private MedicalFieldService medicalFieldService;
    @Autowired
    private RotationPlanSegmentService rotationPlanSegmentService;
    @Autowired
    private AuthorisedDoctorService authorisedDoctorService;
    @Autowired
    private TrainingSiteService trainingSiteService;
    @Autowired
    private RegionService regionService;
    @Autowired
    private Validator validator;

    @GetMapping("/{rotationPlanId}/rotationplansegments/add")
    public String addRotationPlanSegment(@PathVariable UUID rotationPlanId, Model model) {
        RotationPlan rotationPlan = getService().get(rotationPlanId, true);

        RotationPlanSegment rotationPlanSegment = new RotationPlanSegment();
        rotationPlanSegment.setRotationPlan(rotationPlan);

        TrainingPositionFieldFilter filter = new TrainingPositionFieldFilter();
        filter.setRegions(rotationPlan.getRegistrar().getPreferredRegions());

        return listTrainingPosition(rotationPlanSegment, filter, trainingPositionFieldService.get(), model);
    }

    @PostMapping("/{rotationPlanId}/rotationplansegments/add")
    public String addRotationPlanSegment(@PathVariable UUID rotationPlanId, @ModelAttribute @Validated RotationPlanSegment rotationPlanSegment, BindingResult bindingResult, @RequestParam("trainingPosition.id") String trainingPositionId, Model model, ServletRequest servletRequest) {
        boolean bindingErrors = false;

        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());
            bindingErrors = true;
        } else {
            rotationPlanSegment.setRotationPlan(getService().get(rotationPlanId, true, false));
            rotationPlanSegment.setTrainingPosition(trainingPositionFieldService.get(UUID.fromString(trainingPositionId)));

            validator.validate(rotationPlanSegment, bindingResult);
            if (bindingResult.hasErrors()) {
                log.warn(bindingResult.getAllErrors());
                bindingErrors = true;
            }
        }

        if (bindingErrors) {
            TrainingPositionFieldFilter filter = new TrainingPositionFieldFilter();
            filter.setAvailableFrom(rotationPlanSegment.getSegmentBegin());
            filter.setAvailableUntil(rotationPlanSegment.getSegmentEnd());
            filter.setFullTimeEquivalent(rotationPlanSegment.getFullTimeEquivalent());
            // TODO - das MedicalField aus dem Filter kann aufgrund von
            // Hierarchiebeziehungen ein anderes sein als das der WB-Stelle
//	    filter.setMedicalField(rotationPlanSegment.getTrainingPosition().getAuthorisation().getTrainingSite().getMedicalField());
            model.addAttribute("rotationPlanSegment", rotationPlanSegment);
            model.addAttribute("trainingPositionFieldFilter", filter);
            model.addAttribute("trainingPositions", trainingPositionFieldService.get());
            model.addAttribute("medicalFields", medicalFieldService.get());

            return templateFolder() + "/addRotationPlanSegment";
        } else {
            rotationPlanSegment = rotationPlanSegmentService.save(rotationPlanSegment);

            return redirect("/training/rotationplans/" + rotationPlanId + "/rotationplansegments/" + rotationPlanSegment.getId() + "/edit");
        }
    }

    @GetMapping("createforregistrar/{registrarId}")
    public String createRotationPlan(@PathVariable UUID registrarId, Model model) {
        RotationPlan rotationPlan = new RotationPlan();
        rotationPlan.setRegistrar(registrarService.get(registrarId));

        model.addAttribute("rotationPlan", rotationPlan);
        model.addAttribute("trainingSchemes", trainingSchemeService.get());

        return "/training/createRotationPlan";
    }

    @PostMapping("/createforregistrar/{registrarId}")
    public String createRotationPlan(@PathVariable UUID registrarId, @ModelAttribute @Validated RotationPlan rotationPlan, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());

            return "/training/createRotationPlan";
        } else {
            rotationPlan = getService().save(rotationPlan);

            return redirect(mappingEdit(rotationPlan.getId()));
        }
    }

    @Override
    public String editEntity(@PathVariable UUID id, Model model) {
        return editEntity(service.get(id, true, true), model);
    }

    @PostMapping("/{rotationPlanId}/rotationplansegments/update")
    public String editRotationPlanSegment(@PathVariable UUID rotationPlanId, @ModelAttribute @Validated RotationPlanSegment rotationPlanSegment, BindingResult bindingResult, @RequestParam String saveAction, Model model, ServletRequest servletRequest) {
        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());

            rotationPlanSegment.setRotationPlan(getService().get(rotationPlanId));
            rotationPlanSegment.setTrainingPosition(trainingPositionFieldService.get(rotationPlanSegment.getTrainingPosition().getId()));

            model.addAttribute("rotationPlanSegment", rotationPlanSegment);
            model.addAttribute("fundableSegments", rotationPlanSegment.getFundableTrainingSchemeSegments(rotationPlanSegment.getRotationPlan().getTrainingScheme()));
            return templateFolder() + "/editRotationPlanSegment";
        } else {
            rotationPlanSegment = rotationPlanSegmentService.save(rotationPlanSegment);

            return evaluateSaveAction(saveAction, "/training/rotationplans/" + rotationPlanId + "/rotationplansegments/" + rotationPlanSegment.getId() + "/edit", "/training/rotationplans/" + rotationPlanId + "/edit");
        }
    }

    @GetMapping("/{rotationPlanId}/rotationplansegments/{rotationPlanSegmentId}/edit")
    public String editRotationPlanSegment(@PathVariable UUID rotationPlanId, @PathVariable UUID rotationPlanSegmentId, Model model) {
        RotationPlanSegment segment = rotationPlanSegmentService.get(rotationPlanSegmentId, true);

        model.addAttribute("rotationPlanSegment", segment);
        model.addAttribute("fundableSegments", segment.getFundableTrainingSchemeSegments(segment.getRotationPlan().getTrainingScheme()));

        return templateFolder() + "/editRotationPlanSegment";
    }

    @PostMapping("{rotationPlanId}/rotationplansegments/filter")
    public String filterRotationPlanSegment(@PathVariable UUID rotationPlanId, @ModelAttribute @Validated TrainingPositionFieldFilter filter, BindingResult bindingResult, @RequestParam String filterAction, Model model, ServletRequest servletRequest) {
        switch (FilterAction.valueOf(filterAction)) {
            case filter:
                break;
            case reset:
                return redirect(mappingDefault() + "/" + rotationPlanId + "/rotationplansegments/add");
            default:
                throw new RuntimeException("FilterAction " + filterAction + " not supported!");
        }

        RotationPlanSegment rotationPlanSegment = RotationPlanSegment.create(null, null, filter.getAvailableFrom(), filter.getAvailableUntil(), filter.getFullTimeEquivalent(), false, false);
        rotationPlanSegment.setRotationPlan(getService().get(rotationPlanId, true, false));

        if (null == filter.getAvailableFrom()) {
            String[] codes = {"rotationplansegments.availablefrom.notnull"};
            bindingResult.addError(new FieldError("trainingPositionFieldFilter", "availableFrom", filter.getAvailableFrom(), false, codes, null, "NotNull.availableFrom"));
        }
        if (null == filter.getAvailableUntil()) {
            String[] codes = {"rotationplansegments.availablefrom.notnull"};
            bindingResult.addError(new FieldError("trainingPositionFieldFilter", "availableUntil", filter.getAvailableUntil(), false, codes, null, "NotNull.availableUntil"));
        }

        if (!bindingResult.hasErrors()) {
            validator.validate(rotationPlanSegment, bindingResult);
        }

        if (bindingResult.hasErrors()) {
            log.warn(bindingResult.getAllErrors());
            return listTrainingPosition(rotationPlanSegment, filter, trainingPositionFieldService.get(), model);
        } else {
            return listTrainingPosition(rotationPlanSegment, filter, trainingPositionFieldService.get(filter, null), model);
        }
    }

    @Override
    protected void additionalEditAttributes(RotationPlan entity, Model model) {
        super.additionalEditAttributes(entity, model);

        Map<UUID, Integer> plannedSegmentsDuration = new HashMap<>();
        Map<UUID, Integer> finishedSegmentsDuration = new HashMap<>();
        Map<UUID, Integer> overallDuration = new HashMap<>();
        Map<UUID, TrainingSchemeSegmentCompletionStatus> completionStatus = new HashMap<>();

        LocalDate now = LocalDate.now();
        // TODO: eventuell den Filter so anpassen, dass wir bereits abgeleistete bzw.
        // noch geplante WB-Abschnitte als DB-Abfrage ermitteln können
//	RotationPlanSegmentFilter filter = new RotationPlanSegmentFilter();
//	filter.setRotationPlan(entity);
//	filter.setEndBefore(now);

        Integer plannedSegmentsDurationSum = 0;
        Integer finishedSegmentsDurationSum = 0;
        Integer unassignedSegmentsSum = 0;

        for (TrainingSchemeSegment segment : entity.getTrainingScheme().getTrainingSchemeSegments()) {
            List<RotationPlanSegment> actualSegments = entity.getPlannedSegments(now, segment);
            List<RotationPlanSegment> finishedSegments = entity.getFinishedSegments(now, segment);

            int durationFinished = 0;
            for (RotationPlanSegment finishedSegment : finishedSegments) {
                durationFinished += finishedSegment.getNormalisedDuration();
                finishedSegmentsDurationSum += finishedSegment.getNormalisedDuration();
            }
            finishedSegmentsDuration.put(segment.getMedicalField().getId(), durationFinished);

            int durationActual = 0;
            for (RotationPlanSegment actualSegment : actualSegments) {
                durationActual += actualSegment.getNormalisedDuration();
                plannedSegmentsDurationSum += actualSegment.getNormalisedDuration();
            }
            plannedSegmentsDuration.put(segment.getMedicalField().getId(), durationActual);

            int durationSum = durationFinished + durationActual;
            overallDuration.put(segment.getMedicalField().getId(), durationSum);

            if (durationSum < segment.getDuration()) {
                completionStatus.put(segment.getMedicalField().getId(), TrainingSchemeSegmentCompletionStatus.PARTIALLY_COMPLETED);
            } else if (durationSum > segment.getDuration()) {
                completionStatus.put(segment.getMedicalField().getId(), TrainingSchemeSegmentCompletionStatus.OVER_COMPLETED);
            } else {
                completionStatus.put(segment.getMedicalField().getId(), TrainingSchemeSegmentCompletionStatus.COMPLETED);
            }
        }

        for (RotationPlanSegment unassignedSegment : entity.getUnassignedSegments()) {
            unassignedSegmentsSum += unassignedSegment.getNormalisedDuration();
        }

        model.addAttribute("finishedSegments", entity.getFinishedSegments(now));
        model.addAttribute("plannedSegments", entity.getPlannedSegments(now));
        model.addAttribute("unassignedSegmentsSum", unassignedSegmentsSum);

        model.addAttribute("plannedSegmentsDuration", plannedSegmentsDuration);
        model.addAttribute("finishedSegmentsDuration", finishedSegmentsDuration);
        model.addAttribute("overallDuration", overallDuration);
        model.addAttribute("completionStatus", completionStatus);

        model.addAttribute("plannedSegmentsDurationSum", plannedSegmentsDurationSum);
        model.addAttribute("finishedSegmentsDurationSum", finishedSegmentsDurationSum);

        model.addAttribute("treatmentTypes", TreatmentType.values());
    }

    @Override
    protected RotationPlan getEntity() {
        return new RotationPlan();
    }

    @Override
    protected RotationPlanFilter getFilter() {
        return new RotationPlanFilter();
    }

    @Override
    protected String mappingDefault() {
        return "/training/rotationplans";
    }

    @Override
    protected String templateFolder() {
        return "/training";
    }

    private String listTrainingPosition(RotationPlanSegment rotationPlanSegment, TrainingPositionFieldFilter filter, List<TrainingPositionField> trainingPositions, Model model) {
        model.addAttribute("rotationPlanSegment", rotationPlanSegment);
        model.addAttribute("trainingPositionFieldFilter", filter);
        model.addAttribute("trainingPositions", trainingPositions);

        model.addAttribute("medicalFields", medicalFieldService.get());
        model.addAttribute("authorisedDoctors", authorisedDoctorService.get());
        model.addAttribute("trainingSites", trainingSiteService.get());
        model.addAttribute("regions", regionService.get());

        return templateFolder() + "/addRotationPlanSegment";
    }

}
