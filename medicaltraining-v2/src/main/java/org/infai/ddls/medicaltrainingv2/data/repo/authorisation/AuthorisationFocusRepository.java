package org.infai.ddls.medicaltrainingv2.data.repo.authorisation;

import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationFocus;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorisationFocusRepository extends TrainingAppEntityRepository<AuthorisationFocus> {

}
