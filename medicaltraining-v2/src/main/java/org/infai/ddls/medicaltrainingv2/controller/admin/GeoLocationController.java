package org.infai.ddls.medicaltrainingv2.controller.admin;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.infai.ddls.medicaltrainingv2.controller.AbstractTypedTrainingAppController;
import org.infai.ddls.medicaltrainingv2.data.filter.location.GeoLocationFilter;
import org.infai.ddls.medicaltrainingv2.data.service.location.GeoLocationService;
import org.infai.ddls.medicaltrainingv2.model.location.Address;
import org.infai.ddls.medicaltrainingv2.model.location.GeoLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Controller
@RequestMapping("/admin/geolocations")
public class GeoLocationController extends AbstractTypedTrainingAppController<GeoLocation, GeoLocationFilter, GeoLocationService> {
    @Autowired
    private GeoLocationService service;

    @Override
    protected GeoLocation getEntity() {
        return new GeoLocation();
    }

    @Override
    protected GeoLocationFilter getFilter() {
        return new GeoLocationFilter();
    }

    @Override
    protected String mappingDefault() {
        return "/admin/geolocations";
    }

    @Override
    protected String templateFolder() {
        return "/admin";
    }

    @PostMapping("/import")
    public String importCSV(@RequestParam("file") MultipartFile file, Model model) throws Exception {
        Path target = Files.createFile(Paths.get(FileUtils.getTempDirectoryPath() + "/geolocations" + UUID.randomUUID().toString() + ".csv"));
        FileUtils.copyInputStreamToFile(file.getInputStream(), target.toFile());

        Reader reader = new InputStreamReader(file.getInputStream());
        CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader());
        Collection<GeoLocation> geoLocations = new ArrayList<>();


        int i = 1;
        for (CSVRecord record : parser) {
            try {
                String longitude = record.get("@lon");
                String latitude = record.get("@lat");
                String city = record.get("addr:city");
                String zipcode = record.get("addr:postcode");
                String street = record.get("addr:street");
                String housenumber = record.get("addr:housenumber");

                GeoLocation geoLocation = new GeoLocation();
                Address address = new Address();
                address.setStreet(street);
                address.setCity(city);
                address.setZipCode(zipcode);
                address.setHousenumber(housenumber);
                geoLocation.setAddress(address);
                geoLocation.setLatitude(Double.valueOf(latitude));
                geoLocation.setLongitude(Double.valueOf(longitude));
                geoLocation.setName(address.toString());

                geoLocations.add(geoLocation);

                service.save(geoLocation);
                if (i % 1000 == 0) {
                    System.out.println("saved " + i + " locations");
                }
                i++;
            } catch (Exception e) {
                System.out.println("Error reading " + record);
            }
        }

        reader.close();
        parser.close();

        return redirect(mappingDefault() + "/list");
    }

    @PostMapping(value = "/findAddress", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void findAddress(@RequestParam("address") String address, HttpServletResponse response) throws Exception {
        Pageable pageable = PageRequest.of(0, 25, Sort.by(Sort.Direction.ASC, "address.street", "address.housenumber", "address.city"));
        List<GeoLocation> geoLocations = service.get(address, pageable);

        ObjectMapper mapper = new ObjectMapper();
        mapper.addMixIn(GeoLocation.class, GeoLocationJSON.class);

        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getWriter().write(mapper.writeValueAsString(geoLocations));
        response.flushBuffer();
    }

    private class GeoLocationJSON {
        @JsonIgnore
        private boolean archived;
        @JsonIgnore
        private String description;
        @JsonIgnore
        private String notes;
        @JsonIgnore
        private Map<String, String> additionalFields;
    }
}
