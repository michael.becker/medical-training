package org.infai.ddls.medicaltrainingv2.data.dao.authorisation;

import org.infai.ddls.medicaltrainingv2.data.dao.AbstractTrainingAppDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.authorisation.AuthorisationFieldFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.authorisation.AuthorisationFieldRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.authorisation.AuthorisationFieldSpecification;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.springframework.stereotype.Component;

@Component
public class AuthorisationFieldDAO extends AbstractTrainingAppDAO<AuthorisationField, AuthorisationFieldRepository, AuthorisationFieldFilter, AuthorisationFieldSpecification> {

}
