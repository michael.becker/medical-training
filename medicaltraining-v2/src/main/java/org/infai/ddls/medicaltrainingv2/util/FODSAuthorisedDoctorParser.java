package org.infai.ddls.medicaltrainingv2.util;

import org.infai.ddls.medicaltrainingv2.model.admin.ParserEntityAuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.infai.ddls.medicaltrainingv2.model.core.Sex;
import org.infai.ddls.medicaltrainingv2.model.core.Title;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSiteWith;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingScheme;

import javax.validation.constraints.Min;
import java.time.LocalDate;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FODSAuthorisedDoctorParser extends AbstractFODSAuthorisedSheetParser<AuthorisedDoctor, ParserEntityAuthorisedDoctor> {
    private Map<String, AbstractTrainingSite> trainingSites = new HashMap<>();
    private Map<String, Title> titles = new HashMap<>();
    private Map<String, Sex> sexes = new HashMap<>();
    private TrainingScheme trainingScheme;
    private int iteration = 1;

    public FODSAuthorisedDoctorParser(Collection<AbstractTrainingSite> trainingSites, Collection<Title> titles, Collection<Sex> sexes, TrainingScheme trainingScheme) {
        for (AbstractTrainingSite trainingSite : trainingSites) {
            this.trainingSites.put(trainingSite.getName(), trainingSite);
        }

        for (Title title : titles) {
            this.titles.put(title.getName(), title);
        }

        for (Sex sex : sexes) {
            this.sexes.put(sex.getName(), sex);
        }

        this.trainingScheme = trainingScheme;
    }

    public Map<Integer, Collection<ParserEntityAuthorisedDoctor>> parse(String file, @Min(1) int iterations) throws Exception {
        Map<Integer, Collection<ParserEntityAuthorisedDoctor>> authorisedDoctors = new HashMap<>();

        do {
            authorisedDoctors.put(iteration, parse(file));
            iteration++;
        } while (iteration <= iterations);

        return authorisedDoctors;
    }

    @Override
    protected ParserEntityAuthorisedDoctor parserEntity() {
        return new ParserEntityAuthorisedDoctor();
    }

    @Override
    protected AuthorisedDoctor parseRowResult(List<String> rowResult, int rowNumber) throws Exception {
        String doctorFirstname;
        String doctorLastName;
        String title;
        String sex;

        if (iteration == 1) {
            doctorFirstname = rowResult.get(COLUMN_DOCTOR1_FIRSTNAME);
            doctorLastName = rowResult.get(COLUMN_DOCTOR1_LASTNAME);
            title = rowResult.get(COLUMN_DOCTOR1_TITLE);
            sex = rowResult.get(COLUMN_DOCTOR1_SEX);
        } else if (iteration == 2) {
            doctorFirstname = rowResult.get(COLUMN_DOCTOR2_FIRSTNAME);
            doctorLastName = rowResult.get(COLUMN_DOCTOR2_LASTNAME);
            title = rowResult.get(COLUMN_DOCTOR2_TITLE);
            sex = rowResult.get(COLUMN_DOCTOR2_SEX);
        } else if (iteration == 3) {
            doctorFirstname = rowResult.get(COLUMN_DOCTOR3_FIRSTNAME);
            doctorLastName = rowResult.get(COLUMN_DOCTOR3_LASTNAME);
            title = rowResult.get(COLUMN_DOCTOR3_TITLE);
            sex = rowResult.get(COLUMN_DOCTOR3_SEX);
        } else {
            throw new RuntimeException("Unsupported amount of iterations: " + iteration + ", maximum three iterations supported!");
        }

        if (null == doctorFirstname || null == doctorLastName) {
            skipRow();
            return null;
        }

        String streetAndHousenumber = rowResult.get(COLUMN_TRAININGSITE_STREET).trim();
        String street = streetAndHousenumber;
        String housenumber = "n/a";
        int separatorStreetHousenumber = streetAndHousenumber.lastIndexOf(" ");
        if (separatorStreetHousenumber > 0) {
            street = streetAndHousenumber.substring(0, separatorStreetHousenumber).trim();
            housenumber = streetAndHousenumber.substring(separatorStreetHousenumber).trim();
        } else {
            System.out.println(separatorStreetHousenumber);
        }
        AuthorisedDoctor authorisedDoctor = new AuthorisedDoctor();
        authorisedDoctor.setFirstName(doctorFirstname);
        authorisedDoctor.setLastName(doctorLastName);
        authorisedDoctor.setAddress(createAddress(street, housenumber, rowResult.get(COLUMN_TRAININGSITE_ZIP), rowResult.get(COLUMN_TRAININGSITE_CITY)));
        authorisedDoctor.setContactInfo(createContactInfo(COLUMN_TRAININGSITE_EMAIL, COLUMN_TRAININGSITE_FAX, COLUMN_TRAININGSITE_PHONE, COLUMN_TRAININGSITE_MOBILE, COLUMN_TRAININGSITE_WEB, rowResult));

        if (null != title) {
            authorisedDoctor.setTitle(titles.get(title));
        }
        if (null != sex) {
            authorisedDoctor.setSex(sexes.get(sex));
        }

        if (null != rowResult.get(COLUMN_AUTHORISATION_CHECK) && rowResult.get(COLUMN_AUTHORISATION_CHECK).trim().equals("x")) {
            AuthorisationField authorisation = new AuthorisationField();
            authorisation.setAuthorisedDoctor(authorisedDoctor);
            authorisation.setTrainingScheme(trainingScheme);
            authorisation.setTrainingSite((AbstractTrainingSiteWith) trainingSites.get(generateTrainingSiteName(rowResult)));
            authorisation.setValidFrom(LocalDate.now());
            authorisation.setMaximalDuration(6);

            authorisedDoctor.getAuthorisations().add(authorisation);
        }

        return authorisedDoctor;
    }
}
