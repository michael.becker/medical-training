package org.infai.ddls.medicaltrainingv2.data.repo.authorisation;

import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorisationFieldRepository extends TrainingAppEntityRepository<AuthorisationField> {

}
