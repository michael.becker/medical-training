package org.infai.ddls.medicaltrainingv2.model.location;

import lombok.Getter;

@Getter
public enum TrainingSiteType {
    CLINIC(true, true), DEPARTMENT(false, true), DOCTORSOFFICE(true, false);

    // TODO: die jeweiligen Typen noch dynamisch nach den bool-Werten auslesen
    public static TrainingSiteType[] childTypes() {
	return new TrainingSiteType[] { DEPARTMENT };
    }

    public static TrainingSiteType[] parentTypes() {
	return new TrainingSiteType[] { CLINIC, DOCTORSOFFICE };
    }

    private boolean parentType;
    private boolean childsAllowed;

    private TrainingSiteType(boolean parentType, boolean childsAllowed) {
	this.parentType = parentType;
	this.childsAllowed = childsAllowed;
    }
}
