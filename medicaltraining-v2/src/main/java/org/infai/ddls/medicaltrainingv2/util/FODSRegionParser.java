package org.infai.ddls.medicaltrainingv2.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.infai.ddls.medicaltrainingv2.model.admin.ParserEntityRegion;
import org.infai.ddls.medicaltrainingv2.model.location.Region;

public class FODSRegionParser extends AbstractFODSAuthorisedSheetParser<Region, ParserEntityRegion> {
    private Set<String> regionNames = new HashSet<>();

    @Override
    protected ParserEntityRegion parserEntity() {
	return new ParserEntityRegion();
    }

    @Override
    protected Region parseRowResult(List<String> row, int rowNumber) throws Exception {
	String regionName = row.get(COLUMN_TRAININGSITE_REGION);

	if (regionNames.contains(regionName)) {
	    skipRow();
	    return null;
	} else {
	    regionNames.add(regionName);

	    Region region = new Region();
	    region.setName(regionName);

	    return region;
	}
    }

}
