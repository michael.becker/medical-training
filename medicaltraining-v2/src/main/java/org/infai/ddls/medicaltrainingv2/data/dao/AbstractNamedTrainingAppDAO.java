package org.infai.ddls.medicaltrainingv2.data.dao;

import org.infai.ddls.medicaltrainingv2.data.filter.TrainingAppEntityFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.AbstractTrainingAppEntitySpecification;
import org.infai.ddls.medicaltrainingv2.model.AbstractNamedTrainingAppEntity_;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

public abstract class AbstractNamedTrainingAppDAO<T extends AbstractTrainingAppEntity, U extends TrainingAppEntityRepository<T>, V extends TrainingAppEntityFilter<T>, W extends AbstractTrainingAppEntitySpecification<T, V>> extends AbstractTrainingAppDAO<T, U, V, W> {
    @Override
    public Sort getSort() {
        return super.getSort().and(Sort.by(Direction.ASC, AbstractNamedTrainingAppEntity_.name.getName()));
    }
}
