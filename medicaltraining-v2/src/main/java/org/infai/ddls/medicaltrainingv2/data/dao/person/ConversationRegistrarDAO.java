package org.infai.ddls.medicaltrainingv2.data.dao.person;

import org.infai.ddls.medicaltrainingv2.data.dao.AbstractTrainingAppDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.person.ConversationRegistrarFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.person.ConversationRegistrarRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.person.ConversationRegistrarSpecification;
import org.infai.ddls.medicaltrainingv2.model.person.ConversationRegistrar;
import org.springframework.stereotype.Component;

@Component
public class ConversationRegistrarDAO extends AbstractTrainingAppDAO<ConversationRegistrar, ConversationRegistrarRepository, ConversationRegistrarFilter, ConversationRegistrarSpecification> {

}
