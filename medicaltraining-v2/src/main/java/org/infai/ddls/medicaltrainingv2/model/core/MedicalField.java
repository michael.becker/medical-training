package org.infai.ddls.medicaltrainingv2.model.core;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.AbstractNamedTrainingAppEntity;
import org.infai.ddls.medicaltrainingv2.model.ChildRelationUtil;
import org.infai.ddls.medicaltrainingv2.model.CycleFreeConstraint;
import org.infai.ddls.medicaltrainingv2.model.HasChildren;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Audited
@CycleFreeConstraint
@Getter
@Setter
@ToString(callSuper = true)
public class MedicalField extends AbstractNamedTrainingAppEntity implements HasChildren<MedicalField> {
    @ManyToOne
    private MedicalField parent;
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = MedicalField_.PARENT)
    private List<MedicalField> children = new ArrayList<>();

    @Override
    public void setParent(MedicalField parent) {
	if (null == parent) {
	    return;
	} else if (this.equals(parent) || ChildRelationUtil.isChildOf(parent, this)) {
	    throw new RuntimeException("Cycle created!");
	} else {
	    this.parent = parent;
	    this.parent.getChildren().add(this);
	}
    }
}
