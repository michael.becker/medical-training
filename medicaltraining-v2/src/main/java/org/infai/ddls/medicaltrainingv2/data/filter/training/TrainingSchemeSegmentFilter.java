package org.infai.ddls.medicaltrainingv2.data.filter.training;

import org.infai.ddls.medicaltrainingv2.data.filter.TrainingAppEntityFilter;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingSchemeSegment;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public @Data class TrainingSchemeSegmentFilter extends TrainingAppEntityFilter<TrainingSchemeSegment> {

}
