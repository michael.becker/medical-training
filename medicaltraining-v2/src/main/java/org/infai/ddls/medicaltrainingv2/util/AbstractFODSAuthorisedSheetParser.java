package org.infai.ddls.medicaltrainingv2.util;

import de.caterdev.utils.parser.AbstractFODSParser;
import de.caterdev.utils.parser.ParserEntity;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.infai.ddls.medicaltrainingv2.model.location.Address;
import org.infai.ddls.medicaltrainingv2.model.person.ContactInfo;

import java.util.List;

public abstract class AbstractFODSAuthorisedSheetParser<T extends AbstractTrainingAppEntity, U extends ParserEntity<T>> extends AbstractFODSParser<T, U> {
    public static final int COLUMN_DOCTOR1_FIRSTNAME = 1;
    public static final int COLUMN_DOCTOR1_LASTNAME = 0;
    public static final int COLUMN_DOCTOR1_TITLE = 2;
    public static final int COLUMN_DOCTOR1_SEX = 3;

    public static final int COLUMN_DOCTOR2_FIRSTNAME = 10;
    public static final int COLUMN_DOCTOR2_LASTNAME = 9;
    public static final int COLUMN_DOCTOR2_TITLE = 11;
    public static final int COLUMN_DOCTOR2_SEX = 12;

    public static final int COLUMN_DOCTOR3_FIRSTNAME = 15;
    public static final int COLUMN_DOCTOR3_LASTNAME = 14;
    public static final int COLUMN_DOCTOR3_TITLE = 16;
    public static final int COLUMN_DOCTOR3_SEX = 17;

    public static final int COLUMN_TRAININGSITE_STREET = 5;
    public static final int COLUMN_TRAININGSITE_ZIP = 6;
    public static final int COLUMN_TRAININGSITE_CITY = 7;

    public static final int COLUMN_TRAININGSITE_MEDICALFIELD = 4;
    public static final int COLUMN_TRAININGSITE_TYPE = 8;

    public static final int COLUMN_TRAININGSITE_PHONE = 19;
    public static final int COLUMN_TRAININGSITE_MOBILE = 26;
    public static final int COLUMN_TRAININGSITE_FAX = 23;
    public static final int COLUMN_TRAININGSITE_EMAIL = 24;
    public static final int COLUMN_TRAININGSITE_WEB = 25;

    public static final int COLUMN_TRAININGSITE_REGION = 27;

    public static final int COLUMN_AUTHORISATION_CHECK = 36;

    protected Address createAddress(int columnStreet, int columnZip, int columnCity, List<String> row) {
        return createAddress(row.get(columnStreet), row.get(columnZip), row.get(columnCity));
    }

    protected Address createAddress(String columnStreet, String columnZip, String columnCity) {
        return createAddress(columnStreet, "n/a", columnZip, columnCity);
    }

    protected Address createAddress(String columnStreet, String columnHousenumber, String columnZip, String columnCity) {
        Address address = new Address();

        if (null != columnZip) {
            if (columnZip.length() == 4) {
                columnZip = "0" + columnZip;
            } else if (columnZip.startsWith("D-")) {
                columnZip = columnZip.substring(2);
            }
        }

        address.setStreet(columnStreet);
        address.setHousenumber(columnHousenumber);
        address.setZipCode(columnZip);
        address.setCity(columnCity);

        return address;
    }

    protected ContactInfo createContactInfo(int columnEmail, int columnFax, int columnPhone, int columnPhone2, int columnWeb, List<String> row) {
        return createContactInfo(row.get(columnEmail), row.get(columnFax), row.get(columnPhone), row.get(columnPhone2), row.get(columnWeb));
    }

    protected ContactInfo createContactInfo(String email, String faxNumber, String phoneNumber, String phoneNumber2, String web) {
        ContactInfo contactInfo = new ContactInfo();
        if (null != email) {
            contactInfo.setEmail(email.trim());
        }
        if (null != faxNumber) {
            contactInfo.setFaxNumber(faxNumber.trim());
        }
        if (null != phoneNumber) {
            contactInfo.setPhoneNumber(phoneNumber.trim());
        }
        if (null != phoneNumber2) {
            contactInfo.setPhoneNumber2(phoneNumber2.trim());
        }
        if (null != web) {
            contactInfo.setWeb(web.trim());
        }

        return contactInfo;
    }

    protected String generateTrainingSiteName(List<String> row) {
        return "Praxis " + row.get(COLUMN_DOCTOR1_LASTNAME) + " - " + row.get(COLUMN_TRAININGSITE_STREET) + " " + row.get(COLUMN_TRAININGSITE_ZIP) + " - " + row.get(COLUMN_TRAININGSITE_CITY);
    }
}
