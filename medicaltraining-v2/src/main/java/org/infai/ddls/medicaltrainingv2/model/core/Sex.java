package org.infai.ddls.medicaltrainingv2.model.core;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.AbstractNamedTrainingAppEntity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Audited
@Getter
@Setter
public class Sex extends AbstractNamedTrainingAppEntity {
    @NotBlank
    @Column(nullable = false)
    private String defaultPrefix;
    @NotBlank
    @Column(nullable = false)
    private String defaultSalutation;
}
