package org.infai.ddls.medicaltrainingv2.model.admin;

import java.util.ArrayList;
import java.util.List;

import org.infai.ddls.medicaltrainingv2.model.location.Region;

import de.caterdev.utils.parser.ParserEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImportRegionList implements ImportList<Region> {
    private List<ImportRegionListEntity> entities = new ArrayList<>();

    public void setEntities(List<ParserEntity<Region>> entities) {
	for (ParserEntity<Region> entity : entities) {
	    this.entities.add(new ImportRegionListEntity(entity));
	}
    }
}
