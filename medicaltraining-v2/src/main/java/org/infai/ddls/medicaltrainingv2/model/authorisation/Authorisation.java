package org.infai.ddls.medicaltrainingv2.model.authorisation;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSiteWith;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPosition;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPosition_;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingScheme;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Audited
@Getter
@Setter
public abstract class Authorisation<T extends TrainingPosition<?>> extends AbstractTrainingAppEntity {
    @Valid
    @NotNull
    @ManyToOne(optional = false)
    private AuthorisedDoctor authorisedDoctor;
    @Valid
    @NotNull
    @ManyToOne(optional = false)
    private AbstractTrainingSiteWith trainingSite;
    @NotNull
    @Column(nullable = false)
    private LocalDate validFrom;
    private LocalDate validUntil;
    @Valid
    @NotNull
    @ManyToOne(optional = false)
    private TrainingScheme trainingScheme;
    @NotNull
    @Min(value = 1)
    @Column(nullable = false)
    private int maximalDuration;
    @Valid
    @OneToMany(mappedBy = TrainingPosition_.AUTHORISATION, targetEntity = TrainingPosition.class)
    @ToString.Exclude
    private List<T> trainingPositions = new ArrayList<>();
}
