package org.infai.ddls.medicaltrainingv2.data.dao.training;

import org.infai.ddls.medicaltrainingv2.data.dao.AbstractNamedTrainingAppDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.training.TrainingSchemeFilter;
import org.infai.ddls.medicaltrainingv2.data.repo.training.TrainingSchemeRepository;
import org.infai.ddls.medicaltrainingv2.data.spec.training.TrainingSchemeSpecification;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingScheme;
import org.springframework.stereotype.Component;

@Component
public class TrainingSchemeDAO extends AbstractNamedTrainingAppDAO<TrainingScheme, TrainingSchemeRepository, TrainingSchemeFilter, TrainingSchemeSpecification> {

}
