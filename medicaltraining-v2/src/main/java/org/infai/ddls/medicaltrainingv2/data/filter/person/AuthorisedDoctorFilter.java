package org.infai.ddls.medicaltrainingv2.data.filter.person;

import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSiteWith;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthorisedDoctorFilter extends PersonFilter<AuthorisedDoctor> {
    private Integer lanr;
    private AbstractTrainingSiteWith trainingSite;
}
