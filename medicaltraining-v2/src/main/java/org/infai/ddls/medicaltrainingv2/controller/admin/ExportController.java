package org.infai.ddls.medicaltrainingv2.controller.admin;

import org.infai.ddls.medicaltrainingv2.controller.AbstractTrainingAppController;
import org.infai.ddls.medicaltrainingv2.data.filter.location.ClinicFilter;
import org.infai.ddls.medicaltrainingv2.data.filter.location.DoctorsOfficeFilter;
import org.infai.ddls.medicaltrainingv2.data.service.authorisation.AuthorisationFieldService;
import org.infai.ddls.medicaltrainingv2.data.service.location.TrainingSiteService;
import org.infai.ddls.medicaltrainingv2.data.service.person.AuthorisedDoctorService;
import org.infai.ddls.medicaltrainingv2.data.service.person.RegistrarService;
import org.infai.ddls.medicaltrainingv2.data.service.training.TrainingPositionFieldService;
import org.infai.ddls.medicaltrainingv2.util.CSVExport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/admin/export")
public class ExportController extends AbstractTrainingAppController {
    private CSVExport export = new CSVExport();

    @Autowired
    private RegistrarService registrarService;
    @Autowired
    private AuthorisedDoctorService authorisedDoctorService;
    @Autowired
    private TrainingSiteService trainingSiteService;
    @Autowired
    private AuthorisationFieldService authorisationFieldService;
    @Autowired
    private TrainingPositionFieldService trainingPositionFieldService;

    @GetMapping
    public String export(Model model) {
        return "/admin/export";
    }

    @GetMapping("/authorisations.csv")
    @ResponseBody
    public void exportAuthorisations(HttpServletResponse response) throws Exception {
        String result = export.exportAuthorisation(authorisationFieldService.get());
        writeResult(response, result);
    }

    @GetMapping("/authoriseddoctors.csv")
    @ResponseBody
    public void exportAuthorisedDoctors(HttpServletResponse response) throws Exception {
        String result = export.exportAuthorisedDoctors(authorisedDoctorService.get());
        writeResult(response, result);
    }

    @GetMapping("/clinics.csv")
    @ResponseBody
    public void exportClinics(HttpServletResponse response) throws Exception {
        String result = export.exportClinics(trainingSiteService.get(new ClinicFilter(), true, true, null));
        writeResult(response, result);
    }

    @GetMapping("/doctorsoffices.csv")
    @ResponseBody
    public void exportDoctorsOffices(HttpServletResponse response) throws Exception {
        String result = export.exportDoctorsOffices(trainingSiteService.get(new DoctorsOfficeFilter(), true, null));
        writeResult(response, result);
    }

    @GetMapping("/registrars.csv")
    @ResponseBody
    public void exportRegistrars(HttpServletResponse response) throws Exception {
        String result = export.exportRegistrars(registrarService.get(true));
        writeResult(response, result);
    }

    @GetMapping("/trainingpositions.csv")
    @ResponseBody
    public void exportTrainingPositions(HttpServletResponse response) throws Exception {
        String result = export.exportTrainingPositions(trainingPositionFieldService.get());
        writeResult(response, result);
    }

    public void writeResult(HttpServletResponse response, String result) throws Exception {
        response.setContentType("text/csv");
        response.getWriter().write(result);
        response.flushBuffer();
    }
}
