package org.infai.ddls.medicaltrainingv2.controller.core;

import org.infai.ddls.medicaltrainingv2.controller.AbstractTypedTrainingAppController;
import org.infai.ddls.medicaltrainingv2.data.filter.core.EquipmentFilter;
import org.infai.ddls.medicaltrainingv2.data.service.core.EquipmentService;
import org.infai.ddls.medicaltrainingv2.model.core.Equipment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/coredata/equipments")
public class EquipmentController extends AbstractTypedTrainingAppController<Equipment, EquipmentFilter, EquipmentService> {
    @Override
    protected Equipment getEntity() {
	return new Equipment();
    }

    @Override
    protected EquipmentFilter getFilter() {
	return new EquipmentFilter();
    }

    @Override
    protected String mappingDefault() {
	return "/coredata/equipments";
    }

    @Override
    protected String templateFolder() {
	return "/coredata";
    }

}
