package org.infai.ddls.medicaltrainingv2.data.service.location;

import org.infai.ddls.medicaltrainingv2.data.dao.location.GeoLocationDAO;
import org.infai.ddls.medicaltrainingv2.data.filter.location.GeoLocationFilter;
import org.infai.ddls.medicaltrainingv2.data.service.AbstractTrainingAppService;
import org.infai.ddls.medicaltrainingv2.data.spec.location.GeoLocationSpecification;
import org.infai.ddls.medicaltrainingv2.model.location.Address;
import org.infai.ddls.medicaltrainingv2.model.location.GeoLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class GeoLocationService extends AbstractTrainingAppService<GeoLocation, GeoLocationFilter> {
    @Autowired
    private GeoLocationDAO dao;

    @Override
    public List<GeoLocation> get() {
        return dao.get();
    }

    public List<GeoLocation> get(String address, Pageable pageable) {
        GeoLocationFilter filter = new GeoLocationFilter();
        filter.setSubstringSearch(true);
        filter.setStreet(address);

        return dao.get(new GeoLocationSpecification(filter), pageable);
    }

    @Override
    public List<GeoLocation> get(GeoLocationFilter filter, Pageable pageable) {
        return dao.get(new GeoLocationSpecification(filter), pageable);
    }

    @Override
    public GeoLocation get(UUID id) {
        return dao.get(id);
    }

    @Override
    public GeoLocation save(GeoLocation entity) {
        return dao.save(entity);
    }

    public Iterable<GeoLocation> save(Iterable<GeoLocation> entities) {
        return dao.save(entities);
    }
}
