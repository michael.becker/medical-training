package org.infai.ddls.medicaltrainingv2.model.admin;

import de.caterdev.utils.parser.ParserEntity;
import lombok.Getter;
import lombok.Setter;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ImportAuthorisedDoctorList implements ImportList<AuthorisedDoctor> {
    @Valid
    private List<ImportAuthorisedDoctorListEntity> entities = new ArrayList<>();

    public void addEntities(List<ParserEntity<AuthorisedDoctor>> entities) {
        for (ParserEntity<AuthorisedDoctor> entity : entities) {
            this.entities.add(new ImportAuthorisedDoctorListEntity(entity));
        }
    }

    public void addEntities(List<ParserEntityAuthorisedDoctor> entities, int iteration) {
        for (ParserEntityAuthorisedDoctor entity : entities) {
            this.entities.add(new ImportAuthorisedDoctorListEntity(entity, iteration));
        }
    }

}
