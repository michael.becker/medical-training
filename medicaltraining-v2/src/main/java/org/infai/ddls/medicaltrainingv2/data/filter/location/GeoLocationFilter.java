package org.infai.ddls.medicaltrainingv2.data.filter.location;

import lombok.Getter;
import lombok.Setter;
import org.infai.ddls.medicaltrainingv2.model.location.GeoLocation;

@Getter
@Setter
public class GeoLocationFilter extends LocationFilter<GeoLocation> {
    private String street;
    private String housenumber;
    private String zipCode;
    private String city;
    private boolean fuzzySearch = false;
    private boolean substringSearch = false;
}
