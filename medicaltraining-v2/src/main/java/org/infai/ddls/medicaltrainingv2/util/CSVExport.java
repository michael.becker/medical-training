package org.infai.ddls.medicaltrainingv2.util;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.infai.ddls.medicaltrainingv2.model.location.Clinic;
import org.infai.ddls.medicaltrainingv2.model.location.Department;
import org.infai.ddls.medicaltrainingv2.model.location.DoctorsOffice;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.model.person.Registrar;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;
import org.springframework.stereotype.Component;

import java.io.StringWriter;
import java.util.stream.Collectors;

@Component
public class CSVExport {
    public String exportAuthorisation(Iterable<AuthorisationField> authorisations) throws Exception {
        StringWriter out = new StringWriter();
        String[] header = {"UUID", "WB-Stätte", "WB-Ordnung", "WB-Befugter", "Gültig von", "Gültig bis", "Maximale Weiterbildungsdauer", "UUID WB-Stätte", "UUID WB-Ordnung", "UUID WB-Befugter"};
        CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withHeader(header));

        for (AuthorisationField authorisation : authorisations) {
            Object[] record = new Object[header.length];

            record[0] = authorisation.getId();
            record[1] = authorisation.getTrainingSite().getName();
            record[2] = authorisation.getTrainingScheme().getName();
            record[3] = authorisation.getAuthorisedDoctor().getFullName();
            record[4] = authorisation.getValidFrom();
            record[5] = authorisation.getValidUntil();
            record[6] = authorisation.getMaximalDuration();
            record[7] = authorisation.getTrainingSite().getId();
            record[8] = authorisation.getTrainingScheme().getId();
            record[9] = authorisation.getAuthorisedDoctor().getId();

            printer.printRecord(record);
        }

        printer.close();
        return out.toString();
    }

    public String exportAuthorisedDoctors(Iterable<AuthorisedDoctor> authorisedDoctors) throws Exception {
        StringWriter out = new StringWriter();
        CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withHeader("UUID", "Vorname", "Nachname", "Geburtsdatum", "Geschlecht", "Titel", "Straße", "PLZ", "Ort", "E-Mail", "Tel. 1", "Tel. 2", "Fax", "Web", "LANR"));

        for (AuthorisedDoctor authorisedDoctor : authorisedDoctors) {
            Object[] record = new Object[15];
            record[0] = authorisedDoctor.getId();
            record[1] = authorisedDoctor.getFirstName();
            record[2] = authorisedDoctor.getLastName();
            record[3] = authorisedDoctor.getBirthDate();
            if (null != authorisedDoctor.getSex()) {
                record[4] = authorisedDoctor.getSex().getName();
            }
            if (null != authorisedDoctor.getTitle()) {
                record[5] = authorisedDoctor.getTitle().getName();
            }

            if (null != authorisedDoctor.getAddress()) {
                record[6] = authorisedDoctor.getAddress().getStreet();
                record[7] = authorisedDoctor.getAddress().getZipCode();
                record[8] = authorisedDoctor.getAddress().getCity();
            }

            if (null != authorisedDoctor.getContactInfo()) {
                record[9] = authorisedDoctor.getContactInfo().getEmail();
                record[10] = authorisedDoctor.getContactInfo().getPhoneNumber();
                record[11] = authorisedDoctor.getContactInfo().getPhoneNumber2();
                record[12] = authorisedDoctor.getContactInfo().getFaxNumber();
                record[13] = authorisedDoctor.getContactInfo().getWeb();
            }

            record[14] = authorisedDoctor.getLanr();

            printer.printRecord(record);
        }

        printer.close();
        return out.toString();
    }

    public String exportClinics(Iterable<Clinic> clinics) throws Exception {
        StringWriter out = new StringWriter();
        CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withHeader("UUID", "Name", "Straße", "PLZ", "Ort", "E-Mail", "Tel. 1", "Tel. 2", "Fax", "Web", "Klinik", "Behandlungsart", "Fachgebiet", "Ausstattung"));

        for (Clinic clinic : clinics) {
            Object[] record = new Object[14];

            record[0] = clinic.getId();
            record[1] = clinic.getName();

            if (null != clinic.getAddress()) {
                record[2] = clinic.getAddress().getStreet();
                record[3] = clinic.getAddress().getZipCode();
                record[4] = clinic.getAddress().getCity();
            }

            if (null != clinic.getContactInfo()) {
                record[5] = clinic.getContactInfo().getEmail();
                record[6] = clinic.getContactInfo().getPhoneNumber();
                record[7] = clinic.getContactInfo().getPhoneNumber2();
                record[8] = clinic.getContactInfo().getFaxNumber();
                record[9] = clinic.getContactInfo().getWeb();
            }

            printer.printRecord(record);

            for (Department department : clinic.getDepartments()) {
                Object departmentRecord[] = new Object[14];

                departmentRecord[0] = department.getId();
                departmentRecord[1] = department.getName();

                if (null != department.getAddress()) {
                    departmentRecord[2] = department.getAddress().getStreet();
                    departmentRecord[3] = department.getAddress().getZipCode();
                    departmentRecord[4] = department.getAddress().getCity();
                }

                if (null != department.getContactInfo()) {
                    departmentRecord[5] = department.getContactInfo().getEmail();
                    departmentRecord[6] = department.getContactInfo().getPhoneNumber();
                    departmentRecord[7] = department.getContactInfo().getPhoneNumber2();
                    departmentRecord[8] = department.getContactInfo().getFaxNumber();
                    departmentRecord[9] = department.getContactInfo().getWeb();
                }

                departmentRecord[10] = department.getClinic().getId();
                departmentRecord[11] = department.getTreatmentType();
                departmentRecord[12] = department.getMedicalField().getName();
                departmentRecord[13] = department.getEquipments().stream().map(c -> c.getName()).collect(Collectors.joining(","));

                printer.printRecord(departmentRecord);
            }
        }

        printer.close();
        return out.toString();
    }

    public String exportDoctorsOffices(Iterable<DoctorsOffice> doctorsOffices) throws Exception {
        StringWriter out = new StringWriter();
        CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withHeader("UUID", "Name", "Straße", "PLZ", "Ort", "E-Mail", "Tel. 1", "Tel. 2", "Fax", "Web", "Behandlungsart", "Fachgebiet", "Ausstattung"));

        for (DoctorsOffice doctorsOffice : doctorsOffices) {
            Object[] record = new Object[13];
            record[0] = doctorsOffice.getId();
            record[1] = doctorsOffice.getName();

            if (null != doctorsOffice.getAddress()) {
                record[2] = doctorsOffice.getAddress().getStreet();
                record[3] = doctorsOffice.getAddress().getZipCode();
                record[4] = doctorsOffice.getAddress().getCity();
            }

            if (null != doctorsOffice.getContactInfo()) {
                record[5] = doctorsOffice.getContactInfo().getEmail();
                record[6] = doctorsOffice.getContactInfo().getPhoneNumber();
                record[7] = doctorsOffice.getContactInfo().getPhoneNumber2();
                record[8] = doctorsOffice.getContactInfo().getFaxNumber();
                record[9] = doctorsOffice.getContactInfo().getWeb();
            }

            record[10] = doctorsOffice.getTreatmentType();
            record[11] = doctorsOffice.getMedicalField().getName();
            record[12] = doctorsOffice.getEquipments().stream().map(c -> c.getName()).collect(Collectors.joining(","));

            printer.printRecord(record);
        }

        printer.close();
        return out.toString();
    }

    public String exportRegistrars(Iterable<Registrar> registrars) throws Exception {
        StringWriter out = new StringWriter();
        CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withHeader("UUD", "Vorname", "Nachname", "Geburtsdatum", "Geschlecht", "Titel", "Straße", "PLZ", "Ort", "E-Mail", "Tel. 1", "Tel. 2", "Fax", "Web", "KOMPAS Anmeldung", "Quereinstieg", "bevorzugte Regionen", "Datenübertragung VAHS", "Datenübertragung KVSA"));

        for (Registrar registrar : registrars) {
            Object[] record = new Object[19];
            record[0] = registrar.getId();
            record[1] = registrar.getFirstName();
            record[2] = registrar.getLastName();
            record[3] = registrar.getBirthDate();
            if (null != registrar.getSex()) {
                record[4] = registrar.getSex().getName();
            }
            if (null != registrar.getTitle()) {
                record[5] = registrar.getTitle().getName();
            }

            if (null != registrar.getAddress()) {
                record[6] = registrar.getAddress().getStreet();
                record[7] = registrar.getAddress().getZipCode();
                record[8] = registrar.getAddress().getCity();
            }

            if (null != registrar.getContactInfo()) {
                record[9] = registrar.getContactInfo().getEmail();
                record[10] = registrar.getContactInfo().getPhoneNumber();
                record[11] = registrar.getContactInfo().getPhoneNumber2();
                record[12] = registrar.getContactInfo().getFaxNumber();
                record[13] = registrar.getContactInfo().getWeb();
            }

            record[14] = registrar.getKompasSubscription();
            record[15] = registrar.isLateralEntry();
            record[16] = registrar.getPreferredRegions().stream().map(c -> c.getName()).collect(Collectors.joining(","));
            record[17] = registrar.getApprovalVAHSTransmission();
            record[18] = registrar.getApprovalKVSATransmission();
            printer.printRecord(record);
        }

        printer.close();

        return out.toString();
    }

    public String exportTrainingPositions(Iterable<TrainingPositionField> trainingPositions) throws Exception {
        StringWriter out = new StringWriter();
        String header[] = {"UUID", "WB-Stätte", "WB-Befugter", "Verfügbar von", "Verfügbar bis", "Kapazität", "Teilzeit möglich", "UUID WB-Befugnis"};
        CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withHeader(header));

        for (TrainingPositionField trainingPosition : trainingPositions) {
            Object[] record = new Object[header.length];

            record[0] = trainingPosition.getId();
            record[1] = trainingPosition.getAuthorisation().getTrainingSite().getName();
            record[2] = trainingPosition.getAuthorisation().getAuthorisedDoctor().getFullName();
            record[3] = trainingPosition.getAvailableFrom();
            record[4] = trainingPosition.getAvailableUntil();
            record[5] = trainingPosition.getCapacity();
            record[6] = trainingPosition.isPartTimeAvailable();
            record[7] = trainingPosition.getAuthorisation().getId();

            printer.printRecord(record);
        }

        printer.close();
        return out.toString();
    }
}
