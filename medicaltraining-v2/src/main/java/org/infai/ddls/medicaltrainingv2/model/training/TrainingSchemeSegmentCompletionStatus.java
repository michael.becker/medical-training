package org.infai.ddls.medicaltrainingv2.model.training;

public enum TrainingSchemeSegmentCompletionStatus {
    COMPLETED, PARTIALLY_COMPLETED, OVER_COMPLETED;
}
