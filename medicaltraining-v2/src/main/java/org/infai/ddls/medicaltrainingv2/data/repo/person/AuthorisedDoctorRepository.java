package org.infai.ddls.medicaltrainingv2.data.repo.person;

import java.util.Optional;
import java.util.UUID;

import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorisedDoctorRepository extends PersonRepository<AuthorisedDoctor> {
    public Optional<AuthorisedDoctor> findByLanr(Integer lanr);

    public Optional<AuthorisedDoctor> findByLanrAndIdNot(Integer lanr, UUID id);
}
