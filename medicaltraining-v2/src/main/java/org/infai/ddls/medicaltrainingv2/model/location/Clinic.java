package org.infai.ddls.medicaltrainingv2.model.location;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.hibernate.envers.Audited;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Audited
@Getter
@Setter
@ToString(callSuper = true)
public class Clinic extends AbstractTrainingSite {
    @OneToMany(mappedBy = Department_.CLINIC)
    @OrderBy(Department_.NAME)
    @ToString.Exclude
    private List<Department> departments = new ArrayList<>();

    public Clinic() {
	this.setTrainingSiteType(TrainingSiteType.CLINIC);
    }
}
