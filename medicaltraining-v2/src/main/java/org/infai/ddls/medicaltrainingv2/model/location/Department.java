package org.infai.ddls.medicaltrainingv2.model.location;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;

import org.hibernate.envers.Audited;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Audited
@Getter
@Setter
@ToString(callSuper = true)
public class Department extends AbstractTrainingSiteWith {
    @Valid
    @ManyToOne(optional = false) // (cascade = CascadeType.PERSIST)
    private Clinic clinic;

    public Department() {
	this.setTrainingSiteType(TrainingSiteType.DEPARTMENT);
    }

    public void setClinic(Clinic clinic) {
	this.clinic = clinic;
	clinic.getDepartments().add(this);
    }
}
