package org.infai.ddls.medicaltrainingv2.model.person;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.Digits;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField_;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Audited
@Getter
@Setter
public class AuthorisedDoctor extends Doctor<AuthorisedDoctor> {
    @Digits(integer = 9, fraction = 0)
    @Column(unique = true)
    private Integer lanr;
    @Valid
    @OneToMany(mappedBy = AuthorisationField_.AUTHORISED_DOCTOR)
    @ToString.Exclude
    private List<AuthorisationField> authorisations = new ArrayList<>();
}
