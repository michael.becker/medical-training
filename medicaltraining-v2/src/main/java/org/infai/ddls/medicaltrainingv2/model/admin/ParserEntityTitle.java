package org.infai.ddls.medicaltrainingv2.model.admin;

import org.infai.ddls.medicaltrainingv2.model.core.Title;

import de.caterdev.utils.parser.ParserEntity;

public class ParserEntityTitle extends ParserEntity<Title> {
    public ParserEntityTitle() {
    }

    public ParserEntityTitle(ParserEntity<Title> parserEntity) {
	setColumns(parserEntity.getColumns());
	setEntity(parserEntity.getEntity());
	setRowNumber(parserEntity.getRowNumber());
    }
}
