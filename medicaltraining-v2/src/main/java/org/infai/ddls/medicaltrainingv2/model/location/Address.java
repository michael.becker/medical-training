package org.infai.ddls.medicaltrainingv2.model.location;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;
import org.infai.ddls.medicaltrainingv2.model.location.constraint.AddressExistsConstraint;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Embeddable
@AddressExistsConstraint
@Getter
@Setter
@ToString
public class Address {
    @NotBlank
    @Column(nullable = false)
    private String street;
    @NotBlank
    @Column(nullable = false)
    @Pattern(regexp = "^[0-9]{5}$")
    private String zipCode;
    @NotBlank
    @Column(nullable = false)
    private String city;
    @NotBlank
    @Column(nullable = false)
    @ColumnDefault("'N/A'")
    private String housenumber;

    @Transient
    public String getFullAddress() {
        StringBuilder builder = new StringBuilder();
        builder.append(street).append(" ").append(housenumber).append(", ");
        builder.append(zipCode).append("/");
        builder.append(city);

        return builder.toString();
    }
}
