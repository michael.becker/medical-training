package org.infai.ddls.medicaltrainingv2.data.repo.person;

import org.infai.ddls.medicaltrainingv2.model.person.Registrar;
import org.springframework.stereotype.Repository;

@Repository
public interface RegistrarRepository extends PersonRepository<Registrar> {

}
