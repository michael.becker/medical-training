package org.infai.ddls.medicaltrainingv2.data.repo.authorisation;

import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationAdditionalTraining;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorisationAdditionalTrainingRepository extends TrainingAppEntityRepository<AuthorisationAdditionalTraining> {

}
