package org.infai.ddls.medicaltrainingv2.data.filter.location;

import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;
import org.infai.ddls.medicaltrainingv2.model.location.DoctorsOffice;

public class DoctorsOfficeFilter extends TrainingSiteWithFilter<DoctorsOffice> {
    public static DoctorsOfficeFilter create(TrainingSiteFilter<AbstractTrainingSite> originalFilter) {
	DoctorsOfficeFilter filter = new DoctorsOfficeFilter();
	filter.setAllowArchived(originalFilter.isAllowArchived());
	filter.setDistance(originalFilter.getDistance());
	filter.setLatitude(originalFilter.getLatitude());
	filter.setLongitude(originalFilter.getLongitude());
	filter.setRegion(originalFilter.getRegion());
	filter.setTrainingSiteType(originalFilter.getTrainingSiteType());
	filter.setZipCode(originalFilter.getZipCode());

	return filter;
    }
}
