package org.infai.ddls.medicaltrainingv2.data;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface TrainingAppDataInterface {

}
