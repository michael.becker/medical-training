package org.infai.ddls.medicaltrainingv2.data.repo.training;

import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionExclusion;

public interface TrainingPositionExclusionRepository extends TrainingAppEntityRepository<TrainingPositionExclusion> {

}
