package org.infai.ddls.medicaltrainingv2.controller;

import org.infai.ddls.medicaltrainingv2.data.filter.TrainingAppEntityFilter;
import org.infai.ddls.medicaltrainingv2.data.service.TrainingAppService;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;

public interface TrainingAppController<T extends AbstractTrainingAppEntity, U extends TrainingAppEntityFilter<T>, V extends TrainingAppService<T, U>> {
	public static final String MODELATTRIBUTE_FILTER = "filter";
}
