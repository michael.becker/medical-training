package org.infai.ddls.medicaltrainingv2.model.location;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltrainingv2.model.person.ContactInfo;

import lombok.Getter;
import lombok.Setter;

@Entity
@Audited
@Getter
@Setter
public abstract class AbstractTrainingSite extends Location {
    @Valid
    @Embedded
    private ContactInfo contactInfo = new ContactInfo();
    @ManyToOne
    private Region region;
    private TrainingSiteType trainingSiteType;
}
