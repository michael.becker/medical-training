package org.infai.ddls.medicaltrainingv2.test.controller.location;

import org.infai.ddls.medicaltrainingv2.test.controller.AbstractSpringControllerTest;
import org.infai.ddls.medicaltrainingv2.test.data.DatabaseSetupCore;
import org.infai.ddls.medicaltrainingv2.test.data.DatabaseSetupGeoLocations;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

public class GeoLocationControllerTest extends AbstractSpringControllerTest {
    @Test
    @DatabaseSetupGeoLocations
    @WithMockUser
    public void testAjaxRequest() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = postRequest("/admin/geolocations/findAddress").with(csrf());

        requestBuilder.param("address", "H");

        ResultActions result = mvc.perform(requestBuilder);

        result.andDo(print());
    }
}
