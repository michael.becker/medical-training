package org.infai.ddls.medicaltrainingv2.test.controller.training;

import org.infai.ddls.medicaltrainingv2.controller.FilterAction;
import org.infai.ddls.medicaltrainingv2.data.filter.training.TrainingPositionFieldFilter;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment;
import org.infai.ddls.medicaltrainingv2.test.controller.AbstractSpringControllerTest;
import org.infai.ddls.medicaltrainingv2.test.data.DatabaseSetupRotationPlans;
import org.infai.ddls.medicaltrainingv2.test.data.DatabaseSetupTrainingPositions;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RotationPlanControllerTest extends AbstractSpringControllerTest {
    @Test
    @DatabaseSetupRotationPlans
    @DatabaseSetupTrainingPositions
    @WithMockUser
    public void testAddRotationPlanSegment() throws Exception {
        RotationPlanSegment rotationPlanSegment = new RotationPlanSegment();
        rotationPlanSegment.setSegmentBegin(LocalDate.parse("2025-08-01"));
        rotationPlanSegment.setSegmentEnd(LocalDate.parse("2026-02-28"));
        rotationPlanSegment.setFullTimeEquivalent(100);
        rotationPlanSegment.setRotationPlan(rotationPlanAiW1());
        rotationPlanSegment.setTrainingPosition(trainingPositionLANR1DepartmentCC());

        MockHttpServletRequestBuilder requestBuilder = postRequest("/training/rotationplans/" + rotationPlanAiW1().getId() + "/rotationplansegments/add");
        addRotationPlanSegment(requestBuilder, rotationPlanSegment);

        ResultActions result = mvc.perform(requestBuilder);

        result.andExpect(status().isFound());
        result.andExpect(model().hasNoErrors());
    }

    @Test
    @DatabaseSetupRotationPlans
    @DatabaseSetupTrainingPositions
    @WithMockUser
    public void testFilterTrainingPositions() throws Exception {
        TrainingPositionFieldFilter filter = new TrainingPositionFieldFilter();
        filter.setMedicalField(medicalfieldChirurgie());
        filter.setAvailableFrom(LocalDate.parse("2021-09-01"));
        filter.setAvailableUntil(LocalDate.parse("2022-01-31"));

        MockHttpServletRequestBuilder requestBuilder = postRequest("/training/rotationplans/" + rotationPlanAiW1().getId() + "/rotationplansegments/filter");
        addTrainingPositionFieldFilter(requestBuilder, filter);
        addFilterAction(requestBuilder, FilterAction.filter);

        ResultActions result = mvc.perform(requestBuilder);

        result.andDo(print());
        result.andExpect(status().isOk());
        result.andExpect(model().hasNoErrors());
    }
}
