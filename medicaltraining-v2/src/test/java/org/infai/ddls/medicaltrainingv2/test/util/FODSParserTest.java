package org.infai.ddls.medicaltrainingv2.test.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.infai.ddls.medicaltrainingv2.model.admin.ParserEntityAuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.model.admin.ParserEntityDoctorsOffice;
import org.infai.ddls.medicaltrainingv2.model.admin.ParserEntityRegion;
import org.infai.ddls.medicaltrainingv2.model.admin.ParserEntityRegistrar;
import org.infai.ddls.medicaltrainingv2.model.admin.ParserEntitySex;
import org.infai.ddls.medicaltrainingv2.model.admin.ParserEntityTitle;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.infai.ddls.medicaltrainingv2.model.core.Sex;
import org.infai.ddls.medicaltrainingv2.model.core.Title;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;
import org.infai.ddls.medicaltrainingv2.model.location.Region;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingScheme;
import org.infai.ddls.medicaltrainingv2.util.FODSAuthorisedDoctorParser;
import org.infai.ddls.medicaltrainingv2.util.FODSRegionParser;
import org.infai.ddls.medicaltrainingv2.util.FODSRegistrarParser;
import org.infai.ddls.medicaltrainingv2.util.FODSSexParser;
import org.infai.ddls.medicaltrainingv2.util.FODSTitleParser;
import org.infai.ddls.medicaltrainingv2.util.FODSTrainingSiteParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.caterdev.utils.parser.ParserEntity;

public class FODSParserTest {
    public static final String FILE = FODSParserTest.class.getResource("/Weiterbildungsbefugte.fods").getFile();
    public static final String SMALL_FILE = FODSParserTest.class.getResource("/Weiterbildungsbefugte - Small.fods").getFile();
    public static final String AIW_FILE = FODSParserTest.class.getResource("/AiW.fods").getFile();

    private Collection<MedicalField> medicalFields = new ArrayList<>();
    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @BeforeEach
    public void setUp() {
	MedicalField allgemeinmedizin = new MedicalField();
	allgemeinmedizin.setName("Allgemeinmedizin");
	MedicalField innere = new MedicalField();
	innere.setName("Innere (Klinik)");

	medicalFields.add(allgemeinmedizin);
	medicalFields.add(innere);
    }

    @Test
    public void testParseTrainingSites() throws Exception {
	Collection<Region> regions = new ArrayList<>();
	for (ParserEntity<Region> entity : new FODSRegionParser().parse(FILE)) {
	    regions.add(entity.getEntity());
	}
	FODSTrainingSiteParser parser = new FODSTrainingSiteParser(regions, medicalFields);
	Collection<ParserEntityDoctorsOffice> trainingSites = parser.parse(FILE);

	assertNotNull(trainingSites);
	assertEquals(80, trainingSites.size());

	for (ParserEntityDoctorsOffice trainingSite : trainingSites) {
	    System.out.println(validator.validate(trainingSite.getEntity()));
	    switch (trainingSite.getEntity().getTrainingSiteType()) {
	    case CLINIC:
		assertTrue(validator.validate(trainingSite.getEntity()).isEmpty());
		break;
	    case DEPARTMENT:
		assertTrue(validator.validateProperty(trainingSite.getEntity(), "clinic").isEmpty());
		// kein break, damit die nachfolgenden Validierungen auch durchgeführt werden
	    case DOCTORSOFFICE:
		// TODO: manchmal wird das übernommen und manchmal nicht (je nachdem, welche
		// Fachgebiete schon existieren) -> bessere Testmöglichkeit überlegen
//		assertTrue(validator.validateProperty(trainingSite.getEntity(), "medicalField").isEmpty());
		assertTrue(validator.validateProperty(trainingSite.getEntity(), "equipments").isEmpty());
		assertTrue(validator.validateProperty(trainingSite.getEntity(), "treatmentType").isEmpty());
		assertTrue(validator.validateProperty(trainingSite.getEntity(), "authorisedDoctors").isEmpty());
		assertTrue(validator.validateProperty(trainingSite.getEntity(), "contactInfo").isEmpty());
		assertTrue(validator.validateProperty(trainingSite.getEntity(), "region").isEmpty());
		assertTrue(validator.validateProperty(trainingSite.getEntity(), "trainingSiteType").isEmpty());
		break;
	    default:
		break;

	    }
	}
    }

    @Test
    void testParseAuthorisedDoctors() throws Exception {
	Collection<AbstractTrainingSite> trainingSites = new ArrayList<>();
	Collection<Region> regions = new ArrayList<>();
	Collection<Title> titles = new ArrayList<>();
	Collection<Sex> sexes = new ArrayList<>();
	TrainingScheme trainingScheme = new TrainingScheme();
	trainingScheme.setName("Weiterbildung Allgemeinmedizin LSA");

	for (ParserEntity<Region> entity : new FODSRegionParser().parse(FILE)) {
	    regions.add(entity.getEntity());
	}
	for (ParserEntityDoctorsOffice entity : new FODSTrainingSiteParser(regions, medicalFields).parse(FILE)) {
	    trainingSites.add(entity.getEntity());
	}
	for (ParserEntity<Title> entity : new FODSTitleParser().parse(FILE)) {
	    titles.add(entity.getEntity());
	}
	for (ParserEntity<Sex> entity : new FODSSexParser().parse(FILE)) {
	    entity.getEntity().setDefaultPrefix("defaultPrefix");
	    entity.getEntity().setDefaultSalutation("defaultSalutation");
	    sexes.add(entity.getEntity());
	}

	FODSAuthorisedDoctorParser parser = new FODSAuthorisedDoctorParser(trainingSites, titles, sexes, trainingScheme);
	Map<Integer, Collection<ParserEntityAuthorisedDoctor>> authorisedDoctors = parser.parse(FILE, 3);

	assertEquals(3, authorisedDoctors.size());
	assertEquals(100, authorisedDoctors.get(1).size());
	assertEquals(20, authorisedDoctors.get(2).size());
	assertEquals(3, authorisedDoctors.get(3).size());

	for (Integer iteration : authorisedDoctors.keySet()) {
	    for (ParserEntityAuthorisedDoctor authorisedDoctor : authorisedDoctors.get(iteration)) {
		Set<ConstraintViolation<AuthorisedDoctor>> violations = validator.validate(authorisedDoctor.getEntity());
		if (!violations.isEmpty()) {
		    System.out.println(violations);
		}
	    }
	}

	for (ParserEntityAuthorisedDoctor entity : authorisedDoctors.get(3)) {
	    System.out.println(entity.getEntity());
	}
    }

    @Test
    void testParseRegions() throws Exception {
	Collection<ParserEntityRegion> regions = new FODSRegionParser().parse(FILE);

	assertEquals(10, regions.size());

	for (ParserEntity<Region> region : regions) {
	    assertTrue(validator.validate(region.getEntity()).isEmpty());
	}
    }

    @Test
    void testParseRegistrars() throws Exception {
	Collection<Region> regions = new ArrayList<>();
	Collection<Sex> sexes = new ArrayList<>();

	for (ParserEntity<Region> entity : new FODSRegionParser().parse(FILE)) {
	    regions.add(entity.getEntity());
	}
	for (ParserEntity<Sex> entity : new FODSSexParser().parse(FILE)) {
	    entity.getEntity().setDefaultPrefix("defaultPrefix");
	    entity.getEntity().setDefaultSalutation("defaultSalutation");
	    sexes.add(entity.getEntity());
	}

	Collection<ParserEntityRegistrar> registrarEntities = new FODSRegistrarParser(regions, sexes).parse(AIW_FILE);

	for (ParserEntityRegistrar entity : registrarEntities) {
	    System.out.println(entity.getEntity());
	}
    }

    @Test
    void testParseSexes() throws Exception {
	Collection<ParserEntitySex> sexes = new FODSSexParser().parse(FILE);

	assertEquals(2, sexes.size());

	for (ParserEntity<Sex> sex : sexes) {
	    assertTrue(validator.validateProperty(sex.getEntity(), "name").isEmpty());
	    assertFalse(validator.validateProperty(sex.getEntity(), "defaultPrefix").isEmpty());
	    assertFalse(validator.validateProperty(sex.getEntity(), "defaultSalutation").isEmpty());
	}
    }

    @Test
    void testParseTitles() throws Exception {
	Collection<ParserEntityTitle> titles = new FODSTitleParser().parse(FILE);

	assertEquals(7, titles.size());

	for (ParserEntity<Title> title : titles) {
	    assertTrue(validator.validate(title.getEntity()).isEmpty());
	}
    }
}
