package org.infai.ddls.medicaltrainingv2.test;

import com.github.springtestdbunit.bean.DatabaseConfigBean;
import com.github.springtestdbunit.bean.DatabaseDataSourceConnectionFactoryBean;
import org.dbunit.ext.h2.H2DataTypeFactory;
import org.infai.ddls.medicaltrainingv2.controller.TrainingAppController;
import org.infai.ddls.medicaltrainingv2.data.TrainingAppDataInterface;
import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.infai.ddls.medicaltrainingv2.test.data.DBScenario;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@ComponentScan(basePackageClasses = {TrainingAppDataInterface.class, TrainingAppController.class})
@EntityScan(basePackageClasses = AbstractTrainingAppEntity.class)
@EnableJpaRepositories(basePackageClasses = TrainingAppEntityRepository.class)
@EnableTransactionManagement(proxyTargetClass = true)
@PropertySource("classpath:logging.properties")
public class TrainingAppTestConfiguration {

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setUrl("jdbc:h2:mem:db;DB_CLOSE_DELAY=-1");
        dataSource.setUsername("sa");
        dataSource.setPassword("sa");

        return dataSource;
    }

    @Bean
    public DBScenario dbScenario() {
        return new DBScenario();
    }

    @Bean
    public DatabaseConfigBean dbUnitDatabaseConfig() {
        DatabaseConfigBean dbUnitDatabaseConfig = new DatabaseConfigBean();
        dbUnitDatabaseConfig.setDatatypeFactory(h2DataTypeFactory());

        return dbUnitDatabaseConfig;
    }

    @Bean
    public DatabaseDataSourceConnectionFactoryBean dbUnitDatabaseConnection() {
        DatabaseDataSourceConnectionFactoryBean dbUnitDatabaseConnection = new DatabaseDataSourceConnectionFactoryBean();
        dbUnitDatabaseConnection.setDatabaseConfig(dbUnitDatabaseConfig());
        dbUnitDatabaseConnection.setDataSource(dataSource());

        return dbUnitDatabaseConnection;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setJpaVendorAdapter(hibernateJpaVendorAdapter());
        em.setJpaProperties(additionalProperties());

        em.setPackagesToScan("org.infai.ddls.medicaltrainingv2.model");

        return em;
    }

    @Bean
    public H2DataTypeFactory h2DataTypeFactory() {
        return new H2DataTypeFactory();
    }

    @Bean
    public HibernateJpaVendorAdapter hibernateJpaVendorAdapter() {
        return new HibernateJpaVendorAdapter();
    }

    public JpaDialect jpaDialect() {
        return new HibernateJpaDialect();
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        transactionManager.setJpaDialect(jpaDialect());

        return transactionManager;
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
        return new LocalValidatorFactoryBean();
    }

    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.setProperty("hibernate.ejb.naming_strategy", "org.hibernate.cfg.ImprovedNamingStrategy");
        properties.setProperty("hibernate.show_sql", "true");
        properties.setProperty("hibernate.format_sql", "false");

        return properties;
    }
}
