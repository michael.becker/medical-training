package org.infai.ddls.medicaltrainingv2.test.util;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.infai.ddls.medicaltrainingv2.model.location.GeoLocation;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.UUID;

public class JSONExportTest {
    @Test
    public void testExportGeoLocation() throws Exception {
        GeoLocation location = createGeoLocation("Test", "Hainstraße", "11", "04109", "Leipzig");
        ObjectMapper mapper = new ObjectMapper();
        mapper.addMixIn(GeoLocation.class, GeoLocationJSON.class);

        String json = mapper.writeValueAsString(location);
        System.out.println(json);
    }

    private GeoLocation createGeoLocation(String name, String street, String housenumber, String zipCode, String city) {
        GeoLocation geoLocation = new GeoLocation();
        geoLocation.setId(UUID.randomUUID());
        geoLocation.setName(name);
        geoLocation.getAddress().setStreet(street);
        geoLocation.getAddress().setHousenumber(housenumber);
        geoLocation.getAddress().setZipCode(zipCode);
        geoLocation.getAddress().setCity(city);

        return geoLocation;
    }

    private class GeoLocationJSON {
        @JsonIgnore
        private boolean archived;
        @JsonIgnore
        private String description;
        @JsonIgnore
        private String notes;
        @JsonIgnore
        private Map<String, String> additionalFields;
    }
}
