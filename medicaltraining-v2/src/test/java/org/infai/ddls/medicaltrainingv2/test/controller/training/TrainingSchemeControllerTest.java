package org.infai.ddls.medicaltrainingv2.test.controller.training;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.infai.ddls.medicaltrainingv2.model.core.TreatmentType;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingSchemeSegment;
import org.infai.ddls.medicaltrainingv2.test.controller.AbstractSpringControllerTest;
import org.infai.ddls.medicaltrainingv2.test.data.DatabaseSetupTrainingSchemes;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

public class TrainingSchemeControllerTest extends AbstractSpringControllerTest {
    @Test
    @DatabaseSetupTrainingSchemes
    @WithMockUser
    public void testCreateTrainingSchemeSegment() throws Exception {
	TrainingSchemeSegment segment = new TrainingSchemeSegment();
	segment.setMedicalField(medicalfieldAllgemeinmedizin());
	segment.getPermittedTreatmentTypes().add(TreatmentType.Inpatient);
	segment.getPermittedTreatmentTypes().add(TreatmentType.Outpatient);
	segment.setTrainingScheme(trainingschemeAllgemeinmedizinLSA());
	segment.setDuration(6);

	MockHttpServletRequestBuilder requestBuilder = postRequest("/training/trainingschemes/" + trainingschemeAllgemeinmedizinLSA().getId() + "/trainingschemesegments/update").with(csrf());
	requestBuilder.param("saveAction", "saveAndClose");
	addTrainingSchemeSegment(requestBuilder, segment);

	ResultActions result = mvc.perform(requestBuilder);

	result.andDo(print());
	result.andExpect(status().isFound());
	result.andExpect(model().hasNoErrors());
    }

    @Test
    @DatabaseSetupTrainingSchemes
    @WithMockUser
    public void testListTrainingSchemes() throws Exception {
	ResultActions result = mvc.perform(get("/training/trainingschemes/list"));

	result.andDo(print());
	result.andExpect(status().isOk());
	result.andExpect(model().hasNoErrors());
	result.andExpect(model().attributeExists("trainingSchemes"));

	System.out.println(result.andReturn().getModelAndView().getModel().get("trainingSchemes"));
	System.out.println(trainingSchemes.values());
	System.out.println(trainingschemeAllgemeinmedizinLSA());

	result.andExpect(model().attribute("trainingSchemes", containsInAnyOrder(trainingschemeAllgemeinmedizinLSA())));
    }
}
