package org.infai.ddls.medicaltrainingv2.test.model.training;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.infai.ddls.medicaltrainingv2.test.AbstractTrainingAppTest;
import org.infai.ddls.medicaltrainingv2.test.model.Scenario;
import org.junit.jupiter.api.Test;

public class TrainingSchemeTest extends AbstractTrainingAppTest {
    @Test
    public void testCalculateDuration() throws Exception {
	assertEquals(60, Scenario.allgemeinMedizinLSA.getDuration());
    }
}
