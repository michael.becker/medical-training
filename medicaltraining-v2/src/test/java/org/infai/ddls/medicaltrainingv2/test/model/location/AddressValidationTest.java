package org.infai.ddls.medicaltrainingv2.test.model.location;

import org.infai.ddls.medicaltrainingv2.model.location.Address;
import org.infai.ddls.medicaltrainingv2.model.location.constraint.AddressExistsValidator;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddressValidationTest {
    private static Address validAddress = new Address();
    private static Address invalidAddress = new Address();

    static {
        validAddress.setStreet("Goerdelerring");
        validAddress.setHousenumber("9");
        validAddress.setZipCode("04109");
        validAddress.setCity("Leipzig");

        invalidAddress.setStreet("Musterstraße");
        invalidAddress.setHousenumber("15");
        invalidAddress.setZipCode("99999");
        invalidAddress.setCity("Non-Existing-City");

    }

    @Test
    @Disabled("Validierung noch nicht eingebaut")
    public void testDirectValidateInvalidAddressReturnsFalse() throws Exception {
        AddressExistsValidator addressValidator = new AddressExistsValidator();
        assertFalse(addressValidator.isValid(invalidAddress, null));
    }

    @Test
    public void testDirectValidateValidAddressReturnsTrue() throws Exception {
        AddressExistsValidator addressValidator = new AddressExistsValidator();
        assertTrue(addressValidator.isValid(validAddress, null));
    }

    @Test
    @Disabled("Validierung noch nicht eingebaut")
    public void testValidatorCallValidateInvalidAddressIsInvalid() throws Exception {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<Address>> violations = validator.validate(invalidAddress);
        assertFalse(violations.isEmpty());
    }

    @Test
    public void testValidatorCallValidateValidAddressIsValid() throws Exception {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<Address>> violations = validator.validate(validAddress);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void testSeparateStreetAndHousenumber() {
        String streetAndHousenumber = "Alte Hauptsr. 40".trim();
        String trainingSiteStreet = streetAndHousenumber;
        String trainingSiteHousenumber = "n/a";
        int separatorStreetHousenumber = streetAndHousenumber.lastIndexOf(" ");
        if (separatorStreetHousenumber > 0) {
            trainingSiteStreet = streetAndHousenumber.substring(0, separatorStreetHousenumber).trim();
            trainingSiteHousenumber = streetAndHousenumber.substring(separatorStreetHousenumber).trim();
        }

        System.out.println(trainingSiteStreet);
        System.out.println(trainingSiteHousenumber);
    }
}
