package org.infai.ddls.medicaltrainingv2.test.data;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.infai.ddls.medicaltrainingv2.model.core.TreatmentType;
import org.junit.Test;

import com.github.springtestdbunit.annotation.DatabaseSetup;

public class DataInitialisationTest extends AbstractSpringServiceTestJunit4 {
    @Test
    @DatabaseSetupAuthorisations
    public void testAuthorisationsFieldInitialised() throws Exception {
	assertEquals(7, authorisations.size());

	assertNotNull(authorisationFieldLANR1atDepartmentCC());
	assertNotNull(authorisationFieldLANR2atDepartmentAA());
	assertNotNull(authorisationFieldLANR3atDepartmentBB());
	assertNotNull(authorisationFieldLANR4atDepartmentSA1());
	assertNotNull(authorisationFieldLANR5atDepartmentBB1());
	assertNotNull(authorisationFieldLANR6atDepartmentBB1());
	assertNotNull(authorisationFieldLANR6atDepartmentTS1());

	assertEquals(authorisationFieldLANR1atDepartmentCC().getAuthorisedDoctor(), authorisedDoctorLANR1());
	assertEquals(authorisationFieldLANR2atDepartmentAA().getAuthorisedDoctor(), authorisedDoctorLANR2());
	assertEquals(authorisationFieldLANR3atDepartmentBB().getAuthorisedDoctor(), authorisedDoctorLANR3());
	assertEquals(authorisationFieldLANR4atDepartmentSA1().getAuthorisedDoctor(), authorisedDoctorLANR4());
	assertEquals(authorisationFieldLANR5atDepartmentBB1().getAuthorisedDoctor(), authorisedDoctorLANR5());
	assertEquals(authorisationFieldLANR6atDepartmentBB1().getAuthorisedDoctor(), authorisedDoctorLANR6());
	assertEquals(authorisationFieldLANR6atDepartmentTS1().getAuthorisedDoctor(), authorisedDoctorLANR6());

	assertEquals(authorisationFieldLANR1atDepartmentCC().getTrainingSite(), departmentParkkrankenhausCC());
	assertEquals(authorisationFieldLANR2atDepartmentAA().getTrainingSite(), departmentParkkrankenhausAA());
	assertEquals(authorisationFieldLANR3atDepartmentBB().getTrainingSite(), departmentParkkrankenhausBB());
	assertEquals(authorisationFieldLANR4atDepartmentSA1().getTrainingSite(), departmentKKHSchlossalleeSA1());
	assertEquals(authorisationFieldLANR5atDepartmentBB1().getTrainingSite(), departmentPraxisBuellebowBB1());
	assertEquals(authorisationFieldLANR6atDepartmentBB1().getTrainingSite(), departmentPraxisBuellebowBB1());
	assertEquals(authorisationFieldLANR6atDepartmentTS1().getTrainingSite(), departmentPolyklinikTS1());

	assertEquals(authorisationFieldLANR1atDepartmentCC().getTrainingScheme(), trainingschemeAllgemeinmedizinLSA());
	assertEquals(authorisationFieldLANR2atDepartmentAA().getTrainingScheme(), trainingschemeAllgemeinmedizinLSA());
	assertEquals(authorisationFieldLANR3atDepartmentBB().getTrainingScheme(), trainingschemeAllgemeinmedizinLSA());
	assertEquals(authorisationFieldLANR4atDepartmentSA1().getTrainingScheme(), trainingschemeAllgemeinmedizinLSA());
	assertEquals(authorisationFieldLANR5atDepartmentBB1().getTrainingScheme(), trainingschemeAllgemeinmedizinLSA());
	assertEquals(authorisationFieldLANR6atDepartmentBB1().getTrainingScheme(), trainingschemeAllgemeinmedizinLSA());
	assertEquals(authorisationFieldLANR6atDepartmentTS1().getTrainingScheme(), trainingschemeAllgemeinmedizinLSA());
    }

    @Test
    @DatabaseSetup("/authorisedDoctorsDataSet.xml")
    public void testAuthorisedDoctorsIntialised() throws Exception {
	assertEquals(6, authorisedDoctors.size());

	assertNotNull(authorisedDoctorLANR1());
	assertNotNull(authorisedDoctorLANR2());
	assertNotNull(authorisedDoctorLANR3());
	assertNotNull(authorisedDoctorLANR4());
	assertNotNull(authorisedDoctorLANR5());
	assertNotNull(authorisedDoctorLANR6());
    }

    @Test
    @DatabaseSetup("/coreDataSet.xml")
    public void testMedicalFieldsInitialised() throws Exception {
	assertEquals(7, medicalFields.size());

	assertNotNull(medicalfieldAllgemeinmedizin());
	assertNotNull(medicalfieldAnaesthesiologie());
	assertNotNull(medicalfieldChirurgie());
	assertNotNull(medicalfieldHausaerztliche());
	assertNotNull(medicalfieldInnere());
	assertNotNull(medicalfieldKJM());
	assertNotNull(medicalfieldUnmittelbarePatientenversorgung());

	assertEquals(medicalfieldHausaerztliche(), medicalfieldAllgemeinmedizin().getParent());
	assertEquals(medicalfieldUnmittelbarePatientenversorgung(), medicalfieldAnaesthesiologie().getParent());
	assertEquals(medicalfieldUnmittelbarePatientenversorgung(), medicalfieldChirurgie().getParent());
	assertEquals(medicalfieldUnmittelbarePatientenversorgung(), medicalfieldHausaerztliche().getParent());
	assertEquals(medicalfieldUnmittelbarePatientenversorgung(), medicalfieldInnere().getParent());
	assertEquals(medicalfieldUnmittelbarePatientenversorgung(), medicalfieldKJM().getParent());
    }

    @Test
    @DatabaseSetup("/registrarsDataSet.xml")
    public void testRegistrarsInitialised() throws Exception {
	assertEquals(3, registrars.size());

	assertNotNull(registrarAiW1());
	assertNotNull(registrarAiW2());
	assertNotNull(registrarAiW3());
    }

    @Test
    @DatabaseSetupRotationPlans
    public void testRotationPlanSegmentsInitialised() throws Exception {
	assertEquals(8, rotationPlanSegments.size());

	assertNotNull(rotationPlanSegmentAiW1atLANR1DepartmentCC());
	assertNotNull(rotationPlanSegmentAiW1atLANR2DepartmentAA());
	assertNotNull(rotationPlanSegmentAiW1atLANR3DepartmentBB());
	assertNotNull(rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_1());
	assertNotNull(rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_2());
	assertNotNull(rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_3());
	assertNotNull(rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_4());
	assertNotNull(rotationPlanSegmentAiW2atLANR1DepartmentCC());

	assertEquals(rotationPlanAiW1(), rotationPlanSegmentAiW1atLANR1DepartmentCC().getRotationPlan());
	assertEquals(rotationPlanAiW1(), rotationPlanSegmentAiW1atLANR2DepartmentAA().getRotationPlan());
	assertEquals(rotationPlanAiW1(), rotationPlanSegmentAiW1atLANR3DepartmentBB().getRotationPlan());
	assertEquals(rotationPlanAiW1(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_1().getRotationPlan());
	assertEquals(rotationPlanAiW1(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_2().getRotationPlan());
	assertEquals(rotationPlanAiW1(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_3().getRotationPlan());
	assertEquals(rotationPlanAiW1(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_4().getRotationPlan());

	assertEquals(rotationPlanAiW2(), rotationPlanSegmentAiW2atLANR1DepartmentCC().getRotationPlan());

	assertEquals(trainingPositionLANR1DepartmentCC(), rotationPlanSegmentAiW1atLANR1DepartmentCC().getTrainingPosition());
	assertEquals(trainingPositionLANR2DepartmentAA(), rotationPlanSegmentAiW1atLANR2DepartmentAA().getTrainingPosition());
	assertEquals(trainingPositionLANR3DepartmentBB(), rotationPlanSegmentAiW1atLANR3DepartmentBB().getTrainingPosition());
	assertEquals(trainingPositionLANR6DepartmentBB1_1(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_1().getTrainingPosition());
	assertEquals(trainingPositionLANR6DepartmentBB1_1(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_2().getTrainingPosition());
	assertEquals(trainingPositionLANR6DepartmentBB1_1(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_3().getTrainingPosition());
	assertEquals(trainingPositionLANR6DepartmentBB1_1(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_4().getTrainingPosition());

	assertEquals(trainingPositionLANR1DepartmentCC(), rotationPlanSegmentAiW2atLANR1DepartmentCC().getTrainingPosition());

	assertEquals(trainingschemesegmentAllgemeinmedizinLSAChirurgie(), rotationPlanSegmentAiW1atLANR1DepartmentCC().getAssignedSegment());
	assertEquals(trainingschemesegmentAllgemeinmedizinLSAUnmittelbare(), rotationPlanSegmentAiW1atLANR2DepartmentAA().getAssignedSegment());
	assertEquals(trainingschemesegmentAllgemeinmedizinLSAUnmittelbare(), rotationPlanSegmentAiW1atLANR3DepartmentBB().getAssignedSegment());
	assertEquals(trainingschemesegmentAllgemeinmedizinLSAAllgemeinmedizin(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_1().getAssignedSegment());
	assertEquals(trainingschemesegmentAllgemeinmedizinLSAAllgemeinmedizin(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_2().getAssignedSegment());
	assertEquals(trainingschemesegmentAllgemeinmedizinLSAUnmittelbare(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_3().getAssignedSegment());
	assertEquals(trainingschemesegmentAllgemeinmedizinLSAInnereKlinik(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_4().getAssignedSegment());
    }

    @Test
    @DatabaseSetupRotationPlans
    public void testRotationPlansInitialised() throws Exception {
	assertEquals(3, rotationPlans.size());

	assertNotNull(rotationPlanAiW1());
	assertNotNull(rotationPlanAiW2());
	assertNotNull(rotationPlanAiW3());

	assertEquals(trainingschemeAllgemeinmedizinLSA(), rotationPlanAiW1().getTrainingScheme());
	assertEquals(trainingschemeAllgemeinmedizinLSA(), rotationPlanAiW2().getTrainingScheme());
	assertEquals(trainingschemeAllgemeinmedizinLSA(), rotationPlanAiW3().getTrainingScheme());

	assertEquals(registrarAiW1(), rotationPlanAiW1().getRegistrar());
	assertEquals(registrarAiW2(), rotationPlanAiW2().getRegistrar());
	assertEquals(registrarAiW3(), rotationPlanAiW3().getRegistrar());
    }

    @Test
    @DatabaseSetupTrainingPositions
    public void testTrainingPositionsInitialised() throws Exception {
	System.out.println(trainingPositions);
	assertEquals(6, trainingPositions.size());

	assertNotNull(trainingPositionLANR1DepartmentCC());
	assertNotNull(trainingPositionLANR2DepartmentAA());
	assertNotNull(trainingPositionLANR3DepartmentBB());
	assertNotNull(trainingPositionLANR6DepartmentBB1_1());
	assertNotNull(trainingPositionLANR6DepartmentBB1_2());
	assertNotNull(trainingPositionLANR6DepartmentBB1_3());

	assertEquals(trainingPositionLANR1DepartmentCC().getAuthorisation(), authorisationFieldLANR1atDepartmentCC());
	assertEquals(trainingPositionLANR2DepartmentAA().getAuthorisation(), authorisationFieldLANR2atDepartmentAA());
	assertEquals(trainingPositionLANR3DepartmentBB().getAuthorisation(), authorisationFieldLANR3atDepartmentBB());
	assertEquals(trainingPositionLANR6DepartmentBB1_1().getAuthorisation(), authorisationFieldLANR6atDepartmentBB1());
	assertEquals(trainingPositionLANR6DepartmentBB1_2().getAuthorisation(), authorisationFieldLANR6atDepartmentBB1());
	assertEquals(trainingPositionLANR6DepartmentBB1_3().getAuthorisation(), authorisationFieldLANR6atDepartmentBB1());
    }

    @Test
    @DatabaseSetup("/coreDataSet.xml")
    @DatabaseSetup("/trainingSchemesDataSet.xml")
    public void testTrainingSchemesInitialised() throws Exception {
	assertEquals(1, trainingSchemes.size());
	assertEquals(6, trainingSchemeSegments.size());

	assertNotNull(trainingschemeAllgemeinmedizinLSA());
	assertNotNull(trainingschemesegmentAllgemeinmedizinLSAAllgemeinmedizin());
	assertNotNull(trainingschemesegmentAllgemeinmedizinLSAChirurgie());
	assertNotNull(trainingschemesegmentAllgemeinmedizinLSAHausaerztliche());
	assertNotNull(trainingschemesegmentAllgemeinmedizinLSAInnereKlinik());
	assertNotNull(trainingschemesegmentAllgemeinmedizinLSAKJM());
	assertNotNull(trainingschemesegmentAllgemeinmedizinLSAUnmittelbare());

	assertEquals(trainingschemesegmentAllgemeinmedizinLSAAllgemeinmedizin().getMedicalField(), medicalfieldAllgemeinmedizin());
	assertEquals(trainingschemesegmentAllgemeinmedizinLSAChirurgie().getMedicalField(), medicalfieldChirurgie());
	assertEquals(trainingschemesegmentAllgemeinmedizinLSAHausaerztliche().getMedicalField(), medicalfieldHausaerztliche());
	assertEquals(trainingschemesegmentAllgemeinmedizinLSAInnereKlinik().getMedicalField(), medicalfieldInnere());
	assertEquals(trainingschemesegmentAllgemeinmedizinLSAKJM().getMedicalField(), medicalfieldKJM());
	assertEquals(trainingschemesegmentAllgemeinmedizinLSAUnmittelbare().getMedicalField(), medicalfieldUnmittelbarePatientenversorgung());

	assertThat(trainingschemesegmentAllgemeinmedizinLSAAllgemeinmedizin().getPermittedTreatmentTypes(), containsInAnyOrder(TreatmentType.Outpatient));
	assertThat(trainingschemesegmentAllgemeinmedizinLSAChirurgie().getPermittedTreatmentTypes(), containsInAnyOrder(TreatmentType.Inpatient, TreatmentType.Outpatient));
	assertThat(trainingschemesegmentAllgemeinmedizinLSAHausaerztliche().getPermittedTreatmentTypes(), containsInAnyOrder(TreatmentType.Outpatient));
	assertThat(trainingschemesegmentAllgemeinmedizinLSAInnereKlinik().getPermittedTreatmentTypes(), containsInAnyOrder(TreatmentType.Inpatient));
	assertThat(trainingschemesegmentAllgemeinmedizinLSAKJM().getPermittedTreatmentTypes(), containsInAnyOrder(TreatmentType.Inpatient, TreatmentType.Outpatient));
	assertThat(trainingschemesegmentAllgemeinmedizinLSAUnmittelbare().getPermittedTreatmentTypes(), containsInAnyOrder(TreatmentType.Inpatient, TreatmentType.Outpatient));
    }

    @Test
    @DatabaseSetupTrainingSites
    public void testTrainingSitesInitialised() throws Exception {
	assertEquals(11, trainingSites.size());

	assertNotNull(clinicKKHSchlossallee());
	assertNotNull(clinicParkkrankenhaus());
	assertNotNull(clinicPolyklinik());
	assertNotNull(clinicPraxisBuellebow());
	assertNotNull(doctorsOfficePraxisOhneAbteilung());

	assertEquals(doctorsOfficePraxisOhneAbteilung().getMedicalField(), medicalfieldHausaerztliche());

	assertNotNull(departmentKKHSchlossalleeSA1());
	assertNotNull(departmentParkkrankenhausAA());
	assertNotNull(departmentParkkrankenhausBB());
	assertNotNull(departmentParkkrankenhausCC());
	assertNotNull(departmentPolyklinikTS1());
	assertNotNull(departmentPraxisBuellebowBB1());

	assertEquals(departmentKKHSchlossalleeSA1().getClinic(), clinicKKHSchlossallee());
	assertEquals(departmentParkkrankenhausAA().getClinic(), clinicParkkrankenhaus());
	assertEquals(departmentParkkrankenhausBB().getClinic(), clinicParkkrankenhaus());
	assertEquals(departmentParkkrankenhausCC().getClinic(), clinicParkkrankenhaus());
	assertEquals(departmentPolyklinikTS1().getClinic(), clinicPolyklinik());
	assertEquals(departmentPraxisBuellebowBB1().getClinic(), clinicPraxisBuellebow());

	assertEquals(departmentKKHSchlossalleeSA1().getMedicalField(), medicalfieldChirurgie());
	assertEquals(departmentParkkrankenhausAA().getMedicalField(), medicalfieldKJM());
	assertEquals(departmentParkkrankenhausBB().getMedicalField(), medicalfieldAnaesthesiologie());
	assertEquals(departmentParkkrankenhausCC().getMedicalField(), medicalfieldChirurgie());
	assertEquals(departmentPolyklinikTS1().getMedicalField(), medicalfieldHausaerztliche());
	assertEquals(departmentPraxisBuellebowBB1().getMedicalField(), medicalfieldAllgemeinmedizin());
    }
}
