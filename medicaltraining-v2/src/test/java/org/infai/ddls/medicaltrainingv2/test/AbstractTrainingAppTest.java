package org.infai.ddls.medicaltrainingv2.test;

import org.infai.ddls.medicaltrainingv2.model.person.Registrar;

import java.util.Collection;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.*;

public class AbstractTrainingAppTest {
    protected void assertCollection(Collection<?> actualCollection, Object... expectedElements) {
        assertNotNull(actualCollection);
        assertEquals(expectedElements.length, actualCollection.size());

        for (Object expectedElement : expectedElements) {
            assertTrue(actualCollection.contains(expectedElement));
        }

        assertThat(actualCollection, containsInAnyOrder(expectedElements));
    }

    protected void assertList(List<?> actual, Object... expectedObjects) {
        assertThat(actual, containsInAnyOrder(expectedObjects));
    }

    protected void assertSortedList(List<?> actualList, Object... expectedElements) {
        assertThat(actualList, contains(expectedElements));
    }

    protected Registrar createRandomRegistrar() {
        Registrar registrar = new Registrar();
        registrar.setFirstName("Vorname");
        registrar.setLastName("Nachname");

        registrar.getAddress().setStreet("Goerdelerring 9");
        registrar.getAddress().setZipCode("04109");
        registrar.getAddress().setCity("Leipzig");

        return registrar;
    }
}
