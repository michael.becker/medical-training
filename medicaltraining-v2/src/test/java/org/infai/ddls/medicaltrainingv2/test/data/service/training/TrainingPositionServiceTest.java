package org.infai.ddls.medicaltrainingv2.test.data.service.training;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import org.infai.ddls.medicaltrainingv2.data.filter.training.RotationPlanSegmentFilter;
import org.infai.ddls.medicaltrainingv2.data.filter.training.TrainingPositionFieldFilter;
import org.infai.ddls.medicaltrainingv2.data.service.core.MedicalFieldService;
import org.infai.ddls.medicaltrainingv2.data.service.training.RotationPlanSegmentService;
import org.infai.ddls.medicaltrainingv2.data.service.training.RotationPlanService;
import org.infai.ddls.medicaltrainingv2.data.service.training.TrainingPositionFieldService;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;
import org.infai.ddls.medicaltrainingv2.test.data.AbstractSpringServiceTestJunit4;
import org.infai.ddls.medicaltrainingv2.test.data.DatabaseSetupRotationPlans;
import org.infai.ddls.medicaltrainingv2.test.data.DatabaseSetupTrainingPositions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TrainingPositionServiceTest extends AbstractSpringServiceTestJunit4 {
    @Autowired
    private TrainingPositionFieldService service;
    @Autowired
    private MedicalFieldService medicalFieldService;
    @Autowired
    private RotationPlanService rotationPlanService;
    @Autowired
    private RotationPlanSegmentService rotationPlanSegmentService;

    @Test
    @DatabaseSetupTrainingPositions
    public void testFilterTrainingPositionsByMedicalField() throws Exception {
	TrainingPositionFieldFilter filter = new TrainingPositionFieldFilter();
	filter.setMedicalField(medicalFieldService.get(medicalfieldChirurgie().getId(), true, true));

	assertThat(service.get(filter, null), contains(trainingPositionLANR1DepartmentCC()));

	filter.setMedicalField(medicalFieldService.get(medicalfieldHausaerztliche().getId(), true, true));
	assertThat(service.get(filter, null), containsInAnyOrder(trainingPositionLANR6DepartmentBB1_1(), trainingPositionLANR6DepartmentBB1_2(), trainingPositionLANR6DepartmentBB1_3()));

	filter.setMedicalField(medicalFieldService.get(medicalfieldUnmittelbarePatientenversorgung().getId(), true, true));
	assertThat(service.get(filter, null), containsInAnyOrder(trainingPositionLANR1DepartmentCC(), trainingPositionLANR2DepartmentAA(), trainingPositionLANR3DepartmentBB(), trainingPositionLANR6DepartmentBB1_1(), trainingPositionLANR6DepartmentBB1_2(), trainingPositionLANR6DepartmentBB1_3()));
    }

    @Test
    @DatabaseSetupRotationPlans
    @DatabaseSetupTrainingPositions
    public void testGetAvailabilityUsingDateFilter() throws Exception {
	LocalDate from = LocalDate.parse("2017-01-01");
	LocalDate until = LocalDate.parse("2020-12-31");
	TrainingPositionFieldFilter filter = new TrainingPositionFieldFilter();
	filter.setAvailableFrom(from);
	filter.setAvailableUntil(until);
	List<TrainingPositionField> positions = service.get(filter, null);

	for (TrainingPositionField position : positions) {
	    assertEquals(trainingPositions.get(position.getId()).getAvailability(from, until, rotationPlanSegmentService.get(new RotationPlanSegmentFilter(), null)), position.getAvailability());
	}
    }

    @Test
    @DatabaseSetupTrainingPositions
    public void testGetPositionsByAvailableFromAndAvailableUntil() throws Exception {
	TrainingPositionFieldFilter filter = new TrainingPositionFieldFilter();
	filter.setAvailableFrom(LocalDate.parse("2017-01-01"));
	filter.setAvailableUntil(LocalDate.parse("2020-12-31"));
	List<TrainingPositionField> positions = service.get(filter, null);

	assertThat(positions, containsInAnyOrder(trainingPositionLANR1DepartmentCC(), trainingPositionLANR2DepartmentAA(), trainingPositionLANR3DepartmentBB(), trainingPositionLANR6DepartmentBB1_1(), trainingPositionLANR6DepartmentBB1_2()));

	filter = new TrainingPositionFieldFilter();
	filter.setAvailableFrom(LocalDate.parse("2016-12-31"));
	filter.setAvailableUntil(LocalDate.parse("2020-12-31"));
	assertTrue(service.get(filter, null).isEmpty());

	filter = new TrainingPositionFieldFilter();
	filter.setAvailableFrom(LocalDate.parse("2017-01-01"));
	filter.setAvailableUntil(LocalDate.parse("2021-01-01"));
	assertTrue(service.get(filter, null).isEmpty());

	filter = new TrainingPositionFieldFilter();
	filter.setAvailableFrom(LocalDate.parse("2025-02-01"));
	assertThat(service.get(filter, null), contains(trainingPositionLANR6DepartmentBB1_3()));

	filter = new TrainingPositionFieldFilter();
	filter.setAvailableUntil(LocalDate.parse("2099-12-31"));
	assertThat(service.get(filter, null), contains(trainingPositionLANR6DepartmentBB1_3()));
    }

    @Test
    @DatabaseSetupTrainingPositions
    public void testScenarioMatches() throws Exception {
	for (UUID id : trainingPositions.keySet()) {
	    assertEquals(trainingPositions.get(id), service.get(id));
	}

	for (TrainingPositionField position : trainingPositions.values()) {
	    assertEquals(position, service.get(position.getId()));
	}

    }
}
