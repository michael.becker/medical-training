package org.infai.ddls.medicaltrainingv2.test.data;

import com.github.springtestdbunit.annotation.DatabaseSetup;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@DatabaseSetup("/geoLocationsDataSet.xml")
public @interface DatabaseSetupGeoLocations {
}
