package org.infai.ddls.medicaltrainingv2.test.data.service.person;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.infai.ddls.medicaltrainingv2.data.filter.person.AuthorisedDoctorFilter;
import org.infai.ddls.medicaltrainingv2.data.service.person.AuthorisedDoctorService;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.test.data.AbstractSpringServiceTestJunit4;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@DatabaseSetup("/authorisedDoctorsDataSet.xml")
public class AuthorisedDoctorServiceTest extends AbstractSpringServiceTestJunit4 {
    @Autowired
    private AuthorisedDoctorService service;

    @Test
    public void testGetAuthorisedDoctorByAddressText() throws Exception {
        AuthorisedDoctorFilter filter = new AuthorisedDoctorFilter();
        filter.setAddressText("parkplatz");

        List<AuthorisedDoctor> authorisedDoctors = service.get(filter, null);

        assertNotNull(authorisedDoctors);
        assertEquals(1, authorisedDoctors.size());
        assertTrue(authorisedDoctors.contains(this.authorisedDoctors.get(UUID.fromString("8592ccab-cc32-437f-a125-1b3b8c9fc7cf"))));

        filter.setAddressText("leip");
        authorisedDoctors = service.get(filter, null);

        assertNotNull(authorisedDoctors);
        assertEquals(2, authorisedDoctors.size());
        assertTrue(authorisedDoctors.contains(this.authorisedDoctors.get(UUID.fromString("a19ac9e9-bda3-4b40-be47-3ff7e8950f96"))));
        assertTrue(authorisedDoctors.contains(this.authorisedDoctors.get(UUID.fromString("8592ccab-cc32-437f-a125-1b3b8c9fc7cf"))));
    }

    @Test
    public void testGetAuthorisedDoctorByFirstName() throws Exception {
        AuthorisedDoctorFilter filter = new AuthorisedDoctorFilter();
        filter.setFirstName("Chirurgie");

        List<AuthorisedDoctor> authorisedDoctors = service.get(filter, null);

        assertNotNull(authorisedDoctors);
        assertEquals(1, authorisedDoctors.size());
        assertTrue(authorisedDoctors.contains(this.authorisedDoctors.get(UUID.fromString("a19ac9e9-bda3-4b40-be47-3ff7e8950f96"))));
    }

    @Test
    public void testGetAuthorisedDoctorByFullName() throws Exception {
        AuthorisedDoctorFilter filter = new AuthorisedDoctorFilter();
        filter.setFullName("Haus");

        List<AuthorisedDoctor> authorisedDoctors = service.get(filter, null);
        System.out.println(authorisedDoctors);

        assertNotNull(authorisedDoctors);
        assertEquals(4, authorisedDoctors.size());
        assertTrue(authorisedDoctors.contains(this.authorisedDoctors.get(UUID.fromString("a19ac9e9-bda3-4b40-be47-3ff7e8950f96"))));
        assertTrue(authorisedDoctors.contains(this.authorisedDoctors.get(UUID.fromString("8592ccab-cc32-437f-a125-1b3b8c9fc7cf"))));
        assertTrue(authorisedDoctors.contains(this.authorisedDoctors.get(UUID.fromString("3ae95c1a-162e-4505-aec9-03ada48f4b54"))));
        assertTrue(authorisedDoctors.contains(this.authorisedDoctors.get(UUID.fromString("edd88ddd-e10f-42dc-8ed2-6cf92acfeb08"))));
    }

    @Test
    public void testGetAuthorisedDoctorByLANR() throws Exception {
        AuthorisedDoctorFilter filter = new AuthorisedDoctorFilter();
        filter.setLanr(1);

        List<AuthorisedDoctor> authorisedDoctors = service.get(filter, null);

        assertNotNull(authorisedDoctors);
        assertEquals(1, authorisedDoctors.size());
        assertTrue(authorisedDoctors.contains(this.authorisedDoctors.get(UUID.fromString("a19ac9e9-bda3-4b40-be47-3ff7e8950f96"))));
    }

    @Test
    public void testGetAuthorisedDoctorByLastName() throws Exception {
        AuthorisedDoctorFilter filter = new AuthorisedDoctorFilter();
        filter.setLastName("Park");

        List<AuthorisedDoctor> authorisedDoctors = service.get(filter, null);

        assertNotNull(authorisedDoctors);
        assertEquals(3, authorisedDoctors.size());
        assertTrue(authorisedDoctors.contains(this.authorisedDoctors.get(UUID.fromString("a19ac9e9-bda3-4b40-be47-3ff7e8950f96"))));
        assertTrue(authorisedDoctors.contains(this.authorisedDoctors.get(UUID.fromString("8592ccab-cc32-437f-a125-1b3b8c9fc7cf"))));
        assertTrue(authorisedDoctors.contains(this.authorisedDoctors.get(UUID.fromString("3ae95c1a-162e-4505-aec9-03ada48f4b54"))));
    }

    @Test
    public void testGetExistingAuthorisedDoctor() throws Exception {
        assertNotNull(service.get(UUID.fromString("edd88ddd-e10f-42dc-8ed2-6cf92acfeb08")));
    }

    @Test
    @Ignore
    public void testGetNonExistingAuthorisedDoctor() throws Exception {
        assertNull(service.get(UUID.fromString("edd88ddd-e10f-42dc-8ed2-6cf92acfeb09")));
    }

    @Test
    public void testIsLANRused() throws Exception {
        // existierende AutorisedDoctors haben unique LANR
        for (UUID id : this.authorisedDoctors.keySet()) {
            assertFalse(service.isLANRUsedByOther(this.authorisedDoctors.get(id)));
        }

        // keine LANR -> nicht in Benutzung
        AuthorisedDoctor authorisedDoctor = new AuthorisedDoctor();
        authorisedDoctor.getAddress().setStreet("Goerdelerring");
        authorisedDoctor.getAddress().setHousenumber("9");
        authorisedDoctor.getAddress().setZipCode("04109");
        authorisedDoctor.getAddress().setCity("Leipzig");

        assertFalse(service.isLANRUsedByOther(authorisedDoctor));

        // LANR 1 ist bereits in Benutzung
        authorisedDoctor.setLanr(1);
        assertTrue(service.isLANRUsedByOther(authorisedDoctor));

        // LANDR 8 nicht in Benutzung
        authorisedDoctor.setLanr(7);
        assertFalse(service.isLANRUsedByOther(authorisedDoctor));

        // nach dem Speichern: 7 nicht in Benutzung
        authorisedDoctor.setFirstName("test");
        authorisedDoctor.setLastName("test");
        authorisedDoctor = service.save(authorisedDoctor);
        assertFalse(service.isLANRUsedByOther(authorisedDoctor));

        // nach dem Speichern: 1 in Benutzung
        authorisedDoctor.setLanr(1);
        assertTrue(service.isLANRUsedByOther(authorisedDoctor));
    }
}
