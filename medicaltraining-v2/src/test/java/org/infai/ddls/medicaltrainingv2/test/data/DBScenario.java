package org.infai.ddls.medicaltrainingv2.test.data;

import org.infai.ddls.medicaltrainingv2.data.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltrainingv2.data.repo.authorisation.AuthorisationFieldRepository;
import org.infai.ddls.medicaltrainingv2.data.repo.core.EquipmentRepository;
import org.infai.ddls.medicaltrainingv2.data.repo.core.MedicalFieldRepository;
import org.infai.ddls.medicaltrainingv2.data.repo.location.ClinicRepository;
import org.infai.ddls.medicaltrainingv2.data.repo.location.DepartmentRepository;
import org.infai.ddls.medicaltrainingv2.data.repo.location.DoctorsOfficeRepository;
import org.infai.ddls.medicaltrainingv2.data.repo.person.AuthorisedDoctorRepository;
import org.infai.ddls.medicaltrainingv2.data.repo.person.RegistrarRepository;
import org.infai.ddls.medicaltrainingv2.data.repo.training.*;
import org.infai.ddls.medicaltrainingv2.model.AbstractTrainingAppEntity;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.infai.ddls.medicaltrainingv2.model.core.Equipment;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.model.person.Registrar;
import org.infai.ddls.medicaltrainingv2.model.training.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class DBScenario {
    @Autowired
    private AuthorisedDoctorRepository authorisedDoctorRepository;
    @Autowired
    private MedicalFieldRepository medicalFieldRepository;
    @Autowired
    private EquipmentRepository equipmentRepository;
    @Autowired
    private ClinicRepository clinicRepository;
    @Autowired
    private DepartmentRepository departmentRepository;
    @Autowired
    private DoctorsOfficeRepository doctorsOfficeRepository;
    @Autowired
    private TrainingSchemeRepository trainingSchemeRepository;
    @Autowired
    private TrainingSchemeSegmentRepository trainingSchemeSegmentRepository;
    @Autowired
    private AuthorisationFieldRepository authorisationFieldRepository;
    @Autowired
    private TrainingPositionFieldRepository trainingPositionFieldRepository;
    @Autowired
    private RegistrarRepository registrarRepository;
    @Autowired
    private RotationPlanRepository rotationPlanRepository;
    @Autowired
    private RotationPlanSegmentRepository rotationPlanSegmentRepository;

    public Map<UUID, AuthorisationField> getAuthorisations() {
        return findAll(authorisationFieldRepository);
    }

    public Map<UUID, AuthorisedDoctor> getAuthorisedDoctors() {
        return findAll(authorisedDoctorRepository);
    }

    public Map<UUID, Equipment> getEquipments() {
        return findAll(equipmentRepository);
    }

    public Map<UUID, MedicalField> getMedicalFields() {
        return findAll(medicalFieldRepository);
    }

    @Transactional
    public Map<UUID, Registrar> getRegistrars() {
        Map<UUID, Registrar> registrars = findAll(registrarRepository);

        for (Registrar registrar : registrars.values()) {
            registrar.getPreferredRegions().size();
        }

        return registrars;
    }

    @Transactional
    public Map<UUID, RotationPlan> getRotationPlans() {
        Map<UUID, RotationPlan> rotationPlans = findAll(rotationPlanRepository);

        for (RotationPlan rotationPlan : rotationPlans.values()) {
            rotationPlan.getRotationPlanSegments().size();
        }

        return rotationPlans;
    }

    public Map<UUID, RotationPlanSegment> getRotationPlanSegments() {
        return findAll(rotationPlanSegmentRepository);
    }

    @Transactional
    public Map<UUID, TrainingPositionField> getTrainingPositions() {
        Map<UUID, TrainingPositionField> positions = findAll(trainingPositionFieldRepository);

        for (TrainingPositionField position : positions.values()) {
            position.getExclusions().size();
        }

        return positions;
    }

    public Map<UUID, TrainingScheme> getTrainingSchemes() {
        return findAll(trainingSchemeRepository);
    }

    public Map<UUID, TrainingSchemeSegment> getTrainingSchemeSegments() {
        return findAll(trainingSchemeSegmentRepository);
    }

    public Map<UUID, ? extends AbstractTrainingSite> getTrainingSites() {
        Map<UUID, AbstractTrainingSite> sites = new HashMap<>();
        sites.putAll(findAll(clinicRepository));
        sites.putAll(findAll(departmentRepository));
        sites.putAll(findAll(doctorsOfficeRepository));

        return sites;
    }

    @Transactional
    private <T extends AbstractTrainingAppEntity, U extends TrainingAppEntityRepository<T>> Map<UUID, T> findAll(U repository) {
        Map<UUID, T> entities = new HashMap<>();

        for (T entity : repository.findAll()) {
            entities.put(entity.getId(), entity);
        }

        return entities;
    }
}
