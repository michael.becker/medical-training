package org.infai.ddls.medicaltrainingv2.test.data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.github.springtestdbunit.annotation.DatabaseSetup;

@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@DatabaseSetup("/coreDataSet.xml")
@DatabaseSetup("/trainingSchemesDataSet.xml")
public @interface DatabaseSetupTrainingSchemes {

}
