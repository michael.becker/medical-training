package org.infai.ddls.medicaltrainingv2.test.model.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.infai.ddls.medicaltrainingv2.data.filter.core.MedicalFieldFilter;
import org.infai.ddls.medicaltrainingv2.model.ChildRelationUtil;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.junit.jupiter.api.Test;

public class MedicalFieldTest {
    @org.junit.Test
    public void testGetViewName() throws Exception {
	assertEquals("medicalField", new MedicalField().getViewNameSingleEntity());
	assertEquals("medicalFields", new MedicalField().getViewNameMultipleEntities());

	assertEquals("medicalFieldFilter", new MedicalFieldFilter().getViewName());
    }

    @Test
    void testCheckDirectParentRelations() throws Exception {
	MedicalField unmittelbare = new MedicalField();
	unmittelbare.setName("unmittelbare");
	MedicalField allgemeinMedizin = new MedicalField();
	allgemeinMedizin.setName("allgemein");
	MedicalField hausarzt = new MedicalField();
	hausarzt.setName("hausarzt");

	assertFalse(ChildRelationUtil.isChildOf(unmittelbare, unmittelbare));
	assertFalse(ChildRelationUtil.isChildOf(allgemeinMedizin, allgemeinMedizin));
	assertFalse(ChildRelationUtil.isChildOf(hausarzt, hausarzt));

	assertFalse(ChildRelationUtil.isChildOf(allgemeinMedizin, unmittelbare));
	assertFalse(ChildRelationUtil.isChildOf(hausarzt, unmittelbare));

	allgemeinMedizin.setParent(unmittelbare);
	assertFalse(ChildRelationUtil.isChildOf(allgemeinMedizin, allgemeinMedizin));
	assertTrue(ChildRelationUtil.isChildOf(allgemeinMedizin, unmittelbare));
	assertFalse(ChildRelationUtil.isChildOf(unmittelbare, hausarzt));
	assertFalse(ChildRelationUtil.isChildOf(hausarzt, allgemeinMedizin));
	assertFalse(ChildRelationUtil.isChildOf(allgemeinMedizin, hausarzt));

	hausarzt.setParent(allgemeinMedizin);
	assertTrue(ChildRelationUtil.isChildOf(hausarzt, unmittelbare));
	assertTrue(ChildRelationUtil.isChildOf(hausarzt, allgemeinMedizin));
	assertFalse(ChildRelationUtil.isChildOf(unmittelbare, hausarzt));
	assertFalse(ChildRelationUtil.isChildOf(allgemeinMedizin, hausarzt));

	assertEquals(2, unmittelbare.getAllChildren().size());
	assertTrue(unmittelbare.getAllChildren().contains(allgemeinMedizin));
	assertTrue(unmittelbare.getAllChildren().contains(hausarzt));
    }

    @Test
    void testCheckIndirectParentRelations() throws Exception {
	MedicalField unmittelbarePatientenversorgung = new MedicalField();
	unmittelbarePatientenversorgung.setName("Unmittelbare Patientenversorgung");

	MedicalField hausarzt = new MedicalField();
	hausarzt.setName("Hausärztliche Versorgung");
	hausarzt.setParent(unmittelbarePatientenversorgung);
	unmittelbarePatientenversorgung.getChildren().add(hausarzt);
	MedicalField allgemeinMedizin = new MedicalField();
	allgemeinMedizin.setName("Allgemeinmedizin");
	allgemeinMedizin.setParent(hausarzt);
	hausarzt.getChildren().add(allgemeinMedizin);

	assertTrue(ChildRelationUtil.isChildOf(allgemeinMedizin, hausarzt));
	assertTrue(ChildRelationUtil.isChildOf(hausarzt, unmittelbarePatientenversorgung));
	assertTrue(ChildRelationUtil.isChildOf(allgemeinMedizin, unmittelbarePatientenversorgung));

	assertFalse(ChildRelationUtil.isChildOf(unmittelbarePatientenversorgung, allgemeinMedizin));

	assertEquals(2, unmittelbarePatientenversorgung.getAllChildren().size());
	assertTrue(unmittelbarePatientenversorgung.getAllChildren().contains(hausarzt));
	assertTrue(unmittelbarePatientenversorgung.getAllChildren().contains(allgemeinMedizin));
    }

    @Test
    void testThrowsExceptionWhenCreatingDirectCylce() throws Exception {
	MedicalField field = new MedicalField();
	assertThrows(RuntimeException.class, () -> field.setParent(field));
    }

    @Test
    void testThrowsExceptionWhenCreatingIndirectCycle() throws Exception {
	MedicalField parent = new MedicalField();
	parent.setName("parent");
	MedicalField child = new MedicalField();
	child.setName("child");

	child.setParent(parent);
	assertThrows(RuntimeException.class, () -> parent.setParent(child));
    }
}
