package org.infai.ddls.medicaltrainingv2.test.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.infai.ddls.medicaltrainingv2.controller.FilterAction;
import org.infai.ddls.medicaltrainingv2.controller.SaveAction;
import org.infai.ddls.medicaltrainingv2.data.filter.training.TrainingPositionFieldFilter;
import org.infai.ddls.medicaltrainingv2.model.core.TreatmentType;
import org.infai.ddls.medicaltrainingv2.model.location.DoctorsOffice;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingScheme;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingSchemeSegment;
import org.infai.ddls.medicaltrainingv2.test.data.AbstractSpringServiceTestJunit4;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

@AutoConfigureMockMvc
public abstract class AbstractSpringControllerTest extends AbstractSpringServiceTestJunit4 {
    protected static final String ROOT_URI = "http://localhost:8081";
    @Autowired
    protected MockMvc mvc;

    protected void addDoctorsOffice(MockHttpServletRequestBuilder requestBuilder, DoctorsOffice trainingSite) {
	requestBuilder.param("id", trainingSite.getId().toString());
	requestBuilder.param("name", trainingSite.getName());
	requestBuilder.param("address.street", trainingSite.getAddress().getStreet());
	requestBuilder.param("address.housenumber", trainingSite.getAddress().getHousenumber());
	requestBuilder.param("address.city", trainingSite.getAddress().getCity());
	requestBuilder.param("address.zipCode", trainingSite.getAddress().getZipCode());
	requestBuilder.param("medicalField.id", trainingSite.getMedicalField().getId().toString());
	requestBuilder.param("treatmentType", trainingSite.getTreatmentType().toString());
    }

    protected void addFilterAction(MockHttpServletRequestBuilder requestBuilder, FilterAction filterAction) {
	requestBuilder.param(FilterAction.REQUEST_PARAM, filterAction.toString());
    }

    protected void addRotationPlanSegment(MockHttpServletRequestBuilder requestBuilder, RotationPlanSegment rotationPlanSegment) {
	requestBuilder.param("segmentBegin", rotationPlanSegment.getSegmentBegin().toString());
	requestBuilder.param("segmentEnd", rotationPlanSegment.getSegmentEnd().toString());
	requestBuilder.param("fullTimeEquivalent", String.valueOf(rotationPlanSegment.getFullTimeEquivalent()));
	requestBuilder.param("rotationPlan.id", rotationPlanSegment.getRotationPlan().getId().toString());
	requestBuilder.param("trainingPosition.id", rotationPlanSegment.getTrainingPosition().getId().toString());
    }

    protected void addSaveAction(MockHttpServletRequestBuilder requestBuilder, SaveAction saveAction) {
	requestBuilder.param(SaveAction.REQUEST_PARAM, saveAction.toString());
    }

    protected void addTrainingPositionFieldFilter(MockHttpServletRequestBuilder requestBuilder, TrainingPositionFieldFilter filter) {
	if (null != filter.getAvailableFrom()) {
	    requestBuilder.param("availableFrom", String.valueOf(filter.getAvailableFrom()));
	}
	if (null != filter.getAvailableUntil()) {
	    requestBuilder.param("availableUntil", String.valueOf(filter.getAvailableUntil()));
	}
	requestBuilder.param("fullTimeEquivalent", String.valueOf(filter.getFullTimeEquivalent()));
	requestBuilder.param("medicalField.id", String.valueOf(filter.getMedicalField().getId()));
    }

    protected void addTrainingPositionFieldWithExistingAuthorisationAndExistingTrainingSite(MockHttpServletRequestBuilder requestBuilder, TrainingPositionField trainingPosition) {
	requestBuilder.param("id", trainingPosition.getId().toString());
	requestBuilder.param("authorisation.id", trainingPosition.getAuthorisation().getId().toString());
	requestBuilder.param("availableUntil", "");
	requestBuilder.param("availableFrom", "2018-08-06");
	requestBuilder.param("capacity", "1");
	requestBuilder.param("partTimeAvailable", "true");
    }

    protected void addTrainingScheme(MockHttpServletRequestBuilder requestBuilder, TrainingScheme trainingScheme) {
	requestBuilder.param("id", trainingScheme.getId().toString());
	requestBuilder.param("name", trainingScheme.getName());
	requestBuilder.param("description", trainingScheme.getDescription());
    }

    protected void addTrainingSchemeSegment(MockHttpServletRequestBuilder requestBuilder, TrainingSchemeSegment trainingSchemeSegment) {
	requestBuilder.param("id", trainingSchemeSegment.getId().toString());
	requestBuilder.param("trainingScheme.id", trainingSchemeSegment.getTrainingScheme().getId().toString());
	requestBuilder.param("medicalField.id", trainingSchemeSegment.getMedicalField().getId().toString());
	requestBuilder.param("permittedTreatmentTypes", TreatmentType.Inpatient.toString());
	requestBuilder.param("permittedTreatmentTypes", TreatmentType.Outpatient.toString());
	requestBuilder.param("duration", String.valueOf(trainingSchemeSegment.getDuration()));
    }

    protected MockHttpServletRequestBuilder postRequest(String url) {
	return post(url).with(csrf());
    }
}
