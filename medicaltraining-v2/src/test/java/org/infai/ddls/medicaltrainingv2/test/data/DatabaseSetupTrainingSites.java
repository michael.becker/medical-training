package org.infai.ddls.medicaltrainingv2.test.data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.github.springtestdbunit.annotation.DatabaseSetup;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@DatabaseSetup("/coreDataSet.xml")
@DatabaseSetup("/trainingSitesDataSet.xml")
public @interface DatabaseSetupTrainingSites {

}
