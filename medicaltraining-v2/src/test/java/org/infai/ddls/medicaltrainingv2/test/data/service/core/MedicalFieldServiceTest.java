package org.infai.ddls.medicaltrainingv2.test.data.service.core;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;

import org.infai.ddls.medicaltrainingv2.data.service.core.MedicalFieldService;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.infai.ddls.medicaltrainingv2.test.data.AbstractSpringServiceTestJunit4;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.springtestdbunit.annotation.DatabaseSetup;

public class MedicalFieldServiceTest extends AbstractSpringServiceTestJunit4 {
    @Autowired
    private MedicalFieldService service;

    @Test
    @DatabaseSetup("/coreDataSet.xml")
    public void testGetChildren() throws Exception {
	MedicalField unmittelbare = service.get(medicalfieldUnmittelbarePatientenversorgung().getId(), true, true);

	assertThat(unmittelbare.getChildren(), containsInAnyOrder(medicalfieldAnaesthesiologie(), medicalfieldChirurgie(), medicalfieldHausaerztliche(), medicalfieldInnere(), medicalfieldKJM()));
	assertThat(unmittelbare.getAllChildren(), containsInAnyOrder(medicalfieldAllgemeinmedizin(), medicalfieldAnaesthesiologie(), medicalfieldChirurgie(), medicalfieldHausaerztliche(), medicalfieldInnere(), medicalfieldKJM()));
    }

    @Test
    public void testNestMedicalFields() throws Exception {
	MedicalField unmittelbare = new MedicalField();
	unmittelbare.setName("Unmittelbare Patientenversorgung");
	unmittelbare = service.save(unmittelbare);

	MedicalField hausarzt = new MedicalField();
	hausarzt.setName("Hausärztliche Versorgung");
	hausarzt.setParent(unmittelbare);
	hausarzt = service.save(hausarzt);

	MedicalField allgemein = new MedicalField();
	allgemein.setName("Allgemeinmedizin");
	allgemein.setParent(hausarzt);
	allgemein = service.save(allgemein);

	MedicalField anaest = new MedicalField();
	anaest.setName("Anästhesiologie");
	anaest.setParent(unmittelbare);
	anaest = service.save(anaest);

	assertThat(service.get(unmittelbare.getId(), true, false).getChildren(), containsInAnyOrder(hausarzt, anaest));
	assertThat(service.get(unmittelbare.getId(), true, true).getAllChildren(), containsInAnyOrder(hausarzt, allgemein, anaest));
    }
}
