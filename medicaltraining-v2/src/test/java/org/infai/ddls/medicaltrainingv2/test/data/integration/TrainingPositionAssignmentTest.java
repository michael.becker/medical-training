package org.infai.ddls.medicaltrainingv2.test.data.integration;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.infai.ddls.medicaltrainingv2.data.filter.training.RotationPlanSegmentFilter;
import org.infai.ddls.medicaltrainingv2.data.service.training.RotationPlanSegmentService;
import org.infai.ddls.medicaltrainingv2.data.service.training.RotationPlanService;
import org.infai.ddls.medicaltrainingv2.data.service.training.TrainingPositionFieldService;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionAvailability;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;
import org.infai.ddls.medicaltrainingv2.test.data.AbstractSpringServiceTestJunit4;
import org.infai.ddls.medicaltrainingv2.test.data.DatabaseSetupRotationPlans;
import org.infai.ddls.medicaltrainingv2.test.data.DatabaseSetupTrainingPositions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TrainingPositionAssignmentTest extends AbstractSpringServiceTestJunit4 {
    @Autowired
    private TrainingPositionFieldService trainingPositionFieldService;
    @Autowired
    private RotationPlanService rotationPlanService;
    @Autowired
    private RotationPlanSegmentService rotationPlanSegmentService;

    @Test
    @DatabaseSetupRotationPlans
    @DatabaseSetupTrainingPositions
    public void testCapacityIsReducedWhenTrainingPositionIsAssignedToRotationPlanSegment() throws Exception {
	TrainingPositionField trainingPosition = trainingPositionFieldService.get(trainingPositionLANR1DepartmentCC().getId(), true);

	LocalDate from = LocalDate.parse("2018-01-01");
	LocalDate until = LocalDate.parse("2018-09-30");

	// prüfen, dass die Position momentan frei ist
	assertEquals(TrainingPositionAvailability.AVAILABLE, trainingPosition.getAvailability(from, until, rotationPlanSegmentService.get(new RotationPlanSegmentFilter(), null)));

	RotationPlanSegment segment = RotationPlanSegment.create(rotationPlanService.get(rotationPlanAiW2().getId()), trainingPosition, from, until, 100, false, false);
	segment = rotationPlanSegmentService.save(segment);

	// Kapazität 2: prüfen, dass die Position immer noch frei ist
	assertEquals(TrainingPositionAvailability.AVAILABLE, trainingPosition.getAvailability(from, until, rotationPlanSegmentService.get(new RotationPlanSegmentFilter(), null)));

	// WB-Stelle an R-Plan eines anderen AiW hängen, damit ist Kapazität
	// ausgeschöpft und Stelle ist nicht mehr frei
	segment = RotationPlanSegment.create(rotationPlanService.get(rotationPlanAiW3().getId()), trainingPosition, from, until, 100, false, false);
	segment = rotationPlanSegmentService.save(segment);
	assertEquals(TrainingPositionAvailability.OCCUPIED, trainingPosition.getAvailability(from, until, rotationPlanSegmentService.get(new RotationPlanSegmentFilter(), null)));
    }
}
