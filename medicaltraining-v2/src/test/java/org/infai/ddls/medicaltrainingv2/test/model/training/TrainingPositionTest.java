package org.infai.ddls.medicaltrainingv2.test.model.training;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionAvailability;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;
import org.infai.ddls.medicaltrainingv2.test.AbstractTrainingAppTest;
import org.junit.Before;
import org.junit.Test;

public class TrainingPositionTest extends AbstractTrainingAppTest {
    private TrainingPositionField trainingPositionField;

    @Before
    public void setUp() {
	trainingPositionField = new TrainingPositionField();
	trainingPositionField.setAvailableFrom(LocalDate.parse("2018-01-01"));
	trainingPositionField.setAvailableFrom(LocalDate.parse("2030-12-31"));
	trainingPositionField.setCapacity(2);
    }

    @Test
    public void testBlockedDatesBeginAfter() throws Exception {
	trainingPositionField.addExclusion(LocalDate.parse("2018-06-01"), LocalDate.parse("2018-10-31"));

	assertTrue(trainingPositionField.isBlocked(LocalDate.parse("2018-06-15"), LocalDate.parse("2018-09-30")));
	assertTrue(trainingPositionField.isBlocked(LocalDate.parse("2018-06-15"), LocalDate.parse("2018-11-30")));
	assertTrue(trainingPositionField.isBlocked(LocalDate.parse("2018-10-31"), LocalDate.parse("2018-11-30")));
	assertFalse(trainingPositionField.isBlocked(LocalDate.parse("2018-11-01"), LocalDate.parse("2018-11-30")));
    }

    @Test
    public void testBlockedDatesBeginBefore() throws Exception {
	trainingPositionField.addExclusion(LocalDate.parse("2018-06-01"), LocalDate.parse("2018-10-31"));

	assertTrue(trainingPositionField.isBlocked(LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31")));
	assertFalse(trainingPositionField.isBlocked(LocalDate.parse("2018-01-01"), LocalDate.parse("2018-05-31")));
	assertTrue(trainingPositionField.isBlocked(LocalDate.parse("2018-01-01"), LocalDate.parse("2018-06-01")));
	assertTrue(trainingPositionField.isBlocked(LocalDate.parse("2018-06-01"), LocalDate.parse("2018-06-01")));
	assertTrue(trainingPositionField.isBlocked(LocalDate.parse("2018-06-01"), LocalDate.parse("2018-09-30")));
    }

    @Test
    public void testGetAvailabilityAvailable() throws Exception {
	RotationPlanSegment segment1 = new RotationPlanSegment();
	segment1.setSegmentBegin(LocalDate.parse("2018-02-01"));
	segment1.setSegmentEnd(LocalDate.parse("2018-06-30"));
	segment1.setTrainingPosition(trainingPositionField);

	List<RotationPlanSegment> rotationPlanSegments = new ArrayList<>();
	rotationPlanSegments.add(segment1);

	assertEquals(TrainingPositionAvailability.AVAILABLE, trainingPositionField.getAvailability(LocalDate.parse("2017-10-01"), LocalDate.parse("2018-01-31"), rotationPlanSegments));
	assertEquals(TrainingPositionAvailability.AVAILABLE, trainingPositionField.getAvailability(LocalDate.parse("2018-01-01"), LocalDate.parse("2018-06-30"), rotationPlanSegments));
	assertEquals(TrainingPositionAvailability.AVAILABLE, trainingPositionField.getAvailability(LocalDate.parse("2018-06-01"), LocalDate.parse("2018-11-30"), rotationPlanSegments));
	assertEquals(TrainingPositionAvailability.AVAILABLE, trainingPositionField.getAvailability(LocalDate.parse("2018-07-01"), LocalDate.parse("2018-12-31"), rotationPlanSegments));
    }

    @Test
    public void testGetAvailabilityBlocked() throws Exception {
	trainingPositionField.addExclusion(LocalDate.parse("2018-06-01"), LocalDate.parse("2018-10-31"));

	assertEquals(TrainingPositionAvailability.BLOCKED, trainingPositionField.getAvailability(LocalDate.parse("2018-05-01"), LocalDate.parse("2018-06-30"), null));
	assertEquals(TrainingPositionAvailability.BLOCKED, trainingPositionField.getAvailability(LocalDate.parse("2018-07-01"), LocalDate.parse("2018-09-30"), null));
	assertEquals(TrainingPositionAvailability.BLOCKED, trainingPositionField.getAvailability(LocalDate.parse("2018-09-01"), LocalDate.parse("2018-11-30"), null));
    }

    @Test
    public void testGetAvailabilityOccupied() throws Exception {
	RotationPlanSegment segment1 = new RotationPlanSegment();
	segment1.setSegmentBegin(LocalDate.parse("2018-02-01"));
	segment1.setSegmentEnd(LocalDate.parse("2018-06-30"));
	segment1.setTrainingPosition(trainingPositionField);
	RotationPlanSegment segment2 = new RotationPlanSegment();
	segment2.setSegmentBegin(LocalDate.parse("2018-01-01"));
	segment2.setSegmentEnd(LocalDate.parse("2018-05-31"));
	segment2.setTrainingPosition(trainingPositionField);

	List<RotationPlanSegment> rotationPlanSegments = new ArrayList<>();
	rotationPlanSegments.add(segment1);
	rotationPlanSegments.add(segment2);

	assertEquals(TrainingPositionAvailability.OCCUPIED, trainingPositionField.getAvailability(LocalDate.parse("2018-01-01"), LocalDate.parse("2018-04-30"), rotationPlanSegments, 1));
	assertEquals(TrainingPositionAvailability.OCCUPIED, trainingPositionField.getAvailability(LocalDate.parse("2018-02-15"), LocalDate.parse("2018-05-15"), rotationPlanSegments, 1));
	assertEquals(TrainingPositionAvailability.OCCUPIED, trainingPositionField.getAvailability(LocalDate.parse("2018-05-01"), LocalDate.parse("2018-04-30"), rotationPlanSegments, 1));
    }

    @Test
    public void testGetAvailabilityOverlapping() throws Exception {
	RotationPlanSegment segment1 = new RotationPlanSegment();
	segment1.setSegmentBegin(LocalDate.parse("2018-02-01"));
	segment1.setSegmentEnd(LocalDate.parse("2018-06-30"));
	segment1.setTrainingPosition(trainingPositionField);
	RotationPlanSegment segment2 = new RotationPlanSegment();
	segment2.setSegmentBegin(LocalDate.parse("2018-02-01"));
	segment2.setSegmentEnd(LocalDate.parse("2018-06-30"));
	segment2.setTrainingPosition(trainingPositionField);

	List<RotationPlanSegment> rotationPlanSegments = new ArrayList<>();
	rotationPlanSegments.add(segment1);
	rotationPlanSegments.add(segment2);

	assertEquals(TrainingPositionAvailability.OVERLAPPING, trainingPositionField.getAvailability(LocalDate.parse("2017-10-01"), LocalDate.parse("2018-02-28"), rotationPlanSegments, 1));
	assertEquals(TrainingPositionAvailability.OVERLAPPING, trainingPositionField.getAvailability(LocalDate.parse("2018-06-01"), LocalDate.parse("2018-11-30"), rotationPlanSegments, 1));
	assertEquals(TrainingPositionAvailability.OVERLAPPING, trainingPositionField.getAvailability(LocalDate.parse("2018-01-01"), LocalDate.parse("2018-04-30"), rotationPlanSegments, 3));
    }

    @Test
    public void testNoBlockedDates() throws Exception {
	assertFalse(trainingPositionField.isBlocked(LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31")));
    }
}
