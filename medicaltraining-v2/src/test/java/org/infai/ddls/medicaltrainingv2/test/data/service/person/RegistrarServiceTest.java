package org.infai.ddls.medicaltrainingv2.test.data.service.person;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.infai.ddls.medicaltrainingv2.data.filter.core.MedicalFieldFilter;
import org.infai.ddls.medicaltrainingv2.data.filter.person.RegistrarFilter;
import org.infai.ddls.medicaltrainingv2.data.service.core.MedicalFieldService;
import org.infai.ddls.medicaltrainingv2.data.service.person.RegistrarService;
import org.infai.ddls.medicaltrainingv2.model.person.Registrar;
import org.infai.ddls.medicaltrainingv2.test.data.AbstractSpringServiceTestJunit4;
import org.infai.ddls.medicaltrainingv2.test.data.DatabaseSetupRotationPlans;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;

import com.github.springtestdbunit.annotation.DatabaseSetup;

public class RegistrarServiceTest extends AbstractSpringServiceTestJunit4 {
    @Autowired
    private RegistrarService registrarService;
    @Autowired
    private MedicalFieldService medicalFieldService;

    @Test
    @DatabaseSetupRotationPlans
    @Disabled
    public void testGetRegistrarsByActualTrainingPositions() throws Exception {
	RegistrarFilter filter = new RegistrarFilter();
	filter.setTrainingPosition("Park");

	List<Registrar> registrars = registrarService.get(filter, null);

//	assertThat(registrars, contains(regis));

//	assertList(registrars, personDAO.getRegistrar(1L), personDAO.getRegistrar(2L));
    }

    @Test
    @DatabaseSetup("/coreDataSet.xml")
    public void testInitialiseData() throws Exception {
	System.out.println(medicalFieldService.get(new MedicalFieldFilter(), Pageable.unpaged()));
    }

    @Test
    public void testListRegistrars() throws Exception {
	List<Registrar> registrars = registrarService.get(new RegistrarFilter(), Pageable.unpaged());

	assertNotNull(registrars);
    }
}
