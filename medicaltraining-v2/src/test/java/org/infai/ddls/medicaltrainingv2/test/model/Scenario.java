package org.infai.ddls.medicaltrainingv2.test.model;

import java.time.LocalDate;
import java.util.concurrent.ThreadLocalRandom;

import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.infai.ddls.medicaltrainingv2.model.core.TreatmentType;
import org.infai.ddls.medicaltrainingv2.model.location.Clinic;
import org.infai.ddls.medicaltrainingv2.model.location.Department;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlan;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingPositionField;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingScheme;
import org.infai.ddls.medicaltrainingv2.model.training.TrainingSchemeSegment;

public class Scenario {
    public static final MedicalField unmittelbarePatientenversorgung = createMedicalField("Unmittelbare Patientenversorgung", null);
    public static final MedicalField innereMedizin = createMedicalField("Innere Medizin", unmittelbarePatientenversorgung);
    public static final MedicalField chirurgie = createMedicalField("Chirurgie", unmittelbarePatientenversorgung);
    public static final MedicalField kjMedizin = createMedicalField("Kinder- und Jugendmedizin", unmittelbarePatientenversorgung);
    public static final MedicalField hausaerztlicheVersorgung = createMedicalField("Hausärztliche Versorgung", unmittelbarePatientenversorgung);
    public static final MedicalField allgemeinmedizin = createMedicalField("Allgemeinmedizin", hausaerztlicheVersorgung);

    public static final TrainingSchemeSegment innereMedizinSegment = createTrainingSchemeSegment(innereMedizin, 18, TreatmentType.Inpatient);
    public static final TrainingSchemeSegment chirurgieSegment = createTrainingSchemeSegment(chirurgie, 6, TreatmentType.Outpatient, TreatmentType.Inpatient);
    public static final TrainingSchemeSegment kjMedizinSegment = createTrainingSchemeSegment(kjMedizin, 6, TreatmentType.Outpatient, TreatmentType.Inpatient);
    public static final TrainingSchemeSegment unmittelbareSegment = createTrainingSchemeSegment(unmittelbarePatientenversorgung, 6, TreatmentType.Outpatient, TreatmentType.Inpatient);
    public static final TrainingSchemeSegment allgemeinMedizinSegment = createTrainingSchemeSegment(allgemeinmedizin, 18, TreatmentType.Outpatient);
    public static final TrainingSchemeSegment hausarztSegment = createTrainingSchemeSegment(hausaerztlicheVersorgung, 6, TreatmentType.Outpatient);

    public static final TrainingScheme allgemeinMedizinLSA = new TrainingScheme();

    static {
	allgemeinMedizinLSA.setName("FA Allgemeinmedizin LSA");
	allgemeinMedizinLSA.getTrainingSchemeSegments().add(innereMedizinSegment);
	allgemeinMedizinLSA.getTrainingSchemeSegments().add(chirurgieSegment);
	allgemeinMedizinLSA.getTrainingSchemeSegments().add(kjMedizinSegment);
	allgemeinMedizinLSA.getTrainingSchemeSegments().add(unmittelbareSegment);
	allgemeinMedizinLSA.getTrainingSchemeSegments().add(allgemeinMedizinSegment);
	allgemeinMedizinLSA.getTrainingSchemeSegments().add(hausarztSegment);
    }

    public static final AuthorisedDoctor weiterbilderInnere = new AuthorisedDoctor();
    public static final AuthorisedDoctor weiterbilderChirurgie = new AuthorisedDoctor();
    public static final AuthorisedDoctor weiterbilderKJM = new AuthorisedDoctor();
    public static final AuthorisedDoctor weiterbilderAllgemein = new AuthorisedDoctor();

    public static final AuthorisationField weiterbildungInnereAmb = createAuthorisation(innereMedizin, TreatmentType.Outpatient, weiterbilderInnere);
    public static final AuthorisationField weiterbildungInnereStat = createAuthorisation(innereMedizin, TreatmentType.Inpatient, weiterbilderInnere);

    public static final AuthorisationField weiterbildungChirurgieAmb = createAuthorisation(chirurgie, TreatmentType.Outpatient, weiterbilderChirurgie);
    public static final AuthorisationField weiterbildungChirurgieStat = createAuthorisation(chirurgie, TreatmentType.Inpatient, weiterbilderChirurgie);
    public static final AuthorisationField weiterbildungKJMAmb = createAuthorisation(kjMedizin, TreatmentType.Outpatient, weiterbilderKJM);
    public static final AuthorisationField weiterbildungKJMStat = createAuthorisation(kjMedizin, TreatmentType.Inpatient, weiterbilderKJM);
    public static final AuthorisationField weiterbildungAllgemeinAmb = createAuthorisation(allgemeinmedizin, TreatmentType.Outpatient, weiterbilderAllgemein);
    public static final AuthorisationField weiterbildungAllgemeinStat = createAuthorisation(allgemeinmedizin, TreatmentType.Inpatient, weiterbilderAllgemein);
    public static final TrainingPositionField positionInnereAmb = createTrainingPositionField(weiterbildungInnereAmb, LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31"), 2, true);
    public static final TrainingPositionField positionInnereStat = createTrainingPositionField(weiterbildungInnereStat, LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31"), 2, true);;

    public static final TrainingPositionField positionChirurgieAmb = createTrainingPositionField(weiterbildungChirurgieAmb, LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31"), 2, true);
    public static final TrainingPositionField positionChirurgieStat = createTrainingPositionField(weiterbildungChirurgieStat, LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31"), 2, true);
    public static final TrainingPositionField positionKJMAmb = createTrainingPositionField(weiterbildungKJMAmb, LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31"), 2, true);
    public static final TrainingPositionField positionKJMStat = createTrainingPositionField(weiterbildungKJMStat, LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31"), 2, true);
    public static final TrainingPositionField positionAllgemeinAmb = createTrainingPositionField(weiterbildungAllgemeinAmb, LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31"), 2, true);
    public static final TrainingPositionField positionAllgemeinStat = createTrainingPositionField(weiterbildungAllgemeinStat, LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31"), 2, true);
    public static final RotationPlanSegment segmentInnereStat = createRotationPlanSegment(positionInnereStat, LocalDate.parse("2018-01-01"), LocalDate.parse("2019-06-30"));
    public static final RotationPlanSegment segmentChirurgieAmb = createRotationPlanSegment(positionChirurgieAmb, LocalDate.parse("2019-07-01"), LocalDate.parse("2019-12-31"));

    public static final RotationPlanSegment segmentKJMAmb = createRotationPlanSegment(positionKJMAmb, LocalDate.parse("2020-01-01"), LocalDate.parse("2020-06-30"));

    public static final RotationPlanSegment segmentInnereAmb = createRotationPlanSegment(positionInnereAmb, LocalDate.parse("2020-07-01"), LocalDate.parse("2020-12-31"));
    public static final RotationPlanSegment segmentAllgemeinAmb1 = createRotationPlanSegment(positionAllgemeinAmb, LocalDate.parse("2021-01-01"), LocalDate.parse("2022-06-30"));
    public static final RotationPlanSegment segmentAllgemeinStat = createRotationPlanSegment(positionAllgemeinStat, LocalDate.parse("2022-07-01"), LocalDate.parse("2022-12-31"));

    public static final RotationPlan rotationplanAllgemeinmedizin = createRotationPlan(segmentInnereStat, segmentChirurgieAmb, segmentKJMAmb, segmentInnereAmb, segmentAllgemeinAmb1, segmentAllgemeinStat);

    public static MedicalField createMedicalField(String title, MedicalField parent) {
	MedicalField medicalField = new MedicalField();
	medicalField.setName(title);
	medicalField.setParent(parent);

	return medicalField;
    }

    public static RotationPlan createRotationPlan(RotationPlanSegment... rotationPlanSegments) {
	RotationPlan rotationPlan = new RotationPlan();

	for (RotationPlanSegment rotationPlanSegment : rotationPlanSegments) {
	    rotationPlan.getRotationPlanSegments().add(rotationPlanSegment);
	    rotationPlanSegment.setRotationPlan(rotationPlan);
	}

	return rotationPlan;
    }

    public static TrainingPositionField createTrainingPositionField(AuthorisationField authorisation, LocalDate avalilableFrom, LocalDate availableUntil, int capacity, boolean partTimeAvailable) {
	TrainingPositionField position = new TrainingPositionField();
	position.setAuthorisation(authorisation);
	position.setAvailableFrom(avalilableFrom);
	position.setAvailableUntil(availableUntil);
	position.setCapacity(capacity);
	position.setPartTimeAvailable(partTimeAvailable);

	return position;

    }

    public static TrainingSchemeSegment createTrainingSchemeSegment(MedicalField medicalField, int duration, TreatmentType... permittedTreatmentTypes) {
	TrainingSchemeSegment segment = new TrainingSchemeSegment();
	segment.setMedicalField(medicalField);
	segment.setDuration(duration);

	for (TreatmentType treatmentType : permittedTreatmentTypes) {
	    segment.getPermittedTreatmentTypes().add(treatmentType);
	}

	return segment;
    }

    private static AuthorisationField createAuthorisation(MedicalField medicalField, TreatmentType treatmentType, AuthorisedDoctor authorisedDoctor) {
	Clinic trainingSite = new Clinic();
	trainingSite.setName("Testinstitution " + ThreadLocalRandom.current().nextInt());
	Department trainingSiteDepartment = new Department();
	trainingSiteDepartment.setClinic(trainingSite);
	trainingSiteDepartment.setMedicalField(medicalField);
	trainingSiteDepartment.setTreatmentType(treatmentType);

	AuthorisationField authorisation = new AuthorisationField();
	authorisation.setAuthorisedDoctor(authorisedDoctor);
	authorisation.setTrainingSite(trainingSiteDepartment);
	authorisation.setValidFrom(LocalDate.parse("2018-01-01"));
	authorisation.setValidUntil(LocalDate.parse("2099-12-31"));

	return authorisation;
    }

    private static RotationPlanSegment createRotationPlanSegment(TrainingPositionField trainingPosition, LocalDate begin, LocalDate end) {
	RotationPlanSegment rotationPlanSegment = new RotationPlanSegment();
	rotationPlanSegment.setTrainingPosition(trainingPosition);
	rotationPlanSegment.setSegmentBegin(begin);
	rotationPlanSegment.setSegmentEnd(end);
	rotationPlanSegment.setFullTimeEquivalent(100);

	return rotationPlanSegment;
    }
}
