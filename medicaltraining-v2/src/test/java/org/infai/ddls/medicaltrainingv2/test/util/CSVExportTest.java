package org.infai.ddls.medicaltrainingv2.test.util;

import org.infai.ddls.medicaltrainingv2.test.data.AbstractSpringServiceTestJunit4;
import org.infai.ddls.medicaltrainingv2.test.data.DatabaseSetupRegistrars;
import org.infai.ddls.medicaltrainingv2.util.CSVExport;
import org.junit.Test;

public class CSVExportTest extends AbstractSpringServiceTestJunit4 {
    @Test
    @DatabaseSetupRegistrars
    public void testExportRegistrars() throws Exception {
        String result = new CSVExport().exportRegistrars(registrars.values());

        System.out.println(result);
    }
}
