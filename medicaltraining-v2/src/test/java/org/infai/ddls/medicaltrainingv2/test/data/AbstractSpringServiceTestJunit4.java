package org.infai.ddls.medicaltrainingv2.test.data;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import org.infai.ddls.medicaltrainingv2.model.authorisation.AuthorisationField;
import org.infai.ddls.medicaltrainingv2.model.core.Equipment;
import org.infai.ddls.medicaltrainingv2.model.core.MedicalField;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSite;
import org.infai.ddls.medicaltrainingv2.model.location.Clinic;
import org.infai.ddls.medicaltrainingv2.model.location.Department;
import org.infai.ddls.medicaltrainingv2.model.location.DoctorsOffice;
import org.infai.ddls.medicaltrainingv2.model.person.AuthorisedDoctor;
import org.infai.ddls.medicaltrainingv2.model.person.Registrar;
import org.infai.ddls.medicaltrainingv2.model.training.*;
import org.infai.ddls.medicaltrainingv2.test.AbstractTrainingAppTest;
import org.infai.ddls.medicaltrainingv2.test.TrainingAppTestConfiguration;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithSecurityContextTestExecutionListener;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = TrainingAppTestConfiguration.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class, WithSecurityContextTestExecutionListener.class})
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public abstract class AbstractSpringServiceTestJunit4 extends AbstractTrainingAppTest {

    protected Map<UUID, AuthorisedDoctor> authorisedDoctors;
    protected Map<UUID, MedicalField> medicalFields;
    protected Map<UUID, Equipment> equipments;
    protected Map<UUID, ? extends AbstractTrainingSite> trainingSites;
    protected Map<UUID, TrainingScheme> trainingSchemes;
    protected Map<UUID, TrainingSchemeSegment> trainingSchemeSegments;
    protected Map<UUID, AuthorisationField> authorisations;
    protected Map<UUID, TrainingPositionField> trainingPositions;
    protected Map<UUID, RotationPlan> rotationPlans;
    protected Map<UUID, RotationPlanSegment> rotationPlanSegments;
    protected Map<UUID, Registrar> registrars;
    @Autowired
    private DBScenario dbScenario;

    @Before
    public void initialiseData() {
        authorisedDoctors = dbScenario.getAuthorisedDoctors();
        medicalFields = dbScenario.getMedicalFields();
        equipments = dbScenario.getEquipments();
        trainingSites = dbScenario.getTrainingSites();
        trainingSchemes = dbScenario.getTrainingSchemes();
        trainingSchemeSegments = dbScenario.getTrainingSchemeSegments();
        authorisations = dbScenario.getAuthorisations();
        trainingPositions = dbScenario.getTrainingPositions();
        registrars = dbScenario.getRegistrars();
        rotationPlans = dbScenario.getRotationPlans();
        rotationPlanSegments = dbScenario.getRotationPlanSegments();
    }

    @SuppressWarnings("rawtypes")
    protected void assertDataInitialised(Map... data) {
        for (Map date : data) {
            assertFalse(date.isEmpty());
        }
    }

    protected AuthorisationField authorisationFieldLANR1atDepartmentCC() {
        return authorisations.get(UUID.fromString("39c5e6c8-24e0-4f6e-a807-a03f69bfdd25"));
    }

    protected AuthorisationField authorisationFieldLANR2atDepartmentAA() {
        return authorisations.get(UUID.fromString("5b604693-0528-413d-96e6-276c2d44c7ec"));
    }

    protected AuthorisationField authorisationFieldLANR3atDepartmentBB() {
        return authorisations.get(UUID.fromString("4009b668-9268-4cd9-a76e-a650b2511642"));
    }

    protected AuthorisationField authorisationFieldLANR4atDepartmentSA1() {
        return authorisations.get(UUID.fromString("50bb669e-0791-4530-aadf-50787355204f"));
    }

    protected AuthorisationField authorisationFieldLANR5atDepartmentBB1() {
        return authorisations.get(UUID.fromString("cb2b2d9f-f21e-4389-ba37-9dde7f9e4a9f"));
    }

    protected AuthorisationField authorisationFieldLANR6atDepartmentBB1() {
        return authorisations.get(UUID.fromString("207e3e9b-8721-428a-93ac-99ca53b52ff0"));
    }

    protected AuthorisationField authorisationFieldLANR6atDepartmentTS1() {
        return authorisations.get(UUID.fromString("e4170c4d-4703-4b8d-8a39-3ba3ff785b1c"));
    }

    protected AuthorisedDoctor authorisedDoctorLANR1() {
        return authorisedDoctors.get(UUID.fromString("a19ac9e9-bda3-4b40-be47-3ff7e8950f96"));
    }

    protected AuthorisedDoctor authorisedDoctorLANR2() {
        return authorisedDoctors.get(UUID.fromString("8592ccab-cc32-437f-a125-1b3b8c9fc7cf"));
    }

    protected AuthorisedDoctor authorisedDoctorLANR3() {
        return authorisedDoctors.get(UUID.fromString("3ae95c1a-162e-4505-aec9-03ada48f4b54"));
    }

    protected AuthorisedDoctor authorisedDoctorLANR4() {
        return authorisedDoctors.get(UUID.fromString("0d8b4526-b335-413c-9bfe-b53c68e32188"));
    }

    protected AuthorisedDoctor authorisedDoctorLANR5() {
        return authorisedDoctors.get(UUID.fromString("455574a6-c0fc-4e45-9dbe-e531aeb22a44"));
    }

    protected AuthorisedDoctor authorisedDoctorLANR6() {
        return authorisedDoctors.get(UUID.fromString("edd88ddd-e10f-42dc-8ed2-6cf92acfeb08"));
    }

    protected Clinic clinicKKHSchlossallee() {
        return (Clinic) trainingSites.get(UUID.fromString("bc110e0f-6691-4f70-8490-b68cf5a2f056"));
    }

    protected Clinic clinicParkkrankenhaus() {
        return (Clinic) trainingSites.get(UUID.fromString("411ce3a9-323e-42d2-a063-31b5257ca9fe"));
    }

    protected Clinic clinicPolyklinik() {
        return (Clinic) trainingSites.get(UUID.fromString("e3ce3aa1-5292-4f5e-8bf7-3b87114d5352"));
    }

    protected Clinic clinicPraxisBuellebow() {
        return (Clinic) trainingSites.get(UUID.fromString("fb14bfd8-97a0-472e-9401-7d38a29e065b"));
    }

    protected Department departmentKKHSchlossalleeSA1() {
        return (Department) trainingSites.get(UUID.fromString("04268290-57f3-497c-b2ed-c1bd214a8405"));
    }

    protected Department departmentParkkrankenhausAA() {
        return (Department) trainingSites.get(UUID.fromString("71610ef2-0a2f-4901-a8a4-4d8e1fe41b84"));
    }

    protected Department departmentParkkrankenhausBB() {
        return (Department) trainingSites.get(UUID.fromString("dc2bcede-aced-4b94-8dd5-5a0cf88bac5b"));
    }

    protected Department departmentParkkrankenhausCC() {
        return (Department) trainingSites.get(UUID.fromString("c0931a68-912d-43ad-a3f8-9e556aeec0ce"));
    }

    protected Department departmentPolyklinikTS1() {
        return (Department) trainingSites.get(UUID.fromString("3601e611-35e7-46e9-9d58-dbd38a9fb9f2"));
    }

    protected Department departmentPraxisBuellebowBB1() {
        return (Department) trainingSites.get(UUID.fromString("e63a7679-8756-46ce-8dde-81b722d54880"));
    }

    protected DoctorsOffice doctorsOfficePraxisOhneAbteilung() {
        return (DoctorsOffice) trainingSites.get(UUID.fromString("b59f2122-e89e-4227-9c38-90618e06ed3e"));
    }

    protected MedicalField medicalfieldAllgemeinmedizin() {
        return medicalFields.get(UUID.fromString("f316cd5b-e5b4-41e5-a200-e8ed9c533ff6"));
    }

    protected MedicalField medicalfieldAnaesthesiologie() {
        return medicalFields.get(UUID.fromString("18c5a33b-fe57-476c-a78e-2b4a183713a0"));
    }

    protected MedicalField medicalfieldChirurgie() {
        return medicalFields.get(UUID.fromString("d71edccf-bdac-430a-b0c8-d38445539dc7"));
    }

    protected MedicalField medicalfieldHausaerztliche() {
        return medicalFields.get(UUID.fromString("28972cc0-e979-4e49-b2f0-35b58af7ef2f"));
    }

    protected MedicalField medicalfieldInnere() {
        return medicalFields.get(UUID.fromString("a042cc07-75b1-40b1-9305-9b29452ad1f0"));
    }

    protected MedicalField medicalfieldKJM() {
        return medicalFields.get(UUID.fromString("a0d75b07-bf87-469a-a4a0-ca909efec98f"));
    }

    protected MedicalField medicalfieldUnmittelbarePatientenversorgung() {
        return medicalFields.get(UUID.fromString("8d344fe8-d735-4823-9614-59da26759775"));
    }

    protected Registrar registrarAiW1() {
        return registrars.get(UUID.fromString("2abf2db9-dd71-4f75-ab2f-2f25d6cda53d"));
    }

    protected Registrar registrarAiW2() {
        return registrars.get(UUID.fromString("e6b8ec3d-ff6e-4bd6-84c8-597dc7987677"));
    }

    protected Registrar registrarAiW3() {
        return registrars.get(UUID.fromString("22d721b9-6d2a-415a-bae7-216e91758417"));
    }

    protected RotationPlan rotationPlanAiW1() {
        return rotationPlans.get(UUID.fromString("218112ff-2c45-4779-9770-24d1c9924ff6"));
    }

    protected RotationPlan rotationPlanAiW2() {
        return rotationPlans.get(UUID.fromString("b76e1a12-add7-446b-8e6a-c101998088f6"));
    }

    protected RotationPlan rotationPlanAiW3() {
        return rotationPlans.get(UUID.fromString("c35d8132-ed98-4e66-87eb-7b04a83c90ac"));
    }

    protected RotationPlanSegment rotationPlanSegmentAiW1atLANR1DepartmentCC() {
        return rotationPlanSegments.get(UUID.fromString("016461d9-f22a-473f-bbd1-a2dde3281bdd"));
    }

    protected RotationPlanSegment rotationPlanSegmentAiW1atLANR2DepartmentAA() {
        return rotationPlanSegments.get(UUID.fromString("8585572f-4494-40f6-953e-db80f797fa2a"));
    }

    protected RotationPlanSegment rotationPlanSegmentAiW1atLANR3DepartmentBB() {
        return rotationPlanSegments.get(UUID.fromString("60256e3b-2875-411a-b5f1-f4ca97a3ad41"));
    }

    protected RotationPlanSegment rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_1() {
        return rotationPlanSegments.get(UUID.fromString("77c62291-b30b-4dce-855e-452ebb6a8b1c"));
    }

    protected RotationPlanSegment rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_2() {
        return rotationPlanSegments.get(UUID.fromString("9a5a4caa-9708-46ea-8d35-3bc19ddfa299"));
    }

    protected RotationPlanSegment rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_3() {
        return rotationPlanSegments.get(UUID.fromString("cdf12237-dfe1-49e8-99c9-535c4d1cf4e0"));
    }

    protected RotationPlanSegment rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_4() {
        return rotationPlanSegments.get(UUID.fromString("578aec90-2631-46d4-8fd8-665309bdad3f"));
    }

    protected RotationPlanSegment rotationPlanSegmentAiW2atLANR1DepartmentCC() {
        return rotationPlanSegments.get(UUID.fromString("9594ef7b-513b-4968-a729-25d718efd7f4"));
    }

    protected TrainingPositionField trainingPositionLANR1DepartmentCC() {
        return trainingPositions.get(UUID.fromString("1f0cb9a6-eb1d-4efc-b2e7-f10828e0d090"));
    }

    protected TrainingPositionField trainingPositionLANR2DepartmentAA() {
        return trainingPositions.get(UUID.fromString("7d19b869-3954-4964-b87b-c841869a1c12"));
    }

    protected TrainingPositionField trainingPositionLANR3DepartmentBB() {
        return trainingPositions.get(UUID.fromString("5d9a4bab-5fe3-4e08-99dc-3a43185c5777"));
    }

    protected TrainingPositionField trainingPositionLANR6DepartmentBB1_1() {
        return trainingPositions.get(UUID.fromString("cd05aa8e-0552-4f52-a2f1-2423cf56c181"));
    }

    protected TrainingPositionField trainingPositionLANR6DepartmentBB1_2() {
        return trainingPositions.get(UUID.fromString("a2085b50-3915-4f0a-a4e8-276e8547a320"));
    }

    protected TrainingPositionField trainingPositionLANR6DepartmentBB1_3() {
        return trainingPositions.get(UUID.fromString("de6ded23-8932-4d58-a6cb-14fed7c5c106"));
    }

    protected TrainingScheme trainingschemeAllgemeinmedizinLSA() {
        return trainingSchemes.get(UUID.fromString("44e74ec0-f682-4ac1-8316-1aca9428aeec"));
    }

    protected TrainingSchemeSegment trainingschemesegmentAllgemeinmedizinLSAAllgemeinmedizin() {
        return trainingSchemeSegments.get(UUID.fromString("d281e577-0a96-4d4a-a66d-6cbc3a691b45"));
    }

    protected TrainingSchemeSegment trainingschemesegmentAllgemeinmedizinLSAChirurgie() {
        return trainingSchemeSegments.get(UUID.fromString("ea467953-2082-4ce3-b59b-311621a754fc"));
    }

    protected TrainingSchemeSegment trainingschemesegmentAllgemeinmedizinLSAHausaerztliche() {
        return trainingSchemeSegments.get(UUID.fromString("5cc87d86-3ce4-4b1f-8e3d-ebaf8ec73aac"));
    }

    protected TrainingSchemeSegment trainingschemesegmentAllgemeinmedizinLSAInnereKlinik() {
        return trainingSchemeSegments.get(UUID.fromString("4d04beb1-b2d6-4c9d-a5ad-88b43da2f0b5"));
    }

    protected TrainingSchemeSegment trainingschemesegmentAllgemeinmedizinLSAKJM() {
        return trainingSchemeSegments.get(UUID.fromString("54a35ad2-9c03-4019-b6ba-106bb6e292de"));
    }

    protected TrainingSchemeSegment trainingschemesegmentAllgemeinmedizinLSAUnmittelbare() {
        return trainingSchemeSegments.get(UUID.fromString("07e284e6-af4a-4f7f-9ab6-7f46708a74db"));
    }

}
