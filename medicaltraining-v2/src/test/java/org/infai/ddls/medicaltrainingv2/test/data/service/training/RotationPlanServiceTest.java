package org.infai.ddls.medicaltrainingv2.test.data.service.training;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.assertj.core.util.Lists;
import org.infai.ddls.medicaltrainingv2.data.filter.training.RotationPlanFilter;
import org.infai.ddls.medicaltrainingv2.data.filter.training.RotationPlanSegmentFilter;
import org.infai.ddls.medicaltrainingv2.data.service.training.RotationPlanSegmentService;
import org.infai.ddls.medicaltrainingv2.data.service.training.RotationPlanService;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlan;
import org.infai.ddls.medicaltrainingv2.model.training.RotationPlanSegment;
import org.infai.ddls.medicaltrainingv2.test.data.AbstractSpringServiceTestJunit4;
import org.infai.ddls.medicaltrainingv2.test.data.DatabaseSetupRegistrars;
import org.infai.ddls.medicaltrainingv2.test.data.DatabaseSetupRotationPlans;
import org.infai.ddls.medicaltrainingv2.test.data.DatabaseSetupTrainingPositions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class RotationPlanServiceTest extends AbstractSpringServiceTestJunit4 {
    @Autowired
    private RotationPlanService service;
    @Autowired
    private RotationPlanSegmentService rotationPlanSegmentService;

    @Test
    @DatabaseSetupRotationPlans
    public void testCreateOverlappingRotationPlanSegmentFails() throws Exception {
	RotationPlan rotationPlan = new RotationPlan();
	rotationPlan.setRegistrar(registrarAiW1());
	rotationPlan.setTrainingScheme(trainingschemeAllgemeinmedizinLSA());

	rotationPlan = service.save(rotationPlan);

	RotationPlanSegment segment1 = RotationPlanSegment.create(rotationPlan, trainingPositionLANR1DepartmentCC(), LocalDate.parse("2017-01-01"), LocalDate.parse("2017-09-30"), 66, true, true);
	RotationPlanSegment segment2 = RotationPlanSegment.create(rotationPlan, trainingPositionLANR1DepartmentCC(), LocalDate.parse("2017-02-01"), LocalDate.parse("2018-03-31"), 100, false, false);

	rotationPlan.setRotationPlanSegments(Lists.newArrayList(segment1, segment2));

	try {
	    rotationPlan = service.save(rotationPlan);
	    fail("expected ConstraintViolationException");
	} catch (Exception e) {
	    assertEquals(ConstraintViolationException.class, e.getCause().getCause().getClass());
	}
    }

    @Test
    @DatabaseSetupRegistrars
    @DatabaseSetupTrainingPositions
    public void testCreateRotationPlan() throws Exception {
	assertDataInitialised(registrars, trainingSchemes, trainingPositions);

	RotationPlan rotationPlan = new RotationPlan();
	rotationPlan.setRegistrar(registrarAiW3());
	rotationPlan.setTrainingScheme(trainingschemeAllgemeinmedizinLSA());

	RotationPlanSegment segment1 = RotationPlanSegment.create(rotationPlan, trainingPositionLANR1DepartmentCC(), LocalDate.parse("2017-01-01"), LocalDate.parse("2017-09-30"), 66, true, true);
	RotationPlanSegment segment2 = RotationPlanSegment.create(rotationPlan, trainingPositionLANR1DepartmentCC(), LocalDate.parse("2017-10-01"), LocalDate.parse("2018-03-31"), 100, false, false);
	RotationPlanSegment segment3 = RotationPlanSegment.create(rotationPlan, trainingPositionLANR1DepartmentCC(), LocalDate.parse("2018-04-01"), LocalDate.parse("2018-09-30"), 100, false, false);
	RotationPlanSegment segment4 = RotationPlanSegment.create(rotationPlan, trainingPositionLANR1DepartmentCC(), LocalDate.parse("2018-10-01"), LocalDate.parse("2019-03-31"), 100, false, false);
	RotationPlanSegment segment5 = RotationPlanSegment.create(rotationPlan, trainingPositionLANR1DepartmentCC(), LocalDate.parse("2019-04-01"), LocalDate.parse("2020-03-31"), 100, false, false);
	RotationPlanSegment segment6 = RotationPlanSegment.create(rotationPlan, trainingPositionLANR1DepartmentCC(), LocalDate.parse("2020-09-01"), LocalDate.parse("2021-02-28"), 100, false, false);
	RotationPlanSegment segment7 = RotationPlanSegment.create(rotationPlan, trainingPositionLANR1DepartmentCC(), LocalDate.parse("2021-03-01"), LocalDate.parse("2021-08-31"), 100, false, false);

	rotationPlan.setRotationPlanSegments(Lists.newArrayList(segment1, segment2, segment3, segment4, segment5, segment6, segment7));
	rotationPlan = service.save(rotationPlan);

	assertEquals(registrarAiW3(), rotationPlan.getRegistrar());
	assertEquals(trainingschemeAllgemeinmedizinLSA(), rotationPlan.getTrainingScheme());
	assertThat(rotationPlan.getRotationPlanSegments(), containsInAnyOrder(segment1, segment2, segment3, segment4, segment5, segment6, segment7));
    }

    @Test
    @DatabaseSetupRotationPlans
    public void testFilterRotationPlanSegmentsByAuthorisation() throws Exception {
	RotationPlanSegmentFilter filter = new RotationPlanSegmentFilter();
	filter.setAuthorisationField(authorisationFieldLANR1atDepartmentCC());

	List<RotationPlanSegment> rotationPlanSegments = rotationPlanSegmentService.get(filter, null);
	assertThat(rotationPlanSegments, containsInAnyOrder(rotationPlanSegmentAiW1atLANR1DepartmentCC(), rotationPlanSegmentAiW2atLANR1DepartmentCC()));

	filter.setAuthorisationField(authorisationFieldLANR2atDepartmentAA());
	rotationPlanSegments = rotationPlanSegmentService.get(filter, null);
	assertThat(rotationPlanSegments, containsInAnyOrder(rotationPlanSegmentAiW1atLANR2DepartmentAA()));
    }

    @Test
    @DatabaseSetupRotationPlans
    public void testFilterRotationPlanSegmentsByTrainingPosition() throws Exception {
	RotationPlanSegmentFilter filter = new RotationPlanSegmentFilter();
	filter.setTrainingPositionField(trainingPositionLANR1DepartmentCC());

	List<RotationPlanSegment> rotationPlanSegments = rotationPlanSegmentService.get(filter, null);
	assertThat(rotationPlanSegments, containsInAnyOrder(rotationPlanSegmentAiW1atLANR1DepartmentCC(), rotationPlanSegmentAiW2atLANR1DepartmentCC()));

	filter.setTrainingPositionField(trainingPositionLANR6DepartmentBB1_1());
	rotationPlanSegments = rotationPlanSegmentService.get(filter, null);
	assertThat(rotationPlanSegments, containsInAnyOrder(rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_1(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_2(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_3(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_4()));
    }

    @Test
    @DatabaseSetupRotationPlans
    public void testGetActualRotationPlanSegmentOfARegistrar() throws Exception {
	RotationPlanSegmentFilter filter = new RotationPlanSegmentFilter();
	filter.setRegistrar(registrarAiW1());
	filter.setReferenceDate(LocalDate.parse("2018-02-01"));

	List<RotationPlanSegment> segments = rotationPlanSegmentService.get(filter, null);
	assertThat(segments, contains(rotationPlanSegmentAiW1atLANR2DepartmentAA()));
    }

    @Test
    @DatabaseSetupRotationPlans
    public void testGetActualSegmentsByDate() throws Exception {
	RotationPlan rotationPlan = service.get(rotationPlanAiW1().getId(), true, true);
	LocalDate now = LocalDate.parse("2017-12-01");

	assertNotNull(rotationPlan.getPlannedSegments(now));
	assertEquals(6, rotationPlan.getPlannedSegments(now).size());
    }

    @Test
    @DatabaseSetupRotationPlans
    public void testGetActualSegmentsByDateAndTrainingSchemeSegment() throws Exception {
	RotationPlan rotationPlan = service.get(rotationPlanAiW1().getId(), true, true);
	LocalDate now = LocalDate.parse("2017-12-01");

	List<RotationPlanSegment> rotationPlanSegments = rotationPlan.getPlannedSegments(now, trainingschemesegmentAllgemeinmedizinLSAUnmittelbare());
	assertThat(rotationPlanSegments, containsInAnyOrder(rotationPlanSegmentAiW1atLANR2DepartmentAA(), rotationPlanSegmentAiW1atLANR3DepartmentBB(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_3()));

	rotationPlanSegments = rotationPlan.getPlannedSegments(now, trainingschemesegmentAllgemeinmedizinLSAInnereKlinik());
	assertList(rotationPlanSegments, rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_4());

	assertTrue(rotationPlan.getPlannedSegments(now, trainingschemesegmentAllgemeinmedizinLSAChirurgie()).isEmpty());
	assertThat(rotationPlan.getPlannedSegments(now, trainingschemesegmentAllgemeinmedizinLSAAllgemeinmedizin()), containsInAnyOrder(rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_1(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_2()));
    }

    @Test
    @DatabaseSetupRotationPlans
    public void testGetFinishedSegmentsByDate() throws Exception {
	RotationPlan rotationPlan = service.get(rotationPlanAiW1().getId(), true, true);
	LocalDate now = LocalDate.parse("2017-12-01");

	assertThat(rotationPlan.getFinishedSegments(now), contains(rotationPlanSegmentAiW1atLANR1DepartmentCC()));
    }

    @Test
    @DatabaseSetupRotationPlans
    public void testGetFinishedSegmentsByDateAndTrainingSchemeSegment() throws Exception {
	RotationPlan rotationPlan = service.get(rotationPlanAiW1().getId(), true, true);
	LocalDate now = LocalDate.parse("2017-12-01");

	assertTrue(rotationPlan.getFinishedSegments(now, trainingschemesegmentAllgemeinmedizinLSAInnereKlinik()).isEmpty());
	assertThat(rotationPlan.getFinishedSegments(now, trainingschemesegmentAllgemeinmedizinLSAChirurgie()), contains(rotationPlanSegmentAiW1atLANR1DepartmentCC()));
    }

    @Test
    @DatabaseSetupRotationPlans
    public void testGetOrderedRotationPlanSegments() throws Exception {
	RotationPlanFilter filter = new RotationPlanFilter();
	filter.setRegistrar(registrarAiW1());

	List<RotationPlan> rotationPlans = service.get(filter, null, true, true);

	assertThat(rotationPlans, containsInAnyOrder(rotationPlanAiW1()));
	assertThat(rotationPlans.get(0).getRotationPlanSegments(), contains(rotationPlanSegmentAiW1atLANR1DepartmentCC(), rotationPlanSegmentAiW1atLANR2DepartmentAA(), rotationPlanSegmentAiW1atLANR3DepartmentBB(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_4(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_1(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_2(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_3()));
    }

    @Test
    @DatabaseSetupRotationPlans
    public void testGetRotationPlansByRegistrarWithoutPlans() throws Exception {
	RotationPlanFilter filter = new RotationPlanFilter();
	filter.setRegistrar(registrarAiW3());

	List<RotationPlan> rotationPlans = service.get(filter, null, true, true);

	assertThat(rotationPlans, contains(rotationPlanAiW3()));
	assertTrue(rotationPlans.get(0).getRotationPlanSegments().isEmpty());
    }

    @Test
    @DatabaseSetupRotationPlans
    public void testGetRotationPlanSegmentsByRotationPlan() throws Exception {
	RotationPlanSegmentFilter filter = new RotationPlanSegmentFilter();
	filter.setRotationPlan(rotationPlanAiW1());

	List<RotationPlanSegment> segments = rotationPlanSegmentService.get(filter, null);
	for (RotationPlanSegment segment : segments) {
	    System.out.println(segment.getSegmentBegin() + " -- " + segment.getSegmentEnd());
	}
	assertThat(segments, contains(rotationPlanSegmentAiW1atLANR1DepartmentCC(), rotationPlanSegmentAiW1atLANR2DepartmentAA(), rotationPlanSegmentAiW1atLANR3DepartmentBB(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_4(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_1(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_2(), rotationPlanSegmentAiW1atLANR6DepartmentBB1_1_3()));

	filter.setRotationPlan(rotationPlanAiW2());
	segments = rotationPlanSegmentService.get(filter, null);
	assertThat(segments, contains(rotationPlanSegmentAiW2atLANR1DepartmentCC()));

	filter.setRotationPlan(rotationPlanAiW3());
	segments = rotationPlanSegmentService.get(filter, null);
	System.out.println(segments);
	assertTrue(segments.isEmpty());
    }
}
