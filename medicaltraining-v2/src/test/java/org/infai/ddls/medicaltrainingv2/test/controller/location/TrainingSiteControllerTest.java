package org.infai.ddls.medicaltrainingv2.test.controller.location;

import org.infai.ddls.medicaltrainingv2.controller.SaveAction;
import org.infai.ddls.medicaltrainingv2.model.core.TreatmentType;
import org.infai.ddls.medicaltrainingv2.model.location.DoctorsOffice;
import org.infai.ddls.medicaltrainingv2.test.controller.AbstractSpringControllerTest;
import org.infai.ddls.medicaltrainingv2.test.data.DatabaseSetupCore;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TrainingSiteControllerTest extends AbstractSpringControllerTest {
    @Test
    @DatabaseSetupCore
    @WithMockUser
    public void testCreateDoctorsOffice() throws Exception {
        DoctorsOffice site = new DoctorsOffice();
        site.setName("Testpraxis");
        site.setMedicalField(medicalfieldAllgemeinmedizin());
        site.setTreatmentType(TreatmentType.Outpatient);
        site.getAddress().setStreet("Zschochersche Straße");
        site.getAddress().setHousenumber("94");
        site.getAddress().setZipCode("04229");
        site.getAddress().setCity("Leipzig");

        MockHttpServletRequestBuilder requestBuilder = postRequest("/locations/trainingsites/doctorsoffices/update").with(csrf());
        addSaveAction(requestBuilder, SaveAction.save);
        addDoctorsOffice(requestBuilder, site);

        ResultActions result = mvc.perform(requestBuilder);

        result.andDo(print());
        result.andExpect(status().isFound());
        result.andExpect(model().hasNoErrors());

//	result = mvc.perform(requestBuilder);
//	result.andDo(print());
//	result.andExpect(status().isFound());
//	result.andExpect(model().hasNoErrors());
    }
}
