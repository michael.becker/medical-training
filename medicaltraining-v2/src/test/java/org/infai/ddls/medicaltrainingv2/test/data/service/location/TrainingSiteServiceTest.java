package org.infai.ddls.medicaltrainingv2.test.data.service.location;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.infai.ddls.medicaltrainingv2.data.filter.location.ClinicFilter;
import org.infai.ddls.medicaltrainingv2.data.filter.location.TrainingSiteWithFilter;
import org.infai.ddls.medicaltrainingv2.data.service.location.TrainingSiteService;
import org.infai.ddls.medicaltrainingv2.model.core.Equipment;
import org.infai.ddls.medicaltrainingv2.model.core.TreatmentType;
import org.infai.ddls.medicaltrainingv2.model.location.AbstractTrainingSiteWith;
import org.infai.ddls.medicaltrainingv2.model.location.Clinic;
import org.infai.ddls.medicaltrainingv2.model.location.Department;
import org.infai.ddls.medicaltrainingv2.test.data.AbstractSpringServiceTestJunit4;
import org.infai.ddls.medicaltrainingv2.test.data.DatabaseSetupTrainingSites;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@DatabaseSetup("/coreDataSet.xml")
public class TrainingSiteServiceTest extends AbstractSpringServiceTestJunit4 {
    @Autowired
    private TrainingSiteService service;

    @Test
    public void testAddEquipment() throws Exception {
        Clinic trainingSite = new Clinic();
        trainingSite.setName("TestSite");
        trainingSite.getAddress().setStreet("Goerdelerring");
        trainingSite.getAddress().setHousenumber("9");
        trainingSite.getAddress().setZipCode("04109");
        trainingSite.getAddress().setCity("Leipzig");
        trainingSite = service.save(trainingSite);
        Department trainingSiteDepartment = new Department();
        trainingSiteDepartment.setName("TestSiteDepartment");
        trainingSiteDepartment.setClinic(trainingSite);
        trainingSiteDepartment.setMedicalField(medicalFields.get(UUID.fromString("a042cc07-75b1-40b1-9305-9b29452ad1f0")));
        trainingSiteDepartment.setTreatmentType(TreatmentType.Inpatient);
        trainingSiteDepartment.setAddress(trainingSite.getAddress());
        trainingSiteDepartment = service.save(trainingSiteDepartment);

        Equipment ct = equipments.get(UUID.fromString("67a77a9a-a705-4692-bacd-14f4e4752ccf"));
        Equipment mrt = equipments.get(UUID.fromString("2953c0ca-07d5-4793-821b-8638dc37bb98"));

        trainingSiteDepartment.getEquipments().add(ct);
        trainingSiteDepartment.getEquipments().add(mrt);
        trainingSiteDepartment = service.save(trainingSiteDepartment);

        assertCollection(trainingSiteDepartment.getEquipments(), ct, mrt);

        Department actualDepartment = service.getDepartment(trainingSite.getDepartments().get(0).getId(), true, true);
        assertCollection(actualDepartment.getEquipments(), ct, mrt);
    }

    @Test
    @DatabaseSetupTrainingSites
    public void testFilterTrainingSiteByMedicalField() throws Exception {
        TrainingSiteWithFilter<AbstractTrainingSiteWith> filter = new TrainingSiteWithFilter<>();
        filter.setMedicalField(medicalfieldUnmittelbarePatientenversorgung());

        List<AbstractTrainingSiteWith> trainingSites = service.get(filter, true, null);
        Object[] expectedSites = {this.trainingSites.get(UUID.fromString("71610ef2-0a2f-4901-a8a4-4d8e1fe41b84")), this.trainingSites.get(UUID.fromString("dc2bcede-aced-4b94-8dd5-5a0cf88bac5b")), this.trainingSites.get(UUID.fromString("e63a7679-8756-46ce-8dde-81b722d54880")), this.trainingSites.get(UUID.fromString("c0931a68-912d-43ad-a3f8-9e556aeec0ce")), this.trainingSites.get(UUID.fromString("b59f2122-e89e-4227-9c38-90618e06ed3e")), this.trainingSites.get(UUID.fromString("04268290-57f3-497c-b2ed-c1bd214a8405")), this.trainingSites.get(UUID.fromString("3601e611-35e7-46e9-9d58-dbd38a9fb9f2"))};
        assertThat(trainingSites, containsInAnyOrder(expectedSites));

        filter.setMedicalField(medicalfieldChirurgie());
        trainingSites = service.get(filter, true, null);
        assertThat(trainingSites, containsInAnyOrder(departmentParkkrankenhausCC(), departmentKKHSchlossalleeSA1()));
    }

    // @SuppressWarnings("deprecation")
    // @Test
    // @DatabaseSetup("/trainingSitesDataSet.xml")
    // @Transactional
    // @Ignore
    // public void testFilterTrainingSiteByZipCodeAndDistance() throws Exception {
    // initSQLFunctions();
    //
    // TrainingSiteFilter filter = new TrainingSiteFilter();
    // filter.setZipCode("04107");
    // filter.setDistance(112);
    // filter.setLatitude(11.0);
    // filter.setLongitude(10.0);
    //
    // List<TrainingSite> trainingSites = trainingSiteDAO.getTrainingSites(filter,
    // null, true, true);
    //
    // System.out.println(trainingSites);
    //
    // assertNotNull(trainingSites);
    // assertEquals(3, trainingSites.size());
    // }
    //

    @Test
    @DatabaseSetupTrainingSites
    public void testLoadDepartments() throws Exception {
        Clinic clinic = service.getClinic(clinicKKHSchlossallee().getId(), true, false);
        assertThat(clinic.getDepartments(), containsInAnyOrder(departmentKKHSchlossalleeSA1()));

        clinic = service.getClinic(clinicParkkrankenhaus().getId(), true, false);
        assertThat(clinic.getDepartments(), containsInAnyOrder(departmentParkkrankenhausAA(), departmentParkkrankenhausBB(), departmentParkkrankenhausCC()));

        clinic = service.getClinic(clinicPolyklinik().getId(), true, false);
        assertThat(clinic.getDepartments(), containsInAnyOrder(departmentPolyklinikTS1()));

        clinic = service.getClinic(clinicPraxisBuellebow().getId(), true, false);
        assertThat(clinic.getDepartments(), containsInAnyOrder(departmentPraxisBuellebowBB1()));
    }

    @Test
    @DatabaseSetupTrainingSites
    public void testOrderTrainingSiteDepartmentsByTrainingSiteNameAndDepartmentName() throws Exception {
        Clinic trainingSite = service.getClinic(clinicParkkrankenhaus().getId(), true, false);
        assertThat(trainingSite.getDepartments(), contains(departmentParkkrankenhausAA(), departmentParkkrankenhausBB(), departmentParkkrankenhausCC()));
    }

    @Test
    @DatabaseSetupTrainingSites
    public void testOrderTrainingSitesWithDepartmentsByName() throws Exception {
        List<Clinic> trainingSites = service.get(new ClinicFilter(), true, false, null);

        assertThat(trainingSites, contains(clinicKKHSchlossallee(), clinicParkkrankenhaus(), clinicPolyklinik(), clinicPraxisBuellebow()));
        assertThat(trainingSites.get(0).getDepartments(), contains(departmentKKHSchlossalleeSA1()));
        assertThat(trainingSites.get(1).getDepartments(), contains(departmentParkkrankenhausAA(), departmentParkkrankenhausBB(), departmentParkkrankenhausCC()));
        assertThat(trainingSites.get(2).getDepartments(), contains(departmentPolyklinikTS1()));
        assertThat(trainingSites.get(3).getDepartments(), contains(departmentPraxisBuellebowBB1()));
    }

    @Test
    public void testSaveTrainingSiteDepartment() throws Exception {
        Clinic trainingSite = new Clinic();
        trainingSite.setName("TrainingSite");
        trainingSite.getAddress().setStreet("Goerdelerring");
        trainingSite.getAddress().setHousenumber("9");
        trainingSite.getAddress().setZipCode("04109");
        trainingSite.getAddress().setCity("Leipzig");
        trainingSite = service.save(trainingSite);

        Department trainingSiteDepartment = new Department();
        trainingSiteDepartment.setClinic(trainingSite);
        trainingSiteDepartment.setAddress(trainingSite.getAddress());
        trainingSiteDepartment.setMedicalField(medicalfieldChirurgie());
        trainingSiteDepartment.setTreatmentType(TreatmentType.Inpatient);
        trainingSiteDepartment.setName("TrainingSiteDepartmet");

        trainingSiteDepartment = service.save(trainingSiteDepartment);

        assertThat(trainingSite, is(notNullValue()));
        assertThat(trainingSite.getId(), is(notNullValue()));
        assertThat(trainingSite.getDepartments(), is(notNullValue()));
        assertThat(trainingSite.getDepartments(), is(not(empty())));
        assertThat(trainingSite.getDepartments().get(0), is(notNullValue()));
        assertThat(trainingSite.getDepartments().get(0).getId(), is(notNullValue()));
    }
}
