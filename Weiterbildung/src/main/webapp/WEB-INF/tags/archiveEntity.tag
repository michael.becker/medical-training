<%@ tag body-content="empty" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ attribute name="entityPath" required="true" type="java.lang.String"%>
<%@ attribute name="entityUId" required="true" type="java.lang.Long"%>
<%@ attribute name="url" required="false" type="java.lang.String"%>
<%@ attribute name="title" required="false" type="java.lang.String"%>

<c:if test="${empty url}">
	<c:set var="url" value="${entityPath}/${entityUId}/archive" />
</c:if>

<c:url value="/resources/images/archive-button.png" var="archiveButton" />


<a href="<c:url value='${url}'/>" title="${title}"><img src="${archiveButton}" height="20px" /></a>