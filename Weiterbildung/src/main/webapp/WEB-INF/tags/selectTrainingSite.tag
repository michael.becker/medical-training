<%@ tag body-content="empty" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ attribute name="allowNull" required="false" type="java.lang.Boolean" %>
<%@ attribute name="comparatorDepartment" required="false" type="org.infai.ddls.medicaltraining.model.TrainingSite" %>
<%@ attribute name="size" required="false" type="java.lang.Integer" %>
<%@ attribute name="allowMultiple" required="false" type="java.lang.Boolean" %>
<%@ attribute name="path" required="false" type="java.lang.String" %>
<%@ attribute name="onchange" required="false" type="java.lang.String" %>


<c:if test="${empty allowNull}">
	<c:set var="allowEmpty" value="true" />
</c:if>

<c:if test="${empty comparatorDepartment}">
	<c:set var="comparatorDepartment" value="${authorisedDoctor.trainingSite}" />
</c:if>

<c:if test="${empty size}">
	<c:set var="size" value="1" />
</c:if>

<c:if test="${empty allowMultiple}">
	<c:set var="allowMultiple" value="false" />
</c:if>

<c:if test="${empty path}">
	<c:set var="path" value="trainingSite" />
</c:if>

<form:select path="${path}" multiple="${allowMultiple}" size="${size}" onchange="${onchange}" cssErrorClass="error">
	<c:if test="${allowNull eq true}">
		<form:option value="" label="----------" />
	</c:if>
	<c:forEach items="${trainingSites}" var="trainingSite">
		<c:choose>
			<c:when test="${empty trainingSite.children}">
				<c:choose>
					<c:when test="${comparatorDepartment.uid eq trainingSite.uid}">
						<form:option value="${trainingSite.uid}" selected="selected">${trainingSite.name}</form:option>
					</c:when>
					<c:otherwise>
						<form:option value="${trainingSite.uid}">${trainingSite.name}</form:option>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<form:option disabled="true" value="">${trainingSite.name}</form:option>
			</c:otherwise>
		</c:choose>
		<c:forEach items="${trainingSite.children}" var="trainingSiteDepartment">
			<c:choose>
				<c:when test="${comparatorDepartment.uid eq trainingSiteDepartment.uid}">
					<form:option value="${trainingSiteDepartment.uid}" selected="selected">&nbsp;&nbsp;&nbsp;${trainingSiteDepartment.name}</form:option>
				</c:when>
				<c:otherwise>
					<form:option value="${trainingSiteDepartment.uid}">&nbsp;&nbsp;&nbsp;${trainingSiteDepartment.name}</form:option>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</c:forEach>
</form:select>