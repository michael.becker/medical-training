function displayElement(htmlElement) {
	document.getElementById(htmlElement).style = "display: block;"
}

function hideElement(htmlElement) {
	document.getElementById(htmlElement).style = "display: none;";
}

function disableElement(htmlElement) {
	document.getElementById(htmlElement).disabled = true;
}

function enableElement(htmlElement) {
	document.getElementById(htmlElement).disabled = false;
}

function checkButton(button) {
	document.getElementById(button).checked=true;
}

function switchDisplay(htmlElement) {
	if (document.getElementById(htmlElement).style.display == "block") {
		document.getElementById(htmlElement).style.display = "none";
	} else {
		document.getElementById(htmlElement).style.display = "block";
	}
}

function switchCheck(htmlElement) {
	if (document.getElementById(htmlElement).checked == true) {
		document.getElementById(htmlElement).checked = false;
	} else {
		document.getElementById(htmlElement).checked = true;
	}
}