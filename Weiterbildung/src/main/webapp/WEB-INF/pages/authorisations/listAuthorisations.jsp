<%@ include file="/WEB-INF/pages/include/include.jsp"%>

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Übersicht Befugnisse</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main>
	<table class="datatable">
		<thead>
			<tr>
				<th></th>
				<th></th>
				<th><a href="authorisations/listAuthorisations?sort=name,asc">&darr;</a> <span>Weiterbildungsbefugter</span> <a href="authorisations/listAuthorisations?sort=name,desc">&uarr;</a></th>
				<th><a href="authorisations/listAuthorisations?sort=description,asc">&darr;</a> <span>Witerbildungsstätte</span> <a href="authorisations/listAuthorisations?sort=description,desc">&uarr;</a></th>
				<th><a href="authorisations/listAuthorisations?sort=duration,asc">&darr;</a> <span>Gebiet</span><a href="authorisations/listAuthorisations?sort=duration,desc">&uarr;</a></th>
				<th><a href="authorisations/listAuthorisations?sort=duration,asc">&darr;</a> <span>Gültig von</span><a href="authorisations/listAuthorisations?sort=duration,desc">&uarr;</a></th>
				<th><a href="authorisations/listAuthorisations?sort=duration,asc">&darr;</a> <span>Gültig bis</span><a href="authorisations/listAuthorisations?sort=duration,desc">&uarr;</a></th>
				<th>WBO</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${authorisations}" var="authorisation">
				<c:url var="editAuthorisation" value="/authorisations/${authorisation.uid}/edit" />
				<c:url var="deleteAuthorisation" value="/authorisations/${authorisation.uid}/delete" />
				<tr>
					<td><a href="${editAuthorisation}"><img src="${editButton}" /></a></td>
					<td><a href="${deleteAuthorisation}"><img src="${deleteButton}" /></a></td>
					<td><c:out value="${authorisation.authorisedDoctor.name}" /></td>
					<td><c:out value="${authorisation.trainingSiteDepartment.name}" /></td>
					<td><c:out value="${authorisation.trainingSiteDepartment.medicalField}" /> (${authorisation.trainingSiteDepartment.treatmentType})</td>
					<td><tags:localDate date="${authorisation.grantedOn}" /></td>
					<td><tags:localDate date="${authorisation.grantedTo}" /></td>
					<td>${authorisation.trainingScheme.name}</td>
				</tr>
			</c:forEach>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="8"><a href="<c:url value='/authorisations/create'/>">Neuen Datensatz anlegen</a></td>
			</tr>
		</tfoot>
		</main>
	</table>