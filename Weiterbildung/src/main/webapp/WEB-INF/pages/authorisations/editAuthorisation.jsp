<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="postURL" value="/authorisations/edit" />

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Befugnis bearbeiten</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="authorisation" action="${postURL}">
		<fieldset>
			<legend>Befugnis bearbeiten</legend>
			<c:if test="${0 != authorisation.uid}">
				<form:hidden path="uid" />
			</c:if>

			<form:label path="authorisedDoctor">Weiterbildungsbefugter</form:label>
			<form:select path="authorisedDoctor" items="${authorisedDoctors}" itemLabel="name" itemValue="uid" />
			<%@ include file="/WEB-INF/pages/trainingsites/include/selectTrainingSite.jsp"%>
			<form:label path="trainingScheme">Weiterbildungsordnung</form:label>
			<form:select path="trainingScheme" items="${trainingSchemes}" itemLabel="name" itemValue="uid" />
			<form:label path="grantedOn">G�ltig von</form:label>
			<form:input path="grantedOn" type="date" required="true" />
			<form:label path="grantedTo">G�ltig bis</form:label>
			<form:input path="grantedTo" type="date" required="true" />
			<form:label path="maximalDuration">Maximale Weiterbildungsdauer</form:label>
			<form:input path="maximalDuration" type="number" step="1" />
			<form:errors path="maximalDuration"></form:errors>
			<form:button>Speichern</form:button>
			<form:errors />
		</fieldset>
	</form:form> </main>
</body>
</html>
