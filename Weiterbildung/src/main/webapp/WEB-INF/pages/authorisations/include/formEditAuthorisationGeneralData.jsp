<%@ include file="/WEB-INF/pages/include/include.jsp"%>

<fieldset>
	<legend>Allgemeine Daten zur Befugnis</legend>
	<form:label path="trainingScheme">Weiterbildungsordnung</form:label>
	<form:select path="trainingScheme" items="${trainingSchemes}" itemLabel="name" itemValue="uid" />
	<form:label path="grantedOn">G�ltig von</form:label>
	<form:input path="grantedOn" type="date" cssErrorClass="error" />
	<form:errors path="grantedOn" cssClass="error" />
	<form:label path="grantedTo">G�ltig bis</form:label>
	<form:input path="grantedTo" type="date" cssErrorClass="error" />
	<form:errors path="grantedTo" cssClass="error" />
	<form:label path="maximalDuration">Maximale Weiterbildungsdauer</form:label>
	<form:input path="maximalDuration" type="number" step="1" cssErrorClass="error" />
	<form:errors path="maximalDuration" cssClass="error" />
</fieldset>