<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="post_url" value="/trainingsites/update" />


<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Weiterbildungsstätte bearbeiten</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
<script type="text/javascript" src="/medicaltraining/resources/displayElements.js"></script>
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="trainingSite" action="${post_url}">
		<%@ include file="/WEB-INF/pages/trainingsites/include/formEditTrainingSite.jsp"%>

		<c:choose>
			<c:when test="${trainingSite.trainingSiteType eq 'CLINIC'}">
				<c:set var="displayTrainingSiteDepartments" value="block" />
			</c:when>
			<c:otherwise>
				<c:set var="displayTrainingSiteDepartments" value="none" />
			</c:otherwise>
		</c:choose>

		<div id="trainingSiteDepartments" style="display: ${displayTrainingSiteDepartments}">
			<c:if test="${not empty trainingSite.uid}">
				<%@ include file="/WEB-INF/pages/trainingsites/include/listTrainingSiteDepartments.jsp"%>
			</c:if>
		</div>

		<form:button name="saveAction" value="save">Speichern</form:button>
		<form:button name="saveAction" value="saveAndClose">Speichern und Schließen</form:button>
	</form:form> </main>
</body>
</html>