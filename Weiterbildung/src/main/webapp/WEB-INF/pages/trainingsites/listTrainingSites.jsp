<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="filterTrainingSites" value="/trainingsites/filter" />


<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Übersicht Weiterbildungsstätten</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="trainingSiteFilter" action="${filterTrainingSites}">
		<fieldset>
			<legend>Weiterbildungsstätten filtern</legend>
			<form:label path="zipCode">Postleitzahl</form:label>
			<form:input path="zipCode" pattern="[0-9]{5}" required="true" />
			<form:label path="distance">Entfernung (km)</form:label>
			<form:input path="distance" type="number" step="10" />
			<form:label path="medicalField">Fachgebiet</form:label>
			<form:select path="medicalField" items="${medicalFields}" itemLabel="name" itemValue="uid" />
			<form:button>Filtern</form:button>
		</fieldset>
	</form:form>

	<fieldset>
		<legend>Übersicht Weiterbildungsstätten</legend>
		<table class="datatable">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th><a href="trainingsites/list?sort=name,asc">&darr;</a> <span>Name</span> <a href="trainingschemes/list?sort=name,desc">&uarr;</a></th>
					<th><a href="trainingsites/list?sort=street,asc">&darr;</a> <span>Anschrift</span> <a href="trainingschemes/list?sort=street,desc">&uarr;</a></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${trainingSites}" var="trainingSite">
					<tr>
						<td><a href="<c:url value='/trainingsites/${trainingSite.uid}/edit'/>"> <img src="${editButton}" alt="edit-button" title="edit ${trainingSite.name}" />
						</a></td>
						<td><a href="<c:url value='/trainingsites/${trainingSite.uid}/delete'/>"> <img src="${deleteButton}" alt="delete-button" title="delete ${trainingSite.name}" />
						</a>
						<td><c:out value="${trainingSite.name}" /></td>
						<td><c:out value="${trainingSite.address}" /></td>
					</tr>
					<c:forEach items="${trainingSite.children}" var="trainingSiteDepartment">
						<tr>
							<td></td>
							<td></td>
							<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${trainingSiteDepartment.name}&nbsp;(${trainingSiteDepartment.medicalField},&nbsp;${trainingSiteDepartment.treatmentType})</td>
						</tr>
					</c:forEach>
				</c:forEach>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="4"><a href="<c:url value='/trainingsites/create'/>">Neuen Datensatz hinzufügen</a></td>
				</tr>
			</tfoot>
		</table>
	</fieldset>
	</main>
</body>
</html>