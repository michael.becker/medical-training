<%@ include file="/WEB-INF/pages/include/include.jsp"%>

<fieldset>
	<legend>Abteilungen</legend>
	<table class="datatable">
		<thead>
			<tr>
				<th></th>
				<th></th>
				<th>Name</th>
				<th>Anschrift</th>
				<th>Fachgebiet</th>
				<th>Behandlungsart</th>
				<th>Ausstattung</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${trainingSite.children}" var="trainingSiteDepartment">
				<tr>
					<td><a href="<c:url value='/trainingsites/${trainingSite.uid}/trainingsitedepartments/${trainingSiteDepartment.uid}/edit'/>"> <img src="${editButton}" alt="edit-button" title="edit ${trainingSiteDepartment.name}" />
					</a></td>
					<td><a href="<c:url value='/trainingsites/${trainingSite.uid}/trainingsitedepartments/${trainingSiteDepartment.uid}/delete'/>"> <img src="${deleteButton}" alt="delete-button" title="delete ${trainingSiteDepartment.name}" />
					</a></td>
					<td>${trainingSiteDepartment.name}</td>
					<td>${trainingSiteDepartment.address}</td>
					<td>${trainingSiteDepartment.medicalField}</td>
					<td><spring:message code="${trainingSiteDepartment.treatmentType}" /></td>
					<td>${trainingSiteDepartment.equipments}</td>
				</tr>
			</c:forEach>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="7"><a href="<c:url value='/trainingsites/${trainingSite.uid}/trainingsitedepartments/create'/>">Neuen Datensatz hinzufügen</a></td>
			</tr>
		</tfoot>
	</table>
</fieldset>