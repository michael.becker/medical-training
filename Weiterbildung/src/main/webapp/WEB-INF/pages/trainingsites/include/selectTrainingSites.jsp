<%@ include file="/WEB-INF/pages/include/include.jsp"%>

<c:if test="${empty allowNull}">
	<c:set var="allowEmpty" value="true" />
</c:if>

<c:if test="${empty comparatorDepartment}">
	<c:set var="comparatorDepartment" value="${authorisedDoctor.trainingSites}" />
</c:if>

<c:if test="${empty size}">
	<c:set var="size" value="5" />
</c:if>

<c:if test="${empty path}">
	<c:set var="path" value="trainingSite" />
</c:if>

<form:select path="${path}" multiple="true" size="${size}">
	<c:if test="${allowNull eq true}">
		<form:option value="" label="----------" />
	</c:if>
	<c:forEach items="${trainingSites}" var="trainingSite">
		<c:choose>
			<c:when test="${empty trainingSite.children}">
				<c:choose>
					<c:when test="${comparatorDepartment.contains(trainingSite)}">
						<form:option value="${trainingSite.uid}" selected="true">${trainingSite.name}</form:option>
					</c:when>
					<c:otherwise>
						<form:option value="${trainingSite.uid}">${trainingSite.name}</form:option>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<form:option disabled="true" value="">${trainingSite.name}</form:option>
			</c:otherwise>
		</c:choose>
		<c:forEach items="${trainingSite.children}" var="trainingSiteDepartment">
			<c:choose>
				<c:when test="${comparatorDepartment.contains(trainingSiteDepartment)}">
					<form:option value="${trainingSiteDepartment.uid}" selected="true">&nbsp;&nbsp;&nbsp;${trainingSiteDepartment.name}</form:option>
				</c:when>
				<c:otherwise>
					<form:option value="${trainingSiteDepartment.uid}">&nbsp;&nbsp;&nbsp;${trainingSiteDepartment.name}</form:option>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</c:forEach>
</form:select>