<%@ include file="/WEB-INF/pages/include/include.jsp"%>

<c:if test="${empty size}">
	<c:set var="size" value="10" />
</c:if>

<c:if test="${empty allowNull}">
	<c:set var="allowEmpty" value="true" />
</c:if>

<c:if test=${empty allowMultiple}">
	<c:set var="allowMultiple" value="false"/>
</c:if>

<form:select path="trainingSiteDepartments" multiple="${allowMultiple}" size="${size}">
	<c:if test="${allowEmpty eq true}">
		<form:option value="" label="----------" />
	</c:if>
	<c:forEach items="${trainingSites}" var="trainingSite">
		<form:option disabled="true" value="">${trainingSite.name}</form:option>
		<c:forEach items="${trainingSite.trainingSiteDepartments}" var="trainingSiteDepartment">
			<c:choose>
				<c:when test="${authorisedDoctor.trainingSiteDepartments.contains(trainingSiteDepartment)}">
					<form:option value="${trainingSiteDepartment.uid}" selected="true">&nbsp;&nbsp;&nbsp;${trainingSiteDepartment.name}</form:option>
				</c:when>
				<c:otherwise>
					<form:option value="${trainingSiteDepartment.uid}">&nbsp;&nbsp;&nbsp;${trainingSiteDepartment.name}</form:option>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</c:forEach>
</form:select>