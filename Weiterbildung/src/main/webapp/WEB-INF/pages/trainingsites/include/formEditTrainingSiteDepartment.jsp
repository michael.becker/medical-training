<%@ include file="/WEB-INF/pages/include/include.jsp"%>

<fieldset>
	<legend>Abteilung bearbeiten</legend>
	<fieldset>
		<legend>Allgemeine Daten</legend>
		<form:label path="name">
			<spring:message code="name" />
			<form:input path="name" />
		</form:label>
		<form:errors path="name" cssClass="error" />
	</fieldset>

	<%@ include file="/WEB-INF/pages/location/formEditAddressData.jsp"%>
	<%@ include file="/WEB-INF/pages/include/editContactData.jsp"%>


	<fieldset>
		<legend>Fachgebiet</legend>
		Behandlungsart:&nbsp;
		<label for="inpatient">
			<form:radiobutton path="treatmentType" id="inpatient" value="Inpatient" />
			<spring:message code="Inpatient" />
		</label>
		<label for="outpatient">
			<form:radiobutton path="treatmentType" id="outpatient" value="Outpatient" />
			<spring:message code="Outpatient" />
		</label>
		<form:label path="medicalField">
		Medizinisches Fachgebiet
		<form:select path="medicalField" items="${medicalFields}" itemValue="uid" />
			</form:label> <form:errors path="treatmentType" cssClass="error" /> <form:errors path="medicalField" cssClass="error" />
	</fieldset>
	<fieldset>
		<legend>Ausstattung</legend>
		<form:checkboxes items="${equipments}" itemValue="uid" path="equipments" />
	</fieldset>
</fieldset>