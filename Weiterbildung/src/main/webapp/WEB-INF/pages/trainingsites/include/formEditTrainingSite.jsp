<%@ include file="/WEB-INF/pages/include/include.jsp"%>

<c:choose>
	<c:when test="${trainingSite.trainingSiteType eq 'CLINIC'}">
		<c:set var="displayAdditionalFields" value="none" />
	</c:when>
	<c:otherwise>
		<c:set var="displayAdditionalFields" value="block" />
	</c:otherwise>
</c:choose>


<fieldset>
	<legend>Weiterbildungsstätte bearbeiten</legend>
	<c:if test="${not empty trainingSite.uid}">
		<form:hidden path="uid" />
	</c:if>

	<fieldset>
		<legend>Allgemeine Daten</legend>
		<form:label path="name">
			<spring:message code="name" />
			<form:input path="name" cssErrorClass="error" />
		</form:label>
		<form:errors path="name" cssClass="error" />
		<label for="trainingSiteType1" onclick="hideElement('fieldsetTreatmentType');hideElement('fieldsetEquipment');displayElement('trainingSiteDepartments');"> <form:radiobutton path="trainingSiteType" value="CLINIC" /> <spring:message code="CLINIC" />
		</label> <label> <form:radiobutton path="trainingSiteType" value="DOCTORSOFFICE" onclick="displayElement('fieldsetTreatmentType');displayElement('fieldsetEquipment');hideElement('trainingSiteDepartments');" /> <spring:message code="DOCTORSOFFICE" />
		</label>
		<form:errors path="trainingSiteType" />
	</fieldset>

	<%@ include file="/WEB-INF/pages/location/formEditAddressData.jsp"%>
	<%@ include file="/WEB-INF/pages/include/editContactData.jsp"%>


	<fieldset id="fieldsetTreatmentType" style="display: ${displayAdditionalFields}">
		<form:errors cssClass="error" />
		<legend>Fachgebiet</legend>
		<ul class="inline">
			<li><label for="treatmentType1"> <form:radiobutton path="treatmentType" value="Inpatient" cssErrorClass="error" /> <spring:message code="Inpatient" /></label></li>
			<li><label for="treatmentType2"> <form:radiobutton path="treatmentType" value="Outpatient" cssErrorClass="error" /> <spring:message code="Outpatient" /></label></li>
		</ul>
		<form:errors path="treatmentType" cssClass="error" />
		<form:select path="medicalField" items="${medicalFields}" itemValue="uid" cssErrorClass="error" />
	</fieldset>
	<fieldset id="fieldsetEquipment" style="display: ${displayAdditionalFields}">
		<legend>Ausstattung</legend>
		<form:checkboxes items="${equipments}" itemValue="uid" path="equipments" />
	</fieldset>
</fieldset>