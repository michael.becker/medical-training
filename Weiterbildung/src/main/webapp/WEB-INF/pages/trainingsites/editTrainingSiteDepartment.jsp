<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="post_url" value="/trainingsites/${trainingSiteDepartment.parent.uid}/trainingsitedepartments/update" />


<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Abteilung ${trainingSiteDepartment.name} in ${trainingSiteDepartment.parent.name} bearbeiten</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="trainingSiteDepartment" action="${post_url}">
		<c:if test="${not empty trainingSiteDepartment.uid}">
			<form:hidden path="uid" />
		</c:if>
		<form:hidden path="parent.uid" />

		<%@ include file="/WEB-INF/pages/trainingsites/include/formEditTrainingSiteDepartment.jsp"%>

		<form:button>Speichern</form:button>
	</form:form> </main>
</body>
</html>