<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="filterTrainingPositions" value="/training/listRotationPlanSegments" />
<c:url var="addRotationPlanSegment" value="/training/addRotationPlanSegment" />

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Weiterbildungsstation f�r ${registrar.name} bearbeiten</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="filter" action="${filterTrainingPositions}/${rotationPlanSegment.rotationPlan.uid}">
		<fieldset>
			<legend>Daten der geplanten Weiterbildungsstation</legend>
			<form:label path="availableFrom">Beginn</form:label>
			<form:input path="availableFrom" type="date" cssErrorClass="error" />
			<form:errors path="availableFrom" cssClass="error" />
			<form:label path="availableUntil">Ende</form:label>
			<form:input path="availableUntil" type="date" cssErrorClass="error" />
			<form:errors path="availableUntil" cssClass="error" />
			<form:label path="medicalField">Fachgebiet</form:label>
			<form:select path="medicalField">
				<form:options items="${medicalFields}" itemValue="uid" />
			</form:select>
			<form:label path="fullTimeEquivalent">Vollzeit�quivalente</form:label>
			<form:input path="fullTimeEquivalent" type="number" step="0.01" min="0.01" max="1.0" />
			<form:errors class="error" />
			<form:button>verf�gbare Weiterbildungsstellen finden</form:button>
		</fieldset>
	</form:form> <form:form method="post" modelAttribute="rotationPlanSegment" action="${addRotationPlanSegment}">
		<form:hidden path="rotationPlan.uid" />
		<form:hidden path="rotationPlan.registrar.uid" />
		<form:hidden path="begin" />
		<form:hidden path="end" />
		<form:hidden path="fullTimeEquivalent" />
		<fieldset>
			<legend> Verf�gbare Weiterbildungsstellen </legend>
			<table class="datatable">
				<thead>
					<tr>
						<th><spring:message code="trainingSite" /></th>
						<th><spring:message code="medicalField" /></th>
						<th><spring:message code="trainingPosition.availableFrom" /></th>
						<th><spring:message code="trainingPosition.availableUntil" /></th>
						<th><spring:message code="trainingPosition.capacity" /></th>
						<th><spring:message code="trainingPosition.partTimeAvailable" /></th>
						<th><spring:message code="trainingPosition.availability" /></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${trainingPositions}" var="trainingPosition">
						<tr class="${trainingPosition.availability}">
							<td><c:if test="${not empty trainingPosition.authorisationField.trainingSite.parent}">${trainingPosition.authorisationField.trainingSite.parent}&nbsp;-&nbsp;</c:if>${trainingPosition.authorisationField.trainingSite}</td>
							<td>${trainingPosition.authorisationField.trainingSite.medicalField}</td>
							<td><tags:localDate date="${trainingPosition.availableFrom}" pattern="dd.MM.yyyy" /></td>
							<td><tags:localDate date="${trainingPosition.availableUntil}" pattern="dd.MM.yyyy" /></td>
							<td>${trainingPosition.capacity}</td>
							<td><spring:message code="${trainingPosition.partTimeAvailable}" /></td>
							<td><spring:message code="${trainingPosition.availability }" /></td>
							<td><c:if test="${trainingPosition.availability ne 'OCCUPIED' && trainingPositon.availability ne 'BLOCKED'}">
									<form:button name="trainingPosition" value="${trainingPosition.uid}">ausw�hlen</form:button>
								</c:if></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</fieldset>
		<p>
			<form:errors path="begin" />
		</p>
		<p>
			<form:errors path="end" />
		</p>
		<form:errors path="*" class="error" />
	</form:form> </main>
</body>
</html>

