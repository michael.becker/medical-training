<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="post_url" value="/training/updateRotationPlanSegment" />

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Weiterbildungsplan f�r ${registrar.name} bearbeiten</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="rotationPlanSegment" action="${post_url}">
		<fieldset>
			<legend>Weiterbildungsabschnitt bearbeiten</legend>
			<c:if test="${0 != rotationPlanSegment.uid}">
				<form:hidden path="uid" />
				<form:hidden path="trainingPosition.uid" />
				<form:hidden path="begin" />
				<form:hidden path="end" />
				<form:hidden path="fullTimeEquivalent" />
				<form:hidden path="rotationPlan.uid" />
				<form:hidden path="rotationPlan.registrar.uid" />
			</c:if>

			<form:label path="assignedSegment">Abschnitt der Weiterbildungsordnung</form:label>
			<form:select path="assignedSegment">
				<form:option value="" label="------------" />
				<form:options items="${fundableSegments}" itemValue="uid" />
			</form:select>

			<form:label path="funded">Gef�rdert</form:label>
			<form:checkbox path="funded" />
			<form:label path="approved">Anerkannt</form:label>
			<form:checkbox path="approved" />

			<form:button>Speichern</form:button>
		</fieldset>
	</form:form> </main>
</body>
</html>