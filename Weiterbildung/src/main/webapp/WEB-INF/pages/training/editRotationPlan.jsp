<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="post_url" value="/training/editRotationPlan" />

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Weiterbildungsplan f�r ${registrar.name} bearbeiten</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main>
	<h1>
		<spring:message code="editRotationPlan" arguments="${registrar.name}" />
	</h1>
	<fieldset>
		<legend>�bersicht Weiterbildungsplan ${rotationPlan.trainingScheme.name}</legend>
		F�rderh�chstdauer: ${rotationPlan.trainingScheme.duration} Monate (bei Vollzeit)
		<table class="datatable">
			<thead>
				<tr>
					<th>Gebiet</th>
					<th>station�r</th>
					<th>ambulant</th>
					<th>H�chstdauer</th>
					<th>Bereits abgeleistet</th>
					<th>Noch geplant</th>
					<th>Gesamt</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${rotationPlan.trainingScheme.trainingSchemeSegments}" var="trainingSchemeSegment">
					<tr>
						<td>${trainingSchemeSegment.medicalField}</td>
						<td>${trainingSchemeSegment.inpatient }</td>
						<td>${trainingSchemeSegment.outpatient }</td>
						<td>${trainingSchemeSegment.duration}</td>
						<td>${finishedSegmentsDuration[trainingSchemeSegment.medicalField.uid]}</td>
						<td>${actualSegmentsDuration[trainingSchemeSegment.medicalField.uid]}</td>
						<c:set var="overallDuration" value="${finishedSegmentsDuration[trainingSchemeSegment.medicalField.uid] + actualSegmentsDuration[trainingSchemeSegment.medicalField.uid]}" />
						<c:choose>
							<c:when test="${overallDuration gt trainingSchemeSegment.duration}">
								<td style="color: red; font-weight: bold"><c:out value="${overallDuration }"></c:out></td>
							</c:when>
							<c:otherwise>
								<td><c:out value="${overallDuration}"></c:out></td>
							</c:otherwise>
						</c:choose>
					</tr>
				</c:forEach>
				<tr>
					<td colspan="6">Nicht zugewiesene Abschnitte</td>
					<td>${unassignedSegmentsSum}</td>
				</tr>
				<tr>
					<td colspan="3">Summe</td>
					<td class="emphasised">${rotationPlan.trainingScheme.duration}</td>
					<td class="emphasised">${finishedSegmentsDurationSum}</td>
					<td class="emphasised">${actualSegmentsDurationSum}</td>
					<td class="emphasised">${actualSegmentsDurationSum + finishedSegmentsDurationSum + unassignedSegmentsSum}</td>
				</tr>
			</tbody>
		</table>
	</fieldset>

	<fieldset>
		<legend>Bisherige Weiterbildungsstationen</legend>
		<table class="datatable">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th>Weiterbildungsstelle</th>
					<th>Gebiet</th>
					<th>Beginn</th>
					<th>Ende</th>
					<th>Dauer (Monate)</th>
					<th>VZ�</th>
					<th>Dauer (normiert)</th>
					<th>Zugewiesener Abschnitt</th>
					<th>Gef�rdert</th>
					<th>Anerkannt</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${finishedSegments}" var="rotationPlanSegment">
					<tr>
						<td><a href="<c:url value='/training/rotationplansegments/${rotationPlanSegment.uid}/edit'/>"><img src="${editButton}" /></a></td>
						<td><a href="<c:url value='/training/rotationplansegments/${rotationPlanSegment.uid}/delete'/>"><img src="${deleteButton}" /></a></td>
						<td><a href="<c:url value='/trainingpositions/${rotationPlanSegment.trainingPosition.uid}/segments'/>"> <c:if test="${not empty rotationPlanSegment.trainingPosition.authorisationField.trainingSite.parent}">
								${rotationPlanSegment.trainingPosition.authorisationField.trainingSite.parent}&nbps;-&nbsp;
							</c:if> ${rotationPlanSegment.trainingPosition.authorisationField.trainingSite.name}&nbsp;(${rotationPlanSegment.trainingPosition.authorisationField.authorisedDoctor})
						</a></td>
						<td>${rotationPlanSegment.trainingPosition.authorisationField.trainingSite.medicalField}(<spring:message code="${rotationPlanSegment.trainingPosition.authorisationField.trainingSite.treatmentType}" />)
						</td>
						<td><tags:localDate date="${rotationPlanSegment.begin}" pattern="dd.MM.yyyy" /></td>
						<td><tags:localDate date="${rotationPlanSegment.end}" pattern="dd.MM.yyyy" /></td>
						<td>${rotationPlanSegment.duration}</td>
						<td>${rotationPlanSegment.fullTimeEquivalent}</td>
						<td>${rotationPlanSegment.normalisedDuration}</td>
						<td>${rotationPlanSegment.assignedSegment}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</fieldset>

	<fieldset>
		<legend>Aktuelle und geplante Weiterbildungsstationen</legend>
		<table class="datatable">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th>Weiterbildungsstelle</th>
					<th>Gebiet</th>
					<th>Beginn</th>
					<th>Ende</th>
					<th>Dauer (Monate)</th>
					<th>VZ�</th>
					<th>Dauer (normiert)</th>
					<th>Zugewiesener Abschnitt</th>
					<th>F�rderf�hig</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${actualSegments}" var="rotationPlanSegment">
					<tr>
						<td><a href="<c:url value='/training/rotationplansegments/${rotationPlanSegment.uid}/edit'/>"><img src="${editButton}" /></a></td>
						<td><a href="<c:url value='/training/rotationplansegments/${rotationPlanSegment.uid}/delete'/>"><img src="${deleteButton}" /></a></td>
						<td><a href="<c:url value='/trainingpositions/${rotationPlanSegment.trainingPosition.uid}/segments'/>"> <c:if test="${not empty rotationPlanSegment.trainingPosition.authorisationField.trainingSite.parent}">
								${rotationPlanSegment.trainingPosition.authorisationField.trainingSite.parent}&nbps;-&nbsp;
							</c:if> ${rotationPlanSegment.trainingPosition.authorisationField.trainingSite.name}&nbsp;(${rotationPlanSegment.trainingPosition.authorisationField.authorisedDoctor})
						</a></td>
						<td>${rotationPlanSegment.trainingPosition.authorisationField.trainingSite.medicalField}&nbsp;(<spring:message code="${rotationPlanSegment.trainingPosition.authorisationField.trainingSite.treatmentType}" />)
						</td>
						<td><tags:localDate date="${rotationPlanSegment.begin}" pattern="dd.MM.yyyy" /></td>
						<td><tags:localDate date="${rotationPlanSegment.end}" pattern="dd.MM.yyyy" /></td>
						<td>${rotationPlanSegment.duration}</td>
						<td>${rotationPlanSegment.fullTimeEquivalent}</td>
						<td>${rotationPlanSegment.normalisedDuration}</td>
						<td>${rotationPlanSegment.assignedSegment}</td>
						<td class="${rotationPlanSegment.fundableType}"><spring:message code="${rotationPlanSegment.fundableType}" /></td>
					</tr>
				</c:forEach>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="11"><a href="<c:url value='/training/listRotationPlanSegments/${rotationPlan.uid}'/>">Weiterbildungsstation hinzuf�gen</a></td>
				</tr>
			</tfoot>
		</table>
	</fieldset>
	<fieldset>
		<legend>�nderungshistorie</legend>
		<table class="datatable">
			<thead>
				<tr>
					<th>#</th>
					<th>�nderungsart</th>
					<th>�nderung am</th>
					<th>�nderung von</th>
					<th>Weiterbildungsabschnitte</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${revisions}" var="revision">
					<jsp:useBean id="dateValue" class="java.util.Date" />
					<jsp:setProperty name="dateValue" property="time" value="${revision.userRevision.timestamp}" />
					<tr>
						<td>${revision.userRevision.id}</td>
						<td>${revision.revisionTypeText}</td>
						<td><fmt:formatDate value="${dateValue}" pattern="dd.MM.yyyy - HH:mm:ss" /></td>
						<td>${revision.userRevision.userName}</td>
						<td><c:if test="${fn:length(revision.revisionEntity.rotationPlanSegments) gt 0}">
								<table class="datatable">
									<thead class="light">
										<tr>
											<th>Weiterbildungsst�tte</th>
											<th>Gebiet</th>
											<th>Beginn</th>
											<th>Ende</th>
											<th>Dauer (Monate)</th>
											<th>VZ�</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${revision.revisionEntity.rotationPlanSegments}" var="rotationPlanSegment">
											<tr>
												<td>${rotationPlanSegment.trainingPosition.authorisationField.trainingSite.name}:&nbsp;(${rotationPlanSegment.trainingPosition.authorisationField.authorisedDoctor})</td>
												<td>${rotationPlanSegment.trainingPosition.authorisationField.trainingSite.medicalField}&nbsp;(<spring:message code="${rotationPlanSegment.trainingPosition.authorisationField.trainingSite.treatmentType}" />)
												</td>
												<td><tags:localDate date="${rotationPlanSegment.begin}" pattern="dd.MM.yyyy" /></td>
												<td><tags:localDate date="${rotationPlanSegment.end}" pattern="dd.MM.yyyy" /></td>
												<td>${rotationPlanSegment.duration}</td>
												<td>${rotationPlanSegment.fullTimeEquivalent}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</c:if></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</fieldset>
	</main>
</body>
</html>
