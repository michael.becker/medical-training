<%@ include file="/WEB-INF/pages/include/include.jsp"%>

<table class="datatable">
	<thead>
		<tr>
			<th>Name</th>
			<th>Aktion</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${registrars}" var="registrar">
			<tr>
				<td><c:out value="${registrar.name}" /></td>
				<td><a href="<c:url value='/training/editRotationPlan/${registrar.uid}'/>">Rotationsplan bearbeiten</a></td>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="2"><a href="<c:url value='/authorisations/createauthorisation'/>">Neues Element hinzufügen</a></td>
		</tr>
	</tfoot>
</table>