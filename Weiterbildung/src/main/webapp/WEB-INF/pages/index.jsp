<%@ include file="/WEB-INF/pages/include/include.jsp"%>


<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Übersicht Weiterbildungsstätten</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main>
		<h1>Änderungsübersicht</h1>
		<h2>Änderungen am Typ MedicalField</h2>
		<table class="datatable">
			<thead>
				<tr>
					<th>Änderung am</th>
					<th>Änderung von</th>
					<th>Änderungsart</th>
					<th>Neuer Datensatz</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${medicalFieldRevisions}" var="medicalFieldRevision">
				<tr>
					<td>${medicalFieldRevision.userRevision.timestamp}</td>
					<td>${medicalFieldRevision.userRevision.userName}</td>
					<td>${medicalFieldRevision.revisionType}</td>
					<td>${medicalFieldRevision.revisionEntity}</td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
	</main>
</body>
</html>