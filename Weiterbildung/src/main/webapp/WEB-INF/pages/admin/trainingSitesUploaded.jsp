<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="importTrainingSitesURL" value="/admin/trainingsites/uploadDo" />


<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Weiterbildungsstätte importieren</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main>
	<h2>Submitted File</h2>
	<table>
		<tr>
			<td>OriginalFileName:</td>
			<td>${file.originalFilename}</td>
		</tr>
		<tr>
			<td>Type:</td>
			<td>${file.contentType}</td>
		</tr>
	</table>

	<form:form method="post" action="${importTrainingSitesURL}" modelAttribute="trainingSites">
		<fieldset>
			<legend>Zu importierende Weiterbildungsstätte</legend>
			<table class="datatable">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Straße</th>
						<th>PLZ</th>
						<th>Ort</th>
						<th>Behandlungsart</th>
						<th>Fachgebiet</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${trainingSites.trainingSites}" var="trainingSite" varStatus="i">
						<tr>
							<td>${i.index+1}</td>
							<td><form:input path="trainingSites[${i.index}].name" /></td>
							<td><form:input path="trainingSites[${i.index}].address.street" /></td>
							<td><form:input path="trainingSites[${i.index}].address.zipCode" /></td>
							<td><form:input path="trainingSites[${i.index}].address.city" /></td>
							<td><form:radiobuttons path="trainingSites[${i.index}].treatmentType" /></td>
							<td><form:select path="trainingSites[${i.index}].medicalField" items="${medicalFields}" itemValue="uid" />
						</tr>
						<c:forEach items="${trainingSite.children}" var="trainingSiteDepartment">
							<tr>
								<td></td>
								<td></td>
								<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${trainingSiteDepartment.name}&nbsp;(${trainingSiteDepartment.medicalField},&nbsp;${trainingSiteDepartment.treatmentType})</td>
							</tr>
						</c:forEach>
					</c:forEach>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5"><form:button>Weiterbildungsstätte importieren</form:button></td>
					</tr>
				</tfoot>
			</table>
		</fieldset>
		<fieldset>
			<legend>Doppelt vorhandene Weiterbildungsstätte</legend>
			<table class="datatable">
				<thead>
					<tr>
						<th>Zeilennummer</th>
						<th>Name</th>
						<th>Anschrift</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${skippedSites}" var="skippedSite">
						<tr>
							<td>${skippedSite.key}</td>
							<td>${skippedSite.value.name}</td>
							<td>${skippedSite.value.address}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</fieldset>
	</form:form> </main>
</body>
</html>