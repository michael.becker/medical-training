<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="uploadTrainingSitesURL" value="/admin/trainingsites/upload" />

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Weiterbildungsstätte importieren</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<form:form method="post" action="${uploadTrainingSitesURL}" enctype="multipart/form-data">
		<label for="file">Datei auswählen</label>
		<input type="file" name="file" />
		<input type="submit" value="Hochladen" />
	</form:form>
</body>
</html>