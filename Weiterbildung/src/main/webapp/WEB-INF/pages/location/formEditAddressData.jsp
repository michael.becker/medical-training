<%@ include file="/WEB-INF/pages/include/include.jsp"%>

<spring:message code="streetNr" var="streetPlaceholder" />
<spring:message code="city" var="cityPlaceholder" />

<fieldset>
	<legend>Adressdaten</legend>
	<form:label path="address.street">
		<spring:message code="street" />
		<form:input path="address.street" placeholder="${streetPlaceholder}" cssErrorClass="error" />
	</form:label>
	<form:label path="address.zipCode">
		<spring:message code="zipCode" />
		<form:input path="address.zipCode" pattern="[0-9]{5}" placeholder="00000" cssErrorClass="error" />
	</form:label>
	<form:label path="address.city">
		<spring:message code="city" />
		<form:input path="address.city" placeholder="${cityPlaceholder}" cssErrorClass="error" />
	</form:label>

	<form:errors path="address.street" cssClass="error" />
	<form:errors path="address.zipCode" cssClass="error" />
	<form:errors path="address.city" cssClass="error" />
	<form:errors path="address" cssClass="error" />
</fieldset>