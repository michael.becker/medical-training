<%@ include file="/WEB-INF/pages/include/include.jsp"%>


<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Übersicht Weiterbildungsstätten</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main>
	<table class="datatable">
		<thead>
			<tr>
				<th>Revisionsnummer</th>
				<th>Änderungsart</th>
				<th>Änderung am</th>
				<th>Änderung von</th>
				<th>Name</th>
				<th>Beschreibung</th>
				<th>Übergeordnetes Feld</th>
				<th>Untergeordnete Felder</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${revisions}" var="medicalFieldRevision" varStatus="i">
				<jsp:useBean id="dateValueMedField" class="java.util.Date" />
				<jsp:setProperty name="dateValueMedField" property="time" value="${medicalFieldRevision.userRevision.timestamp}" />
				<tr>
					<td>${medicalFieldRevision.userRevision.id}</td>
					<td>${medicalFieldRevision.revisionTypeText}</td>
					<td><fmt:formatDate value="${dateValueMedField}" pattern="dd.MM.yyyy - HH:mm:ss" /></td>
					<td>${medicalFieldRevision.userRevision.userName}</td>
					<td>${medicalFieldRevision.revisionEntity.name}</td>
					<td>${medicalFieldRevision.revisionEntity.description}</td>
					<td>${medicalFieldRevision.revisionEntity.parent}</td>
					<td>${medicalFieldRevision.revisionEntity.children}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	 </main>
</body>
</html>