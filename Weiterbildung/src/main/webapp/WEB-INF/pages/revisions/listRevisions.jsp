<%@ include file="/WEB-INF/pages/include/include.jsp"%>


<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Übersicht Weiterbildungsstätten</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main>
	<h1>Änderungsübersicht</h1>
	<h2>Änderungen am Typ MedicalField</h2>
	<table class="datatable">
		<thead>
			<tr>
				<th>Revisionsnummer</th>
				<th>Änderung am</th>
				<th>Änderung von</th>
				<th>Datensatz</th>
				<th>Änderungsart</th>
				<th>Historie</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${medicalFieldRevisions}" var="medicalFieldRevision" varStatus="i">
				<jsp:useBean id="dateValueMedField" class="java.util.Date" />
				<jsp:setProperty name="dateValueMedField" property="time" value="${medicalFieldRevision.userRevision.timestamp}" />
				<tr>
					<td>${medicalFieldRevision.userRevision.id}</td>
					<td><fmt:formatDate value="${dateValueMedField}" pattern="dd.MM.yyyy - HH:mm:ss" /></td>
					<td>${medicalFieldRevision.userRevision.userName}</td>
					<td>${medicalFieldRevision.revisionEntity}</td>
					<td>${medicalFieldRevision.revisionTypeText}</td>
					<td><a href="<c:url value='/revisions/detail/medicalField/${medicalFieldRevision.revisionEntity.uid}'/>"> <img src="${infoButton}" alt="info-button" width="16px;" height="16px;" /></a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

	<h2>Änderungen am Typ TrainingScheme</h2>
	<table class="datatable">
		<thead>
			<tr>
				<th>Revisionsnummer</th>
				<th>Änderung am</th>
				<th>Änderung von</th>
				<th>Datensatz</th>
				<th>Änderungsart</th>
				<th>Historie</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${trainingSchemeRevisions}" var="trainingSchemeRevision">
				<jsp:useBean id="dateValueTrainScheme" class="java.util.Date" />
				<jsp:setProperty name="dateValueTrainScheme" property="time" value="${trainingSchemeRevision.userRevision.timestamp}" />
				<tr>
					<td>${trainingSchemeRevision.userRevision.id}</td>
					<td><fmt:formatDate value="${dateValueTrainScheme}" pattern="dd.MM.yyyy - HH:mm:ss" /></td>
					<td>${trainingSchemeRevision.userRevision.userName}</td>
					<td>${trainingSchemeRevision.revisionEntity}</td>
					<td>${trainingSchemeRevision.revisionTypeText}</td>
					<td><a href="<c:url value='/revisions/detail/trainingScheme/${trainingSchemeRevision.revisionEntity.uid}'/>"> <img src="${infoButton}" alt="info-button" width="16px;" height="16px;" /></a></td>

				</tr>
			</c:forEach>
		</tbody>
	</table>
	</main>
</body>
</html>