<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="filterTrainingSchemes" value="/trainingschemes/filter" />

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><spring:message code="trainingScheme.overview" /></title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="filter" action="${filterTrainingSchemes}">
		<fieldset>
			<legend>
				<spring:message code="trainingScheme.filter" />
			</legend>
			<form:label path="name">
				<spring:message code="title" />
				<form:input path="name" />
			</form:label>
			<spring:message code="showArchived" var="showArchived" />
			<form:checkbox path="allowArchived" label="${showArchived}" id="allowArchived" />
			<form:button>
				<spring:message code="filter" />
			</form:button>
		</fieldset>
	</form:form>

	<fieldset>
		<legend>
			<spring:message code="trainingScheme.overview" />
		</legend>
		<table class="datatable">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th><a href="trainingschemes/list?sort=name,asc">&darr;</a> <span><spring:message code="title" /></span> <a href="trainingschemes/list?sort=name,desc">&uarr;</a></th>
					<th><a href="trainingschemes/list?sort=description,asc">&darr;</a> <span><spring:message code="description" /></span> <a href="trainingschemes/list?sort=description,desc">&uarr;</a></th>
					<th><a href="trainingschemes/list?sort=duration,asc">&darr;</a> <span><spring:message code="trainingScheme.duration" /></span><a href="trainingschemes/list?sort=duration,desc">&uarr;</a></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${trainingSchemes}" var="trainingScheme">
					<tr class="archived-${trainingScheme.archived}">
						<td><a href="<c:url value='/trainingschemes/${trainingScheme.uid}/edit'/>"> <img src="${editButton}" alt="edit-button" title="edit ${trainingScheme.name}" />
						</a></td>
						<td><a href="<c:url value='/trainingschemes/${trainingScheme.uid}/archive'/>"> <img src="${deleteButton}" alt="delete-button" title="archive ${trainingScheme.name}" />
						</a></td>
						<td><c:out value="${trainingScheme.name}" /></td>
						<td><c:out value="${trainingScheme.description}" /></td>
						<td><c:out value="${trainingScheme.duration}" /> <spring:message code="months" /></td>
					</tr>
				</c:forEach>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="5"><a href="<c:url value='/trainingschemes/create'/>"><spring:message code="trainingScheme.create" /></a></td>
				</tr>
			</tfoot>
		</table>
	</fieldset>
	</main>
</body>
</html>