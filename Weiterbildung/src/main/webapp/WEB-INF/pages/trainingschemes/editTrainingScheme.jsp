<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="post_url" value="/trainingschemes/update" />

<spring:message code="trainingScheme.title" var="trainingSchemeTitle" />
<spring:message code="trainingScheme.description" var="trainingSchemeDescription" />


<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><spring:message code="trainingScheme.edit" /></title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
<script type="text/javascript">
	var counter = ${fn:length(trainingScheme.trainingSchemeSegments)};
	
	function addInput(divName) {
		var fieldset	= document.createElement("fieldset");
		var legend		= document.createElement("legend");
		
		var labelDuration		= document.createElement("label");
		var inputDuration		= document.createElement("input");
		var selectMedicalField	= document.createElement("select");
		var whitespace1			= document.createElement("span");
		var whitespace2			= document.createElement("span");
		var labelInPatient		= document.createElement("label");
		var inputInPatient		= document.createElement("input");
		var labelOutPatient		= document.createElement("label");
		var inputOutPatient		= document.createElement("input");
 
		legend.innerHTML	="Weiterbildungsabschnitt " + (counter+1);
		
		labelDuration.htmlFor	= "trainingSchemeSegments["+counter+"].duration";
		labelDuration.innerHTML	= " Monate ";
		inputDuration.type		= "number";
		//inputDuration.min		= "3";
		inputDuration.value		= "3";
		//inputDuration.required	= "true";
		inputDuration.id		= "trainingSchemeSegments["+counter+"].duration";
		inputDuration.name		= "trainingSchemeSegments["+counter+"].duration";
		
		selectMedicalField.id	= "trainingSchemeSegments["+counter+"].medicalField";
		selectMedicalField.name	= "trainingSchemeSegments["+counter+"].medicalField";
		
		<c:forEach items="${medicalFields}" var="medicalField">
			var option		= document.createElement("option");
			option.text		= "${medicalField.name}";
			option.value	= "${medicalField.uid}"; 
			
			selectMedicalField.appendChild(option);
		</c:forEach>
		
		whitespace1.innerHTML		= "&nbsp;";
		
		inputInPatient.type			= "checkbox";
		inputInPatient.id			= "trainingSchemeSegments["+counter+"].inpatient";
		inputInPatient.name			= "trainingSchemeSegments["+counter+"].inpatient";
		labelInPatient.htmlFor		= "trainingSchemeSegments["+counter+"].inpatient";
		labelInPatient.innerHTML	= "Station�r";
		
		whitespace2.innerHTML		="&nbsp;";
		
		inputOutPatient.type		= "checkbox";
		inputOutPatient.id			= "trainingSchemeSegments["+counter+"].outpatient";
		inputOutPatient.name		= "trainingSchemeSegments["+counter+"].outpatient";
		labelOutPatient.htmlFor		= "trainingSchemeSegments["+counter+"].outpatient";
		labelOutPatient.innerHTML	= "Ambulant";
		
		fieldset.appendChild(legend);		
		fieldset.appendChild(inputDuration);
		fieldset.appendChild(labelDuration);
		fieldset.appendChild(selectMedicalField);
		fieldset.appendChild(whitespace1);
		fieldset.appendChild(inputInPatient);		
		fieldset.appendChild(labelInPatient);
		fieldset.appendChild(whitespace2);
		fieldset.appendChild(inputOutPatient);
		fieldset.appendChild(labelOutPatient);
		
		document.getElementById(divName).appendChild(fieldset);
		counter++;
	}
</script>
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="trainingScheme" action="${post_url}">
		<c:if test="${not empty trainingScheme.uid}">
			<form:hidden path="uid" />
		</c:if>
		<form:hidden path="archived" />

		<fieldset>
			<legend>
				<spring:message code="coreData" />
			</legend>
			<form:label path="name">
				<spring:message code="title" />
				<form:input path="name" placeholder="${trainingSchemeTitle}" />
			</form:label>
			<form:label path="description">
				<spring:message code="description" />
				<form:textarea path="description" placeholder="${trainingSchemeDescription}" />
			</form:label>

			<form:errors path="name" cssClass="error" />
		</fieldset>

		<fieldset id="values">
			<legend>
				<spring:message code="trainingScheme.segments" />
			</legend>
			<c:forEach items="${trainingScheme.trainingSchemeSegments}" varStatus="i" var="trainingSchemeSegment">
				<fieldset>
					<legend>
						<spring:message code="trainingScheme.segment" />
					</legend>
					<form:hidden path="trainingSchemeSegments[${i.index}].uid" />
					<form:input path="trainingSchemeSegments[${i.index}].duration" type="number" />
					<spring:message text="Monate" />
					<form:select path="trainingSchemeSegments[${i.index}].medicalField" items="${medicalFields}" itemValue="uid" />
					<form:checkbox path="trainingSchemeSegments[${i.index}].inpatient" label="Station�r" />
					<form:checkbox path="trainingSchemeSegments[${i.index}].outpatient" label="Ambulant" />
					<form:errors path="trainingSchemeSegments[${i.index}].outpatient" />
					<c:if test="${not empty trainingSchemeSegment.uid}">
						<a href="<c:url value='/trainingschemes/trainingschemesegments/${trainingSchemeSegment.uid}/delete'/>"><spring:message code="trainingScheme.deleteSegment" /></a>
					</c:if>
					<form:errors path="trainingSchemeSegments[${i.index}].duration" cssClass="error" />
				</fieldset>
			</c:forEach>
		</fieldset>

		<spring:message code="trainingScheme.addSegment" var="addSegment" />
		<input type="button" value="${addSegment}" onClick="addInput('values');">

		<form:button>
			<spring:message code="save" />
		</form:button>
		<form:button value="cancel"></form:button>
	</form:form> </main>
</body>
</html>
