<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="filter" value="/coredata/medicalfields/filter" />

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><spring:message code="medicalFieldOverview" /></title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="filter" action="${filter}">
		<fieldset>
			<legend>
				<spring:message code="medicalFieldFilter" />
			</legend>
			<form:label path="name">Name</form:label>
			<form:input path="name" />
			<spring:message code="showArchived" var="showArchived" />
			<form:checkbox path="allowArchived" label="${showArchived}" id="allowArchived" />
			<form:button>
				<spring:message code="filter" />
			</form:button>
		</fieldset>
	</form:form>

	<fieldset>
		<legend>
			<spring:message code="medicalFieldOverview" />
		</legend>
		<table class="datatable">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th><spring:message code="name" /></th>
					<th><spring:message code="description" /></th>
					<th><spring:message code="parentField" /></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${medicalFields}" var="medicalField">
					<tr class="archived-${medicalField.archived}">
						<td><a href="<c:url value='/coredata/medicalfields/${medicalField.uid}/edit'/>"><img src="${editButton}" /></a></td>
						<td><c:choose>
								<c:when test="${medicalField.archived eq true}"></c:when>
								<c:otherwise>
									<a href="<c:url value='/coredata/medicalfields/${medicalField.uid}/archive'/>"> <img src="${deleteButton}" /></a>
								</c:otherwise>
							</c:choose></td>
						<td>${medicalField.name }</td>
						<td>${medicalField.description}</td>
						<td>${medicalField.parent }</td>
					</tr>
				</c:forEach>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="5"><a href="<c:url value='/coredata/medicalfields/create'/>"><spring:message code="medicalFieldAdd" /></a></td>
				</tr>
			</tfoot>
		</table>
	</fieldset>
	</main>
</body>
</html>