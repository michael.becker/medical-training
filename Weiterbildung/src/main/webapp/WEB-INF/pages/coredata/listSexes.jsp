<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="filter" value="/coredata/sexes/filter" />

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><spring:message code="sexOverview" /></title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="filter" action="${filter}">
		<fieldset>
			<legend>
				<spring:message code="sexFilter" />
			</legend>
			<form:label path="name">
				<spring:message code="name" />
			</form:label>
			<form:input path="name" />
			<spring:message code="showArchived" var="showArchived" />
			<form:checkbox path="allowArchived" label="${showArchived}" id="allowArchived" />
			<form:button>
				<spring:message code="filter" />
			</form:button>
		</fieldset>
	</form:form>

	<fieldset>
		<legend>
			<spring:message code="sexOverview" />
		</legend>
		<table class="datatable">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th><spring:message code="name" /></th>
					<th><spring:message code="description" /></th>
					<th><spring:message code="sexDefaultPrefix" /></th>
					<th><spring:message code="sexDefaultSalutation" /></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${sexes}" var="sex">
					<tr class="archived-${sex.archived}">
						<td><a href="<c:url value='/coredata/sexes/${sex.uid}/edit'/>"><img src="${editButton}" /></a></td>
						<td><c:if test="${sex.archived eq false}">
								<a href="<c:url value='/coredata/sexes/${sex.uid}/archive'/>"><img src="${deleteButton}" /></a>
							</c:if></td>
						<td>${sex.name }</td>
						<td>${sex.description}</td>
						<td>${sex.defaultPrefix}</td>
						<td>${sex.defaultSalutation}</td>
					</tr>
				</c:forEach>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="6"><a href="<c:url value='/coredata/sexes/create'/>"><spring:message code="sexCreate" /></a></td>
				</tr>
			</tfoot>
		</table>
	</fieldset>
	</main>

</body>
</html>