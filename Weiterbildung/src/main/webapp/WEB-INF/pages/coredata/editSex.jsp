<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="post_url" value="/coredata/sexes/update" />


<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Geschlecht bearbeiten</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="sex" action="${post_url}">
		<fieldset>
			<legend>Geschlecht bearbeiten</legend>
			<c:if test="${not empty sex.uid}">
				<form:hidden path="uid" />
			</c:if>
			<form:hidden path="archived" />

			<form:label path="name">Name</form:label>
			<form:input path="name" required="true" />
			<form:label path="description">Beschreibung</form:label>
			<form:textarea path="description" />
			<form:label path="defaultPrefix">Standardtitel</form:label>
			<form:input path="defaultPrefix" />
			<form:label path="defaultSalutation">Standardanrede</form:label>
			<form:input path="defaultSalutation" />

			<form:errors path="name" cssClass="error" />
			<form:errors path="defaultPrefix" cssClass="error" />
			<form:errors path="defaultSalutation" cssClass="error" />

			<form:button>Speichern</form:button>
		</fieldset>
	</form:form> </main>
</body>
</html>