<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="post_url" value="/coredata/medicalfields/update" />

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><spring:message code="medicalFieldEdit" arguments="${medicalField.name}" /></title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="medicalField" action="${post_url}">
		<fieldset>
			<legend>
				<spring:message code="medicalFieldEdit" arguments="${medicalField.name}" />
			</legend>
			<c:if test="${not empty medicalField.uid}">
				<form:hidden path="uid" />
			</c:if>
			<form:hidden path="archived" />

			<form:label path="name">
				<spring:message code="name" />
			</form:label>
			<form:input path="name" required="true" />
			<form:label path="description">
				<spring:message code="description" />
			</form:label>
			<form:textarea path="description" />
			<form:label path="parent">
				<spring:message code="parentField" />
			</form:label>
			<form:select path="parent">
				<form:option value="" label="------------" />
				<form:options itemValue="uid" itemLabel="name" items="${medicalFields}" />
			</form:select>
			<spring:message code="archived" var="archived" />
			<form:checkbox path="archived" disabled="true" label="${archived}" />
			<form:button>
				<spring:message code="save" />
			</form:button>
			<form:errors />
		</fieldset>
	</form:form> </main>
</body>
</html>