<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="filter" value="/coredata/equipments/filter" />

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><spring:message code="equipmentOverview" /></title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="filter" action="${filter}">
		<fieldset>
			<legend>
				<spring:message code="equipmentFilter" />
			</legend>
			<form:label path="name">
				<spring:message code="name" />
				<form:input path="name" />
			</form:label>
			<spring:message code="showArchived" var="showArchived" />
			<form:checkbox path="allowArchived" label="${showArchived}" id="allowArchived" />
			<form:button>
				<spring:message code="filter" />
			</form:button>
		</fieldset>
	</form:form>

	<fieldset>
		<legend>
			<spring:message code="equipmentOverview" />
		</legend>
		<table class="datatable">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th><spring:message code="name" /></th>
					<th><spring:message code="description" /></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${equipments}" var="equipment">
					<tr class="archived-${equipment.archived}">
						<td><a href="<c:url value='/coredata/equipments/${equipment.uid}/edit'/>"><img src="${editButton}" /></a></td>
						<td><c:if test="${equipment.archived eq false}">
								<a href="<c:url value='/coredata/equipments/${equipment.uid}/archive'/>"><img src="${deleteButton}" /></a>
							</c:if></td>
						<td>${equipment.name }</td>
						<td>${equipment.description}</td>
					</tr>
				</c:forEach>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="5"><a href="<c:url value='/coredata/equipments/create'/>"><spring:message code="equipmentCreate" /></a></td>
				</tr>
			</tfoot>
		</table>
	</fieldset>
	</main>
</body>
</html>