<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="filter" value="/coredata/titles/filter" />

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><spring:message code="titleOverview" /></title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="filter" action="${filter}">
		<fieldset>
			<legend>
				<spring:message code="titleFilter" />
			</legend>
			<form:label path="name">
				<spring:message code="name" />
			</form:label>
			<form:input path="name" />
			<spring:message code="showArchived" var="showArchived" />
			<form:checkbox path="allowArchived" label="${showArchived}" id="allowArchived" />
			<form:button>
				<spring:message code="filter" />
			</form:button>
		</fieldset>
	</form:form>

	<fieldset>
		<legend>
			<spring:message code="titleOverview" />
		</legend>
		<table class="datatable">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th><spring:message code="name" /></th>
					<th><spring:message code="description" /></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${titles}" var="title">
					<tr class="archived-${title.archived}">
						<td><a href="<c:url value='/coredata/titles/${title.uid}/edit'/>"><img src="${editButton}" /></a></td>
						<td><c:if test="${title.archived eq false}">
								<a href="<c:url value='/coredata/titles/${title.uid}/archive'/>"><img src="${deleteButton}" /></a>
							</c:if></td>
						<td>${title.name }</td>
						<td>${title.description}</td>
					</tr>
				</c:forEach>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="5"><a href="<c:url value='/coredata/titles/create'/>"><spring:message code="titleCreate" /></a></td>
				</tr>
			</tfoot>
		</table>
	</fieldset>
	</main>

</body>
</html>