<%@ include file="/WEB-INF/pages/include/include.jsp"%>

<nav>
	<ul>
		<li>Übergreifende Stammdaten</li>
		<li>
			<ul>
				<li><a href="<c:url value='/coredata/medicalfields/list'/>">Gebiete</a></li>
				<li><a href="<c:url value='/coredata/equipments/list'/>">Ausstattung</a></li>
				<li><a href="<c:url value='/coredata/sexes/list'/>">Geschlechter</a></li>
				<li><a href="<c:url value='/coredata/titles/list'/>">Titel</a></li>
			</ul>
		</li>
		<li>Stammdaten Weiterbildungsordnungen</li>
		<li>
			<ul>
				<li><a href="<c:url value='/trainingschemes/list'/>">Weiterbildungsordnungen</a></li>
			</ul>
		<li>Stammdaten Ärzte in Weiterbildung</li>
		<li>
			<ul>
				<li><a href="<c:url value='/doctors/registrars/list'/>">Ärzte in Weiterbildung</a></li>
			</ul>
		</li>
		<li>Stammdaten Weiterbildungsstätte und -befugte</li>
		<li>
			<ul>
				<li><a href="<c:url value='/trainingsites/list'/>">Weiterbildungsstätte</a></li>
				<li><a href="<c:url value='/doctors/authoriseddoctors/list'/>">Weiterbildungsbefugte</a></li>
			</ul>
		</li>
		<li>Stammdaten Weiterbildungsstellen</li>
		<li>
			<ul>
				<li><a href="<c:url value='/trainingpositions/list'/>">Weiterbildungsstellen</a></li>
			</ul>
		</li>
		<!--li>Stammdaten Rotationspläne</li>
		<li>
			<ul>
				<li><a href="<c:url value='/training/listRegistrars'/>">Rotationspläne</a></li>
			</ul>
		</li-->
		<li>Verwaltung</li>
		<li>
			<ul>
				<li><a href="<c:url value='/resources/Handbuch.pdf'/>">Handbuch</a></li>
				<li><a href="<c:url value='/admin/trainingsites/import'/>">Weiterbildungsstätte importieren</a></li>
			</ul> <!--ul>
				<li><a href="<c:url value='/revisions/list'/>">Änderungshistorie</a></li>
			</ul--> <!--ul>
				<li><a href="<c:url value='/reloadApplicationData'/>">Anwendungsdaten neuladen</a></li>
			</ul-->
		</li>
	</ul>
</nav>