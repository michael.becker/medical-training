<%@ page session="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags"%>

<c:url value="/resources/images/edit-button.png" var="editButton" />
<c:url value="/resources/images/delete-button.png" var="deleteButton" />
<c:url value="/resources/images/perform-button.png" var="performButton" />
<c:url value="/resources/images/info-button.png" var="infoButton"/>