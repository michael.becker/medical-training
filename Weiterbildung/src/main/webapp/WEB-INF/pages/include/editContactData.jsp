<%@ include file="/WEB-INF/pages/include/include.jsp"%>

<fieldset>
	<legend>
		<spring:message code="contactInfo" />
	</legend>
	<form:label path="contactInfo.phoneNumber">
		<spring:message code="phoneNumber" />
	</form:label>
	<form:input path="contactInfo.phoneNumber" />
	<form:label path="contactInfo.phoneNumber2">
		<spring:message code="phoneNumber2" />
	</form:label>
	<form:input path="contactInfo.phoneNumber2" />
	<form:label path="contactInfo.faxNumber">
		<spring:message code="faxNumber" />
	</form:label>
	<form:input path="contactInfo.faxNumber" />
	<form:label path="contactInfo.email">
		<spring:message code="email" />
	</form:label>
	<form:input path="contactInfo.email" placeholder="mail@example.com" />
</fieldset>