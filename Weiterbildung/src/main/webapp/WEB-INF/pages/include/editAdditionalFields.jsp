<%@ include file="/WEB-INF/pages/include/include.jsp"%>

<script type="text/javascript">
	var counter = ${fn:length(entity.additionalFields)};
	
	function addAdditionalField(htmlElement) {
		var inputKey = document.createElement("input");
		var inputValue = document.createElement("input");
		
		inputKey.id		= "additional["+counter+"].key";
		inputKey.name	= "additional["+counter+"].key";
		inputValue.id	= "additional["+counter+"].value";
		inputValue.name	= "additional["+counter+"].value";
		
		document.getElementById(htmlElement).appendChild(inputKey);
		document.getElementById(htmlElement).appendChild(inputValue);
		
		counter++;
	}
</script>

<fieldset id="additionalFields">
	<legend>Zusätzliche Felder</legend>
	<c:forEach items="${entity.additionalFields}" var="additionalField" varStatus="i">
		<input name="additional[${i.index}].key" value="${additionalField.key}" />
		<input name="additional[${i.index}].value" value="${additionalField.value}" />
		<br />
	</c:forEach>


	<input type="button" value="Zusätzliches Feld hinzufügen" onClick="addAdditionalField('additionalFields');"><br/>
</fieldset>