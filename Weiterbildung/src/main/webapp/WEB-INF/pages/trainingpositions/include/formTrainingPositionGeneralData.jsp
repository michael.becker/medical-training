<%@ include file="/WEB-INF/pages/include/include.jsp"%>

<script type="text/javascript">
var counter = ${fn:length(trainingPositionField.exclusions)};

function addExclusion(htmlElement) {
	var inputFrom = document.createElement("input");
	var inputUntil = document.createElement("input");
	
	inputFrom.type	= "date";
	inputFrom.id	= "exclusions["+counter+"].exclusionFrom";
	inputFrom.name	= "exclusions["+counter+"].exclusionFrom";
	inputUntil.type	= "date";
	inputUntil.id	= "exclusions["+counter+"].exclusionUntil";
	inputUntil.name	= "exclusions["+counter+"].exclusionUntil";
	
	document.getElementById(htmlElement).appendChild(inputFrom);
	document.getElementById(htmlElement).appendChild(inputUntil);
	
	counter++;
}
</script>

<fieldset>
	<legend>Allgemeine Daten</legend>
	<c:if test="${0 != trainingPositionField.uid}">
		<form:hidden path="uid" />
	</c:if>

	<form:label path="capacity">Kapazit�t</form:label>
	<form:input path="capacity" type="number" min="0" step="1" cssErrorClass="error" />
	<form:checkbox path="partTimeAvailable" label="Teilzeit m�glich" />
	<form:label path="availableFrom">verf�gbar von</form:label>
	<form:input path="availableFrom" type="date" cssErrorClass="error" />
	<form:label path="availableUntil">verf�gbar bis</form:label>
	<form:input path="availableUntil" type="date" cssErrorClass="error" />

	<form:errors path="capacity" cssClass="error" />
	<form:errors path="availableFrom" cssClass="error" />
	<form:errors path="availableUntil" cssClass="error" />

	<fieldset id="exclusions">
		<legend>Geblockte Zeitr�ume</legend>
		<c:forEach items="${trainingPositionField.exclusions}" var="exclusion" varStatus="i">
		exclusion: ${exclusion.exclusionFrom} - ${exclusion.exclusionUntil}
			<form:hidden path="exclusions[${i.index}].uid" />
			<form:input path="exclusions[${i.index}].exclusionFrom" type="date" cssErrorClass="error"/>
			<form:input path="exclusions[${i.index}].exclusionUntil" type="date" cssErrorClass="error"/>
			<form:errors path="exclusions[${i.index}].exclusionFrom" cssClass="error"/>
			<form:errors path="exclusions[${i.index}].exclusionUntil" cssClass="error"/>
		</c:forEach>
		<input type="button" value="Geblockten Zeitraum hinzuf�gen" onClick="addExclusion('exclusions');">
	</fieldset>
</fieldset>