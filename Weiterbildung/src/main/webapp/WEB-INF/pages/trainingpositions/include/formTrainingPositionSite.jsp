<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<script type="text/javascript" src="/medicaltraining/resources/displayElements.js"></script>
<script type="text/javascript">
	function getAuthorisations(select) {
		var trainingSiteUId = select.options[select.selectedIndex].value;
		$("#authorisationField").empty();
		$.get("/medicaltraining/authorisations/getByTrainingSite/"+trainingSiteUId, function(authorisations) {
			if (authorisations.length == 0) {
				var option = document.createElement("option");
				option.text = "Keine WB-Befugten vorhanden, bitte neuen wählen/erstellen!";
				option.disabled = true;
				option.selected = true;
				document.getElementById("authorisationField").appendChild(option);
			} else {				
				authorisations.forEach(function(item, index) {
					var option = document.createElement("option");
					option.value = item.uid;
					option.text = item.authorisedDoctor.name + " " + item.authorisedDoctor.lastName + " (" + item.grantedOn + " - " + item.grantedTo + ")";
					document.getElementById("authorisationField").appendChild(option);
				});
			}			
		});
	}
</script>

<div id="result"></div>

<fieldset>
	<legend>Weiterbildungsstätte</legend>
	<ul class="two-column-form">
		<li><div class="label">
				<input type="radio" name="selectSite" value="UseExistingTrainingSite" id="useExistingTrainingSite" checked="checked" onclick="hideElement('editTrainingSiteForm');displayElement('formSelectTrainingSite');enableElement('useExistingAuthorised');"><label for="useExistingTrainingSite">Existierende Weiterbildungsstätte wählen</label>
			</div>
			<div class="input" id="formSelectTrainingSite">
				<spring:nestedPath path="authorisationField">
					<tags:selectTrainingSite allowNull="false" comparatorDepartment="${trainingPositionField.authorisationField.trainingSite}" onchange="getAuthorisations(this);" />
				</spring:nestedPath>
			</div></li>
		<li><div class="label">
				<input type="radio" name="selectSite" value="CreateNewTrainingSite" id="createNewTrainingSite" onclick="hideElement('formSelectTrainingSite');displayElement('editTrainingSiteForm');disableElement('useExistingAuthorised');hideElement('formUseExistingAuthorisation');displayElement('formUseExistingDoctor');checkButton('createNewAuthorisation');" /><label for="createNewTrainingSite">Neue Weiterbildungsstätte erstellen</label>
			</div>
			<div class="input" id="editTrainingSiteForm" style="display: none;">
				<spring:nestedPath path="authorisationField.trainingSite">
					<%@ include file="/WEB-INF/pages/trainingsites/include/formEditTrainingSite.jsp"%>
				</spring:nestedPath>
				<input type="checkbox" name="createDepartmentCheck" id="createDepartmentCheck" onclick="switchDisplay('departmentForm');" /> <label for="createDepartmentCheck">Abteilung hinzufügen</label>
				<div id="departmentForm" style="display: none;">
					<spring:nestedPath path="authorisationField.trainingSite.children[0]">
						<%@ include file="/WEB-INF/pages/trainingsites/include/formEditTrainingSiteDepartment.jsp"%>
					</spring:nestedPath>
				</div>
			</div></li>
	</ul>
</fieldset>