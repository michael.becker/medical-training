<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<script type="text/javascript" src="/medicaltraining/resources/displayElements.js"></script>

<fieldset>
	<legend>Weiterbildungsbefugter</legend>
	<ul class="two-column-form">
		<li><div class="label">
				<input type="radio" name="selectAuthorised" value="UseExistingAuthorisation" id="useExistingAuthorised" checked="checked" onclick="hideElement('formUseExistingDoctor'); hideElement('formCreateNewDoctor'); displayElement('formUseExistingAuthorisation');"><label for="useExistingAuthorised">Existierenden WB-Befugten aus der WB-St�tte w�hlen</label>
			</div>
			<div class="input" id="formUseExistingAuthorisation">
				<form:select path="authorisationField" items="${authorisations}" itemValue="uid" itemLabel="displayName" cssErrorClass="error" />
				<form:errors path="authorisationField" cssClass="error" />
			</div></li>
		<li>
			<div class="label">
				<input type="radio" name="selectAuthorised" value="CreateNewAuthorisation" id="createNewAuthorisation" onclick="hideElement('formUseExistingAuthorisation');hideElement('formCreateNewDoctor');displayElement('formUseExistingDoctor');"> <label for="createNewAuthorisation">Existierenden WB-Befugten w�hlen und Befugnis erstellen</label>
			</div>
			<div class="input" id="formUseExistingDoctor" style="display: none;">
				<form:select path="authorisationField.authorisedDoctor" items="${authorisedDoctors}" itemValue="uid" />
				<spring:nestedPath path="authorisationField">
					<%@ include file="/WEB-INF/pages/authorisations/include/formEditAuthorisationGeneralData.jsp"%>
				</spring:nestedPath>
			</div>
		</li>
		<li>
			<div class="label">
				<input type="radio" name="selectAuthorised" value="CreateNewAuthorised" id="createNewAuthorised" onclick="hideElement('formUseExistingAuthorisation');hideElement('formUseExistingDoctor');displayElement('formCreateNewDoctor');"><label for="createNewAuthorised">Neuen WB-Befugten und Befugnis erstellen</label>
			</div>
			<div class="input" id="formCreateNewDoctor" style="display: none;">
				<spring:nestedPath path="authorisationField.authorisedDoctor">
					<%@ include file="/WEB-INF/pages/doctors/editPerson.jsp"%>
					<%@ include file="/WEB-INF/pages/doctors/include/formEditLANR.jsp"%>
				</spring:nestedPath>
				<spring:nestedPath path="authorisationField">
					<%@ include file="/WEB-INF/pages/authorisations/include/formEditAuthorisationGeneralData.jsp"%>
				</spring:nestedPath>
			</div>
		</li>
	</ul>
</fieldset>