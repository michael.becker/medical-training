<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="post_url" value="/trainingpositions/${trainingPositionField.uid}/delete" />


<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Löschen einer Weiterbildungsstelle</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> Bitte das Löschen der folgenden Weiterbildungsstelle bestätigen:
	<ul>
		<li>Weiterbildungsbefugte: ${trainingPositionField.authorisationField.authorisedDoctor}</li>
		<li>Weiterbildungsstätte: ${trainingPositionField.authorisationField.trainingSiteDepartment}</li>
		<li>Zeitraum: ${trainingPositionField.availableFrom}&nbsp;-&nbsp;${trainingPositionField.availableUntil}</li>
	</ul>
	Zugewiesene Weiterbildungsabschnitte (diese werden ebenfalls gelöscht)
	<ul>
		<c:forEach items="${rotationPlanSegments}" var="rotationPlanSegment">
			<li>${rotationPlanSegment.rotationPlan.registrar}:&nbsp;${rotationPlanSegment.begin}&nbsp;-&nbsp;${rotationPlanSegment.end}</li>
		</c:forEach>
	</ul>
	<form:form method="post" modelAttribute="trainingPositionField" action="${post_url}">
		<form:button>Löschen!</form:button>
	</form:form> </main>
</body>
</html>