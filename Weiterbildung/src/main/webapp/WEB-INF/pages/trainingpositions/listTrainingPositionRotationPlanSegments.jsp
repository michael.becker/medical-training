<%@ include file="/WEB-INF/pages/include/include.jsp"%>

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>�bersicht Weiterbildungsstellen</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main>
	<h1>Belegungs�bersicht Weiterbildungsstelle</h1>
	<ul>
		<li>Weiterbildungsbefugter: ${trainingPositionField.authorisationField.authorisedDoctor}</li>
		<li>Maximale Weiterbildungdauer: ${trainingPositionField.authorisationField.maximalDuration} Monate</li>
		<li>
			Institution:
			<c:if test="${not empty trainingPositionField.authorisationField.trainingSite.parent}">
				${trainingPositionField.authorisationField.trainingSite.parent.name}&nbsp;-&nbsp;
			</c:if>
			${trainingPositionField.authorisationField.trainingSite.name}
		</li>
		<li>Verf�gbarkeit der Stelle: ${trainingPositionField.availableFrom} - ${trainingPositionField.availableUntil}</li>
		<li>Blockierte Zeiten: ${trainingPositionField.exclusions}</li>
		<li>H�chstkapazit�t: ${trainingPositionField.capacity}</li>
	</ul>

	<h2>Verf�gbarkeit in den n�chsten 24 Monaten</h2>
	<table class="availability">
		<tr>
			<c:forEach items="${availability}" var="availability">
				<td class="${availability.value}">${availability.key.year}-${availability.key.monthValue}</td>
			</c:forEach>
		</tr>
	</table>

	<h2>Geplante Weiterbildungsabschnitte</h2>
	<table class="datatable">
		<thead>
			<tr>
				<th>Beginn</th>
				<th>Ende</th>
				<th>AiW</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${rotationPlanSegments}" var="segment">

				<tr>
					<td><tags:localDate date="${segment.begin}" pattern="dd.MM.yyyy" /></td>
					<td><tags:localDate date="${segment.end}" pattern="dd.MM.yyyy" /></td>
					<td><a href="<c:url value='/training/editRotationPlan/${segment.rotationPlan.uid}'/>">${segment.rotationPlan.registrar}</a>&nbsp;(${segment.fullTimeEquivalent}&nbsp;VZ�)</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</main>
</body>
</html>