<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="post_url" value="/trainingpositions/update" />

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Weiterbildungsstelle bearbeiten</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
<script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.js"></script>
<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main>
	<h1>Weiterbildungsstelle bearbeiten</h1>
	<form:form method="post" modelAttribute="trainingPositionField" action="${post_url}">
		<form:errors />

		<%@ include file="/WEB-INF/pages/trainingpositions/include/formTrainingPositionGeneralData.jsp"%>
		<%@ include file="/WEB-INF/pages/trainingpositions/include/formTrainingPositionSite.jsp"%>
		<%@ include file="/WEB-INF/pages/trainingpositions/include/formTrainingPositionAuthorisation.jsp"%>

		<form:errors path="*" />
		<form:button>Weiterbildungstelle speichern</form:button>

	</form:form></main>
</body>
</html>
