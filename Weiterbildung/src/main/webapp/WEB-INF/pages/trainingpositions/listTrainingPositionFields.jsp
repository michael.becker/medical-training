<%@ include file="/WEB-INF/pages/include/include.jsp"%>

<c:url var="filterTrainingPosition" value="/trainingpositions/filter" />


<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>�bersicht Weiterbildungsstellen</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="trainingPositionFieldFilter" action="${filterTrainingPosition}">
		<fieldset>
			<legend>Weiterbildungsstellen filtern</legend>

			<form:label path="zipCode">Postleitzahl</form:label>
			<form:input path="zipCode" pattern="[0-9]{5}" required="false" />
			<form:label path="distance">Entfernung (km)</form:label>
			<form:input path="distance" type="number" step="10" />
			<form:label path="medicalField">Fachgebiet</form:label>
			<form:select path="medicalField" items="${medicalFields}" itemLabel="name" itemValue="uid" />

			<form:button>Filtern</form:button>
		</fieldset>

	</form:form>

	<fieldset>
		<legend>�bersicht Weiterbildungsstellen</legend>
		<table class="datatable">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th><a href="trainingpositions/listTrainingPositionFields?sort=authorisation,asc">&darr;</a> <span>Befugter</span> <a href="trainingpositions/listTrainingPositionField?sort=authorisation,desc">&uarr;</a></th>
					<th>Weiterbildungsst�tte</th>
					<th>Gebiet</th>
					<th>Behandlungsart</th>
					<th><a href="trainingpositions/listTrainingPositionFields?sort=capacity,asc">&darr;</a> <span>Kapazit�t</span> <a href="trainingpositions/listTrainingPositionField?sort=capacity,desc">&uarr;</a></th>
					<th><a href="trainingpositions/listTrainingPositionFields?sort=fullTimeEquivalent,asc">&darr;</a> <span>Teilzeit m�glich</span> <a href="trainingpositions/listTrainingPositionField?sort=fullTimeEquivalent,desc">&uarr;</a></th>
					<th><a href="trainingpositions/listTrainingPositionFields?sort=availableFrom,asc">&darr;</a> <span>verf�gbar von</span> <a href="trainingpositions/listTrainingPositionField?sort=availableFrom,desc">&uarr;</a></th>
					<th><a href="trainingpositions/listTrainingPositionFields?sort=availableUntil,asc">&darr;</a> <span>verf�gbar bis</span> <a href="trainingpositions/listTrainingPositionField?sort=availableUntil,desc">&uarr;</a></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${trainingPositionFields}" var="trainingPositionField">
					<c:url var="editTrainingPositionField" value="/trainingpositions/${trainingPositionField.uid}/edit" />
					<c:url var="deleteTrainingPositionField" value="/trainingpositions/${trainingPositionField.uid}/delete" />
					<c:url var="listRotationPlanSegments" value="/trainingpositions/${trainingPositionField.uid}/segments" />

					<tr>
						<td><a href="${editTrainingPositionField}"><img src="${editButton}" /></a>
						<td><a href="${deleteTrainingPositionField}"><img src="${deleteButton}" /></a>
						<td><a href="${listRotationPlanSegments}"><img src="${infoButton}" width="20px" height="20px" /></a>
						<td><c:out value="${trainingPositionField.authorisationField.authorisedDoctor}" /></td>
						<td>
							<c:if test="${not empty trainingPositionField.authorisationField.trainingSite.parent }">
								<c:out value="${trainingPositionField.authorisationField.trainingSite.parent.name}"/>:&nbsp;
							</c:if>
							<c:out value="${trainingPositionField.authorisationField.trainingSite.name}" />
						</td>
						<td><c:out value="${trainingPositionField.authorisationField.trainingSite.medicalField}" /></td>
						<td><spring:message code="${trainingPositionField.authorisationField.trainingSite.treatmentType}"/></td>
						<td><c:out value="${trainingPositionField.capacity}" /></td>
						<td><c:out value="${trainingPositionField.partTimeAvailable}" /></td>
						<td><tags:localDate date="${trainingPositionField.availableFrom}"/></td>
						<td><c:out value="${trainingPositionField.availableUntil}" /></td>
					</tr>
				</c:forEach>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="11"><a href="<c:url value='/trainingpositions/create'/>">Neues Element hinzuf�gen</a></td>
				</tr>
			</tfoot>

		</table>
	</fieldset>
	</main>