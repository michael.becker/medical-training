<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="filterAuthorisedDoctors" value="/doctors/authoriseddoctors/filter" />


<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Übersicht Weiterbildungsbefugte</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="authorisedDoctorFilter" action="${filterAuthorisedDoctors}">
		<fieldset>
			<legend>
				<spring:message code="authorisedDoctors.filter" />
			</legend>
			<form:label path="name">Name</form:label>
			<form:input path="name" />
			<form:label path="lanr">LANR</form:label>
			<form:input path="lanr" />
			<form:label path="addressText">Adresse</form:label>
			<form:input path="addressText" />
			<form:button>Suchen</form:button>
		</fieldset>
	</form:form>

	<fieldset>
		<legend>Übersicht Weiterbildungsbefugte</legend>
		<table class="datatable">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th>LANR</th>
					<th>Name</th>
					<th>Geburtsdatum</th>
					<th>Weiterbildungsstätte</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${authorisedDoctors}" var="authorisedDoctor">
					<tr>
						<td><a href="<c:url value='/doctors/authoriseddoctors/${authorisedDoctor.uid}/edit'/>"><img src="${editButton}" /></a></td>
						<td><a href="<c:url value='/doctors/authoriseddoctors/${authorisedDoctor.uid}/delete'/>"><img src="${deleteButton}" /></a></td>
						<td>${authorisedDoctor.lanr }</td>
						<td>${authorisedDoctor.sex.defaultPrefix}&nbsp;${authorisedDoctor.title.name}&nbsp;${authorisedDoctor.fullName}</td>
						<td><tags:localDate date="${authorisedDoctor.birthDate}" pattern="dd.MM.yyyy" /></td>
						<td><c:forEach items="${authorisedDoctor.trainingSites}" var="trainingSite" varStatus="i">
								<c:if test="${i.index gt 0}">
									<br />
								</c:if>
								<c:if test="${not empty trainingSite.parent }">
									<c:out value="${trainingSite.parent.name}" />:&nbsp;</c:if>
								<c:out value="${trainingSite.name}" />
							</c:forEach></td>
					</tr>
				</c:forEach>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="6"><a href="<c:url value='/doctors/authoriseddoctors/create'/>">neuen Datensatz anlegen</a></td>
				</tr>
			</tfoot>
		</table>
	</fieldset>
	</main>
</body>
</html>