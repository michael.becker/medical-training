<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="post_url" value="/doctors/authoriseddoctors/${authorisedDoctor.uid}/delete" />


<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Weiterbildungsbefugten l�schen</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main>
	<h1>Weiterbildungsbefugten ${authorisedDoctor.fullName} l�schen</h1>
	<p>Bitte das L�schen des folgenden Weiterbildungsbefugten best�tigen:</p>
	<ul>
		<li>Name: ${authorisedDoctor.fullName}
	</ul>
	<p>Zugewiesene Weiterbildungsstellen (werden ebenfalls gel�scht!)</p>
	<ul>
		<c:forEach items="${trainingPositions}" var="trainingPosition">
			<li>
				<p>${trainingPosition.key.authorisationField.trainingSiteDepartment}&nbsp;(${trainingPosition.key.availableFrom}&nbsp;-&nbsp;${trainingPosition.key.availableUntil})</p>
				<p>Zugewiesene Weiterbildungsabschnitte (werden ebenfalls gel�scht!)</p>
				<ul>
					<c:forEach items="${trainingPosition.value}" var="rotationPlanSegment">
						<li>${rotationPlanSegment.rotationPlan.registrar}:&nbsp;${rotationPlanSegment.begin}&nbsp;-&nbsp;${rotationPlanSegment.end}</li>
					</c:forEach>
				</ul>
			</li>
		</c:forEach>
	</ul>

	<form:form method="post" modelAttribute="authorisedDoctor" action="${post_url}">
		<form:button>L�schen!</form:button>
	</form:form> </main>
</body>
</html>