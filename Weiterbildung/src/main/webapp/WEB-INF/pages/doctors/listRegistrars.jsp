<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="filterRegistrars" value="/doctors/registrars/filter" />

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>�bersicht �iW</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="registrarFilter" action="${filterRegistrars}">
		<fieldset>
			<legend>
				<spring:message code="registrars.filter" />
			</legend>
			<form:label path="name">
				<spring:message code="name" />
				<form:input path="name" />
			</form:label>
			<form:label path="addressText">
				<spring:message code="address" />
				<form:input path="addressText" />
			</form:label>
			<form:label path="trainingPosition">
				<spring:message code="actualTrainingPosition" />
				<form:input path="trainingPosition" />
			</form:label>
			<span style="color: red">&lt;---HINWEIS: Funktioniert noch nicht!</span>
			<spring:message code="showArchived" var="showArchived" />
			<form:checkbox path="allowArchived" label="${showArchived}" id="allowArchived" />
			<form:button>
				<spring:message code="filter" />
			</form:button>
		</fieldset>
	</form:form>


	<fieldset>
		<legend>
			<spring:message code="registrars.overview" />
		</legend>
		<table class="datatable">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th><spring:message code="name" /></th>
					<th><spring:message code="birthDate" /></th>
					<th><spring:message code="address" /></th>
					<th><spring:message code="actualTrainingPosition" /></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${registrars}" var="registrar">
					<tr class="archived-${registrar.archived}">
						<td><tags:editEntity entityUId="${registrar.uid}" entityPath="/doctors/registrars" /></td>
						<td><tags:archiveEntity entityUId="${registrar.uid}" entityPath="/doctors/registrars" /></td>
						<td><a href="<c:url value='/training/editRotationPlan/${registrar.uid}'/>"><img src="${performButton}" /></a>
						<td><c:if test="${not empty registrar.sex}">${registrar.sex.defaultPrefix}&nbsp;</c:if> <c:if test="${not empty registrar.title}">${registrar.title.name}&nbsp;</c:if>${registrar.fullName}</td>
						<td><tags:localDate date="${registrar.birthDate}" pattern="dd.MM.yyyy" /></td>
						<td>${registrar.address}</td>
						<td><c:if test="${not empty actualSegments[registrar]}">
								<c:set var="segment" value="${actualSegments[registrar]}" />
								<c:if test="${not empty segment.trainingPosition.authorisationField.trainingSite.parent}">
									<c:out value="${segment.trainingPosition.authorisationField.trainingSite.parent.name}" />:&nbsp;
								</c:if>
								<c:out value="${segment.trainingPosition.authorisationField.trainingSite.name}" />
							</c:if></td>
					</tr>
				</c:forEach>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="7"><a href="<c:url value='/doctors/registrars/create'/>"><spring:message code="registrars.create" /></a></td>
				</tr>
			</tfoot>
		</table>
	</fieldset>
	</main>
</body>
</html>