<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="post_url" value="/doctors/authoriseddoctors/update" />

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Weiterbildungsbefugten bearbeiten</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="authorisedDoctor" action="${post_url}">
		<form:errors />
		<fieldset>
			<legend>Weiterbildungsbefugten bearbeiten</legend>
			<c:if test="${not empty authorisedDoctor.uid}">
				<form:hidden path="uid" />
			</c:if>

			<%@ include file="/WEB-INF/pages/doctors/editPerson.jsp"%>
			<%@ include file="/WEB-INF/pages/doctors/include/formEditLANR.jsp"%>

			<fieldset>
				<legend>Weiterbildungsstätte</legend>
				<div style="float: left; margin-right:15px;">
					<c:set var="comparatorDepartment" value="${authorisedDoctor.trainingSites}" />
					<c:set var="path" value="trainingSites" />
					<c:set var="size" value="20" />
					<%@ include file="/WEB-INF/pages/trainingsites/include/selectTrainingSites.jsp"%>
				</div>
				<h3>Momentan zugewiesene Weiterbildungsstätten</h3>
				<ul>
					<c:forEach items="${authorisedDoctor.trainingSites}" var="trainingSite">
						<li><c:if test="${not empty trainingSite.parent}">
								${trainingSite.parent.name}:&nbsp;
							</c:if> ${trainingSite.name}</li>
					</c:forEach>
				</ul>
			</fieldset>
		</fieldset>

		<c:set var="entity" value="${authorisedDoctor}" />
		<%@ include file="/WEB-INF/pages/include/editAdditionalFields.jsp"%>

		<form:button name="saveAction" value="save">Speichern</form:button>
		<form:button name="saveAction" value="saveAndClose">Speichern und Schließen</form:button>

		<fieldset>
			<legend>Weiterbildungsbefugnisse bearbeiten</legend>
			<c:choose>
				<c:when test="${not empty authorisedDoctor.uid}">
					<table class="datatable">
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th>Weiterbildungsstätte</th>
								<th>Fachgebiet</th>
								<th>Behandlungsart</th>
								<th>Beginn</th>
								<th>Ende</th>
								<th>Ordnung</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${authorisations}" var="authorisation">
								<tr>
									<td><a href="<c:url value='/doctors/authoriseddoctors/${authorisedDoctor.uid}/authorisations/${authorisation.uid}/edit'/>"><img src="${editButton}" /></a></td>
									<td><a href="<c:url value='/doctors/authoriseddoctors/${authorisedDoctor.uid}/authorisations/${authorisation.uid}/delete'/>"><img src="${deleteButton}" /></a></td>
									<td><c:if test="${not empty authorisation.trainingSite.parent}">
											<c:out value="${authorisation.trainingSite.parent.name}" />:&nbsp;
							</c:if> <c:out value="${authorisation.trainingSite.name}" /></td>
									<td>${authorisation.trainingSite.medicalField}</td>
									<td>${authorisation.trainingSite.treatmentType}</td>
									<td>${authorisation.grantedOn}</td>
									<td>${authorisation.grantedTo}</td>
									<td>${authorisation.trainingScheme.name}</td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="8"><a href="<c:url value='/doctors/authoriseddoctors/${authorisedDoctor.uid}/authorisations/create'/>">neuen Datensatz einfügen</a></td>
							</tr>
						</tfoot>
					</table>
				</c:when>
				<c:otherwise>
					<p>Bitte zunächst speichern, um Befugnisse hinzuzufügen!</p>
				</c:otherwise>
			</c:choose>
		</fieldset>
	</form:form>


	<h2>Änderungshistorie</h2>
	<table class="datatable">
		<thead>
			<tr>
				<th>Revisionsnummer</th>
				<th>Änderungsart</th>
				<th>Änderung am</th>
				<th>Änderung von</th>
				<th>Geschlecht</th>
				<th>Titel</th>
				<th>Vorname</th>
				<th>Nachname</th>
				<th>Geburtsdatum</th>
				<th>LANR</th>
				<th>Adresse</th>
				<th>Kontaktdaten</th>
				<th>Anmerkungen</th>
				<th>Zusätzliche Felder</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${revisions}" var="revision">
				<jsp:useBean id="dateValue" class="java.util.Date" />
				<jsp:setProperty name="dateValue" property="time" value="${revision.userRevision.timestamp}" />
				<tr>
					<td>${revision.userRevision.id}</td>
					<td>${revision.revisionTypeText}</td>
					<td><fmt:formatDate value="${dateValue}" pattern="dd.MM.yyyy - HH:mm:ss" /></td>
					<td>${revision.userRevision.userName}</td>
					<td>${revision.revisionEntity.sex.name}</td>
					<td>${revision.revisionEntity.title.name}</td>
					<td>${revision.revisionEntity.name}</td>
					<td>${revision.revisionEntity.lastName}</td>
					<td>${revision.revisionEntity.birthDate}</td>
					<td>${revision.revisionEntity.lanr}</td>
					<td>${revision.revisionEntity.address}</td>
					<td>${revision.revisionEntity.contactInfo}</td>
					<td>${revision.revisionEntity.notes}</td>
					<td>${revision.revisionEntity.additionalFields}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</main>
</body>
</html>