<%@ include file="/WEB-INF/pages/include/include.jsp"%>

<fieldset>
	<legend>Arztspezifische Daten</legend>
	<form:label path="lanr">LANR
	<form:input path="lanr" placeholder="Lebenslange neunstellige Arztnummer" pattern="[0-9]{9}" cssErrorClass="error" />
	</form:label>
	<form:errors path="lanr" cssClass="error" />
</fieldset>
