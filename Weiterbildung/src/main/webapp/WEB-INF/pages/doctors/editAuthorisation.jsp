<%@ include file="/WEB-INF/pages/include/include.jsp"%>
<c:url var="post_url" value="/doctors/authoriseddoctors/${authorisation.authorisedDoctor.uid}/authorisations/edit" />

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Weiterbildungsbefugten bearbeiten</title>
<link rel="stylesheet" type="text/css" href="/medicaltraining/resources/styles.css">
</head>
<body>
	<%@ include file="/WEB-INF/pages/include/nav.jsp"%>

	<main> <form:form method="post" modelAttribute="authorisation" action="${post_url}">
		<fieldset>
			<legend>Befugnis f�r ${authorisation.authorisedDoctor} bearbeiten</legend>
			<c:if test="${0 != authorisation.uid}">
				<form:hidden path="uid" />
			</c:if>
			<form:hidden path="authorisedDoctor.uid" />
			<form:hidden path="name" value="AuthorisationField for authorisedDoctor ${authorisation.authorisedDoctor.uid}" />

			<tags:selectTrainingSite comparatorDepartment="${authorisation.trainingSite}" size="5" />
			<form:errors path="trainingSite" />
			<form:label path="trainingScheme">Weiterbildungsordnung</form:label>
			<form:select path="trainingScheme" items="${trainingSchemes}" itemLabel="name" itemValue="uid" />
			<form:label path="grantedOn">G�ltig von</form:label>
			<form:input path="grantedOn" type="date" cssErrorClass="error" />
			<form:errors path="grantedOn" />
			<form:label path="grantedTo">G�ltig bis</form:label>
			<form:input path="grantedTo" type="date" cssErrorClass="error" />
			<form:errors path="grantedTo" />
			<form:label path="maximalDuration">Maximale Weiterbildungsdauer</form:label>
			<form:input path="maximalDuration" type="number" step="1" cssErrorClass="error" />
			<form:errors path="maximalDuration" />
			<form:hidden path="authorisedDoctor.uid" />
			<form:button name="saveAction" value="save">
				<spring:message code="save" />
			</form:button>
			<form:button name="saveAction" value="saveAndClose">
				<spring:message code="saveAndClose" />
			</form:button>
			<form:errors />
		</fieldset>

	</form:form> </main>
</body>
</html>