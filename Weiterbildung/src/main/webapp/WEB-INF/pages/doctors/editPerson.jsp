<%@ include file="/WEB-INF/pages/include/include.jsp"%>

<spring:message code="firstName" var="firstNamePlaceholder" />
<spring:message code="lastName" var="lastNamePlaceholder" />

<fieldset>
	<legend>
		<spring:message code="personalData" />
	</legend>
	<form:label path="sex">
		<spring:message code="sex" />
		<form:select path="sex">
			<form:option value="" label="------------" />
			<form:options itemValue="uid" itemLabel="name" items="${sexes}" />
		</form:select>
	</form:label>
	<form:label path="title">
		<spring:message code="title" />
		<form:select path="title">
			<form:option value="" label="------------" />
			<form:options itemValue="uid" itemLabel="name" items="${titles}" />
		</form:select>
	</form:label>
	<br />
	<form:label path="name">
		<spring:message code="firstName" />
		<form:input path="name" placeholder="${firstNamePlaceholder}" cssErrorClass="error" />
	</form:label>
	<form:label path="lastName">
		<spring:message code="lastName" />
		<form:input path="lastName" placeholder="${lastNamePlaceholder}" cssErrorClass="error" />
	</form:label>
	<br />
	<form:label path="birthDate">
		<spring:message code="birthDate" />
		<form:input path="birthDate" type="date" />
	</form:label>

	<form:errors path="name" cssClass="error" />
	<form:errors path="lastName" cssClass="error" />
</fieldset>

<%@ include file="/WEB-INF/pages/location/formEditAddressData.jsp"%>
<%@ include file="/WEB-INF/pages/include/editContactData.jsp"%>


<fieldset>
	<legend>
		<spring:message code="notes" />
	</legend>
	<form:textarea path="notes" style="width:100%; height:7.5em;" />
</fieldset>