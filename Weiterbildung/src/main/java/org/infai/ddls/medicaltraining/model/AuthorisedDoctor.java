package org.infai.ddls.medicaltraining.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.validation.Valid;
import javax.validation.constraints.Digits;

import org.hibernate.envers.Audited;

/**
 * Stellt einen Weiterbildungsbefugten dar.
 *
 * @author Michael Becker
 *
 */
@Entity
@Audited
public class AuthorisedDoctor extends Doctor {
    @Digits(integer = 9, fraction = 0)
    @Column(unique = true)
    private Integer lanr;
    @Valid
    @ManyToMany
    private List<TrainingSite> trainingSites;

    public AuthorisedDoctor() {
	this.setTrainingSites(new ArrayList<>());
    }

    public Integer getLanr() {
	return lanr;
    }

    public List<TrainingSite> getTrainingSites() {
	return trainingSites;
    }

    public void setLanr(Integer lanr) {
	this.lanr = lanr;
    }

    public void setTrainingSites(List<TrainingSite> trainingSites) {
	this.trainingSites = trainingSites;
    }

    @Override
    public String toString() {
	return getName() + " " + getLastName();
    }

}
