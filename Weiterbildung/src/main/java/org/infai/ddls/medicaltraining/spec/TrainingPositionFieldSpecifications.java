package org.infai.ddls.medicaltraining.spec;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;
import org.infai.ddls.medicaltraining.model.TrainingPositionField;

@SuppressWarnings("serial")
public class TrainingPositionFieldSpecifications extends AbstractTrainingAppEntitySpecification<TrainingPositionField, TrainingPositionFieldFilter> {
    public TrainingPositionFieldSpecifications(TrainingPositionFieldFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	addPredicate(new MedicalFieldSpecification<TrainingPositionField>(filter.getMedicalField()).toPredicate(root.get("authorisationField").get("trainingSite").get("medicalField"), query, criteriaBuilder));

	addPredicate(isAvailableFrom(filter.getAvailableFrom(), root, query, criteriaBuilder));
	addPredicate(isAvailableUntil(filter.getAvailableUntil(), root, query, criteriaBuilder));

	addPredicate(isAvailable(filter.getFullTimeEquivalent(), root, query, criteriaBuilder));
	addPredicate(isAvailable(filter.getMinimalCapacity(), root, query, criteriaBuilder));

	addPredicate(new LocationSpecification<TrainingPositionField>(filter).toPredicate(root.get("authorisationField").get("trainingSite").get("latitude"), root.get("authorisationField").get("trainingSite").get("longitude"), query, criteriaBuilder));

	if (null != filter.getAuthorisationField()) {
	    addPredicate(isFromAuthorisationField(filter.getAuthorisationField(), root, query, criteriaBuilder));
	}
	if (null != filter.getAuthorisedDoctor()) {
	    addPredicate(isFromAuthorisedDoctor(filter.getAuthorisedDoctor(), root, query, criteriaBuilder));
	}
    }

    private Predicate isAvailable(double fullTimeEquivalents, Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (fullTimeEquivalents < 1.0) {
	    return criteriaBuilder.isTrue(root.get("partTimeAvailable"));
	} else {
	    return criteriaBuilder.conjunction();
	}
    }

    private Predicate isAvailable(int minimalCapacity, Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.greaterThanOrEqualTo(root.get("capacity"), minimalCapacity);
    }

    private Predicate isAvailableFrom(LocalDate begin, Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (null == begin) {
	    return criteriaBuilder.conjunction();
	} else {
	    List<Predicate> predicates = new ArrayList<>();
	    predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("availableFrom"), begin));
	    predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("availableUntil"), begin));

	    return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
	}
    }

    private Predicate isAvailableUntil(LocalDate end, Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (null == end) {
	    return criteriaBuilder.conjunction();
	} else {
	    List<Predicate> predicates = new ArrayList<>();
	    predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("availableUntil"), end));
	    predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("availableFrom"), end));

	    return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
	}
    }

    private Predicate isFromAuthorisationField(AuthorisationField authorisationField, Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get("authorisationField"), authorisationField);
    }

    private Predicate isFromAuthorisedDoctor(AuthorisedDoctor authorisedDoctor, Root<TrainingPositionField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get("authorisationField").get("authorisedDoctor"), authorisedDoctor);
    }

}