package org.infai.ddls.medicaltraining.model;

import java.io.IOException;

import javax.json.JsonObject;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.geotools.referencing.GeodeticCalculator;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

@MappedSuperclass
@Audited
public abstract class Location extends TrainingAppEntity {
    private static final GeoApiContext context = new GeoApiContext.Builder().apiKey("AIzaSyCL2ACPvjLb5E10PKXOYcGFqYF-LccVe1w").build();

    /**
     * Gibt die Koordinaten zur angegebenen Postleitzahl zurück.
     * 
     * @param zipCode
     * @return die Koordinaten zur angegebenen Postleitzahl mit Latitude in Index 0
     *         und Longitude in Index 1
     * @throws ApiException
     * @throws InterruptedException
     * @throws IOException
     */
    public static double[] getCoordinates(String zipCode) throws ApiException, InterruptedException, IOException {
	Client client = ClientBuilder.newClient().register(JsonProcessingFeature.class);
	WebTarget plzSearch = client.target("http://api.zippopotam.us/DE/" + zipCode);
	JsonObject result = plzSearch.request(MediaType.APPLICATION_JSON).get(JsonObject.class);
	String city = result.getJsonArray("places").getJsonObject(0).getJsonString("place name").getString();

	GeocodingResult[] results = GeocodingApi.geocode(context, zipCode + " " + city).await();

	if (0 == results.length) {
	    throw new RuntimeException(zipCode + " not found");
	} else {
	    return new double[] { results[0].geometry.location.lat, results[0].geometry.location.lng };
	}
    }

    public static int getDistance(Location source, Location target) throws ApiException, InterruptedException, IOException {
	return getDistance(source, target.address.getStreet() + " " + target.address.getZipCode() + " " + target.address.getCity());
    }

    public static int getDistance(Location source, String target) throws ApiException, InterruptedException, IOException {
	GeocodingResult[] results = GeocodingApi.geocode(context, target).await();

	if (0 == results.length) {
	    return -1;
	}

	LatLng targetLatLng = results[0].geometry.location;

	return getLatLngDistance(source.getLatitude(), source.getLongitude(), targetLatLng.lat, targetLatLng.lng);
    }

    public static int getLatLngDistance(double sourceLat, double sourceLng, double targetLat, double targetLng) {
	GeodeticCalculator gc = new GeodeticCalculator();
	gc.setStartingGeographicPoint(sourceLng, sourceLat);
	gc.setDestinationGeographicPoint(targetLng, targetLat);

	return ((int) gc.getOrthodromicDistance()) / 1000;
    }

    @Transient
    @JsonIgnore
    private final Log logger = LogFactory.getLog(getClass());

    @NotNull
    @Valid
    @Embedded
    @Column(nullable = false)
    protected Address address;

    protected double latitude;
    protected double longitude;

    public Location() {
	this.address = new Address();
    }

    public Address getAddress() {
	return address;
    }

    public double getLatitude() {
	return latitude;
    }

    public double getLongitude() {
	return longitude;
    }

    public void setAddress(Address address) {
	this.address = address;
    }

    public void updateLatLng() {
	GeocodingResult[] results;
	try {
	    if (null == address.getStreet()) {
		results = GeocodingApi.geocode(context, address.getZipCode() + " " + address.getCity()).await();
	    } else {
		results = GeocodingApi.geocode(context, address.getStreet() + " " + address.getZipCode() + " " + address.getCity()).await();
	    }
	} catch (ApiException | InterruptedException | IOException e) {
	    throw new RuntimeException(e);
	}

	if (0 == results.length) {
	    logger.warn("Geolocations for " + address.getStreet() + " " + address.getZipCode() + "/" + address.getCity() + " could not be resolved");
	    throw new RuntimeException("Geolocations for " + address.getStreet() + " " + address.getZipCode() + "/" + address.getCity() + " could not be resolved");
	} else {
	    this.latitude = results[0].geometry.location.lat;
	    this.longitude = results[0].geometry.location.lng;
	}

    }
}
