package org.infai.ddls.medicaltraining.spec;

import org.infai.ddls.medicaltraining.model.Address;
import org.infai.ddls.medicaltraining.model.ContactInfo;
import org.infai.ddls.medicaltraining.model.Sex;
import org.infai.ddls.medicaltraining.model.Title;

public abstract class PersonFilter extends TrainingAppEntityFilter {
    private String firstName;
    private String lastName;
    private String name;
    private String addressText;
    private Sex sex;
    private Title title;
    private Address address;
    private ContactInfo contactInfo;

    public PersonFilter() {
	this.address = new Address();
	this.contactInfo = new ContactInfo();
    }

    public Address getAddress() {
	return address;
    }

    public String getAddressText() {
	return addressText;
    }

    public ContactInfo getContactInfo() {
	return contactInfo;
    }

    public String getFirstName() {
	return firstName;
    }

    public String getLastName() {
	return lastName;
    }

    @Override
    public String getName() {
	return name;
    }

    public Sex getSex() {
	return sex;
    }

    public Title getTitle() {
	return title;
    }

    public void setAddress(Address address) {
	this.address = address;
    }

    public void setAddressText(String addressText) {
	this.addressText = addressText;
    }

    public void setContactInfo(ContactInfo contactInfo) {
	this.contactInfo = contactInfo;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    @Override
    public void setName(String name) {
	this.name = name;
    }

    public void setSex(Sex sex) {
	this.sex = sex;
    }

    public void setTitle(Title title) {
	this.title = title;
    }

}
