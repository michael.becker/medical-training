package org.infai.ddls.medicaltraining.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.transaction.Transactional;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltraining.model.constraints.CycleFreeConstraint;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Audited
@CycleFreeConstraint
public class MedicalField extends TrainingAppEntity implements IHasChildren<MedicalField> {
    @ManyToOne
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "uid", scope = MedicalField.class)
    private MedicalField parent;
    @OneToMany(mappedBy = "parent")
    @JsonIgnore
    private List<MedicalField> children;

    public MedicalField() {
	this.setChildren(new ArrayList<>());
    }

    public MedicalField(String name) {
	this();
	this.setName(name);
    }

    public MedicalField(String name, String description, MedicalField parent) {
	this();
	this.setName(name);
	this.setDescription(description);
	this.setParent(parent);
    }

    /**
     * Gibt alle Fachgebiete zurück, die direkte oder indirekte Kinder dieses
     * Fachgebiets sind
     * 
     * @return
     */
    @Transactional
    @Transient
    @JsonIgnore
    public Set<MedicalField> getAllChildFields() {
	Set<MedicalField> children = new HashSet<>();

	if (null == this.children) {
	    return children;
	} else {
	    addChildren(this, children);

	    return children;
	}
    }

    @Override
    public List<MedicalField> getChildren() {
	return children;
    }

    @Override
    public MedicalField getParent() {
	return parent;
    }

    @Override
    public void setChildren(List<MedicalField> children) {
	this.children = children;
    }

    @Override
    public void setParent(MedicalField parent) {
	this.parent = parent;

	if (null != this.parent) {
	    this.parent.getChildren().add(this);
	}
    }

    @Transactional
    private void addChildren(MedicalField medicalField, Set<MedicalField> children) {
	for (MedicalField child : medicalField.getChildren()) {
	    if (children.contains(child)) {
		continue;
	    } else {
		children.add(child);
		addChildren(child, children);
	    }
	}
    }
}
