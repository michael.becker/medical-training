package org.infai.ddls.medicaltraining.model.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.infai.ddls.medicaltraining.model.TrainingSite;
import org.infai.ddls.medicaltraining.model.TrainingSiteType;

public class TrainingSiteTreatmentTypeValidator implements ConstraintValidator<TrainingSiteTreatmentTypeConstraint, TrainingSite> {
    @Override
    public boolean isValid(TrainingSite trainingSite, ConstraintValidatorContext context) {
	if (trainingSite.getTrainingSiteType().equals(TrainingSiteType.CLINIC)) {
	    return true;
	} else {
	    if (null == trainingSite.getTreatmentType()) {
		return false;
	    } else {
		return true;
	    }
	}
    }
}
