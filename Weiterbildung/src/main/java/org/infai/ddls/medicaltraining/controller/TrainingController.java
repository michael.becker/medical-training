package org.infai.ddls.medicaltraining.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.infai.ddls.medicaltraining.model.RotationPlan;
import org.infai.ddls.medicaltraining.model.RotationPlanSegment;
import org.infai.ddls.medicaltraining.model.TrainingPositionField;
import org.infai.ddls.medicaltraining.model.TrainingSchemeSegment;
import org.infai.ddls.medicaltraining.repo.PersonDAO;
import org.infai.ddls.medicaltraining.repo.RotationPlanDAO;
import org.infai.ddls.medicaltraining.repo.TrainingPositionDAO;
import org.infai.ddls.medicaltraining.repo.TrainingSchemeDAO;
import org.infai.ddls.medicaltraining.spec.PlannedTrainingPositionFieldFilter;
import org.infai.ddls.medicaltraining.spec.RotationPlanFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("training")
public class TrainingController extends AbstractMedicalTrainingController {
    @Autowired
    private PersonDAO doctorDAO;
    @Autowired
    private TrainingSchemeDAO trainingSchemeDAO;
    @Autowired
    private RotationPlanDAO rotationPlanDAO;
    @Autowired
    private TrainingPositionDAO trainingPositionDAO;

    @PostMapping("addRotationPlanSegment")
    public String addRotationPlanSegment(@Valid @ModelAttribute RotationPlanSegment rotationPlanSegment, BindingResult bindingResult, @RequestParam("trainingPosition") Long trainingPositionUID, Model model, Pageable pageable, ServletRequest servletRequest) {
	PlannedTrainingPositionFieldFilter trainingPositionFieldFilter = PlannedTrainingPositionFieldFilter.create(rotationPlanSegment);

	if (bindingResult.hasErrors()) {
	    logger.info(bindingResult.getAllErrors());
	    return listRotationPlanSegments(trainingPositionDAO.getTrainingPositionsField(trainingPositionFieldFilter, pageable.getSort()), trainingPositionFieldFilter, rotationPlanSegment, model);
	} else {
	    try {
		rotationPlanSegment.setRotationPlan(rotationPlanDAO.getRotationPlan(rotationPlanSegment.getRotationPlan().getUid(), true, false));
		rotationPlanSegment = rotationPlanDAO.saveRotationPlanSegment(rotationPlanSegment);
		return "redirect:/training/editRotationPlan/" + rotationPlanSegment.getRotationPlan().getRegistrar().getUid();
	    } catch (ConstraintViolationException e) {
		for (ConstraintViolation<?> violation : e.getConstraintViolations()) {
		    bindingResult.addError(new ObjectError(violation.getPropertyPath().toString(), violation.getMessage()));
		}

		logger.warn(bindingResult.getAllErrors());

		return listRotationPlanSegments(trainingPositionDAO.getTrainingPositionsField(trainingPositionFieldFilter, pageable.getSort()), trainingPositionFieldFilter, rotationPlanSegment, model);
	    }
	}
    }

    @GetMapping("rotationplansegments/{uid}/delete")
    public String deleteRotationPlanSegment(@PathVariable long uid, Model model) {
	RotationPlanSegment segment = rotationPlanDAO.getRotationPlanSegment(uid);
	Long rotationPlanUID = segment.getRotationPlan().getUid();

	if (rotationPlanDAO.delete(segment)) {
	    return "redirect:/training/editRotationPlan/" + rotationPlanUID;
	} else {
	    return null;
	}
    }

    @GetMapping("editRotationPlan/{uid}")
    public String editRotationPlan(@PathVariable long uid, Model model) {
	RotationPlanFilter filter = new RotationPlanFilter();
	filter.setRegistrar(doctorDAO.getRegistrar(uid));
	List<RotationPlan> rotationPlans = rotationPlanDAO.getRotationPlans(filter, null, true, true);
	RotationPlan rotationPlan;

	if (0 == rotationPlans.size()) {
	    rotationPlan = new RotationPlan();
	    rotationPlan.setRegistrar(doctorDAO.getRegistrar(uid));
	    rotationPlan.setTrainingScheme(trainingSchemeDAO.getTrainingScheme(1L, true)); // TODO: Momentan wird ein fester Plan für WB FA Allgemeinmedizin erstellt
	    rotationPlan = rotationPlanDAO.saveRotationPlan(rotationPlan);
	} else {
	    // TODO: es wird per default der erste Eintrag aus den Rotationsplänen genommen
	    // -> ist es überhaupt sinnvoll, dass ein AiW mehr als einen Rotationsplan
	    // erhalten kann?
	    rotationPlan = rotationPlans.get(0);
	}

	Map<Long, Integer> actualSegmentsDuration = new HashMap<>();
	Map<Long, Integer> finishedSegmentsDuration = new HashMap<>();
	Integer actualSegmentsDurationSum = 0;
	Integer finishedSegmentsDurationSum = 0;
	Integer unassignedSegmentsSum = 0;
	LocalDate now = LocalDate.now();

	for (TrainingSchemeSegment segment : rotationPlan.getTrainingScheme().getTrainingSchemeSegments()) {
	    List<RotationPlanSegment> actualSegments = rotationPlan.getPlannedSegments(now, segment);
	    List<RotationPlanSegment> finishedSegments = rotationPlan.getFinishedSegments(now, segment);

	    int durationActual = 0;
	    for (RotationPlanSegment actualSegment : actualSegments) {
		durationActual += actualSegment.getNormalisedDuration();
		actualSegmentsDurationSum += actualSegment.getNormalisedDuration();
	    }
	    actualSegmentsDuration.put(segment.getMedicalField().getUid(), durationActual);

	    int durationFinished = 0;
	    for (RotationPlanSegment finishedSegment : finishedSegments) {
		durationFinished += finishedSegment.getNormalisedDuration();
		finishedSegmentsDurationSum += finishedSegment.getNormalisedDuration();
	    }
	    finishedSegmentsDuration.put(segment.getMedicalField().getUid(), durationFinished);
	}

	for (RotationPlanSegment unassignedSegment : rotationPlan.getUnassignedSegments()) {
	    unassignedSegmentsSum += unassignedSegment.getNormalisedDuration();
	}

	model.addAttribute("registrar", doctorDAO.getRegistrar(uid));
	model.addAttribute("trainingScheme", trainingSchemeDAO.getTrainingScheme(1L, true));
	model.addAttribute("rotationPlan", rotationPlan);

	model.addAttribute("finishedSegments", rotationPlan.getFinishedSegments(now));
	model.addAttribute("actualSegments", rotationPlan.getPlannedSegments(now));
	System.out.println("ungeplant: " + rotationPlan.getUnassignedSegments());
	model.addAttribute("unassignedSegmentsSum", unassignedSegmentsSum);

	model.addAttribute("actualSegmentsDuration", actualSegmentsDuration);
	model.addAttribute("finishedSegmentsDuration", finishedSegmentsDuration);
	model.addAttribute("actualSegmentsDurationSum", actualSegmentsDurationSum);
	model.addAttribute("finishedSegmentsDurationSum", finishedSegmentsDurationSum);

	model.addAttribute("revisions", revisionDAO.getRotationPlanRevisions(uid, 5));

	return "training/editRotationPlan";
    }

    @GetMapping("rotationplansegments/{uid}/edit")
    public String editRotationPlanSegment(@PathVariable long uid, Model model) {
	RotationPlanSegment segment = rotationPlanDAO.getRotationPlanSegment(uid);
	model.addAttribute("rotationPlanSegment", segment);
	model.addAttribute("fundableSegments", segment.getFundableTrainingSchemeSegments(segment.getRotationPlan().getTrainingScheme()));

	return "training/editRotationPlanSegment";
    }

    @GetMapping("listRegistrars")
    public String listRegistrars(Model model, Pageable pageable) {
	model.addAttribute("registrars", doctorDAO.getRegistrars(pageable.getSort()));

	return "training/listRegistrars";
    }

    @GetMapping("listRotationPlanSegments/{rotationPlanUID}")
    public String listRotationPlanSegments(@PathVariable long rotationPlanUID, Model model, Pageable pageable) {
	return listRotationPlanSegments(trainingPositionDAO.getTrainingPositionsField(new PlannedTrainingPositionFieldFilter(), pageable.getSort()), new PlannedTrainingPositionFieldFilter(), new RotationPlanSegment(rotationPlanDAO.getRotationPlan(rotationPlanUID), 1.0), model);
    }

    @PostMapping("listRotationPlanSegments/{rotationPlanUID}")
    public String listRotationPlanSegments(@PathVariable long rotationPlanUID, @Valid @ModelAttribute("filter") PlannedTrainingPositionFieldFilter filter, BindingResult bindingResult, Model model, Pageable pageable, ServletRequest servletRequest) {
	RotationPlanSegment rotationPlanSegment = RotationPlanSegment.create(filter);
	rotationPlanSegment.setRotationPlan(rotationPlanDAO.getRotationPlan(rotationPlanUID, true, true));

	System.out.println("bearbeite Rotationsplansegment " + rotationPlanSegment + " --- " + rotationPlanSegment.getUid());

	if (bindingResult.hasErrors()) {
	    logger.info(bindingResult.getAllErrors());
	    return listRotationPlanSegments(new ArrayList<>(), filter, rotationPlanSegment, model);
	}

	return listRotationPlanSegments(trainingPositionDAO.getTrainingPositionsField(filter, pageable.getSort()), filter, rotationPlanSegment, model);
    }

    @PostMapping("updateRotationPlanSegment")
    public String updateRotationPlanSegment(@ModelAttribute @Validated RotationPlanSegment rotationPlanSegment, BindingResult bindingResult, Model model, ServletRequest servletRequest) {
	if (bindingResult.hasErrors()) {
	    logger.warn(bindingResult.getAllErrors());
	    return editRotationPlanSegment(rotationPlanSegment.getUid(), model);
	} else {
	    try {
		rotationPlanSegment = rotationPlanDAO.saveRotationPlanSegment(rotationPlanSegment);

		return "redirect:/training/editRotationPlan/" + rotationPlanSegment.getRotationPlan().getRegistrar().getUid();
	    } catch (ConstraintViolationException e) {
		for (ConstraintViolation<?> violation : e.getConstraintViolations()) {
		    bindingResult.addError(new ObjectError(violation.getPropertyPath().toString(), violation.getMessage()));
		}

		logger.warn(bindingResult.getAllErrors());

		return editRotationPlanSegment(rotationPlanSegment.getUid(), model);
	    }
	}
    }

    protected String listRotationPlanSegments(List<TrainingPositionField> trainingPositionFields, PlannedTrainingPositionFieldFilter positionFieldFilter, RotationPlanSegment rotationPlanSegment, Model model) {
	model.addAttribute("trainingPositions", trainingPositionFields);
	model.addAttribute(MODELATTRIBUTE_FILTER, positionFieldFilter);
	model.addAttribute("rotationPlanSegment", rotationPlanSegment);

	populateMedicalFields(model);

	return "training/addRotationPlanSegment";
    }
}
