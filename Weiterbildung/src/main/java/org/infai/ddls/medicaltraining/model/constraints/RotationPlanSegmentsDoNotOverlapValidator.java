package org.infai.ddls.medicaltraining.model.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.infai.ddls.medicaltraining.model.RotationPlan;
import org.infai.ddls.medicaltraining.model.RotationPlanSegment;

public class RotationPlanSegmentsDoNotOverlapValidator implements ConstraintValidator<RotationPlanSegmentsMustNotOverlapConstraint, RotationPlanSegment> {
    /**
     * Prüft, dass ein Abschnitt eines Rotationsplans sich nicht mit anderen
     * Abschnitten überlappt. Überlappungen können folgende Form annehmen:
     * <ul>
     * <li>Beginn aktueller Abschnitt < Beginn existierender Abschnitt && Ende
     * aktueller Abschnitt > Beginn existierender Abschnitt</li>
     * <li>Beginn aktueller Abschnitt = Beginn existierender Abschnitt</li>
     * <li>Beginn aktueller Abschnitt > Beginn existierender Abschnitt && Beginn
     * aktueller Abschnitt < Ende existierender Abschnitt</li>
     * </ul>
     */
    @Override
    public boolean isValid(RotationPlanSegment rotationPlanSegment, ConstraintValidatorContext context) {
	RotationPlan rotationPlan = rotationPlanSegment.getRotationPlan();

	if (null != rotationPlan && null != rotationPlan.getRotationPlanSegments() && rotationPlan.getRotationPlanSegments().size() > 0) {
	    for (RotationPlanSegment testSegment : rotationPlan.getRotationPlanSegments()) {
		System.out.println("vergleiche " + testSegment + " mit " + rotationPlanSegment);
		if (rotationPlanSegment.equals(testSegment)) {
		    continue;
		} else if (null == rotationPlanSegment.getBegin() || null == rotationPlanSegment.getEnd()) {
		    return false;
		} else if (rotationPlanSegment.getBegin().isBefore(testSegment.getBegin()) && rotationPlanSegment.getEnd().isAfter(testSegment.getBegin())) {
		    // Beginn akt < Beginn Test && Ende akt > Beginn Test return false;
		    return false;
		} else if (rotationPlanSegment.getBegin().equals(testSegment.getBegin())) {
		    // Beginn akt == Beginn test
		    return false;
		} else if (rotationPlanSegment.getBegin().isAfter(testSegment.getBegin()) && rotationPlanSegment.getBegin().isBefore(testSegment.getEnd())) {
		    // Beginn akt > Beginn Test && Beginn akt < Ende Test
		    return false;
		}
	    }
	}

	return true;
    }
}
