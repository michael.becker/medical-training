package org.infai.ddls.medicaltraining.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltraining.model.Person;

@SuppressWarnings("serial")
public class PersonSpecification<T extends Person, U extends PersonFilter> extends AbstractTrainingAppEntitySpecification<T, U> {
    public PersonSpecification(U personFilter) {
	super(personFilter);
    }

    @Override
    protected void initPredicates(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (null != filter.getFirstName()) {
	    addPredicate(isLikeFirstName(filter.getFirstName(), root, criteriaBuilder));
	}
	if (null != filter.getLastName()) {
	    addPredicate(isLikeLastName(filter.getLastName(), root, criteriaBuilder));
	}
	if (null != filter.getName()) {
	    Predicate firstName = isLikeFirstName(filter.getName(), root, criteriaBuilder);
	    Predicate lastName = isLikeLastName(filter.getName(), root, criteriaBuilder);

	    addPredicateDisjunction(criteriaBuilder, firstName, lastName);
	}
	if (null != filter.getSex()) {
	    addPredicate(criteriaBuilder.equal(root.get("sex"), filter.getSex()));
	}
	if (null != filter.getTitle()) {
	    addPredicate(criteriaBuilder.equal(root.get("title"), filter.getTitle()));
	}
	if (null != filter.getAddressText()) {
	    Predicate street = isLikeStreet(filter.getAddressText(), root, criteriaBuilder);
	    Predicate zipCode = isLikeZipCode(filter.getAddressText(), root, criteriaBuilder);
	    Predicate city = isLikeCity(filter.getAddressText(), root, criteriaBuilder);

	    addPredicateDisjunction(criteriaBuilder, street, zipCode, city);
	}
    }

    private Predicate isLikeCity(String Street, Root<T> root, CriteriaBuilder criteriaBuilder) {
	return likeIgnoreCase(Street, root.get("address").get("city"), criteriaBuilder);
    }

    private Predicate isLikeFirstName(String firstName, Root<T> root, CriteriaBuilder criteriaBuilder) {
	return likeIgnoreCase(firstName, root.get("name"), criteriaBuilder);
    }

    private Predicate isLikeLastName(String lastName, Root<T> root, CriteriaBuilder criteriaBuilder) {
	return likeIgnoreCase(lastName, root.get("lastName"), criteriaBuilder);
    }

    private Predicate isLikeStreet(String Street, Root<T> root, CriteriaBuilder criteriaBuilder) {
	return likeIgnoreCase(Street, root.get("address").get("street"), criteriaBuilder);
    }

    private Predicate isLikeZipCode(String Street, Root<T> root, CriteriaBuilder criteriaBuilder) {
	return likeIgnoreCase(Street, root.get("address").get("zipCode"), criteriaBuilder);
    }
}
