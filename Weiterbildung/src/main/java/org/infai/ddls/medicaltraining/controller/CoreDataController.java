package org.infai.ddls.medicaltraining.controller;

import java.util.List;

import javax.servlet.ServletRequest;

import org.infai.ddls.medicaltraining.config.SecurityConfiguration;
import org.infai.ddls.medicaltraining.model.Equipment;
import org.infai.ddls.medicaltraining.model.MedicalField;
import org.infai.ddls.medicaltraining.model.Sex;
import org.infai.ddls.medicaltraining.model.Title;
import org.infai.ddls.medicaltraining.spec.EquipmentFilter;
import org.infai.ddls.medicaltraining.spec.MedicalFieldFilter;
import org.infai.ddls.medicaltraining.spec.SexFilter;
import org.infai.ddls.medicaltraining.spec.TitleFilter;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CoreDataController extends AbstractMedicalTrainingController {
    public static final String VIEW_EQUIPMENTS_LIST = "coredata/listEquipments";
    public static final String VIEW_EQUIPMENTS_EDIT = "coredata/editEquipment";
    public static final String VIEW_MEDICALFIELDS_LIST = "coredata/listMedicalFields";
    public static final String VIEW_MEDICALFIELDS_EDIT = "coredata/editMedicalField";
    public static final String VIEW_SEXES_LIST = "coredata/listSexes";
    public static final String VIEW_SEXES_EDIT = "coredata/editSex";
    public static final String VIEW_TITLES_LIST = "coredata/listTitles";
    public static final String VIEW_TITLES_EDIT = "coredata/editTitle";

    @GetMapping(RequestMappings.EQUIPMENTS_ARCHIVE)
    @PreAuthorize(SecurityConfiguration.EL_HAS_ROLE_MANAGER)
    public String equipmentsArchive(@PathVariable long uid, Model model) {
	Equipment equipment = trainingSiteDAO.getEquipment(uid);
	equipment.setArchived(true);

	trainingSiteDAO.save(equipment);

	return redirect(RequestMappings.EQUIPMENTS_LIST);
    }

    @GetMapping(RequestMappings.EQUIPMENTS_CREATE)
    @PreAuthorize(SecurityConfiguration.EL_HAS_ROLE_MANAGER)
    public String equipmentsCreate(Model model) {
	return equipmentEdit(new Equipment(), model);
    }

    @GetMapping(RequestMappings.EQUIPMENTS_EDIT)
    @PreAuthorize(SecurityConfiguration.EL_HAS_ROLE_MANAGER)
    public String equipmentsEdit(@PathVariable(RequestMappings.UID) long equipmentUID, Model model) {
	return equipmentEdit(trainingSiteDAO.getEquipment(equipmentUID), model);
    }

    @PostMapping(RequestMappings.EQUIPMENTS_FILTER)
    public String equipmentsFilter(@ModelAttribute @Validated EquipmentFilter filter, Model model, Pageable pageable) {
	return equipmentsList(trainingSiteDAO.getEquipments(filter, pageable), filter, model);
    }

    @GetMapping(RequestMappings.EQUIPMENTS_LIST)
    public String equipmentsList(Model model) {
	return equipmentsList(trainingSiteDAO.getEquipments(), new EquipmentFilter(), model);
    }

    @PostMapping(RequestMappings.EQUIPMENTS_UPDATE)
    @PreAuthorize(SecurityConfiguration.EL_HAS_ROLE_MANAGER)
    public String equipmentsUpdate(@ModelAttribute(MODELATTRIBUTE_EQUIPMENT) Equipment equipment, BindingResult bindingResult, Model model, ServletRequest servletRequest) {
	System.out.println(formatHTTPParameters(servletRequest));
	if (bindingResult.hasErrors()) {
	    logger.info(bindingResult.getAllErrors());

	    return equipmentEdit(equipment, model);
	} else {
	    trainingSiteDAO.save(equipment);
	    return redirect(RequestMappings.EQUIPMENTS_LIST);
	}
    }

    @GetMapping(RequestMappings.MEDICALFIELDS_ARCHIVE)
    @PreAuthorize(SecurityConfiguration.EL_HAS_ROLE_MANAGER)
    public String medicalFieldsArchive(@PathVariable long uid, Model model) {
	MedicalField medicalField = trainingDAO.getMedicalField(uid);
	medicalField.setArchived(true);

	trainingDAO.save(medicalField);

	return redirect(RequestMappings.MEDICALFIELDS_LIST);
    }

    @GetMapping(RequestMappings.MEDICALFIELDS_CREATE)
    @PreAuthorize(SecurityConfiguration.EL_HAS_ROLE_MANAGER)
    public String medicalFieldsCreate(Model model) {
	return medicalFieldEdit(new MedicalField(), model);
    }

    @GetMapping(RequestMappings.MEDICALFIELDS_EDIT)
    @PreAuthorize(SecurityConfiguration.EL_HAS_ROLE_MANAGER)
    public String medicalFieldsEdit(@PathVariable(RequestMappings.UID) long medicalFieldUID, Model model) {
	return medicalFieldEdit(trainingDAO.getMedicalField(medicalFieldUID, true, true), model);
    }

    @PostMapping(RequestMappings.MEDICALFIELDS_FILTER)
    public String medicalFieldsFilter(@ModelAttribute @Validated MedicalFieldFilter filter, Model model, Pageable pageable) {
	return medicalFieldsList(trainingDAO.getMedicalFields(filter, pageable.getSort()), filter, model);
    }

    @GetMapping(RequestMappings.MEDICALFIELDS_LIST)
    public String medicalFieldsList(Model model) {
	return medicalFieldsList(trainingDAO.getMedicalFields(), new MedicalFieldFilter(), model);
    }

    @PostMapping(RequestMappings.MEDICALFIELDS_UPDATE)
    @PreAuthorize(SecurityConfiguration.EL_HAS_ROLE_MANAGER)
    public String medicalFieldsUpdate(@ModelAttribute(MODELATTRIBUTE_MEDICALFIELD) @Validated MedicalField medicalField, BindingResult bindingResult, Model model, ServletRequest servletRequest) {
	if (bindingResult.hasErrors()) {
	    logger.info(bindingResult.getAllErrors());

	    return medicalFieldEdit(medicalField, model);
	}

	trainingDAO.save(medicalField);
	return redirect(RequestMappings.MEDICALFIELDS_LIST);
    }

    @GetMapping(RequestMappings.SEXES_ARCHIVE)
    @PreAuthorize(SecurityConfiguration.EL_HAS_ROLE_MANAGER)
    public String sexesArchive(@PathVariable long uid, Model model) {
	Sex sex = personDAO.getSex(uid);
	sex.setArchived(true);

	personDAO.save(sex);

	return redirect(RequestMappings.SEXES_LIST);
    }

    @GetMapping(RequestMappings.SEXES_CREATE)
    @PreAuthorize(SecurityConfiguration.EL_HAS_ROLE_MANAGER)
    public String sexesCreate(Model model) {
	return sexEdit(new Sex(), model);
    }

    @GetMapping(RequestMappings.SEXES_EDIT)
    @PreAuthorize(SecurityConfiguration.EL_HAS_ROLE_MANAGER)
    public String sexesEdit(@PathVariable(RequestMappings.UID) long sexUID, Model model) {
	return sexEdit(personDAO.getSex(sexUID), model);
    }

    @PostMapping(RequestMappings.SEXES_FILTER)
    public String sexesFilter(@ModelAttribute @Validated SexFilter filter, Model model, Pageable pageable) {
	return sexesList(personDAO.getSexes(filter, pageable), filter, model);
    }

    @GetMapping(RequestMappings.SEXES_LIST)
    public String sexesList(Model model, Pageable pageable) {
	return sexesList(personDAO.getSexes(), new SexFilter(), model);
    }

    @PostMapping(RequestMappings.SEXES_UPDATE)
    @PreAuthorize(SecurityConfiguration.EL_HAS_ROLE_MANAGER)
    public String sexesUpdate(@ModelAttribute(MODELATTRIBUTE_SEX) @Validated Sex sex, BindingResult bindingResult, Model model, ServletRequest servletRequest) {
	if (bindingResult.hasErrors()) {
	    logger.info(bindingResult.getAllErrors());

	    return sexEdit(sex, model);
	} else {
	    personDAO.save(sex);

	    return redirect(RequestMappings.SEXES_LIST);
	}
    }

    @GetMapping(RequestMappings.TITLES_ARCHIVE)
    @PreAuthorize(SecurityConfiguration.EL_HAS_ROLE_MANAGER)
    public String titlesArchive(@PathVariable long uid, Model model) {
	Title title = personDAO.getTitle(uid);
	title.setArchived(true);

	personDAO.save(title);

	return redirect(RequestMappings.TITLES_LIST);
    }

    @GetMapping(RequestMappings.TITLES_CREATE)
    @PreAuthorize(SecurityConfiguration.EL_HAS_ROLE_MANAGER)
    public String titlesCreate(Model model) {
	return titleEdit(new Title(), model);
    }

    @GetMapping(RequestMappings.TITLES_EDIT)
    @PreAuthorize(SecurityConfiguration.EL_HAS_ROLE_MANAGER)
    public String titlesEdit(@PathVariable(RequestMappings.UID) long titleUID, Model model) {
	return titleEdit(personDAO.getTitle(titleUID), model);
    }

    @PostMapping(RequestMappings.TITLES_FILTER)
    public String titlesFilter(@ModelAttribute @Validated TitleFilter filter, Model model, Pageable pageable) {
	return titlesList(personDAO.getTitles(filter, pageable), filter, model);
    }

    @GetMapping(RequestMappings.TITLES_LIST)
    public String titlesList(Model model, Pageable pageable) {
	return titlesList(personDAO.getTitles(), new TitleFilter(), model);
    }

    @PostMapping(RequestMappings.TITLES_UPDATE)
    @PreAuthorize(SecurityConfiguration.EL_HAS_ROLE_MANAGER)
    public String titlesUpdate(@ModelAttribute(MODELATTRIBUTE_TITLE) @Validated Title title, BindingResult bindingResult, Model model, ServletRequest servletRequest) {
	if (bindingResult.hasErrors()) {
	    logger.info(bindingResult.getAllErrors());

	    return titleEdit(title, model);
	} else {
	    personDAO.save(title);

	    return redirect(RequestMappings.TITLES_LIST);
	}
    }

    private String equipmentEdit(Equipment equipment, Model model) {
	model.addAttribute(MODELATTRIBUTE_EQUIPMENT, equipment);

	return VIEW_EQUIPMENTS_EDIT;
    }

    private String equipmentsList(List<Equipment> equipments, EquipmentFilter filter, Model model) {
	model.addAttribute(MODELATTRIBUTE_EQUIPMENTS, equipments);
	model.addAttribute(MODELATTRIBUTE_FILTER, filter);

	return VIEW_EQUIPMENTS_LIST;
    }

    private String medicalFieldEdit(MedicalField medicalField, Model model) {
	model.addAttribute(MODELATTRIBUTE_MEDICALFIELD, medicalField);
	model.addAttribute(MODELATTRIBUTE_MEDICALFIELDS, trainingDAO.getMedicalFields());

	return VIEW_MEDICALFIELDS_EDIT;
    }

    private String medicalFieldsList(List<MedicalField> medicalFields, MedicalFieldFilter filter, Model model) {
	model.addAttribute(MODELATTRIBUTE_MEDICALFIELDS, medicalFields);
	model.addAttribute(MODELATTRIBUTE_FILTER, filter);

	return VIEW_MEDICALFIELDS_LIST;
    }

    private String sexEdit(Sex sex, Model model) {
	model.addAttribute(MODELATTRIBUTE_SEX, sex);

	return VIEW_SEXES_EDIT;
    }

    private String sexesList(List<Sex> sexes, SexFilter filter, Model model) {
	model.addAttribute(MODELATTRIBUTE_SEXES, sexes);
	model.addAttribute(MODELATTRIBUTE_FILTER, filter);

	return VIEW_SEXES_LIST;
    }

    private String titleEdit(Title title, Model model) {
	model.addAttribute(MODELATTRIBUTE_TITLE, title);

	return VIEW_TITLES_EDIT;
    }

    private String titlesList(List<Title> titles, TitleFilter filter, Model model) {
	model.addAttribute(MODELATTRIBUTE_TITLES, titles);
	model.addAttribute(MODELATTRIBUTE_FILTER, filter);

	return VIEW_TITLES_LIST;
    }
}
