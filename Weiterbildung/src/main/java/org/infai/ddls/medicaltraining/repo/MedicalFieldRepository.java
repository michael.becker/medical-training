package org.infai.ddls.medicaltraining.repo;

import org.infai.ddls.medicaltraining.model.MedicalField;
import org.springframework.stereotype.Repository;

@Repository
interface MedicalFieldRepository extends TrainingAppEntityRepository<MedicalField> {

}
