package org.infai.ddls.medicaltraining.spec;

public class RegistrarFilter extends PersonFilter {
    private String trainingPosition;

    public String getTrainingPosition() {
	return trainingPosition;
    }

    public void setTrainingPosition(String trainingPosition) {
	this.trainingPosition = trainingPosition;
    }
}
