package org.infai.ddls.medicaltraining.repo;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.springframework.stereotype.Repository;

@Repository
interface AuthorisationFieldRepository extends TrainingAppEntityRepository<AuthorisationField> {

}
