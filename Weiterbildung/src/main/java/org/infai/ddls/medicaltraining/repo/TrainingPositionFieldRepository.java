package org.infai.ddls.medicaltraining.repo;

import org.infai.ddls.medicaltraining.model.TrainingPositionField;
import org.springframework.stereotype.Repository;

@Repository
interface TrainingPositionFieldRepository extends TrainingAppEntityRepository<TrainingPositionField> {
}
