package org.infai.ddls.medicaltraining.repo;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;
import org.infai.ddls.medicaltraining.model.Registrar;
import org.infai.ddls.medicaltraining.model.Sex;
import org.infai.ddls.medicaltraining.model.Title;
import org.infai.ddls.medicaltraining.model.TrainingSite;
import org.infai.ddls.medicaltraining.spec.AuthorisedDoctorFilter;
import org.infai.ddls.medicaltraining.spec.AuthorisedDoctorSpecification;
import org.infai.ddls.medicaltraining.spec.RegistrarFilter;
import org.infai.ddls.medicaltraining.spec.RegistrarSpecification;
import org.infai.ddls.medicaltraining.spec.SexFilter;
import org.infai.ddls.medicaltraining.spec.SexSpecification;
import org.infai.ddls.medicaltraining.spec.TitleFilter;
import org.infai.ddls.medicaltraining.spec.TitleSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

@Component
public class PersonDAO extends AbstractTrainingAppDAO {
    @Autowired
    private AuthorisedDoctorRepository authorisedDoctorRepository;
    @Autowired
    private RegistrarRepository registrarRepository;
    @Autowired
    private SexRepository sexRepository;
    @Autowired
    private TitleRepository titleRepository;
    @Autowired
    private AuthorisationDAO authorisationDAO;

    @Transactional
    public boolean delete(AuthorisedDoctor authorisedDoctor) {
	return delete(authorisedDoctor, false, false, false);
    }

    @Transactional
    public boolean delete(AuthorisedDoctor authorisedDoctor, boolean deleteIfAuthorisationIsPresent) {
	return delete(authorisedDoctor, deleteIfAuthorisationIsPresent, false, false);
    }

    @Transactional
    public boolean delete(AuthorisedDoctor authorisedDoctor, boolean deleteIfAuthorisationIsPresent, boolean deleteIfTrainingPositionIsPresent, boolean deleteIfTrainingPositionIsUsed) {
	List<AuthorisationField> authorisations = authorisationDAO.getAuthorisationsField(authorisedDoctor);

	if (!authorisations.isEmpty()) {
	    if (deleteIfAuthorisationIsPresent) {
		for (AuthorisationField authorisation : authorisations) {
		    authorisationDAO.delete(authorisation, deleteIfTrainingPositionIsPresent, deleteIfTrainingPositionIsUsed);
		}
	    } else {
		logger.warn("Attempted to delete " + authorisedDoctor + " but authorisations are present!");
		return false;
	    }
	}

	authorisedDoctorRepository.delete(authorisedDoctor);
	return true;
    }

    @Transactional
    public AuthorisedDoctor getAuthorisedDoctor(Long uid) {
	return getAuthorisedDoctor(uid, false, false);
    }

    @Transactional
    public AuthorisedDoctor getAuthorisedDoctor(Long uid, boolean loadAdditionalFields) {
	return getAuthorisedDoctor(uid, loadAdditionalFields, false);
    }

    @Transactional
    public AuthorisedDoctor getAuthorisedDoctor(Long uid, boolean loadAdditionalFields, boolean loadTrainingSites) {
	Optional<AuthorisedDoctor> optional = authorisedDoctorRepository.findById(uid);

	if (optional.isPresent()) {
	    AuthorisedDoctor authorisedDoctor = optional.get();

	    if (loadAdditionalFields) {
		authorisedDoctor.getAdditionalFields().size();
	    }

	    if (loadTrainingSites) {
		authorisedDoctor.getTrainingSites().size();
	    }

	    return authorisedDoctor;
	} else {
	    return null;
	}
    }

    @Transactional
    public List<AuthorisedDoctor> getAuthorisedDoctors(AuthorisedDoctorFilter filter, Sort sort, boolean loadTrainingSites) {
	return getAuthorisedDoctors(filter, sort, loadTrainingSites, false);
    }

    @Transactional
    public List<AuthorisedDoctor> getAuthorisedDoctors(AuthorisedDoctorFilter filter, Sort sort, boolean loadTrainingSites, boolean loadAdditionalFields) {
	if (null == sort || sort.isUnsorted()) {
	    sort = new Sort(Direction.ASC, "lastName");
	}
	AuthorisedDoctorSpecification spec = new AuthorisedDoctorSpecification(filter);

	List<AuthorisedDoctor> authorisedDoctors = authorisedDoctorRepository.findAll(spec, sort);

	for (AuthorisedDoctor authorisedDoctor : authorisedDoctors) {
	    if (loadTrainingSites) {
		// TODO: Es kommt eine lazy initialization exception, wenn man sich das
		// JSON-Ergebnis komplett anzeigen lässt. Eventuell kann das wieder raus, wenn
		// nur ein Teil des Ergebnisses angezeigt wird.
		for (TrainingSite trainingSite : authorisedDoctor.getTrainingSites()) {
		    trainingSite.getMedicalField().getChildren().size();
		    System.out.println(trainingSite.getMedicalField().getChildren());
		}
		authorisedDoctor.getTrainingSites().size();
	    }

	    if (loadAdditionalFields) {
		authorisedDoctor.getAdditionalFields().size();
	    }
	}

	return authorisedDoctors;
    }

    @Transactional
    public List<AuthorisedDoctor> getAuthorisedDoctors(Sort sort) {
	return getAuthorisedDoctors(new AuthorisedDoctorFilter(), sort, false);
    }

    @Transactional
    public List<AuthorisedDoctor> getAuthorisedDoctors(Sort sort, boolean loadTrainingSites) {
	return getAuthorisedDoctors(new AuthorisedDoctorFilter(), sort, loadTrainingSites);
    }

    @Transactional
    public Registrar getRegistrar(Long uid) {
	Registrar registrar = registrarRepository.findById(uid).get();
	registrar.getAdditionalFields().size();

	return registrar;
    }

    public List<Registrar> getRegistrars(RegistrarFilter filter, Sort sort) {
	RegistrarSpecification spec = new RegistrarSpecification(filter);

	if (null == sort || sort.isUnsorted()) {
	    sort = new Sort(Direction.ASC, "lastName");
	}

	return registrarRepository.findAll(spec, sort);
    }

    public List<Registrar> getRegistrars(Sort sort) {
	return getRegistrars(new RegistrarFilter(), sort);
    }

    public Sex getSex(Long uid) {
	return sexRepository.findById(uid).get();
    }

    public List<Sex> getSexes() {
	return getSexes(new SexFilter(), null);
    }

    public List<Sex> getSexes(SexFilter filter, Pageable pageable) {
	SexSpecification spec = new SexSpecification(filter);
	Sort sort = evaluateSort(pageable);

	return sexRepository.findAll(spec, sort);
    }

    public Title getTitle(Long uid) {
	return titleRepository.findById(uid).get();
    }

    public List<Title> getTitles() {
	return getTitles(new TitleFilter(), null);
    }

    public List<Title> getTitles(TitleFilter filter, Pageable pageable) {
	TitleSpecification spec = new TitleSpecification(filter);
	Sort sort = evaluateSort(pageable);

	return titleRepository.findAll(spec, sort);
    }

    public boolean isLANRusedByOther(AuthorisedDoctor authorisedDoctor) {
	if (null == authorisedDoctor.getLanr()) {
	    return false;
	} else if (null == authorisedDoctor.getUid()) {
	    return authorisedDoctorRepository.findByLanr(authorisedDoctor.getLanr()).isPresent();
	} else {
	    return authorisedDoctorRepository.findByLanrAndUidNot(authorisedDoctor.getLanr(), authorisedDoctor.getUid()).isPresent();
	}
    }

    @Transactional
    public AuthorisedDoctor save(AuthorisedDoctor authorisedDoctor) {
	return authorisedDoctorRepository.save(authorisedDoctor);
    }

    public Registrar save(Registrar registrar) {
	return registrarRepository.save(registrar);
    }

    @Transactional
    public Sex save(Sex sex) {
	return sexRepository.save(sex);
    }

    @Transactional
    public Title save(Title title) {
	return titleRepository.save(title);
    }
}
