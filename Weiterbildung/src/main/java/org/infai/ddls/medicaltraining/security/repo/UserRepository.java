package org.infai.ddls.medicaltraining.security.repo;

import org.infai.ddls.medicaltraining.security.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

}
