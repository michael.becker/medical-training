package org.infai.ddls.medicaltraining.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.model.Registrar;
import org.infai.ddls.medicaltraining.model.RotationPlan;
import org.infai.ddls.medicaltraining.model.TrainingAppEntity;
import org.infai.ddls.medicaltraining.model.TrainingPositionField;

@SuppressWarnings("serial")
public class RotationPlanSpecification<T extends TrainingAppEntity> extends AbstractTrainingAppEntitySpecification<T, RotationPlanFilter> {
    public static final String ATTRIBUTE_NAME_TRAINING_POSITION = "trainingPosition";
    public static final String ATTRIBUTE_NAME_AUTHORISATION = "authorisationField";
    public static final String ATTRIBUTE_NAME_REGISTRAR = "registrar";
    public static final String ATTRIBUTE_NAME_ROTATION_PLAN = "rotationPlan";

    public RotationPlanSpecification(RotationPlanFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (null != filter.getUid()) {
	    addPredicate(hasUID(filter.getUid(), root, query, criteriaBuilder));
	}
	if (null != filter.getAuthorisationField()) {
	    addPredicate(isAtAuthorisation(filter.getAuthorisationField(), root, query, criteriaBuilder));
	}
	if (null != filter.getRegistrar()) {
	    addPredicate(isFromRegistrar(filter.getRegistrar(), root, query, criteriaBuilder));
	}
	if (null != filter.getTrainingPositionField()) {
	    addPredicate(isAtTrainingPosition(filter.getTrainingPositionField(), root, query, criteriaBuilder));
	}
	if (null != filter.getRotationPlan()) {
	    addPredicate(isFromRotationPlan(filter.getRotationPlan(), root, query, criteriaBuilder));
	}
	if (null != filter.getReferenceDate()) {
	    Predicate referenceDateAfterBegin = criteriaBuilder.lessThanOrEqualTo(root.get("begin"), filter.getReferenceDate());
	    Predicate referenceDateBeforEnd = criteriaBuilder.greaterThanOrEqualTo(root.get("end"), filter.getReferenceDate());

	    addPredicateConjunction(criteriaBuilder, referenceDateAfterBegin, referenceDateBeforEnd);
	}
    }

    private Predicate hasUID(Long uid, Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get(ATTRIBUTE_NAME_UID), uid);
    }

    private Predicate isAtAuthorisation(AuthorisationField authorisationField, Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get(ATTRIBUTE_NAME_TRAINING_POSITION).get(ATTRIBUTE_NAME_AUTHORISATION), authorisationField);
    }

    private Predicate isAtTrainingPosition(TrainingPositionField trainingPosition, Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get(ATTRIBUTE_NAME_TRAINING_POSITION), trainingPosition);
    }

    private Predicate isFromRegistrar(Registrar registrar, Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (filter.isSearchForSegments()) {
	    return criteriaBuilder.equal(root.get(ATTRIBUTE_NAME_ROTATION_PLAN).get(ATTRIBUTE_NAME_REGISTRAR), registrar);
	} else {
	    return criteriaBuilder.equal(root.get(ATTRIBUTE_NAME_REGISTRAR), registrar);
	}
    }

    private Predicate isFromRotationPlan(RotationPlan rotationPlan, Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get(ATTRIBUTE_NAME_ROTATION_PLAN), rotationPlan);
    }
}
