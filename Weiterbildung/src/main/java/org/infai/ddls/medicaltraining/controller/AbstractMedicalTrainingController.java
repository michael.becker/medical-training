package org.infai.ddls.medicaltraining.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

import javax.servlet.ServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.infai.ddls.medicaltraining.logging.RevisionDAO;
import org.infai.ddls.medicaltraining.model.TrainingSite;
import org.infai.ddls.medicaltraining.repo.AuthorisationDAO;
import org.infai.ddls.medicaltraining.repo.PersonDAO;
import org.infai.ddls.medicaltraining.repo.RotationPlanDAO;
import org.infai.ddls.medicaltraining.repo.TrainingDAO;
import org.infai.ddls.medicaltraining.repo.TrainingPositionDAO;
import org.infai.ddls.medicaltraining.repo.TrainingSchemeDAO;
import org.infai.ddls.medicaltraining.repo.TrainingSiteNewDAO;
import org.infai.ddls.medicaltraining.spec.AuthorisationFieldFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

@Controller
public abstract class AbstractMedicalTrainingController implements IMedicalTrainingController {
    public static final String MODELATTRIBUTE_EQUIPMENT = "equipment";
    public static final String MODELATTRIBUTE_EQUIPMENTS = "equipments";
    public static final String MODELATTRIBUTE_MEDICALFIELD = "medicalField";
    public static final String MODELATTRIBUTE_MEDICALFIELDS = "medicalFields";
    public static final String MODELATTRIBUTE_SEX = "sex";
    public static final String MODELATTRIBUTE_SEXES = "sexes";
    public static final String MODELATTRIBUTE_TITLE = "title";
    public static final String MODELATTRIBUTE_TITLES = "titles";

    public static final String MODELATTRIBUTE_AUTHORISED_DOCTORS = "authorisedDoctors";
    public static final String MODELATTRIBUTE_AUTHORISED_DOCTOR = "authorisedDoctor";
    public static final String MODELATTRIBUTE_REGISTRARS = "registrars";
    public static final String MODELATTRIBUTE_REGISTRAR = "registrar";

    public static final String MODELATTRIBUTE_TRAININGSCHEMES = "trainingSchemes";
    public static final String MODELATTRIBUTE_TRAININGSCHEME = "trainingScheme";

    public static final String MODELATTRIBUTE_AUTHORISATIONS = "authorisations";

    public static final String MODELATTRIBUTE_FILTER = "filter";

    public static final String SWITCH_CHECKBOX_CHECKED = "on";

    public static final String PARAM_SAVE_ACTION = "saveAction";
    public static final String PARAM_VALUE_SAVE = "save";
    public static final String PARAM_VALUE_SAVEANDCLOSE = "saveAndClose";

    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    protected TrainingPositionDAO trainingPositionDAO;
    @Autowired
    protected TrainingSiteNewDAO trainingSiteDAO;
    @Autowired
    protected TrainingSchemeDAO trainingSchemeDAO;
    @Autowired
    protected AuthorisationDAO authorisationDAO;
    @Autowired
    protected PersonDAO personDAO;
    @Autowired
    protected TrainingDAO trainingDAO;
    @Autowired
    protected RotationPlanDAO rotationPlanDAO;
    @Autowired
    protected RevisionDAO revisionDAO;

    // @ExceptionHandler
    // @ResponseStatus(HttpStatus.BAD_REQUEST)
    // public void handle(Exception e) {
    // logger.error("Returning HTTP 400 Bad Request ", e);
    // }

    protected Map<String, String> evaluateAdditionalFields(ServletRequest servletRequest) {
	Map<String, String> additionalFields = new HashMap<>();

	int i = 0;
	while (null != servletRequest.getParameter("additional[" + i + "].key")) {
	    String key = servletRequest.getParameter("additional[" + i + "].key");
	    String value = servletRequest.getParameter("additional[" + i + "].value");

	    additionalFields.put(key, value);

	    i++;
	}

	return additionalFields;
    }

    protected String formatHTTPParameters(ServletRequest servletRequest) {
	StringBuilder logMessage = new StringBuilder();
	logMessage.append("\nHTTP Parameters\n");
	TreeSet<String> parameters = new TreeSet<>(servletRequest.getParameterMap().keySet());
	for (String parameter : parameters) {
	    for (String parValue : servletRequest.getParameterMap().get(parameter)) {
		logMessage.append("\t").append(parameter).append(": ").append(parValue).append("\n");
	    }
	}

	return logMessage.toString();
    }

    protected boolean isEmpty(String parameter, ServletRequest servletRequest) {
	if (null == servletRequest.getParameter(parameter) || servletRequest.getParameter(parameter).isEmpty()) {
	    return true;
	} else {
	    return false;
	}
    }

    protected boolean isSave(ServletRequest servletRequest) {
	if (servletRequest.getParameter(PARAM_SAVE_ACTION).equals(PARAM_VALUE_SAVE)) {
	    return true;
	} else {
	    return false;
	}
    }

    protected boolean isSaveAndClose(ServletRequest servletRequest) {
	if (servletRequest.getParameter(PARAM_SAVE_ACTION).equals(PARAM_VALUE_SAVEANDCLOSE)) {
	    return true;
	} else {
	    return false;
	}
    }

    protected void populateAuthorisationFields(Model model) {
	model.addAttribute("authorisations", authorisationDAO.getAuthorisationFields(null));
    }

    protected void populateAuthorisations(Model model, TrainingSite trainingSite) {
	AuthorisationFieldFilter filter = new AuthorisationFieldFilter();
	filter.setTrainingSite(trainingSite);

	model.addAttribute(MODELATTRIBUTE_AUTHORISATIONS, authorisationDAO.getAuthorisations(filter, null, false, false, false));
    }

    protected void populateAuthorisedDoctors(Model model) {
	model.addAttribute(MODELATTRIBUTE_AUTHORISED_DOCTORS, personDAO.getAuthorisedDoctors(null, false));
    }

    protected void populateEquipments(Model model) {
	model.addAttribute(MODELATTRIBUTE_EQUIPMENTS, trainingSiteDAO.getEquipments());
    }

    protected void populateMedicalFields(Model model) {
	model.addAttribute(MODELATTRIBUTE_MEDICALFIELDS, trainingDAO.getMedicalFields());
    }

    protected void populateSexes(Model model) {
	model.addAttribute(MODELATTRIBUTE_SEXES, personDAO.getSexes());
    }

    protected void populateTitles(Model model) {
	model.addAttribute(MODELATTRIBUTE_TITLES, personDAO.getTitles());
    }

    protected void populateTrainingSchemes(Model model) {
	model.addAttribute(MODELATTRIBUTE_TRAININGSCHEMES, trainingSchemeDAO.getTrainingSchemes());
    }

    protected void populateTrainingSites(Model model, boolean loadTrainingSiteDepartments, boolean loadEquipments) {
	model.addAttribute("trainingSites", trainingSiteDAO.getTrainingSites(null, loadTrainingSiteDepartments, loadEquipments));
    }

    protected String redirect(String url) {
	return RequestMappings.__REDIRECT + url;
    }
}
