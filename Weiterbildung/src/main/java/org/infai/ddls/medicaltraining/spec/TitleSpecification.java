package org.infai.ddls.medicaltraining.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltraining.model.Title;

@SuppressWarnings("serial")
public class TitleSpecification extends AbstractTrainingAppEntitySpecification<Title, TitleFilter> {
    public TitleSpecification(TitleFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<Title> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

    }

}
