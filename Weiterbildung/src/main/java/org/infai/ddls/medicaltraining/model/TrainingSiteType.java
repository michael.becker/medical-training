package org.infai.ddls.medicaltraining.model;

public enum TrainingSiteType {
    CLINIC, DEPARTMENT, DOCTORSOFFICE;
}
