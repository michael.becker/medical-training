package org.infai.ddls.medicaltraining.spec;

public class TrainingAppEntityFilter {
    private boolean allowArchived;
    private String name;

    public String getName() {
	return name;
    }

    public boolean isAllowArchived() {
	return allowArchived;
    }

    public void setAllowArchived(boolean allowArchived) {
	this.allowArchived = allowArchived;
    }

    public void setName(String name) {
	this.name = name;
    }
}
