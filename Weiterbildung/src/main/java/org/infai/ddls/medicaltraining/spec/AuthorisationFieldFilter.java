package org.infai.ddls.medicaltraining.spec;

import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;
import org.infai.ddls.medicaltraining.model.TrainingSite;

public class AuthorisationFieldFilter extends TrainingAppEntityFilter {
    private TrainingSite trainingSite;
    private AuthorisedDoctor authorisedDoctor;

    public AuthorisedDoctor getAuthorisedDoctor() {
	return authorisedDoctor;
    }

    public TrainingSite getTrainingSite() {
	return trainingSite;
    }

    public void setAuthorisedDoctor(AuthorisedDoctor authorisedDoctor) {
	this.authorisedDoctor = authorisedDoctor;
    }

    public void setTrainingSite(TrainingSite trainingSite) {
	this.trainingSite = trainingSite;
    }
}
