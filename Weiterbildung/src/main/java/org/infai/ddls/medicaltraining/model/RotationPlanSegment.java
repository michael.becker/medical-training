package org.infai.ddls.medicaltraining.model;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltraining.model.constraints.RotationPlanSegmentBeginBeforeEndConstraint;
import org.infai.ddls.medicaltraining.model.constraints.RotationPlanSegmentMinimalDurationConstraint;
import org.infai.ddls.medicaltraining.model.constraints.RotationPlanSegmentsMustNotOverlapConstraint;
import org.infai.ddls.medicaltraining.spec.PlannedTrainingPositionFieldFilter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

/**
 * Repräsentiert einen Abschnitt eines Rotationsplans. Der Abschnitt wird an
 * einer Weiterbildungsstätte erbracht.
 * 
 * @author Michael Becker
 *
 */
@Entity
@RotationPlanSegmentBeginBeforeEndConstraint
@RotationPlanSegmentMinimalDurationConstraint
@RotationPlanSegmentsMustNotOverlapConstraint
@Audited
public class RotationPlanSegment extends TrainingAppEntity {
    /**
     * Erstellt aus dem gegebenen Filter einen Abschnitt eines Rotationsplans, indem
     * die Filterdaten als Abschnittsdaten übernommen werden.
     * 
     * @param filter
     * @return
     */
    public static RotationPlanSegment create(PlannedTrainingPositionFieldFilter filter) {
	RotationPlanSegment segment = new RotationPlanSegment();
	segment.setBegin(filter.getAvailableFrom());
	segment.setEnd(filter.getAvailableUntil());
	segment.setFullTimeEquivalent(filter.getFullTimeEquivalent());

	return segment;
    }

    @ManyToOne(optional = false)
    private TrainingPositionField trainingPosition;
    @NotNull(message = "Das Beginndatum darf nicht null sein!")
    @Column(nullable = false)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate begin;
    @NotNull(message = "Das Enddatum darf nicht leer sein!")
    @Column(nullable = false)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate end;
    @NotNull
    @Column(nullable = false)
    private double fullTimeEquivalent;
    @NotNull
    @Column(nullable = false)
    private boolean funded;
    @NotNull
    @Column(nullable = false)
    private boolean approved;
    @ManyToOne(optional = false)
    private RotationPlan rotationPlan;
    @ManyToOne(optional = true)
    private TrainingSchemeSegment assignedSegment;

    public RotationPlanSegment() {
	this(null, null, null, null, 1.0, false, false);
    }

    public RotationPlanSegment(RotationPlan rotationPlan, double fullTimeEquivalent) {
	this(rotationPlan, null, null, null, fullTimeEquivalent, false, false);
    }

    public RotationPlanSegment(RotationPlan rotationPlan, TrainingPositionField trainingPosition, LocalDate begin, LocalDate end, double fullTimeEquivalent, boolean funded, boolean approved) {
	setRotationPlan(rotationPlan);
	setTrainingPosition(trainingPosition);
	setBegin(begin);
	setEnd(end);
	setFullTimeEquivalent(fullTimeEquivalent);
	setFunded(funded);
	setApproved(approved);

	setName(rotationPlan + ": " + trainingPosition);
    }

    public TrainingSchemeSegment getAssignedSegment() {
	return assignedSegment;
    }

    public LocalDate getBegin() {
	return begin;
    }

    @Transient
    @JsonIgnore
    public int getDuration() {
	if (null == begin || null == end) {
	    return 0;
	} else {
	    Period period = Period.between(begin, end.plusDays(1));

	    return period.getYears() * 12 + period.getMonths();
	}
    }

    public LocalDate getEnd() {
	return end;
    }

    public double getFullTimeEquivalent() {
	return fullTimeEquivalent;
    }

    /**
     * Gibt diejenigen Abschnitte einer Weiterbildungsordnung zurück für die dieser
     * Abschnitt förderbar ist.
     * 
     * @param trainingScheme
     * @return
     */
    @Transient
    @JsonIgnore
    public List<TrainingSchemeSegment> getFundableTrainingSchemeSegments(TrainingScheme trainingScheme) {
	List<TrainingSchemeSegment> trainingSchemeSegments = new ArrayList<>();

	for (TrainingSchemeSegment trainingSchemeSegment : trainingScheme.getTrainingSchemeSegments()) {
	    TreatmentType treatmentType;
	    if (!trainingSchemeSegment.isInpatient()) {
		treatmentType = TreatmentType.Outpatient;
	    } else if (!trainingSchemeSegment.isOutpatient()) {
		treatmentType = TreatmentType.Inpatient;
	    } else {
		treatmentType = null;
	    }

	    if (isFundableFor(trainingSchemeSegment.getMedicalField(), treatmentType)) {
		trainingSchemeSegments.add(trainingSchemeSegment);
	    }
	}

	return trainingSchemeSegments;
    }

    @Transient
    @JsonIgnore
    public FundableType getFundableType() {
	return getFundableType(getBegin(), getAssignedSegment());
    }

    /**
     * Ermittelt, inwiefern der aktuelle Weiterbildungdabschnitt für den gegebenen
     * Abschnitt der Weiterbildungsordnung förderfähig ist.
     * 
     * @param trainingSchemeSegment
     * @return
     */
    @Transient
    @JsonIgnore
    public FundableType getFundableType(LocalDate date, TrainingSchemeSegment trainingSchemeSegment) {
	if (null == trainingSchemeSegment) {
	    return FundableType.Unknown;
	}

	TreatmentType treatmentType;
	if (!trainingSchemeSegment.isInpatient()) {
	    treatmentType = TreatmentType.Outpatient;
	} else if (!trainingSchemeSegment.isOutpatient()) {
	    treatmentType = TreatmentType.Inpatient;
	} else {
	    treatmentType = null;
	}

	if (!isFundableFor(trainingSchemeSegment.getMedicalField(), treatmentType)) {
	    return FundableType.NotFundable;
	} else {
	    // prüfen, ob bereits andere zum angegebenen Zeitpunkt abgeschlossene
	    // Weiterbildungsabschnitte für das
	    // entsprechende Ordnungssegment verplant wurden
	    int fundedDuration = trainingSchemeSegment.getDuration();
	    int plannedDuration = 0;
	    for (RotationPlanSegment rotationPlanSegment : rotationPlan.getFinishedSegments(date, trainingSchemeSegment)) {
		plannedDuration += rotationPlanSegment.getNormalisedDuration();
	    }

	    // falls bereits die maximale Anzahl an Monaten gefördert wurde, ist die
	    // aktuelle Station nicht mehr förderfähig
	    if (plannedDuration >= fundedDuration) {
		return FundableType.NotFundable;
	    }

	    // falls mit der geplanten Anzahl an Monaten die Förderhöchstdauer überschritten
	    // wird, kann die Station nur teilweise gefördert werden
	    plannedDuration += getNormalisedDuration();
	    if (plannedDuration > fundedDuration) {
		System.out.println(this + " ist teilweise förderbar: Zeitraum ist " + (plannedDuration - fundedDuration) + " zu lang");
		return FundableType.PartiallyFundable;
	    }

	    return FundableType.Fundable;
	}
    }

    @Transient
    @JsonIgnore
    public int getNormalisedDuration() {
	return (int) (getDuration() * fullTimeEquivalent);
    }

    public RotationPlan getRotationPlan() {
	return rotationPlan;
    }

    public TrainingPositionField getTrainingPosition() {
	return trainingPosition;
    }

    public boolean isApproved() {
	return approved;
    }

    /**
     * Prüft ob der aktuele Weiterbildungsabschnitt für das gegebene medizinische
     * Feld und den gegebenen Behandlungstypen förderfähig ist.
     * 
     * @param medicalField
     * @param treatmentType
     * @return
     */
    @Transient
    @JsonIgnore
    public boolean isFundableFor(MedicalField medicalField, TreatmentType treatmentType) {

	MedicalField thisField = trainingPosition.getAuthorisationField().getTrainingSite().getMedicalField();
	TreatmentType thisTreatmentType = trainingPosition.getAuthorisationField().getTrainingSite().getTreatmentType();

	if (null == treatmentType || thisTreatmentType.equals(treatmentType)) {
	    if (thisField.equals(medicalField) || ChildRelationUtil.isChildOf(thisField, medicalField)) {
		return true;
	    }
	}

	return false;
    }

    public boolean isFunded() {
	return funded;
    }

    public void setApproved(boolean approved) {
	this.approved = approved;
    }

    public void setAssignedSegment(TrainingSchemeSegment assignedSegment) {
	this.assignedSegment = assignedSegment;
    }

    public void setBegin(LocalDate begin) {
	this.begin = begin;
    }

    public void setEnd(LocalDate end) {
	this.end = end;
    }

    public void setFullTimeEquivalent(double fullTimeEquivalent) {
	this.fullTimeEquivalent = fullTimeEquivalent;
    }

    public void setFunded(boolean funded) {
	this.funded = funded;
    }

    public void setRotationPlan(RotationPlan rotationPlan) {
	this.rotationPlan = rotationPlan;
    }

    public void setTrainingPosition(TrainingPositionField trainingPosition) {
	this.trainingPosition = trainingPosition;
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append(trainingPosition).append(": ").append(begin).append(" - ").append(end).append(" (VZÄ: ").append(fullTimeEquivalent).append(")");

	return builder.toString();
    }
}
