package org.infai.ddls.medicaltraining.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.infai.ddls.medicaltraining.security.model.Role;
import org.infai.ddls.medicaltraining.security.model.User;
import org.infai.ddls.medicaltraining.security.repo.RoleRepository;
import org.infai.ddls.medicaltraining.security.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class ApplicationStartup {
    private Log logger = LogFactory.getLog(getClass());

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @EventListener(ContextRefreshedEvent.class)
    public void contextRefreshedEvent() {
	logger.info("Starting MedicalTrainingApp...");

	initialiseRolesAndUsers();

    }

    private void initialiseRolesAndUsers() {
	logger.info("clearing roles and users");
	userRepository.deleteAll();
	roleRepository.deleteAll();

	logger.info("initialising roles...");
	Role admin = new Role();
	admin.setName("ROLE_ADMIN");
	Role manager = new Role();
	manager.setName("ROLE_MANAGER");
	Role kvsa = new Role();
	kvsa.setName("ROLE_KVSA");
	Role user = new Role();
	user.setName("ROLE_USER");

	admin = roleRepository.save(admin);
	manager = roleRepository.save(manager);
	kvsa = roleRepository.save(kvsa);
	user = roleRepository.save(user);

	logger.info("initialising users...");
	User adminUser = new User();
	adminUser.setUsername("admin");
	adminUser.setPassword(passwordEncoder.encode("admin"));
	adminUser.setActive(true);
	adminUser.getRoles().add(admin);

	User managerUser = new User();
	managerUser.setUsername("manager");
	managerUser.setPassword(passwordEncoder.encode("manager"));
	managerUser.setActive(true);
	managerUser.getRoles().add(manager);

	User kvsaUser = new User();
	kvsaUser.setUsername("kvsa");
	kvsaUser.setPassword(passwordEncoder.encode("kvsa"));
	kvsaUser.setActive(true);
	kvsaUser.getRoles().add(kvsa);

	User userUser = new User();
	userUser.setUsername("user");
	userUser.setPassword(passwordEncoder.encode("user"));
	userUser.setActive(true);
	userUser.getRoles().add(user);

	userRepository.save(adminUser);
	userRepository.save(managerUser);
	userRepository.save(kvsaUser);
	userRepository.save(userUser);
    }
}
