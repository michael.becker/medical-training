package org.infai.ddls.medicaltraining.repo;

import org.infai.ddls.medicaltraining.model.TrainingSchemeSegment;
import org.springframework.stereotype.Repository;

@Repository
interface TrainingSchemeSegmentRepository extends TrainingAppEntityRepository<TrainingSchemeSegment> {

}
