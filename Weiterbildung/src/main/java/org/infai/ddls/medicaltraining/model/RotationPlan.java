package org.infai.ddls.medicaltraining.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Der Rotationsplan ist ein für eine_n bestimmte_n Ärztin/Arzt in Weiterbildung
 * angepasster Weiterbildungsplan.
 * 
 * @author Michael Becker
 */
@Entity
@Audited
public class RotationPlan extends TrainingAppEntity {
    /**
     * die/der ÄiW/AiW, für die/den dieser Rotationsplan gültig ist
     */
    @ManyToOne(optional = false)
    private Registrar registrar;
    /**
     * die Weiterbildungsvorgabe laut Weiterbildungsordnung, für die dieser
     * Rotationsplan erstellt wurde
     */
    @ManyToOne(optional = false)
    private TrainingScheme trainingScheme;
    /**
     * die Weiterbildungsabschnitte des Rotationsplans
     */
    @OneToMany(mappedBy = "rotationPlan")
    @OrderBy("begin ASC")
    private List<RotationPlanSegment> rotationPlanSegments;

    public RotationPlan() {
	this.rotationPlanSegments = new ArrayList<>();
    }

    public RotationPlan(List<RotationPlanSegment> rotationPlanSegments) {
	for (RotationPlanSegment rotationPlanSegment : rotationPlanSegments) {
	    rotationPlanSegment.setRotationPlan(this);
	}
	this.rotationPlanSegments = rotationPlanSegments;
    }

    @Transient
    @JsonIgnore
    public List<RotationPlanSegment> getFinishedSegments(LocalDate date) {
	return getFinishedSegments(date, null);
    }

    /**
     * Gibt diejenigen Weiterbildungsabschnitte zurück, die für den gegebenen
     * Abschnitt der Weiterbildungsordnung bereits verplant sind.
     * 
     * @param date
     * @param trainingSchemeSegment
     * @return
     */
    @Transient
    @JsonIgnore
    public List<RotationPlanSegment> getFinishedSegments(LocalDate date, TrainingSchemeSegment trainingSchemeSegment) {
	List<RotationPlanSegment> finishedSegments = new ArrayList<>(rotationPlanSegments.size());

	for (RotationPlanSegment segment : rotationPlanSegments) {
	    if (segment.getEnd().isBefore(date)) {
		if (null == trainingSchemeSegment || (null != segment.getAssignedSegment() && segment.getAssignedSegment().equals(trainingSchemeSegment))) {
		    finishedSegments.add(segment);
		}
	    }
	}

	return finishedSegments;
    }

    /**
     * Gibt diejenigen {@link #rotationPlanSegments} zurück, welche zum angegebenen
     * Datum noch in Ableistung bzw. für die Zukunft geplant sind. Dies entspricht
     * denjenigen Segmenten, deren Enddatum größer oder gleich ist als das
     * angegebene Datum.
     * 
     * @param date
     * @return
     */
    @Transient
    @JsonIgnore
    public List<RotationPlanSegment> getPlannedSegments(LocalDate date) {
	return getPlannedSegments(date, null);
    }

    /**
     * Gibt diejenigen Weiterbildungsabschnitte zurück, die für den gegebenen
     * Abschnitt der Weiterbildungsordnung noch geplant sind.
     * 
     * @param date
     * @param trainingSchemeSegment
     * @return
     */
    public List<RotationPlanSegment> getPlannedSegments(LocalDate date, TrainingSchemeSegment trainingSchemeSegment) {
	List<RotationPlanSegment> plannedSegments = new ArrayList<>(rotationPlanSegments.size());

	for (RotationPlanSegment segment : rotationPlanSegments) {
	    if (segment.getEnd().isAfter(date) || segment.getEnd().equals(date)) {
		if (null == trainingSchemeSegment || (null != segment.getAssignedSegment() && segment.getAssignedSegment().equals(trainingSchemeSegment))) {
		    plannedSegments.add(segment);
		}
	    }
	}

	return plannedSegments;
    }

    public Registrar getRegistrar() {
	return registrar;
    }

    public List<RotationPlanSegment> getRotationPlanSegments() {
	return rotationPlanSegments;
    }

    public TrainingScheme getTrainingScheme() {
	return trainingScheme;
    }

    @Transient
    @JsonIgnore
    public List<RotationPlanSegment> getUnassignedSegments() {
	List<RotationPlanSegment> unassignedSegments = new ArrayList<>();

	for (RotationPlanSegment rotationPlanSegment : rotationPlanSegments) {
	    if (null == rotationPlanSegment.getAssignedSegment()) {
		unassignedSegments.add(rotationPlanSegment);
	    }
	}

	return unassignedSegments;
    }

    public void setRegistrar(Registrar registrar) {
	this.registrar = registrar;
	setName(registrar + "-RotPlan");
    }

    public void setRotationPlanSegments(List<RotationPlanSegment> rotationPlanSegments) {
	this.rotationPlanSegments = rotationPlanSegments;
    }

    public void setTrainingScheme(TrainingScheme trainingScheme) {
	this.trainingScheme = trainingScheme;
    }
}
