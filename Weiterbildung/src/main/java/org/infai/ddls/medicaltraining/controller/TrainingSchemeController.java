package org.infai.ddls.medicaltraining.controller;

import java.util.List;

import javax.servlet.ServletRequest;

import org.infai.ddls.medicaltraining.model.TrainingScheme;
import org.infai.ddls.medicaltraining.model.TrainingSchemeSegment;
import org.infai.ddls.medicaltraining.repo.TrainingDAO;
import org.infai.ddls.medicaltraining.repo.TrainingSchemeDAO;
import org.infai.ddls.medicaltraining.spec.TrainingAppEntityFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class TrainingSchemeController extends AbstractMedicalTrainingController {
    public static final String VIEW_TRAININGSCHEMES_LIST = "trainingschemes/listTrainingSchemes";
    public static final String VIEW_TRAININGSCHEMES_EDIT = "trainingschemes/editTrainingScheme";

    @Autowired
    private TrainingSchemeDAO dao;
    @Autowired
    private TrainingDAO trainingDAO;

    @GetMapping(RequestMappings.TRAININGSCHEMES_ARCHIVE)
    public String archiveTrainingScheme(@PathVariable long uid, Model model) {
	TrainingScheme trainingScheme = dao.getTrainingScheme(uid, true);
	trainingScheme.setArchived(true);
	for (TrainingSchemeSegment trainingSchemeSegment : trainingScheme.getTrainingSchemeSegments()) {
	    trainingSchemeSegment.setArchived(true);
	}

	dao.save(trainingScheme);

	return redirect(RequestMappings.TRAININGSCHEMES_LIST);
    }

    @GetMapping(RequestMappings.TRAININGSCHEMES_CREATE)
    public String createTrainingScheme(Model model) {
	return editTrainingScheme(new TrainingScheme(), model);
    }

    @GetMapping(RequestMappings.TRAININGSCHEMESEGMENTS_DELETE)
    public String deleteTrainingSchemeSegment(@PathVariable long uid, Model model) {
	TrainingSchemeSegment trainingSchemeSegment = dao.getTrainingSchemeSegment(uid);
	Long trainingSchemeUid = trainingSchemeSegment.getTrainingScheme().getUid();

	dao.deleteTrainingSchemeSegment(trainingSchemeSegment);

	return editTrainingScheme(trainingSchemeUid, model);
    }

    @GetMapping(RequestMappings.TRAININGSCHEMES_EDIT)
    public String editTrainingScheme(@PathVariable long uid, Model model) {
	return editTrainingScheme(dao.getTrainingScheme(uid, true), model);
    }

    @PostMapping(RequestMappings.TRAININGSCHEMES_FILTER)
    public String filterTrainingScheme(@ModelAttribute @Validated TrainingAppEntityFilter filter, Pageable pageable, Model model) {
	return listTrainingSchemes(dao.getTrainingSchemes(filter, pageable, true), filter, model);
    }

    @GetMapping(RequestMappings.TRAININGSCHEMES_LIST)
    public String listTrainingSchemes(Model model, Pageable pageable) {
	return listTrainingSchemes(dao.getTrainingSchemes(new TrainingAppEntityFilter(), pageable, true), new TrainingAppEntityFilter(), model);
    }

    @PostMapping(RequestMappings.TRAININGSCHEMES_UPDATE)
    public String updateTrainingScheme(@ModelAttribute(MODELATTRIBUTE_TRAININGSCHEME) @Validated TrainingScheme trainingScheme, BindingResult bindingResult, Model model, ServletRequest servletRequest) {
	System.out.println(formatHTTPParameters(servletRequest));
	if (logger.isDebugEnabled()) {
	    logger.debug(formatHTTPParameters(servletRequest));
	}

	if (bindingResult.hasErrors()) {
	    logger.info(bindingResult.getAllErrors());
	    return editTrainingScheme(trainingScheme, model);
	}

	dao.save(trainingScheme);

	return redirect(RequestMappings.TRAININGSCHEMES_LIST);
    }

    private String editTrainingScheme(TrainingScheme trainingScheme, Model model) {
	model.addAttribute(MODELATTRIBUTE_TRAININGSCHEME, trainingScheme);
	model.addAttribute(MODELATTRIBUTE_MEDICALFIELDS, trainingDAO.getMedicalFields());

	return VIEW_TRAININGSCHEMES_EDIT;
    }

    private String listTrainingSchemes(List<TrainingScheme> trainingSchemes, TrainingAppEntityFilter filter, Model model) {
	model.addAttribute(MODELATTRIBUTE_TRAININGSCHEMES, trainingSchemes);
	model.addAttribute(MODELATTRIBUTE_FILTER, filter);

	return VIEW_TRAININGSCHEMES_LIST;
    }

}