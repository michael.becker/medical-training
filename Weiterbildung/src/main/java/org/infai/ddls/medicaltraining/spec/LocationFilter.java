package org.infai.ddls.medicaltraining.spec;

import org.infai.ddls.medicaltraining.model.Location;

public abstract class LocationFilter extends TrainingAppEntityFilter {
    public static final String DEFAULT_ZIP_CODE = "99999";

    protected String zipCode;
    protected int distance;
    protected double latitude;
    protected double longitude;

    public LocationFilter() {
	this.zipCode = DEFAULT_ZIP_CODE;
	this.distance = 50;
    }

    public int getDistance() {
	return distance;
    }

    public double getLatitude() {
	return latitude;
    }

    public double getLongitude() {
	return longitude;
    }

    public String getZipCode() {
	return zipCode;
    }

    public void setDistance(int distance) {
	this.distance = distance;
    }

    /**
     * @deprecated use setZipCode -> LatLng wird automatisch berechnet
     */
    @Deprecated
    public void setLatitude(double latitude) {
	this.latitude = latitude;
    }

    /**
     * @deprecated use setZipCode -> LatLng wird automatisch berechnet
     */
    @Deprecated
    public void setLongitude(double longitude) {
	this.longitude = longitude;
    }

    public void setZipCode(String zipCode) throws Exception {
	this.zipCode = zipCode;

	double[] latlng = Location.getCoordinates(zipCode);
	this.latitude = latlng[0];
	this.longitude = latlng[1];
    }
}
