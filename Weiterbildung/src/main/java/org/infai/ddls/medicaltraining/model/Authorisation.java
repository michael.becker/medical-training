package org.infai.ddls.medicaltraining.model;

import java.beans.Transient;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltraining.json.View;
import org.infai.ddls.medicaltraining.model.constraints.AuthorisationBeginBeforeEndConstraint;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

/**
 * Stellt eine Weiterbildungsbefugnis dar. Jede Weiterbildungsbefugnis gilt für
 * einen bestimmten Zeitraum ({@link #grantedOn}, {@link #grantedTo}) und ist an
 * einen Weiterbildungsbefugten ({@link #authorisedDoctor}), der einer
 * bestimmten Weiterbildungsstätte ({@link #trainingSite}) zugeordnet ist,
 * gebunden.
 * 
 * @author Michael Becker
 *
 */
@AuthorisationBeginBeforeEndConstraint
@MappedSuperclass
@Audited
public abstract class Authorisation extends TrainingAppEntity {
    /**
     * der Weiterbildungsbefugte, für den die Befugnis gilt
     */
    @NotNull
    @ManyToOne(optional = false)
    @JsonView(View.Summary.class)
    protected AuthorisedDoctor authorisedDoctor;
    /**
     * die Weiterbildungsstätte, an der die Befugnis gilt
     */
    @NotNull
    @ManyToOne(optional = false)
    @JsonView(View.Summary.class)
    protected TrainingSite trainingSite;
    /**
     * der Zeitpunkt, ab dem die Weiterbildungsbefugnis gilt
     */
    @NotNull
    @Column(nullable = false)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonView(View.Summary.class)
    protected LocalDate grantedOn;
    /**
     * der Zeitpunkt, bis zu dem die Weiterbildungsbefugnis gilt
     */
    @NotNull
    @Column(nullable = false)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonView(View.Summary.class)
    protected LocalDate grantedTo;
    /**
     * die Weiterbildungsordnung, für die die Weiterbildungsbefugnis gilt
     */
    @NotNull
    @ManyToOne(optional = false)
    protected TrainingScheme trainingScheme;
    /**
     * maximale Anzahl an Monaten, die im Rahmen einer Weiterbildungsplans mit
     * dieser Befugnis abgedeckt werden können.
     */
    @NotNull
    @Min(value = 1)
    @Column(nullable = false)
    protected int maximalDuration;

    public AuthorisedDoctor getAuthorisedDoctor() {
	return authorisedDoctor;
    }

    @Transient
    public String getDisplayName() {
	return authorisedDoctor.getFullName() + " (" + grantedOn + " - " + grantedTo + ")";
    }

    public LocalDate getGrantedOn() {
	return grantedOn;
    }

    public LocalDate getGrantedTo() {
	return grantedTo;
    }

    public int getMaximalDuration() {
	return maximalDuration;
    }

    public TrainingScheme getTrainingScheme() {
	return trainingScheme;
    }

    public TrainingSite getTrainingSite() {
	return trainingSite;
    }

    public void setAuthorisedDoctor(AuthorisedDoctor authorisedDoctor) {
	this.authorisedDoctor = authorisedDoctor;
    }

    public void setGrantedOn(LocalDate grantedOn) {
	this.grantedOn = grantedOn;
    }

    public void setGrantedTo(LocalDate grantedTo) {
	this.grantedTo = grantedTo;
    }

    public void setMaximalDuration(int maximalDuration) {
	this.maximalDuration = maximalDuration;
    }

    public void setTrainingScheme(TrainingScheme trainingScheme) {
	this.trainingScheme = trainingScheme;
    }

    public void setTrainingSite(TrainingSite trainingSite) {
	this.trainingSite = trainingSite;
    }

}
