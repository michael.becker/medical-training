package org.infai.ddls.medicaltraining.repo;

import javax.validation.Validator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

public abstract class AbstractTrainingAppDAO {
    protected static final Sort DEFAULT_SORT = Sort.by(Direction.ASC, "archived", "name");
    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    protected Validator validator;

    protected Sort evaluateSort(Pageable pageable) {
	if (null == pageable) {
	    return DEFAULT_SORT;
	} else {
	    return evaluateSort(pageable.getSort());
	}
    }

    protected Sort evaluateSort(Sort sort) {
	if (null == sort || sort.isUnsorted()) {
	    sort = DEFAULT_SORT;
	}

	return sort;
    }
}
