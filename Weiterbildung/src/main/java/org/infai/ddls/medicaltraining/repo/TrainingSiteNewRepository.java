package org.infai.ddls.medicaltraining.repo;

import org.infai.ddls.medicaltraining.model.TrainingSite;

public interface TrainingSiteNewRepository extends TrainingAppEntityRepository<TrainingSite> {

}
