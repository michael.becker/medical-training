package org.infai.ddls.medicaltraining.model;

import java.util.List;

/**
 * Provides methods for hierarchically organised objects.
 * 
 * @author Michael Becker
 *
 * @param <T>
 *            the specific class, must be a subtype of {@link TrainingAppEntity}
 */
public interface IHasChildren<T extends TrainingAppEntity> {
    /**
     * Returns all direct children of this object,
     * 
     * @return all direct children of this object
     */
    public List<T> getChildren();

    /**
     * Returns the parent of this object.
     * 
     * @return the parent of this object
     */
    public T getParent();

    /**
     * Sets the direct children of this object.
     * 
     * @param children
     *            the children of this object
     */
    public void setChildren(List<T> children);

    /**
     * Sets the parent of this object.
     * 
     * @param parent
     *            the parent of this object
     */
    public void setParent(T parent);
}
