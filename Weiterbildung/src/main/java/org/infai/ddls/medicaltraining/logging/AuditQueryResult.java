package org.infai.ddls.medicaltraining.logging;

import javax.persistence.Transient;

import org.hibernate.envers.RevisionType;
import org.infai.ddls.medicaltraining.model.TrainingAppEntity;

public class AuditQueryResult<T extends TrainingAppEntity> {
    private T revisionEntity;
    private UserRevision userRevision;
    private RevisionType revisionType;

    @SuppressWarnings("unchecked")
    public AuditQueryResult(Object[] queryResult) {
	this.revisionEntity = (T) queryResult[0];
	this.userRevision = (UserRevision) queryResult[1];
	this.revisionType = (RevisionType) queryResult[2];
    }

    public T getRevisionEntity() {
	return revisionEntity;
    }

    public RevisionType getRevisionType() {
	return revisionType;
    }

    @Transient
    public String getRevisionTypeText() {
	switch (revisionType) {
	case ADD:
	    return "Hinzugefügt";
	case DEL:
	    return "Gelöscht";
	case MOD:
	    return "Modifiziert";
	default:
	    return "UNBEKANNT";
	}
    }

    public UserRevision getUserRevision() {
	return userRevision;
    }

    public void setRevisionEntity(T revisionEntity) {
	this.revisionEntity = revisionEntity;
    }

    public void setRevisionType(RevisionType revisionType) {
	this.revisionType = revisionType;
    }

    public void setUserRevision(UserRevision userRevision) {
	this.userRevision = userRevision;
    }
}