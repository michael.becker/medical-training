package org.infai.ddls.medicaltraining.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

@Component
public class Utilities {
    @Autowired
    private ResourceLoader resourceLoader;

    public String getCity(String zipCode) throws IOException {
	List<String> lines = Files.readAllLines(getPath("PLZ.tab"));

	for (String line : lines) {
	    String[] values = line.split("\t");

	    if (values[1].equals(zipCode)) {
		return values[4];
	    }
	}

	return null;
    }

    public Path getPath(String filename) throws IOException {
	return resourceLoader.getResource("classpath:" + filename).getFile().toPath();
    }
}
