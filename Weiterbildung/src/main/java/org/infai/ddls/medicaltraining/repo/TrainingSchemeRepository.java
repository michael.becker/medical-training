package org.infai.ddls.medicaltraining.repo;

import org.infai.ddls.medicaltraining.model.TrainingScheme;
import org.springframework.stereotype.Repository;

@Repository
interface TrainingSchemeRepository extends TrainingAppEntityRepository<TrainingScheme> {
}
