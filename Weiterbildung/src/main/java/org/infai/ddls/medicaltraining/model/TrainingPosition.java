package org.infai.ddls.medicaltraining.model;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

@MappedSuperclass
@Audited
public abstract class TrainingPosition extends TrainingAppEntity {
    public static final int DEFAULT_MAXIMAL_OVERLAP_DURATION = 2;

    @NotNull
    @Column(nullable = false)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    protected LocalDate availableFrom;
    @NotNull
    @Column(nullable = false)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    protected LocalDate availableUntil;
    @Min(1)
    protected int capacity;
    protected boolean partTimeAvailable;
    @NotNull
    @OneToMany
    @Valid
    protected List<TrainingPositionExclusion> exclusions;
    @Transient
    private TrainingPositionAvailability availability;

    public TrainingPosition() {
	setExclusions(new ArrayList<>());
	setPartTimeAvailable(false);
	setAvailability(TrainingPositionAvailability.AVAILABLE);
    }

    public void addExclusion(LocalDate from, LocalDate until) {
	this.exclusions.add(new TrainingPositionExclusion(from, until));
    }

    @Transient
    @JsonIgnore
    public TrainingPositionAvailability getAvailability() {
	if (null == availability) {
	    return TrainingPositionAvailability.AVAILABLE;
	} else {
	    return availability;
	}
    }

    /**
     * Ruft {@link #getAvailability(LocalDate, LocalDate, List, int)} mit dem Wert
     * von {@link #DEFAULT_MAXIMAL_OVERLAP_DURATION} auf.
     * 
     * @param from
     *            Startdatum der Verfügbarkeitsprüfung
     * @param until
     *            Enddatum der Verfügbarkeitsprüfung
     * @param rotationPlanSegments
     *            Weiterbildungsabschnitte, die bereits verplant sind
     */
    @Transient
    @JsonIgnore
    public TrainingPositionAvailability getAvailability(LocalDate from, LocalDate until, List<RotationPlanSegment> rotationPlanSegments) {
	return getAvailability(from, until, rotationPlanSegments, DEFAULT_MAXIMAL_OVERLAP_DURATION);
    }

    /**
     * Ermittelt, ob diese Weiterbildungsstelle in einem bestimmten Zeitraum
     * verfügbar ist abhängig von den geplanten Weiterbildungsabschnitten.
     * 
     * @param from
     *            Startdatum der Verfügbarkeitsprüfung
     * @param until
     *            Enddatum der Verfügbarkeitsprüfung
     * @param rotationPlanSegments
     *            Weiterbildungsabschnitte, die bereits verplant sind
     * @param maximalOverlap
     *            maximale Anzahl an Monaten, die mit einem bestehenden
     *            Weiterbildungsabschnitt überlappt werden darf
     * @return
     *         <ul>
     *         <li>{@link TrainingPositionAvailability#AVAILABLE}, wenn die
     *         Weiterbildungsstelle im geplanten Zeitraum frei ist</li>
     *         <li>{@link TrainingPositionAvailability#BLOCKED}, wenn die
     *         Weiterbildungsstelle im geplanten Zeitraum geblockt ist</li>
     *         <li>{@link TrainingPositionAvailability#OVERLAPPING}, wenn die
     *         Weiterbildungsstelle im geplanten Zeitraum belegt ist aber sich die
     *         Belegung maximal um die gegebene Anzahl an Monaten überlappen</li>
     *         <li>{@link TrainingPositionAvailability#OCCUPIED}, wenn die
     *         Weiterbildungsstelle im geplanten Zeitraum belegt ist und sich die
     *         Belegung mehr als die gegebene Anzahl an Monaten überlappen</li>
     *         </ul>
     */
    @Transient
    @JsonIgnore
    public TrainingPositionAvailability getAvailability(LocalDate from, LocalDate until, List<RotationPlanSegment> rotationPlanSegments, int maximalOverlap) {
	System.out.println("prüfe Verfügbarkeit der Position " + getName());
	System.out.println("\tfrom: " + from);
	System.out.println("\tuntil: " + until);
	System.out.println("\tverplante Segmente: " + rotationPlanSegments);
	if (isBlocked(from, until)) {
	    return TrainingPositionAvailability.BLOCKED;
	}

	// die bereits existierenden Weiterbildungsabschnitte im angegebenen Zeitraum
	List<RotationPlanSegment> existingSegments = new ArrayList<>(rotationPlanSegments.size());
	for (RotationPlanSegment rotationPlanSegment : rotationPlanSegments) {
	    LocalDate segmentBegin = rotationPlanSegment.getBegin();
	    LocalDate segmentEnd = rotationPlanSegment.getEnd();

	    if ((from.isBefore(segmentBegin) || from.isEqual(segmentBegin)) && (until.isAfter(segmentBegin) || until.isEqual(segmentBegin))) {
		existingSegments.add(rotationPlanSegment);
	    } else if (from.isAfter(segmentBegin) && (from.isBefore(segmentEnd) || from.isEqual(segmentEnd))) {
		existingSegments.add(rotationPlanSegment);
	    }
	}

	// wenn die Anzahl existierender Weiterbildungsabschnitte kleiner der Kapazität
	// ist, ist alles in Ordnung und die Stelle ist noch verfügbar
	if (existingSegments.size() < capacity) {
	    return TrainingPositionAvailability.AVAILABLE;
	}

	for (RotationPlanSegment rotationPlanSegment : rotationPlanSegments) {
	    LocalDate segmentBegin = rotationPlanSegment.getBegin();
	    LocalDate segmentEnd = rotationPlanSegment.getEnd();

	    if ((from.isBefore(segmentBegin) || from.isEqual(segmentBegin)) && (until.isAfter(segmentBegin) || until.isEqual(segmentBegin))) {
		int overlappingMonths = Period.between(segmentBegin, until.plusDays(1)).getMonths();

		if (overlappingMonths <= maximalOverlap) {
		    return TrainingPositionAvailability.OVERLAPPING;
		} else {
		    return TrainingPositionAvailability.OCCUPIED;
		}
	    } else if (from.isAfter(segmentBegin) && (from.isBefore(segmentEnd) || from.isEqual(segmentEnd))) {
		int overlappingMonths = Period.between(from, segmentEnd.plusDays(1)).getMonths();

		if (overlappingMonths <= maximalOverlap) {
		    return TrainingPositionAvailability.OVERLAPPING;
		} else {
		    return TrainingPositionAvailability.OCCUPIED;
		}
	    }
	}

	return TrainingPositionAvailability.AVAILABLE;
    }

    public LocalDate getAvailableFrom() {
	return availableFrom;
    }

    public LocalDate getAvailableUntil() {
	return availableUntil;
    }

    public int getCapacity() {
	return capacity;
    }

    public List<TrainingPositionExclusion> getExclusions() {
	return exclusions;
    }

    /**
     * Prüft, ob die Weiterbildungsstelle im angegebenen Zeitraum blockiert ist,
     * d.h. ob die Weiterbildungsstelle angegeben hat, dass zu dieser Zeit keine
     * freien Stellen existieren.
     * 
     * @param from
     * @param until
     * @return
     */
    @Transient
    @JsonIgnore
    public boolean isBlocked(LocalDate from, LocalDate until) {
	if (0 == exclusions.size()) {
	    return false;
	} else {
	    for (TrainingPositionExclusion exclusion : exclusions) {
		if ((from.isBefore(exclusion.getExclusionFrom()) || from.isEqual(exclusion.getExclusionFrom())) && (until.isAfter(exclusion.getExclusionFrom()) || until.isEqual(exclusion.getExclusionFrom()))) {
		    return true;
		} else if (from.isAfter(exclusion.getExclusionFrom()) && (from.isBefore(exclusion.getExclusionUntil()) || from.isEqual(exclusion.getExclusionUntil()))) {
		    return true;
		}
	    }

	}
	return false;
    }

    public boolean isPartTimeAvailable() {
	return partTimeAvailable;
    }

    public void setAvailability(TrainingPositionAvailability availability) {
	this.availability = availability;
    }

    public void setAvailableFrom(LocalDate availableFrom) {
	this.availableFrom = availableFrom;
    }

    public void setAvailableUntil(LocalDate availableUntil) {
	this.availableUntil = availableUntil;
    }

    public void setCapacity(int capacity) {
	this.capacity = capacity;
    }

    public void setExclusions(List<TrainingPositionExclusion> exclusions) {
	this.exclusions = exclusions;
    }

    public void setPartTimeAvailable(boolean partTimeAvailable) {
	this.partTimeAvailable = partTimeAvailable;
    }
}