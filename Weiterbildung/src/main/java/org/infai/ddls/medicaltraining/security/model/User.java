package org.infai.ddls.medicaltraining.security.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long uid;
    @NotBlank
    @Column(nullable = false)
    private String username;
    @NotEmpty
    @Column(nullable = false)
    private String password;
    private boolean active;
    @NotNull
    @ManyToMany
    private Set<Role> roles;

    public User() {
	this.roles = new HashSet<>();
    }

    public String getPassword() {
	return password;
    }

    public Set<Role> getRoles() {
	return roles;
    }

    public Long getUid() {
	return uid;
    }

    public String getUsername() {
	return username;
    }

    public boolean isActive() {
	return active;
    }

    public void setActive(boolean active) {
	this.active = active;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public void setRoles(Set<Role> roles) {
	this.roles = roles;
    }

    public void setUid(Long uid) {
	this.uid = uid;
    }

    public void setUsername(String username) {
	this.username = username;
    }
}
