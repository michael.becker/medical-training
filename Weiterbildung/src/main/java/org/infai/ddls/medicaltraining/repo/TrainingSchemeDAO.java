package org.infai.ddls.medicaltraining.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.infai.ddls.medicaltraining.model.TrainingScheme;
import org.infai.ddls.medicaltraining.model.TrainingSchemeSegment;
import org.infai.ddls.medicaltraining.spec.TrainingAppEntityFilter;
import org.infai.ddls.medicaltraining.spec.TrainingSchemeSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;

@Component
public class TrainingSchemeDAO extends AbstractTrainingAppDAO {
    @Autowired
    private TrainingSchemeRepository trainingSchemeRepository;
    @Autowired
    private TrainingSchemeSegmentRepository trainingSchemeSegmentRepository;
    @Autowired
    private TrainingDAO trainingDAO;

    public void deleteTrainingSchemeSegment(TrainingSchemeSegment trainingSchemeSegment) {
	trainingSchemeSegmentRepository.delete(trainingSchemeSegment);
    }

    @Transactional
    public TrainingScheme getTrainingScheme(Long uid, boolean loadTrainingSegments) {
	TrainingScheme trainingScheme = trainingSchemeRepository.findById(uid).get();

	if (loadTrainingSegments) {
	    trainingScheme.getTrainingSchemeSegments().size();
	}

	return trainingScheme;
    }

    @Transactional
    public List<TrainingScheme> getTrainingSchemes() {
	return getTrainingSchemes(new TrainingAppEntityFilter(), null, false);
    }

    @Transactional
    public List<TrainingScheme> getTrainingSchemes(TrainingAppEntityFilter filter, Pageable pageable, boolean loadTrainingSegments) {
	TrainingSchemeSpecification spec = new TrainingSchemeSpecification(filter);
	Sort sort = evaluateSort(pageable);

	List<TrainingScheme> trainingSchemes = trainingSchemeRepository.findAll(spec, sort);

	if (loadTrainingSegments) {
	    for (TrainingScheme trainingScheme : trainingSchemes) {
		for (TrainingSchemeSegment trainingSchemeSegment : trainingScheme.getTrainingSchemeSegments()) {
		    trainingSchemeSegment.getTrainingScheme();
		}
	    }
	}

	return trainingSchemes;
    }

    public TrainingSchemeSegment getTrainingSchemeSegment(Long uid) {
	return trainingSchemeSegmentRepository.findById(uid).get();
    }

    public List<TrainingSchemeSegment> getTrainingSchemeSegments() {
	return Lists.newArrayList(trainingSchemeSegmentRepository.findAll());
    }

    @Transactional
    public TrainingScheme save(TrainingScheme trainingScheme) {
	TrainingScheme result = trainingSchemeRepository.save(trainingScheme);
	for (TrainingSchemeSegment trainingSchemeSegment : trainingScheme.getTrainingSchemeSegments()) {
	    trainingSchemeSegment.setTrainingScheme(trainingScheme);
	    save(trainingSchemeSegment);
	}

	return result;
    }

    @Transactional
    public TrainingSchemeSegment save(TrainingSchemeSegment trainingSchemeSegment) {
	trainingSchemeSegment.setMedicalField(trainingDAO.save(trainingSchemeSegment.getMedicalField()));

	return trainingSchemeSegmentRepository.save(trainingSchemeSegment);
    }
}
