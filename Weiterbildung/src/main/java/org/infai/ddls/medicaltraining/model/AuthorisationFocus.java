package org.infai.ddls.medicaltraining.model;

import javax.persistence.Entity;

import org.hibernate.envers.Audited;

/**
 * Stellt die Befugnis für die Weiterbildung in einem bestimmten medizinischen
 * Schwerpunkt dar.
 * 
 * @author Michael Becker
 *
 */
@Entity
@Audited
public class AuthorisationFocus extends Authorisation {

}
