package org.infai.ddls.medicaltraining.logging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.envers.RevisionListener;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class CustomRevisionListener implements RevisionListener {
    public static final String DEFAULT_USERNAME = "DEFAULT_USERNAME";

    protected final Log logger = LogFactory.getLog(getClass());

    @Override
    public void newRevision(Object revisionEntity) {
	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

	if (null == authentication || (authentication instanceof AnonymousAuthenticationToken)) {
	    ((UserRevision) revisionEntity).setUserName(DEFAULT_USERNAME);
	    logger.warn("unauthenticated modification of " + revisionEntity);

	    throw new SecurityException("unauthenticated modification of " + revisionEntity);
	} else {
	    ((UserRevision) revisionEntity).setUserName(authentication.getName());
	}
    }
}
