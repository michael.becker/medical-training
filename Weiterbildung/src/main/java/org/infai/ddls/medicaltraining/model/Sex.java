package org.infai.ddls.medicaltraining.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

import org.hibernate.envers.Audited;

@Entity
@Audited
public class Sex extends TrainingAppEntity {
    @NotBlank
    @Column(nullable = false)
    private String defaultPrefix;
    @NotBlank
    @Column(nullable = false)
    private String defaultSalutation;

    public Sex() {
    }

    public Sex(String name, String description, String defaultPrefix, String defaultSalutation) {
	setName(name);
	setDescription(description);
	setDefaultPrefix(defaultPrefix);
	setDefaultSalutation(defaultSalutation);
    }

    public String getDefaultPrefix() {
	return defaultPrefix;
    }

    public String getDefaultSalutation() {
	return defaultSalutation;
    }

    public void setDefaultPrefix(String defaultPrefix) {
	this.defaultPrefix = defaultPrefix;
    }

    public void setDefaultSalutation(String defaultSalutation) {
	this.defaultSalutation = defaultSalutation;
    }
}
