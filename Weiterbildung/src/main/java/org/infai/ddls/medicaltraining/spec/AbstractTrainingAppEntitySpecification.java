package org.infai.ddls.medicaltraining.spec;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltraining.model.TrainingAppEntity;

@SuppressWarnings("serial")
public abstract class AbstractTrainingAppEntitySpecification<T extends TrainingAppEntity, U extends TrainingAppEntityFilter> implements TrainingAppSpecification<T> {
    private List<Predicate> predicates;
    protected U filter;

    // public AbstractTrainingAppEntitySpecification() {
    // this(new TrainingAppEntityFilter());
    // }

    public AbstractTrainingAppEntitySpecification(U filter) {
	this.predicates = new ArrayList<>();
	this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	initPredicates(root, query, criteriaBuilder);

	if (null != filter && !filter.isAllowArchived()) {
	    // wir müssen hier beide Prädikate prüfen, da je nach DB-Implementierung boolean
	    // unterschiedlich abgebildet werden kann
	    Predicate isFalse = criteriaBuilder.isFalse(root.get("archived"));
	    Predicate isNull = criteriaBuilder.isNull(root.get("archived"));

	    addPredicateDisjunction(criteriaBuilder, isFalse, isNull);
	}

	if (null != filter && null != filter.getName()) {
	    addPredicate(likeIgnoreCase(filter.getName(), root.get("name"), criteriaBuilder));
	}

	return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }

    protected void addPredicate(Predicate predicate) {
	this.predicates.add(predicate);
    }

    protected void addPredicateConjunction(CriteriaBuilder criteriaBuilder, Predicate... predicates) {
	this.predicates.add(criteriaBuilder.and(predicates));
    }

    protected void addPredicateDisjunction(CriteriaBuilder criteriaBuilder, Predicate... predicates) {
	this.predicates.add(criteriaBuilder.or(predicates));
    }

    protected abstract void initPredicates(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder);

    protected Predicate likeIgnoreCase(String pattern, Path<String> path, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.like(criteriaBuilder.lower(path), "%" + pattern.toLowerCase() + "%");
    }
}
