package org.infai.ddls.medicaltraining.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltraining.json.View;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;

@MappedSuperclass
@Audited
@JsonInclude(Include.NON_EMPTY)
public abstract class TrainingAppEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @JsonView(View.Summary.class)
    private Long uid;
    @NotBlank
    @Column(nullable = false)
    @JsonView(View.Summary.class)
    private String name;
    @Lob
    @JsonView(View.Summary.class)
    private String description;
    @Lob
    private String notes;
    @ElementCollection
    @Lob
    private Map<String, String> additionalFields;
    private Boolean archived;

    public TrainingAppEntity() {
	this.setAdditionalFields(new HashMap<>());
	this.archived = false;
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof TrainingAppEntity && null != ((TrainingAppEntity) obj).uid && null != this.uid) {
	    return obj.getClass().equals(this.getClass()) && ((TrainingAppEntity) obj).uid.equals(this.uid);
	} else {
	    return super.equals(obj);
	}
    }

    public Map<String, String> getAdditionalFields() {
	return additionalFields;
    }

    public Boolean getArchived() {
	return archived;
    }

    public String getDescription() {
	return description;
    }

    public String getName() {
	return name;
    }

    public String getNotes() {
	return notes;
    }

    public Long getUid() {
	return uid;
    }

    public void setAdditionalFields(Map<String, String> additionalFields) {
	this.additionalFields = additionalFields;
    }

    public void setArchived(Boolean archived) {
	this.archived = archived;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setNotes(String notes) {
	this.notes = notes;
    }

    public void setUid(Long uid) {
	this.uid = uid;
    }

    @Override
    public String toString() {
	return name;
    }
}
