package org.infai.ddls.medicaltraining.controller;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;
import org.infai.ddls.medicaltraining.model.Registrar;
import org.infai.ddls.medicaltraining.model.RotationPlanSegment;
import org.infai.ddls.medicaltraining.model.TrainingPositionField;
import org.infai.ddls.medicaltraining.spec.AuthorisedDoctorFilter;
import org.infai.ddls.medicaltraining.spec.RegistrarFilter;
import org.infai.ddls.medicaltraining.spec.TrainingPositionFieldFilter;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class DoctorController extends AbstractMedicalTrainingController {
    public static final String VIEW_REGISTRARS_LIST = "doctors/listRegistrars";
    public static final String VIEW_REGISTRARS_EDIT = "doctors/editRegistrar";
    public static final String VIEW_AUTHORISED_DOCTORS_LIST = "doctors/listAuthorisedDoctors";
    public static final String VIEW_AUTHORISED_DOCTORS_EDIT = "doctors/editAuthorisedDoctor";

    public static final String editAuthorisation(Long authorisedDoctorUId, Long authorisationUId) {
	return "/doctors/authoriseddoctors/" + authorisedDoctorUId + "/authorisations/" + authorisationUId + "/edit";
    }

    public static final String editAuthorisedDoctor(Long authorisedDoctorUId) {
	return "/doctors/authoriseddoctors/" + authorisedDoctorUId + "/edit";
    }

    @GetMapping("doctors/authoriseddoctors/{authorisedDoctorUID}/authorisations/create")
    public String createAuthorisation(@PathVariable long authorisedDoctorUID, Model model) {
	AuthorisationField authorisationField = new AuthorisationField();
	authorisationField.setAuthorisedDoctor(personDAO.getAuthorisedDoctor(authorisedDoctorUID, true, true));

	return editAuthorisation(authorisationField, model);
    }

    @GetMapping(RequestMappings.AUTHORISED_DOCTORS_CREATE)
    public String createAuthorisedDoctor(Model model) {
	return editAuthorisedDoctor(new AuthorisedDoctor(), model);
    }

    @GetMapping(RequestMappings.REGISTRARS_CREATE)
    public String createRegistrar(Model model) {
	return editRegistrar(new Registrar(), model);
    }

    @PostMapping("doctors/authoriseddoctors/{uid}/delete")
    public String deleteAuthorisedDoctorPerform(@PathVariable("uid") long uid, Model model) {
	if (personDAO.delete(personDAO.getAuthorisedDoctor(uid, false, false), true, true, true)) {
	    return redirect(RequestMappings.AUTHORISED_DOCTORS_LIST);
	} else {
	    return null;
	}
    }

    @GetMapping("doctors/authoriseddoctors/{uid}/delete")
    public String deleteAuthorisedDoctorVerify(@PathVariable("uid") long uid, Model model) {
	AuthorisedDoctor authorisedDoctor = personDAO.getAuthorisedDoctor(uid, false, true);

	TrainingPositionFieldFilter trainingPositionFieldFilter = new TrainingPositionFieldFilter();
	trainingPositionFieldFilter.setAuthorisedDoctor(authorisedDoctor);
	List<TrainingPositionField> positions = trainingPositionDAO.getTrainingPositionsField(trainingPositionFieldFilter, null);

	Map<TrainingPositionField, List<RotationPlanSegment>> segments = new HashMap<>();
	for (TrainingPositionField position : positions) {
	    segments.put(position, rotationPlanDAO.getRotationPlanSegments(position, null));
	}

	model.addAttribute("authorisedDoctor", authorisedDoctor);
	model.addAttribute("trainingPositions", segments);

	return "doctors/deleteAuthorisedDoctor";
    }

    @GetMapping("doctors/authoriseddoctors/{authorisedDoctorUID}/authorisations/{authorisationUID}/edit")
    public String editAuthorisation(@PathVariable long authorisedDoctorUID, @PathVariable long authorisationUID, Model model) {
	return editAuthorisation(authorisationDAO.getAuthorisationField(authorisationUID), model);
    }

    @GetMapping(RequestMappings.AUTHORISED_DOCTORS_EDIT)
    public String editAuthorisedDoctor(@PathVariable long uid, Model model) {
	return editAuthorisedDoctor(personDAO.getAuthorisedDoctor(uid, true, true), model);
    }

    @GetMapping(RequestMappings.REGISTRARS_EDIT)
    public String editRegistrar(@PathVariable long uid, Model model) {
	return editRegistrar(personDAO.getRegistrar(uid), model);
    }

    @PostMapping("doctors/authoriseddoctors/filter")
    public String filterAuthorisedDoctors(@ModelAttribute("authorisedDoctorFilter") @Validated AuthorisedDoctorFilter authorisedDoctorFilter, BindingResult bindingResult, Model model, Pageable pageable, ServletRequest servletRequest) {
	return listAuthorisedDoctors(personDAO.getAuthorisedDoctors(authorisedDoctorFilter, pageable.getSort(), true), authorisedDoctorFilter, model);
    }

    @GetMapping(RequestMappings.AUTHORISED_DOCTORS_LIST)
    public String listAuthorisedDoctors(Model model, Pageable pageable) {
	return listAuthorisedDoctors(personDAO.getAuthorisedDoctors(pageable.getSort(), true), new AuthorisedDoctorFilter(), model);
    }

    @GetMapping(RequestMappings.REGISTRARS_LIST)
    public String listRegistrars(Model model, Pageable pageable) {
	return listRegistrars(personDAO.getRegistrars(pageable.getSort()), new RegistrarFilter(), model);
    }

    @GetMapping("doctors/registrars/{registrarUId}/archive")
    public String registrarArchive(@PathVariable long registrarUId, Model model) {
	Registrar registrar = personDAO.getRegistrar(registrarUId);
	registrar.setArchived(true);

	personDAO.save(registrar);

	return redirect(RequestMappings.REGISTRARS_LIST);
    }

    @PostMapping("doctors/registrars/filter")
    public String registrarFilter(@ModelAttribute("registrarFilter") @Validated RegistrarFilter registrarFilter, BindingResult bindingResult, Model model, Pageable pageable, ServletRequest servletRequest) {
	return listRegistrars(personDAO.getRegistrars(registrarFilter, pageable.getSort()), registrarFilter, model);
    }

    @PostMapping("doctors/authoriseddoctors/{authorisedDoctorUID}/authorisations/edit")
    public String updateAuthorisation(@PathVariable long authorisedDoctorUID, @ModelAttribute("authorisation") @Validated AuthorisationField authorisationField, BindingResult bindingResult, Model model, ServletRequest servletRequest) {
	System.out.println(formatHTTPParameters(servletRequest));
	if (bindingResult.hasErrors()) {
	    logger.warn(bindingResult.getAllErrors());

	    System.out.println(authorisationField.getAuthorisedDoctor());
	    authorisationField.setAuthorisedDoctor(personDAO.getAuthorisedDoctor(authorisedDoctorUID, false, false));
	    System.out.println(authorisationField.getAuthorisedDoctor());

	    return editAuthorisation(authorisationField, model);
	}

	System.out.println("speichere authorisation");
	System.out.println("\t" + authorisationField.getUid());
	System.out.println("\t" + authorisationField.getAuthorisedDoctor());

	authorisationField = authorisationDAO.save(authorisationField);

	System.out.println("authorisation gespeichert");
	System.out.println("\t" + authorisationField.getUid());
	System.out.println("\t" + authorisationField.getAuthorisedDoctor());

	if (isSave(servletRequest)) {
	    return redirect(editAuthorisation(authorisationField.getAuthorisedDoctor().getUid(), authorisationField.getUid()));
	} else if (isSaveAndClose(servletRequest)) {
	    return redirect(editAuthorisedDoctor(authorisationField.getAuthorisedDoctor().getUid()));
	} else {
	    throw new RuntimeException("Parameter saveAction not set or invalid");
	}
    }

    @PostMapping(RequestMappings.AUTHORISED_DOCTORS_UPDATE)
    public String updateAuthorisedDoctor(@ModelAttribute(MODELATTRIBUTE_AUTHORISED_DOCTOR) @Validated AuthorisedDoctor authorisedDoctor, BindingResult bindingResult, Model model, ServletRequest servletRequest) {
	if (personDAO.isLANRusedByOther(authorisedDoctor)) {
	    bindingResult.rejectValue("lanr", "lanr.inUse", "lanr.inUse");
	}

	if (bindingResult.hasErrors()) {
	    logger.info(bindingResult.getAllErrors());
	    return editAuthorisedDoctor(authorisedDoctor, model);
	}

	Map<String, String> additionalFields = evaluateAdditionalFields(servletRequest);
	authorisedDoctor.setAdditionalFields(additionalFields);

	personDAO.save(authorisedDoctor);

	if (isSave(servletRequest)) {
	    return editAuthorisedDoctor(authorisedDoctor, model);
	} else if (isSaveAndClose(servletRequest)) {
	    return redirect(RequestMappings.AUTHORISED_DOCTORS_LIST);
	} else {
	    throw new RuntimeException("Parameter saveAction not set or not valid!");
	}
    }

    @PostMapping(RequestMappings.REGISTRARS_UPDATE)
    public String updateRegistrar(@ModelAttribute(MODELATTRIBUTE_REGISTRAR) @Validated Registrar registrar, BindingResult bindingResult, Model model, ServletRequest servletRequest) {
	if (bindingResult.hasErrors()) {
	    logger.info(bindingResult.getAllErrors());
	    return editRegistrar(registrar, model);
	}

	Map<String, String> additionalFields = evaluateAdditionalFields(servletRequest);
	registrar.setAdditionalFields(additionalFields);

	personDAO.save(registrar);

	return redirect(RequestMappings.REGISTRARS_LIST);
    }

    private String editAuthorisation(AuthorisationField authorisationField, Model model) {
	model.addAttribute("authorisation", authorisationField);

	populateTrainingSchemes(model);
	populateTrainingSites(model, true, false);

	return "doctors/editAuthorisation";
    }

    private String editAuthorisedDoctor(AuthorisedDoctor authorisedDoctor, Model model) {
	model.addAttribute(MODELATTRIBUTE_AUTHORISED_DOCTOR, authorisedDoctor);
	model.addAttribute("authorisations", authorisationDAO.getAuthorisationsField(authorisedDoctor));
	model.addAttribute("revisions", revisionDAO.getAuthorisedDoctorRevisions(authorisedDoctor.getUid()));

	populateSexes(model);
	populateTitles(model);
	populateTrainingSites(model, true, false);

	return VIEW_AUTHORISED_DOCTORS_EDIT;
    }

    private String editRegistrar(Registrar registrar, Model model) {
	model.addAttribute(MODELATTRIBUTE_REGISTRAR, registrar);
	model.addAttribute("revisions", revisionDAO.getRegistrarRevisions(registrar.getUid(), 5));

	populateSexes(model);
	populateTitles(model);

	return VIEW_REGISTRARS_EDIT;
    }

    private String listAuthorisedDoctors(List<AuthorisedDoctor> authorisedDoctors, AuthorisedDoctorFilter authorisedDoctorFilter, Model model) {
	model.addAttribute(MODELATTRIBUTE_AUTHORISED_DOCTORS, authorisedDoctors);
	model.addAttribute("authorisedDoctorFilter", authorisedDoctorFilter);

	return VIEW_AUTHORISED_DOCTORS_LIST;
    }

    private String listRegistrars(List<Registrar> registrars, RegistrarFilter filter, Model model) {
	Map<Registrar, RotationPlanSegment> actualSegments = new HashMap<>();
	LocalDate referenceDate = LocalDate.now();
	for (Registrar registrar : registrars) {
	    actualSegments.put(registrar, rotationPlanDAO.getRotationPlanSegment(registrar, referenceDate));
	}

	model.addAttribute(MODELATTRIBUTE_REGISTRARS, registrars);
	model.addAttribute("actualSegments", actualSegments);
	model.addAttribute("registrarFilter", filter);

	return VIEW_REGISTRARS_LIST;
    }
}
