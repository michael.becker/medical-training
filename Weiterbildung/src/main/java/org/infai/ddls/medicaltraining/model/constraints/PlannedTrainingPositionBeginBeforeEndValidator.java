package org.infai.ddls.medicaltraining.model.constraints;

import java.time.LocalDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.infai.ddls.medicaltraining.spec.PlannedTrainingPositionFieldFilter;

public class PlannedTrainingPositionBeginBeforeEndValidator implements ConstraintValidator<PlannedTrainingPositionBeginBeforEndConstraint, PlannedTrainingPositionFieldFilter> {

    @Override
    public boolean isValid(PlannedTrainingPositionFieldFilter value, ConstraintValidatorContext context) {
	LocalDate begin = value.getAvailableFrom();
	LocalDate end = value.getAvailableUntil();

	if (null == begin || null == end) {
	    return true;
	}

	if (end.isBefore(begin)) {
	    return false;
	} else {
	    return true;
	}
    }

}
