package org.infai.ddls.medicaltraining.logging;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;
import org.infai.ddls.medicaltraining.model.MedicalField;
import org.infai.ddls.medicaltraining.model.Registrar;
import org.infai.ddls.medicaltraining.model.RotationPlan;
import org.infai.ddls.medicaltraining.model.RotationPlanSegment;
import org.infai.ddls.medicaltraining.model.TrainingAppEntity;
import org.infai.ddls.medicaltraining.model.TrainingScheme;
import org.infai.ddls.medicaltraining.model.TrainingSchemeSegment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RevisionDAO {
    @Autowired
    private EntityManager entityManager;

    @Transactional
    public List<AuditQueryResult<AuthorisedDoctor>> getAuthorisedDoctorRevisions(Long uid) {
	List<AuditQueryResult<AuthorisedDoctor>> revisions = getRevisions(AuthorisedDoctor.class, uid);

	for (AuditQueryResult<AuthorisedDoctor> revision : revisions) {
	    revision.getRevisionEntity().getAdditionalFields().size();

	    System.out.println(revision.getRevisionEntity().getSex());
	    System.out.println(revision.getRevisionEntity().getTitle());
	}

	return revisions;
    }

    @Transactional
    public List<AuditQueryResult<MedicalField>> getMedicalFieldRevisions(Long uid) {
	List<AuditQueryResult<MedicalField>> revisions = getRevisions(MedicalField.class, uid);

	for (AuditQueryResult<MedicalField> revision : revisions) {
	    revision.getRevisionEntity().getChildren().size();
	    revision.getRevisionEntity().getParent();

	    // TODO: Das muss irgendwie drin sein, damit er das in der JSP anzeigen kann ->
	    // eine schönere Lösung zur Initialisierung finden
	    System.out.println(revision.getRevisionEntity().getParent());

	    if (null != revision.getRevisionEntity().getParent()) {
		revision.getRevisionEntity().getParent().getUid();
	    }
	}

	return revisions;
    }

    @Transactional
    public List<AuditQueryResult<Registrar>> getRegistrarRevisions(Long uid) {
	return getRegistrarRevisions(uid, 0);
    }

    @Transactional
    public List<AuditQueryResult<Registrar>> getRegistrarRevisions(Long uid, int maxResults) {
	List<AuditQueryResult<Registrar>> revisions = getRevisions(Registrar.class, uid, maxResults);

	for (AuditQueryResult<Registrar> revision : revisions) {
	    revision.getRevisionEntity().getAdditionalFields().size();

	    System.out.println(revision.getRevisionEntity().getSex());
	    System.out.println(revision.getRevisionEntity().getTitle());
	}

	return revisions;
    }

    @Transactional
    public <T extends TrainingAppEntity> List<AuditQueryResult<T>> getRevisions(Class<T> type) {
	AuditReader auditReader = AuditReaderFactory.get(entityManager);

	@SuppressWarnings("unchecked")
	List<Object[]> revisions = auditReader.createQuery().forRevisionsOfEntity(type, false, true).addOrder(AuditEntity.revisionNumber().desc()).getResultList();
	List<AuditQueryResult<T>> auditQueryResults = new ArrayList<>(revisions.size());

	for (Object[] revision : revisions) {
	    auditQueryResults.add(new AuditQueryResult<>(revision));
	}

	return auditQueryResults;
    }

    @Transactional
    public <T extends TrainingAppEntity> List<AuditQueryResult<T>> getRevisions(Class<T> type, Long uid) {
	return getRevisions(type, uid, 0, 0);
    }

    @Transactional
    public <T extends TrainingAppEntity> List<AuditQueryResult<T>> getRevisions(Class<T> type, Long uid, int maxResults) {
	return getRevisions(type, uid, 0, maxResults);
    }

    @Transactional
    public <T extends TrainingAppEntity> List<AuditQueryResult<T>> getRevisions(Class<T> type, Long uid, int begin, int end) {
	AuditReader auditReader = AuditReaderFactory.get(entityManager);

	AuditQuery query = auditReader.createQuery().forRevisionsOfEntity(type, false, true).add(AuditEntity.id().eq(uid)).addOrder(AuditEntity.revisionNumber().desc());

	if (0 != begin) {
	    query.setFirstResult(begin);
	}
	if (0 != end) {
	    query.setMaxResults(end - begin);
	}

	@SuppressWarnings("unchecked")
	List<Object[]> revisions = query.getResultList();
	List<AuditQueryResult<T>> auditQueryResults = new ArrayList<>(revisions.size());

	for (Object[] revision : revisions) {
	    auditQueryResults.add(new AuditQueryResult<>(revision));
	}

	return auditQueryResults;
    }

    @Transactional
    public List<AuditQueryResult<RotationPlan>> getRotationPlanRevisions(Long uid, int maxResults) {
	List<AuditQueryResult<RotationPlan>> revisions = getRevisions(RotationPlan.class, uid, maxResults);

	for (AuditQueryResult<RotationPlan> revision : revisions) {
	    // TODO auch hier muss die Ausgabe erfolgen, damit nachher in der Session darauf
	    // zugegriffen werden kann...
	    System.out.println("rotationplansegments: " + revision.getRevisionEntity().getRotationPlanSegments());
	    for (RotationPlanSegment rotationPlanSegment : revision.getRevisionEntity().getRotationPlanSegments()) {
		System.out.println(rotationPlanSegment.getTrainingPosition().getAuthorisationField().getTrainingSite());
		System.out.println(rotationPlanSegment.getTrainingPosition().getAuthorisationField().getTrainingSite().getMedicalField());
		System.out.println(rotationPlanSegment.getTrainingPosition().getAuthorisationField().getTrainingSite().getTreatmentType());
		System.out.println(rotationPlanSegment.getTrainingPosition().getAuthorisationField().getAuthorisedDoctor());
	    }
	    revision.getRevisionEntity().getRotationPlanSegments().size();
	}

	return revisions;
    }

    @Transactional
    public List<AuditQueryResult<TrainingScheme>> getTrainingSchemeRevisions(Long uid) {
	List<AuditQueryResult<TrainingScheme>> revisions = getRevisions(TrainingScheme.class, uid);

	for (AuditQueryResult<TrainingScheme> revision : revisions) {
	    revision.getRevisionEntity().getTrainingSchemeSegments().size();
	    for (TrainingSchemeSegment segment : revision.getRevisionEntity().getTrainingSchemeSegments()) {
		segment.getMedicalField().getParent();
		segment.getTrainingScheme();
	    }
	}

	return revisions;
    }
}
