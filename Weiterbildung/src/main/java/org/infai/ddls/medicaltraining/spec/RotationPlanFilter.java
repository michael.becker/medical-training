package org.infai.ddls.medicaltraining.spec;

import java.time.LocalDate;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.model.Registrar;
import org.infai.ddls.medicaltraining.model.RotationPlan;
import org.infai.ddls.medicaltraining.model.TrainingPositionField;

public class RotationPlanFilter extends TrainingAppEntityFilter {
    private Long uid;
    private AuthorisationField authorisationField;
    private TrainingPositionField trainingPositionField;
    private RotationPlan rotationPlan;
    private Registrar registrar;
    private LocalDate referenceDate;
    // TODO: Kleiner Hack, damit in der Spezifikation bei der Suche nach Registrar
    // UND Segmentdaten nach dem korrekten Attribut gesucht wird. Perspektivisch
    // müsse es einen RotationPlanFilter und einen RotationPlanSegmentFilter geben.
    private boolean searchForSegments;

    public RotationPlanFilter() {
	this.searchForSegments = false;
    }

    public AuthorisationField getAuthorisationField() {
	return authorisationField;
    }

    public LocalDate getReferenceDate() {
	return referenceDate;
    }

    public Registrar getRegistrar() {
	return registrar;
    }

    public RotationPlan getRotationPlan() {
	return rotationPlan;
    }

    public TrainingPositionField getTrainingPositionField() {
	return trainingPositionField;
    }

    public Long getUid() {
	return uid;
    }

    public boolean isSearchForSegments() {
	return searchForSegments;
    }

    public void setAuthorisationField(AuthorisationField authorisationField) {
	this.authorisationField = authorisationField;
    }

    public void setReferenceDate(LocalDate referenceDate) {
	this.referenceDate = referenceDate;
    }

    public void setRegistrar(Registrar registrar) {
	this.registrar = registrar;
    }

    public void setRotationPlan(RotationPlan rotationPlan) {
	this.rotationPlan = rotationPlan;
    }

    public void setSearchForSegments(boolean searchForSegments) {
	this.searchForSegments = searchForSegments;
    }

    public void setTrainingPositionField(TrainingPositionField trainingPositionField) {
	this.trainingPositionField = trainingPositionField;
    }

    public void setUid(Long uid) {
	this.uid = uid;
    }
}
