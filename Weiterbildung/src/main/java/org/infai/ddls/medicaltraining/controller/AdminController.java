package org.infai.ddls.medicaltraining.controller;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletRequest;

import org.apache.commons.io.FileUtils;
import org.infai.ddls.medicaltraining.config.SecurityConfiguration;
import org.infai.ddls.medicaltraining.imprt.FODSTrainingSiteParser;
import org.infai.ddls.medicaltraining.model.TrainingSite;
import org.infai.ddls.medicaltraining.model.TrainingSiteList;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@PreAuthorize(SecurityConfiguration.EL_HAS_ROLE_ADMIN)
public class AdminController extends AbstractMedicalTrainingController {
    @GetMapping("/admin/trainingsites/import")
    public String uploadFormTrainingSites() {
	return "admin/importTrainingSites";
    }

    @PostMapping("/admin/trainingsites/uploadDo")
    public String uploadTrainingSitesDo(@ModelAttribute @Validated TrainingSiteList trainingSites, BindingResult bindingResult, Model model, ServletRequest servletRequest) {
	if (bindingResult.hasErrors()) {
	    logger.warn(bindingResult.getAllErrors());

	    return editTrainingSites(trainingSites, model);
	} else {
	    trainingSiteDAO.save(trainingSites.getTrainingSites());

	    return redirect(RequestMappings.TRAININGSITES_LIST);
	}
    }

    @PostMapping("/admin/trainingsites/upload")
    public String uploadTrainingSitesPreview(@RequestParam("file") MultipartFile file, Model model) throws Exception {
	File targetFile = Files.createTempFile("trainingsites", ".fods").toFile();
	System.out.println(targetFile.getAbsolutePath());
	FileUtils.copyInputStreamToFile(file.getInputStream(), targetFile);

	FODSTrainingSiteParser parser = new FODSTrainingSiteParser();
	Collection<TrainingSite> trainingSites = parser.parse(targetFile);

	model.addAttribute("file", file);
	model.addAttribute("skippedSites", parser.getSkippedTrainingSites());

	TrainingSiteList trainingSiteList = new TrainingSiteList();
	trainingSiteList.setTrainingSites(new ArrayList<>(trainingSites));
	return editTrainingSites(trainingSiteList, model);
    }

    private String editTrainingSites(TrainingSiteList trainingSites, Model model) {
	model.addAttribute("trainingSites", trainingSites);
	populateMedicalFields(model);
	return "admin/trainingSitesUploaded";
    }

}
