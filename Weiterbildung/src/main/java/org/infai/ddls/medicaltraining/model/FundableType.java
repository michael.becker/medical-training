package org.infai.ddls.medicaltraining.model;

public enum FundableType {
    Fundable, NotFundable, PartiallyFundable, Unknown;
}
