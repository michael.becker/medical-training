package org.infai.ddls.medicaltraining.controller;

import java.util.List;

import javax.servlet.ServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.infai.ddls.medicaltraining.model.TrainingSite;
import org.infai.ddls.medicaltraining.repo.TrainingDAO;
import org.infai.ddls.medicaltraining.repo.TrainingSiteNewDAO;
import org.infai.ddls.medicaltraining.spec.TrainingSiteFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class TrainingSiteController extends AbstractMedicalTrainingController {
    public static final String editTrainingSite(Long uid) {
	return "/trainingsites/" + uid + "/edit";
    }

    @Autowired
    private TrainingSiteNewDAO dao;

    @Autowired
    private TrainingDAO trainingDAO;

    private final Log logger = LogFactory.getLog(getClass());

    @GetMapping(RequestMappings.TRAININGSITES_CREATE)
    public String createTrainingSite(Model model) {
	return editTrainingSite(new TrainingSite(), model);
    }

    @GetMapping(RequestMappings.TRAININGSITEDEPARTMENTS_CREATE)
    public String createTrainingSiteDepartment(@PathVariable("trainingSiteUID") long trainingSiteUID, Model model) {
	TrainingSite trainingSiteDepartment = new TrainingSite(dao.getTrainingSite(trainingSiteUID, true, true));

	return editTrainingSiteDepartment(trainingSiteDepartment, model);
    }

    @GetMapping(RequestMappings.TRAININGSITES_EDIT)
    public String editTrainingSite(@PathVariable long uid, Model model) {
	return editTrainingSite(dao.getTrainingSite(uid, true, true), model);
    }

    @GetMapping(RequestMappings.TRAININGSITEDEPARTMENTS_EDIT)
    public String editTrainingSiteDepartment(@PathVariable("trainingSiteUID") long trainingSiteUID, @PathVariable long trainingSiteDepartmentUID, Model model) {
	return editTrainingSiteDepartment(dao.getTrainingSite(trainingSiteDepartmentUID, false, true), model);
    }

    @PostMapping("trainingsites/filter")
    public String filterTrainingSites(@ModelAttribute("trainingSiteFilter") @Validated TrainingSiteFilter trainingSiteFilter, BindingResult bindingResult, Model model, Pageable pageable, ServletRequest servletRequest) {
	return null;
	// return listTrainingSites(dao.getTrainingSites(trainingSiteFilter,
	// pageable.getSort()), trainingSiteFilter, model);
    }

    @GetMapping(RequestMappings.TRAININGSITES_LIST)
    public String listTrainingSites(Model model, Pageable pageable) {
	return listTrainingSites(dao.getTrainingSites(pageable.getSort(), true, true), new TrainingSiteFilter(), model);
    }

    @PostMapping(RequestMappings.TRAININGSITES_UPDATE)
    public String updateTrainingSite(@ModelAttribute @Validated TrainingSite trainingSite, BindingResult bindingResult, Model model, ServletRequest servletRequest) {
	if (bindingResult.hasErrors()) {
	    logger.error(bindingResult.getAllErrors());
	    return editTrainingSite(trainingSite, model);
	}

	trainingSite = dao.save(trainingSite);

	if (isSave(servletRequest)) {
	    return redirect(editTrainingSite(trainingSite.getUid()));
	} else if (isSaveAndClose(servletRequest)) {
	    return redirect(RequestMappings.TRAININGSITES_LIST);
	} else {
	    throw new RuntimeException("Parameter saveAction not set or not valid!");
	}
    }

    @PostMapping(RequestMappings.TRAININGSITEDEPARTMENTS_UPDATE)
    public String updateTrainingSiteDepartment(@PathVariable("trainingSiteUID") long trainingSiteUID, @ModelAttribute("trainingSiteDepartment") @Validated TrainingSite trainingSiteDepartment, BindingResult bindingResult, Model model, ServletRequest servletRequest) {
	if (null == trainingSiteDepartment.getMedicalField()) {
	    bindingResult.rejectValue("medicalField", "NotNull.medicalField", "NotNull.medicalField");
	}
	if (null == trainingSiteDepartment.getTreatmentType()) {
	    bindingResult.rejectValue("treatmentType", "NotNull.treatmentType", "NotNull.treatmentType");
	}

	if (bindingResult.hasErrors()) {
	    logger.info(bindingResult.getAllErrors());
	    return editTrainingSiteDepartment(trainingSiteDepartment, model);
	} else {
	    dao.save(trainingSiteDepartment);
	    return redirect("/trainingsites/" + trainingSiteUID + "/edit");
	}
    }

    private String editTrainingSite(TrainingSite trainingSite, Model model) {
	model.addAttribute("trainingSite", trainingSite);

	populateEquipments(model);
	populateMedicalFields(model);

	return "trainingsites/editTrainingSite";
    }

    private String editTrainingSiteDepartment(TrainingSite trainingSiteDepartment, Model model) {
	model.addAttribute("trainingSiteDepartment", trainingSiteDepartment);

	populateEquipments(model);
	populateMedicalFields(model);

	return "trainingsites/editTrainingSiteDepartment";
    }

    private String listTrainingSites(List<TrainingSite> trainingSites, TrainingSiteFilter trainingSiteFilter, Model model) {
	model.addAttribute("trainingSites", trainingSites);
	model.addAttribute("trainingSiteFilter", trainingSiteFilter);
	model.addAttribute("medicalFields", trainingDAO.getMedicalFields());

	return "trainingsites/listTrainingSites";

    }
}
