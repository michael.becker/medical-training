package org.infai.ddls.medicaltraining.model;

import javax.persistence.Entity;

import org.hibernate.envers.Audited;

/**
 * Stellt eine Befugnis für eine Zusatz-Weiterbildung dar
 * 
 * @author Michael Becker
 *
 */
@Entity
@Audited
public class AuthorisationAdditionalTraining extends Authorisation {

}
