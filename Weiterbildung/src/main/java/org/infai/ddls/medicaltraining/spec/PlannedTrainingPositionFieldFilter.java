package org.infai.ddls.medicaltraining.spec;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import org.infai.ddls.medicaltraining.model.MedicalField;
import org.infai.ddls.medicaltraining.model.RotationPlanSegment;
import org.infai.ddls.medicaltraining.model.constraints.PlannedTrainingPositionBeginBeforEndConstraint;
import org.infai.ddls.medicaltraining.model.constraints.PlannedTrainingPositionDurationConstraint;

@PlannedTrainingPositionBeginBeforEndConstraint
@PlannedTrainingPositionDurationConstraint
public class PlannedTrainingPositionFieldFilter extends TrainingAppEntityFilter {
    public static PlannedTrainingPositionFieldFilter create(RotationPlanSegment rotationPlanSegment) {

	PlannedTrainingPositionFieldFilter filter = new PlannedTrainingPositionFieldFilter();
	filter.setAvailableFrom(rotationPlanSegment.getBegin());
	filter.setAvailableUntil(rotationPlanSegment.getEnd());
	filter.setFullTimeEquivalent(rotationPlanSegment.getFullTimeEquivalent());

	if (null != rotationPlanSegment.getTrainingPosition()) {
	    filter.setMedicalField(rotationPlanSegment.getTrainingPosition().getAuthorisationField().getTrainingSite().getMedicalField());
	}

	return filter;
    }

    @NotNull
    private MedicalField medicalField;
    @NotNull
    private LocalDate availableFrom;
    @NotNull
    private LocalDate availableUntil;
    @NotNull
    private double fullTimeEquivalent;

    public PlannedTrainingPositionFieldFilter() {
	this.setFullTimeEquivalent(1.0);
    }

    public LocalDate getAvailableFrom() {
	return availableFrom;
    }

    public LocalDate getAvailableUntil() {
	return availableUntil;
    }

    public double getFullTimeEquivalent() {
	return fullTimeEquivalent;
    }

    public MedicalField getMedicalField() {
	return medicalField;
    }

    public void setAvailableFrom(LocalDate availableFrom) {
	this.availableFrom = availableFrom;
    }

    public void setAvailableUntil(LocalDate availableUntil) {
	this.availableUntil = availableUntil;
    }

    public void setFullTimeEquivalent(double fullTimeEquivalent) {
	this.fullTimeEquivalent = fullTimeEquivalent;
    }

    public void setMedicalField(MedicalField medicalField) {
	this.medicalField = medicalField;
    }
}
