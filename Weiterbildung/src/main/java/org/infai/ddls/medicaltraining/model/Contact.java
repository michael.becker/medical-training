package org.infai.ddls.medicaltraining.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

@Entity
@Audited
public class Contact extends TrainingAppEntity {
    @NotNull
    @Column(nullable = false)
    private LocalDate contactDate;
    @NotNull
    @Column(nullable = false)
    private String contactPerson;
    @NotNull
    @ManyToOne
    private Person contact;
    @NotNull
    @Column(nullable = false)
    private ContactType contactType;

    public Person getContact() {
	return contact;
    }

    public LocalDate getContactDate() {
	return contactDate;
    }

    public String getContactPerson() {
	return contactPerson;
    }

    public ContactType getContactType() {
	return contactType;
    }

    public void setContact(Person contact) {
	this.contact = contact;
    }

    public void setContactDate(LocalDate contactDate) {
	this.contactDate = contactDate;
    }

    public void setContactPerson(String contactPerson) {
	this.contactPerson = contactPerson;
    }

    public void setContactType(ContactType contactType) {
	this.contactType = contactType;
    }
}
