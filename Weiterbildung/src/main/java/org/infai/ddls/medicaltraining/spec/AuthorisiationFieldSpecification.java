package org.infai.ddls.medicaltraining.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;
import org.springframework.data.jpa.domain.Specification;

@Deprecated
public class AuthorisiationFieldSpecification {
    @Deprecated
    @SuppressWarnings("serial")
    public static Specification<AuthorisationField> hasAuthorisedDoctor(AuthorisedDoctor authorisedDoctor) {
	return new Specification<AuthorisationField>() {
	    @Override
	    public Predicate toPredicate(Root<AuthorisationField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		return criteriaBuilder.equal(root.get("authorisedDoctor"), authorisedDoctor);
	    }
	};
    }
}
