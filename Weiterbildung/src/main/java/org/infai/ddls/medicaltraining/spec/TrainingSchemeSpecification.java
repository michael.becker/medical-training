package org.infai.ddls.medicaltraining.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltraining.model.TrainingScheme;

@SuppressWarnings("serial")
public class TrainingSchemeSpecification extends AbstractTrainingAppEntitySpecification<TrainingScheme, TrainingAppEntityFilter> {

    public TrainingSchemeSpecification(TrainingAppEntityFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<TrainingScheme> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
    }

}
