package org.infai.ddls.medicaltraining.repo;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.infai.ddls.medicaltraining.model.Equipment;
import org.infai.ddls.medicaltraining.model.TrainingSite;
import org.infai.ddls.medicaltraining.spec.EquipmentFilter;
import org.infai.ddls.medicaltraining.spec.EquipmentSpecification;
import org.infai.ddls.medicaltraining.spec.TrainingSiteFilter;
import org.infai.ddls.medicaltraining.spec.TrainingSiteSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@Component
public class TrainingSiteNewDAO extends AbstractTrainingAppDAO {
    @Autowired
    private TrainingSiteNewRepository trainingSiteRepository;
    @Autowired
    private EquipmentRepository equipmentRepository;
    @Autowired
    private TrainingDAO trainingDAO;

    public Equipment getEquipment(Long uid) {
	return equipmentRepository.findById(uid).get();
    }

    public List<Equipment> getEquipments() {
	return getEquipments(new EquipmentFilter(), null);
    }

    public List<Equipment> getEquipments(EquipmentFilter filter, Pageable pageable) {
	EquipmentSpecification spec = new EquipmentSpecification(filter);
	Sort sort = evaluateSort(pageable);

	return equipmentRepository.findAll(spec, sort);
    }

    @Transactional
    public TrainingSite getTrainingSite(Long uid, boolean loadChildSites, boolean loadEquipments) {
	TrainingSite trainingSite = trainingSiteRepository.findById(uid).get();

	if (loadChildSites) {
	    trainingSite.getChildren().size();

	    if (loadEquipments) {
		for (TrainingSite childSite : trainingSite.getChildren()) {
		    childSite.getEquipments().size();
		}
	    }
	}

	if (loadEquipments) {
	    trainingSite.getEquipments().size();
	}

	return trainingSite;
    }

    @Transactional
    public List<TrainingSite> getTrainingSites(Sort sort, boolean loadChildSites, boolean loadEquipments) {
	return getTrainingSites(new TrainingSiteFilter(), sort, loadChildSites, loadEquipments);
    }

    @Transactional
    public List<TrainingSite> getTrainingSites(TrainingSiteFilter filter, Sort sort, boolean loadChildSites, boolean loadEquipments) {
	if (null != filter.getMedicalField()) {
	    // notwendig für transitive Kindbeziehungen
	    filter.setMedicalField(trainingDAO.getMedicalField(filter.getMedicalField().getUid(), true, true));
	}

	List<TrainingSite> trainingSites = trainingSiteRepository.findAll(new TrainingSiteSpecification(filter), evaluateSort(sort));

	if (loadChildSites) {
	    for (TrainingSite trainingSite : trainingSites) {
		trainingSite.getChildren().size();

		if (loadEquipments) {
		    for (TrainingSite childSite : trainingSite.getChildren()) {
			childSite.getEquipments().size();
		    }
		}
	    }
	}

	if (loadEquipments) {
	    for (TrainingSite trainingSite : trainingSites) {
		trainingSite.getEquipments().size();
	    }
	}

	return trainingSites;

	// CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
	// CriteriaQuery<TrainingSiteNew> criteriaQuery =
	// criteriaBuilder.createQuery(TrainingSiteNew.class);
	// Root<TrainingSiteNew> root = criteriaQuery.from(TrainingSiteNew.class);
	//
	// if (null != filter.getMedicalField()) {
	// // notwendig für transitive Kindbeziehungen
	// filter.setMedicalField(trainingDAO.getMedicalField(filter.getMedicalField().getUid(),
	// true, true));
	// }
	//
	// criteriaQuery.where(new TrainingSiteSpecification(filter).toPredicate(root,
	// criteriaQuery, criteriaBuilder));
	// criteriaQuery.orderBy(criteriaBuilder.asc(root.get("parent").get("name")),
	// criteriaBuilder.asc(root.get("name")));
	//
	// Query query = entityManager.createQuery(criteriaQuery);
	//
	// if (0 != filter.getLatitude()) {
	// System.out.println("searching for " + filter.getLatitude() + ", " +
	// filter.getLongitude());
	// query.setParameter("latitude", filter.getLatitude());
	// query.setParameter("longitude", filter.getLongitude());
	// }
	//
	// @SuppressWarnings("unchecked")
	// List<TrainingSiteNew> trainingSiteDepartments = query.getResultList();
	// for (TrainingSiteNew trainingSite : trainingSiteDepartments) {
	// System.out.println(trainingSite.getName());
	// }
	//
	// // TODO: ziemlich hackig. Wenn nur nach Abteilungen gesucht wird, dann werden
	// // diese hier direkt zurückgegeben, ohne dass die Zuweisung zu den WB-Stätten
	// // erfolgt
	// if (filter.isDepartment()) {
	// return trainingSiteDepartments;
	// }
	//
	// List<TrainingSiteNew> trainingSites = new ArrayList<>();
	// for (TrainingSiteNew trainingSiteDepartment : trainingSiteDepartments) {
	// if (null == trainingSiteDepartment.getParent()) {
	// continue;
	// }
	// System.out.println("\t" + trainingSiteDepartment.getName() +
	// trainingSiteDepartment.getChildren());
	// if (!trainingSites.contains(trainingSiteDepartment.getParent())) {
	// TrainingSiteNew trainingSite = trainingSiteDepartment.getParent();
	// trainingSite.getChildren().clear();
	// trainingSites.add(trainingSite);
	// }
	//
	// trainingSiteDepartment.getParent().getChildren().add(trainingSiteDepartment);
	// }
	//
	// for (TrainingSiteNew trainingSite : trainingSites) {
	// trainingSite.getChildren().size();
	// }
	//
	// return trainingSites;
    }

    @Transactional
    public Equipment save(Equipment equipment) {
	return equipmentRepository.save(equipment);
    }

    @Transactional
    public List<TrainingSite> save(List<TrainingSite> trainingSites) {
	List<TrainingSite> savedTrainingSites = new ArrayList<>(trainingSites.size());

	for (TrainingSite trainingSite : trainingSites) {
	    savedTrainingSites.add(save(trainingSite));
	}

	return savedTrainingSites;
    }

    @Transactional
    public TrainingSite save(TrainingSite trainingSite) {
	List<TrainingSite> childSites = new ArrayList<>(trainingSite.getChildren().size());
	for (TrainingSite childSite : trainingSite.getChildren()) {
	    childSites.add(save(childSite));
	}

	trainingSite.setChildren(childSites);

	trainingSite.updateLatLng();
	return trainingSiteRepository.save(trainingSite);
    }
}
