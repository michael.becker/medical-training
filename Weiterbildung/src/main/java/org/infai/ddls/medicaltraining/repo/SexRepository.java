package org.infai.ddls.medicaltraining.repo;

import org.infai.ddls.medicaltraining.model.Sex;
import org.springframework.stereotype.Repository;

@Repository
public interface SexRepository extends TrainingAppEntityRepository<Sex> {

}
