package org.infai.ddls.medicaltraining.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.infai.ddls.medicaltraining.model.TrainingSite;

public class TrainingSiteList {
    @Valid
    List<TrainingSite> trainingSites;

    public TrainingSiteList() {
	this.trainingSites = new ArrayList<>();
    }

    public List<TrainingSite> getTrainingSites() {
	return trainingSites;
    }

    public void setTrainingSites(List<TrainingSite> trainingSites) {
	this.trainingSites = trainingSites;
    }
}
