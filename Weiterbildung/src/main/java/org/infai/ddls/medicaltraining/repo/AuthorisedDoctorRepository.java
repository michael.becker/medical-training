package org.infai.ddls.medicaltraining.repo;

import java.util.Optional;

import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;
import org.springframework.stereotype.Repository;

@Repository
interface AuthorisedDoctorRepository extends TrainingAppEntityRepository<AuthorisedDoctor> {
    public Optional<AuthorisedDoctor> findByLanr(Integer lanr);

    public Optional<AuthorisedDoctor> findByLanrAndUidNot(Integer lanr, Long uid);
}
