package org.infai.ddls.medicaltraining.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

/**
 * Weiterbilungsstelle in einem medizinischen Gebiet (Allgemeinmedizin,
 * Chirurgie etc.). Jede Stelle wird durch eine Befugnis abgedeckt, über die
 * festgelegt wird, wo die Stelle ist.
 * 
 * @author Michael Becker
 *
 */
@Entity
@Audited
public class TrainingPositionField extends TrainingPosition {
    @ManyToOne
    private AuthorisationField authorisationField;

    public TrainingPositionField() {
    }

    public TrainingPositionField(AuthorisationField authorisationField, LocalDate availableFrom, LocalDate availableUntil, int capacity, boolean partTimeAvailable) {
	this.authorisationField = authorisationField;
	this.availableFrom = availableFrom;
	this.availableUntil = availableUntil;
	this.capacity = capacity;
	this.partTimeAvailable = partTimeAvailable;
    }

    public AuthorisationField getAuthorisationField() {
	return authorisationField;
    }

    public void setAuthorisationField(AuthorisationField authorisationField) {
	this.authorisationField = authorisationField;
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append(" vom ").append(availableFrom).append(" bis ").append(availableUntil);
	builder.append(" Kapazität: ").append(capacity).append(" VZÄ: ").append(partTimeAvailable);

	return builder.toString();
    }

}
