package org.infai.ddls.medicaltraining.model;

public enum TrainingPositionAvailability {
    BLOCKED, OCCUPIED, OVERLAPPING, AVAILABLE, UNKNOWN;
}
