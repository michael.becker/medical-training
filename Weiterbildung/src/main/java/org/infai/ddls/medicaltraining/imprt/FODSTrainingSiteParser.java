package org.infai.ddls.medicaltraining.imprt;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.infai.ddls.medicaltraining.model.TrainingSite;
import org.infai.ddls.medicaltraining.model.TreatmentType;

import de.caterdev.utils.parser.AbstractFODSParser;

public class FODSTrainingSiteParser extends AbstractFODSParser<TrainingSite> {
    private static final int COLUMN_DOCTOR_NAME = 0;
    private static final int COLUMN_DOCTOR_FIRSTNAME = 1;
    private static final int COLUMN_TRAININGSITE_STREET = 5;
    private static final int COLUMN_TRAININGSITE_ZIP = 6;
    private static final int COLUMN_TRAININGSITE_CITY = 7;
    private static final int COLUMN_TRAININGSITE_TYPE = 8;
    private static final int COLUMN_TRAININGSITE_PHONE = 19;
    private static final int COLUMN_TRAININGSITE_FAX = 23;
    private static final int COLUMN_TRAININGSITE_EMAIL = 24;
    private static final int COLUMN_TRAININGSITE_WEB = 25;

    private static final String TYPE_SINGLE_PRAXIS = "Einzelpraxis";
    private Set<String> trainingSiteNames;
    private Map<Integer, TrainingSite> skippedTrainingSites;

    public FODSTrainingSiteParser() {
	this.trainingSiteNames = new HashSet<>();
	this.skippedTrainingSites = new HashMap<>();
    }

    public Map<Integer, TrainingSite> getSkippedTrainingSites() {
	return skippedTrainingSites;
    }

    @Override
    protected TrainingSite parseRowResult(List<String> rowResult, int rowNumber) throws Exception {
	// System.out.println(rowNumber + ": " + rowResult);
	String doctorFirstname = rowResult.get(COLUMN_DOCTOR_FIRSTNAME);
	String doctorName = rowResult.get(COLUMN_DOCTOR_NAME);
	String trainingSiteCity = rowResult.get(COLUMN_TRAININGSITE_CITY);
	String trainingSiteStreet = rowResult.get(COLUMN_TRAININGSITE_STREET);
	String trainingSiteZIP = rowResult.get(COLUMN_TRAININGSITE_ZIP);

	boolean skip = false;

	if (null == doctorFirstname) {
	    System.out.println("skipped #" + rowNumber + " doctorFirstName");
	    skip = true;
	}
	if (null == doctorName) {
	    System.out.println("skipped #" + rowNumber + " doctorName");
	    skip = true;
	}
	if (null == trainingSiteStreet) {
	    System.out.println("skipped #" + rowNumber + ": trainingSiteStreet");
	    skip = true;
	}
	if (null == trainingSiteZIP) {
	    System.out.println("skipped #" + rowNumber + ": trainingSiteZIP");
	    skip = true;
	}
	if (null == trainingSiteCity) {
	    System.out.println("skipped #" + rowNumber + ": trainingSiteCity");
	    skip = true;
	}

	if (skip) {
	    skipRow();
	    return null;
	}

	if (trainingSiteZIP.length() == 4) {
	    trainingSiteZIP = "0" + trainingSiteZIP;
	} else if (trainingSiteZIP.startsWith("D-")) {
	    trainingSiteZIP = trainingSiteZIP.substring(2);
	}

	// TODO: derzeit gibt es nicht die Möglichkeit, dass zwei Ärzte mit gleichem
	// Namen in einer Stadt eine Praxis haben
	String trainingSiteName = "Praxis " + doctorName + " - " + trainingSiteStreet + " " + trainingSiteZIP + " - " + trainingSiteCity;

	TrainingSite trainingSite = new TrainingSite();
	trainingSite.setName(trainingSiteName);
	trainingSite.getAddress().setStreet(trainingSiteStreet);
	trainingSite.getAddress().setZipCode(trainingSiteZIP);
	trainingSite.getAddress().setCity(trainingSiteCity);

	trainingSite.getContactInfo().setPhoneNumber(rowResult.get(COLUMN_TRAININGSITE_PHONE));
	trainingSite.getContactInfo().setFaxNumber(rowResult.get(COLUMN_TRAININGSITE_FAX));
	trainingSite.getContactInfo().setEmail(rowResult.get(COLUMN_TRAININGSITE_EMAIL));
	trainingSite.getContactInfo().setWeb(rowResult.get(COLUMN_TRAININGSITE_WEB));

	// TODO: Behandlungsart und Fachgebiet noch anhand der Einträge bestimmen lassen
	trainingSite.setTreatmentType(TreatmentType.Outpatient);

	if (trainingSiteNames.contains(trainingSiteName)) {
	    skippedTrainingSites.put(rowNumber, trainingSite);
	    skipRow();
	    return null;
	} else {
	    trainingSiteNames.add(trainingSiteName);
	}
	return trainingSite;
    }
}
