package org.infai.ddls.medicaltraining.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltraining.model.Registrar;

@SuppressWarnings("serial")
public class RegistrarSpecification extends AbstractTrainingAppEntitySpecification<Registrar, RegistrarFilter> {
    public RegistrarSpecification(RegistrarFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<Registrar> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	addPredicate(new PersonSpecification<Registrar, RegistrarFilter>(filter).toPredicate(root, query, criteriaBuilder));

	if (null != filter.getTrainingPosition()) {
	}

    }

}
