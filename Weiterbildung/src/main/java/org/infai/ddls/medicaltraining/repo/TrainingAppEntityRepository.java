package org.infai.ddls.medicaltraining.repo;

import org.infai.ddls.medicaltraining.model.TrainingAppEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

@NoRepositoryBean
public interface TrainingAppEntityRepository<T extends TrainingAppEntity> extends PagingAndSortingRepository<T, Long>, JpaSpecificationExecutor<T> {

}
