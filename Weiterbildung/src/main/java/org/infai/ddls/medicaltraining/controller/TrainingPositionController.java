package org.infai.ddls.medicaltraining.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;
import org.infai.ddls.medicaltraining.model.RotationPlanSegment;
import org.infai.ddls.medicaltraining.model.TrainingAppEntity;
import org.infai.ddls.medicaltraining.model.TrainingPositionAvailability;
import org.infai.ddls.medicaltraining.model.TrainingPositionExclusion;
import org.infai.ddls.medicaltraining.model.TrainingPositionField;
import org.infai.ddls.medicaltraining.model.TrainingScheme;
import org.infai.ddls.medicaltraining.model.TrainingSite;
import org.infai.ddls.medicaltraining.model.TrainingSiteType;
import org.infai.ddls.medicaltraining.model.TreatmentType;
import org.infai.ddls.medicaltraining.spec.RotationPlanFilter;
import org.infai.ddls.medicaltraining.spec.TrainingPositionFieldFilter;
import org.infai.ddls.medicaltraining.spec.TrainingSiteFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class TrainingPositionController extends AbstractMedicalTrainingController {
    public enum SelectAuthorisation {
	UseExistingAuthorisation, CreateNewAuthorisation, CreateNewAuthorised;

    }

    public enum SelectSite {
	UseExistingTrainingSite, CreateNewTrainingSite, CreateNewTrainingSiteWithDepartment;
    }

    public static final String PARAM_AUTHORISATION_FIELD = "authorisationField";

    public static final String PARAM_AUTHORISATION_FIELD_NAME = PARAM_AUTHORISATION_FIELD + ".name";
    public static final String PARAM_AUTHORISATION_GRANTED_ON = PARAM_AUTHORISATION_FIELD + ".grantedOn";
    public static final String PARAM_AUTHORISATION_GRANTED_TO = PARAM_AUTHORISATION_FIELD + ".grantedTo";
    public static final String PARAM_AUTHORISATION_MAXIMAL_DURATION = PARAM_AUTHORISATION_FIELD + ".maximalDuration";
    public static final String PARAM_AUTHORISATION_TRAINING_SCHEME = PARAM_AUTHORISATION_FIELD + ".trainingScheme";
    public static final String PARAM_DOCTOR = PARAM_AUTHORISATION_FIELD + ".authorisedDoctor";

    public static final String PARAM_DOCTOR_FIRSTNAME = PARAM_DOCTOR + ".name";
    public static final String PARAM_DOCTOR_LASTNAME = PARAM_DOCTOR + ".lastName";
    public static final String PARAM_DOCTOR_BIRTHDATE = PARAM_DOCTOR + ".birthdate";
    public static final String PARAM_DOCTOR_LANR = PARAM_DOCTOR + ".lanr";
    public static final String PARAM_DOCTOR_SEX = PARAM_DOCTOR + ".sex";
    public static final String PARAM_DOCTOR_TITLE = PARAM_DOCTOR + ".title";
    public static final String PARAM_DOCTOR_STREET = PARAM_DOCTOR + ".address.street";
    public static final String PARAM_DOCTOR_ZIPCODE = PARAM_DOCTOR + ".address.zipCode";
    public static final String PARAM_DOCTOR_CITY = PARAM_DOCTOR + ".address.city";
    public static final String PARAM_TRAININGSITE = PARAM_AUTHORISATION_FIELD + ".trainingSite";

    public static final String PARAM_TRAININGSITE_NAME = PARAM_TRAININGSITE + ".name";
    public static final String PARAM_TRAININGSITE_STREET = PARAM_TRAININGSITE + ".address.street";
    public static final String PARAM_TRAININGSITE_ZIPCODE = PARAM_TRAININGSITE + ".address.zipCode";
    public static final String PARAM_TRAININGSITE_CITY = PARAM_TRAININGSITE + ".address.city";
    public static final String PARAM_TRAININGSITE_MEDICALFIELD = PARAM_TRAININGSITE + ".medicalField";
    public static final String PARAM_TRAININGSITE_TREATMENT_TYPE = PARAM_TRAININGSITE + ".treatmentType";
    public static final String PARAM_TRAININGSITE_TYPE = PARAM_TRAININGSITE + ".trainingSiteType";
    public static final String PARAM_TRAININGSITE_EQUIPMENT = PARAM_TRAININGSITE + ".equipment";
    public static final String PARAM_TRAININGSITE_EMAIL = PARAM_TRAININGSITE + ".contactInfo.email";
    public static final String PARAM_TRAININGSITE_PHONENUMBER = PARAM_TRAININGSITE + ".contactInfo.phoneNumber";
    public static final String PARAM_TRAININGSITE_PHONENUMBER2 = PARAM_TRAININGSITE + ".contactInfo.phoneNumber2";
    public static final String PARAM_TRAININGSITE_FAXNUMBER = PARAM_TRAININGSITE + ".contactInfo.faxNumber";
    public static final String PARAM_TRAININGSITE_WEB = PARAM_TRAININGSITE + ".contactInfo.web";

    public static final String PARAM_DEPARTMENT = PARAM_TRAININGSITE + ".children[0]";
    public static final String PARAM_DEPARTMENT_NAME = PARAM_DEPARTMENT + ".name";
    public static final String PARAM_DEPARTMENT_STREET = PARAM_DEPARTMENT + ".address.street";
    public static final String PARAM_DEPARTMENT_ZIPCODE = PARAM_DEPARTMENT + ".address.zipCode";
    public static final String PARAM_DEPARTMENT_CITY = PARAM_DEPARTMENT + ".address.city";
    public static final String PARAM_DEPARTMENT_MEDICALFIELD = PARAM_DEPARTMENT + ".medicalField";
    public static final String PARAM_DEPARTMENT_TREATMENT_TYPE = PARAM_DEPARTMENT + ".treatmentType";
    public static final String PARAM_DEPARTMENT_EQUIPMENT = PARAM_DEPARTMENT + ".equipment";
    public static final String PARAM_DEPARTMENT_EMAIL = PARAM_DEPARTMENT + ".contactInfo.email";
    public static final String PARAM_DEPARTMENT_PHONENUMBER = PARAM_DEPARTMENT + ".contactInfo.phoneNumber";
    public static final String PARAM_DEPARTMENT_PHONENUMBER2 = PARAM_DEPARTMENT + ".contactInfo.phoneNumber2";
    public static final String PARAM_DEPARTMENT_FAXNUMBER = PARAM_DEPARTMENT + ".contactInfo.faxNumber";
    public static final String PARAM_DEPARTMENT_WEB = PARAM_DEPARTMENT + ".contactInfo.web";

    public static final String PARAM_AVAILABLE_FROM = "availableFrom";
    public static final String PARAM_AVAILABLE_UNTIL = "availableUntil";
    public static final String PARAM_CAPACITY = "capacity";
    public static final String PARAM_PT_AVAILABLE = "partTimeAvailable";
    public static final String SWITCH_SELECT_AUTHORISATION = "selectAuthorised";

    public static final String SWITCH_USE_AUTHORISATION_CASE_EXISTING = "useExistingAuthorised";
    public static final String SWITCH_USE_AUTHORISATION_CASE_NEW_AUTHORISATION = "createNewAuthorisation";
    public static final String SWITCH_USE_AUTHORISATION_CASE_NEW_AUTHORISED = "createNewAuthorised";
    public static final String SWITCH_SELECT_SITE = "selectSite";

    public static final String SWITCH_CREATE_DEPARTMENT = "createDepartmentCheck";
    @Autowired
    private Validator validator;

    @GetMapping(RequestMappings.TRAININGPOSITIONS_CREATE)
    public String createTrainingPositionField(Model model) {
	return editTrainingPositionField(new TrainingPositionField(), model);
    }

    @PostMapping("{trainingPositionFieldUID}/delete")
    public String deleteTrainingPositionFieldPerform(@PathVariable("trainingPositionFieldUID") long trainingPositionFieldUID, Model model) {
	if (trainingPositionDAO.delete(trainingPositionDAO.getTrainingPositionField(trainingPositionFieldUID), true)) {
	    return listTrainingPositionFields(trainingPositionDAO.getTrainingPositionsField(null, true), new TrainingPositionFieldFilter(0), model);
	} else {
	    return null;
	}
    }

    @GetMapping("/trainingpositions/{trainingPositionFieldUID}/delete")
    public String deleteTrainingPositionFieldVerify(@PathVariable("trainingPositionFieldUID") long trainingPositionFieldUID, Model model) {
	TrainingPositionField position = trainingPositionDAO.getTrainingPositionField(trainingPositionFieldUID);
	model.addAttribute("trainingPositionField", position);
	model.addAttribute("rotationPlanSegments", rotationPlanDAO.getRotationPlanSegments(position, null));

	return "trainingpositions/deleteTrainingPositionField";
    }

    @GetMapping(RequestMappings.TRAININGPOSITIONS_EDIT)
    public String editTrainingPositionField(@PathVariable long uid, Model model) {
	return editTrainingPositionField(trainingPositionDAO.getTrainingPositionField(uid, true), model);
    }

    @PostMapping("/trainingpositions/filter")
    public String filterTrainingPositionFields(@ModelAttribute("trainingPositionFieldFilter") @Validated TrainingPositionFieldFilter filter, BindingResult bindingResult, Model model, Pageable pageable, ServletRequest servletRequest) {
	return listTrainingPositionFields(trainingPositionDAO.getTrainingPositionsField(filter, pageable.getSort(), true), filter, model);
    }

    @GetMapping("/trainingpositions/{trainingPositionFieldUID}/segments")
    public String listTrainingPositionFieldRotationPlanSegments(@PathVariable("trainingPositionFieldUID") long trainingPositionFieldUID, Model model, Pageable pageable) {
	TrainingPositionField trainingPositionField = trainingPositionDAO.getTrainingPositionField(trainingPositionFieldUID, true);

	RotationPlanFilter filter = new RotationPlanFilter();
	filter.setTrainingPositionField(trainingPositionField);

	List<RotationPlanSegment> segementsForThisPosition = rotationPlanDAO.getRotationPlanSegments(filter, pageable.getSort());
	model.addAttribute("rotationPlanSegments", segementsForThisPosition);
	model.addAttribute("trainingPositionField", trainingPositionField);

	// Verfügbarkeiten dieser Weiterbildungsstelle je Monat berechnen lassen
	Map<LocalDate, TrainingPositionAvailability> availability = new LinkedHashMap<>();
	LocalDate now = LocalDate.now();
	for (int i = 0; i < 24; i++) {
	    LocalDate testDate = now.plusMonths(i);
	    TrainingPositionAvailability avail = trainingPositionField.getAvailability(testDate.withDayOfMonth(1), testDate.withDayOfMonth(testDate.lengthOfMonth()), segementsForThisPosition, 0);
	    availability.put(testDate, avail);
	}
	model.addAttribute("availability", availability);

	return "trainingpositions/listTrainingPositionRotationPlanSegments";
    }

    @GetMapping(RequestMappings.TRAININGPOSITIONS_LIST)
    public String listTrainingPositionFields(Model model, Pageable pageable) {
	return listTrainingPositionFields(trainingPositionDAO.getTrainingPositionsField(pageable.getSort(), true), new TrainingPositionFieldFilter(0), model);
    }

    @PostMapping(RequestMappings.TRAININGPOSITIONS_UPDATE)
    public String updateTrainingPosition(Model model, ServletRequest servletRequest) {
	System.out.println(formatHTTPParameters(servletRequest));

	TrainingSite trainingSite;
	AuthorisationField authorisationField;
	TrainingPositionField trainingPosition = createTrainingPosition(servletRequest);

	DataBinder binder = new DataBinder(trainingPosition, "trainingPosition");
	binder.setValidator(validator);

	boolean saveTrainingSite = false;
	boolean saveTrainingSiteParent = false;
	boolean saveAuthorisation = false;
	boolean saveAuthorised = false;

	SelectSite selectSite = SelectSite.valueOf(servletRequest.getParameter(SWITCH_SELECT_SITE));
	switch (selectSite) {
	case CreateNewTrainingSite: {
	    if (null != servletRequest.getParameter(SWITCH_CREATE_DEPARTMENT) && servletRequest.getParameter(SWITCH_CREATE_DEPARTMENT).equals(SWITCH_CHECKBOX_CHECKED)) {
		TrainingSite parent = createTrainingSite(servletRequest, true);
		trainingSite = createTrainingSiteDepartment(servletRequest, parent);
		trainingSite.setParent(parent);
		addBindingErrors(parent, "authorisationField.trainingSite.parent", binder);
		saveTrainingSiteParent = true;
	    } else {
		trainingSite = createTrainingSite(servletRequest, false);
	    }

	    addBindingErrors(trainingSite, "authorisationField.trainingSite", binder);

	    saveTrainingSite = true;
	    break;
	}
	case UseExistingTrainingSite: {
	    trainingSite = trainingSiteDAO.getTrainingSite(Long.valueOf(servletRequest.getParameter(PARAM_TRAININGSITE)), true, true);
	    break;
	}
	default:
	    throw new RuntimeException("Illegal value " + selectSite + " for parameter selectSite!");
	}

	// Anschließend wird die Befugnis verwendet
	SelectAuthorisation selectAuthorisation = SelectAuthorisation.valueOf(servletRequest.getParameter(SWITCH_SELECT_AUTHORISATION));
	switch (selectAuthorisation) {
	case UseExistingAuthorisation:
	    // eine bereits existierende Befugnis wird so wie sie ist für die Stelle
	    // verwendet
	    if (isEmpty(PARAM_AUTHORISATION_FIELD, servletRequest)) {
		authorisationField = null;
		binder.getBindingResult().addError(new ObjectError("authorisationField", "authorisationField not set!"));
		addBindingErrors(trainingPosition, "authorisedDoctor", binder);
	    } else {
		authorisationField = authorisationDAO.getAuthorisationField(Long.valueOf(servletRequest.getParameter("authorisationField")));
		authorisationField.setTrainingSite(trainingSite);
	    }
	    break;
	case CreateNewAuthorisation: {
	    // eine neue Befugnis für einen vorhanden Weiterbilder wird erstellt,
	    // gleichzeitig wird der Weiterbilder der Weiterbildungsstelle zugewiesen
	    authorisationField = new AuthorisationField();
	    AuthorisedDoctor authorisedDoctor = personDAO.getAuthorisedDoctor(Long.valueOf(servletRequest.getParameter("authorisationField.authorisedDoctor")), false, true);
	    authorisedDoctor.getTrainingSites().add(trainingSite);
	    TrainingScheme trainingScheme = trainingSchemeDAO.getTrainingScheme(Long.valueOf(servletRequest.getParameter("authorisationField.trainingScheme")), false);

	    authorisationField.setAuthorisedDoctor(authorisedDoctor);
	    authorisationField.setTrainingSite(trainingSite);
	    authorisationField.setTrainingScheme(trainingScheme);

	    if (null == servletRequest.getParameter(PARAM_AUTHORISATION_GRANTED_ON) || servletRequest.getParameter(PARAM_AUTHORISATION_GRANTED_ON).isEmpty()) {
		authorisationField.setGrantedOn(null);
	    } else {
		authorisationField.setGrantedOn(LocalDate.parse(servletRequest.getParameter("authorisationField.grantedOn")));
	    }

	    if (null == servletRequest.getParameter(PARAM_AUTHORISATION_GRANTED_TO) || servletRequest.getParameter(PARAM_AUTHORISATION_GRANTED_TO).isEmpty()) {
		authorisationField.setGrantedTo(null);
	    } else {
		authorisationField.setGrantedTo(LocalDate.parse(servletRequest.getParameter(PARAM_AUTHORISATION_GRANTED_TO)));
	    }
	    authorisationField.setMaximalDuration(Integer.valueOf(servletRequest.getParameter("authorisationField.maximalDuration")));
	    authorisationField.setName(authorisationField.getAuthorisedDoctor() + " @ " + authorisationField.getTrainingSite());

	    addBindingErrors(authorisationField, "authorisationField", binder);

	    saveAuthorisation = true;
	    saveAuthorised = true;
	    break;
	}
	case CreateNewAuthorised: {
	    // ein neuer Weiterbilder mit einer neuen Befugnis wird erstellt, gleichzeitig
	    // wird der neue Weiterbilder der Weiterbildungstelle zugewiesen
	    TrainingScheme trainingScheme = trainingSchemeDAO.getTrainingScheme(Long.valueOf(servletRequest.getParameter("authorisationField.trainingScheme")), false);

	    AuthorisedDoctor authorisedDoctor = createAuthorisedDoctor(servletRequest, trainingSite);
	    authorisedDoctor.getTrainingSites().add(trainingSite);

	    addBindingErrors(authorisedDoctor, "authorisationField.authorisedDoctor", binder);

	    authorisationField = new AuthorisationField();
	    authorisationField.setAuthorisedDoctor(authorisedDoctor);
	    authorisationField.setTrainingSite(trainingSite);
	    authorisationField.setTrainingScheme(trainingScheme);

	    if (null == servletRequest.getParameterValues(PARAM_AUTHORISATION_GRANTED_ON)[1] || servletRequest.getParameterValues(PARAM_AUTHORISATION_GRANTED_ON)[1].isEmpty()) {
		authorisationField.setGrantedOn(null);
	    } else {
		authorisationField.setGrantedOn(LocalDate.parse(servletRequest.getParameterValues(PARAM_AUTHORISATION_GRANTED_ON)[1]));
	    }
	    if (null == servletRequest.getParameterValues(PARAM_AUTHORISATION_GRANTED_TO)[1] || servletRequest.getParameterValues(PARAM_AUTHORISATION_GRANTED_TO)[1].isEmpty()) {
		authorisationField.setGrantedTo(null);
	    } else {
		authorisationField.setGrantedTo(LocalDate.parse(servletRequest.getParameterValues(PARAM_AUTHORISATION_GRANTED_TO)[1]));
	    }

	    authorisationField.setMaximalDuration(Integer.valueOf(servletRequest.getParameterValues("authorisationField.maximalDuration")[1]));
	    authorisationField.setName(authorisationField.getAuthorisedDoctor() + " @ " + authorisationField.getTrainingSite());

	    addBindingErrors(authorisationField, "authorisationField", binder);

	    saveAuthorisation = true;
	    saveAuthorised = true;
	    break;
	}
	default:
	    throw new RuntimeException("Illegal value " + selectAuthorisation + " for parameter selectAuthorised!");
	}

	trainingPosition.setAuthorisationField(authorisationField);
	trainingPosition.setName(trainingPosition.getAuthorisationField() + ": " + trainingPosition.getAvailableFrom() + " - " + trainingPosition.getAvailableUntil());
	trainingPosition.setExclusions(createExclusions(servletRequest));

	binder.validate();

	if (binder.getBindingResult().hasErrors()) {
	    logger.info(binder.getBindingResult().getAllErrors());
	    model.addAttribute(BindingResult.MODEL_KEY_PREFIX + "trainingPositionField", binder.getBindingResult());
	    return editTrainingPositionField(trainingPosition, model);
	} else {
	    trainingPositionDAO.save(trainingPosition, saveAuthorised, saveAuthorisation, saveTrainingSite, saveTrainingSiteParent);
	}

	return "redirect:/trainingpositions/list";
    }

    protected String editTrainingPositionField(TrainingPositionField trainingPositionField, Model model) {
	System.out.println("bearbeite " + trainingPositionField);
	if (null != trainingPositionField.getAuthorisationField()) {
	    System.out.println("\t" + trainingPositionField.getAuthorisationField().getTrainingSite());
	    System.out.println("\t" + trainingPositionField.getAuthorisationField().getTrainingSite().getEquipments());
	}
	model.addAttribute("trainingPositionField", trainingPositionField);

	populateAuthorisationFields(model);
	if (null != trainingPositionField.getAuthorisationField()) {
	    populateAuthorisations(model, trainingPositionField.getAuthorisationField().getTrainingSite());
	} else {
	    populateAuthorisations(model, trainingSiteDAO.getTrainingSites(new TrainingSiteFilter(), null, false, false).get(1));
	}
	populateTrainingSites(model, true, true);
	populateMedicalFields(model);
	populateEquipments(model);
	populateAuthorisedDoctors(model);
	populateTrainingSchemes(model);
	populateSexes(model);
	populateTitles(model);

	for (TrainingPositionExclusion exclusion : trainingPositionField.getExclusions()) {
	    System.out.println(exclusion.getExclusionFrom() + " - " + exclusion.getExclusionUntil());
	}
	System.out.println(LocalDate.parse("2018-11-01"));

	return "trainingpositions/editTrainingPositionField";
    }

    protected String listTrainingPositionFields(List<TrainingPositionField> trainingPositionFields, TrainingPositionFieldFilter filter, Model model) {
	model.addAttribute("trainingPositionFields", trainingPositionFields);
	model.addAttribute("trainingPositionFieldFilter", filter);

	populateMedicalFields(model);

	return "trainingpositions/listTrainingPositionFields";
    }

    private void addBindingErrors(TrainingAppEntity object, String path, DataBinder to) {
	DataBinder binder = new DataBinder(object);
	binder.setValidator(validator);
	binder.validate();

	for (ObjectError error : binder.getBindingResult().getAllErrors()) {
	    ObjectError wrappedError;
	    if (error instanceof FieldError) {
		wrappedError = new FieldError(error.getObjectName(), path + "." + ((FieldError) error).getField(), ((FieldError) error).getRejectedValue(), ((FieldError) error).isBindingFailure(), error.getCodes(), error.getArguments(), error.getDefaultMessage());
	    } else {
		wrappedError = new ObjectError(error.getObjectName(), error.getCodes(), error.getArguments(), error.getDefaultMessage());
	    }

	    to.getBindingResult().addError(wrappedError);
	}
    }

    private AuthorisedDoctor createAuthorisedDoctor(ServletRequest servletRequest, TrainingSite trainingSite) {
	AuthorisedDoctor authorisedDoctor = new AuthorisedDoctor();

	// Allgemeine Daten
	authorisedDoctor.setName(servletRequest.getParameter(PARAM_DOCTOR_FIRSTNAME));
	authorisedDoctor.setLastName(servletRequest.getParameter(PARAM_DOCTOR_LASTNAME));

	if (null != servletRequest.getParameter(PARAM_DOCTOR_SEX) && !servletRequest.getParameter(PARAM_DOCTOR_SEX).equals("")) {
	    authorisedDoctor.setSex(personDAO.getSex(Long.valueOf(servletRequest.getParameter(PARAM_DOCTOR_SEX))));
	}
	if (null != servletRequest.getParameter(PARAM_DOCTOR_TITLE) && !servletRequest.getParameter(PARAM_DOCTOR_TITLE).equals("")) {
	    authorisedDoctor.setTitle(personDAO.getTitle(Long.valueOf(servletRequest.getParameter(PARAM_DOCTOR_TITLE))));
	}
	if (null != servletRequest.getParameter(PARAM_DOCTOR_BIRTHDATE) && !servletRequest.getParameter(PARAM_DOCTOR_BIRTHDATE).equals("")) {
	    authorisedDoctor.setBirthDate(LocalDate.parse(servletRequest.getParameter(PARAM_DOCTOR_BIRTHDATE)));
	}

	// Adresse
	if (null == servletRequest.getParameter(PARAM_DOCTOR_STREET) || servletRequest.getParameter(PARAM_DOCTOR_STREET).equals("")) {
	    authorisedDoctor.getAddress().setStreet(trainingSite.getAddress().getStreet());
	} else {
	    authorisedDoctor.getAddress().setStreet(servletRequest.getParameter(PARAM_DOCTOR_STREET));
	}
	if (null == servletRequest.getParameter(PARAM_DOCTOR_ZIPCODE) || servletRequest.getParameter(PARAM_DOCTOR_ZIPCODE).equals("")) {
	    authorisedDoctor.getAddress().setZipCode(trainingSite.getAddress().getZipCode());
	} else {
	    authorisedDoctor.getAddress().setZipCode(servletRequest.getParameter(PARAM_DOCTOR_ZIPCODE));
	}
	if (null == servletRequest.getParameter(PARAM_DOCTOR_CITY) || servletRequest.getParameter(PARAM_DOCTOR_CITY).equals("")) {
	    authorisedDoctor.getAddress().setCity(trainingSite.getAddress().getCity());
	} else {
	    authorisedDoctor.getAddress().setCity(servletRequest.getParameter(PARAM_DOCTOR_CITY));
	}

	authorisedDoctor.getContactInfo().setPhoneNumber(servletRequest.getParameter("authorisationField.authorisedDoctor.contactInfo.phoneNumber"));
	authorisedDoctor.getContactInfo().setPhoneNumber2(servletRequest.getParameter("authorisationField.authorisedDoctor.contactInfo.phoneNumber2"));
	authorisedDoctor.getContactInfo().setFaxNumber(servletRequest.getParameter("authorisationField.authorisedDoctor.contactInfo.faxNumber"));
	authorisedDoctor.getContactInfo().setEmail(servletRequest.getParameter("authorisationField.authorisedDoctor.contactInfo.email"));

	authorisedDoctor.setNotes(servletRequest.getParameter("authorisationField.authorisedDoctor.notes"));

	if (null != servletRequest.getParameter(PARAM_DOCTOR_LANR) && !servletRequest.getParameter(PARAM_DOCTOR_LANR).equals("")) {
	    authorisedDoctor.setLanr(Integer.valueOf(servletRequest.getParameter(PARAM_DOCTOR_LANR)));
	}

	return authorisedDoctor;
    }

    private List<TrainingPositionExclusion> createExclusions(ServletRequest servletRequest) {
	boolean exclusionExists = true;
	int i = 0;
	List<TrainingPositionExclusion> exclusions = new ArrayList<>();
	while (exclusionExists) {
	    String exclusionUID = servletRequest.getParameter("exclusions[" + i + "].uid");
	    String exclusionFrom = servletRequest.getParameter("exclusions[" + i + "].exclusionFrom");
	    String exclusionUntil = servletRequest.getParameter("exclusions[" + i + "].exclusionUntil");
	    exclusionExists = false;
	    i++;

	    TrainingPositionExclusion exclusion = new TrainingPositionExclusion();
	    if (null != exclusionUID && !exclusionUID.equals("")) {
		exclusion.setUid(Long.valueOf(exclusionUID));
		exclusionExists = true;
	    }
	    if (null != exclusionFrom && !exclusionFrom.equals("")) {
		exclusion.setExclusionFrom(LocalDate.parse(exclusionFrom));
		exclusionExists = true;
	    }
	    if (null != exclusionUntil && !exclusionUntil.equals("")) {
		exclusion.setExclusionUntil(LocalDate.parse(exclusionUntil));
		exclusionExists = true;
	    }

	    if (exclusionExists) {
		exclusion.setName(exclusion.getExclusionFrom() + " - " + exclusion.getExclusionUntil());
		exclusions.add(exclusion);
	    }
	}

	return exclusions;
    }

    private TrainingPositionField createTrainingPosition(ServletRequest servletRequest) {
	TrainingPositionField trainingPosition = new TrainingPositionField();
	if (null != servletRequest.getParameter("uid") && !servletRequest.getParameter("uid").equals("")) {
	    trainingPosition.setUid(Long.valueOf(servletRequest.getParameter("uid")));
	}

	if (null == servletRequest.getParameter(PARAM_AVAILABLE_FROM) || servletRequest.getParameter(PARAM_AVAILABLE_FROM).equals("")) {
	    trainingPosition.setAvailableFrom(null);
	} else {
	    trainingPosition.setAvailableFrom(LocalDate.parse(servletRequest.getParameter(PARAM_AVAILABLE_FROM)));
	}

	if (null == servletRequest.getParameter(PARAM_AVAILABLE_UNTIL) || servletRequest.getParameter(PARAM_AVAILABLE_UNTIL).equals("")) {
	    trainingPosition.setAvailableUntil(null);
	} else {
	    trainingPosition.setAvailableUntil(LocalDate.parse(servletRequest.getParameter(PARAM_AVAILABLE_UNTIL)));
	}

	if (null == servletRequest.getParameter(PARAM_CAPACITY) || servletRequest.getParameter(PARAM_CAPACITY).equals("")) {
	    trainingPosition.setCapacity(0);
	} else {
	    trainingPosition.setCapacity(Integer.parseInt(servletRequest.getParameter("capacity")));
	}

	if (null != servletRequest.getParameter("partTimeAvailable") && !servletRequest.getParameter("partTimeAvailable").equals("")) {
	    trainingPosition.setPartTimeAvailable(Boolean.parseBoolean(servletRequest.getParameter("partTimeAvailable")));
	}

	return trainingPosition;
    }

    private TrainingSite createTrainingSite(ServletRequest servletRequest, boolean hasChildren) {
	TrainingSite trainingSite = new TrainingSite();
	trainingSite.setName(servletRequest.getParameter(PARAM_TRAININGSITE_NAME));
	trainingSite.getAddress().setStreet(servletRequest.getParameter(PARAM_TRAININGSITE_STREET));
	trainingSite.getAddress().setZipCode(servletRequest.getParameter(PARAM_TRAININGSITE_ZIPCODE));
	trainingSite.getAddress().setCity(servletRequest.getParameter(PARAM_TRAININGSITE_CITY));
	trainingSite.setTrainingSiteType(TrainingSiteType.valueOf(servletRequest.getParameter(PARAM_TRAININGSITE_TYPE)));

	trainingSite.getContactInfo().setEmail(servletRequest.getParameter(PARAM_TRAININGSITE_EMAIL));
	trainingSite.getContactInfo().setFaxNumber(servletRequest.getParameter(PARAM_TRAININGSITE_FAXNUMBER));
	trainingSite.getContactInfo().setPhoneNumber(servletRequest.getParameter(PARAM_TRAININGSITE_PHONENUMBER));
	trainingSite.getContactInfo().setPhoneNumber2(servletRequest.getParameter(PARAM_TRAININGSITE_PHONENUMBER2));
	trainingSite.getContactInfo().setWeb(servletRequest.getParameter(PARAM_TRAININGSITE_WEB));

	if (!hasChildren) {
	    if (null == servletRequest.getParameter(PARAM_TRAININGSITE_MEDICALFIELD) || servletRequest.getParameter(PARAM_TRAININGSITE_MEDICALFIELD).equals("")) {
		trainingSite.setMedicalField(null);
	    } else {
		trainingSite.setMedicalField(trainingDAO.getMedicalField(Long.valueOf(servletRequest.getParameter(PARAM_TRAININGSITE_MEDICALFIELD))));
	    }
	    if (null == servletRequest.getParameter(PARAM_TRAININGSITE_TREATMENT_TYPE) || servletRequest.getParameter(PARAM_TRAININGSITE_TREATMENT_TYPE).equals("")) {
		trainingSite.setTreatmentType(null);
	    } else {
		trainingSite.setTreatmentType(TreatmentType.valueOf(servletRequest.getParameter(PARAM_TRAININGSITE_TREATMENT_TYPE)));
	    }
	}

	return trainingSite;
    }

    private TrainingSite createTrainingSiteDepartment(ServletRequest servletRequest, TrainingSite trainingSite) {
	if (null == servletRequest.getParameter(PARAM_DEPARTMENT_NAME)) {
	    return null;
	} else {
	    TrainingSite trainingSiteDepartment = new TrainingSite();

	    trainingSiteDepartment.setName(servletRequest.getParameter(PARAM_DEPARTMENT_NAME));
	    trainingSiteDepartment.setMedicalField(trainingDAO.getMedicalField(Long.valueOf(servletRequest.getParameter(PARAM_DEPARTMENT_MEDICALFIELD))));
	    trainingSiteDepartment.setTreatmentType(TreatmentType.valueOf(servletRequest.getParameter(PARAM_DEPARTMENT_TREATMENT_TYPE)));
	    trainingSiteDepartment.setTrainingSiteType(TrainingSiteType.DEPARTMENT);

	    if (null == servletRequest.getParameter(PARAM_DEPARTMENT_STREET) || servletRequest.getParameter(PARAM_DEPARTMENT_STREET).equals("")) {
		trainingSiteDepartment.getAddress().setStreet(trainingSite.getAddress().getStreet());
	    } else {
		trainingSiteDepartment.getAddress().setStreet(servletRequest.getParameter(PARAM_DEPARTMENT_STREET));
	    }
	    if (null == servletRequest.getParameter(PARAM_DEPARTMENT_ZIPCODE) || servletRequest.getParameter(PARAM_DEPARTMENT_ZIPCODE).equals("")) {
		trainingSiteDepartment.getAddress().setZipCode(trainingSite.getAddress().getZipCode());
	    } else {
		trainingSiteDepartment.getAddress().setZipCode(servletRequest.getParameter(PARAM_DEPARTMENT_ZIPCODE));
	    }
	    if (null == servletRequest.getParameter(PARAM_DEPARTMENT_CITY) || servletRequest.getParameter(PARAM_DEPARTMENT_CITY).equals("")) {
		trainingSiteDepartment.getAddress().setCity(trainingSite.getAddress().getCity());
	    } else {
		trainingSiteDepartment.getAddress().setCity(servletRequest.getParameter(PARAM_DEPARTMENT_CITY));
	    }

	    trainingSiteDepartment.getContactInfo().setEmail(servletRequest.getParameter(PARAM_DEPARTMENT_EMAIL));
	    trainingSiteDepartment.getContactInfo().setFaxNumber(servletRequest.getParameter(PARAM_DEPARTMENT_FAXNUMBER));
	    trainingSiteDepartment.getContactInfo().setPhoneNumber(servletRequest.getParameter(PARAM_DEPARTMENT_PHONENUMBER));
	    trainingSiteDepartment.getContactInfo().setPhoneNumber2(servletRequest.getParameter(PARAM_DEPARTMENT_PHONENUMBER2));
	    trainingSiteDepartment.getContactInfo().setWeb(servletRequest.getParameter(PARAM_DEPARTMENT_WEB));

	    return trainingSiteDepartment;
	}
    }
}
