package org.infai.ddls.medicaltraining.config;

import javax.validation.Validator;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

@Configuration
@ComponentScan(basePackages = "org.infai.ddls.medicaltraining")
public class ApplicationConfiguration {
    @Bean
    public MessageSource messageSource() {
	ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
	messageSource.setBasename("classpath:messages/messages");
	messageSource.setDefaultEncoding("UTF-8");

	return messageSource;
    }

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
	MethodValidationPostProcessor processor = new MethodValidationPostProcessor();
	processor.setValidator(validator());

	return processor;
    }

    @Bean
    public Validator validator() {
	return new LocalValidatorFactoryBean();
    }
}
