package org.infai.ddls.medicaltraining.controller.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.infai.ddls.medicaltraining.controller.AbstractMedicalTrainingController;
import org.infai.ddls.medicaltraining.json.View;
import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.spec.AuthorisationFieldFilter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

@RestController
public class AuthorisationRESTController extends AbstractMedicalTrainingController {

    @GetMapping("/authorisations/getByTrainingSite/{trainingSiteUId}")
    @JsonView(View.Summary.class)
    public List<AuthorisationField> getAuthorisations(@PathVariable Long trainingSiteUId, HttpServletRequest servletRequest) {
	System.out.println(formatHTTPParameters(servletRequest));
	System.out.println("loading for " + trainingSiteUId);

	AuthorisationFieldFilter filter = new AuthorisationFieldFilter();
	filter.setTrainingSite(trainingSiteDAO.getTrainingSite(trainingSiteUId, false, false));

	List<AuthorisationField> authorisations = authorisationDAO.getAuthorisations(filter, null, true, true, true);

	System.out.println("returning " + authorisations);

	return authorisations;
    }
}
