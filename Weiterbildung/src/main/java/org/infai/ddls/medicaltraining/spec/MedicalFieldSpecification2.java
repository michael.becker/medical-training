package org.infai.ddls.medicaltraining.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltraining.model.MedicalField;

@SuppressWarnings("serial")
public class MedicalFieldSpecification2 extends AbstractTrainingAppEntitySpecification<MedicalField, MedicalFieldFilter> {
    public MedicalFieldSpecification2(MedicalFieldFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<MedicalField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
    }
}
