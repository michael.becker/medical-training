package org.infai.ddls.medicaltraining.controller;

public interface RequestMappings {
    public static final String __REDIRECT = "redirect:";
    public static final String UID = "uid";

    public static final String EQUIPMENTS_LIST = "/coredata/equipments/list";
    public static final String EQUIPMENTS_FILTER = "/coredata/equipments/filter";
    public static final String EQUIPMENTS_CREATE = "/coredata/equipments/create";
    public static final String EQUIPMENTS_EDIT = "/coredata/equipments/{" + UID + "}/edit";
    public static final String EQUIPMENTS_UPDATE = "/coredata/equipments/update";
    public static final String EQUIPMENTS_ARCHIVE = "/coredata/equipments/{" + UID + "}/archive";

    public static final String MEDICALFIELDS_LIST = "/coredata/medicalfields/list";
    public static final String MEDICALFIELDS_FILTER = "/coredata/medicalfields/filter";
    public static final String MEDICALFIELDS_CREATE = "/coredata/medicalfields/create";
    public static final String MEDICALFIELDS_EDIT = "/coredata/medicalfields/{" + UID + "}/edit";
    public static final String MEDICALFIELDS_UPDATE = "/coredata/medicalfields/update";
    public static final String MEDICALFIELDS_ARCHIVE = "/coredata/medicalfields/{" + UID + "}/archive";

    public static final String SEXES_LIST = "/coredata/sexes/list";
    public static final String SEXES_FILTER = "/coredata/sexes/filter";
    public static final String SEXES_CREATE = "/coredata/sexes/create";
    public static final String SEXES_EDIT = "/coredata/sexes/{" + UID + "}/edit";
    public static final String SEXES_UPDATE = "/coredata/sexes/update";
    public static final String SEXES_ARCHIVE = "/coredata/sexes/{" + UID + "}/archive";

    public static final String TITLES_LIST = "/coredata/titles/list";
    public static final String TITLES_FILTER = "/coredata/titles/filter";
    public static final String TITLES_CREATE = "/coredata/titles/create";
    public static final String TITLES_EDIT = "/coredata/titles/{" + UID + "}/edit";
    public static final String TITLES_UPDATE = "/coredata/titles/update";
    public static final String TITLES_ARCHIVE = "/coredata/titles/{" + UID + "}/archive";

    public static final String AUTHORISED_DOCTORS_LIST = "/doctors/authoriseddoctors/list";
    public static final String AUTHORISED_DOCTORS_CREATE = "/doctors/authoriseddoctors/create";
    public static final String AUTHORISED_DOCTORS_EDIT = "/doctors/authoriseddoctors/{" + UID + "}/edit";
    public static final String AUTHORISED_DOCTORS_UPDATE = "/doctors/authoriseddoctors/update";
    public static final String AUTHORISED_DOCTORS_ARCHIVE = "/authoriseddoctors/{" + UID + "}/archive";

    public static final String REGISTRARS_LIST = "/doctors/registrars/list";
    public static final String REGISTRARS_CREATE = "/doctors/registrars/create";
    public static final String REGISTRARS_EDIT = "/doctors/registrars/{" + UID + "}/edit";
    public static final String REGISTRARS_UPDATE = "/doctors/registrars/update";
    public static final String REGISTRARS_ARCHIVE = "/registrars/{" + UID + "}/archive";

    public static final String TRAININGSCHEMES_LIST = "/trainingschemes/list";
    public static final String TRAININGSCHEMES_FILTER = "/trainingschemes/filter";
    public static final String TRAININGSCHEMES_CREATE = "/trainingschemes/create";
    public static final String TRAININGSCHEMES_EDIT = "/trainingschemes/{" + UID + "}/edit";
    public static final String TRAININGSCHEMES_UPDATE = "/trainingschemes/update";
    public static final String TRAININGSCHEMES_ARCHIVE = "/trainingschemes/{" + UID + "}/archive";

    public static final String TRAININGSCHEMESEGMENTS_DELETE = "/trainingschemes/trainingschemesegments/{uid}/delete";

    public static final String TRAININGSITES_LIST = "/trainingsites/list";
    public static final String TRAININGSITES_CREATE = "/trainingsites/create";
    public static final String TRAININGSITES_EDIT = "/trainingsites/{" + UID + "}/edit";
    public static final String TRAININGSITES_UPDATE = "/trainingsites/update";
    public static final String TRAININGSITES_ARCHIVE = "/trainingsites/{" + UID + "}/archive";

    public static final String TRAININGSITEDEPARTMENTS_CREATE = "/trainingsites/{trainingSiteUID}/trainingsitedepartments/create";
    public static final String TRAININGSITEDEPARTMENTS_EDIT = "/trainingsites/{trainingSiteUID}/trainingsitedepartments/{trainingSiteDepartmentUID}/edit";
    public static final String TRAININGSITEDEPARTMENTS_UPDATE = "/trainingsites/{trainingSiteUID}/trainingsitedepartments/update";
    public static final String TRAININGSITEDEPARTMENTS_ARCHIVE = "/trainingsites/{trainingSiteUID}/trainingsitedepartments/{trainingSiteDepartmentUID}/archive";

    public static final String TRAININGPOSITIONS_LIST = "/trainingpositions/list";
    public static final String TRAININGPOSITIONS_CREATE = "/trainingpositions/create";
    public static final String TRAININGPOSITIONS_EDIT = "/trainingpositions/{uid}/edit";
    public static final String TRAININGPOSITIONS_UPDATE = "/trainingpositions/update";
    public static final String TRAININGPOSITIONS_ARCHIVE = "/trainingpositions/archive";
}
