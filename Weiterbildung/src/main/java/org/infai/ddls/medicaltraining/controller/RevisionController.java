package org.infai.ddls.medicaltraining.controller;

import org.infai.ddls.medicaltraining.logging.RevisionDAO;
import org.infai.ddls.medicaltraining.model.MedicalField;
import org.infai.ddls.medicaltraining.model.TrainingScheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("revisions")
public class RevisionController extends AbstractMedicalTrainingController {
    @Autowired
    private RevisionDAO revisionDAO;

    @GetMapping("detail/{type}/{uid}")
    public String detailedRevisionHistory(@PathVariable("type") String type, @PathVariable("uid") long uid, Model model) {
	if (type.equals("medicalField")) {
	    model.addAttribute("revisions", revisionDAO.getMedicalFieldRevisions(uid));

	    return "revisions/detailedRevisionHistoryMedicalField";
	} else if (type.equals("trainingScheme")) {
	    model.addAttribute("revisions", revisionDAO.getTrainingSchemeRevisions(uid));

	    return "revisions/detailedRevisionHistoryTrainingScheme";
	}

	return "revisions/detailedRevisionHistory";
    }

    @GetMapping("list")
    public String revisionHisotoryOverview(Model model) {
	model.addAttribute("medicalFieldRevisions", revisionDAO.getRevisions(MedicalField.class));
	model.addAttribute("trainingSchemeRevisions", revisionDAO.getRevisions(TrainingScheme.class));

	return "revisions/listRevisions";
    }
}
