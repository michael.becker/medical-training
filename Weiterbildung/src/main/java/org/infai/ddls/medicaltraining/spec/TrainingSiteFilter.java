package org.infai.ddls.medicaltraining.spec;

import org.infai.ddls.medicaltraining.model.MedicalField;

public class TrainingSiteFilter extends LocationFilter {
    private MedicalField medicalField;
    // true: auch Abteilungen (TrainingSites mit parent) werden gefunden, false:
    // Abteilungen werden nicht gefunde
    private boolean allowDepartments;
    // true: auch Oberelemente (TrainingSites ohne parent) werden gefunden, false:
    // Oberelemente werden nicht gefunden
    private boolean allowParents;

    public TrainingSiteFilter() {
	this.setAllowDepartments(false);
	this.setAllowParents(true);
    }

    public MedicalField getMedicalField() {
	return medicalField;
    }

    public boolean isAllowDepartments() {
	return allowDepartments;
    }

    public boolean isAllowParents() {
	return allowParents;
    }

    public void setAllowDepartments(boolean allowDepartments) {
	this.allowDepartments = allowDepartments;
    }

    public void setAllowParents(boolean allowParents) {
	this.allowParents = allowParents;
    }

    public void setMedicalField(MedicalField medicalField) {
	this.medicalField = medicalField;
    }

}
