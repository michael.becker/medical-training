package org.infai.ddls.medicaltraining.model;

public enum TreatmentType {
    Inpatient, Outpatient;
}
