package org.infai.ddls.medicaltraining.controller;

import java.nio.file.Files;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.infai.ddls.medicaltraining.logging.RevisionDAO;
import org.infai.ddls.medicaltraining.model.MedicalField;
import org.infai.ddls.medicaltraining.model.TrainingScheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController extends AbstractMedicalTrainingController {
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private ResourceLoader resourceLoader;
    @Autowired
    private RevisionDAO revisionDAO;

    @GetMapping("index")
    public String index(Model model) {
	System.out.println(revisionDAO.getRevisions(MedicalField.class));

	model.addAttribute("medicalFieldRevisions", revisionDAO.getRevisions(MedicalField.class));
	model.addAttribute("trainingSchemeRevisions", revisionDAO.getRevisions(TrainingScheme.class));

	return "/index";
    }

    @Transactional
    @GetMapping("reloadApplicationData")
    public String reloadApplication(Model model) throws Exception {
	String reloadDB = new String(Files.readAllBytes(resourceLoader.getResource("classpath:reload_db.sql").getFile().toPath()));
	String initialData = new String(Files.readAllBytes(resourceLoader.getResource("classpath:initial_data.sql").getFile().toPath())).replace("\n", "").replace("\r", "");
	Query query = entityManager.createNativeQuery(reloadDB);
	query.executeUpdate();

	query = entityManager.createNativeQuery(initialData);
	query.executeUpdate();

	return index(model);
    }
}
