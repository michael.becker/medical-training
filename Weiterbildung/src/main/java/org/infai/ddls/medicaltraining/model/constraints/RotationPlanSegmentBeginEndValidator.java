package org.infai.ddls.medicaltraining.model.constraints;

import java.time.LocalDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.infai.ddls.medicaltraining.model.RotationPlanSegment;

public class RotationPlanSegmentBeginEndValidator implements ConstraintValidator<RotationPlanSegmentBeginBeforeEndConstraint, RotationPlanSegment> {
    @Override
    public boolean isValid(RotationPlanSegment rotationPlanSegment, ConstraintValidatorContext context) {
	LocalDate begin = rotationPlanSegment.getBegin();
	LocalDate end = rotationPlanSegment.getEnd();

	if (null == begin || null == end) {
	    return true;
	}

	if (end.isBefore(begin)) {
	    return false;
	} else {
	    return true;
	}
    }
}
