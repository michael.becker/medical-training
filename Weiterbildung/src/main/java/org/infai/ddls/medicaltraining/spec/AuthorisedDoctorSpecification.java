package org.infai.ddls.medicaltraining.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;

@SuppressWarnings("serial")
public class AuthorisedDoctorSpecification extends AbstractTrainingAppEntitySpecification<AuthorisedDoctor, AuthorisedDoctorFilter> {
    public AuthorisedDoctorSpecification(AuthorisedDoctorFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<AuthorisedDoctor> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	addPredicate(new PersonSpecification<AuthorisedDoctor, AuthorisedDoctorFilter>(filter).toPredicate(root, query, criteriaBuilder));

	if (null != filter.getLanr()) {
	    addPredicate(criteriaBuilder.like(root.get("lanr").as(String.class), "%" + filter.getLanr() + "%"));
	}

	if (null != filter.getFullname()) {
	    Predicate firstname = likeIgnoreCase(filter.getFullname(), root.get("name"), criteriaBuilder);
	    Predicate lastname = likeIgnoreCase(filter.getFullname(), root.get("lastName"), criteriaBuilder);

	    addPredicateDisjunction(criteriaBuilder, firstname, lastname);
	}
    }

}
