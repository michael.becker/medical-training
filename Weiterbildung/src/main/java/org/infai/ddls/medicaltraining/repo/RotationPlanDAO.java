package org.infai.ddls.medicaltraining.repo;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.infai.ddls.medicaltraining.model.Registrar;
import org.infai.ddls.medicaltraining.model.RotationPlan;
import org.infai.ddls.medicaltraining.model.RotationPlanSegment;
import org.infai.ddls.medicaltraining.model.TrainingPositionField;
import org.infai.ddls.medicaltraining.spec.RotationPlanFilter;
import org.infai.ddls.medicaltraining.spec.RotationPlanSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
public class RotationPlanDAO extends AbstractTrainingAppDAO {
    @Autowired
    private RotationPlanRepository rotationPlanRepository;
    @Autowired
    private RotationPlanSegmentRepository rotationPlanSegmentRepository;

    /**
     * Löscht einen Rotationsplan. Wenn der Rotationsplan Segmente enthält, wird er
     * nicht gelöscht.
     * 
     * @param rotationPlan
     * @return true, wenn der Rotationsplan gelöscht wurde; false, wenn der
     *         Rotationsplan nicht gelöscht werden konnte, weil er Segmente enthält
     */
    @Transactional
    public boolean delete(RotationPlan rotationPlan) {
	return delete(rotationPlan, false);
    }

    /**
     * Löscht einen Rotationsplan.
     * 
     * @param rotationPlan
     *            der zu löschende Rotationsplan
     * @param deleteIfRotationPlanSegmentsArePresent
     *            legt fest, ob der Rotationsplan auch gelöscht werden soll, wenn er
     *            Segmente enthält
     * @return true, wenn der Rotationsplan gelöscht wurde; false, wenn der
     *         Rotationsplan nicht gelöscht werden konnte, weil er Segmente enthält
     *         und der entsprechende Parameter nicht gesetzt wurde
     */
    @Transactional
    public boolean delete(RotationPlan rotationPlan, boolean deleteIfRotationPlanSegmentsArePresent) {
	List<RotationPlanSegment> rotationPlanSegments = getRotationPlanSegments(rotationPlan, null);
	System.out.println("segments: " + rotationPlanSegments);
	if (!rotationPlanSegments.isEmpty()) {
	    if (deleteIfRotationPlanSegmentsArePresent) {
		for (RotationPlanSegment rotationPlanSegment : rotationPlanSegments) {
		    delete(rotationPlanSegment);
		}
	    } else {
		logger.warn("Attempted to delete RotationPlan " + rotationPlan + " but has assigned RotationPlanSegments!");
		return false;
	    }
	}

	rotationPlanRepository.delete(rotationPlan);

	return true;
    }

    /**
     * Löscht einen einzelnen Abschnitt eines Rotationsplans.
     * 
     * @param rotationPlanSegment
     *            der zu löschende Abschnitt
     * @return true
     */
    @Transactional
    public boolean delete(RotationPlanSegment rotationPlanSegment) {
	rotationPlanSegmentRepository.delete(rotationPlanSegment);
	return true;
    }

    /**
     * Gibt einen einzelnen Rotationsplan zurück. Im Rotationsplan sind keine
     * Abschnitte und keine Weiterbildungsordnungreferenzen enthalten.
     * 
     * @param uid
     *            die UID des Rotationsplans
     * @return den Rotationsplan mit der gegebenen UID oder null, wenn kein
     *         entsprechender Rotationsplan existiert
     */
    @Transactional
    public RotationPlan getRotationPlan(Long uid) {
	return getRotationPlan(uid, false, false);
    }

    /**
     * Gibt einen einzelnen Rotationsplan zurück. Mit Hilfe der Parameter kann
     * festgelegt werden, ob die Abschnitte des Rotationsplans und der
     * entsprechenden Weiterbildungsordnung mitgeladen werden sollen.
     * 
     * @param uid
     *            die UID des Rotationsplans
     * @param loadRotationPlanSegments
     *            legt fest, ob die Abschnitte des Rotationsplans mitgeladen werden
     *            sollen
     * @param loadTrainingSchemeSegments
     *            legt fest, ob die Abschnitte der Weiterbildungsordnung mitgeladen
     *            werden sollen
     * @return den Rotationsplan mit der gegebenen UID oder null, wenn kein
     *         entsprechender Rotationsplan existiert
     */
    @Transactional
    public RotationPlan getRotationPlan(Long uid, boolean loadRotationPlanSegments, boolean loadTrainingSchemeSegments) {
	Optional<RotationPlan> optional = rotationPlanRepository.findById(uid);
	if (optional.isPresent()) {
	    RotationPlan rotationPlan = optional.get();

	    if (loadRotationPlanSegments) {
		rotationPlan.getRotationPlanSegments().size();
	    }

	    if (loadTrainingSchemeSegments) {
		rotationPlan.getTrainingScheme().getTrainingSchemeSegments().size();
	    }

	    return rotationPlan;
	} else {
	    return null;
	}
    }

    /**
     * Gibt alle Rotationspläne zurück, die auf den gegebenen Filter matchen.
     * 
     * @param filter
     *            der Filter, zu dem die Rotationspläne passen sollen
     * @param sort
     *            Sortierung der Rotationspläne
     * @param loadRotationPlanSegments
     *            legt fest, ob die Abschnitte der Rotationspläne mitgeladen werden
     *            sollen
     * @param loadTrainingSchemeSegments
     *            leggt fest, ob die Abschnitte der Weiterbildungsordnung mitgeladen
     *            werden sollen
     * @return alle Rotationspläne, die dem Filter entsprechen
     */
    @Transactional
    public List<RotationPlan> getRotationPlans(RotationPlanFilter filter, Sort sort, boolean loadRotationPlanSegments, boolean loadTrainingSchemeSegments) {
	Specification<RotationPlan> spec = new RotationPlanSpecification<>(filter);

	List<RotationPlan> rotationPlans;
	if (null == sort) {
	    rotationPlans = rotationPlanRepository.findAll(spec);
	} else {
	    rotationPlans = rotationPlanRepository.findAll(spec, sort);
	}

	if (loadRotationPlanSegments) {
	    for (RotationPlan rotationPlan : rotationPlans) {
		rotationPlan.getRotationPlanSegments().size();
	    }
	}

	if (loadTrainingSchemeSegments) {
	    for (RotationPlan rotationPlan : rotationPlans) {
		rotationPlan.getTrainingScheme().getTrainingSchemeSegments().size();
	    }
	}

	return rotationPlans;
    }

    /**
     * Gibt alle Rotationspläne zurück.
     * 
     * @param sort
     *            Sortierung der Rotationspläe
     * @return alle Rotationspläne
     */
    @Transactional
    public List<RotationPlan> getRotationPlans(Sort sort) {
	return getRotationPlans(new RotationPlanFilter(), null, false, false);
    }

    /**
     * Gibt einen einzelnen Weiterbildungsabschnitt zurück.
     * 
     * @param uid
     *            die UID des Weiterbildungsabschnitts
     * @return den Weiterbildungsabschnitt mit der gegebenen UID oder null, wenn
     *         keine entsprechende Entity existiert
     */
    @Transactional
    public RotationPlanSegment getRotationPlanSegment(Long uid) {
	Optional<RotationPlanSegment> optional = rotationPlanSegmentRepository.findById(uid);

	if (optional.isPresent()) {
	    RotationPlanSegment rotationPlanSegment = optional.get();
	    rotationPlanSegment.getRotationPlan().getTrainingScheme().getTrainingSchemeSegments().size();
	    System.out.println(rotationPlanSegment.getTrainingPosition().getAuthorisationField().getTrainingSite());
	    System.err.println(rotationPlanSegment.getTrainingPosition().getAuthorisationField().getAuthorisedDoctor());

	    return rotationPlanSegment;
	} else {
	    return null;
	}
    }

    /**
     * Gobt denjenigen Weiterbildungsabschnitt zurück, zu dem der angegebene AiW zum
     * angegebenen Referenzdatum angestellt ist.
     * 
     * @param registrar
     *            AiW
     * @param referenceDate
     *            Referenzdatum, für das gesucht wird
     * @return Weiterbildungsabschnitt des gegebenen AiW zum Referenzdatum oder
     *         null, wenn kein solcher vorhanden ist
     */
    public RotationPlanSegment getRotationPlanSegment(Registrar registrar, LocalDate referenceDate) {
	RotationPlanFilter filter = new RotationPlanFilter();
	filter.setSearchForSegments(true);
	filter.setRegistrar(registrar);
	filter.setReferenceDate(referenceDate);

	List<RotationPlanSegment> segments = getRotationPlanSegments(filter, null);
	if (0 == segments.size()) {
	    return null;
	} else {
	    return segments.get(0);
	}
    }

    /**
     * Gibt alle Weiterbildungsabschnitte zurück, die zum gegebenen
     * Weiterbildungsplan gehören.
     * 
     * @param rotationPlan
     * @param sort
     * @return
     */
    @Transactional
    public List<RotationPlanSegment> getRotationPlanSegments(RotationPlan rotationPlan, Sort sort) {
	RotationPlanFilter filter = new RotationPlanFilter();
	filter.setRotationPlan(rotationPlan);

	return getRotationPlanSegments(filter, sort);
    }

    /**
     * Gibt alle Weiterbildungsabschnitte zurück, die dem entsprechenden Filter
     * matchen.
     * 
     * @param filter
     *            Filter für Weiterbildungsabschnitte
     * @param sort
     *            Sortierung der Abschnitte
     * @return alle Weiterbildungsabeshcnitte, die dem Filter entsprechen
     */
    @Transactional
    public List<RotationPlanSegment> getRotationPlanSegments(RotationPlanFilter filter, Sort sort) {
	Specification<RotationPlanSegment> spec = new RotationPlanSpecification<>(filter);

	if (null == sort) {
	    return rotationPlanSegmentRepository.findAll(spec);
	} else {
	    return rotationPlanSegmentRepository.findAll(spec, sort);
	}
    }

    /**
     * Gibt alle Weiterbildungsabschnitte zurück
     * 
     * @param sort
     * @return
     */
    @Transactional
    public List<RotationPlanSegment> getRotationPlanSegments(Sort sort) {
	return getRotationPlanSegments(new RotationPlanFilter(), sort);
    }

    /**
     * Gibt alle Weiterbildungsabschnitte der gegebenen Weiterbildungsstelle zurück.
     * 
     * @param trainingPositionField
     * @param sort
     * @return
     */
    @Transactional
    public List<RotationPlanSegment> getRotationPlanSegments(TrainingPositionField trainingPositionField, Sort sort) {
	RotationPlanFilter filter = new RotationPlanFilter();
	filter.setTrainingPositionField(trainingPositionField);

	return getRotationPlanSegments(filter, sort);
    }

    /**
     * Speichert einen Weiterbildungsplan.
     * 
     * @param rotationPlan
     * @return
     */
    @Transactional
    public RotationPlan saveRotationPlan(RotationPlan rotationPlan) {
	RotationPlan result = rotationPlanRepository.save(rotationPlan);
	rotationPlanSegmentRepository.saveAll(rotationPlan.getRotationPlanSegments());

	return result;
    }

    /**
     * Speichert einen Weiterbildungsabschnitt.
     * 
     * @param rotationPlanSegment
     * @return
     */
    @Transactional
    public RotationPlanSegment saveRotationPlanSegment(RotationPlanSegment rotationPlanSegment) {
	System.err.println("speichere " + rotationPlanSegment);
	return rotationPlanSegmentRepository.save(rotationPlanSegment);
    }
}
