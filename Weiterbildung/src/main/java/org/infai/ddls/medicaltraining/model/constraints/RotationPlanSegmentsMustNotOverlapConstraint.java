package org.infai.ddls.medicaltraining.model.constraints;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = RotationPlanSegmentsDoNotOverlapValidator.class)
public @interface RotationPlanSegmentsMustNotOverlapConstraint {
    Class<?>[] groups() default {};

    String message() default "{rotationplan.segmentsoverlap}";

    Class<? extends Payload>[] payload() default {};
}
