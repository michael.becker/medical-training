package org.infai.ddls.medicaltraining.model.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.infai.ddls.medicaltraining.model.TrainingSchemeSegment;

public class TrainingSchemeSegmentTreatmentTypeSetValidator implements ConstraintValidator<TrainingSchemeSegmentTreatmentTypeSetConstraint, TrainingSchemeSegment> {

    @Override
    public boolean isValid(TrainingSchemeSegment trainingSchemeSegment, ConstraintValidatorContext context) {
	if (!trainingSchemeSegment.isInpatient() && !trainingSchemeSegment.isOutpatient()) {
	    return false;
	} else {
	    return true;
	}
    }

}
