package org.infai.ddls.medicaltraining.model.constraints;

import java.io.IOException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.infai.ddls.medicaltraining.model.Address;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;

public class AddressExistsValidator implements ConstraintValidator<AddressExistsConstraint, Address> {
    private static final GeoApiContext geoContext = new GeoApiContext.Builder().apiKey("AIzaSyCL2ACPvjLb5E10PKXOYcGFqYF-LccVe1w").build();

    @Override
    public boolean isValid(Address address, ConstraintValidatorContext context) {
	GeocodingResult[] results;

	try {
	    results = GeocodingApi.geocode(geoContext, address.getStreet() + " " + address.getZipCode() + " " + address.getCity()).await();
	} catch (ApiException | InterruptedException | IOException e) {
	    throw new RuntimeException(e);
	}

	if (0 == results.length) {
	    return false;
	} else {
	    return true;
	}
    }

}
