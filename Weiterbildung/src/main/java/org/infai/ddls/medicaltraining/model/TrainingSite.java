package org.infai.ddls.medicaltraining.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltraining.model.constraints.TrainingSiteTreatmentTypeConstraint;

@Entity
@Audited
@TrainingSiteTreatmentTypeConstraint
public class TrainingSite extends Location implements IHasChildren<TrainingSite> {
    @ManyToOne
    private MedicalField medicalField;
    @ManyToMany
    private List<Equipment> equipments;
    private TreatmentType treatmentType;
//    @ManyToMany(mappedBy = "trainingSites")
    @ManyToMany
    @OrderBy("lastname ASC")
    private List<AuthorisedDoctor> authorisedDoctors;
    @ManyToOne
    private TrainingSite parent;
    @OneToMany(mappedBy = "parent")
    @OrderBy("name ASC")
    private List<TrainingSite> children;
    @Valid
    @Embedded
    private ContactInfo contactInfo;
    @NotNull
    @Column(nullable = false)
    private TrainingSiteType trainingSiteType;
    @ManyToOne
    private Region region;

    public TrainingSite() {
	this(null);
    }

    public TrainingSite(TrainingSite parent) {
	if (null != parent) {
	    this.address.setStreet(parent.getAddress().getStreet());
	    this.address.setZipCode(parent.getAddress().getZipCode());
	    this.address.setCity(parent.getAddress().getCity());

	    this.setParent(parent);
	}

	this.setAuthorisedDoctors(new ArrayList<>());
	this.setChildren(new ArrayList<>());
	this.setEquipments(new ArrayList<>());
	this.setContactInfo(new ContactInfo());
	this.setTrainingSiteType(TrainingSiteType.DOCTORSOFFICE);
    }

    public List<AuthorisedDoctor> getAuthorisedDoctors() {
	return authorisedDoctors;
    }

    @Override
    public List<TrainingSite> getChildren() {
	return children;
    }

    public ContactInfo getContactInfo() {
	return contactInfo;
    }

    public List<Equipment> getEquipments() {
	return equipments;
    }

    public MedicalField getMedicalField() {
	return medicalField;
    }

    @Override
    public TrainingSite getParent() {
	return parent;
    }

    public Region getRegion() {
	return region;
    }

    public TrainingSiteType getTrainingSiteType() {
	return trainingSiteType;
    }

    public TreatmentType getTreatmentType() {
	return treatmentType;
    }

    public void setAuthorisedDoctors(List<AuthorisedDoctor> authorisedDoctors) {
	this.authorisedDoctors = authorisedDoctors;
    }

    @Override
    public void setChildren(List<TrainingSite> children) {
	this.children = children;
    }

    public void setContactInfo(ContactInfo contactInfo) {
	this.contactInfo = contactInfo;
    }

    public void setEquipments(List<Equipment> equipments) {
	this.equipments = equipments;
    }

    public void setMedicalField(MedicalField medicalField) {
	this.medicalField = medicalField;
    }

    @Override
    public void setParent(TrainingSite parent) {
	this.parent = parent;

	if (null != parent && !parent.getChildren().contains(this)) {
	    this.parent.getChildren().add(this);
	}
    }

    public void setRegion(Region region) {
	this.region = region;
    }

    public void setTrainingSiteType(TrainingSiteType trainingSiteType) {
	this.trainingSiteType = trainingSiteType;
    }

    public void setTreatmentType(TreatmentType treatmentType) {
	this.treatmentType = treatmentType;
    }
}
