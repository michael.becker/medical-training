package org.infai.ddls.medicaltraining.spec;

import java.time.LocalDate;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;
import org.infai.ddls.medicaltraining.model.MedicalField;
import org.infai.ddls.medicaltraining.model.RotationPlanSegment;

public class TrainingPositionFieldFilter extends LocationFilter {
    public static TrainingPositionFieldFilter create(RotationPlanSegment rotationPlanSegment) {

	TrainingPositionFieldFilter filter = new TrainingPositionFieldFilter();
	filter.setAvailableFrom(rotationPlanSegment.getBegin());
	filter.setAvailableUntil(rotationPlanSegment.getEnd());
	filter.setFullTimeEquivalent(rotationPlanSegment.getFullTimeEquivalent());

	if (null != rotationPlanSegment.getTrainingPosition()) {
	    filter.setMedicalField(rotationPlanSegment.getTrainingPosition().getAuthorisationField().getTrainingSite().getMedicalField());
	}

	return filter;
    }

    private MedicalField medicalField;
    private LocalDate availableFrom;
    private LocalDate availableUntil;
    private int minimalCapacity;
    private AuthorisationField authorisationField;
    private AuthorisedDoctor authorisedDoctor;

    private double fullTimeEquivalent;

    public TrainingPositionFieldFilter() {
	this.setFullTimeEquivalent(1.0);
	this.setMinimalCapacity(1);
    }

    public TrainingPositionFieldFilter(int minimalCapacity) {
	this.setFullTimeEquivalent(1.0);
	this.setMinimalCapacity(minimalCapacity);
    }

    public AuthorisationField getAuthorisationField() {
	return authorisationField;
    }

    public AuthorisedDoctor getAuthorisedDoctor() {
	return authorisedDoctor;
    }

    public LocalDate getAvailableFrom() {
	return availableFrom;
    }

    public LocalDate getAvailableUntil() {
	return availableUntil;
    }

    public double getFullTimeEquivalent() {
	return fullTimeEquivalent;
    }

    public MedicalField getMedicalField() {
	return medicalField;
    }

    public int getMinimalCapacity() {
	return minimalCapacity;
    }

    public void setAuthorisationField(AuthorisationField authorisationField) {
	this.authorisationField = authorisationField;
    }

    public void setAuthorisedDoctor(AuthorisedDoctor authorisedDoctor) {
	this.authorisedDoctor = authorisedDoctor;
    }

    public void setAvailableFrom(LocalDate availableFrom) {
	this.availableFrom = availableFrom;
    }

    public void setAvailableUntil(LocalDate availableUntil) {
	this.availableUntil = availableUntil;
    }

    public void setFullTimeEquivalent(double fullTimeEquivalent) {
	this.fullTimeEquivalent = fullTimeEquivalent;
    }

    public void setMedicalField(MedicalField medicalField) {
	this.medicalField = medicalField;
    }

    public void setMinimalCapacity(int minimalCapacity) {
	this.minimalCapacity = minimalCapacity;
    }
}
