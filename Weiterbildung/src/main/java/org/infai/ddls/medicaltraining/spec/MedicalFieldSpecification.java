package org.infai.ddls.medicaltraining.spec;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltraining.model.MedicalField;
import org.infai.ddls.medicaltraining.model.TrainingAppEntity;
import org.springframework.data.jpa.domain.Specification;

/**
 * Erstellt ein Prädikat, um ein {@link MedicalField} zu selektieren. Das
 * Prädikat prüft, ob das Gebiet der zu prüfenten Entity mit dem gegebenen
 * Gebiet übereinstimmt oder ein Kindgebiet davon ist. Damit das funktioniert,
 * müssen die Kindgebiete des gegebenen Gebiets initialisiert sein.
 * 
 * @author Michael Becker
 *
 * @param <T>
 */
@SuppressWarnings("serial")
public class MedicalFieldSpecification<T extends TrainingAppEntity> implements Specification<T> {
    public static final String ATTRIBUTE_NAME_MEDICAL_FIELD = "medicalField";

    private MedicalField medicalField;

    public MedicalFieldSpecification(MedicalField medicalField) {
	this.medicalField = medicalField;
    }

    /**
     * Erstellt ein Prädikat beginnend ab dem angegebenen Pfad. Der Pfad wird um die
     * Abfrage nach dem Attribute medicalField ergänzt.
     * 
     * @param path
     * @param query
     * @param criteriaBuilder
     * @return
     */
    public Predicate toPredicate(Path<T> path, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	List<Predicate> fieldPredicates = new ArrayList<>();

	if (null != medicalField) {
	    Predicate equalFieldPredicate = criteriaBuilder.equal(path, medicalField);
	    fieldPredicates.add(equalFieldPredicate);

	    for (MedicalField medicalField : medicalField.getAllChildFields()) {
		Predicate childFieldPredicate = criteriaBuilder.equal(path, medicalField);
		fieldPredicates.add(childFieldPredicate);
	    }

	    return criteriaBuilder.or(fieldPredicates.toArray(new Predicate[fieldPredicates.size()]));
	} else {
	    return criteriaBuilder.conjunction();
	}
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return toPredicate(root.get(ATTRIBUTE_NAME_MEDICAL_FIELD), query, criteriaBuilder);
    }
}