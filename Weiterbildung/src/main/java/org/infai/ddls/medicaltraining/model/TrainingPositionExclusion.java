package org.infai.ddls.medicaltraining.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

@Entity
@Audited
public class TrainingPositionExclusion extends TrainingAppEntity {
    @NotNull
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate exclusionFrom;
    @NotNull
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate exclusionUntil;

    public TrainingPositionExclusion() {
    }

    public TrainingPositionExclusion(LocalDate exclusionFrom, LocalDate exclusionUntil) {
	this.exclusionFrom = exclusionFrom;
	this.exclusionUntil = exclusionUntil;
    }

    public LocalDate getExclusionFrom() {
	return exclusionFrom;
    }

    public LocalDate getExclusionUntil() {
	return exclusionUntil;
    }

    public void setExclusionFrom(LocalDate exclusionFrom) {
	this.exclusionFrom = exclusionFrom;
    }

    public void setExclusionUntil(LocalDate exclusionUntil) {
	this.exclusionUntil = exclusionUntil;
    }

    @Override
    public String toString() {
	return exclusionFrom + " - " + exclusionUntil;
    }
}
