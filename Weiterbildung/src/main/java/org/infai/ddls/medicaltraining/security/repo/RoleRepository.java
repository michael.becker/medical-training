package org.infai.ddls.medicaltraining.security.repo;

import org.infai.ddls.medicaltraining.security.model.Role;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends PagingAndSortingRepository<Role, Long> {

}
