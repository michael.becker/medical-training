package org.infai.ddls.medicaltraining.controller;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;
import org.infai.ddls.medicaltraining.model.Equipment;
import org.infai.ddls.medicaltraining.model.MedicalField;
import org.infai.ddls.medicaltraining.model.Sex;
import org.infai.ddls.medicaltraining.model.Title;
import org.infai.ddls.medicaltraining.model.TrainingPositionField;
import org.infai.ddls.medicaltraining.model.TrainingScheme;
import org.infai.ddls.medicaltraining.model.TrainingSchemeSegment;
import org.infai.ddls.medicaltraining.model.TrainingSite;
import org.infai.ddls.medicaltraining.repo.AuthorisationDAO;
import org.infai.ddls.medicaltraining.repo.PersonDAO;
import org.infai.ddls.medicaltraining.repo.TrainingDAO;
import org.infai.ddls.medicaltraining.repo.TrainingPositionDAO;
import org.infai.ddls.medicaltraining.repo.TrainingSchemeDAO;
import org.infai.ddls.medicaltraining.repo.TrainingSiteNewDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

@ControllerAdvice
public class DefaultControllerAdvice {
    private class AuthorisationFieldEditorSupport extends PropertyEditorSupport {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
	    setValue(authorisationDAO.getAuthorisationField(Long.valueOf(text)));
	}
    }

    private class AuthorisedDoctorEditorSupport extends PropertyEditorSupport {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
	    if (null == text || text.equals("")) {
		setValue(null);
	    } else {
		setValue(personDAO.getAuthorisedDoctor(Long.valueOf(text), true, true));
	    }
	}
    }

    private class EquipmentListEditorSupport extends PropertyEditorSupport {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
	    if (null == text || text.equals("")) {
		setValue(null);
	    } else {
		List<Equipment> equipments = new ArrayList<>();
		for (String equipmentUID : text.split(",")) {
		    equipments.add(trainingSiteDAO.getEquipment(Long.valueOf(equipmentUID)));
		}
		setValue(equipments);
	    }
	}
    }

    private class LocalDateEditorSupport extends PropertyEditorSupport {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
	    if (null == text || text.equals("")) {
		setValue(null);
	    } else {
		setValue(LocalDate.parse(text, DateTimeFormatter.ISO_DATE));
	    }
	}
    }

    private class MedicalFieldEditorSupport extends PropertyEditorSupport {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
	    if (null == text || text.equals("")) {
		setValue(null);
	    } else {
		setValue(trainingDAO.getMedicalField(Long.valueOf(text), true, true));
	    }
	}
    }

    private class SexEditorSupport extends PropertyEditorSupport {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
	    if (null == text || text.equals("")) {
		setValue(null);
	    } else {
		setValue(personDAO.getSex(Long.valueOf(text)));
	    }
	}
    }

    private class TitleEditorSupport extends PropertyEditorSupport {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
	    if (null == text || text.equals("")) {
		setValue(null);
	    } else {
		setValue(personDAO.getTitle(Long.valueOf(text)));
	    }
	}
    }

    private class TrainingPositionFieldEditorSupport extends PropertyEditorSupport {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
	    if (null == text || text.equals("")) {
		setValue(null);
	    } else {
		setValue(trainingPositionDAO.getTrainingPositionField(Long.valueOf(text)));
	    }
	}
    }

    private class TrainingSchemeEditorSupport extends PropertyEditorSupport {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
	    if (null == text || text.equals("")) {
		setValue(null);
	    } else {
		setValue(trainingSchemeDAO.getTrainingScheme(Long.valueOf(text), false));
	    }
	}
    }

    private class TrainingSchemeSegmentEditorSupport extends PropertyEditorSupport {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
	    if (null == text || text.equals("")) {
		setValue(null);
	    } else {
		setValue(trainingSchemeDAO.getTrainingSchemeSegment(Long.valueOf(text)));
	    }
	}
    }

    private class TrainingSiteEditorSupport extends PropertyEditorSupport {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
	    if (null == text || text.equals("")) {
		setValue(null);
	    } else {
		setValue(trainingSiteDAO.getTrainingSite(Long.valueOf(text), true, true));
	    }
	}
    }

    @Autowired
    private Validator validator;

    @Autowired
    private AuthorisationDAO authorisationDAO;
    @Autowired
    private TrainingDAO trainingDAO;
    @Autowired
    private TrainingSiteNewDAO trainingSiteDAO;
    @Autowired
    private PersonDAO personDAO;
    @Autowired
    private TrainingSchemeDAO trainingSchemeDAO;
    @Autowired
    private TrainingPositionDAO trainingPositionDAO;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
	binder.setAutoGrowCollectionLimit(500);
	binder.setValidator(validator);
	binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));

	binder.registerCustomEditor(Sex.class, new SexEditorSupport());
	binder.registerCustomEditor(Title.class, new TitleEditorSupport());

	binder.registerCustomEditor(LocalDate.class, new LocalDateEditorSupport());
	binder.registerCustomEditor(AuthorisationField.class, new AuthorisationFieldEditorSupport());
	binder.registerCustomEditor(MedicalField.class, new MedicalFieldEditorSupport());
	binder.registerCustomEditor(TrainingSite.class, new TrainingSiteEditorSupport());
	binder.registerCustomEditor(AuthorisedDoctor.class, new AuthorisedDoctorEditorSupport());
	binder.registerCustomEditor(TrainingPositionField.class, new TrainingPositionFieldEditorSupport());
	binder.registerCustomEditor(TrainingScheme.class, new TrainingSchemeEditorSupport());
	binder.registerCustomEditor(TrainingSchemeSegment.class, new TrainingSchemeSegmentEditorSupport());

	binder.registerCustomEditor(List.class, "equipments", new EquipmentListEditorSupport());

	// binder.registerCustomEditor(Boolean.class, "inpatient", new
	// CustomBooleanEditor(false));

    }
}
