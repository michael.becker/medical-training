package org.infai.ddls.medicaltraining.model;

import javax.persistence.MappedSuperclass;

import org.hibernate.envers.Audited;

@MappedSuperclass
@Audited
public abstract class Doctor extends Person {
}
