package org.infai.ddls.medicaltraining.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltraining.model.TrainingSite;

@SuppressWarnings("serial")
public class TrainingSiteSpecification extends AbstractTrainingAppEntitySpecification<TrainingSite, TrainingSiteFilter> {
    public TrainingSiteSpecification(TrainingSiteFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<TrainingSite> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	addPredicate(new MedicalFieldSpecification<TrainingSite>(filter.getMedicalField()).toPredicate(root, query, criteriaBuilder));
	addPredicate(new LocationSpecification<TrainingSite>(filter).toPredicate(root, query, criteriaBuilder));
	addPredicate(allowDepartments(filter.isAllowDepartments(), root, query, criteriaBuilder));
	addPredicate(allowParents(filter.isAllowParents(), root, query, criteriaBuilder));
    }

    private Predicate allowDepartments(boolean allowDepartments, Root<TrainingSite> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (allowDepartments) {
	    return criteriaBuilder.conjunction();
	} else {
	    return criteriaBuilder.isNull(root.get("parent"));
	}
    }

    private Predicate allowParents(boolean allowParents, Root<TrainingSite> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (allowParents) {
	    return criteriaBuilder.conjunction();
	} else {
	    return criteriaBuilder.isNotNull(root.get("parent"));
	}
    }

}
