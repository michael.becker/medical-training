package org.infai.ddls.medicaltraining.model.constraints;

import java.time.LocalDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.infai.ddls.medicaltraining.model.Authorisation;

public class AuthorisationBeginEndValidator implements ConstraintValidator<AuthorisationBeginBeforeEndConstraint, Authorisation> {

    @Override
    public boolean isValid(Authorisation value, ConstraintValidatorContext context) {
	LocalDate begin = value.getGrantedOn();
	LocalDate end = value.getGrantedTo();

	if (null == begin) {
	    return false;
	} else if (null == end) {
	    return true;
	} else if (end.isBefore(begin)) {
	    return false;
	} else {
	    return true;
	}
    }

}
