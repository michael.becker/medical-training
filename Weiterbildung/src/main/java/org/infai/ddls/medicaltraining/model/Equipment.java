package org.infai.ddls.medicaltraining.model;

import javax.persistence.Entity;

import org.hibernate.envers.Audited;

@Entity
@Audited
public class Equipment extends TrainingAppEntity {
    public Equipment() {

    }

    public Equipment(String name, String description) {
	setName(name);
	setDescription(description);
    }
}
