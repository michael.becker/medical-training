package org.infai.ddls.medicaltraining.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltraining.json.View;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

/**
 * Arzt in Weiterbildung
 *
 * @author Michael Becker
 *
 */
@Entity
@Audited
public class Registrar extends Doctor {
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonView(View.Summary.class)
    private LocalDate kompasSubscription;
    @ColumnDefault("false")
    private boolean lateralEntry;
    @ManyToMany
    private List<Region> preferredRegions;
    private LocalDate approvalVAHSTransmission;
    private LocalDate approvalKVSATransmission;

    public LocalDate getApprovalKVSATransmission() {
	return approvalKVSATransmission;
    }

    public LocalDate getApprovalVAHSTransmission() {
	return approvalVAHSTransmission;
    }

    public LocalDate getKompasSubscription() {
	return kompasSubscription;
    }

    public List<Region> getPreferredRegions() {
	return preferredRegions;
    }

    public boolean isLateralEntry() {
	return lateralEntry;
    }

    public void setApprovalKVSATransmission(LocalDate approvalKVSATransmission) {
	this.approvalKVSATransmission = approvalKVSATransmission;
    }

    public void setApprovalVAHSTransmission(LocalDate approvalVAHSTransmission) {
	this.approvalVAHSTransmission = approvalVAHSTransmission;
    }

    public void setKompasSubscription(LocalDate kompasSubscription) {
	this.kompasSubscription = kompasSubscription;
    }

    public void setLateralEntry(boolean lateralEntry) {
	this.lateralEntry = lateralEntry;
    }

    public void setPreferredRegions(List<Region> preferredRegions) {
	this.preferredRegions = preferredRegions;
    }
}
