package org.infai.ddls.medicaltraining.repo;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.transaction.Transactional;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;
import org.infai.ddls.medicaltraining.model.RotationPlanSegment;
import org.infai.ddls.medicaltraining.model.TrainingPositionField;
import org.infai.ddls.medicaltraining.spec.AuthorisationFieldFilter;
import org.infai.ddls.medicaltraining.spec.AuthorisiationFieldSpecification;
import org.infai.ddls.medicaltraining.spec.AuthorisiationFieldSpecificationNew;
import org.infai.ddls.medicaltraining.spec.RotationPlanFilter;
import org.infai.ddls.medicaltraining.spec.TrainingPositionFieldFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;

@Component
public class AuthorisationDAO extends AbstractTrainingAppDAO {
    @Autowired
    private AuthorisationFieldRepository authorisationFieldRepository;
    @Autowired
    private TrainingPositionDAO trainingPositionDAO;
    @Autowired
    private RotationPlanDAO rotationPlanDAO;

    @Transactional
    public boolean delete(AuthorisationField authorisationField) {
	return delete(authorisationField, false, false);
    }

    @Transactional
    public boolean delete(AuthorisationField authorisationField, boolean deleteIfTrainingPositionIsPresent) {
	return delete(authorisationField, true, false);
    }

    public boolean delete(AuthorisationField authorisationField, boolean deleteIfTrainingPositionIsPresent, boolean deleteIfTrainingPositionIsUsed) {
	TrainingPositionFieldFilter trainingPositionFilter = new TrainingPositionFieldFilter();
	trainingPositionFilter.setAuthorisationField(authorisationField);
	List<TrainingPositionField> trainingPositions = trainingPositionDAO.getTrainingPositionsField(trainingPositionFilter, null);
	List<RotationPlanSegment> allRotationPlanSegments = new ArrayList<>();

	if (!trainingPositions.isEmpty()) {
	    if (deleteIfTrainingPositionIsPresent) {
		for (TrainingPositionField trainingPosition : trainingPositions) {
		    RotationPlanFilter rotationPlanFilter = new RotationPlanFilter();
		    rotationPlanFilter.setTrainingPositionField(trainingPosition);
		    List<RotationPlanSegment> rotationPlanSegments = rotationPlanDAO.getRotationPlanSegments(rotationPlanFilter, null);
		    if (!rotationPlanSegments.isEmpty()) {
			if (deleteIfTrainingPositionIsUsed) {
			    allRotationPlanSegments.addAll(rotationPlanSegments);
			} else {
			    logger.warn("Attempted to delete TrainingPositionField " + trainingPosition + " but has assigned RotationPlanSegments!");
			    return false;
			}
		    }
		}
	    } else {
		logger.warn("Attempted to delete AuthorisationField " + authorisationField + " but has TrainingPositions!");
		return false;
	    }
	}

	for (RotationPlanSegment rotationPlanSegment : allRotationPlanSegments) {
	    rotationPlanDAO.delete(rotationPlanSegment);
	}

	for (TrainingPositionField trainingPosition : trainingPositions) {
	    trainingPositionDAO.delete(trainingPosition);
	}

	authorisationFieldRepository.delete(authorisationField);
	return true;
    }

    @Transactional
    public boolean delete(List<AuthorisationField> authorisationFields) {
	authorisationFieldRepository.deleteAll(authorisationFields);

	return true;
    }

    @Transactional
    public AuthorisationField getAuthorisationField(Long uid) {
	try {
	    AuthorisationField authorisation = authorisationFieldRepository.findById(uid).get();
	    System.out.println(authorisation.getTrainingSite());

	    return authorisation;
	} catch (NoSuchElementException e) {
	    logger.warn("Attempted to load AuthorisationField with UID " + uid + " but no value is present!");
	    return null;
	}
    }

    // TODO: Momentan wird die Ausstattung immer mitgeladen. Das noch optional
    // machen.
    @Transactional
    public List<AuthorisationField> getAuthorisationFields(Sort sort) {
	if (null == sort || sort.isUnsorted()) {
	    sort = new Sort(Direction.ASC, "authorisedDoctor.lastName");
	}

	List<AuthorisationField> authorisations = Lists.newArrayList(authorisationFieldRepository.findAll(sort));

	for (AuthorisationField authorisation : authorisations) {
	    authorisation.getTrainingSite().getEquipments().size();
	}

	return authorisations;
    }

    @Transactional
    public List<AuthorisationField> getAuthorisations(AuthorisationFieldFilter filter, Sort sort, boolean loadTrainingSites, boolean loadAdditionalFields, boolean loadMedicalFieldChildren) {
	List<AuthorisationField> authorisations = authorisationFieldRepository.findAll(new AuthorisiationFieldSpecificationNew(filter), evaluateSort(sort));

	return authorisations;
    }

    @Transactional
    public List<AuthorisationField> getAuthorisationsField(AuthorisedDoctor authorisedDoctor) {
	if (null == authorisedDoctor.getUid()) {
	    return new ArrayList<>(1);
	} else {
	    Specification<AuthorisationField> spec = AuthorisiationFieldSpecification.hasAuthorisedDoctor(authorisedDoctor);

	    return authorisationFieldRepository.findAll(spec);
	}
    }

    public AuthorisationField save(AuthorisationField authorisationField) {
	return authorisationFieldRepository.save(authorisationField);
    }
}
