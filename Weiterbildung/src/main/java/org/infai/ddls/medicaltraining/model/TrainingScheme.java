package org.infai.ddls.medicaltraining.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.Valid;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * Stellt eine Weiterbildung mit Abschnitten entsprechend einer gültigen
 * Weiterbildungsordnung dar. Jede Weiterbildung besteht aus einer Reihe von
 * Weiterbildungsabschnitten ({@link TrainingSchemeSegment}).
 * 
 * @author mbecker
 *
 */
@Entity
@Audited
public class TrainingScheme extends TrainingAppEntity {
    @Valid
    @OneToMany(mappedBy = "trainingScheme")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "uid", scope = TrainingSchemeSegment.class)
    private List<TrainingSchemeSegment> trainingSchemeSegments;

    public TrainingScheme() {
	this.trainingSchemeSegments = new ArrayList<>();
    }

    public TrainingScheme(String name, String description, TrainingSchemeSegment... trainingSegments) {
	setName(name);
	setDescription(description);

	this.trainingSchemeSegments = new ArrayList<>();

	for (TrainingSchemeSegment trainingSegment : trainingSegments) {
	    this.trainingSchemeSegments.add(trainingSegment);
	}
    }

    public void addTrainingSegment(TrainingSchemeSegment trainingSegment) {
	this.trainingSchemeSegments.add(trainingSegment);
    }

    @Transient
    @JsonIgnore
    public int getDuration() {
	int duration = 0;
	for (TrainingSchemeSegment trainingSegment : trainingSchemeSegments) {
	    duration += trainingSegment.getDuration();
	}

	return duration;
    }

    public List<TrainingSchemeSegment> getTrainingSchemeSegments() {
	return trainingSchemeSegments;
    }

    public void setTrainingSchemeSegments(List<TrainingSchemeSegment> trainingSchemeSegments) {
	this.trainingSchemeSegments = trainingSchemeSegments;
    }
}
