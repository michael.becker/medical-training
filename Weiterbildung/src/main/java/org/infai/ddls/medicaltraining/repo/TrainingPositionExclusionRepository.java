package org.infai.ddls.medicaltraining.repo;

import org.infai.ddls.medicaltraining.model.TrainingPositionExclusion;

public interface TrainingPositionExclusionRepository extends TrainingAppEntityRepository<TrainingPositionExclusion> {

}
