package org.infai.ddls.medicaltraining.spec;

import org.infai.ddls.medicaltraining.model.TrainingSite;

public class AuthorisedDoctorFilter extends PersonFilter {
    private Integer lanr;
    private String fullname;
    private TrainingSite trainingSite;

    public String getFullname() {
	return fullname;
    }

    public Integer getLanr() {
	return lanr;
    }

    public TrainingSite getTrainingSite() {
	return trainingSite;
    }

    public void setFullname(String fullname) {
	this.fullname = fullname;
    }

    public void setLanr(Integer lanr) {
	this.lanr = lanr;
    }

    public void setTrainingSite(TrainingSite trainingSite) {
	this.trainingSite = trainingSite;
    }
}
