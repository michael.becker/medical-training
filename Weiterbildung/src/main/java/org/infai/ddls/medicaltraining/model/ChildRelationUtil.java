package org.infai.ddls.medicaltraining.model;

/**
 * Stellt Methoden für die Arbeit mit {@link IHasChildren}-Objekten bereit.
 * 
 * @author Michael Becker
 *
 */
public class ChildRelationUtil {
    /**
     * Prüft ob das gegebene value-Element in einer Kind-Relation mit dem gegebenen
     * parent-Element steht.
     * 
     * @param value
     *            das Element, für das geprüft werden soll, ob es (indirektes) Kind
     *            von parent ist
     * @param parent
     *            das Elternelement
     * @return true, wenn value ein (indirekts) Kind von parent ist, ansonsten false
     */
    @SuppressWarnings("unchecked")
    public static boolean isChildOf(IHasChildren<? extends TrainingAppEntity> value, IHasChildren<? extends TrainingAppEntity> parent) {
	if (null == value.getParent()) {
	    return false;
	} else if (value.getParent().equals(parent)) {
	    return true;
	} else {
	    // TODO eventuell lässt sich das noch schöner handeln als mit einem solchen Cast
	    return isChild((IHasChildren<? extends TrainingAppEntity>) value.getParent(), parent);
	}
    }

    @SuppressWarnings("unchecked")
    private static boolean isChild(IHasChildren<? extends TrainingAppEntity> value, IHasChildren<? extends TrainingAppEntity> parent) {
	if (null == value.getParent()) {
	    return false;
	} else if (value.getParent().equals(parent)) {
	    return true;
	} else {
	    return isChild((IHasChildren<? extends TrainingAppEntity>) value.getParent(), parent);
	}
    }
}
