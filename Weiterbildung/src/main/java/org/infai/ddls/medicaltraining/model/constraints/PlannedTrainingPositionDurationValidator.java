package org.infai.ddls.medicaltraining.model.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.infai.ddls.medicaltraining.model.RotationPlanSegment;
import org.infai.ddls.medicaltraining.spec.PlannedTrainingPositionFieldFilter;

public class PlannedTrainingPositionDurationValidator implements ConstraintValidator<PlannedTrainingPositionDurationConstraint, PlannedTrainingPositionFieldFilter> {
    @Override
    public boolean isValid(PlannedTrainingPositionFieldFilter value, ConstraintValidatorContext context) {
	RotationPlanSegment segment = RotationPlanSegment.create(value);

	return new RotationPlanSegmentDurationValidator().isValid(segment, context);
    }

}
