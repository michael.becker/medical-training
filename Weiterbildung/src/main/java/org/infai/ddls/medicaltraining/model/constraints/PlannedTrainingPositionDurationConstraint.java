package org.infai.ddls.medicaltraining.model.constraints;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PlannedTrainingPositionDurationValidator.class)
public @interface PlannedTrainingPositionDurationConstraint {
    Class<?>[] groups() default {};

    String message() default "{plannedtrainingposition.durationtooshort}";

    Class<? extends Payload>[] payload() default {};
}
