package org.infai.ddls.medicaltraining.security.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long uid;
    @NotBlank
    @Column(nullable = false)
    private String name;

    public String getName() {
	return name;
    }

    public Long getUid() {
	return uid;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setUid(Long uid) {
	this.uid = uid;
    }
}
