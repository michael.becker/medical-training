package org.infai.ddls.medicaltraining.logging;

import javax.persistence.Entity;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;

@SuppressWarnings("serial")
@Entity
@RevisionEntity(CustomRevisionListener.class)
public class UserRevision extends DefaultRevisionEntity {
    private String userName;

    public String getUserName() {
	return userName;
    }

    public void setUserName(String userName) {
	this.userName = userName;
    }
}
