package org.infai.ddls.medicaltraining.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.infai.ddls.medicaltraining.repo.TrainingAppEntityRepository;
import org.infai.ddls.medicaltraining.security.repo.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(basePackageClasses = { TrainingAppEntityRepository.class, UserRepository.class })
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
public class DatabaseConfiguration {
    @Value("${spring.datasource.mysql.driverClassName}")
    private String driverClassName;
    @Value("${spring.datasource.mysql.url}")
    private String url;
    @Value("${spring.datasource.mysql.username}")
    private String username;
    @Value("${spring.datasource.mysql.password}")
    private String password;
    @Value("${hibernate.dialect}")
    private String hibernateDialect;
    @Value("${hibernate.hbm2ddl.auto}")
    private String hibernateHbm2DDLAuto;
    @Value("${hibernate.ejb.naming_strategy}")
    private String hibernateEJBNamingStrategy;
    @Value("${hibernate.show_sql}")
    private String hibernateShowSQL;
    @Value("${hibernate.format_sql}")
    private String hibernateFormatSQL;
    @Value("${hibernate.hbm2ddl.import_files}")
    private String hibernateHbm2DDLImportFiles;
    @Value("${hibernate.jdbc.time_zone}")
    private String hibernateJDBCTimeZone;

    @Bean
    public DataSource dataSource() {
	BasicDataSource dataSource = new BasicDataSource();
	dataSource.setDriverClassName(driverClassName);
	dataSource.setUrl(url);
	dataSource.setUsername(username);
	dataSource.setPassword(password);

	return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
	LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
	em.setDataSource(dataSource());
	em.setJpaVendorAdapter(hibernateJpaVendorAdapter());
	em.setPackagesToScan(new String[] { "org.infai.ddls.medicaltraining.model", "org.infai.ddls.medicaltraining.logging", "org.infai.ddls.medicaltraining.security.model" });
	em.setJpaProperties(additionalProperties());

	return em;
    }

    @Bean
    public HibernateJpaVendorAdapter hibernateJpaVendorAdapter() {
	return new HibernateJpaVendorAdapter();
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory emf) {
	JpaTransactionManager tm = new JpaTransactionManager();
	tm.setEntityManagerFactory(emf);

	return tm;
    }

    protected Properties additionalProperties() {
	Properties properties = new Properties();
	properties.put("hibernate.dialect", hibernateDialect);
	properties.put("hibernate.hbm2ddl.auto", hibernateHbm2DDLAuto);
	properties.put("hibernate.ejb.naming_strategy", hibernateEJBNamingStrategy);
	properties.put("hibernate.show_sql", hibernateShowSQL);
	properties.put("hibernate.format_sql", hibernateFormatSQL);
	properties.put("hibernate.hbm2ddl.import_files", hibernateHbm2DDLImportFiles);
	properties.put("hibernate.jdbc.time_zone", hibernateJDBCTimeZone);

	return properties;
    }
}