package org.infai.ddls.medicaltraining.repo;

import org.infai.ddls.medicaltraining.model.Registrar;
import org.springframework.stereotype.Repository;

@Repository
interface RegistrarRepository extends TrainingAppEntityRepository<Registrar> {

}
