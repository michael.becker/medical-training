package org.infai.ddls.medicaltraining.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;
import org.infai.ddls.medicaltraining.model.TrainingSite;

@SuppressWarnings("serial")
public class AuthorisiationFieldSpecificationNew extends AbstractTrainingAppEntitySpecification<AuthorisationField, AuthorisationFieldFilter> {
    public AuthorisiationFieldSpecificationNew(AuthorisationFieldFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<AuthorisationField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	if (null != filter.getTrainingSite()) {
	    addPredicate(hasTrainingSite(filter.getTrainingSite(), root, query, criteriaBuilder));
	}
	if (null != filter.getAuthorisedDoctor()) {
	    addPredicate(hasAuthorisedDoctor(filter.getAuthorisedDoctor(), root, query, criteriaBuilder));
	}
    }

    private Predicate hasAuthorisedDoctor(AuthorisedDoctor authorisedDoctor, Root<AuthorisationField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get("authorisedDoctor"), authorisedDoctor);
    }

    private Predicate hasTrainingSite(TrainingSite trainingSite, Root<AuthorisationField> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return criteriaBuilder.equal(root.get("trainingSite"), trainingSite);
    }

}
