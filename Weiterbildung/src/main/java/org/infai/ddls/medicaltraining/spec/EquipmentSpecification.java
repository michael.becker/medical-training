package org.infai.ddls.medicaltraining.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltraining.model.Equipment;

@SuppressWarnings("serial")
public class EquipmentSpecification extends AbstractTrainingAppEntitySpecification<Equipment, EquipmentFilter> {
    public EquipmentSpecification(EquipmentFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<Equipment> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
    }
}
