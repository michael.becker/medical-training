package org.infai.ddls.medicaltraining.spec;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltraining.model.Location;
import org.infai.ddls.medicaltraining.model.TrainingAppEntity;
import org.springframework.data.jpa.domain.Specification;

/**
 * Erstellt ein Prädikat, um {@link Location} anhand des gegebenen
 * {@link LocationFilter} zu selektieren.
 * 
 * @author Michael Becker
 *
 * @param <T>
 */
@SuppressWarnings("serial")
public class LocationSpecification<T extends TrainingAppEntity> implements Specification<T> {
    public static final String JPA_PARAMETER_NAME_LATITUDE = "latitude";
    public static final String JPA_PARAMETER_NAME_LONGITUDE = "longitude";
    public static final String SQL_FUNCTION_NAME_DISTANCE_CALC = "lat_lng_distance";
    public static final String ATTRIBUTE_LATITUDE = "latitude";
    public static final String ATTRIBUTE_LONGITUDE = "longitude";

    private LocationFilter locationFilter;

    public LocationSpecification(LocationFilter locationFilter) {
	this.locationFilter = locationFilter;
    }

    public Predicate toPredicate(Path<T> latitudePath, Path<T> longitudePath, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	List<Predicate> predicates = new ArrayList<>();

	if (!locationFilter.getZipCode().equals(LocationFilter.DEFAULT_ZIP_CODE)) {
	    ParameterExpression<Double> latitude = criteriaBuilder.parameter(Double.class, JPA_PARAMETER_NAME_LATITUDE);
	    ParameterExpression<Double> longitude = criteriaBuilder.parameter(Double.class, JPA_PARAMETER_NAME_LONGITUDE);

	    predicates.add(criteriaBuilder.lessThanOrEqualTo(criteriaBuilder.function(SQL_FUNCTION_NAME_DISTANCE_CALC, Integer.class, latitudePath, longitudePath, latitude, longitude), locationFilter.getDistance()));
	}

	return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	return toPredicate(root.get(JPA_PARAMETER_NAME_LATITUDE), root.get(JPA_PARAMETER_NAME_LONGITUDE), query, criteriaBuilder);
    }

}
