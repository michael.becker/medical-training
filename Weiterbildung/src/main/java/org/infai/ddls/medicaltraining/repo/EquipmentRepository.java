package org.infai.ddls.medicaltraining.repo;

import org.infai.ddls.medicaltraining.model.Equipment;

public interface EquipmentRepository extends TrainingAppEntityRepository<Equipment> {

}
