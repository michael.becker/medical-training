package org.infai.ddls.medicaltraining.config;

import javax.servlet.FilterRegistration;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.DispatcherServlet;

public class ApplicationInitializer implements WebApplicationInitializer {
    private String TMP_FOLDER = "/tmp";
    private int MAX_UPLOAD_SIZE = 5 * 1024 * 1024;

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
	AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
	context.register(ApplicationConfiguration.class);

	servletContext.addListener(new ContextLoaderListener(context));
	ServletRegistration.Dynamic medicalTrainingServlet = servletContext.addServlet("medicaltraining", new DispatcherServlet(context));

	medicalTrainingServlet.setLoadOnStartup(1);
	medicalTrainingServlet.addMapping("/");

	MultipartConfigElement multipartConfigElement = new MultipartConfigElement(TMP_FOLDER, MAX_UPLOAD_SIZE, MAX_UPLOAD_SIZE * 2, MAX_UPLOAD_SIZE / 2);
	medicalTrainingServlet.setMultipartConfig(multipartConfigElement);

	FilterRegistration.Dynamic encodingFilter = servletContext.addFilter("encoding-filter", new CharacterEncodingFilter("UTF-8", true));
	encodingFilter.addMappingForUrlPatterns(null, true, "/*");

	FilterRegistration.Dynamic hiddenHTTPMethodFilter = servletContext.addFilter("HiddenHttpMethodFilter", new HiddenHttpMethodFilter());
	hiddenHTTPMethodFilter.addMappingForServletNames(null, true, "medicaltraining");

	FilterRegistration.Dynamic springSecurityFilterChain = servletContext.addFilter("springSecurityFilterChain", new DelegatingFilterProxy());
	springSecurityFilterChain.addMappingForUrlPatterns(null, true, "/*");
    }
}
