package org.infai.ddls.medicaltraining.repo;

import org.infai.ddls.medicaltraining.model.Title;
import org.springframework.stereotype.Repository;

@Repository
public interface TitleRepository extends TrainingAppEntityRepository<Title> {

}
