package org.infai.ddls.medicaltraining.repo;

import org.infai.ddls.medicaltraining.model.RotationPlanSegment;
import org.springframework.stereotype.Repository;

@Repository
interface RotationPlanSegmentRepository extends TrainingAppEntityRepository<RotationPlanSegment> {

}
