package org.infai.ddls.medicaltraining.model;

import java.beans.Transient;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.envers.Audited;
import org.infai.ddls.medicaltraining.json.View;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Audited
public abstract class Person extends TrainingAppEntity {
    @Past
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate birthDate;
    @NotBlank
    @Column(nullable = false)
    @JsonView(View.Summary.class)
    private String lastName;
    @NotNull
    @Valid
    @Embedded
    @Column(nullable = false)
    private Address address;
    @Valid
    @Embedded
    private ContactInfo contactInfo;
    @Valid
    @ManyToOne
    private Sex sex;
    @Valid
    @ManyToOne
    private Title title;
    @OneToMany(mappedBy = "contact")
    private List<Contact> contacts;

    public Person() {
	this.address = new Address();
	this.contactInfo = new ContactInfo();
    }

    public Address getAddress() {
	return address;
    }

    public LocalDate getBirthDate() {
	return birthDate;
    }

    public ContactInfo getContactInfo() {
	return contactInfo;
    }

    public List<Contact> getContacts() {
	return contacts;
    }

    @Transient
    public String getFullName() {
	return getName() + " " + getLastName();
    }

    public String getLastName() {
	return lastName;
    }

    public Sex getSex() {
	return sex;
    }

    public Title getTitle() {
	return title;
    }

    public void setAddress(Address address) {
	this.address = address;
    }

    public void setBirthDate(LocalDate birthDate) {
	this.birthDate = birthDate;
    }

    public void setContactInfo(ContactInfo contactInfo) {
	this.contactInfo = contactInfo;
    }

    public void setContacts(List<Contact> contacts) {
	this.contacts = contacts;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public void setSex(Sex sex) {
	this.sex = sex;
    }

    public void setTitle(Title title) {
	this.title = title;
    }

    @Override
    public String toString() {
	return getFullName();
    }
}
