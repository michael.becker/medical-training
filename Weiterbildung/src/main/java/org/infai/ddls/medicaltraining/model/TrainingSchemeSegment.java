package org.infai.ddls.medicaltraining.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Stellt einen Abschnitt einer durch eine Weiterbildungsordnung definierten
 * Weiterbildung ({@link TrainingScheme }) dar.
 * 
 * @author mbecker
 *
 */
@Entity
@Audited
public class TrainingSchemeSegment extends TrainingAppEntity {
    @NotNull
    @Min(3)
    private int duration;
    @ManyToOne
    private MedicalField medicalField;
    private boolean outpatient;
    private boolean inpatient;
    @ManyToOne
    @JsonIgnore
    private TrainingScheme trainingScheme;

    public TrainingSchemeSegment() {
	this.duration = 3;
	this.inpatient = false;
	this.outpatient = false;

	setName("n/a");
    }

    public TrainingSchemeSegment(int duration, MedicalField medicalField, boolean outpatient, boolean inpatient) {
	setDuration(duration);
	setMedicalField(medicalField);
	setOutpatient(outpatient);
	setInpatient(inpatient);
	setName(medicalField.getName());
    }

    public int getDuration() {
	return duration;
    }

    public MedicalField getMedicalField() {
	return medicalField;
    }

    public TrainingScheme getTrainingScheme() {
	return trainingScheme;
    }

    public boolean isInpatient() {
	return inpatient;
    }

    public boolean isOutpatient() {
	return outpatient;
    }

    public void setDuration(int duration) {
	this.duration = duration;
    }

    public void setInpatient(boolean inpatient) {
	this.inpatient = inpatient;
    }

    public void setMedicalField(MedicalField medicalField) {
	this.medicalField = medicalField;
    }

    public void setOutpatient(boolean outpatient) {
	this.outpatient = outpatient;
    }

    public void setTrainingScheme(TrainingScheme trainingScheme) {
	this.trainingScheme = trainingScheme;
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append(duration).append(" Monate ").append(medicalField);

	if (inpatient && !outpatient) {
	    builder.append(" stationär");
	} else if (!inpatient && outpatient) {
	    builder.append(" ambulant");
	} else if (inpatient && outpatient) {
	    builder.append(" ambulant oder stationär");
	}

	return builder.toString();
    }
}
