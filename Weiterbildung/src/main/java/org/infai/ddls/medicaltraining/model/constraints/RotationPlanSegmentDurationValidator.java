package org.infai.ddls.medicaltraining.model.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.infai.ddls.medicaltraining.model.RotationPlanSegment;

public class RotationPlanSegmentDurationValidator implements ConstraintValidator<RotationPlanSegmentMinimalDurationConstraint, RotationPlanSegment> {

    @Override
    public boolean isValid(RotationPlanSegment rotationPlanSegment, ConstraintValidatorContext context) {
	if (rotationPlanSegment.getNormalisedDuration() < 3) {
	    return false;
	} else {
	    return true;
	}
    }
}
