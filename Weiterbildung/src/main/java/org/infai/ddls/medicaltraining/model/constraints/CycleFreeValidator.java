package org.infai.ddls.medicaltraining.model.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.infai.ddls.medicaltraining.model.ChildRelationUtil;
import org.infai.ddls.medicaltraining.model.IHasChildren;
import org.infai.ddls.medicaltraining.model.TrainingAppEntity;

public class CycleFreeValidator implements ConstraintValidator<CycleFreeConstraint, IHasChildren<? extends TrainingAppEntity>> {
    @Override
    public boolean isValid(IHasChildren<? extends TrainingAppEntity> value, ConstraintValidatorContext context) {
	if (null == value.getParent()) {
	    return true;
	} else if (ChildRelationUtil.isChildOf(value, value)) {
	    return false;
	} else {
	    return true;
	}
    }
}