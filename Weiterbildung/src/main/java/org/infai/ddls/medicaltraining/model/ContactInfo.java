package org.infai.ddls.medicaltraining.model;

import javax.persistence.Embeddable;
import javax.validation.constraints.Email;

@Embeddable
public class ContactInfo {
    @Email
    private String email;
    private String phoneNumber;
    private String phoneNumber2;
    private String faxNumber;
    private String web;

    public String getEmail() {
	return email;
    }

    public String getFaxNumber() {
	return faxNumber;
    }

    public String getPhoneNumber() {
	return phoneNumber;
    }

    public String getPhoneNumber2() {
	return phoneNumber2;
    }

    public String getWeb() {
	return web;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public void setFaxNumber(String faxNumber) {
	this.faxNumber = faxNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
	this.phoneNumber = phoneNumber;
    }

    public void setPhoneNumber2(String phoneNumber2) {
	this.phoneNumber2 = phoneNumber2;
    }

    public void setWeb(String web) {
	this.web = web;
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	if (null != email) {
	    builder.append("\nE-Mail: ").append(email);
	}
	if (null != phoneNumber) {
	    builder.append("\nTel: ").append(phoneNumber);
	}
	if (null != phoneNumber2) {
	    builder.append("\nTel2: ").append(phoneNumber2);
	}
	if (null != faxNumber) {
	    builder.append("\nFax: ").append(faxNumber);
	}

	return builder.toString();
    }
}
