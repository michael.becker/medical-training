package org.infai.ddls.medicaltraining.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.infai.ddls.medicaltraining.model.constraints.AddressExistsConstraint;

@Embeddable
@AddressExistsConstraint
public class Address {
    @NotBlank
    @Column(nullable = false)
    private String street;
    @NotBlank
    @Column(nullable = false)
    @Pattern(regexp = "^[0-9]{5}$")
    private String zipCode;
    @NotBlank
    @Column(nullable = false)
    private String city;
    // private double latitude;
    // private double longitude;

    public String getCity() {
	return city;
    }

    // public double getLatitude() {
    // return latitude;
    // }
    //
    // public double getLongitude() {
    // return longitude;
    // }

    public String getStreet() {
	return street;
    }

    public String getZipCode() {
	return zipCode;
    }

    public void setCity(String city) {
	this.city = city;
    }

    public void setStreet(String street) {
	this.street = street;
    }

    public void setZipCode(String zipCode) {
	this.zipCode = zipCode;
    }

    @Override
    public String toString() {
	return street + ", " + zipCode + "/" + city;
    }

    protected void updateLatLng() {

    }
}
