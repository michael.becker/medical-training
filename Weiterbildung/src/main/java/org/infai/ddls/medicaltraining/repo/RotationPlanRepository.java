package org.infai.ddls.medicaltraining.repo;

import org.infai.ddls.medicaltraining.model.Registrar;
import org.infai.ddls.medicaltraining.model.RotationPlan;
import org.springframework.stereotype.Repository;

@Repository
interface RotationPlanRepository extends TrainingAppEntityRepository<RotationPlan> {
    public Iterable<RotationPlan> findAllByRegistrar(Registrar registrar);
}
