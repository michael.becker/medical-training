package org.infai.ddls.medicaltraining.repo;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.infai.ddls.medicaltraining.model.RotationPlanSegment;
import org.infai.ddls.medicaltraining.model.TrainingPositionExclusion;
import org.infai.ddls.medicaltraining.model.TrainingPositionField;
import org.infai.ddls.medicaltraining.spec.LocationSpecification;
import org.infai.ddls.medicaltraining.spec.PlannedTrainingPositionFieldFilter;
import org.infai.ddls.medicaltraining.spec.TrainingPositionFieldFilter;
import org.infai.ddls.medicaltraining.spec.TrainingPositionFieldSpecifications;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;

@Component
public class TrainingPositionDAO extends AbstractTrainingAppDAO {
    @Autowired
    private TrainingPositionFieldRepository trainingPositionFieldRepository;
    @Autowired
    private TrainingDAO trainingDAO;
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private TrainingPositionExclusionRepository trainingPositionExclusionRepository;
    @Autowired
    private RotationPlanDAO rotationPlanDAO;
    @Autowired
    private TrainingSiteNewDAO trainingSiteDAO;
    @Autowired
    private AuthorisationDAO authorisationDAO;
    @Autowired
    private PersonDAO personDAO;

    /**
     * Deletes the given training position. If there exist rotation plan segments
     * with this training position, the position will not be deleted.
     * 
     * @param trainingPositionField
     *            the position to delete
     * @return true, if the position was deleted; false, if there eixst rotation
     *         plan segments with this position and it, thus, was not deleted
     */
    @Transactional
    public boolean delete(TrainingPositionField trainingPositionField) {
	return delete(trainingPositionField, false);
    }

    /**
     * Deletes the given training position.
     * 
     * @param trainingPositionField
     *            the position to delete
     * @param deleteIfRotationPlanSegmentsAssigned
     *            specifies if the position is to be deleted even when rotation plan
     *            segments exist that are assigned to this position
     * @return true, if the position was deleted; false, if there exist rotation
     *         plan segments with this position and the respective parameter was not
     *         set
     */
    public boolean delete(TrainingPositionField trainingPositionField, boolean deleteIfRotationPlanSegmentsAssigned) {
	List<RotationPlanSegment> rotationPlanSegments = rotationPlanDAO.getRotationPlanSegments(trainingPositionField, null);
	System.out.println("zugewiesene segments: " + rotationPlanSegments);
	if (!rotationPlanSegments.isEmpty()) {
	    if (deleteIfRotationPlanSegmentsAssigned) {
		for (RotationPlanSegment rotationPlanSegment : rotationPlanSegments) {
		    rotationPlanDAO.delete(rotationPlanSegment);
		}
	    } else {
		logger.warn("Attempted to delete TrainingPosition " + trainingPositionField + " but has assigned RotationPlanSegments");
		return false;
	    }
	}

	trainingPositionFieldRepository.delete(trainingPositionField);

	return true;
    }

    @Transactional
    public TrainingPositionField getTrainingPositionField(Long uid) {
	return getTrainingPositionField(uid, false);
    }

    @Transactional
    public TrainingPositionField getTrainingPositionField(Long uid, boolean loadExclusions) {
	try {
	    TrainingPositionField position = trainingPositionFieldRepository.findById(uid).get();

	    if (loadExclusions) {
		position.getExclusions().size();
	    }

	    position.getAuthorisationField().getTrainingSite().getChildren().size();

	    // TODO: Momentan wird die Ausrüstung immer mitgeladen. Das noch optional
	    // machen.
	    position.getAuthorisationField().getTrainingSite().getEquipments().size();

	    return position;
	} catch (NoSuchElementException e) {
	    logger.warn("Attempted to load TrainingPositionField with UID " + uid + " but no value is present!");
	    return null;
	}
    }

    @Transactional
    public List<TrainingPositionField> getTrainingPositionsField(PlannedTrainingPositionFieldFilter filter, Sort sort) {
	TrainingPositionFieldFilter trainingPositionFieldFilter = new TrainingPositionFieldFilter();
	trainingPositionFieldFilter.setAvailableFrom(filter.getAvailableFrom());
	trainingPositionFieldFilter.setAvailableUntil(filter.getAvailableUntil());
	trainingPositionFieldFilter.setFullTimeEquivalent(filter.getFullTimeEquivalent());
	trainingPositionFieldFilter.setMedicalField(filter.getMedicalField());

	return getTrainingPositionsField(trainingPositionFieldFilter, sort);
    }

    @Transactional
    public List<TrainingPositionField> getTrainingPositionsField(Sort sort, boolean loadExclusions) {
	List<TrainingPositionField> positions = new ArrayList<>();
	if (null == sort) {
	    positions = Lists.newArrayList(trainingPositionFieldRepository.findAll());
	} else {
	    positions = Lists.newArrayList(trainingPositionFieldRepository.findAll(sort));
	}

	if (loadExclusions) {
	    initExclusions(positions);
	}

	return positions;
    }

    @Transactional
    public List<TrainingPositionField> getTrainingPositionsField(TrainingPositionFieldFilter filter, Sort sort) {
	return getTrainingPositionsField(filter, sort, false);
    }

    @Transactional
    public List<TrainingPositionField> getTrainingPositionsField(TrainingPositionFieldFilter filter, Sort sort, boolean loadExclusions) {
	CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
	CriteriaQuery<TrainingPositionField> criteriaQuery = criteriaBuilder.createQuery(TrainingPositionField.class);
	Root<TrainingPositionField> root = criteriaQuery.from(TrainingPositionField.class);

	if (null != filter.getMedicalField()) {
	    // das ist notwendig, damit im Filter rekursiv gesucht werden kann -> hier
	    // werden die Kind-Gebiete mitgeladen
	    filter.setMedicalField(trainingDAO.getMedicalField(filter.getMedicalField().getUid(), true, true));
	}

	criteriaQuery.where(new TrainingPositionFieldSpecifications(filter).toPredicate(root, criteriaQuery, criteriaBuilder));
	Query query = entityManager.createQuery(criteriaQuery);

	if (0 != filter.getLatitude()) {
	    System.out.println("searching for " + filter.getLatitude() + ", " + filter.getLongitude());
	    query.setParameter(LocationSpecification.JPA_PARAMETER_NAME_LATITUDE, filter.getLatitude());
	    query.setParameter(LocationSpecification.JPA_PARAMETER_NAME_LONGITUDE, filter.getLongitude());
	}

	@SuppressWarnings("unchecked")
	List<TrainingPositionField> positions = query.getResultList();

	if (loadExclusions) {
	    initExclusions(positions);
	}

	if (null != filter.getAvailableFrom() && null != filter.getAvailableUntil()) {
	    initExclusions(positions);
	    for (TrainingPositionField position : positions) {
		List<RotationPlanSegment> rotationPlanSegments = rotationPlanDAO.getRotationPlanSegments(position, DEFAULT_SORT);
		position.setAvailability(position.getAvailability(filter.getAvailableFrom(), filter.getAvailableUntil(), rotationPlanSegments));
	    }
	}

	return positions;
    }

    @Transactional
    public TrainingPositionField save(TrainingPositionField trainingPositionField) {
	for (TrainingPositionExclusion exclusion : trainingPositionField.getExclusions()) {
	    trainingPositionExclusionRepository.save(exclusion);
	}

	return trainingPositionFieldRepository.save(trainingPositionField);
    }

    @Transactional
    public TrainingPositionField save(TrainingPositionField trainingPosition, boolean saveAuthorised, boolean saveAuthorisation, boolean saveTrainingSite, boolean saveTrainingSiteParent) {
	if (saveTrainingSiteParent) {
	    trainingPosition.getAuthorisationField().getTrainingSite().setParent(trainingSiteDAO.save(trainingPosition.getAuthorisationField().getTrainingSite().getParent()));
	}
	if (saveTrainingSite) {
	    trainingPosition.getAuthorisationField().setTrainingSite(trainingSiteDAO.save(trainingPosition.getAuthorisationField().getTrainingSite()));
	}
	if (saveAuthorised) {
	    trainingPosition.getAuthorisationField().setAuthorisedDoctor(personDAO.save(trainingPosition.getAuthorisationField().getAuthorisedDoctor()));
	}
	if (saveAuthorisation) {
	    trainingPosition.setAuthorisationField(authorisationDAO.save(trainingPosition.getAuthorisationField()));
	}

	return save(trainingPosition);
    }

    @Transactional
    private void initExclusions(List<TrainingPositionField> positions) {
	for (TrainingPositionField position : positions) {
	    position.getExclusions().size();
	}

    }
}
