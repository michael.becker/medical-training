package org.infai.ddls.medicaltraining.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.infai.ddls.medicaltraining.model.Sex;

@SuppressWarnings("serial")
public class SexSpecification extends AbstractTrainingAppEntitySpecification<Sex, SexFilter> {
    public SexSpecification(SexFilter filter) {
	super(filter);
    }

    @Override
    protected void initPredicates(Root<Sex> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	// TODO Auto-generated method stub

    }

}
