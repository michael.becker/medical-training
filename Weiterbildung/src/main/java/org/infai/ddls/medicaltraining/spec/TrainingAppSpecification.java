package org.infai.ddls.medicaltraining.spec;

import org.infai.ddls.medicaltraining.model.TrainingAppEntity;
import org.springframework.data.jpa.domain.Specification;

public interface TrainingAppSpecification<T extends TrainingAppEntity> extends Specification<T> {
    public static final String ATTRIBUTE_NAME_UID = "uid";
}
