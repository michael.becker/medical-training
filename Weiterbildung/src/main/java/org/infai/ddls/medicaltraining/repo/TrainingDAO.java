package org.infai.ddls.medicaltraining.repo;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.infai.ddls.medicaltraining.model.MedicalField;
import org.infai.ddls.medicaltraining.spec.MedicalFieldFilter;
import org.infai.ddls.medicaltraining.spec.MedicalFieldSpecification2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

@Component
public class TrainingDAO extends AbstractTrainingAppDAO {
    @Autowired
    private MedicalFieldRepository medicalFieldRepository;

    @Transactional
    public MedicalField getMedicalField(Long uid) {
	return getMedicalField(uid, false, false);
    }

    @Transactional
    public MedicalField getMedicalField(Long uid, boolean loadChildFields) {
	return getMedicalField(uid, loadChildFields, false);
    }

    @Transactional
    public MedicalField getMedicalField(Long uid, boolean loadChildFields, boolean loadIndirectChildFields) {
	MedicalField medicalField = medicalFieldRepository.findById(uid).get();

	if (loadChildFields) {
	    medicalField.getChildren().size();
	}

	if (loadIndirectChildFields) {
	    medicalField.getAllChildFields().size();
	}

	return medicalField;
    }

    @Transactional
    public List<MedicalField> getMedicalFields() {
	return getMedicalFields(new MedicalFieldFilter(), DEFAULT_SORT);
    }

    @Transactional
    public List<MedicalField> getMedicalFields(MedicalFieldFilter filter, Sort sort) {
	MedicalFieldSpecification2 spec = new MedicalFieldSpecification2(filter);
	sort = evaluateSort(sort).and(new Sort(Direction.ASC, "archived"));

	return medicalFieldRepository.findAll(spec, sort);
    }

    @Transactional
    public List<MedicalField> getMedicalFields(Sort sort) {
	return getMedicalFields(new MedicalFieldFilter(), sort);
    }

    @Transactional
    public MedicalField save(MedicalField medicalField) {
	// TODO: eigentlich müsste der Validator automatisch beim Speichern aufgerufen
	// werden. Allerdings fehlt der Testfall
	// MedicalFieldDAOTest#testCreateTransitiveCycleFailsWithExistingMedicalFields
	// fehl, wenn der Validator an dieser Stelle nicht explizit aufgerufen wird...
	Set<ConstraintViolation<MedicalField>> violations = validator.validate(medicalField);
	if (!violations.isEmpty()) {
	    throw new ConstraintViolationException(violations);
	}

	return medicalFieldRepository.save(medicalField);
    }
}
