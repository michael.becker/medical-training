package org.infai.ddls.medicaltraining.test.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.repo.AuthorisationDAO;
import org.infai.ddls.medicaltraining.repo.RotationPlanDAO;
import org.infai.ddls.medicaltraining.repo.TrainingPositionDAO;
import org.infai.ddls.medicaltraining.repo.TrainingSiteNewDAO;
import org.infai.ddls.medicaltraining.spec.AuthorisationFieldFilter;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import com.github.springtestdbunit.annotation.DatabaseSetup;

@DatabaseSetup("/doctorsDataSet.xml")
@DatabaseSetup("/medicalFieldsDataSet.xml")
@DatabaseSetup("/trainingSchemesDataSet.xml")
@DatabaseSetup("/trainingSitesDataSet.xml")
public class AuthorisationDAOTest extends AbstractSpringRepositoryTest {
    @Autowired
    private AuthorisationDAO authorisationDAO;
    @Autowired
    private TrainingPositionDAO trainingPositionDAO;
    @Autowired
    private RotationPlanDAO rotationPlanDAO;
    @Autowired
    private TrainingSiteNewDAO trainingSiteDAO;

    @Test
    @DatabaseSetup("/authorisationsDataSet.xml")
    @WithMockUser
    public void testDeleteAuthorisationWithoutDependencies() throws Exception {
	assertNotNull(authorisationDAO.getAuthorisationField(1L));
	assertEquals(7, authorisationDAO.getAuthorisationFields(null).size());

	AuthorisationField authorisationField = authorisationDAO.getAuthorisationField(1L);

	assertTrue(authorisationDAO.delete(authorisationField));
	assertNull(authorisationDAO.getAuthorisationField(1L));
	assertEquals(6, authorisationDAO.getAuthorisationFields(null).size());
    }

    @Test
    @DatabaseSetup("/authorisationsDataSet.xml")
    @DatabaseSetup("/trainingPositionsDataSet.xml")
    @WithMockUser
    public void testDeleteAuthorisationWithTrainingPositions() throws Exception {
	assertNotNull(authorisationDAO.getAuthorisationField(1L));
	assertEquals(7, authorisationDAO.getAuthorisationFields(null).size());
	assertNotNull(trainingPositionDAO.getTrainingPositionField(1L));
	assertEquals(5, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	assertEquals(authorisationDAO.getAuthorisationField(1L), trainingPositionDAO.getTrainingPositionField(1L).getAuthorisationField());

	AuthorisationField authorisationField = authorisationDAO.getAuthorisationField(1L);

	assertFalse(authorisationDAO.delete(authorisationField));
	assertTrue(authorisationDAO.delete(authorisationField, true));

	assertNull(authorisationDAO.getAuthorisationField(1L));
	assertEquals(6, authorisationDAO.getAuthorisationFields(null).size());
	assertNull(trainingPositionDAO.getTrainingPositionField(1L));
	assertEquals(4, trainingPositionDAO.getTrainingPositionsField(null, false).size());
    }

    @Test
    @DatabaseSetup("/authorisationsDataSet.xml")
    @DatabaseSetup("/trainingPositionsDataSet.xml")
    @DatabaseSetup("/rotationPlansDataSet.xml")
    @WithMockUser
    public void testDeleteAuthorisationWithTrainingPositionsAndRotationPlans() throws Exception {
	assertNotNull(authorisationDAO.getAuthorisationField(1L));
	assertEquals(7, authorisationDAO.getAuthorisationFields(null).size());
	assertNotNull(trainingPositionDAO.getTrainingPositionField(1L));
	assertEquals(5, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	assertEquals(authorisationDAO.getAuthorisationField(1L), trainingPositionDAO.getTrainingPositionField(1L).getAuthorisationField());
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(1L));
	assertEquals(trainingPositionDAO.getTrainingPositionField(1L), rotationPlanDAO.getRotationPlanSegment(1L).getTrainingPosition());

	AuthorisationField authorisationField = authorisationDAO.getAuthorisationField(1L);

	assertFalse(authorisationDAO.delete(authorisationField));
	assertFalse(authorisationDAO.delete(authorisationField, true));
	assertTrue(authorisationDAO.delete(authorisationField, true, true));

	assertNull(authorisationDAO.getAuthorisationField(1L));
	assertEquals(6, authorisationDAO.getAuthorisationFields(null).size());
	assertNull(trainingPositionDAO.getTrainingPositionField(1L));
	assertEquals(4, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	assertNull(rotationPlanDAO.getRotationPlanSegment(1L));
    }

    @Test
    @DatabaseSetup("/authorisationsDataSet.xml")
    public void testGetAuthorisationsByTrainingSite() throws Exception {
	AuthorisationFieldFilter filter = new AuthorisationFieldFilter();
	filter.setTrainingSite(trainingSiteDAO.getTrainingSite(31L, false, false));

	List<AuthorisationField> authorisations = authorisationDAO.getAuthorisations(filter, null, true, true, true);
	assertNotNull(authorisations);
	assertEquals(2, authorisations.size());
    }
}
