package org.infai.ddls.medicaltraining.test.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;
import org.infai.ddls.medicaltraining.model.Registrar;
import org.infai.ddls.medicaltraining.model.Sex;
import org.infai.ddls.medicaltraining.repo.AuthorisationDAO;
import org.infai.ddls.medicaltraining.repo.PersonDAO;
import org.infai.ddls.medicaltraining.repo.RotationPlanDAO;
import org.infai.ddls.medicaltraining.repo.TrainingPositionDAO;
import org.infai.ddls.medicaltraining.spec.AuthorisedDoctorFilter;
import org.infai.ddls.medicaltraining.spec.RegistrarFilter;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import com.github.springtestdbunit.annotation.DatabaseSetup;

@DatabaseSetup("/doctorsDataSet.xml")
public class PersonDAOTest extends AbstractSpringRepositoryTest {
    @Autowired
    private PersonDAO personDAO;
    @Autowired
    private AuthorisationDAO authorisationDAO;
    @Autowired
    private TrainingPositionDAO trainingPositionDAO;
    @Autowired
    private RotationPlanDAO rotationPlanDAO;

    @Test
    @DatabaseSetup("/medicalFieldsDataSet.xml")
    @DatabaseSetup("/trainingSchemesDataSet.xml")
    @DatabaseSetup("/trainingSitesDataSet.xml")
    @DatabaseSetup("/authorisationsDataSet.xml")
    @DatabaseSetup("/trainingPositionsDataSet.xml")
    @DatabaseSetup("/rotationPlansDataSet.xml")
    @WithMockUser
    public void testDeleteAuthorisedDoctorWithAuthorisationAndTrainingPositionAndRotationPlanSegments() throws Exception {
	AuthorisedDoctor authorisedDoctor = personDAO.getAuthorisedDoctor(1L, false);
	assertNotNull(authorisedDoctor);
	assertNotNull(authorisationDAO.getAuthorisationField(1L));
	assertEquals(7, authorisationDAO.getAuthorisationFields(null).size());
	assertNotNull(trainingPositionDAO.getTrainingPositionField(1L));
	assertEquals(5, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(1L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(10L));
	assertEquals(8, rotationPlanDAO.getRotationPlanSegments(null).size());

	assertTrue(personDAO.delete(authorisedDoctor, true, true, true));

	// der Weiterbildungsbefugte ist gelöscht
	assertNull(personDAO.getAuthorisedDoctor(1L, false));
	assertEquals(5, personDAO.getAuthorisedDoctors(null).size());

	// die Befugnis des Weiterbildungsbefugten ist gelöscht
	assertNull(authorisationDAO.getAuthorisationField(1L));
	assertEquals(6, authorisationDAO.getAuthorisationFields(null).size());

	// die Weiterbildungsstelle der Befugnis ist gelöscht
	assertNull(trainingPositionDAO.getTrainingPositionField(1L));
	assertEquals(4, trainingPositionDAO.getTrainingPositionsField(null, false).size());

	// die Weiterbildungsabschnitte der Weiterbildungsstelle sind gelöscht
	assertNull(rotationPlanDAO.getRotationPlanSegment(1L));
	assertNull(rotationPlanDAO.getRotationPlanSegment(10L));
	assertEquals(6, rotationPlanDAO.getRotationPlanSegments(null).size());
    }

    @Test
    @WithMockUser
    public void testDeleteAuthorisedDoctorWithoutDependencies() throws Exception {
	AuthorisedDoctor authorisedDoctor = personDAO.getAuthorisedDoctor(1L, false);

	personDAO.delete(authorisedDoctor);

	assertNull(personDAO.getAuthorisedDoctor(1L, false));
	assertEquals(5, personDAO.getAuthorisedDoctors(null).size());
    }

    @Test
    public void testGetAuthorisedDoctorByAddressText() throws Exception {
	AuthorisedDoctorFilter filter = new AuthorisedDoctorFilter();
	filter.setAddressText("parkplatz");

	List<AuthorisedDoctor> authorisedDoctors = personDAO.getAuthorisedDoctors(filter, null, false);

	assertNotNull(authorisedDoctors);
	assertEquals(1, authorisedDoctors.size());
	assertTrue(authorisedDoctors.contains(personDAO.getAuthorisedDoctor(2L, false)));

	filter.setAddressText("leip");
	authorisedDoctors = personDAO.getAuthorisedDoctors(filter, null, false);

	assertNotNull(authorisedDoctors);
	assertEquals(2, authorisedDoctors.size());
	assertTrue(authorisedDoctors.contains(personDAO.getAuthorisedDoctor(1L, false)));
	assertTrue(authorisedDoctors.contains(personDAO.getAuthorisedDoctor(2L, false)));
    }

    @Test
    public void testGetAuthorisedDoctorByFirstName() throws Exception {
	AuthorisedDoctorFilter filter = new AuthorisedDoctorFilter();
	filter.setFirstName("Chirurgie");

	List<AuthorisedDoctor> authorisedDoctors = personDAO.getAuthorisedDoctors(filter, null, false);

	assertNotNull(authorisedDoctors);
	assertEquals(1, authorisedDoctors.size());
	assertTrue(authorisedDoctors.contains(personDAO.getAuthorisedDoctor(1L, false)));
    }

    @Test
    public void testGetAuthorisedDoctorByLANR() throws Exception {
	AuthorisedDoctorFilter filter = new AuthorisedDoctorFilter();
	filter.setLanr(1);

	List<AuthorisedDoctor> authorisedDoctors = personDAO.getAuthorisedDoctors(filter, null, false);

	assertNotNull(authorisedDoctors);
	assertEquals(1, authorisedDoctors.size());
	assertTrue(authorisedDoctors.contains(personDAO.getAuthorisedDoctor(1L, false)));
    }

    @Test
    public void testGetAuthorisedDoctorByLastName() throws Exception {
	AuthorisedDoctorFilter filter = new AuthorisedDoctorFilter();
	filter.setLastName("Park");

	List<AuthorisedDoctor> authorisedDoctors = personDAO.getAuthorisedDoctors(filter, null, false);

	assertNotNull(authorisedDoctors);
	assertEquals(3, authorisedDoctors.size());
	assertTrue(authorisedDoctors.contains(personDAO.getAuthorisedDoctor(1L, false)));
	assertTrue(authorisedDoctors.contains(personDAO.getAuthorisedDoctor(2L, false)));
	assertTrue(authorisedDoctors.contains(personDAO.getAuthorisedDoctor(3L, false)));
    }

    @Test
    public void testGetAuthorisedDoctorByName() throws Exception {
	AuthorisedDoctorFilter filter = new AuthorisedDoctorFilter();
	filter.setFullname("Haus");

	List<AuthorisedDoctor> authorisedDoctors = personDAO.getAuthorisedDoctors(filter, null, false);
	System.out.println(authorisedDoctors);

	assertNotNull(authorisedDoctors);
	assertEquals(4, authorisedDoctors.size());
	assertTrue(authorisedDoctors.contains(personDAO.getAuthorisedDoctor(1L, false)));
	assertTrue(authorisedDoctors.contains(personDAO.getAuthorisedDoctor(2L, false)));
	assertTrue(authorisedDoctors.contains(personDAO.getAuthorisedDoctor(3L, false)));
	assertTrue(authorisedDoctors.contains(personDAO.getAuthorisedDoctor(6L, false)));
    }

    @Test
    public void testGetExistingAuthorisedDoctor() throws Exception {
	assertNotNull(personDAO.getAuthorisedDoctor(1L, false));
    }

    @Test
    public void testGetNonExistingAuthorisedDoctor() throws Exception {
	assertNull(personDAO.getAuthorisedDoctor(10L, false));
    }

    @Test
    @DatabaseSetup("/medicalFieldsDataSet.xml")
    @DatabaseSetup("/trainingSchemesDataSet.xml")
    @DatabaseSetup("/trainingSitesDataSet.xml")
    @DatabaseSetup("/authorisationsDataSet.xml")
    @DatabaseSetup("/trainingPositionsDataSet.xml")
    @DatabaseSetup("/rotationPlansDataset.xml")
    @Ignore
    public void testGetRegistrarsByActualTrainingPositions() throws Exception {
	RegistrarFilter filter = new RegistrarFilter();
	filter.setTrainingPosition("Park");

	List<Registrar> registrars = personDAO.getRegistrars(filter, null);

	assertList(registrars, personDAO.getRegistrar(1L), personDAO.getRegistrar(2L));
    }

    @Test
    @WithMockUser
    public void testIsLANRused() throws Exception {
	assertFalse(personDAO.isLANRusedByOther(personDAO.getAuthorisedDoctor(1L, true)));
	assertFalse(personDAO.isLANRusedByOther(personDAO.getAuthorisedDoctor(2L, true)));
	assertFalse(personDAO.isLANRusedByOther(personDAO.getAuthorisedDoctor(3L, true)));
	assertFalse(personDAO.isLANRusedByOther(personDAO.getAuthorisedDoctor(4L, true)));
	assertFalse(personDAO.isLANRusedByOther(personDAO.getAuthorisedDoctor(5L, true)));
	assertFalse(personDAO.isLANRusedByOther(personDAO.getAuthorisedDoctor(6L, true)));

	// keine LANR -> nicht in Benutzung
	AuthorisedDoctor authorisedDoctor = new AuthorisedDoctor();
	authorisedDoctor.getAddress().setStreet("Goerdelerring 9");
	authorisedDoctor.getAddress().setZipCode("04109");
	authorisedDoctor.getAddress().setCity("Leipzig");

	assertFalse(personDAO.isLANRusedByOther(authorisedDoctor));

	// LANR 1 ist bereits in Benutzung
	authorisedDoctor.setLanr(1);
	assertTrue(personDAO.isLANRusedByOther(authorisedDoctor));

	// LANDR 8 nicht in Benutzung
	authorisedDoctor.setLanr(7);
	assertFalse(personDAO.isLANRusedByOther(authorisedDoctor));

	// nach dem Speichern: 7 nicht in Benutzung
	authorisedDoctor.setName("test");
	authorisedDoctor.setLastName("test");
	authorisedDoctor = personDAO.save(authorisedDoctor);
	assertFalse(personDAO.isLANRusedByOther(authorisedDoctor));

	// nach dem Speichern: 1 in Benutzung
	authorisedDoctor.setLanr(1);
	assertTrue(personDAO.isLANRusedByOther(authorisedDoctor));
    }

    @Test
    @WithMockUser
    public void testSetSex() throws Exception {
	Sex male = new Sex("männlich", "Ein Junge", "Herr", "Sehr geehrter Herr");
	Sex female = new Sex("weiblich", "Ein Mädchen", "Frau", "Sehr geehrte Frau");

	male = personDAO.save(male);
	female = personDAO.save(female);

	Registrar registrar = new Registrar();
	registrar.setName("Vorname");
	registrar.setLastName("Nachname");
	registrar.getAddress().setStreet("Goerdelerring 9");
	registrar.getAddress().setZipCode("04109");
	registrar.getAddress().setCity("Leipzig");
	registrar = personDAO.save(registrar);
	registrar.setSex(male);
	registrar = personDAO.save(registrar);
    }
}
