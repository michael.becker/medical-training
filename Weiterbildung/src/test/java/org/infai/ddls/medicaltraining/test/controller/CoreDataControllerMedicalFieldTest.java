package org.infai.ddls.medicaltraining.test.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.infai.ddls.medicaltraining.controller.RequestMappings;
import org.infai.ddls.medicaltraining.model.MedicalField;
import org.infai.ddls.medicaltraining.repo.TrainingDAO;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.github.springtestdbunit.annotation.DatabaseSetup;

public class CoreDataControllerMedicalFieldTest extends AbstractWebMVCTest {
    @Autowired
    private TrainingDAO trainingDAO;

    @Test
    @DatabaseSetup("/medicalFieldsDataSet.xml")
    @WithMockUser(roles = "MANAGER")
    public void testUpdateMedicalFieldWithCyclesFails() throws Exception {
	MockHttpServletRequestBuilder postRequest = post(RequestMappings.MEDICALFIELDS_UPDATE);

	MedicalField unmittelbare = trainingDAO.getMedicalField(4L, true, true);
	MedicalField chirurgie = trainingDAO.getMedicalField(2L, true, true);

	unmittelbare.setParent(chirurgie);

	addMedicalField(postRequest, unmittelbare);

	ResultActions resultActions = mockMvc.perform(postRequest);

	resultActions.andExpect(status().isOk());
	resultActions.andExpect(model().hasErrors());
    }

    @Test
    @DatabaseSetup("/medicalFieldsDataSet.xml")
    @WithMockUser(roles = "MANAGER")
    public void testUpdateMedicalFieldWithoutParentFieldSucceeds() throws Exception {
	System.out.println(SecurityContextHolder.getContext().getAuthentication());
	MockHttpServletRequestBuilder postRequest = post(RequestMappings.MEDICALFIELDS_UPDATE);

	MedicalField unmittelbare = trainingDAO.getMedicalField(4L, true, true);

	addMedicalField(postRequest, unmittelbare);

	ResultActions resultActions = mockMvc.perform(postRequest);

	resultActions.andExpect(status().isFound());
	resultActions.andExpect(model().hasNoErrors());
    }
}
