package org.infai.ddls.medicaltraining.test.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileInputStream;

import org.infai.ddls.medicaltraining.model.TrainingSiteList;
import org.infai.ddls.medicaltraining.repo.TrainingSiteNewRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.github.springtestdbunit.annotation.DatabaseSetup;

public class AdminControllerTest extends AbstractWebMVCTest {
    @Autowired
    private TrainingSiteNewRepository trainingSiteRepository;

    @Test
    @WithMockUser(roles = "ADMIN")
    @DatabaseSetup("/medicalFieldsDataSet.xml")
    public void testImportTrainingSites() throws Exception {
	File file = new File(getClass().getResource("/Weiterbildungsbefugte.fods").getFile());
	FileInputStream fis = new FileInputStream(file);
	MockMultipartFile trainingSitesFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, fis);

	MockHttpServletRequestBuilder request = multipart("/admin/trainingsites/upload").file(trainingSitesFile);

	ResultActions result = mockMvc.perform(request);

	result.andExpect(status().isOk());
	result.andExpect(model().hasNoErrors());
	result.andExpect(model().attributeExists("trainingSites"));

	TrainingSiteList trainingSites = (TrainingSiteList) result.andReturn().getModelAndView().getModel().get("trainingSites");

	request = post("/admin/trainingsites/uploadDo");
	for (int i = 0; i < trainingSites.getTrainingSites().size(); i++) {
	    request.param("trainingSites[" + i + "].name", trainingSites.getTrainingSites().get(i).getName());
	    request.param("trainingSites[" + i + "].address.street", trainingSites.getTrainingSites().get(i).getAddress().getStreet());
	    request.param("trainingSites[" + i + "].address.zipCode", trainingSites.getTrainingSites().get(i).getAddress().getZipCode());
	    request.param("trainingSites[" + i + "].address.city", trainingSites.getTrainingSites().get(i).getAddress().getCity());
	    request.param("trainingSites[" + i + "].treatmentType", "Inpatient");
	    request.param("trainingSites[" + i + "].medicalField", "1");
	}

	result = mockMvc.perform(request);
	result.andExpect(status().isFound());
	result.andExpect(model().hasNoErrors());

	assertEquals(8, trainingSiteRepository.count());
    }
}
