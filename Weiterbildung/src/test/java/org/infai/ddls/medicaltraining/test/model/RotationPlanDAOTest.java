package org.infai.ddls.medicaltraining.test.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.infai.ddls.medicaltraining.model.RotationPlan;
import org.infai.ddls.medicaltraining.model.RotationPlanSegment;
import org.infai.ddls.medicaltraining.repo.AuthorisationDAO;
import org.infai.ddls.medicaltraining.repo.PersonDAO;
import org.infai.ddls.medicaltraining.repo.RotationPlanDAO;
import org.infai.ddls.medicaltraining.repo.TrainingPositionDAO;
import org.infai.ddls.medicaltraining.repo.TrainingSchemeDAO;
import org.infai.ddls.medicaltraining.spec.RotationPlanFilter;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.google.common.collect.Lists;

@EnableWebMvc
@DatabaseSetup("/doctorsDataSet.xml")
@DatabaseSetup("/medicalFieldsDataSet.xml")
@DatabaseSetup("/trainingSchemesDataSet.xml")
@DatabaseSetup("/trainingSitesDataSet.xml")
@DatabaseSetup("/authorisationsDataSet.xml")
@DatabaseSetup("/trainingPositionsDataSet.xml")
public class RotationPlanDAOTest extends AbstractSpringRepositoryTest {
    @Autowired
    private RotationPlanDAO rotationPlanDAO;
    @Autowired
    private PersonDAO doctorDAO;
    @Autowired
    private TrainingSchemeDAO trainingSchemeDAO;
    @Autowired
    private AuthorisationDAO authorisationDAO;
    @Autowired
    private TrainingPositionDAO trainingPositionDAO;

    @Test
    @WithMockUser
    public void testCreateOverlappingRotationSegmentsPlansFails() throws Exception {
	RotationPlan rotationPlan = new RotationPlan();
	rotationPlan.setRegistrar(doctorDAO.getRegistrar(1L));
	rotationPlan.setTrainingScheme(trainingSchemeDAO.getTrainingScheme(1L, false));

	rotationPlan = rotationPlanDAO.saveRotationPlan(rotationPlan);

	RotationPlanSegment segment1 = new RotationPlanSegment(rotationPlan, trainingPositionDAO.getTrainingPositionField(1L), LocalDate.parse("2017-01-01"), LocalDate.parse("2017-09-30"), 0.66, true, true);
	RotationPlanSegment segment2 = new RotationPlanSegment(rotationPlan, trainingPositionDAO.getTrainingPositionField(1L), LocalDate.parse("2017-02-01"), LocalDate.parse("2018-03-31"), 1.0, false, false);

	rotationPlan.setRotationPlanSegments(Lists.newArrayList(segment1, segment2));

	try {
	    rotationPlan = rotationPlanDAO.saveRotationPlan(rotationPlan);
	    fail("expected ConstraintViolationException");
	} catch (ConstraintViolationException e) {
	}
    }

    @Test
    @WithMockUser
    public void testCreateRotationPlan() throws Exception {
	RotationPlan rotationPlan = new RotationPlan();
	rotationPlan.setRegistrar(doctorDAO.getRegistrar(1L));
	rotationPlan.setTrainingScheme(trainingSchemeDAO.getTrainingScheme(1L, false));
	rotationPlan.setName("New RotPlan for " + rotationPlan.getRegistrar());

	RotationPlanSegment segment1 = new RotationPlanSegment(rotationPlan, trainingPositionDAO.getTrainingPositionField(1L), LocalDate.parse("2017-01-01"), LocalDate.parse("2017-09-30"), 0.66, true, true);
	RotationPlanSegment segment2 = new RotationPlanSegment(rotationPlan, trainingPositionDAO.getTrainingPositionField(1L), LocalDate.parse("2017-10-01"), LocalDate.parse("2018-03-31"), 1.0, false, false);
	RotationPlanSegment segment3 = new RotationPlanSegment(rotationPlan, trainingPositionDAO.getTrainingPositionField(1L), LocalDate.parse("2018-04-01"), LocalDate.parse("2018-09-30"), 1.0, false, false);
	RotationPlanSegment segment4 = new RotationPlanSegment(rotationPlan, trainingPositionDAO.getTrainingPositionField(1L), LocalDate.parse("2018-10-01"), LocalDate.parse("2019-03-31"), 1.0, false, false);
	RotationPlanSegment segment5 = new RotationPlanSegment(rotationPlan, trainingPositionDAO.getTrainingPositionField(1L), LocalDate.parse("2019-04-01"), LocalDate.parse("2020-03-31"), 1.0, false, false);
	RotationPlanSegment segment6 = new RotationPlanSegment(rotationPlan, trainingPositionDAO.getTrainingPositionField(1L), LocalDate.parse("2020-09-01"), LocalDate.parse("2021-02-28"), 1.0, false, false);
	RotationPlanSegment segment7 = new RotationPlanSegment(rotationPlan, trainingPositionDAO.getTrainingPositionField(1L), LocalDate.parse("2021-03-01"), LocalDate.parse("2021-08-31"), 1.0, false, false);

	rotationPlan.setRotationPlanSegments(Lists.newArrayList(segment1, segment2, segment3, segment4, segment5, segment6, segment7));

	rotationPlan = rotationPlanDAO.saveRotationPlan(rotationPlan);

	assertNotNull(rotationPlan);
	assertNotNull(rotationPlan.getRegistrar());
	assertNotNull(rotationPlan.getTrainingScheme());

	assertNotNull(rotationPlan.getRotationPlanSegments());
	assertEquals(7, rotationPlan.getRotationPlanSegments().size());
    }

    @Test
    @DatabaseSetup("/rotationPlansDataset.xml")
    @WithMockUser
    public void testDeleteRotationPlanSegment() throws Exception {
	assertNotNull(rotationPlanDAO.getRotationPlan(1L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(1L));
	assertTrue(rotationPlanDAO.getRotationPlan(1L, true, false).getRotationPlanSegments().contains(rotationPlanDAO.getRotationPlanSegment(1L)));

	RotationPlanSegment rotationPlanSegment = rotationPlanDAO.getRotationPlanSegment(1L);
	assertTrue(rotationPlanDAO.delete(rotationPlanSegment));

	assertNotNull(rotationPlanDAO.getRotationPlan(1L));
	assertNull(rotationPlanDAO.getRotationPlanSegment(1L));
    }

    @Test
    @DatabaseSetup("/rotationPlansDataset.xml")
    @WithMockUser
    public void testDeleteRotationPlanWithoutDependencies() throws Exception {
	assertNotNull(rotationPlanDAO.getRotationPlan(1L));
	assertEquals(3, rotationPlanDAO.getRotationPlans(null).size());
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(1L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(2L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(3L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(4L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(5L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(6L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(7L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(10L));
	assertEquals(8, rotationPlanDAO.getRotationPlanSegments(null).size());

	RotationPlan rotationPlan = rotationPlanDAO.getRotationPlan(3L);
	assertTrue(rotationPlanDAO.delete(rotationPlan));
	assertNull(rotationPlanDAO.getRotationPlan(3L));
	assertEquals(2, rotationPlanDAO.getRotationPlans(null).size());
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(1L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(2L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(3L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(4L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(5L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(6L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(7L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(10L));
	assertEquals(8, rotationPlanDAO.getRotationPlanSegments(null).size());

    }

    @Test
    @DatabaseSetup("/rotationPlansDataset.xml")
    public void testDeleteRotationPlanWithRotationPlanSegmentsFailure() throws Exception {
	assertNotNull(rotationPlanDAO.getRotationPlan(1L));
	assertEquals(3, rotationPlanDAO.getRotationPlans(null).size());
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(1L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(2L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(3L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(4L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(5L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(6L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(7L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(10L));
	assertEquals(8, rotationPlanDAO.getRotationPlanSegments(null).size());

	RotationPlan rotationPlan = rotationPlanDAO.getRotationPlan(1L);
	assertFalse(rotationPlanDAO.delete(rotationPlan, false));
	assertNotNull(rotationPlanDAO.getRotationPlan(1L));
	assertEquals(3, rotationPlanDAO.getRotationPlans(null).size());
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(1L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(2L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(3L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(4L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(5L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(6L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(7L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(10L));
	assertEquals(8, rotationPlanDAO.getRotationPlanSegments(null).size());
    }

    @Test
    @DatabaseSetup("/rotationPlansDataset.xml")
    @WithMockUser
    public void testDeleteRotationPlanWithRotationPlanSegmentsSuccess() throws Exception {
	assertNotNull(rotationPlanDAO.getRotationPlan(1L));
	assertEquals(3, rotationPlanDAO.getRotationPlans(null).size());
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(1L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(2L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(3L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(4L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(5L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(6L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(7L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(10L));
	assertEquals(8, rotationPlanDAO.getRotationPlanSegments(null).size());

	RotationPlan rotationPlan = rotationPlanDAO.getRotationPlan(1L);
	assertTrue(rotationPlanDAO.delete(rotationPlan, true));
	assertNull(rotationPlanDAO.getRotationPlan(1L));
	assertEquals(2, rotationPlanDAO.getRotationPlans(null).size());
	assertNull(rotationPlanDAO.getRotationPlanSegment(1L));
	assertNull(rotationPlanDAO.getRotationPlanSegment(2L));
	assertNull(rotationPlanDAO.getRotationPlanSegment(3L));
	assertNull(rotationPlanDAO.getRotationPlanSegment(4L));
	assertNull(rotationPlanDAO.getRotationPlanSegment(5L));
	assertNull(rotationPlanDAO.getRotationPlanSegment(6L));
	assertNull(rotationPlanDAO.getRotationPlanSegment(7L));
	assertNotNull(rotationPlanDAO.getRotationPlanSegment(10L));
	assertEquals(1, rotationPlanDAO.getRotationPlanSegments(null).size());
    }

    @Test
    @DatabaseSetup("/rotationPlansDataSet.xml")
    public void testFilterRotationPlanSegmentsByAuthorisation() throws Exception {
	RotationPlanFilter filter = new RotationPlanFilter();
	filter.setAuthorisationField(authorisationDAO.getAuthorisationField(1L));

	List<RotationPlanSegment> rotationPlanSegments = rotationPlanDAO.getRotationPlanSegments(filter, null);
	assertList(rotationPlanSegments, rotationPlanDAO.getRotationPlanSegment(1L), rotationPlanDAO.getRotationPlanSegment(10L));

	filter.setAuthorisationField(authorisationDAO.getAuthorisationField(2L));
	rotationPlanSegments = rotationPlanDAO.getRotationPlanSegments(filter, null);
	assertList(rotationPlanSegments, rotationPlanDAO.getRotationPlanSegment(2L));
    }

    @Test
    @DatabaseSetup("/rotationPlansDataset.xml")
    public void testFilterRotationPlanSegmentsByTrainingPosition() throws Exception {
	RotationPlanFilter filter = new RotationPlanFilter();
	filter.setTrainingPositionField(trainingPositionDAO.getTrainingPositionField(1L));

	List<RotationPlanSegment> rotationPlanSegments = rotationPlanDAO.getRotationPlanSegments(filter, null);
	assertList(rotationPlanSegments, rotationPlanDAO.getRotationPlanSegment(1L), rotationPlanDAO.getRotationPlanSegment(10L));

	filter.setTrainingPositionField(trainingPositionDAO.getTrainingPositionField(4L));
	rotationPlanSegments = rotationPlanDAO.getRotationPlanSegments(filter, null);
	assertList(rotationPlanSegments, rotationPlanDAO.getRotationPlanSegment(4L), rotationPlanDAO.getRotationPlanSegment(5L), rotationPlanDAO.getRotationPlanSegment(6L), rotationPlanDAO.getRotationPlanSegment(7L));
    }

    @Test
    @DatabaseSetup("/rotationPlansDataSet.xml")
    public void testGetActualRotationPlanSegmentOfARegistrar() throws Exception {
	RotationPlanFilter filter = new RotationPlanFilter();
	filter.setRegistrar(doctorDAO.getRegistrar(1L));
	filter.setReferenceDate(LocalDate.parse("2018-02-01"));
	filter.setSearchForSegments(true);

	List<RotationPlanSegment> segments = rotationPlanDAO.getRotationPlanSegments(filter, null);

	assertList(segments, rotationPlanDAO.getRotationPlanSegment(2L));

	assertEquals(segments.get(0), rotationPlanDAO.getRotationPlanSegment(doctorDAO.getRegistrar(1L), LocalDate.parse("2018-02-01")));
    }

    @Test
    @DatabaseSetup("/rotationPlansDataSet.xml")
    public void testGetActualSegmentsByDate() throws Exception {
	RotationPlan rotationPlan = rotationPlanDAO.getRotationPlan(1L, true, true);
	LocalDate now = LocalDate.parse("2017-12-01");

	assertNotNull(rotationPlan.getPlannedSegments(now));
	assertEquals(6, rotationPlan.getPlannedSegments(now).size());
    }

    @Test
    @DatabaseSetup("/rotationPlansDataSet.xml")
    public void testGetActualSegmentsByDateAndTrainingSchemeSegment() throws Exception {
	RotationPlan rotationPlan = rotationPlanDAO.getRotationPlan(1L, true, true);
	LocalDate now = LocalDate.parse("2017-12-01");

	List<RotationPlanSegment> rotationPlanSegments = rotationPlan.getPlannedSegments(now, trainingSchemeDAO.getTrainingSchemeSegment(4L));
	assertList(rotationPlanSegments, rotationPlanDAO.getRotationPlanSegment(2L), rotationPlanDAO.getRotationPlanSegment(3L), rotationPlanDAO.getRotationPlanSegment(6L));

	rotationPlanSegments = rotationPlan.getPlannedSegments(now, trainingSchemeDAO.getTrainingSchemeSegment(1L));
	assertList(rotationPlanSegments, rotationPlanDAO.getRotationPlanSegment(7L));

	assertEquals(0, rotationPlan.getPlannedSegments(now, trainingSchemeDAO.getTrainingSchemeSegment(2L)).size());
	assertEquals(2, rotationPlan.getPlannedSegments(now, trainingSchemeDAO.getTrainingSchemeSegment(5L)).size());
    }

    @Test
    @DatabaseSetup("/rotationPlansDataSet.xml")
    public void testGetFinishedSegmentsByDate() throws Exception {
	RotationPlan rotationPlan = rotationPlanDAO.getRotationPlan(1L, true, true);
	LocalDate now = LocalDate.parse("2017-12-01");

	assertNotNull(rotationPlan.getFinishedSegments(now));
	assertEquals(1, rotationPlan.getFinishedSegments(now).size());
    }

    @Test
    @DatabaseSetup("/rotationPlansDataSet.xml")
    public void testGetFinishedSegmentsByDateAndTrainingSchemeSegment() throws Exception {
	RotationPlan rotationPlan = rotationPlanDAO.getRotationPlan(1L, true, true);
	LocalDate now = LocalDate.parse("2017-12-01");

	assertNotNull(rotationPlan.getFinishedSegments(now, trainingSchemeDAO.getTrainingSchemeSegment(1L)));
	assertEquals(0, rotationPlan.getFinishedSegments(now, trainingSchemeDAO.getTrainingSchemeSegment(1L)).size());

	assertNotNull(rotationPlan.getFinishedSegments(now, trainingSchemeDAO.getTrainingSchemeSegment(2L)));
	assertEquals(1, rotationPlan.getFinishedSegments(now, trainingSchemeDAO.getTrainingSchemeSegment(2L)).size());
    }

    @Test
    @DatabaseSetup("/rotationPlansDataSet.xml")
    public void testGetOrderedRotationPlanSegments() throws Exception {
	RotationPlanFilter filter = new RotationPlanFilter();
	filter.setRegistrar(doctorDAO.getRegistrar(1L));

	RotationPlan rotationPlan = rotationPlanDAO.getRotationPlans(filter, null, true, true).get(0);

	for (RotationPlanSegment rotationPlanSegment : rotationPlan.getRotationPlanSegments()) {
	    System.out.println(rotationPlanSegment.getBegin() + " --- " + rotationPlanSegment.getEnd() + ": " + rotationPlanSegment.getTrainingPosition().getAuthorisationField().getTrainingSite().getName());
	}

	for (int i = 0; i < rotationPlan.getRotationPlanSegments().size() - 1; i++) {
	    RotationPlanSegment actualSegment = rotationPlan.getRotationPlanSegments().get(i);
	    RotationPlanSegment nextSegment = rotationPlan.getRotationPlanSegments().get(i + 1);

	    assertTrue(actualSegment.getBegin().isBefore(nextSegment.getBegin()));
	}
    }

    @Test
    @DatabaseSetup("/rotationPlansDataSet.xml")
    public void testGetRotationPlansByRegistrarWithoutPlans() throws Exception {
	RotationPlanFilter filter = new RotationPlanFilter();
	filter.setRegistrar(doctorDAO.getRegistrar(3L));

	List<RotationPlan> rotationPlans = rotationPlanDAO.getRotationPlans(filter, null, true, true);

	assertNotNull(rotationPlans);
	assertEquals(0, rotationPlans.size());
    }

    @Test
    @DatabaseSetup("/rotationPlansDataSet.xml")
    public void testGetRotationPlansByRegistrarWithPlans() throws Exception {
	RotationPlanFilter filter = new RotationPlanFilter();
	filter.setRegistrar(doctorDAO.getRegistrar(1L));

	List<RotationPlan> rotationPlans = rotationPlanDAO.getRotationPlans(filter, null, true, true);

	assertList(rotationPlans, rotationPlanDAO.getRotationPlan(1L));
    }

    @Test
    @DatabaseSetup("/rotationPlansDataSet.xml")
    public void testGetRotationPlanSegmentsByRotationPlan() throws Exception {
	List<RotationPlanSegment> rotationPlanSegments = rotationPlanDAO.getRotationPlanSegments(rotationPlanDAO.getRotationPlan(1L), null);
	assertNotNull(rotationPlanSegments);
	assertEquals(7, rotationPlanSegments.size());
	assertTrue(rotationPlanSegments.contains(rotationPlanDAO.getRotationPlanSegment(1L)));
	assertTrue(rotationPlanSegments.contains(rotationPlanDAO.getRotationPlanSegment(2L)));
	assertTrue(rotationPlanSegments.contains(rotationPlanDAO.getRotationPlanSegment(3L)));
	assertTrue(rotationPlanSegments.contains(rotationPlanDAO.getRotationPlanSegment(4L)));
	assertTrue(rotationPlanSegments.contains(rotationPlanDAO.getRotationPlanSegment(5L)));
	assertTrue(rotationPlanSegments.contains(rotationPlanDAO.getRotationPlanSegment(6L)));
	assertTrue(rotationPlanSegments.contains(rotationPlanDAO.getRotationPlanSegment(7L)));

	rotationPlanSegments = rotationPlanDAO.getRotationPlanSegments(rotationPlanDAO.getRotationPlan(2L), null);
	assertNotNull(rotationPlanSegments);
	assertEquals(1, rotationPlanSegments.size());
	assertTrue(rotationPlanSegments.contains(rotationPlanDAO.getRotationPlanSegment(10L)));

	rotationPlanSegments = rotationPlanDAO.getRotationPlanSegments(rotationPlanDAO.getRotationPlan(3L), null);
	assertNotNull(rotationPlanSegments);
	assertTrue(rotationPlanSegments.isEmpty());
    }

}
