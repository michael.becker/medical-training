package org.infai.ddls.medicaltraining.test.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.github.springtestdbunit.annotation.DatabaseSetup;

//@RunWith(SpringRunner.class)
//@ContextConfiguration(locations = { "file:src/test/resources/testContext.xml" })
//@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
//@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
//@WebAppConfiguration
//@EnableWebMvc
@DatabaseSetup("/doctorsDataSet.xml")
@DatabaseSetup("/medicalFieldsDataSet.xml")
@DatabaseSetup("/trainingSchemesDataSet.xml")
@DatabaseSetup("/trainingSitesDataSet.xml")
@DatabaseSetup("/authorisationsDataSet.xml")
public class TrainingControllerTest extends AbstractWebMVCTest {
    @Autowired
    protected WebApplicationContext webApplicationContext;
    protected MockMvc mockMvc;

    @Override
    @Before
    public void setUp() {
	mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Ignore
    @Test
    public void testCreateOverlappingRotationPlanSegmentsFails() throws Exception {

    }

    @Test
    @WithMockUser
    public void testEditRotationPlan() throws Exception {
	MockHttpServletRequestBuilder get = get("/training/editRotationPlan/1");

	ResultActions result = mockMvc.perform(get);
	result.andExpect(status().isOk());
    }

    @Ignore
    @Test
    public void testFilterTrainingPositions() throws Exception {
	MockHttpServletRequestBuilder postRequest = post("/training/filterTrainingPositions");

	postRequest.param("begin", String.valueOf("2018-01-01"));
	postRequest.param("end", String.valueOf("2018-07-31"));

	ResultActions result = mockMvc.perform(postRequest);
	result.andExpect(status().isOk());
    }
}
