package org.infai.ddls.medicaltraining.test.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.infai.ddls.medicaltraining.model.Address;
import org.infai.ddls.medicaltraining.model.constraints.AddressExistsValidator;
import org.junit.Test;

public class AddressTest {
    @Test
    public void testDirectValidateInvalidAddressReturnsFalse() throws Exception {
	AddressExistsValidator addressValidator = new AddressExistsValidator();

	Address address = new Address();
	address.setStreet("Musterstraße 15");
	address.setZipCode("99999");
	address.setCity("Non-Existing-City");

	assertFalse(addressValidator.isValid(address, null));
    }

    @Test
    public void testDirectValidateValidAddressReturnsTrue() throws Exception {
	AddressExistsValidator addressValidator = new AddressExistsValidator();

	Address address = new Address();
	address.setStreet("Goerdelerring 9");
	address.setZipCode("04109");
	address.setCity("Leipzig");

	assertTrue(addressValidator.isValid(address, null));
    }

    @Test
    public void testValidatorCallValidateInvalidAddressIsInvalid() throws Exception {
	Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

	Address address = new Address();
	address.setStreet("Musterstraße 15");
	address.setZipCode("01011");
	address.setCity("Non-Existing-City");

	Set<ConstraintViolation<Address>> violations = validator.validate(address);
	assertFalse(violations.isEmpty());
    }

    @Test
    public void testValisatorCallValidateValidAddressIsValid() throws Exception {
	Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

	Address address = new Address();
	address.setStreet("Goerdelerring 9");
	address.setZipCode("04109");
	address.setCity("Leipzig");

	Set<ConstraintViolation<Address>> violations = validator.validate(address);
	assertTrue(violations.isEmpty());
    }
}
