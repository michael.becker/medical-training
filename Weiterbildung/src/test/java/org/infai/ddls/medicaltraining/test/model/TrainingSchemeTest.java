package org.infai.ddls.medicaltraining.test.model;

import static org.junit.Assert.assertEquals;

import org.infai.ddls.medicaltraining.test.repo.Scenario;
import org.junit.Test;

public class TrainingSchemeTest {
    @Test
    public void testCalculateDuration() throws Exception {
	assertEquals(60, Scenario.allgemeinMedizinLSA.getDuration());
    }
}
