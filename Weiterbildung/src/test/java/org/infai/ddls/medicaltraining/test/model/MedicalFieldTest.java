package org.infai.ddls.medicaltraining.test.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.infai.ddls.medicaltraining.model.ChildRelationUtil;
import org.infai.ddls.medicaltraining.model.MedicalField;
import org.junit.Test;

public class MedicalFieldTest {
    @Test
    public void testCheckDirectParentRelations() throws Exception {
	MedicalField unmittelbarePatientenversorgung = new MedicalField("Unmittelbare Patientenversorgung");
	MedicalField allgemeinMedizin = new MedicalField("Allgemeinmedizin", "Allgemeinmedizin", unmittelbarePatientenversorgung);
	MedicalField anaesthesiologie = new MedicalField("Anästhesiologie", "Anästhesiologie", unmittelbarePatientenversorgung);

	assertFalse(ChildRelationUtil.isChildOf(allgemeinMedizin, allgemeinMedizin));
	assertTrue(ChildRelationUtil.isChildOf(allgemeinMedizin, unmittelbarePatientenversorgung));
	assertTrue(ChildRelationUtil.isChildOf(anaesthesiologie, unmittelbarePatientenversorgung));
	assertFalse(ChildRelationUtil.isChildOf(unmittelbarePatientenversorgung, allgemeinMedizin));
	assertFalse(ChildRelationUtil.isChildOf(unmittelbarePatientenversorgung, anaesthesiologie));

	assertEquals(2, unmittelbarePatientenversorgung.getAllChildFields().size());
    }

    @Test
    public void testCheckDirectParentRelationsWithCycle() throws Exception {
	MedicalField field = new MedicalField();
	field.setParent(field);

	assertTrue(ChildRelationUtil.isChildOf(field, field));
	assertTrue(field.getAllChildFields().contains(field));
    }

    @Test
    public void testCheckIndirectParentRelations() throws Exception {
	MedicalField unmittelbarePatientenversorgung = new MedicalField("Unmittelbare Patientenversorgung");
	MedicalField hausarzt = new MedicalField("Hausärztliche Versorgung", "HÄV", unmittelbarePatientenversorgung);
	MedicalField allgemeinMedizin = new MedicalField("Allgemeinmedizin", "Allgemeinmedizin", hausarzt);

	assertTrue(ChildRelationUtil.isChildOf(allgemeinMedizin, hausarzt));
	assertTrue(ChildRelationUtil.isChildOf(hausarzt, unmittelbarePatientenversorgung));
	assertTrue(ChildRelationUtil.isChildOf(allgemeinMedizin, unmittelbarePatientenversorgung));

	assertFalse(ChildRelationUtil.isChildOf(unmittelbarePatientenversorgung, allgemeinMedizin));

	assertEquals(2, unmittelbarePatientenversorgung.getAllChildFields().size());
    }

    @Test
    public void testCheckIndirectParentRelationsWithCycle() throws Exception {
	MedicalField parent = new MedicalField("parent");
	MedicalField child = new MedicalField("child");

	child.setParent(parent);
	parent.setParent(child);

	assertTrue(ChildRelationUtil.isChildOf(parent, parent));
	assertTrue(parent.getAllChildFields().contains(parent));
    }
}
