package org.infai.ddls.medicaltraining.test.security;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.infai.ddls.medicaltraining.test.controller.AbstractWebMVCTest;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

public class UserRoleAccessTest extends AbstractWebMVCTest {
    @Test
    @WithMockUser(roles = "ADMIN")
    public void testAdminAccessToImport() throws Exception {
	MockHttpServletRequestBuilder postRequest = get("/admin/trainingsites/import");

	ResultActions resultActions = mockMvc.perform(postRequest);

	resultActions.andExpect(status().isOk());
	resultActions.andExpect(model().hasNoErrors());
    }
}
