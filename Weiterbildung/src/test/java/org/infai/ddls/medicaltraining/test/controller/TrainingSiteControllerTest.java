package org.infai.ddls.medicaltraining.test.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.infai.ddls.medicaltraining.controller.RequestMappings;
import org.infai.ddls.medicaltraining.model.TrainingSite;
import org.infai.ddls.medicaltraining.repo.TrainingSiteNewDAO;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.github.springtestdbunit.annotation.DatabaseSetup;

@DatabaseSetup("/medicalFieldsDataSet.xml")
public class TrainingSiteControllerTest extends AbstractWebMVCTest {
    @Autowired
    private TrainingSiteNewDAO trainingSiteDAO;

    @Test
    @DatabaseSetup("/trainingSitesDataSet.xml")
    public void testListTrainingSites() throws Exception {
	MockHttpServletRequestBuilder get = get("/trainingsites/list");

	ResultActions result = mockMvc.perform(get);
	result.andExpect(status().isOk());
    }

    @Test
    @DatabaseSetup("/trainingSitesDataSet.xml")
    @WithMockUser
    public void testUpdateTrainingSite() throws Exception {
	MockHttpServletRequestBuilder requestBuilder = post(RequestMappings.TRAININGSITES_UPDATE);
	TrainingSite trainingSite = trainingSiteDAO.getTrainingSite(1L, true, true);
	addTrainingSite(requestBuilder, trainingSite);
	addSaveParam(requestBuilder);

	System.out.println("xxxxchecking " + trainingSite.getChildren());
	System.out.println("xxxxchecking " + trainingSite.getTrainingSiteType());

	ResultActions result = mockMvc.perform(requestBuilder);
	result.andExpect(status().isFound());
    }

}
