package org.infai.ddls.medicaltraining.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.infai.ddls.medicaltraining.model.Address;

public class AbstractTrainingAppTest {
    protected void assertCollection(Collection<?> actualCollection, Object... expctedElements) {
	assertNotNull(actualCollection);
	assertEquals(expctedElements.length, actualCollection.size());

	for (Object expectedElement : expctedElements) {
	    assertTrue(expectedElement + " not contained in " + actualCollection, actualCollection.contains(expectedElement));
	}
    }

    /**
     * Prüft folgende Bedinungen:
     * <ul>
     * <li>die übergebene Liste <code>actualList</code> ist nicht null</li>
     * <li>die Größe der Liste entspricht der Anzahl der gegebenen
     * Listenelemente</li>
     * <li>alle Listenelemente sind in der gegebenen Liste <code>actualList</code>
     * enthalten</li>
     * </ul>
     * 
     * @param actualList
     * @param expectedList
     */
    protected void assertList(List<?> actualList, Object... expectedElements) {
	assertNotNull(actualList);
	assertEquals(expectedElements.length, actualList.size());

	for (Object expectedElement : expectedElements) {
	    assertTrue(expectedElement + " not contained in " + actualList, actualList.contains(expectedElement));
	}
    }

    protected void assertSortedList(List<?> actualList, Object... expectedElements) {
	assertNotNull(actualList);
	assertEquals(expectedElements.length, actualList.size());

	for (int i = 0; i < expectedElements.length; i++) {
	    assertEquals(expectedElements[i], actualList.get(i));
	}
    }

    protected void compare(@NotNull Address expected, @NotNull Address actual) {
	assertNotNull(actual);
	assertEquals(expected.getStreet(), actual.getStreet());
	assertEquals(expected.getZipCode(), actual.getZipCode());
	assertEquals(expected.getCity(), actual.getCity());
    }
}
