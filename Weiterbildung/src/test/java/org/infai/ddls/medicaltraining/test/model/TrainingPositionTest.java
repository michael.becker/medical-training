package org.infai.ddls.medicaltraining.test.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.infai.ddls.medicaltraining.model.RotationPlanSegment;
import org.infai.ddls.medicaltraining.model.TrainingPositionAvailability;
import org.infai.ddls.medicaltraining.model.TrainingPositionField;
import org.junit.Before;
import org.junit.Test;

public class TrainingPositionTest {
    private TrainingPositionField trainingPositionField;

    @Before
    public void setUp() {
	trainingPositionField = new TrainingPositionField();
	trainingPositionField.setAvailableFrom(LocalDate.parse("2018-01-01"));
	trainingPositionField.setAvailableFrom(LocalDate.parse("2030-12-31"));
	trainingPositionField.setCapacity(2);
    }

    @Test
    public void testBlockedDatesBeginAfter() throws Exception {
	trainingPositionField.addExclusion(LocalDate.parse("2018-06-01"), LocalDate.parse("2018-10-31"));

	assertTrue(trainingPositionField.isBlocked(LocalDate.parse("2018-06-15"), LocalDate.parse("2018-09-30")));
	assertTrue(trainingPositionField.isBlocked(LocalDate.parse("2018-06-15"), LocalDate.parse("2018-11-30")));
	assertTrue(trainingPositionField.isBlocked(LocalDate.parse("2018-10-31"), LocalDate.parse("2018-11-30")));
	assertFalse(trainingPositionField.isBlocked(LocalDate.parse("2018-11-01"), LocalDate.parse("2018-11-30")));
    }

    @Test
    public void testBlockedDatesBeginBefore() throws Exception {
	trainingPositionField.addExclusion(LocalDate.parse("2018-06-01"), LocalDate.parse("2018-10-31"));

	assertTrue(trainingPositionField.isBlocked(LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31")));
	assertFalse(trainingPositionField.isBlocked(LocalDate.parse("2018-01-01"), LocalDate.parse("2018-05-31")));
	assertTrue(trainingPositionField.isBlocked(LocalDate.parse("2018-01-01"), LocalDate.parse("2018-06-01")));
	assertTrue(trainingPositionField.isBlocked(LocalDate.parse("2018-06-01"), LocalDate.parse("2018-06-01")));
	assertTrue(trainingPositionField.isBlocked(LocalDate.parse("2018-06-01"), LocalDate.parse("2018-09-30")));
    }

    // @Test
    // public void testGetAvailabilityWithMultipleAssignedRotationPlanSegments()
    // throws Exception {
    // AuthorisedDoctor doctor = new AuthorisedDoctor();
    // doctor.setName("Einar");
    // doctor.setLastName("Schleef");
    //
    // TrainingSite trainingSite = new TrainingSite();
    // trainingSite.setName("TrainingSite");
    //
    // AuthorisationField authorisation = new AuthorisationField();
    // authorisation.setAuthorisedDoctor(doctor);
    // authorisation.setTrainingSite(trainingSite);
    // authorisation.setMaximalDuration(6);
    // authorisation.setGrantedOn(LocalDate.parse("2018-01-01"));
    // authorisation.setGrantedTo(LocalDate.parse("2099-12-31"));
    //
    // TrainingPositionField trainingPosition = new TrainingPositionField();
    // trainingPosition.setAvailableFrom(LocalDate.parse("2018-06-01"));
    // trainingPosition.setAvailableUntil(LocalDate.parse("2020-02-26"));
    // trainingPosition.setAuthorisationField(authorisation);
    // trainingPosition.setCapacity(1);
    //
    // TrainingPositionAvailability availability =
    // trainingPosition.getAvailability(LocalDate.parse("2019-01-01"),
    // LocalDate.parse("2019-06-30"), new ArrayList<>());
    //
    // System.out.println(availability);
    // }

    @Test
    public void testGetAvailabilityAvailable() throws Exception {
	RotationPlanSegment segment1 = new RotationPlanSegment();
	segment1.setBegin(LocalDate.parse("2018-02-01"));
	segment1.setEnd(LocalDate.parse("2018-06-30"));

	List<RotationPlanSegment> rotationPlanSegments = new ArrayList<>();
	rotationPlanSegments.add(segment1);

	assertEquals(TrainingPositionAvailability.AVAILABLE, trainingPositionField.getAvailability(LocalDate.parse("2017-10-01"), LocalDate.parse("2018-01-31"), rotationPlanSegments));
	assertEquals(TrainingPositionAvailability.AVAILABLE, trainingPositionField.getAvailability(LocalDate.parse("2018-01-01"), LocalDate.parse("2018-06-30"), rotationPlanSegments));
	assertEquals(TrainingPositionAvailability.AVAILABLE, trainingPositionField.getAvailability(LocalDate.parse("2018-06-01"), LocalDate.parse("2018-11-30"), rotationPlanSegments));
	assertEquals(TrainingPositionAvailability.AVAILABLE, trainingPositionField.getAvailability(LocalDate.parse("2018-07-01"), LocalDate.parse("2018-12-31"), rotationPlanSegments));
    }

    @Test
    public void testGetAvailabilityBlocked() throws Exception {
	trainingPositionField.addExclusion(LocalDate.parse("2018-06-01"), LocalDate.parse("2018-10-31"));

	assertEquals(TrainingPositionAvailability.BLOCKED, trainingPositionField.getAvailability(LocalDate.parse("2018-05-01"), LocalDate.parse("2018-06-30"), null));
	assertEquals(TrainingPositionAvailability.BLOCKED, trainingPositionField.getAvailability(LocalDate.parse("2018-07-01"), LocalDate.parse("2018-09-30"), null));
	assertEquals(TrainingPositionAvailability.BLOCKED, trainingPositionField.getAvailability(LocalDate.parse("2018-09-01"), LocalDate.parse("2018-11-30"), null));
    }

    @Test
    public void testGetAvailabilityOccupied() throws Exception {
	RotationPlanSegment segment1 = new RotationPlanSegment();
	segment1.setBegin(LocalDate.parse("2018-02-01"));
	segment1.setEnd(LocalDate.parse("2018-06-30"));
	RotationPlanSegment segment2 = new RotationPlanSegment();
	segment2.setBegin(LocalDate.parse("2018-01-01"));
	segment2.setEnd(LocalDate.parse("2018-05-31"));

	List<RotationPlanSegment> rotationPlanSegments = new ArrayList<>();
	rotationPlanSegments.add(segment1);
	rotationPlanSegments.add(segment2);

	assertEquals(TrainingPositionAvailability.OCCUPIED, trainingPositionField.getAvailability(LocalDate.parse("2018-01-01"), LocalDate.parse("2018-04-30"), rotationPlanSegments, 1));
	assertEquals(TrainingPositionAvailability.OCCUPIED, trainingPositionField.getAvailability(LocalDate.parse("2018-02-15"), LocalDate.parse("2018-05-15"), rotationPlanSegments, 1));
	assertEquals(TrainingPositionAvailability.OCCUPIED, trainingPositionField.getAvailability(LocalDate.parse("2018-05-01"), LocalDate.parse("2018-04-30"), rotationPlanSegments, 1));
    }

    @Test
    public void testGetAvailabilityOverlapping() throws Exception {
	RotationPlanSegment segment1 = new RotationPlanSegment();
	segment1.setBegin(LocalDate.parse("2018-02-01"));
	segment1.setEnd(LocalDate.parse("2018-06-30"));
	RotationPlanSegment segment2 = new RotationPlanSegment();
	segment2.setBegin(LocalDate.parse("2018-02-01"));
	segment2.setEnd(LocalDate.parse("2018-06-30"));

	List<RotationPlanSegment> rotationPlanSegments = new ArrayList<>();
	rotationPlanSegments.add(segment1);
	rotationPlanSegments.add(segment2);

	assertEquals(TrainingPositionAvailability.OVERLAPPING, trainingPositionField.getAvailability(LocalDate.parse("2017-10-01"), LocalDate.parse("2018-02-28"), rotationPlanSegments, 1));
	assertEquals(TrainingPositionAvailability.OVERLAPPING, trainingPositionField.getAvailability(LocalDate.parse("2018-06-01"), LocalDate.parse("2018-11-30"), rotationPlanSegments, 1));
	assertEquals(TrainingPositionAvailability.OVERLAPPING, trainingPositionField.getAvailability(LocalDate.parse("2018-01-01"), LocalDate.parse("2018-04-30"), rotationPlanSegments, 3));
    }

    @Test
    public void testNoBlockedDates() throws Exception {
	assertFalse(trainingPositionField.isBlocked(LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31")));
    }
}
