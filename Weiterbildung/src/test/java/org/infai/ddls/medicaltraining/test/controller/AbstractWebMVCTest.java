package org.infai.ddls.medicaltraining.test.controller;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.model.MedicalField;
import org.infai.ddls.medicaltraining.model.TrainingSite;
import org.infai.ddls.medicaltraining.model.TrainingSiteType;
import org.infai.ddls.medicaltraining.test.model.AbstractSpringRepositoryTest;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
public abstract class AbstractWebMVCTest extends AbstractSpringRepositoryTest {
    @Autowired
    protected WebApplicationContext webApplicationContext;
    protected MockMvc mockMvc;

    @Before
    public void setUp() {
	mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    protected void addAuthorisation(MockHttpServletRequestBuilder requestBuilder, AuthorisationField authorisationField) {
//	requestBuilder.param(, values)
    }

    protected void addMedicalField(MockHttpServletRequestBuilder requestBuilder, MedicalField medicalField) {
	if (null != medicalField.getUid()) {
	    requestBuilder.param("uid", String.valueOf(medicalField.getUid()));
	}

	requestBuilder.param("name", medicalField.getName());
	requestBuilder.param("description", medicalField.getDescription());

	if (null != medicalField.getParent()) {
	    requestBuilder.param("parent", String.valueOf(medicalField.getParent().getUid()));
	} else {
	    requestBuilder.param("parent", "");
	}
    }

    protected void addSaveAndCloseParam(MockHttpServletRequestBuilder requestBuilder) {
	requestBuilder.param("saveAction", "saveAndClose");
    }

    protected void addSaveParam(MockHttpServletRequestBuilder requestBuilder) {
	requestBuilder.param("saveAction", "save");
    }

    protected void addTrainingSite(MockHttpServletRequestBuilder requestBuilder, TrainingSite trainingSite) {
	requestBuilder.param("name", trainingSite.getName());
	requestBuilder.param("address.street", trainingSite.getAddress().getStreet());
	requestBuilder.param("address.zipCode", trainingSite.getAddress().getZipCode());
	requestBuilder.param("address.city", trainingSite.getAddress().getCity());
	requestBuilder.param("trainingSiteType", String.valueOf(trainingSite.getTrainingSiteType()));

	if (!trainingSite.getTrainingSiteType().equals(TrainingSiteType.CLINIC)) {
	    requestBuilder.param("medicalField", String.valueOf(trainingSite.getMedicalField().getUid()));
	    requestBuilder.param("treatmentType", String.valueOf(trainingSite.getTreatmentType()));
	}
    }
}
