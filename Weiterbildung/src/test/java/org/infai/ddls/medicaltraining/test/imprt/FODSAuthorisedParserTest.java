package org.infai.ddls.medicaltraining.test.imprt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collection;

import org.infai.ddls.medicaltraining.imprt.FODSTrainingSiteParser;
import org.infai.ddls.medicaltraining.model.TrainingSite;
import org.junit.Test;

public class FODSAuthorisedParserTest {

    @Test
    public void testParseFile() throws Exception {
	FODSTrainingSiteParser parser = new FODSTrainingSiteParser();
	Collection<TrainingSite> trainingSites = parser.parse(getClass().getResource("/Weiterbildungsbefugte.fods").getFile(), true, false);

	assertNotNull(trainingSites);
	assertEquals(8, trainingSites.size());

	for (TrainingSite trainingSite : trainingSites) {
	    System.out.println(trainingSite);
	}

	System.out.println("skipped: " + parser.getSkippedTrainingSites());
    }
}
