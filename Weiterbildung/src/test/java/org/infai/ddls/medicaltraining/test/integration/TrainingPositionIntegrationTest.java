package org.infai.ddls.medicaltraining.test.integration;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.infai.ddls.medicaltraining.model.RotationPlanSegment;
import org.infai.ddls.medicaltraining.model.TrainingPositionAvailability;
import org.infai.ddls.medicaltraining.model.TrainingPositionField;
import org.infai.ddls.medicaltraining.repo.RotationPlanDAO;
import org.infai.ddls.medicaltraining.repo.TrainingPositionDAO;
import org.infai.ddls.medicaltraining.test.controller.AbstractWebMVCTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import com.github.springtestdbunit.annotation.DatabaseSetup;

@DatabaseSetup("/trainingPositionIntegrationTestDataSet.xml")
public class TrainingPositionIntegrationTest extends AbstractWebMVCTest {
    @Autowired
    private TrainingPositionDAO trainingPositionDAO;
    @Autowired
    private RotationPlanDAO rotationPlanDAO;

    @Test
    @WithMockUser
    public void testCapacityIsReducedWhenTrainingPositionIsAssigned() throws Exception {
	// Trainingposition holen
	TrainingPositionField trainingPosition = trainingPositionDAO.getTrainingPositionField(1L, true);

	LocalDate from = LocalDate.parse("2018-01-01");
	LocalDate until = LocalDate.parse("2018-06-30");

	// prüfen, dass die Position momentan frei ist
	assertEquals(TrainingPositionAvailability.AVAILABLE, trainingPosition.getAvailability(from, until, rotationPlanDAO.getRotationPlanSegments(null)));

	// trainingpostion an rotationplan eines aiw hängen
	RotationPlanSegment segment1 = new RotationPlanSegment(rotationPlanDAO.getRotationPlan(1L, true, true), trainingPosition, from, until, 1.0, false, false);
	rotationPlanDAO.saveRotationPlanSegment(segment1);

	// prüfen, dass die Position immer noch frei ist
	assertEquals(TrainingPositionAvailability.AVAILABLE, trainingPosition.getAvailability(from, until, rotationPlanDAO.getRotationPlanSegments(null)));

	// trainingposition an rotiationplan eines anderen aiw hängen
	RotationPlanSegment segment2 = new RotationPlanSegment(rotationPlanDAO.getRotationPlan(2L, true, true), trainingPosition, from, until, 1.0, false, false);
	rotationPlanDAO.saveRotationPlanSegment(segment2);

	// prüfen, dass die Position nicht mehr frei ist
	assertEquals(TrainingPositionAvailability.OCCUPIED, trainingPosition.getAvailability(from, until, rotationPlanDAO.getRotationPlanSegments(null)));

    }
}
