package org.infai.ddls.medicaltraining.test.repo;

import java.time.LocalDate;
import java.util.concurrent.ThreadLocalRandom;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;
import org.infai.ddls.medicaltraining.model.MedicalField;
import org.infai.ddls.medicaltraining.model.RotationPlan;
import org.infai.ddls.medicaltraining.model.RotationPlanSegment;
import org.infai.ddls.medicaltraining.model.TrainingPositionField;
import org.infai.ddls.medicaltraining.model.TrainingScheme;
import org.infai.ddls.medicaltraining.model.TrainingSchemeSegment;
import org.infai.ddls.medicaltraining.model.TrainingSite;
import org.infai.ddls.medicaltraining.model.TreatmentType;

import com.google.common.collect.Lists;

public class Scenario {
    public static final MedicalField unmittelbarePatientenversorgung = new MedicalField("Unmittelbare Patientenversorgung");

    public static final MedicalField innereMedizin = new MedicalField("Innere Medizin", null, unmittelbarePatientenversorgung);;
    public static final MedicalField chirurgie = new MedicalField("Chirurgie", null, unmittelbarePatientenversorgung);;
    public static final MedicalField kjMedizin = new MedicalField("Kinder- und Jugendmedizin", null, unmittelbarePatientenversorgung);;
    public static final MedicalField hausaerztlicheVersorgung = new MedicalField("Hausärztliche Versorgung", null, unmittelbarePatientenversorgung);;
    public static final MedicalField allgemeinmedizin = new MedicalField("Allgemeinmedizin", null, hausaerztlicheVersorgung);

    public static final TrainingSchemeSegment innereMedizinSegment = new TrainingSchemeSegment(18, innereMedizin, false, true);
    public static final TrainingSchemeSegment chirurgieSegment = new TrainingSchemeSegment(6, chirurgie, true, true);
    public static final TrainingSchemeSegment kjMedizinSegment = new TrainingSchemeSegment(6, kjMedizin, true, true);
    public static final TrainingSchemeSegment unmittelbareSegment = new TrainingSchemeSegment(6, unmittelbarePatientenversorgung, true, true);
    public static final TrainingSchemeSegment allgemeinMedizinSegment = new TrainingSchemeSegment(18, allgemeinmedizin, true, false);
    public static final TrainingSchemeSegment hausarztSegment = new TrainingSchemeSegment(6, hausaerztlicheVersorgung, true, false);

    public static final TrainingScheme allgemeinMedizinLSA = new TrainingScheme("FA Allgemeinmedizin LSA", null, innereMedizinSegment, chirurgieSegment, kjMedizinSegment, unmittelbareSegment, allgemeinMedizinSegment, hausarztSegment);

    public static final AuthorisedDoctor weiterbilderInnere = new AuthorisedDoctor();

    public static final AuthorisedDoctor weiterbilderChirurgie = new AuthorisedDoctor();
    public static final AuthorisedDoctor weiterbilderKJM = new AuthorisedDoctor();
    public static final AuthorisedDoctor weiterbilderAllgemein = new AuthorisedDoctor();

    public static final AuthorisationField weiterbildungInnereAmb = createAuthorisation(innereMedizin, TreatmentType.Outpatient, weiterbilderInnere);
    public static final AuthorisationField weiterbildungInnereStat = createAuthorisation(innereMedizin, TreatmentType.Inpatient, weiterbilderInnere);
    public static final AuthorisationField weiterbildungChirurgieAmb = createAuthorisation(chirurgie, TreatmentType.Outpatient, weiterbilderChirurgie);
    public static final AuthorisationField weiterbildungChirurgieStat = createAuthorisation(chirurgie, TreatmentType.Inpatient, weiterbilderChirurgie);
    public static final AuthorisationField weiterbildungKJMAmb = createAuthorisation(kjMedizin, TreatmentType.Outpatient, weiterbilderKJM);
    public static final AuthorisationField weiterbildungKJMStat = createAuthorisation(kjMedizin, TreatmentType.Inpatient, weiterbilderKJM);
    public static final AuthorisationField weiterbildungAllgemeinAmb = createAuthorisation(allgemeinmedizin, TreatmentType.Outpatient, weiterbilderAllgemein);
    public static final AuthorisationField weiterbildungAllgemeinStat = createAuthorisation(allgemeinmedizin, TreatmentType.Inpatient, weiterbilderAllgemein);;

    public static final TrainingPositionField positionInnereAmb = new TrainingPositionField(weiterbildungInnereAmb, LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31"), 2, true);
    public static final TrainingPositionField positionInnereStat = new TrainingPositionField(weiterbildungInnereStat, LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31"), 2, true);
    public static final TrainingPositionField positionChirurgieAmb = new TrainingPositionField(weiterbildungChirurgieAmb, LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31"), 2, true);
    public static final TrainingPositionField positionChirurgieStat = new TrainingPositionField(weiterbildungChirurgieStat, LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31"), 2, true);
    public static final TrainingPositionField positionKJMAmb = new TrainingPositionField(weiterbildungKJMAmb, LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31"), 2, true);
    public static final TrainingPositionField positionKJMStat = new TrainingPositionField(weiterbildungKJMStat, LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31"), 2, true);
    public static final TrainingPositionField positionAllgemeinAmb = new TrainingPositionField(weiterbildungAllgemeinAmb, LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31"), 2, true);
    public static final TrainingPositionField positionAllgemeinStat = new TrainingPositionField(weiterbildungAllgemeinStat, LocalDate.parse("2018-01-01"), LocalDate.parse("2030-12-31"), 2, true);

    public static final RotationPlanSegment segmentInnereStat = createRotationPlanSegment(positionInnereStat, LocalDate.parse("2018-01-01"), LocalDate.parse("2019-06-30"));
    public static final RotationPlanSegment segmentChirurgieAmb = createRotationPlanSegment(positionChirurgieAmb, LocalDate.parse("2019-07-01"), LocalDate.parse("2019-12-31"));
    public static final RotationPlanSegment segmentKJMAmb = createRotationPlanSegment(positionKJMAmb, LocalDate.parse("2020-01-01"), LocalDate.parse("2020-06-30"));
    public static final RotationPlanSegment segmentInnereAmb = createRotationPlanSegment(positionInnereAmb, LocalDate.parse("2020-07-01"), LocalDate.parse("2020-12-31"));
    public static final RotationPlanSegment segmentAllgemeinAmb1 = createRotationPlanSegment(positionAllgemeinAmb, LocalDate.parse("2021-01-01"), LocalDate.parse("2022-06-30"));
    public static final RotationPlanSegment segmentAllgemeinStat = createRotationPlanSegment(positionAllgemeinStat, LocalDate.parse("2022-07-01"), LocalDate.parse("2022-12-31"));
    public static final RotationPlan rotationplanAllgemeinmedizin = new RotationPlan(Lists.newArrayList(segmentInnereStat, segmentChirurgieAmb, segmentKJMAmb, segmentInnereAmb, segmentAllgemeinAmb1, segmentAllgemeinStat));

    private static AuthorisationField createAuthorisation(MedicalField medicalField, TreatmentType treatmentType, AuthorisedDoctor authorisedDoctor) {
	TrainingSite trainingSite = new TrainingSite();
	trainingSite.setName("Testinstitution " + ThreadLocalRandom.current().nextInt());
	TrainingSite trainingSiteDepartment = new TrainingSite();
	trainingSiteDepartment.setParent(trainingSite);
	trainingSiteDepartment.setMedicalField(medicalField);
	trainingSiteDepartment.setTreatmentType(treatmentType);

	AuthorisationField authorisation = new AuthorisationField();
	authorisation.setAuthorisedDoctor(authorisedDoctor);
	authorisation.setTrainingSite(trainingSiteDepartment);
	authorisation.setGrantedOn(LocalDate.parse("2018-01-01"));
	authorisation.setGrantedTo(LocalDate.parse("2099-12-31"));

	return authorisation;
    }

    private static RotationPlanSegment createRotationPlanSegment(TrainingPositionField trainingPosition, LocalDate begin, LocalDate end) {
	RotationPlanSegment rotationPlanSegment = new RotationPlanSegment();
	rotationPlanSegment.setTrainingPosition(trainingPosition);
	rotationPlanSegment.setBegin(begin);
	rotationPlanSegment.setEnd(end);

	return rotationPlanSegment;
    }
}
