package org.infai.ddls.medicaltraining.test.model;

import org.infai.ddls.medicaltraining.model.Address;
import org.infai.ddls.medicaltraining.model.Location;
import org.junit.Test;

public class LocationTest {
    private class MockLocation extends Location {
	public MockLocation(String street, String zipCode, String city) {
	    this.address = new Address();
	    this.address.setStreet(street);
	    this.address.setZipCode(zipCode);
	    this.address.setCity(city);

	    updateLatLng();
	}
    }

    @Test
    public void testGetCoordinates() throws Exception {
	double[] coord = Location.getCoordinates("04109");
	System.out.println(coord[0] + ", " + coord[1]);
    }

    @Test
    public void testGetDistance() throws Exception {
	MockLocation source = new MockLocation("Hainstraße 11", "04109", "Leipzig");
	MockLocation targetUni = new MockLocation("Augustusplatz 10", "04109", "Leipzig");
	MockLocation targetHalle = new MockLocation(null, "06108", "Halle (Saale)");

	System.out.println(source.getLatitude() + ", " + source.getLongitude());
	System.out.println(targetUni.getLatitude() + ", " + targetUni.getLongitude());
	System.out.println(targetHalle.getLatitude() + ", " + targetHalle.getLongitude());

	System.out.println(Location.getDistance(source, targetUni));
	System.out.println(Location.getDistance(source, targetHalle));
    }
}
