package org.infai.ddls.medicaltraining.test.controller.rest;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.repo.AuthorisationDAO;
import org.infai.ddls.medicaltraining.test.controller.AbstractWebMVCTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.annotation.DatabaseSetup;

public class AuthorisationRESTControllerTest extends AbstractWebMVCTest {
    @Autowired
    private AuthorisationDAO authorisationDAO;

    @Test
    @DatabaseSetup("/medicalFieldsDataSet.xml")
    @DatabaseSetup("/trainingSitesDataSet.xml")
    @DatabaseSetup("/doctorsDataSet.xml")
    @DatabaseSetup("/trainingSchemesDataSet.xml")
    @DatabaseSetup("/authorisationsDataSet.xml")
    public void testGetAuthorisationsByTrainingSiteWebRequest() throws Exception {
	MockHttpServletRequestBuilder get = get("/authorisations/getByTrainingSite/31");

	ResultActions result = mockMvc.perform(get);
	result.andExpect(status().isOk());

	System.out.println(result.andReturn().getResponse().getContentAsString());

	ObjectMapper mapper = new ObjectMapper();
	List<AuthorisationField> authorisations = mapper.readerFor(new TypeReference<List<AuthorisationField>>() {
	}).readValue(result.andReturn().getResponse().getContentAsString());

	assertNotNull(authorisations);
	assertList(authorisations, authorisationDAO.getAuthorisationField(5L), authorisationDAO.getAuthorisationField(7L));
    }
}
