package org.infai.ddls.medicaltraining.test.json;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.model.MedicalField;
import org.infai.ddls.medicaltraining.model.TrainingSite;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONSerializationTest {
    @Test
    public void testSerializeMedialField() throws Exception {

	AuthorisationField authorisation = new AuthorisationField();
	authorisation.setName("Authorisation");
	TrainingSite trainingSite = new TrainingSite();
	trainingSite.setName("TrainingSite");
	MedicalField medicalField = new MedicalField();
	medicalField.setUid(1L);
	medicalField.setName("medicalField");
	MedicalField parent = new MedicalField();
	parent.setUid(2L);
	parent.setName("parentMedicalField");

	medicalField.setParent(parent);
	trainingSite.setMedicalField(medicalField);
	authorisation.setTrainingSite(trainingSite);

	System.out.println(medicalField.getParent());
	System.out.println(medicalField.getChildren());

	System.out.println(parent.getParent());
	System.out.println(parent.getChildren());

	ObjectMapper mapper = new ObjectMapper();

	String resultMedialField = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(medicalField);
	System.out.println(resultMedialField);
	MedicalField resultField = mapper.readValue(resultMedialField, MedicalField.class);
	System.out.println(resultField);
	System.out.println(resultField.getParent());
	System.out.println(resultField.getChildren());
	System.out.println(resultField.getParent().getParent());
	System.out.println(resultField.getParent().getChildren());

	String resultAuthorisation = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(authorisation);

	System.out.println(resultAuthorisation);

	authorisation = mapper.readerFor(AuthorisationField.class).readValue(resultAuthorisation);
	System.out.println(authorisation);

    }
}
