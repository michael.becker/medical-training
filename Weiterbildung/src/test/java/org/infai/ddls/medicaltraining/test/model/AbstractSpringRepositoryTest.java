package org.infai.ddls.medicaltraining.test.model;

import java.nio.file.Files;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.validation.Validator;

import org.infai.ddls.medicaltraining.test.AbstractTrainingAppTest;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.security.test.context.support.WithSecurityContextTestExecutionListener;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.github.springtestdbunit.DbUnitTestExecutionListener;

@RunWith(SpringRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/testContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class, WithSecurityContextTestExecutionListener.class })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@WebAppConfiguration
@Sql(scripts = "classpath:user_h2.sql")
public abstract class AbstractSpringRepositoryTest extends AbstractTrainingAppTest {
    @Autowired
    private ResourceLoader resourceLoader;
    @Autowired
    private EntityManager entityManager;
    @Autowired
    protected Validator validator;

    // @Before
    // @Transactional
    // public void initACLDatabaseTables() throws Exception {
    // String aclInit = new
    // String(Files.readAllBytes(resourceLoader.getResource("classpath:acl_h2.sql").getFile().toPath()));
    // Query query = entityManager.createNativeQuery(aclInit);
    // query.executeUpdate();
    // }

    @Transactional
    protected void initSQLFunctions() throws Exception {
	String initDBFunctions = new String(Files.readAllBytes(resourceLoader.getResource("classpath:db_functions_h2.sql").getFile().toPath()));
	Query query = entityManager.createNativeQuery(initDBFunctions);
	query.executeUpdate();
    }

}
