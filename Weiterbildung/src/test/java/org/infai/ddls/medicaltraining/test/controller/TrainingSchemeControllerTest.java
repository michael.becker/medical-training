package org.infai.ddls.medicaltraining.test.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.infai.ddls.medicaltraining.controller.RequestMappings;
import org.infai.ddls.medicaltraining.model.TrainingScheme;
import org.infai.ddls.medicaltraining.model.TrainingSchemeSegment;
import org.infai.ddls.medicaltraining.repo.TrainingDAO;
import org.infai.ddls.medicaltraining.repo.TrainingSchemeDAO;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.github.springtestdbunit.annotation.DatabaseSetup;

@DatabaseSetup("/medicalFieldsDataSet.xml")
public class TrainingSchemeControllerTest extends AbstractWebMVCTest {
    @Autowired
    protected WebApplicationContext webApplicationContext;
    protected MockMvc mockMvc;
    @Autowired
    private TrainingSchemeDAO dao;
    @Autowired
    private TrainingDAO trainingDAO;

    @Override
    @Before
    public void setUp() {
	mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    @WithMockUser
    public void testCreateTrainingScheme() throws Exception {
	TrainingScheme allgemeinMedizin = getMockScheme();

	MockHttpServletRequestBuilder postRequest = post(RequestMappings.TRAININGSCHEMES_UPDATE);
	postRequest.contentType(MediaType.APPLICATION_FORM_URLENCODED);

	postRequest.param("name", allgemeinMedizin.getName());
	postRequest.param("description", allgemeinMedizin.getDescription());

	for (int i = 0; i < allgemeinMedizin.getTrainingSchemeSegments().size(); i++) {
	    TrainingSchemeSegment trainingSchemeSegment = allgemeinMedizin.getTrainingSchemeSegments().get(i);
	    postRequest.param("trainingSchemeSegments[" + i + "].duration", String.valueOf(trainingSchemeSegment.getDuration()));
	    postRequest.param("trainingSchemeSegments[" + i + "].medicalField", String.valueOf(trainingSchemeSegment.getMedicalField().getUid()));
	    if (trainingSchemeSegment.isInpatient()) {
		postRequest.param("trainingSchemeSegments[" + i + "].inpatient", "on");
	    }
	    if (trainingSchemeSegment.isOutpatient()) {
		postRequest.param("trainingSchemeSegments[" + i + "].outpatient", "on");
	    }
	}

	ResultActions result = mockMvc.perform(postRequest);
	result.andExpect(status().isFound());
	result.andExpect(model().hasNoErrors());

	assertEquals(1, dao.getTrainingSchemes().size());
	assertEquals(6, dao.getTrainingSchemeSegments().size());
	for (TrainingSchemeSegment trainingSchemeSegment : dao.getTrainingSchemeSegments()) {
	    assertEquals(dao.getTrainingSchemes().get(0).getUid(), trainingSchemeSegment.getTrainingScheme().getUid());
	}
    }

    @Test
    @DatabaseSetup("/trainingSchemesDataSet.xml")
    public void testEditTrainingScheme() throws Exception {
	MockHttpServletRequestBuilder get = get("/trainingschemes/1/edit");

	ResultActions result = mockMvc.perform(get);
	result.andExpect(status().isOk());
    }

    @Test
    @DatabaseSetup("/trainingSchemesDataSet.xml")
    public void testListTrainingSchemes() throws Exception {
	MockHttpServletRequestBuilder get = get(RequestMappings.TRAININGSCHEMES_LIST);

	ResultActions result = mockMvc.perform(get);
	result.andExpect(status().isOk());
    }

    @Ignore
    @Test
    @DatabaseSetup("/trainingSchemesDataSet.xml")
    public void testUpdateTrainingScheme() throws Exception {

    }

    private TrainingScheme getMockScheme() {
	TrainingSchemeSegment innere = new TrainingSchemeSegment(18, trainingDAO.getMedicalField(1L), false, true);
	TrainingSchemeSegment chirurgie = new TrainingSchemeSegment(6, trainingDAO.getMedicalField(2L), true, true);
	TrainingSchemeSegment kinder = new TrainingSchemeSegment(6, trainingDAO.getMedicalField(3L), true, true);
	TrainingSchemeSegment unmitt = new TrainingSchemeSegment(6, trainingDAO.getMedicalField(4L), true, true);
	TrainingSchemeSegment allgemein = new TrainingSchemeSegment(18, trainingDAO.getMedicalField(5L), true, false);
	TrainingSchemeSegment hausarzt = new TrainingSchemeSegment(6, trainingDAO.getMedicalField(6L), true, false);

	TrainingScheme allgemeinMedizin = new TrainingScheme("FA Allgemeinmedizin", "Facharztweiterbildung Allgemeinmedizin", innere, chirurgie, kinder, unmitt, allgemein, hausarzt);

	return allgemeinMedizin;
    }
}
