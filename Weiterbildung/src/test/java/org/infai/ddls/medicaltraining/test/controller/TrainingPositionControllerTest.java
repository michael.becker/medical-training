package org.infai.ddls.medicaltraining.test.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.infai.ddls.medicaltraining.controller.AbstractMedicalTrainingController;
import org.infai.ddls.medicaltraining.controller.RequestMappings;
import org.infai.ddls.medicaltraining.controller.TrainingPositionController;
import org.infai.ddls.medicaltraining.model.Address;
import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;
import org.infai.ddls.medicaltraining.model.TrainingPositionField;
import org.infai.ddls.medicaltraining.model.TrainingSite;
import org.infai.ddls.medicaltraining.model.TrainingSiteType;
import org.infai.ddls.medicaltraining.model.TreatmentType;
import org.infai.ddls.medicaltraining.repo.AuthorisationDAO;
import org.infai.ddls.medicaltraining.repo.PersonDAO;
import org.infai.ddls.medicaltraining.repo.TrainingDAO;
import org.infai.ddls.medicaltraining.repo.TrainingPositionDAO;
import org.infai.ddls.medicaltraining.repo.TrainingSchemeDAO;
import org.infai.ddls.medicaltraining.repo.TrainingSiteNewDAO;
import org.infai.ddls.medicaltraining.spec.TrainingPositionFieldFilter;
import org.infai.ddls.medicaltraining.spec.TrainingSiteFilter;
import org.infai.ddls.medicaltraining.test.model.AbstractSpringRepositoryTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseSetups;

@EnableWebMvc
@DatabaseSetups({ @DatabaseSetup("/medicalFieldsDataSet.xml"), @DatabaseSetup("/trainingSchemesDataSet.xml"), @DatabaseSetup("/doctorsDataSet.xml"), @DatabaseSetup("/trainingSitesDataSet.xml"), @DatabaseSetup("/authorisationsDataSet.xml") })
public class TrainingPositionControllerTest extends AbstractSpringRepositoryTest {
    @Autowired
    protected WebApplicationContext webApplicationContext;
    protected MockMvc mockMvc;
    @Autowired
    private AuthorisationDAO authorisationDAO;
    @Autowired
    private PersonDAO personDAO;
    @Autowired
    private TrainingSiteNewDAO trainingSiteDAO;
    @Autowired
    private TrainingPositionDAO trainingPositionDAO;
    @Autowired
    private TrainingSchemeDAO trainingSchemeDAO;
    @Autowired
    private TrainingDAO trainingDAO;

    private TrainingSiteFilter departmentFilter;

    @Before
    public void setUp() {
	mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	departmentFilter = new TrainingSiteFilter();
	departmentFilter.setAllowParents(false);
	departmentFilter.setAllowDepartments(true);

	assertEquals(7, authorisationDAO.getAuthorisationFields(null).size());
	assertEquals(6, personDAO.getAuthorisedDoctors(null).size());
	assertEquals(5, trainingSiteDAO.getTrainingSites(new TrainingSiteFilter(), null, false, false).size());
	assertEquals(6, trainingSiteDAO.getTrainingSites(departmentFilter, null, false, false).size());
	assertEquals(0, trainingPositionDAO.getTrainingPositionsField(null, false).size());
    }

    @Test
    @WithMockUser
    public void testCreateTrainingPositionWithExclusions() throws Exception {
	MockHttpServletRequestBuilder postRequest = post(RequestMappings.TRAININGPOSITIONS_UPDATE);
	postRequest.contentType(MediaType.APPLICATION_FORM_URLENCODED);

	TrainingPositionField mockPosition = createMockPosition();
	addTrainingPosition(postRequest, mockPosition);
	postRequest.param(TrainingPositionController.PARAM_AUTHORISATION_FIELD, mockPosition.getAuthorisationField().getUid().toString());
	postRequest.param(TrainingPositionController.PARAM_DOCTOR, mockPosition.getAuthorisationField().getAuthorisedDoctor().getUid().toString());
	postRequest.param(TrainingPositionController.PARAM_TRAININGSITE, "-31");

	postRequest.param(TrainingPositionController.SWITCH_SELECT_AUTHORISATION, String.valueOf(TrainingPositionController.SelectAuthorisation.UseExistingAuthorisation));
	postRequest.param(TrainingPositionController.SWITCH_SELECT_SITE, String.valueOf(TrainingPositionController.SelectSite.UseExistingTrainingSite));

	postRequest.param("exclusions[0].exclusionFrom", "2018-01-01");
	postRequest.param("exclusions[0].exclusionUntil", "2018-02-01");
	postRequest.param("exclusions[1].exclusionFrom", "2019-01-01");
	postRequest.param("exclusions[1].exclusionUntil", "2019-02-01");

	ResultActions result = mockMvc.perform(postRequest);
	result.andExpect(status().isFound());

	TrainingPositionField trainingPosition = trainingPositionDAO.getTrainingPositionField(1L, true);

	assertNotNull(trainingPosition);
	assertNotNull(trainingPosition.getExclusions());
	assertEquals(2, trainingPosition.getExclusions().size());
	assertNotNull(trainingPosition.getExclusions().get(0).getExclusionFrom());
	assertNotNull(trainingPosition.getExclusions().get(0).getExclusionUntil());
	assertEquals(LocalDate.parse("2018-01-01"), trainingPosition.getExclusions().get(0).getExclusionFrom());
	assertEquals(LocalDate.parse("2018-02-01"), trainingPosition.getExclusions().get(0).getExclusionUntil());
	assertNotNull(trainingPosition.getExclusions().get(1).getExclusionFrom());
	assertNotNull(trainingPosition.getExclusions().get(1).getExclusionUntil());
	assertEquals(LocalDate.parse("2019-01-01"), trainingPosition.getExclusions().get(1).getExclusionFrom());
	assertEquals(LocalDate.parse("2019-02-01"), trainingPosition.getExclusions().get(1).getExclusionUntil());
    }

    @Test
    @WithMockUser
    public void testCreateTrainingPositionWithExistingTrainingSiteAndExistingDoctorAndExistingAuthorisation() throws Exception {
	MockHttpServletRequestBuilder postRequest = post(RequestMappings.TRAININGPOSITIONS_UPDATE);
	postRequest.contentType(MediaType.APPLICATION_FORM_URLENCODED);

	TrainingPositionField mockPosition = createMockPosition();
	addTrainingPosition(postRequest, mockPosition);
	postRequest.param(TrainingPositionController.PARAM_AUTHORISATION_FIELD, mockPosition.getAuthorisationField().getUid().toString());
	postRequest.param(TrainingPositionController.PARAM_DOCTOR, mockPosition.getAuthorisationField().getAuthorisedDoctor().getUid().toString());
	postRequest.param(TrainingPositionController.PARAM_TRAININGSITE, "-31");

	postRequest.param(TrainingPositionController.SWITCH_SELECT_AUTHORISATION, String.valueOf(TrainingPositionController.SelectAuthorisation.UseExistingAuthorisation));
	postRequest.param(TrainingPositionController.SWITCH_SELECT_SITE, String.valueOf(TrainingPositionController.SelectSite.UseExistingTrainingSite));

	ResultActions result = mockMvc.perform(postRequest);
	result.andExpect(status().isFound());

	assertEquals(7, authorisationDAO.getAuthorisationFields(null).size());
	assertEquals(6, personDAO.getAuthorisedDoctors(null).size());
	assertEquals(5, trainingSiteDAO.getTrainingSites(new TrainingSiteFilter(), null, false, false).size());
	assertEquals(6, trainingSiteDAO.getTrainingSites(departmentFilter, null, false, false).size());
	assertEquals(1, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	assertNotNull(trainingPositionDAO.getTrainingPositionField(1L));

	compare(mockPosition, trainingPositionDAO.getTrainingPositionField(1L));
    }

    @Test
    @WithMockUser
    public void testCreateTrainingPositionWithExistingTrainingSiteAndExistingDoctorAndNewAuthorisation() throws Exception {
	MockHttpServletRequestBuilder postRequest = post(RequestMappings.TRAININGPOSITIONS_UPDATE);
	postRequest.contentType(MediaType.APPLICATION_FORM_URLENCODED);

	TrainingPositionField mockPosition = createMockPosition();
	AuthorisationField authorisation = createMockAuthorisation(mockPosition.getAuthorisationField().getAuthorisedDoctor());

	addAuthorisation1(postRequest, authorisation);
	addTrainingPosition(postRequest, mockPosition);
	postRequest.param(TrainingPositionController.PARAM_DOCTOR, authorisation.getAuthorisedDoctor().getUid().toString());
	postRequest.param(TrainingPositionController.PARAM_TRAININGSITE, "-31");

	postRequest.param(TrainingPositionController.SWITCH_SELECT_AUTHORISATION, String.valueOf(TrainingPositionController.SelectAuthorisation.CreateNewAuthorisation));
	postRequest.param(TrainingPositionController.SWITCH_SELECT_SITE, String.valueOf(TrainingPositionController.SelectSite.UseExistingTrainingSite));

	ResultActions result = mockMvc.perform(postRequest);
	result.andExpect(status().isFound());

	assertEquals(8, authorisationDAO.getAuthorisationFields(null).size());
	assertEquals(6, personDAO.getAuthorisedDoctors(null).size());
	assertEquals(5, trainingSiteDAO.getTrainingSites(new TrainingSiteFilter(), null, false, false).size());
	assertEquals(6, trainingSiteDAO.getTrainingSites(departmentFilter, null, false, false).size());
	assertEquals(1, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	assertNotNull(trainingPositionDAO.getTrainingPositionField(1L));

	mockPosition.setAuthorisationField(authorisationDAO.getAuthorisationField(8L));
	compare(mockPosition, trainingPositionDAO.getTrainingPositionField(1L));
    }

    @Test
    @WithMockUser
    public void testCreateTrainingPositionWithExistingTrainingSiteWithDepartmentAndNewDoctorAndNewAuthorisation() throws Exception {
	MockHttpServletRequestBuilder postRequest = post(RequestMappings.TRAININGPOSITIONS_UPDATE);
	postRequest.contentType(MediaType.APPLICATION_FORM_URLENCODED);

	TrainingPositionField mockPosition = createMockPosition();
	AuthorisedDoctor doctor = createMockDoctor();
	AuthorisationField authorisation = createMockAuthorisation(doctor);

	addAuthorisedDoctor(postRequest, doctor);
	addAuthorisation(postRequest, authorisation);
	addTrainingPosition(postRequest, mockPosition);

	postRequest.param(TrainingPositionController.PARAM_TRAININGSITE, "-31");
	postRequest.param(TrainingPositionController.SWITCH_SELECT_AUTHORISATION, String.valueOf(TrainingPositionController.SelectAuthorisation.CreateNewAuthorised));
	postRequest.param(TrainingPositionController.SWITCH_SELECT_SITE, String.valueOf(TrainingPositionController.SelectSite.UseExistingTrainingSite));

	ResultActions result = mockMvc.perform(postRequest);
	result.andExpect(model().hasNoErrors());
	result.andExpect(status().isFound());

	assertEquals(8, authorisationDAO.getAuthorisationFields(null).size());
	assertEquals(7, personDAO.getAuthorisedDoctors(null).size());
	assertEquals(5, trainingSiteDAO.getTrainingSites(new TrainingSiteFilter(), null, false, false).size());
	assertEquals(6, trainingSiteDAO.getTrainingSites(departmentFilter, null, false, false).size());
	assertEquals(1, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	assertNotNull(trainingPositionDAO.getTrainingPositionField(1L));

	mockPosition.setAuthorisationField(authorisationDAO.getAuthorisationField(8L));
	compare(mockPosition, trainingPositionDAO.getTrainingPositionField(1L));
    }

    @Test
    @WithMockUser
    public void testCreateTrainingPositionWithExistingTrainingSiteWithoutDepartmentAndNewDoctorAndNewAuthorisation() throws Exception {
	MockHttpServletRequestBuilder postRequest = post(RequestMappings.TRAININGPOSITIONS_UPDATE);
	postRequest.contentType(MediaType.APPLICATION_FORM_URLENCODED);

	TrainingPositionField mockPosition = createMockPosition();
	AuthorisedDoctor doctor = createMockDoctor();
	AuthorisationField authorisation = createMockAuthorisation(doctor);

	addAuthorisedDoctor(postRequest, doctor);
	addAuthorisation(postRequest, authorisation);
	addTrainingPosition(postRequest, mockPosition);

	postRequest.param(TrainingPositionController.PARAM_TRAININGSITE, "-50");
	postRequest.param(TrainingPositionController.SWITCH_SELECT_AUTHORISATION, String.valueOf(TrainingPositionController.SelectAuthorisation.CreateNewAuthorised));
	postRequest.param(TrainingPositionController.SWITCH_SELECT_SITE, String.valueOf(TrainingPositionController.SelectSite.UseExistingTrainingSite));

	ResultActions result = mockMvc.perform(postRequest);
	result.andExpect(status().isFound());

	assertEquals(8, authorisationDAO.getAuthorisationFields(null).size());
	assertEquals(7, personDAO.getAuthorisedDoctors(null).size());
	assertEquals(5, trainingSiteDAO.getTrainingSites(new TrainingSiteFilter(), null, false, false).size());
	assertEquals(6, trainingSiteDAO.getTrainingSites(departmentFilter, null, false, false).size());
	assertEquals(1, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	assertNotNull(trainingPositionDAO.getTrainingPositionField(1L));

	mockPosition.setAuthorisationField(authorisationDAO.getAuthorisationField(8L));
	compare(mockPosition, trainingPositionDAO.getTrainingPositionField(1L));
    }

    @Test
    @WithMockUser
    public void testCreateTrainingPositionWithNewTrainingSiteAndExistingDoctorAndNewAuthorisation() throws Exception {
	MockHttpServletRequestBuilder postRequest = post(RequestMappings.TRAININGPOSITIONS_UPDATE);
	postRequest.contentType(MediaType.APPLICATION_FORM_URLENCODED);

	TrainingPositionField mockPosition = createMockPosition();
	TrainingSite trainingSite = createMockTrainingSiteWithDepartment();
	AuthorisationField authorisation = createMockAuthorisation(mockPosition.getAuthorisationField().getAuthorisedDoctor());

	addTrainingSite(postRequest, trainingSite, true);
	addTrainingPosition(postRequest, mockPosition);
	addAuthorisation1(postRequest, authorisation);

	postRequest.param(TrainingPositionController.PARAM_DOCTOR, String.valueOf(authorisation.getAuthorisedDoctor().getUid()));
	postRequest.param(TrainingPositionController.SWITCH_SELECT_AUTHORISATION, String.valueOf(TrainingPositionController.SelectAuthorisation.CreateNewAuthorisation));
	postRequest.param(TrainingPositionController.SWITCH_SELECT_SITE, TrainingPositionController.SelectSite.CreateNewTrainingSite.toString());
	postRequest.param(TrainingPositionController.SWITCH_CREATE_DEPARTMENT, AbstractMedicalTrainingController.SWITCH_CHECKBOX_CHECKED);

	ResultActions result = mockMvc.perform(postRequest);
	result.andExpect(model().hasNoErrors());
	result.andExpect(status().isFound());

	assertEquals(8, authorisationDAO.getAuthorisationFields(null).size());
	assertEquals(6, personDAO.getAuthorisedDoctors(null).size());
	assertEquals(6, trainingSiteDAO.getTrainingSites(new TrainingSiteFilter(), null, false, false).size());
	assertEquals(7, trainingSiteDAO.getTrainingSites(departmentFilter, null, false, false).size());
	assertEquals(1, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	assertNotNull(trainingPositionDAO.getTrainingPositionField(1L));

	mockPosition.setAuthorisationField(authorisationDAO.getAuthorisationField(8L));
	compare(mockPosition, trainingPositionDAO.getTrainingPositionField(1L));
    }

    @Test
    @WithMockUser
    public void testCreateTrainingPositionWithNewTrainingSiteWithDepartmentAndImplicitAddressAndNewDoctorAndNewAuthorisation() throws Exception {
	MockHttpServletRequestBuilder postRequest = post(RequestMappings.TRAININGPOSITIONS_UPDATE);
	postRequest.contentType(MediaType.APPLICATION_FORM_URLENCODED);

	TrainingPositionField trainingPosition = createMockPosition();
	AuthorisedDoctor authorisedDoctor = createMockDoctor();
	AuthorisationField authorisation = createMockAuthorisation(authorisedDoctor);
	TrainingSite trainingSite = createMockTrainingSiteWithDepartment();

	addAuthorisedDoctor(postRequest, authorisedDoctor);
	addAuthorisation(postRequest, authorisation);
	addTrainingSite(postRequest, trainingSite, false);
	addTrainingPosition(postRequest, trainingPosition);

	postRequest.param(TrainingPositionController.SWITCH_SELECT_AUTHORISATION, TrainingPositionController.SelectAuthorisation.CreateNewAuthorised.toString());
	postRequest.param(TrainingPositionController.SWITCH_SELECT_SITE, TrainingPositionController.SelectSite.CreateNewTrainingSite.toString());
	postRequest.param(TrainingPositionController.SWITCH_CREATE_DEPARTMENT, AbstractMedicalTrainingController.SWITCH_CHECKBOX_CHECKED);

	ResultActions result = mockMvc.perform(postRequest);
	result.andExpect(model().hasNoErrors());
	result.andExpect(status().isFound());

	assertEquals(8, authorisationDAO.getAuthorisationFields(null).size());
	assertEquals(7, personDAO.getAuthorisedDoctors(null).size());
	assertEquals(6, trainingSiteDAO.getTrainingSites(new TrainingSiteFilter(), null, false, false).size());
	assertEquals(7, trainingSiteDAO.getTrainingSites(departmentFilter, null, false, false).size());
	assertEquals(1, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	assertNotNull(trainingPositionDAO.getTrainingPositionField(1L));

	trainingPosition.setAuthorisationField(authorisationDAO.getAuthorisationField(8L));
	compare(trainingPosition, trainingPositionDAO.getTrainingPositionField(1L));

	System.out.println("compare " + trainingSite + ", " + trainingSite.getChildren());
	System.out.println("\twith " + trainingSiteDAO.getTrainingSite(52L, true, true) + ", " + trainingSiteDAO.getTrainingSite(52L, true, true).getChildren());

	compare(trainingSite, trainingSiteDAO.getTrainingSite(52L, true, true), true);
    }

    @Test
    @WithMockUser
    public void testCreateTrainingPositionWithNewTrainingSiteWithDepartmentAndNewDoctorAndNewAuthorisation() throws Exception {
	MockHttpServletRequestBuilder postRequest = post(RequestMappings.TRAININGPOSITIONS_UPDATE);
	postRequest.contentType(MediaType.APPLICATION_FORM_URLENCODED);

	TrainingPositionField trainingPosition = createMockPosition();
	AuthorisedDoctor authorisedDoctor = createMockDoctor();
	AuthorisationField authorisation = createMockAuthorisation(authorisedDoctor);
	TrainingSite trainingSite = createMockTrainingSiteWithDepartment();

	addAuthorisedDoctor(postRequest, authorisedDoctor);
	addAuthorisation(postRequest, authorisation);
	addTrainingSite(postRequest, trainingSite, true);
	addTrainingPosition(postRequest, trainingPosition);

	postRequest.param(TrainingPositionController.SWITCH_SELECT_AUTHORISATION, TrainingPositionController.SelectAuthorisation.CreateNewAuthorised.toString());
	postRequest.param(TrainingPositionController.SWITCH_SELECT_SITE, TrainingPositionController.SelectSite.CreateNewTrainingSite.toString());
	postRequest.param(TrainingPositionController.SWITCH_CREATE_DEPARTMENT, AbstractMedicalTrainingController.SWITCH_CHECKBOX_CHECKED);

	ResultActions result = mockMvc.perform(postRequest);
	result.andExpect(model().hasNoErrors());
	result.andExpect(status().isFound());

	assertEquals(8, authorisationDAO.getAuthorisationFields(null).size());
	assertEquals(7, personDAO.getAuthorisedDoctors(null).size());
	assertEquals(6, trainingSiteDAO.getTrainingSites(new TrainingSiteFilter(), null, false, false).size());
	assertEquals(7, trainingSiteDAO.getTrainingSites(departmentFilter, null, false, false).size());
	assertEquals(1, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	assertNotNull(trainingPositionDAO.getTrainingPositionField(1L));

	trainingPosition.setAuthorisationField(authorisationDAO.getAuthorisationField(8L));
	compare(trainingPosition, trainingPositionDAO.getTrainingPositionField(1L));

	System.out.println("compare " + trainingSite + ", " + trainingSite.getChildren());
	System.out.println("\twith " + trainingSiteDAO.getTrainingSite(52L, true, true) + ", " + trainingSiteDAO.getTrainingSite(52L, true, true).getChildren());

	compare(trainingSite, trainingSiteDAO.getTrainingSite(52L, true, true), true);
    }

    @Test
    @WithMockUser
    public void testCreateTrainingPositionWithNewTrainingSiteWithoutDepartmentAndNewDoctorAndNewAuthorisation() throws Exception {
	MockHttpServletRequestBuilder postRequest = post(RequestMappings.TRAININGPOSITIONS_UPDATE);
	postRequest.contentType(MediaType.APPLICATION_FORM_URLENCODED);

	TrainingPositionField trainingPosition = createMockPosition();
	AuthorisedDoctor authorisedDoctor = createMockDoctor();
	AuthorisationField authorisation = createMockAuthorisation(authorisedDoctor);
	TrainingSite trainingSite = createMockTrainingSiteWithoutDepartment();

	addAuthorisedDoctor(postRequest, authorisedDoctor);
	addAuthorisation(postRequest, authorisation);
	addTrainingSite(postRequest, trainingSite, true);
	addTrainingPosition(postRequest, trainingPosition);

	postRequest.param(TrainingPositionController.SWITCH_SELECT_AUTHORISATION, TrainingPositionController.SelectAuthorisation.CreateNewAuthorised.toString());
	postRequest.param(TrainingPositionController.SWITCH_SELECT_SITE, TrainingPositionController.SelectSite.CreateNewTrainingSite.toString());

	ResultActions result = mockMvc.perform(postRequest);
	result.andExpect(status().isFound());

	assertEquals(8, authorisationDAO.getAuthorisationFields(null).size());
	assertEquals(7, personDAO.getAuthorisedDoctors(null).size());
	assertEquals(6, trainingSiteDAO.getTrainingSites(new TrainingSiteFilter(), null, false, false).size());
	assertEquals(6, trainingSiteDAO.getTrainingSites(departmentFilter, null, false, false).size());
	assertEquals(1, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	System.out.println(trainingPositionDAO.getTrainingPositionsField(new TrainingPositionFieldFilter(), null));
	assertNotNull(trainingPositionDAO.getTrainingPositionsField(null, false).get(0));

	compare(trainingSite, trainingSiteDAO.getTrainingSite(51L, true, true), true);

    }

    @Test
    public void testErrorWithIncompleteData() throws Exception {
	MockHttpServletRequestBuilder postRequest = post(RequestMappings.TRAININGPOSITIONS_UPDATE);
	postRequest.contentType(MediaType.APPLICATION_FORM_URLENCODED);

	postRequest.param("authorisationField.authorisedDoctor", "-1");
	postRequest.param("authorisationField.authorisedDoctor.address.city", "");
	postRequest.param("authorisationField.authorisedDoctor.address.street", "");
	postRequest.param("authorisationField.authorisedDoctor.address.zipCode", "");
	postRequest.param("authorisationField.authorisedDoctor.birthDate", "");
	postRequest.param("authorisationField.authorisedDoctor.contactInfo..email", "");
	postRequest.param("authorisationField.authorisedDoctor.contactInfo.faxNumber", "");
	postRequest.param("authorisationField.authorisedDoctor.contactInfo.phoneNumber", "");
	postRequest.param("authorisationField.authorisedDoctor.contactInfo.phoneNumber2", "");
	postRequest.param("authorisationField.authorisedDoctor.lanr", "");
	postRequest.param("authorisationField.authorisedDoctor.lastname", "");
	postRequest.param("authorisationField.authorisedDoctor.name", "");
	postRequest.param("authorisationField.authorisedDoctor.notes", "");
	postRequest.param("authorisationField.authorisedDoctor.sex", "");
	postRequest.param("authorisationField.authorisedDoctor.title", "");
	postRequest.param("authorisationField.grantedOn", "");
	postRequest.param("authorisationField.grantedOn", "");
	postRequest.param("authorisationField.maximalDuration", "");
	postRequest.param("authorisationField.maximalDuration", "");
	postRequest.param("authorisationField.trainingScheme", "1");
	postRequest.param("authorisationField.trainingScheme", "1");
	postRequest.param("authorisationField.trainingSite", "50");
	postRequest.param("authorisationField.trainingSite.address.city", "");
	postRequest.param("authorisationField.trainingSite.address.street", "");
	postRequest.param("authorisationField.trainingSite.address.zipCode", "");
	postRequest.param("authorisationField.trainingSite.children[0].address.city", "");
	postRequest.param("authorisationField.trainingSite.children[0].address.street", "");
	postRequest.param("authorisationField.trainingSite.children[0].address.zipCode", "");
	postRequest.param("authorisationField.trainingSite.children[0].contactInfo.email", "");
	postRequest.param("authorisationField.trainingSite.children[0].contactInfo.faxNumber", "");
	postRequest.param("authorisationField.trainingSite.children[0].contactInfo.phoneNumber", "");
	postRequest.param("authorisationField.trainingSite.children[0].contactInfo.phoneNumber2", "");
	postRequest.param("authorisationField.trainingSite.children[0].medicalField", "3");
	postRequest.param("authorisationField.trainingSite.children[0].name", "");
	postRequest.param("authorisationField.trainingSite.contactInfo.email", "");
	postRequest.param("authorisationField.trainingSite.contactInfo.faxNumber", "");
	postRequest.param("authorisationField.trainingSite.contactInfo.phoneNumber", "");
	postRequest.param("authorisationField.trainingSite.contactInfo.phoneNumber2", "");
	postRequest.param("authorisationField.trainingSite.medcialField", "6");
	postRequest.param("authorisationField.trainingSite.name:", "");
	postRequest.param("authorisationField.trainingSite.trainingSiteType", "DOCTORSOFFICE");
	postRequest.param("availableFrom", "2018-07-01");
	postRequest.param("availableUntil", "2020-05-31");
	postRequest.param("capacity", "1");
	postRequest.param("partTimeAvailable", "true");
	postRequest.param("selectAuthorised", "UseExistingAuthorisation");
	postRequest.param("selectSite", "UseExistingTrainingSite");

	ResultActions result = mockMvc.perform(postRequest);
	result.andExpect(model().hasErrors());
	result.andExpect(status().isOk());
    }

    private void addAuthorisation(MockHttpServletRequestBuilder requestBuilder, AuthorisationField authorisation) {
	// TODO: Der Controller unterscheidet gerade noch ins Array rein, eventuell kann
	// das durch clevere Umbenennung von Feldern im JSP verhindert werden
	requestBuilder.param(TrainingPositionController.PARAM_AUTHORISATION_GRANTED_ON, "");
	requestBuilder.param(TrainingPositionController.PARAM_AUTHORISATION_GRANTED_ON, authorisation.getGrantedOn().toString());
	requestBuilder.param(TrainingPositionController.PARAM_AUTHORISATION_GRANTED_TO, "");
	requestBuilder.param(TrainingPositionController.PARAM_AUTHORISATION_GRANTED_TO, authorisation.getGrantedTo().toString());
	requestBuilder.param(TrainingPositionController.PARAM_AUTHORISATION_MAXIMAL_DURATION, "");
	requestBuilder.param(TrainingPositionController.PARAM_AUTHORISATION_MAXIMAL_DURATION, String.valueOf(authorisation.getMaximalDuration()));
	// requestBuilder.param(TrainingPositionController.PARAM_AUTHORISATION_FIELD_NAME,
	// "");
	// requestBuilder.param(TrainingPositionController.PARAM_AUTHORISATION_FIELD_NAME,
	// authorisation.getName());
	requestBuilder.param(TrainingPositionController.PARAM_AUTHORISATION_TRAINING_SCHEME, String.valueOf(authorisation.getTrainingScheme().getUid()));
    }

    private void addAuthorisation1(MockHttpServletRequestBuilder requestBuilder, AuthorisationField authorisation) {
	requestBuilder.param(TrainingPositionController.PARAM_AUTHORISATION_GRANTED_ON, authorisation.getGrantedOn().toString());
	requestBuilder.param(TrainingPositionController.PARAM_AUTHORISATION_GRANTED_TO, authorisation.getGrantedTo().toString());
	requestBuilder.param(TrainingPositionController.PARAM_AUTHORISATION_MAXIMAL_DURATION, String.valueOf(authorisation.getMaximalDuration()));
	requestBuilder.param(TrainingPositionController.PARAM_AUTHORISATION_TRAINING_SCHEME, String.valueOf(authorisation.getTrainingScheme().getUid()));
	// requestBuilder.param(TrainingPositionController.PARAM_AUTHORISATION_FIELD_NAME,
	// authorisation.getName());

    }

    private void addAuthorisedDoctor(MockHttpServletRequestBuilder requestBuilder, AuthorisedDoctor authorisedDoctor) {
	requestBuilder.param(TrainingPositionController.PARAM_DOCTOR_FIRSTNAME, authorisedDoctor.getName());
	requestBuilder.param(TrainingPositionController.PARAM_DOCTOR_LASTNAME, authorisedDoctor.getLastName());
	requestBuilder.param(TrainingPositionController.PARAM_DOCTOR_STREET, authorisedDoctor.getAddress().getStreet());
	requestBuilder.param(TrainingPositionController.PARAM_DOCTOR_ZIPCODE, authorisedDoctor.getAddress().getZipCode());
	requestBuilder.param(TrainingPositionController.PARAM_DOCTOR_CITY, authorisedDoctor.getAddress().getCity());
    }

    private void addTrainingPosition(MockHttpServletRequestBuilder requestBuilder, TrainingPositionField trainingPosition) {
	requestBuilder.param("availableFrom", String.valueOf(trainingPosition.getAvailableFrom()));
	requestBuilder.param("availableUntil", String.valueOf(trainingPosition.getAvailableUntil()));
	requestBuilder.param("capacity", String.valueOf(trainingPosition.getCapacity()));
	requestBuilder.param("partTimeAvailable", String.valueOf(trainingPosition.isPartTimeAvailable()));
	requestBuilder.param("name", trainingPosition.getName());
    }

    private void addTrainingSite(MockHttpServletRequestBuilder requestBuilder, TrainingSite trainingSite, boolean addDepartmentAddress) {
	requestBuilder.param(TrainingPositionController.PARAM_TRAININGSITE_NAME, trainingSite.getName());
	requestBuilder.param(TrainingPositionController.PARAM_TRAININGSITE_STREET, trainingSite.getAddress().getStreet());
	requestBuilder.param(TrainingPositionController.PARAM_TRAININGSITE_ZIPCODE, trainingSite.getAddress().getZipCode());
	requestBuilder.param(TrainingPositionController.PARAM_TRAININGSITE_CITY, trainingSite.getAddress().getCity());
	requestBuilder.param(TrainingPositionController.PARAM_TRAININGSITE_TYPE, String.valueOf(trainingSite.getTrainingSiteType()));

	if (0 == trainingSite.getChildren().size()) {
	    requestBuilder.param(TrainingPositionController.PARAM_TRAININGSITE_MEDICALFIELD, String.valueOf(trainingSite.getMedicalField().getUid()));
	    requestBuilder.param(TrainingPositionController.PARAM_TRAININGSITE_TREATMENT_TYPE, String.valueOf(trainingSite.getTreatmentType()));
	} else {
	    requestBuilder.param(TrainingPositionController.PARAM_DEPARTMENT_NAME, trainingSite.getChildren().get(0).getName());
	    requestBuilder.param(TrainingPositionController.PARAM_DEPARTMENT_MEDICALFIELD, String.valueOf(trainingSite.getChildren().get(0).getMedicalField().getUid()));
	    requestBuilder.param(TrainingPositionController.PARAM_DEPARTMENT_TREATMENT_TYPE, String.valueOf(trainingSite.getChildren().get(0).getTreatmentType()));

	    if (addDepartmentAddress) {
		requestBuilder.param(TrainingPositionController.PARAM_DEPARTMENT_STREET, trainingSite.getChildren().get(0).getAddress().getStreet());
		requestBuilder.param(TrainingPositionController.PARAM_DEPARTMENT_ZIPCODE, trainingSite.getChildren().get(0).getAddress().getZipCode());
		requestBuilder.param(TrainingPositionController.PARAM_DEPARTMENT_CITY, trainingSite.getChildren().get(0).getAddress().getCity());
	    }
	}
    }

    private void compare(TrainingPositionField expected, TrainingPositionField actual) {
	assertEquals(expected.getAuthorisationField(), actual.getAuthorisationField());
	assertEquals(expected.getAvailableFrom(), actual.getAvailableFrom());
	assertEquals(expected.getAvailableUntil(), actual.getAvailableUntil());
	assertEquals(expected.getCapacity(), actual.getCapacity());
	assertEquals(expected.isPartTimeAvailable(), actual.isPartTimeAvailable());
    }

    private void compare(TrainingSite expected, TrainingSite actual, boolean compareChildren) {
	compare(expected.getAddress(), actual.getAddress());

	if (null == expected.getParent()) {
	    assertNull(actual.getParent());
	} else {
	    compare(expected.getParent(), actual.getParent(), false);
	}

	if (compareChildren) {
	    assertEquals(expected.getChildren().size(), actual.getChildren().size());

	    for (int i = 0; i < expected.getChildren().size(); i++) {
		compare(expected.getChildren().get(i), actual.getChildren().get(i), false);
	    }
	}

	assertEquals(expected.getName(), actual.getName());
	assertEquals(expected.getMedicalField(), actual.getMedicalField());
	assertEquals(expected.getEquipments(), actual.getEquipments());
	assertEquals(expected.getTreatmentType(), actual.getTreatmentType());
    }

    private AuthorisationField createMockAuthorisation(AuthorisedDoctor authorisedDoctor) {
	AuthorisationField authorisationField = new AuthorisationField();
	authorisationField.setGrantedOn(LocalDate.parse("2018-01-01"));
	authorisationField.setGrantedTo(LocalDate.parse("2025-12-31"));
	authorisationField.setMaximalDuration(6);
	authorisationField.setTrainingScheme(trainingSchemeDAO.getTrainingScheme(-1L, false));

	if (null != authorisedDoctor) {
	    authorisationField.setAuthorisedDoctor(authorisedDoctor);
	}

	authorisationField.setName("Mock Authorisation");

	return authorisationField;
    }

    private AuthorisedDoctor createMockDoctor() {
	AuthorisedDoctor authorisedDoctor = new AuthorisedDoctor();
	authorisedDoctor.setName("Vorname");
	authorisedDoctor.setLastName("Nachname");
	authorisedDoctor.getAddress().setStreet("Goerdelerring 9");
	authorisedDoctor.getAddress().setZipCode("04109");
	authorisedDoctor.getAddress().setCity("Leipzig");

	return authorisedDoctor;
    }

    private TrainingPositionField createMockPosition() {
	TrainingPositionField position = new TrainingPositionField();

	position.setAuthorisationField(authorisationDAO.getAuthorisationField(-5L));
	position.setAvailableFrom(LocalDate.parse("2018-01-01"));
	position.setAvailableUntil(LocalDate.parse("2018-03-31"));
	position.setCapacity(1);
	position.setPartTimeAvailable(true);
	position.setName("Mock Position");

	return position;
    }

    private TrainingSite createMockTrainingSiteWithDepartment() {
	Address address = new Address();
	address.setStreet("Goerdelerring 9");
	address.setZipCode("04109");
	address.setCity("Leipzig");

	TrainingSite trainingSite = new TrainingSite();
	trainingSite.setAddress(address);
	trainingSite.setName("TrainingSite");
	trainingSite.setTrainingSiteType(TrainingSiteType.CLINIC);

	TrainingSite department = new TrainingSite();
	department.setMedicalField(trainingDAO.getMedicalField(2L));
	department.setTreatmentType(TreatmentType.Inpatient);
	department.setName("Department");
	department.setAddress(address);
	department.setParent(trainingSite);
	department.setTrainingSiteType(TrainingSiteType.DEPARTMENT);

	return trainingSite;
    }

    private TrainingSite createMockTrainingSiteWithoutDepartment() {
	Address address = new Address();
	address.setStreet("Goerdelerring 9");
	address.setZipCode("04109");
	address.setCity("Leipzig");

	TrainingSite trainingSite = new TrainingSite();
	trainingSite.setMedicalField(trainingDAO.getMedicalField(-2L));
	trainingSite.setTreatmentType(TreatmentType.Inpatient);
	trainingSite.setAddress(address);
	trainingSite.setName("TrainingSite");
	trainingSite.setTrainingSiteType(TrainingSiteType.DOCTORSOFFICE);

	return trainingSite;
    }
}
