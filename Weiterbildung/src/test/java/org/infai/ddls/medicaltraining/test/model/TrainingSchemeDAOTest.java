package org.infai.ddls.medicaltraining.test.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import org.infai.ddls.medicaltraining.model.AuthorisationField;
import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;
import org.infai.ddls.medicaltraining.model.MedicalField;
import org.infai.ddls.medicaltraining.model.TrainingPositionField;
import org.infai.ddls.medicaltraining.model.TrainingScheme;
import org.infai.ddls.medicaltraining.model.TrainingSchemeSegment;
import org.infai.ddls.medicaltraining.model.TrainingSite;
import org.infai.ddls.medicaltraining.model.TreatmentType;
import org.infai.ddls.medicaltraining.repo.TrainingSchemeDAO;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

public class TrainingSchemeDAOTest extends AbstractSpringRepositoryTest {
    @Autowired
    private TrainingSchemeDAO dao;

    @Test
    @WithMockUser
    public void testCreateTrainingScheme() throws Exception {
	// Definition Weiterbildung FA Allgemeinmedizin nach WBOSA
	MedicalField innereMedizinField = new MedicalField("Innere Medizin");
	MedicalField chirurgieField = new MedicalField("Chirurgie");
	MedicalField kinderJugendMedizinField = new MedicalField("Kinder- und Jugendmedizin");
	MedicalField allgemeinMedizinField = new MedicalField("Allgemeinmedizin");
	MedicalField anaesthesiologie = new MedicalField("Anästhesiologie");

	MedicalField unmittelbareVersorgungField = new MedicalField("Unmittelbare Patientenversorgung (freie Wahl)");
	MedicalField hausarztVersorgungField = new MedicalField("Hausärztliche Versorgung (freie Wahl)");

	TrainingSchemeSegment innereMedizinSegment = new TrainingSchemeSegment(18, innereMedizinField, false, true);
	TrainingSchemeSegment chirurgieSegment = new TrainingSchemeSegment(6, chirurgieField, true, true);
	TrainingSchemeSegment kinderJugendMedizinSegment = new TrainingSchemeSegment(6, kinderJugendMedizinField, true, true);
	TrainingSchemeSegment unmittelbareVersorgungSegment = new TrainingSchemeSegment(6, unmittelbareVersorgungField, true, true);
	TrainingSchemeSegment allgemeinMedizinSegment = new TrainingSchemeSegment(18, allgemeinMedizinField, true, false);
	TrainingSchemeSegment hausarztVersorgungSegment = new TrainingSchemeSegment(6, hausarztVersorgungField, true, false);

	TrainingScheme allgemeinMedizinTraining = new TrainingScheme("Allgemeinmedizin", "FA Allgemeinmedizin", innereMedizinSegment, chirurgieSegment, kinderJugendMedizinSegment, unmittelbareVersorgungSegment, allgemeinMedizinSegment, hausarztVersorgungSegment);

	dao.save(allgemeinMedizinTraining);

	System.out.println(allgemeinMedizinTraining.getName() + ": " + allgemeinMedizinTraining.getTrainingSchemeSegments());
	System.out.println(dao.getTrainingSchemes());

	// Weiterbildungsstätten definieren
	TrainingSite parkkrankenhaus = new TrainingSite();
	parkkrankenhaus.setName("Parkkrankenhaus");
	TrainingSite tsd1 = new TrainingSite();
	tsd1.setParent(parkkrankenhaus);
	tsd1.setMedicalField(allgemeinMedizinField);
	tsd1.setTreatmentType(TreatmentType.Outpatient);
	TrainingSite tsd2 = new TrainingSite();
	tsd2.setParent(parkkrankenhaus);
	tsd2.setMedicalField(anaesthesiologie);
	tsd2.setTreatmentType(TreatmentType.Inpatient);
	TrainingSite tsd3 = new TrainingSite();
	tsd3.setParent(parkkrankenhaus);
	tsd3.setMedicalField(anaesthesiologie);
	tsd3.setTreatmentType(TreatmentType.Outpatient);

	System.out.println(parkkrankenhaus.getName() + ": " + parkkrankenhaus.getChildren());

	// Weiterbildungsbefugte definieren
	AuthorisedDoctor authorisedDoc1 = new AuthorisedDoctor();
	authorisedDoc1.setName("Frau");
	authorisedDoc1.setLastName("Weiterbilderin");
	AuthorisedDoctor authorisedDoc2 = new AuthorisedDoctor();
	authorisedDoc2.setName("Herr");
	authorisedDoc2.setLastName("Weiterbilder");

	// Weiterbildungsbefugnisse definieren
	AuthorisationField authorisation1 = new AuthorisationField();
	authorisation1.setAuthorisedDoctor(authorisedDoc1);
	authorisation1.setTrainingSite(tsd1);
	authorisation1.setGrantedOn(LocalDate.parse("2017-01-01"));
	authorisation1.setGrantedTo(LocalDate.parse("2020-12-31"));

	AuthorisationField authorisation2 = new AuthorisationField();
	authorisation2.setAuthorisedDoctor(authorisedDoc2);
	authorisation2.setTrainingSite(tsd2);
	authorisation2.setGrantedOn(LocalDate.parse("2017-01-01"));
	authorisation2.setGrantedTo(LocalDate.parse("2020-12-31"));

	System.out.println(authorisation1);
	System.out.println(authorisation2);

	// Weiterbildungsstellen definieren
	TrainingPositionField trainingPosition1 = new TrainingPositionField(authorisation1, LocalDate.parse("01.01.2018", DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)), LocalDate.parse("30.06.2018", DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)), 2, false);
	TrainingPositionField trainingPosition2 = new TrainingPositionField(authorisation2, LocalDate.parse("01.01.2018", DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)), LocalDate.parse("31.12.2018", DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)), 2, true);

	System.out.println(trainingPosition1);
	System.out.println(trainingPosition2);
    }
}
