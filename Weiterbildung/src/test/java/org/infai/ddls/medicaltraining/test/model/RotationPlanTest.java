package org.infai.ddls.medicaltraining.test.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.List;

import org.infai.ddls.medicaltraining.model.FundableType;
import org.infai.ddls.medicaltraining.model.RotationPlanSegment;
import org.infai.ddls.medicaltraining.model.TrainingScheme;
import org.infai.ddls.medicaltraining.model.TrainingSchemeSegment;
import org.infai.ddls.medicaltraining.model.TreatmentType;
import org.infai.ddls.medicaltraining.test.AbstractTrainingAppTest;
import org.infai.ddls.medicaltraining.test.repo.Scenario;
import org.junit.Test;

public class RotationPlanTest extends AbstractTrainingAppTest {
    @Test
    public void testGetFinishedSegmentsByDate() throws Exception {
	List<RotationPlanSegment> rotationPlanSegments = Scenario.rotationplanAllgemeinmedizin.getFinishedSegments(LocalDate.parse("2018-01-01"));
	assertNotNull(rotationPlanSegments);
	assertEquals(0, rotationPlanSegments.size());

	rotationPlanSegments = Scenario.rotationplanAllgemeinmedizin.getFinishedSegments(LocalDate.parse("2021-01-01"));
	assertList(rotationPlanSegments, Scenario.segmentInnereStat, Scenario.segmentChirurgieAmb, Scenario.segmentKJMAmb, Scenario.segmentInnereAmb);
    }

    @Test
    public void testGetFinishedSegmentsByDateAndTrainingSchemeSegment() throws Exception {
	Scenario.segmentInnereStat.setAssignedSegment(Scenario.innereMedizinSegment);
	List<RotationPlanSegment> rotationPlanSegments = Scenario.rotationplanAllgemeinmedizin.getFinishedSegments(LocalDate.parse("2021-01-01"), Scenario.innereMedizinSegment);
	assertList(rotationPlanSegments, Scenario.segmentInnereStat);

	Scenario.segmentChirurgieAmb.setAssignedSegment(Scenario.unmittelbareSegment);
	Scenario.segmentKJMAmb.setAssignedSegment(Scenario.unmittelbareSegment);
	Scenario.segmentInnereAmb.setAssignedSegment(Scenario.unmittelbareSegment);
	rotationPlanSegments = Scenario.rotationplanAllgemeinmedizin.getFinishedSegments(LocalDate.parse("2021-01-01"), Scenario.unmittelbareSegment);
	assertList(rotationPlanSegments, Scenario.segmentChirurgieAmb, Scenario.segmentKJMAmb, Scenario.segmentInnereAmb);

	Scenario.segmentInnereStat.setAssignedSegment(null);
    }

    @Test
    public void testGetFundableTrainingSchemeSegments() throws Exception {
	TrainingScheme trainingScheme = Scenario.allgemeinMedizinLSA;

	List<TrainingSchemeSegment> fundableSegments = Scenario.segmentAllgemeinAmb1.getFundableTrainingSchemeSegments(trainingScheme);
	assertNotNull(fundableSegments);
	assertEquals(3, fundableSegments.size());
	assertFalse(fundableSegments.contains(Scenario.innereMedizinSegment));
	assertFalse(fundableSegments.contains(Scenario.chirurgieSegment));
	assertFalse(fundableSegments.contains(Scenario.kjMedizinSegment));
	assertTrue(fundableSegments.contains(Scenario.unmittelbareSegment));
	assertTrue(fundableSegments.contains(Scenario.allgemeinMedizinSegment));
	assertTrue(fundableSegments.contains(Scenario.hausarztSegment));

	fundableSegments = Scenario.segmentChirurgieAmb.getFundableTrainingSchemeSegments(trainingScheme);
	assertNotNull(fundableSegments);
	assertEquals(2, fundableSegments.size());
	assertFalse(fundableSegments.contains(Scenario.innereMedizinSegment));
	assertTrue(fundableSegments.contains(Scenario.chirurgieSegment));
	assertFalse(fundableSegments.contains(Scenario.kjMedizinSegment));
	assertTrue(fundableSegments.contains(Scenario.unmittelbareSegment));
	assertFalse(fundableSegments.contains(Scenario.allgemeinMedizinSegment));
	assertFalse(fundableSegments.contains(Scenario.hausarztSegment));

	fundableSegments = Scenario.segmentInnereStat.getFundableTrainingSchemeSegments(trainingScheme);
	assertNotNull(fundableSegments);
	assertEquals(2, fundableSegments.size());
	assertTrue(fundableSegments.contains(Scenario.innereMedizinSegment));
	assertFalse(fundableSegments.contains(Scenario.chirurgieSegment));
	assertFalse(fundableSegments.contains(Scenario.kjMedizinSegment));
	assertTrue(fundableSegments.contains(Scenario.unmittelbareSegment));
	assertFalse(fundableSegments.contains(Scenario.allgemeinMedizinSegment));
	assertFalse(fundableSegments.contains(Scenario.hausarztSegment));
    }

    @Test
    public void testGetFundableTypeWithoutAssignments() throws Exception {
	assertEquals(FundableType.Fundable, Scenario.segmentAllgemeinAmb1.getFundableType(LocalDate.parse("2018-01-01"), Scenario.allgemeinMedizinSegment));
	assertEquals(FundableType.PartiallyFundable, Scenario.segmentAllgemeinAmb1.getFundableType(LocalDate.parse("2018-01-01"), Scenario.hausarztSegment));
	assertEquals(FundableType.PartiallyFundable, Scenario.segmentAllgemeinAmb1.getFundableType(LocalDate.parse("2018-01-01"), Scenario.unmittelbareSegment));
	assertEquals(FundableType.NotFundable, Scenario.segmentAllgemeinAmb1.getFundableType(LocalDate.parse("2018-01-01"), Scenario.chirurgieSegment));
	assertEquals(FundableType.NotFundable, Scenario.segmentAllgemeinAmb1.getFundableType(LocalDate.parse("2018-01-01"), Scenario.innereMedizinSegment));
	assertEquals(FundableType.NotFundable, Scenario.segmentAllgemeinAmb1.getFundableType(LocalDate.parse("2018-01-01"), Scenario.kjMedizinSegment));

	assertEquals(FundableType.NotFundable, Scenario.segmentAllgemeinStat.getFundableType(LocalDate.parse("2018-01-01"), Scenario.allgemeinMedizinSegment));
    }

    @Test
    public void testGetPlannedSegmentsByDate() throws Exception {
	List<RotationPlanSegment> rotationPlanSegments = Scenario.rotationplanAllgemeinmedizin.getPlannedSegments(LocalDate.parse("2018-01-01"));
	assertList(rotationPlanSegments, Scenario.segmentInnereStat, Scenario.segmentChirurgieAmb, Scenario.segmentKJMAmb, Scenario.segmentInnereAmb, Scenario.segmentAllgemeinAmb1, Scenario.segmentAllgemeinStat);

	rotationPlanSegments = Scenario.rotationplanAllgemeinmedizin.getPlannedSegments(LocalDate.parse("2020-01-01"));
	assertList(rotationPlanSegments, Scenario.segmentKJMAmb, Scenario.segmentInnereAmb, Scenario.segmentAllgemeinAmb1, Scenario.segmentAllgemeinStat);
    }

    @Test
    public void testGetPlannedSegmentsByDateAndTrainingSchemeSegment() throws Exception {
	Scenario.segmentInnereStat.setAssignedSegment(Scenario.innereMedizinSegment);
	List<RotationPlanSegment> rotationPlanSegments = Scenario.rotationplanAllgemeinmedizin.getPlannedSegments(LocalDate.parse("2018-01-01"), Scenario.innereMedizinSegment);
	assertList(rotationPlanSegments, Scenario.segmentInnereStat);

	Scenario.segmentInnereStat.setAssignedSegment(Scenario.unmittelbareSegment);
	Scenario.segmentChirurgieAmb.setAssignedSegment(Scenario.unmittelbareSegment);
	Scenario.segmentKJMAmb.setAssignedSegment(Scenario.unmittelbareSegment);
	Scenario.segmentInnereAmb.setAssignedSegment(Scenario.unmittelbareSegment);
	Scenario.segmentAllgemeinAmb1.setAssignedSegment(Scenario.unmittelbareSegment);
	Scenario.segmentAllgemeinStat.setAssignedSegment(Scenario.unmittelbareSegment);
	rotationPlanSegments = Scenario.rotationplanAllgemeinmedizin.getPlannedSegments(LocalDate.parse("2018-01-01"), Scenario.unmittelbareSegment);
	assertList(rotationPlanSegments, Scenario.segmentInnereStat, Scenario.segmentChirurgieAmb, Scenario.segmentKJMAmb, Scenario.segmentInnereAmb, Scenario.segmentAllgemeinAmb1, Scenario.segmentAllgemeinStat);

	Scenario.segmentInnereStat.setAssignedSegment(Scenario.innereMedizinSegment);
	Scenario.segmentInnereAmb.setAssignedSegment(Scenario.innereMedizinSegment);
	rotationPlanSegments = Scenario.rotationplanAllgemeinmedizin.getPlannedSegments(LocalDate.parse("2018-01-01"), Scenario.innereMedizinSegment);
	assertList(rotationPlanSegments, Scenario.segmentInnereStat, Scenario.segmentInnereAmb);

	Scenario.segmentInnereStat.setAssignedSegment(null);
	Scenario.segmentChirurgieAmb.setAssignedSegment(null);
	Scenario.segmentKJMAmb.setAssignedSegment(null);
	Scenario.segmentInnereAmb.setAssignedSegment(null);
	Scenario.segmentAllgemeinAmb1.setAssignedSegment(null);
	Scenario.segmentAllgemeinStat.setAssignedSegment(null);
    }

    @Test
    public void testIsFundableFor() throws Exception {
	assertTrue(Scenario.segmentAllgemeinAmb1.isFundableFor(Scenario.allgemeinmedizin, TreatmentType.Outpatient));
	assertTrue(Scenario.segmentAllgemeinAmb1.isFundableFor(Scenario.hausaerztlicheVersorgung, TreatmentType.Outpatient));
	assertTrue(Scenario.segmentAllgemeinAmb1.isFundableFor(Scenario.unmittelbarePatientenversorgung, TreatmentType.Outpatient));

	assertFalse(Scenario.segmentAllgemeinAmb1.isFundableFor(Scenario.allgemeinmedizin, TreatmentType.Inpatient));
	assertFalse(Scenario.segmentAllgemeinAmb1.isFundableFor(Scenario.hausaerztlicheVersorgung, TreatmentType.Inpatient));
	assertFalse(Scenario.segmentAllgemeinAmb1.isFundableFor(Scenario.unmittelbarePatientenversorgung, TreatmentType.Inpatient));

	assertTrue(Scenario.segmentAllgemeinAmb1.isFundableFor(Scenario.allgemeinmedizin, null));
	assertTrue(Scenario.segmentAllgemeinAmb1.isFundableFor(Scenario.hausaerztlicheVersorgung, null));
	assertTrue(Scenario.segmentAllgemeinAmb1.isFundableFor(Scenario.unmittelbarePatientenversorgung, null));

	assertTrue(Scenario.segmentInnereAmb.isFundableFor(Scenario.innereMedizin, TreatmentType.Outpatient));
	assertTrue(Scenario.segmentInnereStat.isFundableFor(Scenario.innereMedizin, TreatmentType.Inpatient));

	assertTrue(Scenario.segmentInnereAmb.isFundableFor(Scenario.innereMedizin, null));
    }

    @Test
    public void testNotFundableWithAssignments() throws Exception {
	Scenario.segmentAllgemeinAmb1.setAssignedSegment(Scenario.allgemeinMedizinSegment);

	assertEquals(FundableType.NotFundable, Scenario.segmentAllgemeinStat.getFundableType(LocalDate.parse("2022-07-01"), Scenario.allgemeinMedizinSegment));

	Scenario.segmentAllgemeinAmb1.setAssignedSegment(null);
    }

    @Test
    public void testPartiallyFundableWithAssignments() throws Exception {
	Scenario.segmentAllgemeinStat.setAssignedSegment(Scenario.allgemeinMedizinSegment);

	assertEquals(FundableType.PartiallyFundable, Scenario.segmentAllgemeinAmb1.getFundableType(LocalDate.parse("2023-07-01"), Scenario.allgemeinMedizinSegment));
	Scenario.segmentAllgemeinStat.setAssignedSegment(null);
    }
}
