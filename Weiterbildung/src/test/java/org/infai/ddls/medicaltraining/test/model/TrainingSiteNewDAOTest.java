package org.infai.ddls.medicaltraining.test.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.transaction.Transactional;

import org.infai.ddls.medicaltraining.model.Equipment;
import org.infai.ddls.medicaltraining.model.TrainingSite;
import org.infai.ddls.medicaltraining.model.TrainingSiteType;
import org.infai.ddls.medicaltraining.model.TreatmentType;
import org.infai.ddls.medicaltraining.repo.TrainingDAO;
import org.infai.ddls.medicaltraining.repo.TrainingSiteNewDAO;
import org.infai.ddls.medicaltraining.spec.TrainingSiteFilter;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.google.common.collect.Lists;

@DatabaseSetup("/medicalFieldsDataSet.xml")
public class TrainingSiteNewDAOTest extends AbstractSpringRepositoryTest {
    @Autowired
    private TrainingSiteNewDAO trainingSiteDAO;
    @Autowired
    private TrainingDAO trainingDAO;

    @Test
    @WithMockUser
    public void testAddEquipment() throws Exception {
	TrainingSite trainingSite = new TrainingSite();
	trainingSite.setName("TestSite");
	trainingSite.getAddress().setStreet("Goerdelerring 9");
	trainingSite.getAddress().setZipCode("04109");
	trainingSite.getAddress().setCity("Leipzig");
	trainingSite.setTrainingSiteType(TrainingSiteType.CLINIC);
	TrainingSite trainingSiteDepartment = new TrainingSite(trainingSite);
	trainingSiteDepartment.setName("TestSiteDepartment");
	trainingSiteDepartment.setParent(trainingSite);
	trainingSiteDepartment.setMedicalField(trainingDAO.getMedicalField(1L));
	trainingSiteDepartment.setTreatmentType(TreatmentType.Inpatient);
	trainingSiteDepartment.setTrainingSiteType(TrainingSiteType.DEPARTMENT);

	Equipment ct = trainingSiteDAO.getEquipment(1L);
	Equipment mrt = trainingSiteDAO.getEquipment(2L);

	trainingSiteDepartment.getEquipments().add(ct);
	trainingSiteDepartment.getEquipments().add(mrt);

	assertCollection(trainingSiteDepartment.getEquipments(), ct, mrt);

	trainingSite = trainingSiteDAO.save(trainingSite);

	TrainingSite actualDepartment = Lists.newArrayList(trainingSite.getChildren()).get(0);

	assertCollection(actualDepartment.getEquipments(), ct, mrt);
    }

    @Test
    @DatabaseSetup("/trainingSitesDataSet.xml")
    public void testFilterTrainingSiteByMedicalField() throws Exception {
	TrainingSiteFilter filter = new TrainingSiteFilter();
	filter.setAllowParents(true);
	filter.setAllowDepartments(true);
	filter.setMedicalField(trainingDAO.getMedicalField(4L)); // unmittelbare Patientenversorgung

	List<TrainingSite> trainingSites = trainingSiteDAO.getTrainingSites(filter, null, true, true);
	assertSortedList(trainingSites, trainingSiteDAO.getTrainingSite(12L, false, false), trainingSiteDAO.getTrainingSite(13L, false, false), trainingSiteDAO.getTrainingSite(31L, false, false), trainingSiteDAO.getTrainingSite(11L, false, false), trainingSiteDAO.getTrainingSite(50L, false, false), trainingSiteDAO.getTrainingSite(21L, false, false),
		trainingSiteDAO.getTrainingSite(41L, false, false));

	filter.setMedicalField(trainingDAO.getMedicalField(2L));
	trainingSites = trainingSiteDAO.getTrainingSites(filter, null, true, true);
	assertSortedList(trainingSites, trainingSiteDAO.getTrainingSite(11L, false, false));
    }

    @SuppressWarnings("deprecation")
    @Test
    @DatabaseSetup("/trainingSitesDataSet.xml")
    @Transactional
    @Ignore
    public void testFilterTrainingSiteByZipCodeAndDistance() throws Exception {
	initSQLFunctions();

	TrainingSiteFilter filter = new TrainingSiteFilter();
	filter.setZipCode("04107");
	filter.setDistance(112);
	filter.setLatitude(11.0);
	filter.setLongitude(10.0);

	List<TrainingSite> trainingSites = trainingSiteDAO.getTrainingSites(filter, null, true, true);

	System.out.println(trainingSites);

	assertNotNull(trainingSites);
	assertEquals(3, trainingSites.size());
    }

    @Test
    @DatabaseSetup("/trainingSitesDataSet.xml")
    public void testFilterTrainingSiteDepartments() throws Exception {
	TrainingSiteFilter filter = new TrainingSiteFilter();
	filter.setAllowDepartments(true);
	filter.setAllowParents(false);

	List<TrainingSite> trainingSiteDepartments = trainingSiteDAO.getTrainingSites(filter, null, false, false);

	assertSortedList(trainingSiteDepartments, trainingSiteDAO.getTrainingSite(12L, false, false), trainingSiteDAO.getTrainingSite(13L, false, false), trainingSiteDAO.getTrainingSite(31L, false, false), trainingSiteDAO.getTrainingSite(11L, false, false), trainingSiteDAO.getTrainingSite(21L, false, false), trainingSiteDAO.getTrainingSite(41L, false, false));
    }

    @Test
    @DatabaseSetup("/trainingSitesDataSet.xml")
    public void testFilterTrainingSites() throws Exception {
	List<TrainingSite> trainingSites = trainingSiteDAO.getTrainingSites(new TrainingSiteFilter(), null, false, false);

	assertSortedList(trainingSites, trainingSiteDAO.getTrainingSite(2L, false, false), trainingSiteDAO.getTrainingSite(1L, false, false), trainingSiteDAO.getTrainingSite(4L, false, false), trainingSiteDAO.getTrainingSite(3L, false, false), trainingSiteDAO.getTrainingSite(50L, false, false));
    }

    @Test
    @DatabaseSetup("/trainingSitesDataSet.xml")
    public void testOrderTrainingSiteDepartmentsByTrainingSiteNameAndDepartmentName() throws Exception {
	TrainingSite trainingSite = trainingSiteDAO.getTrainingSite(1L, true, false);

	assertNotNull(trainingSite.getChildren());
	assertEquals(3, trainingSite.getChildren().size());
	assertEquals(trainingSiteDAO.getTrainingSite(12L, false, false), trainingSite.getChildren().get(0));
	assertEquals(trainingSiteDAO.getTrainingSite(13L, false, false), trainingSite.getChildren().get(1));
	assertEquals(trainingSiteDAO.getTrainingSite(11L, false, false), trainingSite.getChildren().get(2));
    }

    @Test
    @DatabaseSetup("/trainingSitesDataSet.xml")
    public void testOrderTrainingSitesWithDepartmentsByName() throws Exception {
	List<TrainingSite> trainingSites = trainingSiteDAO.getTrainingSites(new TrainingSiteFilter(), null, true, false);

	assertNotNull(trainingSites);
	assertEquals(5, trainingSites.size());
	assertEquals(trainingSiteDAO.getTrainingSite(2L, false, false).getUid(), trainingSites.get(0).getUid());
	assertNotNull(trainingSites.get(0).getChildren());
	assertEquals(1, trainingSites.get(0).getChildren().size());

	assertEquals(trainingSiteDAO.getTrainingSite(1L, false, false), trainingSites.get(1));
	assertNotNull(trainingSites.get(1).getChildren());
	assertEquals(3, trainingSites.get(1).getChildren().size());
	assertEquals(trainingSiteDAO.getTrainingSite(12L, false, false), trainingSites.get(1).getChildren().get(0));
	assertEquals(trainingSiteDAO.getTrainingSite(13L, false, false), trainingSites.get(1).getChildren().get(1));
	assertEquals(trainingSiteDAO.getTrainingSite(11L, false, false), trainingSites.get(1).getChildren().get(2));

	assertEquals(trainingSiteDAO.getTrainingSite(4L, false, false), trainingSites.get(2));
	assertNotNull(trainingSites.get(2).getChildren());
	assertEquals(1, trainingSites.get(2).getChildren().size());

	assertEquals(trainingSiteDAO.getTrainingSite(3L, false, false), trainingSites.get(3));
	assertNotNull(trainingSites.get(3).getChildren());
	assertEquals(1, trainingSites.get(3).getChildren().size());

	// TODO: die Assertion noch mit reinnehmen, um zu prüfen, dass die Filtermethode
	// das richtige zurückliefert
	// assertEquals(trainingSiteDAO.getTrainingSites(null, true, false),
	// trainingSiteDAO.getTrainingSites(new TrainingSiteFilter(), null));
    }

    @Test
    @WithMockUser
    public void testSaveTrainingSiteDepartment() throws Exception {
	TrainingSite trainingSite = new TrainingSite();
	trainingSite.setName("TrainingSite");
	trainingSite.getAddress().setStreet("Goerdelerring 9");
	trainingSite.getAddress().setZipCode("04109");
	trainingSite.getAddress().setCity("Leipzig");
	trainingSite.setTrainingSiteType(TrainingSiteType.CLINIC);

	TrainingSite trainingSiteDepartment = new TrainingSite(trainingSite);
	trainingSiteDepartment.setParent(trainingSite);
	trainingSiteDepartment.setMedicalField(trainingDAO.getMedicalField(1L));
	trainingSiteDepartment.setTreatmentType(TreatmentType.Inpatient);
	trainingSiteDepartment.setName("TrainingSiteDepartmet");
	trainingSiteDepartment.setTrainingSiteType(TrainingSiteType.DEPARTMENT);

	trainingSite = trainingSiteDAO.save(trainingSite);

	assertNotNull(trainingSite.getChildren());
	assertEquals(1, trainingSite.getChildren().size());
	assertNotNull(Lists.newArrayList(trainingSite.getChildren()).get(0).getUid());
    }
}
