package org.infai.ddls.medicaltraining.test.model;

import org.infai.ddls.medicaltraining.model.AuthorisedDoctor;
import org.infai.ddls.medicaltraining.model.Registrar;
import org.infai.ddls.medicaltraining.repo.PersonDAO;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

public class PersonDAOTest2 extends AbstractSpringRepositoryTest {
    @Autowired
    private PersonDAO personDAO;

    @Test
    @WithMockUser
//    @DatabaseSetup("/doctorsDataSet.xml")
    public void testInitialiseDoctorsDataSet() throws Exception {
	AuthorisedDoctor authorisedDoctor = new AuthorisedDoctor();
	authorisedDoctor.setName("Vorname");
	authorisedDoctor.setLastName("Nachname");
	authorisedDoctor.getAddress().setStreet("Goerdelerring 9");
	authorisedDoctor.getAddress().setZipCode("04109");
	authorisedDoctor.getAddress().setCity("Leipzig");

	authorisedDoctor = personDAO.save(authorisedDoctor);
	System.out.println(authorisedDoctor.getUid());

	Registrar registrar = new Registrar();
	registrar.setName("Vorname");
	registrar.setLastName("Nachname");
	registrar.getAddress().setStreet("Goerdelerring 9");
	registrar.getAddress().setZipCode("04109");
	registrar.getAddress().setCity("Leipzig");

	registrar = personDAO.save(registrar);
	System.out.println(registrar.getUid());
    }
}
