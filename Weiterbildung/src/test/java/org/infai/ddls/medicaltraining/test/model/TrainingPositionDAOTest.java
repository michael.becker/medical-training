package org.infai.ddls.medicaltraining.test.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.infai.ddls.medicaltraining.model.TrainingPositionField;
import org.infai.ddls.medicaltraining.repo.RotationPlanDAO;
import org.infai.ddls.medicaltraining.repo.TrainingDAO;
import org.infai.ddls.medicaltraining.repo.TrainingPositionDAO;
import org.infai.ddls.medicaltraining.spec.TrainingPositionFieldFilter;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import com.github.springtestdbunit.annotation.DatabaseSetup;

@DatabaseSetup("/doctorsDataSet.xml")
@DatabaseSetup("/medicalFieldsDataSet.xml")
@DatabaseSetup("/trainingSchemesDataSet.xml")
@DatabaseSetup("/trainingSitesDataSet.xml")
@DatabaseSetup("/authorisationsDataSet.xml")
@DatabaseSetup("/trainingPositionsDataSet.xml")
public class TrainingPositionDAOTest extends AbstractSpringRepositoryTest {
    @Autowired
    private TrainingPositionDAO trainingPositionDAO;
    @Autowired
    private TrainingDAO trainingDAO;
    @Autowired
    private RotationPlanDAO rotationPlanDAO;

    @Test
    @DatabaseSetup("/rotationPlansDataSet.xml")
    public void testDeleteTrainingPositionWithAssignedRotationPlanSegmentsFailure() throws Exception {
	TrainingPositionField position = trainingPositionDAO.getTrainingPositionField(1L);

	assertNotNull(position);
	assertEquals(5, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	assertEquals(2, rotationPlanDAO.getRotationPlanSegments(position, null).size());
	assertTrue(rotationPlanDAO.getRotationPlanSegments(position, null).contains(rotationPlanDAO.getRotationPlanSegment(1L)));
	assertTrue(rotationPlanDAO.getRotationPlanSegments(position, null).contains(rotationPlanDAO.getRotationPlanSegment(10L)));

	assertFalse(trainingPositionDAO.delete(trainingPositionDAO.getTrainingPositionField(1L), false));
	position = trainingPositionDAO.getTrainingPositionField(1L);

	assertNotNull(position);
	assertEquals(5, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	assertEquals(2, rotationPlanDAO.getRotationPlanSegments(trainingPositionDAO.getTrainingPositionField(1L), null).size());
	assertTrue(rotationPlanDAO.getRotationPlanSegments(position, null).contains(rotationPlanDAO.getRotationPlanSegment(1L)));
	assertTrue(rotationPlanDAO.getRotationPlanSegments(position, null).contains(rotationPlanDAO.getRotationPlanSegment(10L)));
    }

    @Test
    @DatabaseSetup("/rotationPlansDataSet.xml")
    @WithMockUser
    public void testDeleteTrainingPositionWithAssignedRotationPlanSegmentsSuccess() throws Exception {
	TrainingPositionField position = trainingPositionDAO.getTrainingPositionField(1L);
	assertNotNull(position);
	assertEquals(5, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	assertEquals(2, rotationPlanDAO.getRotationPlanSegments(position, null).size());
	assertTrue(rotationPlanDAO.getRotationPlanSegments(position, null).contains(rotationPlanDAO.getRotationPlanSegment(1L)));
	assertTrue(rotationPlanDAO.getRotationPlanSegments(position, null).contains(rotationPlanDAO.getRotationPlanSegment(10L)));

	assertTrue(trainingPositionDAO.delete(trainingPositionDAO.getTrainingPositionField(1L), true));

	assertNull(trainingPositionDAO.getTrainingPositionField(1L));
	assertEquals(4, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	assertNull(rotationPlanDAO.getRotationPlanSegment(1L));
	assertNull(rotationPlanDAO.getRotationPlanSegment(10L));
    }

    @Test
    @DatabaseSetup("/rotationPlansDataSet.xml")
    @WithMockUser
    public void testDeleteTrainingPositionWithoutDependencies() throws Exception {
	assertNotNull(trainingPositionDAO.getTrainingPositionField(5L));
	assertEquals(5, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	assertEquals(0, rotationPlanDAO.getRotationPlanSegments(trainingPositionDAO.getTrainingPositionField(5L), null).size());

	assertTrue(trainingPositionDAO.delete(trainingPositionDAO.getTrainingPositionField(5L)));
	assertNull(trainingPositionDAO.getTrainingPositionField(5L));
	assertEquals(4, trainingPositionDAO.getTrainingPositionsField(null, false).size());
	assertEquals(8, rotationPlanDAO.getRotationPlanSegments(null).size());
    }

    @Test
    public void testFilterTrainingPositionsByMedicalField() throws Exception {
	TrainingPositionFieldFilter filter = new TrainingPositionFieldFilter();
	filter.setMedicalField(trainingDAO.getMedicalField(4L, true, true));

	System.out.println("++++FILTER: " + filter.getMedicalField().getAllChildFields());

	List<TrainingPositionField> positions = trainingPositionDAO.getTrainingPositionsField(filter, null);

	System.out.println(positions);
	System.out.println(trainingPositionDAO.getTrainingPositionsField(null, true));

	assertNotNull(positions);
	assertEquals(5, positions.size());
    }

    @Test
    @Transactional
    @WithMockUser
    public void testFilterTrainingPositionsByZipCode() throws Exception {
	initSQLFunctions();
	TrainingPositionFieldFilter filter = new TrainingPositionFieldFilter();
	filter.setZipCode("04109");
	filter.setDistance(50);

	trainingPositionDAO.getTrainingPositionsField(filter, null);
    }

    @Test
    public void testGetPositionsByAvailableFromAndAvailableUntil() throws Exception {
	TrainingPositionFieldFilter filter = new TrainingPositionFieldFilter();
	filter.setAvailableFrom(LocalDate.parse("2017-01-01"));
	filter.setAvailableUntil(LocalDate.parse("2020-12-31"));
	assertEquals(5, trainingPositionDAO.getTrainingPositionsField(filter, null).size());

	filter.setAvailableFrom(LocalDate.parse("2016-12-31"));
	filter.setAvailableUntil(LocalDate.parse("2020-12-31"));
	assertEquals(0, trainingPositionDAO.getTrainingPositionsField(filter, null).size());

	filter.setAvailableFrom(LocalDate.parse("2017-01-01"));
	filter.setAvailableUntil(LocalDate.parse("2021-01-01"));
	assertEquals(0, trainingPositionDAO.getTrainingPositionsField(filter, null).size());
    }
}
