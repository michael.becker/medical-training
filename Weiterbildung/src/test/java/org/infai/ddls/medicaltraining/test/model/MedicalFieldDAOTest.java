package org.infai.ddls.medicaltraining.test.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.infai.ddls.medicaltraining.model.MedicalField;
import org.infai.ddls.medicaltraining.repo.TrainingDAO;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.springtestdbunit.annotation.DatabaseSetup;

public class MedicalFieldDAOTest extends AbstractSpringRepositoryTest {
    @Autowired
    private TrainingDAO trainingDAO;

    @Test(expected = ConstraintViolationException.class)
    public void testCreateDirectCycleFails() throws Exception {
	MedicalField medicalField = new MedicalField("MedicalField");
	medicalField.setParent(medicalField);

	trainingDAO.save(medicalField);
    }

    @Test
    @DatabaseSetup("/medicalFieldsDataSet.xml")
    public void testCreateTransitiveCycleFailsWithExistingMedicalFields() throws Exception {
	MedicalField unmittelbarePatientenversorgung = trainingDAO.getMedicalField(4L, true, true);
	MedicalField allgemeinmedizin = trainingDAO.getMedicalField(5L, true, true);

	unmittelbarePatientenversorgung.setParent(allgemeinmedizin);

	try {
	    trainingDAO.save(unmittelbarePatientenversorgung);
	    fail("expected constraintviolationexception");
	} catch (ConstraintViolationException e) {
	}
    }

    @Test
    public void testCreateTransitiveCycleFailsWithNewMedicalFields() throws Exception {
	MedicalField parentField = new MedicalField("parent");
	MedicalField childField = new MedicalField("child");

	childField.setParent(parentField);
	parentField.setParent(childField);

	assertEquals(childField, childField.getParent().getParent());

	try {
	    trainingDAO.save(parentField);
	    fail("expected constraintviolationexception");
	} catch (ConstraintViolationException e) {
	}
    }

    @Test
    @DatabaseSetup("/medicalFieldsDataSet.xml")
    public void testGetChildren() throws Exception {
	MedicalField unmittelbarePatientenversorgung = trainingDAO.getMedicalField(4L, true, true);

	assertNotNull(unmittelbarePatientenversorgung.getAllChildFields());
	assertEquals(6, unmittelbarePatientenversorgung.getAllChildFields().size());
    }

    @Test
    @Transactional
    public void testNestMedicalFields() throws Exception {
	MedicalField unmittelbarePatientenversorgung = new MedicalField("Unmittelbare Patientenversorgung");
	Long unmitUID = trainingDAO.save(unmittelbarePatientenversorgung).getUid();
	MedicalField hausarzt = new MedicalField("Hausärztliche Versorgung", "HÄV", unmittelbarePatientenversorgung);
	Long hausarztUID = trainingDAO.save(hausarzt).getUid();
	MedicalField allgemeinMedizin = new MedicalField("Allgemeinmedizin", "Allgemeinmedizin", hausarzt);
	Long allgUID = trainingDAO.save(allgemeinMedizin).getUid();
	MedicalField anaesthesiologie = new MedicalField("Anästhesiologie", "Anästhesiologie", unmittelbarePatientenversorgung);
	Long anaeUID = trainingDAO.save(anaesthesiologie).getUid();

	unmittelbarePatientenversorgung = trainingDAO.getMedicalField(unmitUID, true);
	assertNotNull(unmittelbarePatientenversorgung);
	assertNotNull(unmittelbarePatientenversorgung.getChildren());
	assertEquals(2, unmittelbarePatientenversorgung.getChildren().size());

	hausarzt = trainingDAO.getMedicalField(hausarztUID, true);
	assertNotNull(hausarzt);
	assertNotNull(hausarzt.getChildren());
	assertEquals(1, hausarzt.getChildren().size());
	assertNotNull(hausarzt.getParent());
	assertEquals(unmittelbarePatientenversorgung.getUid(), hausarzt.getParent().getUid());

	allgemeinMedizin = trainingDAO.getMedicalField(allgUID, false);
	assertNotNull(allgemeinMedizin);
	assertNotNull(allgemeinMedizin.getParent());
	assertEquals(hausarzt.getUid(), allgemeinMedizin.getParent().getUid());

	anaesthesiologie = trainingDAO.getMedicalField(anaeUID, false);
	assertNotNull(anaesthesiologie);
	assertNotNull(anaesthesiologie.getParent());
	assertEquals(unmittelbarePatientenversorgung.getUid(), anaesthesiologie.getParent().getUid());
    }

    @Test
    @DatabaseSetup("/medicalFieldsDataSet.xml")
    public void testValidationOfExistingFieldWithoutParentFieldSucceeds() throws Exception {
	MedicalField unmittelbare = trainingDAO.getMedicalField(4L, true, true);

	assertTrue(validator.validate(unmittelbare).isEmpty());
    }

    @Test
    public void testValidationOfNewFieldWithoutParentFieldsSucceeds() throws Exception {
	MedicalField medicalField = new MedicalField("medicalField");

	assertTrue(validator.validate(medicalField).isEmpty());
    }
}
