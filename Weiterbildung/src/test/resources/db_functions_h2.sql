DROP ALIAS IF EXISTS lat_lng_distance;

CREATE ALIAS lat_lng_distance FOR "org.infai.ddls.medicaltraining.model.Location.getLatLngDistance";